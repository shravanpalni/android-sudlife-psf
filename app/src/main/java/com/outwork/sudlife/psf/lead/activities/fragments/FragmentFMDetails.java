package com.outwork.sudlife.psf.lead.activities.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.notifications.GlobalData;
import com.outwork.sudlife.psf.lead.activities.CreateFamilyMemberActivity;
import com.outwork.sudlife.psf.lead.adapters.FamilyMemberAdapter;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bvlbh on 4/16/2018.
 */

public class FragmentFMDetails extends Fragment {

    ListView lvBottom;
    BottomSheetDialog bottomSheetDialog;
    RecyclerView rcvFamilyMembers;
    EditText etName, etRelation, etOccupation, etLeadType, etAge, etWorking, etAppointmentDate, etFinancialPlanningDoneFor, etTentativeInvestmentYear, etFinancialPlanninginFutureFor, etTentativeAmount;
    TextInputLayout tilOccupation;
    Button bntFMCreate;
    TextView tvNoData;
    String pvisitdate;
    LeadModel leadModel;
    private FragmentActivity myContext;
    FloatingActionButton fab;
    ArrayList<String> stringArrayList;
    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                etAppointmentDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etAppointmentDate.setText(visitdt);
                etAppointmentDate.setEnabled(true);
                pvisitdate = TimeUtils.convertDatetoUnix(visitdt);
            }
        }

        @Override
        public void onDateTimeCancel() {
            etAppointmentDate.setEnabled(true);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.family_member_creation, null);
        tvNoData = (TextView) view.findViewById(R.id.tv_nodata);
        fab = (FloatingActionButton) view.findViewById(R.id.fb_lead_add);
        rcvFamilyMembers = (RecyclerView) view.findViewById(R.id.rcv_family_members);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreateFamilyMemberActivity.class);
                intent.putExtra("type", "create");
                startActivity(intent);
            }
        });
        getFamilyMembers(GlobalData.getInstance().getLeadModel());
        return view;
    }

    public void getFamilyMembers(LeadModel leadModel) {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> postFM = client.getFamilyMembers(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), leadModel.getLeadid());
        postFM.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {

                        Type listType = new TypeToken<ArrayList<LeadModel>>() {
                        }.getType();
                        if (response.body() != null) {
                            ArrayList<LeadModel> leadModelArrayList = new Gson().fromJson(response.body().getData(), listType);
                            if (leadModelArrayList.size() != 0) {
                                rcvFamilyMembers.setVisibility(View.VISIBLE);
                                tvNoData.setVisibility(View.GONE);
                                FamilyMemberAdapter familyMemberAdapter = new FamilyMemberAdapter(leadModelArrayList, getActivity());
                                rcvFamilyMembers.setAdapter(familyMemberAdapter);
                                rcvFamilyMembers.setLayoutManager(new LinearLayoutManager(getActivity()));
                            } else {
                                tvNoData.setVisibility(View.VISIBLE);
                                rcvFamilyMembers.setVisibility(View.GONE);
                            }
                        } else {
                            tvNoData.setVisibility(View.VISIBLE);
                            rcvFamilyMembers.setVisibility(View.GONE);
                        }
                        //showAlertDialog(response.body().getMessage());
                    } else {
                        tvNoData.setVisibility(View.VISIBLE);
                        rcvFamilyMembers.setVisibility(View.GONE);
                    }
                } else {
                    showAlertDialog("Failed");
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                showAlertDialog("Failed");

            }
        });
      /*  try {
            Response<RestResponse> restResponse = postFM.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                    showAlertDialog(restResponse.message());
                }
            } else {
                showAlertDialog("Failed to Create");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }


    public void bottomSheetDialog(final EditText editText, ArrayList<String> strings, String title) {
        View modalbottomsheet = getActivity().getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(getActivity());
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());

                bottomSheetDialog.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });

    }

    public void showAlertDialog(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }



 /*   @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getFamilyMembers(GlobalData.getInstance().getLeadModel());
    }*/
}
