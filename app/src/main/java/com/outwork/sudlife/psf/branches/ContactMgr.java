package com.outwork.sudlife.psf.branches;

import android.content.Context;

import java.util.List;

import com.outwork.sudlife.psf.branches.dao.ContactDao;
import com.outwork.sudlife.psf.branches.models.ContactModel;

/**
 * Created by Panch on 2/23/2017.
 */

public class ContactMgr {
    private static Context context;
    private static ContactDao contactDao;

    public ContactMgr(Context context) {
        this.context = context;
        contactDao = contactDao.getInstance(context);
    }

    private static ContactMgr instance;

    public static ContactMgr getInstance(Context context) {
        if (instance == null)
            instance = new ContactMgr(context);
        return instance;
    }

    public static boolean insertContact(Context context, ContactModel contactModel) {
        contactDao.insertContact(contactModel);
        return true;
    }

    public boolean insertContactList(List<ContactModel> contactModelList) {
        contactDao.insertContactList(contactModelList);
        return true;
    }

    public void insertLocalContact(ContactModel contactModelList, String status) {
        contactDao.insertLocalContact(contactModelList, status);
    }

    public ContactModel getContact(String customerId) {
        return contactDao.getContact(customerId);
    }

    public List<ContactModel> getContactByCustomerID(String groupId, String userId, String customerID) {
        return contactDao.getContactByCustomerID(groupId, userId, customerID);
    }

    public List<ContactModel> getContactByStatus(String userId, String status) {
        return contactDao.getContactByStatus(userId, status);
    }

    public ContactModel getContactbyId(String contactId) {
        return contactDao.getContactbyId(contactId);
    }

    public ContactModel getContactbylocalId(int contactId) {
        return contactDao.getContactbylocalId(contactId);
    }

    public List<ContactModel> getContactList(String groupId, String userId) {
        return contactDao.getContactList(groupId, userId);
    }

    public List<String> getContactNamesList(String userId) {
        return contactDao.getContactNamesList(userId);
    }

    public List<ContactModel> getContactList(String groupId, String userId, String userInput) {
        return contactDao.getContactList(groupId, userId, userInput);
    }
    public List<ContactModel> getContactSearchList(String groupId, String userId, String userInput) {
        return contactDao.getContactSearchList(groupId, userId, userInput);
    }

    public List<ContactModel> getContactOnlineList(String groupId, String userId, String userInput, String status) {
        return contactDao.getContactOnlineList(groupId, userId, userInput, status);
    }

    public String getMaxModifiedDate(String groupId, String userId) {
        return contactDao.getMaxModifiedDate(groupId, userId);
    }

    public boolean updateContactInfo(String phoneNO, String eMail, String designation, String speciality, String classification, String contactid) {
        return contactDao.updateContactInfo(phoneNO, eMail, designation, speciality, classification, contactid);
    }

    public void updateContact(ContactModel contactModel, String userid) {
        contactDao.updateContact(contactModel, userid);
    }

    public void updateContactOnline(ContactModel contactModel, String userid, String contactId) {
        contactDao.updateContactOnline(contactModel, userid, contactId);
    }

}