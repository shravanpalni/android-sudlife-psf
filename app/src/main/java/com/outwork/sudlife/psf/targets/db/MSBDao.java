package com.outwork.sudlife.psf.targets.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.outwork.sudlife.psf.dao.DBHelper;
import com.outwork.sudlife.psf.dao.IvokoProvider;
import com.outwork.sudlife.psf.targets.models.MSBModel;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;

/**
 * Created by bvlbh on 3/24/2018.
 */


public class MSBDao {

    public static final String TAG = MSBDao.class.getSimpleName();

    private static MSBDao instance;
    private Context applicationContext;

    public MSBDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public static MSBDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new MSBDao(applicationContext);
        }
        return instance;
    }


    /*public void insertMSBRecords(ArrayList<MSBModel> modelArrayList) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        for (int i = 0; i < modelArrayList.size(); i++) {
            values.put(IvokoProvider.MonthlySummeryForBranch.FTM, modelArrayList.getFtm());
            values.put(IvokoProvider.MonthlySummeryForBranch.ACTIVITIES_PLANNED, modelArrayList.getActivitiesplanned());
            values.put(IvokoProvider.MonthlySummeryForBranch.VISITS_PLANNED, modelArrayList.getVisitsplanned());
            values.put(IvokoProvider.MonthlySummeryForBranch.EXPECTED_CONNECTIONS, modelArrayList.getExpectedconncetions());
            values.put(IvokoProvider.MonthlySummeryForBranch.EXPECTED_LEADS, modelArrayList.getExpectedleads());
            values.put(IvokoProvider.MonthlySummeryForBranch.EXPECTED_BUSINESS, modelArrayList.getExpectedbusiness());
            values.put(IvokoProvider.MonthlySummeryForBranch.ACTIVITES_COMPLETED, modelArrayList.getActivitiescompleted());
            values.put(IvokoProvider.MonthlySummeryForBranch.VISITS_COMPLETED, modelArrayList.getVistiscompleted());
            values.put(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_CONNECTION, modelArrayList.getAchievedconnection());
            values.put(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_LEADS, modelArrayList.getAchievedleads());
            values.put(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_BUSINESS, modelArrayList.getAchievedbusiness());
            values.put(IvokoProvider.MonthlySummeryForBranch.STATUS_OF_SUCCESS_OR_FAIL, modelArrayList.getStatusofsuccessorfail());
            values.put(IvokoProvider.MonthlySummeryForBranch.NETWORK_STATUS, modelArrayList.getNetworkstatus());
            values.put(IvokoProvider.MonthlySummeryForBranch.OFFLINE_OR_ONLINE_STATUS, modelArrayList.getOfflineoronlinestatus());
            values.put(IvokoProvider.MonthlySummeryForBranch.MONTH, modelArrayList.getMonth());
            values.put(IvokoProvider.MonthlySummeryForBranch.YEAR, modelArrayList.getYear());
            values.put(IvokoProvider.MonthlySummeryForBranch.CUSTOMER_NAME, modelArrayList.getCustomername());
            values.put(IvokoProvider.MonthlySummeryForBranch.TARGET_ID, modelArrayList.getTargetid());
            values.put(IvokoProvider.MonthlySummeryForBranch.OBJECT_ID, modelArrayList.getObjectid());
            db.insert(IvokoProvider.MonthlySummeryForBranch.TABLE_NAME, null, values);
        }
    }*/


    public void insertMSBRecords(MSBModel msbModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.MonthlySummeryForBranch.FTM, msbModel.getFtm());
        values.put(IvokoProvider.MonthlySummeryForBranch.ACTIVITIES_PLANNED, msbModel.getActivitiesplanned());
        values.put(IvokoProvider.MonthlySummeryForBranch.VISITS_PLANNED, msbModel.getVisitsplanned());
        values.put(IvokoProvider.MonthlySummeryForBranch.EXPECTED_CONNECTIONS, msbModel.getExpectedconncetions());
        values.put(IvokoProvider.MonthlySummeryForBranch.EXPECTED_LEADS, msbModel.getExpectedleads());
        values.put(IvokoProvider.MonthlySummeryForBranch.EXPECTED_BUSINESS, msbModel.getExpectedbusiness());
        values.put(IvokoProvider.MonthlySummeryForBranch.ACTIVITES_COMPLETED, msbModel.getActivitiescompleted());
        values.put(IvokoProvider.MonthlySummeryForBranch.VISITS_COMPLETED, msbModel.getVistiscompleted());
        values.put(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_CONNECTION, msbModel.getAchievedconnection());
        values.put(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_LEADS, msbModel.getAchievedleads());
        values.put(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_BUSINESS, msbModel.getAchievedbusiness());
        values.put(IvokoProvider.MonthlySummeryForBranch.STATUS_OF_SUCCESS_OR_FAIL, msbModel.getStatusofsuccessorfail());
        values.put(IvokoProvider.MonthlySummeryForBranch.NETWORK_STATUS, msbModel.getNetworkstatus());
        values.put(IvokoProvider.MonthlySummeryForBranch.OFFLINE_OR_ONLINE_STATUS, msbModel.getOfflineoronlinestatus());
        values.put(IvokoProvider.MonthlySummeryForBranch.MONTH, msbModel.getMonth());
        values.put(IvokoProvider.MonthlySummeryForBranch.YEAR, msbModel.getYear());
        values.put(IvokoProvider.MonthlySummeryForBranch.CUSTOMER_NAME, msbModel.getCustomername());
        values.put(IvokoProvider.MonthlySummeryForBranch.TARGET_ID, msbModel.getTargetid());
        values.put(IvokoProvider.MonthlySummeryForBranch.OBJECT_ID, msbModel.getObjectid());
        values.put(IvokoProvider.MonthlySummeryForBranch.USER_ID, msbModel.getUserid());
        db.insert(IvokoProvider.MonthlySummeryForBranch.TABLE_NAME, null, values);
    }

    public void insertTargetsList(ArrayList<MSBModel> msbModelArrayList) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        try {
            for (int j = 0; j < msbModelArrayList.size(); j++) {
                MSBModel msbModel = msbModelArrayList.get(j);
                if (Utils.isNotNullAndNotEmpty(msbModel.getTargetid()))
                    if (isTargetExist(db, msbModel.getTargetid())) {
                        deleteTargetRecordID(msbModel.getTargetid());
                        msbModel.setUserid(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        if (Utils.isNotNullAndNotEmpty(msbModel.getCustomername()))
                            insertMSBRecords(msbModel);
                    } else {
                        msbModel.setUserid(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        if (Utils.isNotNullAndNotEmpty(msbModel.getCustomername()))
                            insertMSBRecords(msbModel);
                    }
            }
        } finally {
        }
    }

    public ArrayList<MSBModel> getMSBRecordsBasedOnDate(String userId, int month, int date) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        ArrayList<MSBModel> msbModelArrayList = new ArrayList<>();
        Cursor cursor = null;

        // security
        //String sql = "";
        /*sql += "SELECT * FROM " + IvokoProvider.MonthlySummeryForBranch.TABLE_NAME;
        sql += " WHERE " + IvokoProvider.MonthlySummeryForBranch.USER_ID + " = '" + userId + "'";
        sql += " AND " + IvokoProvider.MonthlySummeryForBranch.MONTH + " = '" + month + "'";
        sql += " AND " + IvokoProvider.MonthlySummeryForBranch.DATE + " = '" + date + "'";*/
        //Cursor cursor = db.rawQuery(sql, null);

        cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.MonthlySummeryForBranch.TABLE_NAME + " WHERE " + IvokoProvider.MonthlySummeryForBranch.USER_ID + " = ?" + " AND " +
                IvokoProvider.MonthlySummeryForBranch.MONTH + " = ? " + " AND " +
                IvokoProvider.MonthlySummeryForBranch.DATE + " = ? " , new String[]{userId,String.valueOf(month),String.valueOf(date)});

        if (cursor.moveToFirst()) {
            do {
                MSBModel msbModel = new MSBModel();
                msbModel.setId(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch._ID)));
                msbModel.setFtm(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.FTM)));
                msbModel.setActivitiesplanned(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACTIVITIES_PLANNED)));
                msbModel.setVisitsplanned(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.VISITS_PLANNED)));
                msbModel.setExpectedconncetions(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_CONNECTIONS)));
                msbModel.setExpectedleads(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_LEADS)));
                msbModel.setExpectedbusiness(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_BUSINESS)));
                msbModel.setActivitiescompleted(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACTIVITES_COMPLETED)));
                msbModel.setVistiscompleted(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.VISITS_COMPLETED)));
                msbModel.setAchievedconnection(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_CONNECTION)));
                msbModel.setAchievedleads(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_LEADS)));
                msbModel.setAchievedbusiness(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_BUSINESS)));
                msbModel.setStatusofsuccessorfail(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.STATUS_OF_SUCCESS_OR_FAIL)));
                msbModel.setNetworkstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.NETWORK_STATUS)));
                msbModel.setOfflineoronlinestatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.OFFLINE_OR_ONLINE_STATUS)));
                msbModel.setMonth(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.MONTH)));
                msbModel.setYear(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.YEAR)));
                msbModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.CUSTOMER_NAME)));
                msbModel.setTargetid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.TARGET_ID)));
                msbModel.setObjectid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.OBJECT_ID)));
                msbModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.USER_ID)));
                msbModelArrayList.add(msbModel);
            } while (cursor.moveToNext());
        }
        return msbModelArrayList;

    }

    public ArrayList<MSBModel> getMSBRecordsBasedOnMonth(String userId, int month) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        ArrayList<MSBModel> msbModelArrayList = new ArrayList<>();



        //security
        /*String sql = "";
        sql += "SELECT * FROM " + IvokoProvider.MonthlySummeryForBranch.TABLE_NAME;
        sql += " WHERE " + IvokoProvider.MonthlySummeryForBranch.USER_ID + " = '" + userId + "'";
        sql += " AND " + IvokoProvider.MonthlySummeryForBranch.MONTH + " = '" + month + "'";*/
        //String query = "SELECT * FROM " + IvokoProvider.MonthlySummeryForBranch.TABLE_NAME + " WHERE " + IvokoProvider.MonthlySummeryForBranch.USER_ID + " = '" + userid + "'";
        //Cursor cursor = db.rawQuery(sql, null);
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.MonthlySummeryForBranch.TABLE_NAME + " WHERE " + IvokoProvider.MonthlySummeryForBranch.USER_ID + " = ?" + " AND " +
                IvokoProvider.MonthlySummeryForBranch.MONTH + " = ? "  , new String[]{userId,String.valueOf(month)});
        if (cursor.moveToFirst()) {
            do {
                MSBModel msbModel = new MSBModel();
                msbModel.setId(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch._ID)));
                msbModel.setFtm(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.FTM)));
                msbModel.setActivitiesplanned(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACTIVITIES_PLANNED)));
                msbModel.setVisitsplanned(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.VISITS_PLANNED)));
                msbModel.setExpectedconncetions(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_CONNECTIONS)));
                msbModel.setExpectedleads(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_LEADS)));
                msbModel.setExpectedbusiness(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_BUSINESS)));
                msbModel.setActivitiescompleted(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACTIVITES_COMPLETED)));
                msbModel.setVistiscompleted(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.VISITS_COMPLETED)));
                msbModel.setAchievedconnection(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_CONNECTION)));
                msbModel.setAchievedleads(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_LEADS)));
                msbModel.setAchievedbusiness(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_BUSINESS)));
                msbModel.setStatusofsuccessorfail(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.STATUS_OF_SUCCESS_OR_FAIL)));
                msbModel.setNetworkstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.NETWORK_STATUS)));
                msbModel.setOfflineoronlinestatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.OFFLINE_OR_ONLINE_STATUS)));
                msbModel.setMonth(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.MONTH)));
                msbModel.setYear(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.YEAR)));
                msbModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.CUSTOMER_NAME)));
                msbModel.setTargetid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.TARGET_ID)));
                msbModel.setObjectid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.OBJECT_ID)));
                msbModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.USER_ID)));
                msbModelArrayList.add(msbModel);
            } while (cursor.moveToNext());
        }
        return msbModelArrayList;

    }


    public void deleteTargetRecordID(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.MonthlySummeryForBranch.TABLE_NAME)) {
            db.delete(IvokoProvider.MonthlySummeryForBranch.TABLE_NAME, IvokoProvider.MonthlySummeryForBranch.TARGET_ID + "=?", new String[]{id});
//            db.execSQL("delete from " + Customer.TABLE + " WHERE " + Customer.customerid + " = '" + id + "'");
        }
    }

    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            else{
                cursor.close();
            }
        }
        return false;
    }


    public ArrayList<MSBModel> getMSBRecoreds(String userid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        ArrayList<MSBModel> msbModelArrayList = new ArrayList<>();

        Cursor cursor = null;
        // security
        // String query = "SELECT * FROM " + IvokoProvider.MonthlySummeryForBranch.TABLE_NAME + " WHERE " + IvokoProvider.MonthlySummeryForBranch.USER_ID + " = '" + userid + "'";

        cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.MonthlySummeryForBranch.TABLE_NAME + " WHERE " + IvokoProvider.MonthlySummeryForBranch.USER_ID + " = ?"   , new String[]{userid});


        //Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                MSBModel msbModel = new MSBModel();
                msbModel.setId(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch._ID)));
                msbModel.setFtm(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.FTM)));
                msbModel.setActivitiesplanned(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACTIVITIES_PLANNED)));
                msbModel.setVisitsplanned(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.VISITS_PLANNED)));
                msbModel.setExpectedconncetions(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_CONNECTIONS)));
                msbModel.setExpectedleads(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_LEADS)));
                msbModel.setExpectedbusiness(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_BUSINESS)));
                msbModel.setActivitiescompleted(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACTIVITES_COMPLETED)));
                msbModel.setVistiscompleted(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.VISITS_COMPLETED)));
                msbModel.setAchievedconnection(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_CONNECTION)));
                msbModel.setAchievedleads(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_LEADS)));
                msbModel.setAchievedbusiness(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_BUSINESS)));
                msbModel.setStatusofsuccessorfail(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.STATUS_OF_SUCCESS_OR_FAIL)));
                msbModel.setNetworkstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.NETWORK_STATUS)));
                msbModel.setOfflineoronlinestatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.OFFLINE_OR_ONLINE_STATUS)));
                msbModel.setMonth(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.MONTH)));
                msbModel.setYear(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.YEAR)));
                msbModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.CUSTOMER_NAME)));
                msbModel.setTargetid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.TARGET_ID)));
                msbModel.setObjectid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.OBJECT_ID)));
                msbModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.USER_ID)));
                msbModelArrayList.add(msbModel);
            } while (cursor.moveToNext());
        }
        return msbModelArrayList;
    }

    public ArrayList<MSBModel> getMSBOfflineRecoreds(String userid, String status) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        ArrayList<MSBModel> msbModelArrayList = new ArrayList<>();

        // security

       /* String query = "SELECT * FROM " + IvokoProvider.MonthlySummeryForBranch.TABLE_NAME + " WHERE " + IvokoProvider.MonthlySummeryForBranch.USER_ID + " = '" + userid + "'"
                + " AND " + IvokoProvider.MonthlySummeryForBranch.OFFLINE_OR_ONLINE_STATUS + " = '" + status + "'";*/

        Cursor cursor = null;
        cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.MonthlySummeryForBranch.TABLE_NAME + " WHERE " + IvokoProvider.MonthlySummeryForBranch.USER_ID + " = ?" + " AND " +
                IvokoProvider.MonthlySummeryForBranch.OFFLINE_OR_ONLINE_STATUS + " = ? "  , new String[]{userid,status});


        //Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                MSBModel msbModel = new MSBModel();
                msbModel.setId(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch._ID)));
                msbModel.setFtm(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.FTM)));
                msbModel.setActivitiesplanned(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACTIVITIES_PLANNED)));
                msbModel.setVisitsplanned(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.VISITS_PLANNED)));
                msbModel.setExpectedconncetions(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_CONNECTIONS)));
                msbModel.setExpectedleads(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_LEADS)));
                msbModel.setExpectedbusiness(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.EXPECTED_BUSINESS)));
                msbModel.setActivitiescompleted(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACTIVITES_COMPLETED)));
                msbModel.setVistiscompleted(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.VISITS_COMPLETED)));
                msbModel.setAchievedconnection(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_CONNECTION)));
                msbModel.setAchievedleads(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_LEADS)));
                msbModel.setAchievedbusiness(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.ACHIEVED_BUSINESS)));
                msbModel.setStatusofsuccessorfail(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.STATUS_OF_SUCCESS_OR_FAIL)));
                msbModel.setNetworkstatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.NETWORK_STATUS)));
                msbModel.setOfflineoronlinestatus(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.OFFLINE_OR_ONLINE_STATUS)));
                msbModel.setMonth(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.MONTH)));
                msbModel.setYear(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.YEAR)));
                msbModel.setCustomername(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.CUSTOMER_NAME)));
                msbModel.setTargetid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.TARGET_ID)));
                msbModel.setObjectid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.OBJECT_ID)));
                msbModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.MonthlySummeryForBranch.USER_ID)));
                msbModelArrayList.add(msbModel);
            } while (cursor.moveToNext());
        }
        return msbModelArrayList;
    }

    public void editMSBRecoreds(ArrayList<MSBModel> editMsbModelArrayList) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
    }

    private boolean isTargetExist(SQLiteDatabase db, String objectId) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.MonthlySummeryForBranch.TABLE_NAME + " WHERE " + IvokoProvider.MonthlySummeryForBranch.TARGET_ID + " = ?", new String[]{objectId});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void updateRecord(MSBModel msbModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.MonthlySummeryForBranch.EXPECTED_CONNECTIONS, msbModel.getExpectedconncetions());
        values.put(IvokoProvider.MonthlySummeryForBranch.EXPECTED_LEADS, msbModel.getExpectedleads());
        values.put(IvokoProvider.MonthlySummeryForBranch.EXPECTED_BUSINESS, msbModel.getExpectedbusiness());
        values.put(IvokoProvider.MonthlySummeryForBranch.OFFLINE_OR_ONLINE_STATUS, msbModel.getOfflineoronlinestatus());
        try {
            db.update(IvokoProvider.MonthlySummeryForBranch.TABLE_NAME, values, IvokoProvider.MonthlySummeryForBranch._ID + " = " + msbModel.getId() + " AND " +
                    IvokoProvider.MonthlySummeryForBranch.TARGET_ID + " = ?", new String[]{msbModel.getTargetid()});
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void updateTargetRecordByStatus(MSBModel msbModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.MonthlySummeryForBranch.OFFLINE_OR_ONLINE_STATUS, "");
        try {
            db.update(IvokoProvider.MonthlySummeryForBranch.TABLE_NAME, values, IvokoProvider.MonthlySummeryForBranch._ID + " = " + msbModel.getId() + " AND " +
                    IvokoProvider.MonthlySummeryForBranch.TARGET_ID + " = ?", new String[]{msbModel.getTargetid()});
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
