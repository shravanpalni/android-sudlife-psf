package com.outwork.sudlife.psf.planner.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shravanch on 30-05-2018.
 */

public class TeamMemberPlannerModel implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("activityid")
    @Expose
    private String activityid;
    @SerializedName("activitytype")
    @Expose
    private String activitytype;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("customername")
    @Expose
    private String customername;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("customerid")
    @Expose
    private String customerid;
    @SerializedName("contactid")
    @Expose
    private String contactid;
    @SerializedName("contactname")
    @Expose
    private String contactname;
    @SerializedName("opportunityid")
    @Expose
    private String opportunityid;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("notificationstatus")
    @Expose
    private String notificationstatus;
    @SerializedName("scheduleday")
    @Expose
    private Integer scheduleday;
    @SerializedName("schedulemonth")
    @Expose
    private Integer schedulemonth;
    @SerializedName("scheduleyear")
    @Expose
    private Integer scheduleyear;
    @SerializedName("scheduletime")
    @Expose
    private Integer scheduletime;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("subtype")
    @Expose
    private String subtype;
    @SerializedName("purpose")
    @Expose
    private String purpose;
    @SerializedName("otheractions")
    @Expose
    private String otheractions;
    @SerializedName("otherfollowupactions")
    @Expose
    private String otherfollowupactions;
    @SerializedName("outcome")
    @Expose
    private String outcome;
    @SerializedName("planstatus")
    @Expose
    private Integer planstatus;
    @SerializedName("completionstatus")
    @Expose
    private Integer completionstatus;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("checkintime")
    @Expose
    private Integer checkintime;
    @SerializedName("checkinlocation")
    @Expose
    private String checkinlocation;
    @SerializedName("checkinlat")
    @Expose
    private String checkinlat;
    @SerializedName("checkinlong")
    @Expose
    private String checkinlong;
    @SerializedName("checkouttime")
    @Expose
    private Integer checkouttime;
    @SerializedName("checkoutlocation")
    @Expose
    private String checkoutlocation;
    @SerializedName("checkoutlat")
    @Expose
    private String checkoutlat;
    @SerializedName("checkoutlong")
    @Expose
    private String checkoutlong;
    @SerializedName("followupdate")
    @Expose
    private Integer followupdate;
    @SerializedName("followupaction")
    @Expose
    private String followupaction;
    @SerializedName("createdby")
    @Expose
    private String createdby;
    @SerializedName("createddate")
    @Expose
    private String createddate;
    @SerializedName("devicetimestamp")
    @Expose
    private Integer devicetimestamp;
    @SerializedName("modifiedby")
    @Expose
    private String modifiedby;
    @SerializedName("modifieddate")
    @Expose
    private String modifieddate;
    @SerializedName("productspromoted")
    @Expose
    private String productspromoted;
    @SerializedName("leadid")
    @Expose
    private String leadid;


    public String getLeadid() {
        return leadid;
    }

    public void setLeadid(String leadid) {
        this.leadid = leadid;
    }

    public Integer getLocalleadid() {
        return localleadid;
    }

    public void setLocalleadid(Integer localleadid) {
        this.localleadid = localleadid;
    }

    private Integer localleadid;

    @SerializedName("isdeleted")
    @Expose
    private Boolean isdeleted;
    @SerializedName("plancreatedby")
    @Expose
    private String plancreatedby;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("jointvisit")
    @Expose
    private Boolean jointvisit;
    @SerializedName("supervisorid")
    @Expose
    private String supervisorid;//for getting supervisorid
    @SerializedName("supervisorname")
    @Expose
    private String supervisorname;//for getting supervisor names
    @SerializedName("RescheduleReason")
    @Expose
    private String reshdreason;
    @SerializedName("RescheduleDate")
    @Expose
    private Integer reschduledate;

    private int lid;
    private String network_status;
    private Integer localopportunityid;

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getLid() {
        return lid;
    }

    public void setLid(int lid) {
        this.lid = lid;
    }

    public String getOtheractions() {
        return otheractions;
    }

    public void setOtheractions(String otheractions) {
        this.otheractions = otheractions;
    }

    public String getNetwork_status() {
        return network_status;
    }

    public void setNetwork_status(String network_status) {
        this.network_status = network_status;
    }

    public String getActivityid() {
        return activityid;
    }

    public void setActivityid(String activityid) {
        this.activityid = activityid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getContactid() {
        return contactid;
    }

    public void setContactid(String contactid) {
        this.contactid = contactid;
    }

    public String getSupervisorname() {
        return supervisorname;
    }

    public void setSupervisorname(String supervisorname) {
        this.supervisorname = supervisorname;
    }

    public String getNotificationstatus() {
        return notificationstatus;
    }

    public void setNotificationstatus(String notificationstatus) {
        this.notificationstatus = notificationstatus;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getSupervisorid() {
        return supervisorid;
    }

    public void setSupervisorid(String supervisorid) {
        this.supervisorid = supervisorid;
    }

    public String getOpportunityid() {
        return opportunityid;
    }

    public void setOpportunityid(String opportunityid) {
        this.opportunityid = opportunityid;
    }

    public String getPlancreatedby() {
        return plancreatedby;
    }

    public void setPlancreatedby(String plancreatedby) {
        this.plancreatedby = plancreatedby;
    }

    public String getActivitytype() {
        return activitytype;
    }

    public void setActivitytype(String activitytype) {
        this.activitytype = activitytype;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Integer getPlanstatus() {
        return planstatus;
    }

    public void setPlanstatus(Integer planstatus) {
        this.planstatus = planstatus;
    }

    public Integer getCompletestatus() {
        return completionstatus;
    }

    public void setCompletestatus(Integer completestatus) {
        this.completionstatus = completestatus;
    }

    public Integer getScheduletime() {
        return scheduletime;
    }

    public void setScheduletime(Integer scheduletime) {
        this.scheduletime = scheduletime;
    }

    public Integer getCheckintime() {
        return checkintime;
    }

    public void setCheckintime(Integer checkintime) {
        this.checkintime = checkintime;
    }

    public Integer getLocalopportunityid() {
        return localopportunityid;
    }

    public void setLocalopportunityid(Integer localopportunityid) {
        this.localopportunityid = localopportunityid;
    }

    public Integer getCheckouttime() {
        return checkouttime;
    }

    public void setCheckouttime(Integer checkouttime) {
        this.checkouttime = checkouttime;
    }

    public String getCheckinlocation() {
        return checkinlocation;
    }

    public void setCheckinlocation(String checkinlocation) {
        this.checkinlocation = checkinlocation;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getJointvisit() {
        return jointvisit;
    }

    public void setJointvisit(Boolean jointvisit) {
        this.jointvisit = jointvisit;
    }

    public String getReshdreason() {
        return reshdreason;
    }

    public void setReshdreason(String reshdreason) {
        this.reshdreason = reshdreason;
    }
    public String getOtherfollowupactions() {
        return otherfollowupactions;
    }

    public void setOtherfollowupactions(String otherfollowupactions) {
        this.otherfollowupactions = otherfollowupactions;
    }


    public String getCheckoutlocation() {
        return checkoutlocation;
    }

    public void setCheckoutlocation(String checkoutlocation) {
        this.checkoutlocation = checkoutlocation;
    }

    public String getCheckinlat() {
        return checkinlat;
    }

    public void setCheckinlat(String checkinlat) {
        this.checkinlat = checkinlat;
    }

    public String getCheckinlong() {
        return checkinlong;
    }

    public void setCheckinlong(String checkinlong) {
        this.checkinlong = checkinlong;
    }

    public String getCheckoutlat() {
        return checkoutlat;
    }

    public void setCheckoutlat(String checkoutlat) {
        this.checkoutlat = checkoutlat;
    }

    public String getCheckoutlong() {
        return checkoutlong;
    }

    public void setCheckoutlong(String checkoutlong) {
        this.checkoutlong = checkoutlong;
    }

    public Integer getDevicetimestamp() {
        return devicetimestamp;
    }

    public void setDevicetimestamp(Integer devicetimestamp) {
        this.devicetimestamp = devicetimestamp;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public Integer getFollowupdate() {
        return followupdate;
    }

    public void setFollowupdate(Integer followupdate) {
        this.followupdate = followupdate;
    }

    public Integer getReschduledate() {
        return reschduledate;
    }

    public void setReschduledate(Integer reschduledate) {
        this.reschduledate = reschduledate;
    }

    public String getFollowupaction() {
        return followupaction;
    }

    public void setFollowupaction(String followupaction) {
        this.followupaction = followupaction;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(String modifieddate) {
        this.modifieddate = modifieddate;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getProductspromoted() {
        return productspromoted;
    }

    public void setProductspromoted(String productspromoted) {
        this.productspromoted = productspromoted;
    }

    public Integer getScheduleday() {
        return scheduleday;
    }

    public void setScheduleday(Integer scheduleday) {
        this.scheduleday = scheduleday;
    }

    public Integer getSchedulemonth() {
        return schedulemonth;
    }

    public void setSchedulemonth(Integer schedulemonth) {
        this.schedulemonth = schedulemonth;
    }

    public Integer getScheduleyear() {
        return scheduleyear;
    }

    public void setScheduleyear(Integer scheduleyear) {
        this.scheduleyear = scheduleyear;
    }

    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

}
