package com.outwork.sudlife.psf.lead.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.gson.Gson;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.db.LeadDao;
import com.outwork.sudlife.psf.lead.db.ProposalCodesDao;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.lead.model.ProposalCodesModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.localbase.ProductDataMgr;
import com.outwork.sudlife.psf.localbase.ProductMasterDto;
import com.outwork.sudlife.psf.localbase.StaticDataDto;
import com.outwork.sudlife.psf.localbase.StaticDataMgr;
import com.outwork.sudlife.psf.opportunity.OpportunityMgr;
import com.outwork.sudlife.psf.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class UpdateLeadActivity extends BaseActivity {
    private BottomSheetDialog bottomSheetDialog;
    private ListView lvBottom;
    private TextView tvfName, tvlName, isnri,tv_nri,tvContactNumber, tv_city, tv_pincode, tv_bank, tv_branch_code, tv_branch_name, tv_address, toolbar_title,tv_countryname,tv_countrycode,countryname,countrycode;
    private TextInputLayout tiLayoutothers, tiLayoutSubStatus;
    private EditText etLeadStage, etEmailId, etAlternativeContactNo, etAge, etExistingCustomer, etPremiumPaid, etProductName, etProductone, etProducttwo, etProductNameOther,
            etOccupation, et_campaigndate, etSourceOfLead, etOther, etNoOfFamilyNumbers, etZccSupportRequried, etPreferredLang,
            etPreferredDate, etPreferredTime, etNotes, etStatus, etEducation, etSubStatus, etIncome, etPremiumExpected, etNextFollowupDate, etConversionPropensity,
            etProposalNumber, etLeadCreatedDate, etMarriedStatus, etFirstAppointmentDate, etActualPremium;
    private AlertDialog.Builder alertDialog;
    private List<String> myList;
    private String pvisitdate, fappointmentDate, nappointmentDate;
    private ArrayList<String> stringArrayList;
    private List<String> stagesList = new ArrayList<>();
    private LeadModel leadModel = new LeadModel();
    private List<ProposalCodesModel> proposalCodesModelList = new ArrayList<>();
    private PlannerModel plannerModel = new PlannerModel();
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                etNextFollowupDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etNextFollowupDate.setText(visitdt);
                etNextFollowupDate.setEnabled(true);
                etNextFollowupDate.setError(null);
                nappointmentDate = TimeUtils.convertDatetoUnix(visitdt);
            }
            if (customSelector == 1) {
                etFirstAppointmentDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etFirstAppointmentDate.setText(visitdt);
                etFirstAppointmentDate.setEnabled(true);
                etFirstAppointmentDate.setError(null);
                fappointmentDate = TimeUtils.convertDatetoUnix(visitdt);
            }
            if (customSelector == 3) {
                etPreferredDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etPreferredDate.setText(visitdt);
                etPreferredDate.setEnabled(true);
                pvisitdate = TimeUtils.convertDatetoUnix(visitdt);
                etPreferredDate.setError(null);
            }
        }

        @Override
        public void onDateTimeCancel() {
            etNextFollowupDate.setEnabled(true);
            etFirstAppointmentDate.setEnabled(true);
            etPreferredDate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_lead);

        mgr = LocalBroadcastManager.getInstance(this);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("leadobj")) {
            leadModel = new Gson().fromJson(getIntent().getStringExtra("leadobj"), LeadModel.class);
        }
        if (SharedPreferenceManager.getInstance().getString(Constants.STAGES_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                OpportunityIntentService.insertOpportunityStages(UpdateLeadActivity.this);
            }
        }
        if (SharedPreferenceManager.getInstance().getString(Constants.PROPOSAL_CODES_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                LeadIntentService.insertProposalCodes(UpdateLeadActivity.this);
            }
        }
        initilizeViews();
        initToolBar();
        populateData(leadModel);
        setListener();

        Utils.setTypefaces(IvokoApplication.robotoTypeface, etLeadStage, etEmailId, etAlternativeContactNo, etAge, etExistingCustomer, etPremiumPaid, etProductName, etProductone, etProductNameOther,
                etOccupation, et_campaigndate, etSourceOfLead, etOther, etNoOfFamilyNumbers, etZccSupportRequried, etPreferredLang,
                etPreferredDate, etPreferredTime, etNotes, etStatus, etEducation, etSubStatus, etIncome, etPremiumExpected, etNextFollowupDate, etConversionPropensity,
                etProposalNumber, etLeadCreatedDate, etProducttwo, etActualPremium, etMarriedStatus, etFirstAppointmentDate);
        Utils.setTypefaces(IvokoApplication.robotoLightTypeface, (TextView) findViewById(R.id.fname),
                (TextView) findViewById(R.id.lname), (TextView) findViewById(R.id.contact_number),
                (TextView) findViewById(R.id.address), (TextView) findViewById(R.id.city),
                (TextView) findViewById(R.id.pincode), (TextView) findViewById(R.id.bank),
                (TextView) findViewById(R.id.branch_code), (TextView) findViewById(R.id.branch_name));
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoMediumTypeface,
                (TextView) findViewById(R.id.personalinfo_lbl), (TextView) findViewById(R.id.pastinfo_lbl),
                (TextView) findViewById(R.id.otherinfo_lbl), (TextView) findViewById(R.id.bankinfo_lbl),
                (TextView) findViewById(R.id.zccinfo_lbl), (TextView) findViewById(R.id.updateinfo_lbl));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("pcodes_broadcast"));
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("oppstage_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    public void populateData(LeadModel leadModel) {
        if (Utils.isNotNullAndNotEmpty(leadModel.getFirstname())) {
            tvfName.setText(leadModel.getFirstname());
        } else {
            tvfName.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLastname())) {
            tvlName.setText(leadModel.getLastname());
        } else {
            tvlName.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getCampaigndate())) {
            et_campaigndate.setText(leadModel.getCampaigndate());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getIsnri())) {

            if(leadModel.getIsnri().equalsIgnoreCase("true")) {
                tv_nri.setText("YES");

            }
            else{
                tv_nri.setText("NO");
            }
        } else {
            tv_nri.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getContactno())) {
            tvContactNumber.setText(leadModel.getContactno());
        } else {
            tvContactNumber.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getCountrycode())) {
            tv_countrycode.setText(leadModel.getCountrycode());
        } else {
            tv_countrycode.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getCountryname())) {
            tv_countryname.setText(leadModel.getCountryname());
        } else {
            tv_countryname.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getEmail())) {
            etEmailId.setText(leadModel.getEmail());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getAlternatecontactno())) {
            etAlternativeContactNo.setText(leadModel.getAlternatecontactno());
        }
        if (leadModel.getAge() != null) {
            etAge.setText(String.valueOf(leadModel.getAge()));
        }
        if (leadModel.getIsexistingcustomer() != null) {
            if (leadModel.getIsexistingcustomer()) {
                etExistingCustomer.setText("Yes");
                findViewById(R.id.et_premium_paid_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_product_name_lbl).setVisibility(View.VISIBLE);
            } else {
                etExistingCustomer.setText("No");
                findViewById(R.id.et_premium_paid_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_name_lbl).setVisibility(View.GONE);
            }
        }
        if (leadModel.getPremiumpaid() != null) {
            etPremiumPaid.setText(String.valueOf(leadModel.getPremiumpaid()));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProductname())) {
            etProductName.setText(leadModel.getProductname());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProduct1())) {
            etProductone.setText(leadModel.getProduct1());
        }
//        if (Utils.isNotNullAndNotEmpty(leadModel.getProduct2())) {
//            etProducttwo.setText(leadModel.getProduct2());
//        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getAddress())) {
            tv_address.setText(leadModel.getAddress());
        } else {
            tv_address.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getCity())) {
            tv_city.setText(leadModel.getCity());
        } else {
            tv_city.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getPincode())) {
            tv_pincode.setText(leadModel.getPincode());
        } else {
            tv_pincode.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getOccupation())) {
            etOccupation.setText(leadModel.getOccupation());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadsource())) {
            etSourceOfLead.setText(leadModel.getLeadsource());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getOtherleadsource())) {
            etOther.setText(leadModel.getOtherleadsource());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getBank())) {
            tv_bank.setText(leadModel.getBank());
        } else {
            tv_bank.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getBankbranchcode())) {
            tv_branch_code.setText(leadModel.getBankbranchcode());
        } else {
            tv_branch_code.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getFamilymemberscount() != null) {
            etNoOfFamilyNumbers.setText(String.valueOf(leadModel.getFamilymemberscount()));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getBankbranchname())) {
            tv_branch_name.setText(leadModel.getBankbranchname());
        } else {
            tv_branch_name.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getZccsupportrequired() != null) {
            if (leadModel.getZccsupportrequired()) {
                etZccSupportRequried.setText("Yes");
                findViewById(R.id.et_preferred_language_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_preferred_Date_lbl).setVisibility(View.VISIBLE);
            } else {
                etZccSupportRequried.setText("No");
                findViewById(R.id.et_preferred_language_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_preferred_Date_lbl).setVisibility(View.GONE);
            }
        }
        if (leadModel.getIsmarried() != null) {
            if (leadModel.getIsmarried()) {
                etMarriedStatus.setText("Yes");
            } else {
                etMarriedStatus.setText("No");
            }
        }
        if (leadModel.getPreferreddatetime() != null) {
            if (Utils.isNotNullAndNotEmpty(String.valueOf(leadModel.getPreferreddatetime()))) {
                etPreferredDate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getPreferreddatetime()), "dd/MM/yyyy hh:mm a"));
            }
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getPreferredlanguage())) {
            etPreferredLang.setText(leadModel.getPreferredlanguage());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getStatus())) {
            etStatus.setText(leadModel.getStatus());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getSubstatus())) {
            etSubStatus.setText(leadModel.getSubstatus());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getExpectedpremium())) {
            etPremiumExpected.setText(leadModel.getExpectedpremium());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getConversionpropensity())) {
            etConversionPropensity.setText(leadModel.getConversionpropensity());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProposalnumber())) {
            etProposalNumber.setText(String.valueOf(leadModel.getProposalnumber()));
        }
        if (leadModel.getLeadcreatedon() != null) {
            if (leadModel.getLeadcreatedon() != 0) {
                etLeadCreatedDate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getLeadcreatedon()), "dd/MM/yyyy hh:mm a"));
            }
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getEducation())) {
            etEducation.setText(leadModel.getEducation());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getIncomeband())) {
            etIncome.setText(leadModel.getIncomeband());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getRemarks())) {
            etNotes.setText(leadModel.getRemarks());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadstage())) {
            if (leadModel.getLeadstage().equalsIgnoreCase("Open")) {
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
//                    findViewById(R.id.et_product_two).setVisibility(View.GONE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
            } else if (leadModel.getLeadstage().equalsIgnoreCase("Contacted & meeting fixed")) {
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
//                    findViewById(R.id.et_product_two).setVisibility(View.GONE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
            } else if (leadModel.getLeadstage().equalsIgnoreCase("Proposition presented")) {
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
            } else if (leadModel.getLeadstage().equalsIgnoreCase("Not interested")) {
                findViewById(R.id.et_notes_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
            } else if (leadModel.getLeadstage().equalsIgnoreCase("Converted")) {
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.VISIBLE);
            }
            etLeadStage.setText(leadModel.getLeadstage());
        }
    }

    private void setListener() {
        etExistingCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etExistingCustomer.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Yes");
                stringArrayList.add("No");
                bottomSheetDialogforExistingCust(etExistingCustomer, stringArrayList, "Are You Existing SUD Life customer?");
            }
        });
        etMarriedStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Yes");
                stringArrayList.add("No");
                bottomSheetDialog(etMarriedStatus, stringArrayList, "Married (Yes/No)?");

            }
        });
        etSourceOfLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSourceOfLead.setFocusableInTouchMode(false);
                List<StaticDataDto> staticList = new StaticDataMgr(UpdateLeadActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "leadsource");
                if (staticList.size() != 0) {
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i < staticList.size(); i++) {
                        strings.add(staticList.get(i).getName());
                    }
                    bottomSheetDialog(etSourceOfLead, strings, "Select Source Of Lead");
                }
            }
        });
        etNoOfFamilyNumbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringArrayList = new ArrayList<>();
                for (int i = 1; i <= 10; i++) {
                    stringArrayList.add(String.valueOf(i));
                }
                bottomSheetDialog(etNoOfFamilyNumbers, stringArrayList, "Select No Of Family Members");
            }
        });
        etZccSupportRequried.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etZccSupportRequried.setFocusableInTouchMode(true);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Yes");
                stringArrayList.add("No");
                bottomSheetDialogforExistingCust(etZccSupportRequried, stringArrayList, "ZCC Suport Required");
            }
        });
        etPreferredLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPreferredLang.setFocusableInTouchMode(false);
                List<StaticDataDto> staticList = new StaticDataMgr(UpdateLeadActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "preferredlanguage");
                if (staticList.size() != 0) {
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i < staticList.size(); i++) {
                        strings.add(staticList.get(i).getName());
                    }
                    bottomSheetDialog(etPreferredLang, strings, "Select Preferred Langugae");
                }

            }
        });
        etIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<StaticDataDto> staticList = new StaticDataMgr(UpdateLeadActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "incomeband");
                if (staticList.size() != 0) {
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i < staticList.size(); i++) {
                        strings.add(staticList.get(i).getName());
                    }
                    bottomSheetDialog(etIncome, strings, "Select Income Band");
                }
            }
        });
        etNotes.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_notes) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        etNextFollowupDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNextFollowupDate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 0);
                Date minDate = calendar.getTime();
                calendar.add(Calendar.DAY_OF_YEAR, 30);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(minDate)
                        .setMinDate(minDate)
                        .setMaxDate(backDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
        etFirstAppointmentDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFirstAppointmentDate.setEnabled(false);
                etFirstAppointmentDate.setFocusableInTouchMode(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 30);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });
        etPreferredDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPreferredDate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 30);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(3)
                        .build()
                        .show();
            }
        });
        etLeadStage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etLeadStage.setFocusableInTouchMode(false);
                stagesList = OpportunityMgr.getInstance(UpdateLeadActivity.this).getOpportunitystagesnamesList(userid);
                if (stagesList.size() != 0) {
                    bottomSheetDialogForSubStatus(((EditText) findViewById(R.id.et_lead_status)), stagesList, "Select Lead Stage");
                }
            }
        });
        etConversionPropensity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringArrayList = new ArrayList<>();
                stringArrayList.add("RED");
                stringArrayList.add("AMBER");
                stringArrayList.add("GREEN");
                bottomSheetDialog(((EditText) findViewById(R.id.et_conversion_propensity)), stringArrayList, "Select Conversion propensity");

            }
        });
        etProductName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etProductName.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(UpdateLeadActivity.this).getProductList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(etProductName, stringArrayList, "Select Product");
                }
            }
        });
        etProductone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etProductone.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(UpdateLeadActivity.this).getProductList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(etProductone, stringArrayList, "Select Product");
                }
            }
        });
        etProducttwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etProducttwo.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(UpdateLeadActivity.this).getProductList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(etProducttwo, stringArrayList, "Select Product");
                }
            }
        });
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Update Lead");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    public void bottomSheetDialogforExistingCust(final EditText editText, ArrayList<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(UpdateLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(UpdateLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                if (editText.getId() == R.id.et_existing_sud_ife_customer) {
                    if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("No")) {
                        etPremiumPaid.setText("");
                        findViewById(R.id.et_premium_paid_lbl).setVisibility(View.GONE);
                        findViewById(R.id.et_product_name_lbl).setVisibility(View.GONE);
                        etPremiumPaid.setVisibility(View.GONE);
                        etProductName.setText("");
                        etProductName.setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.et_premium_paid_lbl).setVisibility(View.VISIBLE);
                        findViewById(R.id.et_product_name_lbl).setVisibility(View.VISIBLE);
                        etPremiumPaid.setVisibility(View.VISIBLE);
                        etProductName.setVisibility(View.VISIBLE);
                    }
                } else if (editText.getId() == R.id.et_zcc_support_requried) {
                    if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("No")) {
                        etPreferredLang.setText("");
                        findViewById(R.id.et_preferred_language_lbl).setVisibility(View.GONE);
                        findViewById(R.id.et_preferred_Date_lbl).setVisibility(View.GONE);
                        etPreferredLang.setVisibility(View.GONE);
                        etPreferredDate.setText("");
                        etPreferredDate.setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.et_preferred_language_lbl).setVisibility(View.VISIBLE);
                        findViewById(R.id.et_preferred_Date_lbl).setVisibility(View.VISIBLE);
                        etPreferredLang.setVisibility(View.VISIBLE);
                        etPreferredDate.setVisibility(View.VISIBLE);
                    }
                }
                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    public void bottomSheetDialog(final EditText editText, ArrayList<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(UpdateLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(UpdateLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Others")) {
                    tiLayoutothers.setVisibility(View.VISIBLE);
                } else {
                    tiLayoutothers.setVisibility(View.GONE);
                }
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Walk-in")) {
                    showpopup("Visit");
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Lead Generation Activity")) {
                    showpopup("Activity");
                } else {
                    et_campaigndate.setText("");
                }
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Met")) {
//                    tiLayoutSubStatus.setVisibility(View.VISIBLE);
                    ((ScrollView) findViewById(R.id.scroll_view)).fullScroll(ScrollView.FOCUS_DOWN);
                } else {
                    tiLayoutSubStatus.setVisibility(View.GONE);
                }
                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    public void initilizeViews() {
        tiLayoutothers = (TextInputLayout) findViewById(R.id.tilayout_other);
        tiLayoutSubStatus = (TextInputLayout) findViewById(R.id.til_sub_status);
        tvfName = (TextView) findViewById(R.id.tv_fname);
        tvlName = (TextView) findViewById(R.id.tv_lname);
        isnri = (TextView) findViewById(R.id.isnri);
        tv_nri = (TextView) findViewById(R.id.tv_nri);
        tv_countrycode = (TextView) findViewById(R.id.tv_countrycode);
        tv_countryname = (TextView) findViewById(R.id.tv_countryname);
        tvContactNumber = (TextView) findViewById(R.id.tv_contact_number);
        etEmailId = (EditText) findViewById(R.id.et_email_id);
        etAlternativeContactNo = (EditText) findViewById(R.id.et_alt_contactno);
        etAge = (EditText) findViewById(R.id.et_age);
        etExistingCustomer = (EditText) findViewById(R.id.et_existing_sud_ife_customer);
        etMarriedStatus = (EditText) findViewById(R.id.et_married_status);
        etPremiumPaid = (EditText) findViewById(R.id.et_premium_paid);
        etProductName = (EditText) findViewById(R.id.et_product_name);
        etProductone = (EditText) findViewById(R.id.et_product_one);
        etProducttwo = (EditText) findViewById(R.id.et_product_two);
        etActualPremium = (EditText) findViewById(R.id.et_actpremium);
        etProductNameOther = (EditText) findViewById(R.id.et_other_product_name);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_city = (TextView) findViewById(R.id.tv_city);
        tv_pincode = (TextView) findViewById(R.id.tv_pincode);
        etOccupation = (EditText) findViewById(R.id.et_occupation);
        etSourceOfLead = (EditText) findViewById(R.id.et_source_of_lead);
        etOther = (EditText) findViewById(R.id.et_other);
        et_campaigndate = (EditText) findViewById(R.id.et_campaigndate);
        tv_bank = (TextView) findViewById(R.id.tv_bank);
        tv_branch_code = (TextView) findViewById(R.id.tv_branch_code);
        etNoOfFamilyNumbers = (EditText) findViewById(R.id.et_no_of_family_members);
        tv_branch_name = (TextView) findViewById(R.id.tv_branch_name);
        etZccSupportRequried = (EditText) findViewById(R.id.et_zcc_support_requried);
        etPreferredLang = (EditText) findViewById(R.id.et_preferred_language);
        etPreferredDate = (EditText) findViewById(R.id.et_preferred_Date);
        etPreferredTime = (EditText) findViewById(R.id.et_preferred_time);
        etLeadStage = (EditText) findViewById(R.id.et_lead_status);
        etStatus = (EditText) findViewById(R.id.et_metting_status);
        etSubStatus = (EditText) findViewById(R.id.et_sub_status);
        etPremiumExpected = (EditText) findViewById(R.id.et_expected_actpremium);
        etNextFollowupDate = (EditText) findViewById(R.id.et_next_followup_date);
        etFirstAppointmentDate = (EditText) findViewById(R.id.et_first_appointment_date);
        etConversionPropensity = (EditText) findViewById(R.id.et_conversion_propensity);
        etProposalNumber = (EditText) findViewById(R.id.et_proposal_no);
        etLeadCreatedDate = (EditText) findViewById(R.id.et_lead_created_date);
        etEducation = (EditText) findViewById(R.id.et_education);
        etIncome = (EditText) findViewById(R.id.et_income);
        etNotes = (EditText) findViewById(R.id.et_notes);





        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                stagesList = OpportunityMgr.getInstance(UpdateLeadActivity.this).getOpportunitystagesnamesList(userid);
                proposalCodesModelList = ProposalCodesDao.getInstance(UpdateLeadActivity.this).getProposalCodesList(groupId);
            }
        };
    }

    public void showpopup(String activitytype) {
        myList = new ArrayList<String>();
        myList = PlannerMgr.getInstance(UpdateLeadActivity.this).getPlannerDatesListByBranches(userid, leadModel.getBankbranchname(), activitytype, Integer.parseInt(TimeUtils.getCurrentDate("MM")), Integer.parseInt(TimeUtils.getCurrentDate("yyyy")));
        myList.add(0, TimeUtils.getCurrentDate("dd/MM/yyyy"));
        alertDialog = new AlertDialog.Builder(UpdateLeadActivity.this);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(UpdateLeadActivity.this);
//        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.layout_popup, null);

        alertDialog.setView(convertView);
        ListView lv = (ListView) convertView.findViewById(R.id.list);
        final AlertDialog alert = alertDialog.create();
//        alert.setTitle(" Select Visit Date:");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, myList);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
                et_campaigndate.setText(arg0.getItemAtPosition(position).toString());
                alert.cancel();
            }
        });
        alert.show();
    }

    public void bottomSheetDialogForSubStatus(final EditText editText, List<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(UpdateLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(UpdateLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Open")) {
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Contacted & meeting fixed")) {
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Proposition presented")) {
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Not interested")) {
                    findViewById(R.id.et_notes_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Converted")) {
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                }
                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }


    public void updateLead() {
//        If status is Met, Followup Date and Expected Premium is Mandatory
        // If Leadstatus(stage) is converted, Proposal number is mandatory
        //If zcc support is required, Language is mandatory
        if (etAlternativeContactNo.getText().toString().length() != 0 && etAlternativeContactNo.getText().toString().length() < 10) {
            etAlternativeContactNo.setError("Please Enter 10 Digits Contact Number");
            etAlternativeContactNo.requestFocus();
        } else if (etAlternativeContactNo.getText().toString().matches("(\\d)\\1{9}")) {
            etAlternativeContactNo.setError("Please Enter Valid Contact Number");
            etAlternativeContactNo.requestFocus();
        } else if (etEmailId.getText().toString().length() != 0 && !Utils.isValidEmail(etEmailId.getText().toString().trim())) {
            etEmailId.setError("Pleae Enter Valid Email");
            etEmailId.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etAge.getText().toString())) {
            etAge.setError("Age is mandatory");
            etAge.requestFocus();
        } else if (Integer.parseInt(etAge.getText().toString()) > 120) {
            etAge.setError("Age should be below 120");
            etAge.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etExistingCustomer.getText().toString())) {
            etExistingCustomer.setFocusableInTouchMode(true);
            etExistingCustomer.setError("Please Select Existing or Not");
            etExistingCustomer.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etSourceOfLead.getText().toString())) {
            etSourceOfLead.setFocusableInTouchMode(true);
            etSourceOfLead.setError("Source of Lead is mandatory");
            etSourceOfLead.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etLeadStage.getText().toString())) {
            etLeadStage.setFocusableInTouchMode(true);
            etLeadStage.setError("Please Select Lead Stage");
            etLeadStage.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Converted") && !Utils.isNotNullAndNotEmpty(etProducttwo.getText().toString())) {
            etProducttwo.setFocusableInTouchMode(true);
            etProducttwo.setError("Please Select a Product");
            etProducttwo.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Converted") && !Utils.isNotNullAndNotEmpty(etActualPremium.getText().toString())) {
            etActualPremium.setError("Please Enter Actual Premium Paid");
            etActualPremium.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Converted") && !Utils.isNotNullAndNotEmpty(etProposalNumber.getText().toString())) {
            etProposalNumber.setError("Please Enter Proposal Number");
            etProposalNumber.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Converted") && LeadDao.getInstance(UpdateLeadActivity.this).isProposalNoExist(etProposalNumber.getText().toString(), userid)) {
            etProposalNumber.setError("Proposal Number already exists");
            etProposalNumber.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Not interested") && !Utils.isNotNullAndNotEmpty(etNotes.getText().toString())) {
            etNotes.setError("Please Specify Reason");
            etNotes.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Contacted & meeting fixed") && !Utils.isNotNullAndNotEmpty(etNextFollowupDate.getText().toString())) {
            etNextFollowupDate.setFocusableInTouchMode(true);
            etNextFollowupDate.setError("Please Select Appointment Date");
            etNextFollowupDate.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(etNextFollowupDate.getText().toString())) {
            etNextFollowupDate.setFocusableInTouchMode(true);
            etNextFollowupDate.setError("Please Select Appointment Date");
            etNextFollowupDate.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(etPremiumExpected.getText().toString())) {
            etPremiumExpected.setError("Please Specify Expected Premium");
            etPremiumExpected.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Proposition presented") && (etPremiumExpected.getText().toString().equalsIgnoreCase("0") || etPremiumExpected.getText().toString().length() > 7)) {
            etPremiumExpected.setError("Please Enter Valid Expected Premium");
            etPremiumExpected.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(etProductone.getText().toString())) {
            etProductone.setFocusableInTouchMode(true);
            etProductone.setError("Please Select a Product");
            etProductone.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(etConversionPropensity.getText().toString())) {
            etConversionPropensity.setError("Please Select Convertion Propensity");
            etConversionPropensity.requestFocus();
        } else {
            if (etLeadStage.getText().toString().equalsIgnoreCase("Converted") && Utils.isNotNullAndNotEmpty(etProposalNumber.getText().toString())) {
                proposalCodesModelList = ProposalCodesDao.getInstance(UpdateLeadActivity.this).getProposalCodesList(groupId);
                for (int i = 0; i < proposalCodesModelList.size(); i++) {
                    long pno = Long.parseLong(etProposalNumber.getText().toString());
                    if (pno >= proposalCodesModelList.get(i).getMinvalue() && pno <= proposalCodesModelList.get(i).getMaxvalue()) {
                        etProposalNumber.setError(null);
                        updateLeaddata();
                        break;
                    } else {
                        etProposalNumber.setError("Please Enter Valid Proposal Number");
                        etProposalNumber.requestFocus();
                    }
                }
            } else {
                updateLeaddata();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.order_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            updateLead();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateLeaddata() {
        leadModel.setUserid(userid);
        leadModel.setGroupid(groupId);
        leadModel.setEmail(etEmailId.getText().toString());
        leadModel.setAlternatecontactno(etAlternativeContactNo.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etAge.getText().toString()))
            leadModel.setAge(Integer.parseInt(etAge.getText().toString()));
        leadModel.setEducation(etEducation.getText().toString());
        leadModel.setOccupation(etOccupation.getText().toString());
        leadModel.setIncomeband(etIncome.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etNoOfFamilyNumbers.getText().toString()))
            leadModel.setFamilymemberscount(Integer.parseInt(etNoOfFamilyNumbers.getText().toString()));
        if (Utils.isNotNullAndNotEmpty(etMarriedStatus.getText().toString()))
            if (etMarriedStatus.getText().toString().equalsIgnoreCase("yes")) {
                leadModel.setIsmarried(true);
            } else {
                leadModel.setIsmarried(false);
            }
        if (Utils.isNotNullAndNotEmpty(etNotes.getText().toString()))
            leadModel.setRemarks(etNotes.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etExistingCustomer.getText().toString()))
            if (etExistingCustomer.getText().toString().equalsIgnoreCase("yes")) {
                leadModel.setIsexistingcustomer(true);
            } else {
                leadModel.setIsexistingcustomer(false);
            }
        if (Utils.isNotNullAndNotEmpty(etPremiumPaid.getText().toString()))
            leadModel.setPremiumpaid(Integer.parseInt(etPremiumPaid.getText().toString()));
        leadModel.setProductname(etProductName.getText().toString());
        leadModel.setLeadsource(etSourceOfLead.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etOther.getText().toString()))
            leadModel.setOtherleadsource(etOther.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etZccSupportRequried.getText().toString()))
            if (etZccSupportRequried.getText().toString().equalsIgnoreCase("yes")) {
                leadModel.setZccsupportrequired(true);
            } else {
                leadModel.setZccsupportrequired(false);
            }
        leadModel.setPreferredlanguage(etPreferredLang.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etPreferredDate.getText().toString()))
            leadModel.setPreferreddatetime(Integer.parseInt(TimeUtils.convertDatetoUnix(etPreferredDate.getText().toString())));
        leadModel.setStatus(etStatus.getText().toString());
        leadModel.setLeadstage(etLeadStage.getText().toString());
        leadModel.setProduct1(etProductone.getText().toString());
        leadModel.setProduct2(etProducttwo.getText().toString());
        leadModel.setSubstatus(etSubStatus.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etPremiumExpected.getText().toString()))
            leadModel.setExpectedpremium(etPremiumExpected.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etActualPremium.getText().toString()))
            leadModel.setActualpremiumpaid(Integer.parseInt(etActualPremium.getText().toString()));
        leadModel.setConversionpropensity(etConversionPropensity.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etProposalNumber.getText().toString()))
            leadModel.setProposalnumber(etProposalNumber.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etLeadStage.getText().toString()))
            if (etLeadStage.getText().toString().equalsIgnoreCase("Contacted & meeting fixed")) {
                if (leadModel.getFirstappointmentdate() != null) {
                    if (leadModel.getFirstappointmentdate() == 0)
                        leadModel.setFirstappointmentdate(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
                } else {
                    leadModel.setFirstappointmentdate(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
                }
            }
        if (Utils.isNotNullAndNotEmpty(etNextFollowupDate.getText().toString()))
            leadModel.setNextfollowupdate(Integer.parseInt(TimeUtils.convertDatetoUnix(etNextFollowupDate.getText().toString())));
        LeadMgr.getInstance(UpdateLeadActivity.this).updateLead(leadModel, "offline");
        if (Utils.isNotNullAndNotEmpty(etLeadStage.getText().toString()))
            if (etLeadStage.getText().toString().equalsIgnoreCase("Contacted & meeting fixed")) {
                postcreateplan(TimeUtils.convertDatetoUnix(etNextFollowupDate.getText().toString()));
            } else if (etLeadStage.getText().toString().equalsIgnoreCase("Proposition presented")) {
                postcreateplan(TimeUtils.convertDatetoUnix(etNextFollowupDate.getText().toString()));
            }
        LeadIntentService.syncLeadstoServer(UpdateLeadActivity.this);
        showAlert("", "Your Lead is updated successfully",
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);
    }

    private void postcreateplan(String sdate) {
        plannerModel.setGroupid(groupId);
        plannerModel.setUserid(userid);
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadid())) {
            plannerModel.setLeadid(leadModel.getLeadid());
            plannerModel.setLocalleadid(leadModel.getId());
        } else {
            plannerModel.setLocalleadid(leadModel.getId());
        }
        if (Utils.isNotNullAndNotEmpty(sdate)) {
            plannerModel.setScheduleday(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", TimeUtils.getFormattedDatefromUnix(sdate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setSchedulemonth(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", TimeUtils.getFormattedDatefromUnix(sdate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setScheduleyear(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", TimeUtils.getFormattedDatefromUnix(sdate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setScheduletime(Integer.parseInt(sdate));
        }
        plannerModel.setFirstname(leadModel.getFirstname());
        plannerModel.setLastname(leadModel.getLastname());
        plannerModel.setPlanstatus(1);
        plannerModel.setCompletestatus(0);
        plannerModel.setType("Appointment");
        plannerModel.setActivitytype("Appointment");
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadstage()))
            plannerModel.setSubtype(leadModel.getLeadstage());
        plannerModel.setNetwork_status("offline");
        PlannerMgr.getInstance(UpdateLeadActivity.this).insertPlanner(plannerModel, leadModel.getUserid(), leadModel.getGroupid());
    }
}
