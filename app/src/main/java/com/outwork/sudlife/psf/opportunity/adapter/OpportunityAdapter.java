package com.outwork.sudlife.psf.opportunity.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.opportunity.OpportunityMgr;
import com.outwork.sudlife.psf.opportunity.activities.ViewOpportunityActivity;
import com.outwork.sudlife.psf.opportunity.models.OpportunityModel;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

/**
 * Created by Panch on 11/23/2015.
 */
public class OpportunityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private Context context;
    private List<OpportunityModel> opportunityModelList = new ArrayList<>();
    private List<OpportunityModel> opportunityModelArrayList = new ArrayList<>();
    private OpportunityFilter opportunityFilter;

    public static class OpportunityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView fCustomerName, stage, fVisitDate, cDescription, subject;
        public ImageView fCircle;
        private RelativeLayout row;

        public OpportunityViewHolder(View v) {
            super(v);
            fCircle = (ImageView) v.findViewById(R.id.profileImage);
            fCustomerName = (TextView) v.findViewById(R.id.ffCustomerName);
            fVisitDate = (TextView) v.findViewById(R.id.ffVisitDate);
            cDescription = (TextView) v.findViewById(R.id.ffCallType);
            stage = (TextView) v.findViewById(R.id.stage);
            row = (RelativeLayout) v.findViewById(R.id.row);
        }

        @Override
        public void onClick(View v) {

        }
    }

    class FeedExtraViewHolder extends OpportunityViewHolder {
        public FeedExtraViewHolder(View itemView) {
            super(itemView);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public OpportunityAdapter(Context mContext, List<OpportunityModel> listfilledforms) {
        this.context = mContext;
        this.opportunityModelList = listfilledforms;
        this.opportunityModelArrayList = listfilledforms;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = inflater.inflate(R.layout.list_opprt_row, parent, false);
        viewHolder = new FeedExtraViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        OpportunityModel dto = (OpportunityModel) this.opportunityModelList.get(position);
        OpportunityViewHolder viewHolder = (OpportunityViewHolder) holder;
        configureOrdinaryViewHolder(viewHolder, dto);
    }


    @Override
    public int getItemCount() {
        if (opportunityModelList != null) {
            return opportunityModelList.size();
        }
        return 0;
    }

    public void setItems(List<OpportunityModel> formList) {
        if (this.opportunityModelList.size() > 0) {
            this.opportunityModelList.clear();
        }
        this.opportunityModelList = formList;
    }

    private void configureOrdinaryViewHolder(OpportunityViewHolder holder, final OpportunityModel dto) {
        if (!TextUtils.isEmpty(dto.getClosingdate())) {
            String formattedDay = TimeUtils.getFormattedDatefromUnix(dto.getClosingdate(), "MMM dd yyyy");
            holder.fVisitDate.setText(formattedDay);
        }
        if (Utils.isNotNullAndNotEmpty(dto.getNetwork_status())) {
//            if (dto.getNetwork_status().equalsIgnoreCase("offline")) {
//                holder.fVisitDate.setTextColor(ContextCompat.getColor(context, R.color.material_red_300));
//                holder.fCustomerName.setTextColor(ContextCompat.getColor(context, R.color.material_red_300));
//                holder.cDescription.setTextColor(ContextCompat.getColor(context, R.color.material_red_300));
//            } else {
                holder.fVisitDate.setTextColor(ContextCompat.getColor(context, R.color.material_grey_700));
                holder.fCustomerName.setTextColor(Color.parseColor("#212121"));
                holder.cDescription.setTextColor(Color.parseColor("#757575"));
//            }
        } else {
            holder.fVisitDate.setTextColor(ContextCompat.getColor(context, R.color.material_grey_700));
            holder.fCustomerName.setTextColor(Color.parseColor("#212121"));
            holder.cDescription.setTextColor(Color.parseColor("#757575"));
        }

        StringBuilder stringBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(dto.getFirstname())) {
            stringBuilder.append(dto.getFirstname());
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLastname())) {
            stringBuilder.append(" ");
            stringBuilder.append(dto.getLastname());
        }
        holder.fCustomerName.setText(stringBuilder);

//        StringBuilder stringBuilder1 = new StringBuilder();
//        if (Utils.isNotNullAndNotEmpty(dto.getOccupation())) {
//            stringBuilder1.append(dto.getAge());
//        }
//        if (dto.getAge() != null) {
//            if (Utils.isNotNullAndNotEmpty(dto.getOccupation())) {
//                stringBuilder1.append(", ");
//                stringBuilder1.append(dto.getAge());
//            } else {
//                stringBuilder1.append(dto.getAge());
//            }
//        }
        holder.cDescription.setVisibility(View.GONE);
        if (Utils.isNotNullAndNotEmpty(dto.getStage())) {
            holder.stage.setVisibility(View.VISIBLE);
            holder.stage.setText(dto.getStage());
            String colorcode = OpportunityMgr.getInstance(context).getOpportunitystagescolor(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), dto.getStage());
            if (Utils.isNotNullAndNotEmpty(colorcode)) {
                holder.stage.setTextColor(Color.parseColor("#" + colorcode));
            }
        } else {
            holder.stage.setVisibility(View.GONE);
        }
        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ViewOpportunityActivity.class);
                intent.putExtra("FORMDTO", dto);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public Filter getFilter() {
        if (opportunityFilter == null)
            opportunityFilter = new OpportunityFilter();
        return opportunityFilter;
    }

    private class OpportunityFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0 || TextUtils.isEmpty(constraint)) {
                results.values = opportunityModelArrayList;
                results.count = opportunityModelArrayList.size();
            } else {
                List<OpportunityModel> fileredCustList = new OpportunityMgr(context)
                        .getOpportunityListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                results.values = fileredCustList;
                results.count = fileredCustList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null) {
                opportunityModelList = (ArrayList<OpportunityModel>) results.values;
                notifyDataSetChanged();
            } else {
                opportunityModelList = opportunityModelArrayList;
                notifyDataSetChanged();
            }
        }
    }
}