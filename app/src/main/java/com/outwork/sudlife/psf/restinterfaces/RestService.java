package com.outwork.sudlife.psf.restinterfaces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Panch on 3/20/2016.
 */
public class RestService {

    //public static final String API_BASE_URL = "http://sudlifeservices.outwork.in:7000/";//local
   public static final String API_BASE_URL = "https://psfapi.outwork.in:443/";//psf production

    private static OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .build();
    private static Gson gson = new GsonBuilder().setLenient().create();

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client);

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }


    //public static final String API_BASE_URL_v1 = "http://sudlifeservices.outwork.in:7000/";//v1---//local
   public static final String API_BASE_URL_v1 = "https://psfapi.outwork.in:443/";//v1---//psf production

    private static Retrofit.Builder builderv1 =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL_v1)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client);

    public static <S> S createServicev1(Class<S> serviceClass) {
        Retrofit retrofit = builderv1.client(client).build();
        return retrofit.create(serviceClass);
    }
}
