package com.outwork.sudlife.psf.more;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.more.models.PollAnswer;
import com.outwork.sudlife.psf.more.models.PollModel;
import com.outwork.sudlife.psf.more.service.PollService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewPollActivity extends BaseActivity {

    private LinearLayout answerLayout;
    private ArrayList<PollAnswer> pollAnswers;
    private PollAnswer answer;
    private int n = 10000;
    private TextView submit, polldate, question;
    private String pollQuestion, pollId, pollDate, answerOption;
    private String userToken;
    public boolean isPolled = false;
    public ImageView cancel;
    private LinearLayout submitLayout;
    private PollModel dto = new PollModel();

    private float highest;

    private static int previously_selected_layout = 0;

    private TextView pollAnswerText;
    private ImageView pollCheck;
    private CheckBox pollCheckBox;
    private TextView pollCount;
    private View answerView;
    private View pollpercentview;
    private String groupId;
    private PollModel pollDto;
    private boolean isBundle;
    private Bundle pollBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_poll);
        previously_selected_layout = 0;
        userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");

        answerLayout = (LinearLayout) findViewById(R.id.pollAnswerLayout);
        submit = (TextView) findViewById(R.id.submitPoll);
        submitLayout = (LinearLayout) findViewById(R.id.pollSubmit);
        polldate = (TextView) findViewById(R.id.pollDate);
        question = (TextView) findViewById(R.id.pollQuestion);
        initializeToolBar();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (previously_selected_layout > 0) {
                    submitPoll(pollId, String.valueOf(previously_selected_layout));
                }

            }
        });


        pollBundle = getIntent().getExtras();
        if (pollBundle.getBoolean("haspollinfo")) {
            initializeValues(pollBundle, pollDto, true);
        } else {
            pollId = pollBundle.getString("pollid", "nopollid");
            if (!pollId.equalsIgnoreCase("nopollid")) {
                getPoll(pollId);
            }
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_view_poll, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    private void initializeLayout() {
        if (!TextUtils.isEmpty(pollDate)) {
            String formattedDate = Utils.getFormattedDate(pollDate, true, 2);
            polldate.setText(formattedDate);
        } else {
            polldate.setText("Today");
        }
        question.setText(pollQuestion);
        // checkPolled(pollId);


        if (pollAnswers.size() > 0 && !isPolled) {
            //addSeperator();
        }
        for (int i = 0; i < pollAnswers.size(); i++) {
            answer = null;
            answer = pollAnswers.get(i);
            addAnswerView(answer, isPolled, true);
            //  if (!isPolled){
            //      addSeperator();
            //   }
            //  addSeperator();

        }

        if (isPolled) {
            submitLayout.setVisibility(View.INVISIBLE);
        } else {
            submit.setVisibility(View.VISIBLE);
        }

    }


    private void addAnswerView(PollAnswer pollAnswer, boolean polled, boolean progress) {

        if (polled) {
            LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            answerView = li.inflate(R.layout.list_item_pollanswers, answerLayout, false);

            pollAnswerText = (TextView) answerView.findViewById(R.id.pollanstext);
            pollAnswerText.setText(pollAnswer.getOptionText());
            answerView.setId(Integer.parseInt(pollAnswer.getOptionNr()));

            pollCount = (TextView) answerView.findViewById(R.id.votecount);
            pollCount.setText(String.valueOf(answer.getVoteCount()) + " votes");

            ProgressBar voteProgress = (ProgressBar) answerView.findViewById(R.id.votepercent);

            int votepercent = Math.round(pollAnswer.getOptionPercentage());
            voteProgress.setProgress(votepercent);
            if (pollAnswer.getOptionPercentage() == highest) {

            } else if (votepercent > 0) {
                // voteProgress.setProgressTintList(ColorStateList.valueOf(Color.RED));
                final LayerDrawable layers = (LayerDrawable) voteProgress.getProgressDrawable();
                layers.getDrawable(1).setColorFilter(Color.MAGENTA, PorterDuff.Mode.SRC_IN);
                voteProgress.setProgressDrawable(layers);
                // voteProgress.getProgressDrawable().setColorFilter(
                //       Color.MAGENTA, android.graphics.PorterDuff.Mode.SRC_IN);
            }

            TextView pollpercentage = (TextView) answerView.findViewById(R.id.pollpercent);
            pollpercentage.setText(votepercent + "%");

            n = n + 1;


        } else {
            LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            answerView = li.inflate(R.layout.list_item_pollanswer, answerLayout, false);

            pollAnswerText = (TextView) answerView.findViewById(R.id.pollanswer);
            //  pollCheck = (ImageView)answerView.findViewById(R.id.pollCheck);
            final CheckBox pollCheckBoxone = (CheckBox) answerView.findViewById(R.id.pollCheckBox);
            // pollCount = (TextView)answerView.findViewById(R.id.pollCount);

            pollAnswerText.setText(pollAnswer.getOptionText());
            answerView.setId(Integer.parseInt(pollAnswer.getOptionNr()));
            n = n + 1;

            answerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (previously_selected_layout > 0) {
                        View v1 = findViewById(previously_selected_layout);
                        TextView pollanswer = (TextView) v1.findViewById(R.id.pollanswer);
                        CheckBox polledCheckBox = (CheckBox) v1.findViewById(R.id.pollCheckBox);
                        pollanswer.setTextColor(Color.parseColor("#DE000000"));
                        v1.setBackgroundColor(Color.WHITE);
                        polledCheckBox.setChecked(false);


                    }
                    TextView pollanswer = (TextView) v.findViewById(R.id.pollanswer);
                    pollanswer.setTextColor(Color.WHITE);
                    pollCheckBoxone.setChecked(true);
                    //  v.setBackgroundColor(Color.parseColor("#6BBE98"));
                    v.setBackgroundColor(Color.parseColor("#4AA27A"));
                    previously_selected_layout = v.getId();
                }
            });
        }

        answerLayout.addView(answerView);
    }


    private void submitPoll(String pollId, String answerId) {
        PollService client = RestService.createService(PollService.class);
        Call<RestResponse> poll = client.poll(userToken, pollId, answerId);
        poll.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                Utils.displayToast(ViewPollActivity.this, "Your answer is submitted. Thank you.");
                Intent in = new Intent();
                setResult(RESULT_OK, in);
                finish();
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
            }
        });

    }


    private void getPoll(String pollId) {
        PollService client = RestService.createService(PollService.class);
        Call<RestResponse> poll = client.getPoll(userToken, pollId, groupId);
        poll.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    RestResponse jsonResponse = response.body();
                    try {
                        if (!TextUtils.isEmpty(jsonResponse.getData())) {
                            JSONObject pollObject = new JSONObject(jsonResponse.getData());
                            PollModel pollData = PollModel.parse(pollObject);
                            if (pollData != null) {
                                pollBundle = new Bundle();
                                initializeValues(pollBundle, pollData, false);

                            }

                        } else {
                            //  setNoPolls();

                        }

                    } catch (JSONException e) {
                        //setNoPolls();
                    }
                } else {
                    // setNoPolls();
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

            }
        });

    }

    private void initializeValues(Bundle b, PollModel polldto, boolean isBundle) {

        if (isBundle) {
            if (b != null) {
                pollQuestion = b.getString("pollquestion");
                pollId = b.getString("pollid");
                pollDate = b.getString("polldate");
                isPolled = b.getBoolean("polled");
                answerOption = b.getString("answer", "0");
                highest = b.getFloat("highest", 0);
                pollAnswers = b.getParcelableArrayList("pollanswers");
                initializeLayout();
            }


        } else {
            if (polldto != null) {
                pollQuestion = polldto.getPollQuestion();
                pollId = polldto.getPollId();
                pollDate = polldto.getPostedDate();
                isPolled = polldto.Isanswered();
                answerOption = polldto.getAnswerdOption();
                highest = polldto.getHighestOptionNr();
                pollAnswers = (ArrayList<PollAnswer>) polldto.getPollAnswers();
                initializeLayout();
            }
        }


    }

    private void initializeToolBar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.vPollToolBar);

        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

}
