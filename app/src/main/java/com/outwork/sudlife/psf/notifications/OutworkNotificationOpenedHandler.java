package com.outwork.sudlife.psf.notifications;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.outwork.sudlife.psf.planner.activities.CalenderActivity;
import com.outwork.sudlife.psf.ui.activities.SignInActvity;
import com.outwork.sudlife.psf.ui.base.SplashActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

/**
 * Created by Habi on 10-04-2017.
 */
public class OutworkNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
    private Context mContext;

    public OutworkNotificationOpenedHandler(Context context) {
        mContext = context;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        if (result.notification.payload != null) {
            if (Utils.isNotNullAndNotEmpty(result.notification.payload.body)) {
                if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""))) {
                  /*  Intent intent = new Intent(mContext, CalenderActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);*/

                    Intent intent = new Intent(mContext, SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }else {
                    Intent intent = new Intent(mContext, SignInActvity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
            }
        }

        if (actionType == OSNotificationAction.ActionType.ActionTaken)
            Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);
    }

    public static String getCalculatedDate(String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        int j = days;
        cal.add(Calendar.DAY_OF_YEAR, j);
        return s.format(new Date(cal.getTimeInMillis()));
    }
}