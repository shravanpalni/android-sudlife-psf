package com.outwork.sudlife.psf.lead.model;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.utilities.Utils;

/**
 * Created by apple on 11/7/16.
 */

public class ChildViewHolder extends RecyclerView.ViewHolder {

    public TextView tvName, tvCityName, tvconversionprop, tvLeadStatus, leadcode, status, allocate, reject, reallocate;
    public  CardView cardview;

    public ChildViewHolder(View itemView) {
        super(itemView);
        //name = (TextView) itemView.findViewById(R.id.lead_status);





        tvName = (TextView) itemView.findViewById(R.id.tv_branchname);
        tvCityName = (TextView) itemView.findViewById(R.id.tv_cityName);
        tvconversionprop = (TextView) itemView.findViewById(R.id.cnvsprop);
        tvLeadStatus = (TextView) itemView.findViewById(R.id.lead_status);
        leadcode = (TextView) itemView.findViewById(R.id.leadcode);
        status = (TextView) itemView.findViewById(R.id.status);
        allocate = (TextView) itemView.findViewById(R.id.allocate);
        reject = (TextView) itemView.findViewById(R.id.reject);
        reallocate = (TextView) itemView.findViewById(R.id.reallocate);
        cardview = (CardView) itemView.findViewById(R.id.cv_lead);
        Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, tvName);
        Utils.setTypefaces(IvokoApplication.robotoTypeface, tvCityName, tvconversionprop, tvLeadStatus, leadcode);




    }
}
