package com.outwork.sudlife.psf.lead.model;

/**
 * Created by shravanch on 13-12-2019.
 */

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "leadid",
        "userid",
        "groupid",
        "assignedtoid",
        "branchid",
        "branchname",
        "leadcode",
        "policyholderid",
        "policyholdersalutation",
        "policyholderfirstname",
        "policyholderlastname",
        "policyholderdob",
        "gender",
        "education",
        "occupation",
        "annualincome",
        "contactno",
        "landlineno",
        "emailid",
        "alternatemobileno",
        "alternatelandline1",
        "alternatelandline2",
        "nomineename",
        "nomineedob",
        "laname",
        "ladob",
        "nomineecontactno",
        "alternateemailid",
        "psfzone",
        "psfregion",
        "sudbranchoffice",
        "leadstage",
        "isexistingcustomer",
        "policyno",
        "policystatus",
        "bankname",
        "productname",
        "sumassured",
        "riskcommdate",
        "frequency",
        "installmentpremium",
        "totalreceivedpremium",
        "fundvalue",
        "ppt",
        "pt",
        "paidtodate",
        "pptlastdate",
        "maturitydate",
        "maturityvalue",
        "lapseddate",
        "leadsource",
        "appointmentdate",
        "meetingaddress",
        "remarks",
        "expectedpremiumpaid",
        "actualpremiumpaid",
        "conversionpropensity",
        "proposalnumber",
        "createddate",
        "createdday",
        "createdmonth",
        "createdyear",
        "modifieddate",
        "isjointvisit",
        "supervisorempcode",
        "supervisorname",
        "empcode",
        "emptype",
        "empname",
        "crmleadid",
        "referencetype",
        "referenceleadcode",
        "supervisorid",
        "bmempcode",
        "bmname",
        "bmuserid",
        "cmempcode",
        "cmuserid",
        "cmname",
        "vpempcode",
        "vpname",
        "vpuserid",
        "isdeleted",
        "createdby",
        "modifiedby",
        "product1",
        "product2"


})
public class PSFLeadModel {

    @JsonProperty("id")
    private Integer id;
    private String network_status;
    private String warning_message;
    private Integer status_code;
    private Integer retry_count;
    @JsonProperty("leadid")
    private String leadid;
    @JsonProperty("leadtype")
    private String leadtype;
    @JsonProperty("userid")
    private String userid;
    @JsonProperty("groupid")
    private String groupid;
    @JsonProperty("assignedtoid")
    private String assignedtoid;
    @JsonProperty("branchid")
    private String branchid;



    @JsonProperty("branchname")
    private String branchname;
    @JsonProperty("isdeleted")
    private Boolean isdeleted;
    @JsonProperty("createdby")
    private String createdby;
    @JsonProperty("modifiedby")
    private String modifiedby;




    @JsonProperty("leadcode")
    private String leadcode;
    @JsonProperty("policyHolderId")
    private String policyholderid;
    @JsonProperty("policyholdersalutation")
    private String policyholdersalutation;
    @JsonProperty("policyholderfirstname")
    private String policyholderfirstname;
    @JsonProperty("policyholderlastname")
    private String policyholderlastname;
    @JsonProperty("policyholderdob")
    private String policyholderdob;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("education")
    private String education;
    @JsonProperty("occupation")
    private String occupation;
    @JsonProperty("annualincome")
    private Integer annualincome;
    @JsonProperty("contactno")
    private String contactno;
    @JsonProperty("referencemobileno")
    private String referencemobileno;
    @JsonProperty("landlineno")
    private String landlineno;
    @JsonProperty("emailid")
    private String emailid;
    @JsonProperty("alternatemobileno")
    private String alternatemobileno;
    @JsonProperty("alternatelandline1")
    private String alternatelandline1;
    @JsonProperty("alternatelandline2")
    private String alternatelandline2;
    @JsonProperty("nomineename")
    private String nomineename;
    @JsonProperty("nomineedob")
    private String nomineedob;
    @JsonProperty("laname")
    private String laname;
    @JsonProperty("ladob")
    private String ladob;
    @JsonProperty("nomineecontactno")
    private String nomineecontactno;
    @JsonProperty("alternateemailid")
    private String alternateemailid;
    @JsonProperty("psfzone")
    private String psfzone;
    @JsonProperty("psfregion")
    private String psfregion;
    @JsonProperty("sudbranchoffice")
    private String sudbranchoffice;
    @JsonProperty("leadstage")
    private String leadstage;
    @JsonProperty("isexistingcustomer")
    private Boolean isexistingcustomer;
    @JsonProperty("policyno")
    private String policyno;
    @JsonProperty("policystatus")
    private String policystatus;
    @JsonProperty("bankname")
    private String bankname;
    @JsonProperty("productname")
    private String productname;
    @JsonProperty("sumassured")
    private Integer sumassured;
    @JsonProperty("riskcommdate")
    private String riskcommdate;
    @JsonProperty("frequency")
    private String frequency;
    @JsonProperty("installmentpremium")
    private Integer installmentpremium;
    @JsonProperty("totalreceivedpremium")
    private Integer totalreceivedpremium;
    @JsonProperty("fundvalue")
    private Integer fundvalue;
    @JsonProperty("ppt")
    private Integer ppt;
    @JsonProperty("pt")
    private Integer pt;
    @JsonProperty("paidtodate")
    private String paidtodate;
    @JsonProperty("pptlastdate")
    private String pptlastdate;
    @JsonProperty("maturitydate")
    private String maturitydate;
    @JsonProperty("maturityvalue")
    private Integer maturityvalue;
    @JsonProperty("lapseddate")
    private String lapseddate;
    @JsonProperty("leadsource")
    private String leadsource;
    @JsonProperty("appointmentdate")
    private String appointmentdate;

    @JsonProperty("appointmentday")
    private Integer appointmentday;
    @JsonProperty("appointmentmonth")
    private Integer appointmentmonth;
    @JsonProperty("appointmentyear")
    private Integer appointmentyear;



    @JsonProperty("meetingaddress")
    private String meetingaddress;
    @JsonProperty("remarks")
    private String remarks;
    @JsonProperty("expectedpremiumpaid")
    private Integer expectedpremiumpaid;
    @JsonProperty("actualpremiumpaid")
    private Integer actualpremiumpaid;
    @JsonProperty("conversionpropensity")
    private String conversionpropensity;
    @JsonProperty("proposalnumber")
    private String proposalnumber;
    @JsonProperty("createddate")
    private String createddate;
    @JsonProperty("createdday")
    private Integer createdday;
    @JsonProperty("createdmonth")
    private Integer createdmonth;
    @JsonProperty("createdyear")
    private Integer createdyear;
    @JsonProperty("modifieddate")
    private String modifieddate;
    @JsonProperty("isjointvisit")
    private Boolean isjointvisit;
    @JsonProperty("supervisorempcode")
    private String supervisorempcode;
    @JsonProperty("supervisorname")
    private String supervisorname;
    @JsonProperty("empcode")
    private String empcode;
    @JsonProperty("emptype")
    private String emptype;
    @JsonProperty("empname")
    private String empname;

    @JsonProperty("crmleadid")
    private String crmleadid;

    @JsonProperty("referencetype")
    private String referencetype;

    @JsonProperty("referenceleadcode")
    private String referenceleadcode;

    @JsonProperty("supervisorid")
    private String supervisorid;

    @JsonProperty("bmempcode")
    private String bmempcode;

    @JsonProperty("bmname")
    private String bmname;

    @JsonProperty("bmuserid")
    private String bmuserid;

    @JsonProperty("cmempcode")
    private String cmempcode;

    @JsonProperty("cmuserid")
    private String cmuserid;

    @JsonProperty("cmname")
    private String cmname;

    @JsonProperty("vpempcode")
    private String vpempcode;

    @JsonProperty("vpname")
    private String vpname;

    @JsonProperty("vpuserid")
    private String vpuserid;






    @JsonProperty("product1")
    private String product1;

    @JsonProperty("product2")
    private String product2;


    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("leadid")
    public String getLeadId() {
        return leadid;
    }

    @JsonProperty("leadid")
    public void setLeadId(String leadid) {
        this.leadid = leadid;
    }

    @JsonProperty("leadtype")
    public String getLeadType() {
        return leadtype;
    }

    @JsonProperty("leadtype")
    public void setLeadType(String leadtype) {
        this.leadtype = leadtype;
    }


    @JsonProperty("userid")
    public String getUserId() {
        return userid;
    }

    @JsonProperty("userid")
    public void setUserId(String userid) {
        this.userid = userid;
    }

    @JsonProperty("groupid")
    public String getGroupId() {
        return groupid;
    }

    @JsonProperty("groupid")
    public void setGroupId(String groupid) {
        this.groupid = groupid;
    }

    @JsonProperty("assignedtoid")
    public String getAssignedToId() {
        return assignedtoid;
    }

    @JsonProperty("assignedtoid")
    public void setAssignedToId(String assignedtoid) {
        this.assignedtoid = assignedtoid;
    }

    @JsonProperty("branchid")
    public String getBranchId() {
        return branchid;
    }

    @JsonProperty("branchid")
    public void setBranchId(String branchid) {
        this.branchid = branchid;
    }

    @JsonProperty("leadcode")
    public String getLeadCode() {
        return leadcode;
    }

    @JsonProperty("leadcode")
    public void setLeadCode(String leadcode) {
        this.leadcode = leadcode;
    }

    @JsonProperty("policyholderid")
    public String getPolicyHolderId() {
        return policyholderid;
    }

    @JsonProperty("policyholderid")
    public void setPolicyHolderId(String policyholderid) {
        this.policyholderid = policyholderid;
    }

    @JsonProperty("policyholdersalutation")
    public String getPolicyHolderSalutation() {
        return policyholdersalutation;
    }

    @JsonProperty("policyholdersalutation")
    public void setPolicyHolderSalutation(String policyholdersalutation) {
        this.policyholdersalutation = policyholdersalutation;
    }

    @JsonProperty("policyholderfirstname")
    public String getPolicyHolderFirstName() {
        return policyholderfirstname;
    }

    @JsonProperty("policyholderfirstname")
    public void setPolicyHolderFirstName(String policyholderfirstname) {
        this.policyholderfirstname = policyholderfirstname;
    }

    @JsonProperty("policyholderlastname")
    public String getPolicyHolderLastName() {
        return policyholderlastname;
    }

    @JsonProperty("policyholderlastname")
    public void setPolicyHolderLastName(String policyholderlastname) {
        this.policyholderlastname = policyholderlastname;
    }

    @JsonProperty("policyholderdob")
    public String getPolicyHolderDob() {
        return policyholderdob;
    }

    @JsonProperty("policyholderdob")
    public void setPolicyHolderDob(String policyholderdob) {
        this.policyholderdob = policyholderdob;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonProperty("education")
    public String getEducation() {
        return education;
    }

    @JsonProperty("education")
    public void setEducation(String education) {
        this.education = education;
    }

    @JsonProperty("occupation")
    public String getOccupation() {
        return occupation;
    }

    @JsonProperty("occupation")
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @JsonProperty("annualincome")
    public Integer getAnnualIncome() {
        return annualincome;
    }

    @JsonProperty("annualincome")
    public void setAnnualIncome(Integer annualincome) {
        this.annualincome = annualincome;
    }

    @JsonProperty("contactno")
    public String getContactNo() {
        return contactno;
    }

    @JsonProperty("contactno")
    public void setContactNo(String contactno) {
        this.contactno = contactno;
    }


    @JsonProperty("referencemobileno")
    public String getReferenceMobileNo() {
        return referencemobileno;
    }

    @JsonProperty("referencemobileno")
    public void setReferenceMobileNo(String referencemobileno) {
        this.referencemobileno = referencemobileno;
    }

    @JsonProperty("landlineno")
    public String getLandLineNo() {
        return landlineno;
    }

    @JsonProperty("landlineno")
    public void setLandLineNo(String landlineno) {
        this.landlineno = landlineno;
    }

    @JsonProperty("emailid")
    public String getEmailId() {
        return emailid;
    }

    @JsonProperty("emailid")
    public void setEmailId(String emailid) {
        this.emailid = emailid;
    }

    @JsonProperty("alternatemobileno")
    public String getAlternateMobileNo() {
        return alternatemobileno;
    }

    @JsonProperty("alternatemobileno")
    public void setAlternateMobileNo(String alternatemobileno) {
        this.alternatemobileno = alternatemobileno;
    }

    @JsonProperty("alternatelandline1")
    public String getAlternateLandLine1() {
        return alternatelandline1;
    }

    @JsonProperty("alternatelandline1")
    public void setAlternateLandLine1(String alternatelandline1) {
        this.alternatelandline1 = alternatelandline1;
    }

    @JsonProperty("alternatelandline2")
    public String getAlternateLandLine2() {
        return alternatelandline2;
    }

    @JsonProperty("alternatelandline2")
    public void setAlternateLandLine2(String alternatelandline2) {
        this.alternatelandline2 = alternatelandline2;
    }

    @JsonProperty("nomineename")
    public String getNomineeName() {
        return nomineename;
    }

    @JsonProperty("nomineename")
    public void setNomineeName(String nomineename) {
        this.nomineename = nomineename;
    }

    @JsonProperty("nomineedob")
    public String getNomineeDob() {
        return nomineedob;
    }

    @JsonProperty("nomineedob")
    public void setNomineeDob(String nomineedob) {
        this.nomineedob = nomineedob;
    }

    @JsonProperty("laname")
    public String getLaName() {
        return laname;
    }

    @JsonProperty("laname")
    public void setLaName(String laname) {
        this.laname = laname;
    }

    @JsonProperty("ladob")
    public String getLaDob() {
        return ladob;
    }

    @JsonProperty("ladob")
    public void setLaDob(String ladob) {
        this.ladob = ladob;
    }

    @JsonProperty("nomineecontactno")
    public String getNomineeContactNo() {
        return nomineecontactno;
    }

    @JsonProperty("nomineecontactno")
    public void setNomineeContactNo(String nomineecontactno) {
        this.nomineecontactno = nomineecontactno;
    }

    @JsonProperty("alternateemailid")
    public String getAlternateEmailId() {
        return alternateemailid;
    }

    @JsonProperty("alternateemailid")
    public void setAlternateEmailId(String alternateemailid) {
        this.alternateemailid = alternateemailid;
    }

    @JsonProperty("psfzone")
    public String getPsfZone() {
        return psfzone;
    }

    @JsonProperty("psfzone")
    public void setPsfZone(String psfzone) {
        this.psfzone = psfzone;
    }

    @JsonProperty("psfregion")
    public String getPsfRegion() {
        return psfregion;
    }

    @JsonProperty("psfregion")
    public void setPsfRegion(String psfregion) {
        this.psfregion = psfregion;
    }

    @JsonProperty("sudbranchoffice")
    public String getSudBranchOffice() {
        return sudbranchoffice;
    }

    @JsonProperty("sudbranchoffice")
    public void setSudBranchOffice(String sudbranchoffice) {
        this.sudbranchoffice = sudbranchoffice;
    }

    @JsonProperty("leadstage")
    public String getLeadStage() {
        return leadstage;
    }

    @JsonProperty("leadstage")
    public void setLeadStage(String leadstage) {
        this.leadstage = leadstage;
    }

    @JsonProperty("isexistingcustomer")
    public Boolean getIsExistingCustomer() {
        return isexistingcustomer;
    }

    @JsonProperty("isexistingcustomer")
    public void setIsExistingCustomer(Boolean isexistingcustomer) {
        this.isexistingcustomer = isexistingcustomer;
    }

    @JsonProperty("policyno")
    public String getPolicyNo() {
        return policyno;
    }

    @JsonProperty("policyno")
    public void setPolicyNo(String policyno) {
        this.policyno = policyno;
    }

    @JsonProperty("policystatus")
    public String getPolicyStatus() {
        return policystatus;
    }

    @JsonProperty("policystatus")
    public void setPolicyStatus(String policystatus) {
        this.policystatus = policystatus;
    }

    @JsonProperty("bankname")
    public String getBankName() {
        return bankname;
    }

    @JsonProperty("bankname")
    public void setBankName(String bankname) {
        this.bankname = bankname;
    }

    @JsonProperty("productname")
    public String getProductName() {
        return productname;
    }

    @JsonProperty("productname")
    public void setProductName(String productname) {
        this.productname = productname;
    }

    @JsonProperty("sumassured")
    public Integer getSumAssured() {
        return sumassured;
    }

    @JsonProperty("sumassured")
    public void setSumAssured(Integer sumassured) {
        this.sumassured = sumassured;
    }

    @JsonProperty("riskcommdate")
    public String getRiskCommDate() {
        return riskcommdate;
    }

    @JsonProperty("riskcommdate")
    public void setRiskCommDate(String riskcommdate) {
        this.riskcommdate = riskcommdate;
    }

    @JsonProperty("frequency")
    public String getFrequency() {
        return frequency;
    }

    @JsonProperty("frequency")
    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    @JsonProperty("installmentpremium")
    public Integer getInstallmentPremium() {
        return installmentpremium;
    }

    @JsonProperty("installmentpremium")
    public void setInstallmentPremium(Integer installmentpremium) {
        this.installmentpremium = installmentpremium;
    }

    @JsonProperty("totalreceivedpremium")
    public Integer getTotalReceivedPremium() {
        return totalreceivedpremium;
    }

    @JsonProperty("totalreceivedpremium")
    public void setTotalReceivedPremium(Integer totalreceivedpremium) {
        this.totalreceivedpremium = totalreceivedpremium;
    }

    @JsonProperty("fundvalue")
    public Integer getFundValue() {
        return fundvalue;
    }

    @JsonProperty("fundvalue")
    public void setFundValue(Integer fundvalue) {
        this.fundvalue = fundvalue;
    }

    @JsonProperty("ppt")
    public Integer getPpt() {
        return ppt;
    }

    @JsonProperty("ppt")
    public void setPpt(Integer ppt) {
        this.ppt = ppt;
    }

    @JsonProperty("pt")
    public Integer getPt() {
        return pt;
    }

    @JsonProperty("pt")
    public void setPt(Integer pt) {
        this.pt = pt;
    }

    @JsonProperty("paidtodate")
    public String getPaidToDate() {
        return paidtodate;
    }

    @JsonProperty("paidtodate")
    public void setPaidToDate(String paidtodate) {
        this.paidtodate = paidtodate;
    }

    @JsonProperty("pptlastdate")
    public String getPptLastDate() {
        return pptlastdate;
    }

    @JsonProperty("pptlastdate")
    public void setPptLastDate(String pptlastdate) {
        this.pptlastdate = pptlastdate;
    }

    @JsonProperty("maturitydate")
    public String getMaturityDate() {
        return maturitydate;
    }

    @JsonProperty("maturitydate")
    public void setMaturityDate(String maturitydate) {
        this.maturitydate = maturitydate;
    }

    @JsonProperty("maturityvalue")
    public Integer getMaturityValue() {
        return maturityvalue;
    }

    @JsonProperty("maturityvalue")
    public void setMaturityValue(Integer maturityvalue) {
        this.maturityvalue = maturityvalue;
    }

    @JsonProperty("lapseddate")
    public String getLapsedDate() {
        return lapseddate;
    }

    @JsonProperty("lapseddate")
    public void setLapsedDate(String lapseddate) {
        this.lapseddate = lapseddate;
    }

    @JsonProperty("leadsource")
    public String getLeadSource() {
        return leadsource;
    }

    @JsonProperty("leadsource")
    public void setLeadSource(String leadsource) {
        this.leadsource = leadsource;
    }

    @JsonProperty("appointmentdate")
    public String getAppointmentDate() {
        return appointmentdate;
    }

    @JsonProperty("appointmentdate")
    public void setAppointmentDate(String appointmentdate) {
        this.appointmentdate = appointmentdate;
    }




    @JsonProperty("appointmentday")
    public Integer getAppointmentDay() {
        return appointmentday;
    }

    @JsonProperty("appointmentday")
    public void setAppointmentDay(Integer appointmentday) {
        this.appointmentday = appointmentday;
    }

    @JsonProperty("appointmentmonth")
    public Integer getAppointmentMonth() {
        return appointmentmonth;
    }

    @JsonProperty("appointmentmonth")
    public void setAppointmentMonth(Integer appointmentmonth) {
        this.appointmentmonth = appointmentmonth;
    }

    @JsonProperty("appointmentyear")
    public Integer getAppointmentYear() {
        return appointmentyear;
    }

    @JsonProperty("appointmentyear")
    public void setAppointmentYear(Integer appointmentyear) {
        this.appointmentyear = appointmentyear;
    }






    @JsonProperty("meetingaddress")
    public String getMeetingAddress() {
        return meetingaddress;
    }

    @JsonProperty("meetingaddress")
    public void setMeetingAddress(String meetingaddress) {
        this.meetingaddress = meetingaddress;
    }

    @JsonProperty("remarks")
    public String getRemarks() {
        return remarks;
    }

    @JsonProperty("remarks")
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @JsonProperty("expectedpremiumpaid")
    public Integer getExpectedPremiumPaid() {
        return expectedpremiumpaid;
    }

    @JsonProperty("expectedpremiumpaid")
    public void setExpectedPremiumPaid(Integer expectedpremiumpaid) {
        this.expectedpremiumpaid = expectedpremiumpaid;
    }

    @JsonProperty("actualpremiumpaid")
    public Integer getActualPremiumPaid() {
        return actualpremiumpaid;
    }

    @JsonProperty("actualpremiumpaid")
    public void setActualPremiumPaid(Integer actualpremiumpaid) {
        this.actualpremiumpaid = actualpremiumpaid;
    }

    @JsonProperty("conversionpropensity")
    public String getConversionPropensity() {
        return conversionpropensity;
    }

    @JsonProperty("conversionpropensity")
    public void setConversionPropensity(String conversionpropensity) {
        this.conversionpropensity = conversionpropensity;
    }

    @JsonProperty("proposalnumber")
    public String getProposalNumber() {
        return proposalnumber;
    }

    @JsonProperty("proposalnumber")
    public void setProposalNumber(String proposalnumber) {
        this.proposalnumber = proposalnumber;
    }

    @JsonProperty("createddate")
    public String getCreatedDate() {
        return createddate;
    }

    @JsonProperty("createddate")
    public void setCreatedDate(String createddate) {
        this.createddate = createddate;
    }

    @JsonProperty("createdday")
    public Integer getCreatedDay() {
        return createdday;
    }

    @JsonProperty("createdday")
    public void setCreatedDay(Integer createdday) {
        this.createdday = createdday;
    }

    @JsonProperty("createdmonth")
    public Integer getCreatedMonth() {
        return createdmonth;
    }

    @JsonProperty("createdmonth")
    public void setCreatedMonth(Integer createdmonth) {
        this.createdmonth = createdmonth;
    }

    @JsonProperty("createdyear")
    public Integer getCreatedYear() {
        return createdyear;
    }

    @JsonProperty("createdyear")
    public void setCreatedYear(Integer createdyear) {
        this.createdyear = createdyear;
    }

    @JsonProperty("modifieddate")
    public String getModifiedDate() {
        return modifieddate;
    }

    @JsonProperty("modifieddate")
    public void setModifiedDate(String modifieddate) {
        this.modifieddate = modifieddate;
    }

    @JsonProperty("isjointvisit")
    public Boolean getIsjointVisit() {
        return isjointvisit;
    }

    @JsonProperty("isjointvisit")
    public void setIsjointVisit(Boolean isjointvisit) {
        this.isjointvisit = isjointvisit;
    }

    @JsonProperty("supervisorempcode")
    public String getSupervisorEmpCode() {
        return supervisorempcode;
    }

    @JsonProperty("supervisorempcode")
    public void setSupervisorEmpCode(String supervisorempcode) {
        this.supervisorempcode = supervisorempcode;
    }

    @JsonProperty("supervisorname")
    public String getSupervisorName() {
        return supervisorname;
    }

    @JsonProperty("supervisorname")
    public void setSupervisorName(String supervisorname) {
        this.supervisorname = supervisorname;
    }

    @JsonProperty("empcode")
    public String getEmpCode() {
        return empcode;
    }

    @JsonProperty("empcode")
    public void setEmpCode(String empcode) {
        this.empcode = empcode;
    }

    @JsonProperty("emptype")
    public String getEmpType() {
        return emptype;
    }

    @JsonProperty("emptype")
    public void setEmpType(String emptype) {
        this.emptype = emptype;
    }

    @JsonProperty("empname")
    public String getEmpName() {
        return empname;
    }

    @JsonProperty("empname")
    public void setEmpName(String empname) {
        this.empname = empname;
    }


    @JsonProperty("crmleadid")
    public String getCrmLeadId() {
        return crmleadid;
    }

    @JsonProperty("crmleadid")
    public void setCrmLeadId(String crmleadid) {
        this.crmleadid = crmleadid;
    }

    @JsonProperty("referencetype")
    public String getReferenceType() {
        return referencetype;
    }

    @JsonProperty("referencetype")
    public void setReferenceType(String referencetype) {
        this.referencetype = referencetype;
    }

    @JsonProperty("referenceleadcode")
    public String getReferenceLeadCode() {
        return referenceleadcode;
    }

    @JsonProperty("referenceleadcode")
    public void setReferenceLeadCode(String referenceleadcode) {
        this.referenceleadcode = referenceleadcode;
    }

    @JsonProperty("supervisorid")
    public String getSupervisorId() {
        return supervisorid;
    }

    @JsonProperty("supervisorid")
    public void setSupervisorId(String supervisorid) {
        this.supervisorid = supervisorid;
    }

    @JsonProperty("bmempcode")
    public String getBmEmpCode() {
        return bmempcode;
    }

    @JsonProperty("bmempcode")
    public void setBmEmpCode(String bmempcode) {
        this.bmempcode = bmempcode;
    }

    @JsonProperty("bmname")
    public String getBmName() {
        return bmname;
    }

    @JsonProperty("bmname")
    public void setBmName(String bmname) {
        this.bmname = bmname;
    }

    @JsonProperty("bmuserid")
    public String getBmUserid() {
        return bmuserid;
    }

    @JsonProperty("bmuserid")
    public void setBmUserid(String bmuserid) {
        this.bmuserid = bmuserid;
    }

    @JsonProperty("cmempcode")
    public String getCmEmpCode() {
        return cmempcode;
    }

    @JsonProperty("cmempcode")
    public void setCmEmpCode(String cmempcode) {
        this.cmempcode = cmempcode;
    }

    @JsonProperty("cmuserid")
    public String getCmUserid() {
        return cmuserid;
    }

    @JsonProperty("cmuserid")
    public void setCmUserid(String cmuserid) {
        this.cmuserid = cmuserid;
    }

    @JsonProperty("cmname")
    public String getCmName() {
        return cmname;
    }

    @JsonProperty("cmname")
    public void setCmName(String cmname) {
        this.cmname = cmname;
    }

    @JsonProperty("vpempcode")
    public String getVpEmpCode() {
        return vpempcode;
    }

    @JsonProperty("vpempcode")
    public void setVpEmpCode(String vpempcode) {
        this.vpempcode = vpempcode;
    }

    @JsonProperty("vpname")
    public String getVpName() {
        return vpname;
    }

    @JsonProperty("vpname")
    public void setVpName(String vpname) {
        this.vpname = vpname;
    }

    @JsonProperty("vpuserid")
    public String getVpUserid() {
        return vpuserid;
    }

    @JsonProperty("vpuserid")
    public void setVpUserid(String vpuserid) {
        this.vpuserid = vpuserid;
    }


    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }


    @JsonProperty("product1")
    public String getProduct1() {
        return product1;
    }

    @JsonProperty("product1")
    public void setProduct1(String product1) {
        this.product1 = product1;
    }

    @JsonProperty("product2")
    public String getProduct2() {
        return product2;
    }

    @JsonProperty("product2")
    public void setProduct2(String product2) {
        this.product2 = product2;
    }


    public String getNetwork_status() {
        return network_status;
    }

    public void setNetwork_status(String network_status) {
        this.network_status = network_status;
    }


    public String getWarning_message() {
        return warning_message;
    }

    public void setWarning_message(String warning_message) {
        this.warning_message = warning_message;
    }

    public Integer getStatus_code() {
        return status_code;
    }

    public void setStatus_code(Integer status_code) {
        this.status_code = status_code;
    }


    public Integer getRetry_count() {
        return retry_count;
    }

    public void setRetry_count(Integer retry_count) {
        this.retry_count = retry_count;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonProperty("branchname")
    public String getBranchname() {
        return branchname;
    }
    @JsonProperty("branchname")
    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }
    @JsonProperty("createdby")
    public String getCreatedby() {
        return createdby;
    }
    @JsonProperty("createdby")
    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }
    @JsonProperty("modifiedby")
    public String getModifiedby() {
        return modifiedby;
    }
    @JsonProperty("modifiedby")
    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

}