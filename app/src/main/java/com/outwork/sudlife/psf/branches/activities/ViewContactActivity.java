package com.outwork.sudlife.psf.branches.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.BranchesMgr;
import com.outwork.sudlife.psf.branches.models.BranchesModel;
import com.outwork.sudlife.psf.branches.models.ContactModel;
import com.outwork.sudlife.psf.restinterfaces.POJO.GeoAddress;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

public class ViewContactActivity extends BaseActivity {

    private TextView customerName, practisetype, contactname, contactdesignation, phoneno, email, location;
    private ImageView call;
    private BranchesModel branchesModel;
    private boolean ishealthcareEnabled = false;
    private LinearLayout specialitylayout, playout;
    private TextView speciality, classificaiton;
    private ContactModel contactModel;
    public static final int REQUEST_PERMISSION_CALL = 13;
    protected static String[] PERMISSIONS_CALL = {
            Manifest.permission.CALL_PHONE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contact);

        ishealthcareEnabled = SharedPreferenceManager.getInstance().getBoolean(Constants.isHealthCare, false);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("contact")) {
            contactModel = new Gson().fromJson(getIntent().getStringExtra("contact"), ContactModel.class);
        }
        initToolBar();
        viewInitialize();
        initValues();
        setListener();
    }

    private void setListener() {
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (Utils.isValidPhoneNumber(phoneno.getText().toString())) {
                        setCall(phoneno.getText().toString());
                    } else {
                        showToast("Check the number...");
                    }
                } catch (android.content.ActivityNotFoundException ex) {
                    showToast("Call failed, please try again later!");
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setCall(String number) {
        int camPermission = ActivityCompat.checkSelfPermission(ViewContactActivity.this, Manifest.permission.CALL_PHONE);
        if (camPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    ViewContactActivity.this,
                    PERMISSIONS_CALL,
                    REQUEST_PERMISSION_CALL);
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_CALL: {
                if (grantResults != null) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        setCall(phoneno.getText().toString());
                    } else {
                        showToast(getResources().getString(R.string.locationtoast));
                    }
                } else {
                    showToast(getResources().getString(R.string.locationtoast));
                }
                return;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == 200 || requestCode == REQUEST_PERMISSION_CALL) {
                    if (data != null && data.getData() != null) {
                    } else { // Otherwise get the image stored by the camera
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(ViewContactActivity.this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void viewInitialize() {
        contactname = (TextView) findViewById(R.id.uName);
        customerName = (TextView) findViewById(R.id.uBusinessname);
        practisetype = (TextView) findViewById(R.id.practisetype);
        contactdesignation = (TextView) findViewById(R.id.uContactDesignation);
        email = (TextView) findViewById(R.id.uContactEmail);
        phoneno = (TextView) findViewById(R.id.uContactPhoneNo);
        call = (ImageView) findViewById(R.id.call);
        location = (TextView) findViewById(R.id.uLocation);
        specialitylayout = (LinearLayout) findViewById(R.id.spllayout);
        playout = (LinearLayout) findViewById(R.id.playout);
        speciality = (TextView) findViewById(R.id.vspeciality);
        classificaiton = (TextView) findViewById(R.id.vclassification);

        if (ishealthcareEnabled) {
            specialitylayout.setVisibility(View.VISIBLE);
            playout.setVisibility(View.VISIBLE);
        }
    }

    private void initValues() {
        if (contactModel != null) {
            if (Utils.isNotNullAndNotEmpty(contactModel.getCustomerid())) {
                branchesModel = new BranchesMgr(this).getCustomerData(contactModel.getCustomerid());
            }
            if (branchesModel != null) {
                if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomername())) {
                    customerName.setText(branchesModel.getCustomername());
                } else {
                    customerName.setVisibility(View.GONE);
                }
                GeoAddress custAddress = branchesModel.getAddress();
                setAddressValues(custAddress);
            }
            StringBuilder stringBuilder = new StringBuilder();

            if (Utils.isNotNullAndNotEmpty(contactModel.getSalutation())) {
                stringBuilder.append(contactModel.getSalutation());
                stringBuilder.append(".");
            }
            if (Utils.isNotNullAndNotEmpty(contactModel.getCdisplayname())) {
                stringBuilder.append(contactModel.getCdisplayname());
                contactname.setText(stringBuilder);
            }
            if (Utils.isNotNullAndNotEmpty(contactModel.getDesignation())) {
                contactdesignation.setText(contactModel.getDesignation());
            } else {
                contactdesignation.setVisibility(View.GONE);
            }
            if (Utils.isNotNullAndNotEmpty(contactModel.getEmail())) {
                email.setText(contactModel.getEmail());
            } else {
                email.setText(getResources().getString(R.string.notAvailable));
            }
            if (Utils.isNotNullAndNotEmpty(contactModel.getPhoneno())) {
//                call.setVisibility(View.VISIBLE);
                phoneno.setText(contactModel.getPhoneno());
            } else {
                phoneno.setText(getResources().getString(R.string.notAvailable));
//                call.setVisibility(View.GONE);
            }
            if (Utils.isNotNullAndNotEmpty(contactModel.getClassification())) {
                classificaiton.setText(contactModel.getClassification());
            } else {
                classificaiton.setVisibility(View.GONE);
            }
        }

        if (ishealthcareEnabled) {
            if (Utils.isNotNullAndNotEmpty(contactModel.getSpeciality())) {
                speciality.setText(contactModel.getSpeciality());
                practisetype.setTextColor(ContextCompat.getColor(this, R.color.text_black_text_87));
            } else {
                speciality.setText(getResources().getString(R.string.notAvailable));
                practisetype.setTextColor(ContextCompat.getColor(this, R.color.material_grey_500));
            }
            if (Utils.isNotNullAndNotEmpty(contactModel.getPracticing())) {
                practisetype.setText(contactModel.getPracticing());
                practisetype.setTextColor(ContextCompat.getColor(this, R.color.text_black_text_87));
            } else {
                practisetype.setText(getResources().getString(R.string.notAvailable));
                practisetype.setTextColor(ContextCompat.getColor(this, R.color.material_grey_500));
            }
        } else {
            specialitylayout.setVisibility(View.GONE);
            playout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

//    private void initializeToolBar() {
//        Toolbar toolbar = (Toolbar) findViewById(R.id.vCustomerToolBar);
//        setSupportActionBar(toolbar);
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayShowTitleEnabled(true);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
//        }
//    }


    private void setAddressValues(GeoAddress address) {
        if (address != null) {
            String fulladdress = "";

            if (Utils.isNotNullAndNotEmpty(address.getAddressline1())) {
                fulladdress = address.getAddressline1() + " ";
            }
            if (Utils.isNotNullAndNotEmpty(address.getAddressline2())) {
                fulladdress = fulladdress + address.getAddressline2() + " ";
            } else if (Utils.isNotNullAndNotEmpty(address.getLocality())) {
                fulladdress = fulladdress + address.getLocality() + " ";
            }
            if (Utils.isNotNullAndNotEmpty(address.getLandmark())) {
                fulladdress = fulladdress + address.getLandmark() + " ";
            }
            if (Utils.isNotNullAndNotEmpty(address.getCity())) {
                fulladdress = fulladdress + address.getCity();
            }
            if (Utils.isNotNullAndNotEmpty(fulladdress)) {
                location.setText(fulladdress);
            } else {
                location.setVisibility(View.GONE);
            }
        }
    }
}
