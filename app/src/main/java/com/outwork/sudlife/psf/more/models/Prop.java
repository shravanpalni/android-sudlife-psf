package com.outwork.sudlife.psf.more.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Prop {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("flag")
    @Expose
    private String flag;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

}
