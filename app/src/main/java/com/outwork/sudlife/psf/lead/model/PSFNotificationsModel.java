package com.outwork.sudlife.psf.lead.model;



/*import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;*/

/**
 * Created by shravanch on 27-12-2019.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PSFNotificationsModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("relationuserid")
    @Expose
    private String relationuserid;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("objectid")
    @Expose
    private String objectid;
    @SerializedName("notifyid")
    @Expose
    private String notifyid;
    @SerializedName("notifytext")
    @Expose
    private String notifytext;
    @SerializedName("modulename")
    @Expose
    private String modulename;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("eventdate")
    @Expose
    private String eventdate;
    @SerializedName("createddate")
    @Expose
    private String createddate;
    @SerializedName("isread")
    @Expose
    private Boolean isread;
    @SerializedName("isdeleted")
    @Expose
    private Boolean isdeleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getRelationuserid() {
        return relationuserid;
    }

    public void setRelationuserid(String relationuserid) {
        this.relationuserid = relationuserid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getObjectid() {
        return objectid;
    }

    public void setObjectid(String objectid) {
        this.objectid = objectid;
    }

    public String getNotifyid() {
        return notifyid;
    }

    public void setNotifyid(String notifyid) {
        this.notifyid = notifyid;
    }

    public String getNotifytext() {
        return notifytext;
    }

    public void setNotifytext(String notifytext) {
        this.notifytext = notifytext;
    }

    public String getModulename() {
        return modulename;
    }

    public void setModulename(String modulename) {
        this.modulename = modulename;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEventdate() {
        return eventdate;
    }

    public void setEventdate(String eventdate) {
        this.eventdate = eventdate;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public Boolean getIsread() {
        return isread;
    }

    public void setIsread(Boolean isread) {
        this.isread = isread;
    }

    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

}

/*@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "userid",
        "groupid",
        "objectid",
        "notifyid",
        "notifytext",
        "modulename",
        "type",
        "eventdate",
        "createddate",
        "isdeleted"
})

public class PSFNotificationsModel {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("userid")
    private String userid;
    @JsonProperty("groupid")
    private String groupid;
    @JsonProperty("objectid")
    private String objectid;
    @JsonProperty("notifyid")
    private String notifyid;
    @JsonProperty("notifytext")
    private String notifytext;
    @JsonProperty("modulename")
    private String modulename;
    @JsonProperty("type")
    private String type;
    @JsonProperty("eventdate")
    private String eventdate;
    @JsonProperty("createddate")
    private String createddate;
    @JsonProperty("isread")
    private Boolean isread;
    @JsonProperty("isdeleted")
    private Boolean isdeleted;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("userid")
    public String getUserid() {
        return userid;
    }

    @JsonProperty("userid")
    public void setUserid(String userid) {
        this.userid = userid;
    }

    @JsonProperty("groupid")
    public String getGroupid() {
        return groupid;
    }

    @JsonProperty("groupid")
    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    @JsonProperty("objectid")
    public String getObjectid() {
        return objectid;
    }

    @JsonProperty("objectid")
    public void setObjectid(String objectid) {
        this.objectid = objectid;
    }

    @JsonProperty("notifyid")
    public String getNotifyid() {
        return notifyid;
    }

    @JsonProperty("notifyid")
    public void setNotifyid(String notifyid) {
        this.notifyid = notifyid;
    }

    @JsonProperty("notifytext")
    public String getNotifytext() {
        return notifytext;
    }

    @JsonProperty("notifytext")
    public void setNotifytext(String notifytext) {
        this.notifytext = notifytext;
    }

    @JsonProperty("modulename")
    public String getModulename() {
        return modulename;
    }

    @JsonProperty("modulename")
    public void setModulename(String modulename) {
        this.modulename = modulename;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("eventdate")
    public String getEventdate() {
        return eventdate;
    }

    @JsonProperty("eventdate")
    public void setEventdate(String eventdate) {
        this.eventdate = eventdate;
    }

    @JsonProperty("createddate")
    public String getCreateddate() {
        return createddate;
    }

    @JsonProperty("createddate")
    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }



    @JsonProperty("isread ")
    public Boolean getIsread () {
        return isread ;
    }

    @JsonProperty("isread ")
    public void setIsread (Boolean isread ) {
        this.isread  = isread ;
    }







    @JsonProperty("isdeleted")
    public Boolean getIsdeleted() {
        return isdeleted;
    }

    @JsonProperty("isdeleted")
    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}*/
