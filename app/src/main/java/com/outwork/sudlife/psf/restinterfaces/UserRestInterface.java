package com.outwork.sudlife.psf.restinterfaces;

import com.outwork.sudlife.psf.restinterfaces.POJO.DeviceSubscription;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by Panch on 3/22/2016.
 */
public interface UserRestInterface {
    @POST("admin/v1/login")
    Call<RestResponse> login(@Query("loginid") String loginID, @Query("password") String password);

    @POST("AppUserService/user/login")
    Call<RestResponse> signIn(@Header("apprelease") String apprelease,
                              @Header("appversion") String appversion,
                              @Header("libversion") String libversion,
                              @Header("osversion") String osversion,
                              @Header("model") String model,
                              @Header("os") String os,
                              @Header("manufacturer") String manufacturer,
                              @Query("loginID") String loginID, @Query("password") String password);

    @POST("AppUserService/user/signup/validate")
    Call<RestResponse> signUpValidate(@Query("loginID") String loginID, @Query("groupcode") String groupcode);

    @POST("AppUserService/user/signup")
    Call<RestResponse> signUp(@Query("loginID") String loginID, @Query("password") String password, @Query("groupcode") String groupcode);

    @POST("AppUserService/user/subscribe")
    Call<RestResponse> subscribeUsertoGroup(@Query("groupid") String groupid, @Query("userid") String userid);

    @POST("admin/v1/forgotpassword")
    Call<RestResponse> newforgotPassword(@Query("loginid") String email);

    @PUT("mobile/v1/user/resetpassword")
    Call<RestResponse> resetPassword(@Header("utoken") String userToken,
                                     @Query("oldpassword") String oldpassword,
                                     @Query("newpassword") String newpassword);

    @POST("AppUserService/user/resend-activation")
    Call<RestResponse> resendActivation(@Query("loginID") String loginID);

    @POST("AppUserService/user/subscribe/notification")
    Call<RestResponse> subscribeNotification(@Header("utoken") String userToken, @Body DeviceSubscription JsonObject);

    @GET("mobile/v1/user/versionhistory")
    Call<RestResponse> versionHistory(@Query("versioncode") String versioncode, @Query("os") String os);



    @GET("mobile/v1/teams/userallteams")
    Call<RestResponse> getallteams(@Header("utoken") String userToken);
}
