package com.outwork.sudlife.psf.localbase;

import android.content.Context;

import java.util.List;

/**
 * Created by Panch on 2/23/2017.
 */

public class StaticDataMgr {
    private static Context context;
    private static StaticDataDao staticDataDao;

    public StaticDataMgr(Context context) {
        this.context = context;
        staticDataDao = staticDataDao.getInstance(context);
    }

    private static StaticDataMgr instance;

    public static StaticDataMgr getInstance(Context context) {
        if (instance == null)
            instance = new StaticDataMgr(context);
        return instance;
    }

    public void insertStaticList(List<StaticDataDto> prodList,String groupId) {
        staticDataDao.insertStaticList(prodList,groupId);
    }
    public List<StaticDataDto> getStaticList( String groupId,String listtype,String userInput){
        return staticDataDao.getListbyType(groupId,listtype,userInput);
    }

    public List<StaticDataDto> getStaticList( String groupId,String listtype){
        return staticDataDao.getListbyType(groupId,listtype);
    }

    public List<String> getListNames(String groupId,String listtype){
        return staticDataDao.getListNames(groupId,listtype);
    }
    public List<String> getListNamesbyModifiedDate(String groupId,String listtype){
        return staticDataDao.getListNamesbyModifiedDate(groupId,listtype);
    }
}



