package com.outwork.sudlife.psf.more.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessageModel {
    @SerializedName("modifiedTime")
    @Expose
    private String modifiedTime;
    @SerializedName("accessTime")
    @Expose
    private String accessTime;
    @SerializedName("appID")
    @Expose
    private String appID;
    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("messageMasterID")
    @Expose
    private String messageMasterID;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("networkID")
    @Expose
    private String networkID;
    @SerializedName("objectType")
    @Expose
    private String objectType;
    @SerializedName("ownerID")
    @Expose
    private String ownerID;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("invitee")
    @Expose
    private String invitee;
    @SerializedName("relatedObjectData")
    @Expose
    private String relatedObjectData;
    @SerializedName("relatedObjectID")
    @Expose
    private String relatedObjectID;
    @SerializedName("relatedObjectType")
    @Expose
    private String relatedObjectType;
    @SerializedName("createddate")
    @Expose
    private String createddate;
    @SerializedName("subtype")
    @Expose
    private String subtype;
    @SerializedName("prop")
    @Expose
    private Prop prop;
    @SerializedName("imageurl")
    @Expose
    private String imageurl;
    @SerializedName("commentcount")
    @Expose
    private String commentcount;
    @SerializedName("commentflag")
    @Expose
    private String commentflag;
    @SerializedName("share")
    @Expose
    private Share share;
    @SerializedName("fileobject")
    @Expose
    private List<FileModel> fileobject = null;
    @SerializedName("isanonymous")
    @Expose
    private String isanonymous;
    @SerializedName("modifieddate")
    @Expose
    private String modifieddate;
    @SerializedName("channelid")
    @Expose
    private String channelid;
    @SerializedName("sharecount")
    @Expose
    private String sharecount;
    @SerializedName("viewcount")
    @Expose
    private String viewcount;
    @SerializedName("readcount")
    @Expose
    private String readcount;
    @SerializedName("isread")
    @Expose
    private String isread;
    @SerializedName("posttype")
    @Expose
    private String posttype;
    @SerializedName("postedby")
    @Expose
    private Postedby postedby;

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(String accessTime) {
        this.accessTime = accessTime;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getMessageMasterID() {
        return messageMasterID;
    }

    public void setMessageMasterID(String messageMasterID) {
        this.messageMasterID = messageMasterID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNetworkID() {
        return networkID;
    }

    public void setNetworkID(String networkID) {
        this.networkID = networkID;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getInvitee() {
        return invitee;
    }

    public void setInvitee(String invitee) {
        this.invitee = invitee;
    }

    public String getRelatedObjectData() {
        return relatedObjectData;
    }

    public void setRelatedObjectData(String relatedObjectData) {
        this.relatedObjectData = relatedObjectData;
    }

    public String getRelatedObjectID() {
        return relatedObjectID;
    }

    public void setRelatedObjectID(String relatedObjectID) {
        this.relatedObjectID = relatedObjectID;
    }

    public String getRelatedObjectType() {
        return relatedObjectType;
    }

    public void setRelatedObjectType(String relatedObjectType) {
        this.relatedObjectType = relatedObjectType;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public Prop getProp() {
        return prop;
    }

    public void setProp(Prop prop) {
        this.prop = prop;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getCommentcount() {
        return commentcount;
    }

    public void setCommentcount(String commentcount) {
        this.commentcount = commentcount;
    }

    public String getCommentflag() {
        return commentflag;
    }

    public void setCommentflag(String commentflag) {
        this.commentflag = commentflag;
    }

    public Share getShare() {
        return share;
    }

    public void setShare(Share share) {
        this.share = share;
    }

    public List<FileModel> getFileobject() {
        return fileobject;
    }

    public void setFileobject(List<FileModel> fileobject) {
        this.fileobject = fileobject;
    }

    public String getIsanonymous() {
        return isanonymous;
    }

    public void setIsanonymous(String isanonymous) {
        this.isanonymous = isanonymous;
    }

    public String getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(String modifieddate) {
        this.modifieddate = modifieddate;
    }

    public String getChannelid() {
        return channelid;
    }

    public void setChannelid(String channelid) {
        this.channelid = channelid;
    }

    public String getSharecount() {
        return sharecount;
    }

    public void setSharecount(String sharecount) {
        this.sharecount = sharecount;
    }

    public String getViewcount() {
        return viewcount;
    }

    public void setViewcount(String viewcount) {
        this.viewcount = viewcount;
    }

    public String getReadcount() {
        return readcount;
    }

    public void setReadcount(String readcount) {
        this.readcount = readcount;
    }

    public String getIsread() {
        return isread;
    }

    public void setIsread(String isread) {
        this.isread = isread;
    }

    public String getPosttype() {
        return posttype;
    }

    public void setPosttype(String posttype) {
        this.posttype = posttype;
    }

    public Postedby getPostedby() {
        return postedby;
    }

    public void setPostedby(Postedby postedby) {
        this.postedby = postedby;
    }

}
