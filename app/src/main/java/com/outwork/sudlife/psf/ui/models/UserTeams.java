package com.outwork.sudlife.psf.ui.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shravanch on 25-04-2019.
 */

public class UserTeams implements Serializable {



    @SerializedName("teamid")
    @Expose
    private String teamid;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("internalrole")
    @Expose
    private String internalrole;





    public String getTeamid() {
        return teamid;
    }

    public void setTeamid(String teamid) {
        this.teamid = teamid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInternalrole() {
        return internalrole;
    }

    public void setInternalrole(String internalrole) {
        this.internalrole = internalrole;
    }



}
