package com.outwork.sudlife.psf.lead.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.gson.Gson;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.activities.ViewLeadActivity;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class TeamLeadsAdapter extends RecyclerView.Adapter<TeamLeadsAdapter.LeadViewHolder> implements Filterable {
    private Context context;
    private final LayoutInflater mInflater;
    private List<LeadModel> leadModels;
    private LeadFilter leadFilter;
    private String type, stage;
    private List<LeadModel> leadModelArrayList = new ArrayList<>();

    public static class LeadViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvCityName, tvconversionprop, tvLeadStatus, leadcode;
        View status;
        CardView cardview;

        public LeadViewHolder(View v) {
            super(v);
            tvName = (TextView) itemView.findViewById(R.id.tv_branchname);
            tvCityName = (TextView) itemView.findViewById(R.id.tv_cityName);
            tvconversionprop = (TextView) itemView.findViewById(R.id.cnvsprop);
            tvLeadStatus = (TextView) itemView.findViewById(R.id.lead_status);
            leadcode = (TextView) itemView.findViewById(R.id.leadcode);
            status = itemView.findViewById(R.id.status);
            cardview = (CardView) itemView.findViewById(R.id.cv_lead);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, tvName);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, tvCityName, tvconversionprop, tvLeadStatus, leadcode);
        }
    }

    public TeamLeadsAdapter(List<LeadModel> leadModels, Context context, String type, String stage) {
        this.context = context;
        this.leadModels = leadModels;
        this.leadModelArrayList = leadModels;
        this.type = type;
        this.stage = stage;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public LeadViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lead_list_item, parent, false);
        return new LeadViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LeadViewHolder holder, final int position) {
        final LeadModel dto = (LeadModel) this.leadModels.get(position);
        StringBuilder stringBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(dto.getFirstname()))
            stringBuilder.append(dto.getFirstname());
        if (Utils.isNotNullAndNotEmpty(dto.getLastname()))
            stringBuilder.append(" " + dto.getLastname());
        StringBuilder branchBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(dto.getBankbranchname()))
            branchBuilder.append(dto.getBankbranchname());
        if (Utils.isNotNullAndNotEmpty(dto.getBank()))
            branchBuilder.append(" ( " + dto.getBank() + " )");
        holder.tvName.setText(stringBuilder.toString());
        holder.tvCityName.setText(branchBuilder.toString());
        if (Utils.isNotNullAndNotEmpty(dto.getLeadstage()))
            holder.tvLeadStatus.setText(dto.getLeadstage());
        if (Utils.isNotNullAndNotEmpty(dto.getStatus())) {
            holder.tvconversionprop.setText(dto.getStatus());
        } else {
            holder.tvconversionprop.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(dto.getLeadcode()))
            holder.leadcode.setText(dto.getLeadcode());
        if (Utils.isNotNullAndNotEmpty(leadModels.get(position).getNetwork_status())) {
            if (leadModels.get(position).getNetwork_status().equalsIgnoreCase("offline")) {
                holder.status.setVisibility(View.VISIBLE);
            } else {
                holder.status.setVisibility(View.GONE);
            }
        } else {
            holder.status.setVisibility(View.GONE);
        }
        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ViewLeadActivity.class);
                intent.putExtra("leadobj", new Gson().toJson(dto));
                intent.putExtra("teamlead", "team");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return leadModels.size();
    }

    @Override
    public Filter getFilter() {
        if (leadFilter == null)
            leadFilter = new LeadFilter();
        return leadFilter;
    }

    private class LeadFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0 || TextUtils.isEmpty(constraint)) {
                results.values = leadModelArrayList;
                results.count = leadModelArrayList.size();
            } else if (Utils.isNotNullAndNotEmpty(type) && stage != null) {
                if (type.equalsIgnoreCase("name")) {
                    List<LeadModel> fileredCustList = new ArrayList<LeadModel>();
                    for (int i = 0; i < leadModelArrayList.size(); i++) {
                        if ((leadModelArrayList.get(i).getFirstname().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                                (leadModelArrayList.get(i).getLastname().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                            fileredCustList.add(leadModelArrayList.get(i));
                        }
                    }
                    results.values = fileredCustList;
                    results.count = fileredCustList.size();
                } else if (type.equalsIgnoreCase("branchname")) {
                    List<LeadModel> fileredCustList = new ArrayList<LeadModel>();
                    for (int i = 0; i < leadModelArrayList.size(); i++) {
                        if ((leadModelArrayList.get(i).getBankbranchname().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                            fileredCustList.add(leadModelArrayList.get(i));
                        }
                    }
                    results.values = fileredCustList;
                    results.count = fileredCustList.size();
                } else if (type.equalsIgnoreCase("bankname")) {
                    List<LeadModel> fileredCustList = new ArrayList<LeadModel>();
                    for (int i = 0; i < leadModelArrayList.size(); i++) {
                        if ((leadModelArrayList.get(i).getBank().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                            fileredCustList.add(leadModelArrayList.get(i));
                        }
                    }
                    results.values = fileredCustList;
                    results.count = fileredCustList.size();
                } else if (type.equalsIgnoreCase("contactno")) {
                    List<LeadModel> fileredCustList = new ArrayList<LeadModel>();
                    for (int i = 0; i < leadModelArrayList.size(); i++) {
                        if ((leadModelArrayList.get(i).getContactno().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                            fileredCustList.add(leadModelArrayList.get(i));
                        }
                    }
                    results.values = fileredCustList;
                    results.count = fileredCustList.size();
                } else if (type.equalsIgnoreCase("sudcode")) {
                    List<LeadModel> fileredCustList = new ArrayList<LeadModel>();
                    for (int i = 0; i < leadModelArrayList.size(); i++) {
                        if ((leadModelArrayList.get(i).getLeadcode().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                            fileredCustList.add(leadModelArrayList.get(i));
                        }
                    }
                    results.values = fileredCustList;
                    results.count = fileredCustList.size();
                }
            } else {
                List<LeadModel> fileredCustList = new ArrayList<LeadModel>();
                for (int i = 0; i < leadModelArrayList.size(); i++) {
                    if ((leadModelArrayList.get(i).getFirstname().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                            (leadModelArrayList.get(i).getLastname().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                            (leadModelArrayList.get(i).getBankbranchname().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                            (leadModelArrayList.get(i).getBank().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                            (leadModelArrayList.get(i).getContactno().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                            (leadModelArrayList.get(i).getConversionpropensity().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                            (leadModelArrayList.get(i).getLeadcode().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        fileredCustList.add(leadModelArrayList.get(i));
                    }
                }
                results.values = fileredCustList;
                results.count = fileredCustList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null) {
                leadModels = (List<LeadModel>) results.values;
                notifyDataSetChanged();
            } else {
                leadModels = leadModelArrayList;
                notifyDataSetChanged();
            }
        }
    }
}