package com.outwork.sudlife.psf.lead.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shravanch on 18-12-2019.
 */

public class PSFLeadIdAutoCompleteAdapter extends BaseAdapter implements Filterable {
    private Context context;
    private final LayoutInflater mInflater;

    private List<PSFLeadModel> resultList = new ArrayList<PSFLeadModel>();
    private static final int MAX_RESULTS = 10;
    private String groupid;
    private String userid;

    static class ViewHolder {
        public TextView name;
        public TextView line2;
    }

    public PSFLeadIdAutoCompleteAdapter(Context context, String groupId,String userId){

        this.context = context;
        this.userid = userId;
        this.groupid = groupId;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.resultList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this.resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @SuppressLint("ViewHolder") @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {


        View view = convertView;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_contact_dropdown, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView)view.findViewById(R.id.contactname);
            viewHolder.line2 = (TextView)view.findViewById(R.id.contactline2);
            view.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) view.getTag();

        PSFLeadModel dto = (PSFLeadModel) this.resultList.get(position);

        holder.name.setText(dto.getPolicyHolderFirstName());
        holder.line2.setText(dto.getContactNo());

       /* String contactName = dto.getCdisplayname();

        String customerName = dto.getCustomername();

        if (TextUtils.isEmpty(customerName) || contactName.equalsIgnoreCase(customerName) ){
            if (!TextUtils.isEmpty(dto.getPhoneno())){
                holder.line2.setText("Individual "+ dto.getPhoneno());
            } else {
                holder.line2.setText("Individual");
            }
        } else  {
            holder.line2.setText(customerName);
        }*/
        return view;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence userinput) {
                FilterResults filterResults = new FilterResults();
                if (userinput != null) {
                    List<PSFLeadModel> contactModelList = new LeadMgr(context).getLeadIdList(groupid,userid,userinput.toString());
                    filterResults.values = contactModelList;
                    filterResults.count = contactModelList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence userinput, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<PSFLeadModel>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }
}