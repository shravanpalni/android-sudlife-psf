package com.outwork.sudlife.psf.lead.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.services.CustomerService;
import com.outwork.sudlife.psf.dao.DBHelper;
import com.outwork.sudlife.psf.dao.IvokoProvider;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.activities.PSFLeadListActivity;
import com.outwork.sudlife.psf.lead.activities.PSFUpdateLeadActivity;
import com.outwork.sudlife.psf.lead.activities.PSFViewLeadActivity;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.lead.model.PSFFlsModel;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.activities.NotificationsListActivity;
import com.outwork.sudlife.psf.ui.base.AppBaseActivity;
import com.outwork.sudlife.psf.ui.models.Userprofile;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shravanch on 17-12-2019.
 */

public class PSFLeadsAdapter extends RecyclerView.Adapter<PSFLeadsAdapter.LeadViewHolder> implements Filterable {
    private Context context;
    private final LayoutInflater mInflater;
    private List<PSFLeadModel> leadModels;
    private PSFLeadsAdapter.LeadFilter leadFilter;
    private String type, stage, selectedleadsource, maturitydatemonth,flsuserid;
    private List<PSFLeadModel> leadModelArrayList = new ArrayList<>();
    private ArrayList<Userprofile> flsList = new ArrayList();
    private ArrayList<Userprofile> showflsList = new ArrayList();
    private List<String> teamuserlist = new ArrayList();
    private String selectedTeamId;


    public static class LeadViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvCityName, tvconversionprop, tvLeadStatus, leadcode, status, allocate, reject, reallocate, warning_tv, tv_maturitydate;
        ImageView statusCode;
        //View status;
        CardView cardview;

        public LeadViewHolder(View v) {
            super(v);
            tvName = (TextView) itemView.findViewById(R.id.tv_branchname);
            tv_maturitydate = (TextView) itemView.findViewById(R.id.tv_maturitydate);
            tvCityName = (TextView) itemView.findViewById(R.id.tv_cityName);
            tvconversionprop = (TextView) itemView.findViewById(R.id.cnvsprop);
            tvLeadStatus = (TextView) itemView.findViewById(R.id.lead_status);
            leadcode = (TextView) itemView.findViewById(R.id.leadcode);
            status = (TextView) itemView.findViewById(R.id.status);
            allocate = (TextView) itemView.findViewById(R.id.allocate);
            reject = (TextView) itemView.findViewById(R.id.reject);
            reallocate = (TextView) itemView.findViewById(R.id.reallocate);
            warning_tv = (TextView) itemView.findViewById(R.id.warning_tv);
            statusCode = (ImageView) itemView.findViewById(R.id.statusCode);
            cardview = (CardView) itemView.findViewById(R.id.cv_lead);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, tvName);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, tvCityName, tvconversionprop, tvLeadStatus, leadcode);
        }
    }

    public PSFLeadsAdapter(List<PSFLeadModel> leadModels, Context context, String type, String stage, List<String> userlist, String selectedTeamId, String selectedleadsource, String maturitydatemonth,String flsuserid) {
        this.context = context;
        this.leadModels = leadModels;
        this.leadModelArrayList = leadModels;
        this.type = type;
        this.stage = stage;
        this.teamuserlist = userlist;
        this.selectedTeamId = selectedTeamId;
        this.selectedleadsource = selectedleadsource;
        this.maturitydatemonth = maturitydatemonth;
        this.flsuserid = flsuserid;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public PSFLeadsAdapter.LeadViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.psflead_list_item, parent, false);
        return new PSFLeadsAdapter.LeadViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LeadViewHolder holder, final int position) {
        final PSFLeadModel dto = (PSFLeadModel) this.leadModels.get(position);
        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
            if (Utils.isNotNullAndNotEmpty(dto.getLeadId())){
                List<PSFNotificationsModel> psfNotificationsRejectedList = new ArrayList();
                psfNotificationsRejectedList = LeadMgr.getInstance(context).getPSFNotificationUserBasedonLeadid(dto.getLeadId());
                if (psfNotificationsRejectedList.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.cardview.setBackgroundColor(context.getColor(R.color.main_color_grey_400));
                    }
                }else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.cardview.setBackgroundColor(context.getColor(R.color.white));
                    }
                }
            }


        }

        StringBuilder stringBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(dto.getPolicyHolderFirstName()))
            stringBuilder.append(dto.getPolicyHolderFirstName());
        if (Utils.isNotNullAndNotEmpty(dto.getPolicyHolderLastName()))
            stringBuilder.append(" " + dto.getPolicyHolderLastName());
        StringBuilder branchBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(dto.getLeadSource()))
            branchBuilder.append(dto.getLeadSource());
      /*  if (Utils.isNotNullAndNotEmpty(dto.getBank()))
            branchBuilder.append(" ( " + dto.getBank() + " )");*/
        holder.tvName.setText(stringBuilder.toString());
        holder.tvCityName.setText(branchBuilder.toString());
       /* if (Utils.isNotNullAndNotEmpty(dto.getLeadSource())) {

            if (dto.getLeadSource().equalsIgnoreCase("Maturity")) {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date = null;
                if (Utils.isNotNullAndNotEmpty(dto.getMaturityDate())) {
                    try {

                        date = sdf.parse(dto.getMaturityDate());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    sdf = new SimpleDateFormat("MMM , yyyy");
                    String formatedDateString = sdf.format(date);
                    holder.tv_maturitydate.setVisibility(View.VISIBLE);
                    holder.tv_maturitydate.setText(formatedDateString);

                } else {
                    holder.tv_maturitydate.setVisibility(View.VISIBLE);
                    holder.tv_maturitydate.setText("Date not available");
                }


            }


        } else {
            holder.tv_maturitydate.setVisibility(View.GONE);
        }*/


        if (Utils.isNotNullAndNotEmpty(dto.getLeadStage()))
            holder.tvLeadStatus.setText(dto.getLeadStage());


        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
            if (Utils.isNotNullAndNotEmpty(dto.getLeadStage())) {

                if (dto.getLeadStage().equalsIgnoreCase("Unallocated") && (!(dto.getLeadSource().equalsIgnoreCase("Reference/Self")))) {

                    holder.allocate.setVisibility(View.VISIBLE);
                    holder.reallocate.setVisibility(View.GONE);


                }

                if ((dto.getLeadStage().equalsIgnoreCase("Pre-Fixed Appointment") && (!(dto.getLeadSource().equalsIgnoreCase("Reference/Self")))) || (dto.getLeadStage().equalsIgnoreCase("Proposition presented") && dto.getConversionPropensity().equalsIgnoreCase("RED") && (!(dto.getLeadSource().equalsIgnoreCase("Reference/Self"))))) {

                    if(!dto.getBmUserid().equalsIgnoreCase(dto.getAssignedToId())){
                        holder.reallocate.setVisibility(View.VISIBLE);
                    }else {
                        holder.reallocate.setVisibility(View.GONE);
                    }

                    holder.allocate.setVisibility(View.GONE);
                  /*  if (!(dto.getLeadSource().equalsIgnoreCase("Reference/Self"))) {
                    }*/


                }

            }

        }


        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {

            if (Utils.isNotNullAndNotEmpty(dto.getLeadStage())) {

                if (dto.getLeadStage().equalsIgnoreCase("Pre-Fixed Appointment")) {

                    if (!(dto.getLeadSource().equalsIgnoreCase("Reference/Self"))) {
                        holder.reject.setVisibility(View.VISIBLE);
                    }


                }

            }

        }




        /*if (Utils.isNotNullAndNotEmpty(dto.getLeadStage())) {
            holder.tvconversionprop.setText(dto.getLeadStage());
        } else {
            holder.tvconversionprop.setVisibility(View.GONE);
        }*/
        if (Utils.isNotNullAndNotEmpty(dto.getLeadCode()))
            holder.leadcode.setText(dto.getLeadCode());
        if (Utils.isNotNullAndNotEmpty(leadModels.get(position).getNetwork_status())) {
            if (leadModels.get(position).getNetwork_status().equalsIgnoreCase("offline")) {
                holder.status.setText("Offline");
                holder.status.setVisibility(View.VISIBLE);

            } else {
                holder.status.setVisibility(View.GONE);
            }
        } else {
            holder.status.setVisibility(View.GONE);
        }

        if (Utils.isNotNullAndNotEmpty(leadModels.get(position).getWarning_message())) { //dequick


            if (leadModels.get(position).getWarning_message().equalsIgnoreCase("Initiated lead update")){
                holder.warning_tv.setText("* Warning: " + leadModels.get(position).getWarning_message());
                holder.warning_tv.setVisibility(View.GONE);
            }else {
                holder.warning_tv.setText("* Warning: " + leadModels.get(position).getWarning_message());
                holder.warning_tv.setVisibility(View.VISIBLE);
            }


           /* holder.warning_tv.setText("* Warning: " + leadModels.get(position).getWarning_message());
            holder.warning_tv.setVisibility(View.VISIBLE);*/


        } else {
            holder.warning_tv.setVisibility(View.GONE);
        }


        if (leadModels.get(position).getStatus_code() == 3) {


            int count = LeadMgr.getInstance(context).getRetryCount(dto);

            if (count >= 4) {
                holder.statusCode.setVisibility(View.GONE);
                holder.warning_tv.setText("* Warning: There was a failure to upload your lead id" + leadModels.get(position).getLeadCode() + "to Digi-Quick due to some error. Please contact Outwork Helpdesk 7995591183.");
                holder.warning_tv.setVisibility(View.VISIBLE);


            } else if (count < 4) {
                holder.statusCode.setVisibility(View.VISIBLE);
            }


        } else {
            holder.statusCode.setVisibility(View.GONE);
        } //dequick


        holder.allocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!selectedTeamId.equalsIgnoreCase("") && selectedTeamId != null) {

                    getTeamHierarchyMembers(selectedTeamId, dto.getLeadId(), true);
                }
                //getTeamHierarchyMembers("56d113b813b6fa486ed8a7e30afc4048", "");


            }
        });

        holder.reallocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!selectedTeamId.equalsIgnoreCase("") && selectedTeamId != null) {

                    getTeamHierarchyMembers(selectedTeamId, dto.getLeadId(), false);
                }


            }
        });


        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PSFFlsModel psfFlsModel = new PSFFlsModel();
                psfFlsModel.setLeadid(dto.getLeadId());
                psfFlsModel.setAssignedtoid(dto.getBmUserid());
                psfFlsModel.setLeadstage("Unallocated");
                psfFlsModel.setLeadcode(dto.getLeadCode());
                psfFlsModel.setLeadsource(dto.getLeadSource());
                rejectLead(psfFlsModel);


            }
        });


        holder.statusCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {

                    int count = LeadMgr.getInstance(context).getRetryCount(dto);

                    if (count <= 4) {
                        //count ++;
                        //LeadMgr.getInstance(context).updateRetryCount(dto,leadModels.get(position).getLeadId(),count);
                        Intent intent = new Intent(context, PSFUpdateLeadActivity.class);
                        intent.putExtra("leadobj", new Gson().toJson(dto));
                        context.startActivity(intent);
                    }/*else{
                            showToast("Max limit reached.Please contact support.");
                        }  */

                } else {

                    showToast("Please check your ninternet connection.");
                }


            }
        });


        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PSFViewLeadActivity.class);
                intent.putExtra("leadobj", new Gson().toJson(dto));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (leadModels.size() > 0) {
            return leadModels.size();
        } else return 0;
    }

    private void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }

    @Override
    public Filter getFilter() {
        if (leadFilter == null)
            leadFilter = new PSFLeadsAdapter.LeadFilter();
        return leadFilter;
    }

    private class LeadFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0 || TextUtils.isEmpty(constraint)) {
                results.values = leadModelArrayList;
                results.count = leadModelArrayList.size();
            } else if (Utils.isNotNullAndNotEmpty(type) && stage != null) {
                if (type.equalsIgnoreCase("flswise")) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
//                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
//                                getLeadListSearchbyName(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);

                        List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFFLMWiseListSearchnameBM(flsuserid,constraint.toString(),stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFFLMWiseListSearchnameBM(flsuserid,constraint.toString(),"");
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }
                }else if (type.equalsIgnoreCase("myleadswise")) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
//                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
//                                getLeadListSearchbyName(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);

                        List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFMydirectleadsListSearchnameBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""),constraint.toString(),stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFMydirectleadsListSearchnameBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""),constraint.toString(),"");
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }
                }


                else if (type.equalsIgnoreCase("ascdate")) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListSearchbyName(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }
                } else if (type.equalsIgnoreCase("desdate")) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListSearchbyName(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }
                } else if (type.equalsIgnoreCase("campaign")) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                    getPSFLeadListSearchbyCampaignWiseBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage, teamuserlist);
                            results.values = fileredCustList;
                            results.count = fileredCustList.size();

                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                    getPSFLeadListSearchbyCampaignWise(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                            results.values = fileredCustList;
                            results.count = fileredCustList.size();
                        }


                    } else {


                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                            List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                    getPSFLeadListforSearchBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), teamuserlist);
                            results.values = fileredCustList;
                            results.count = fileredCustList.size();

                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                    getPSFLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                            results.values = fileredCustList;
                            results.count = fileredCustList.size();

                        }


                    }
                } else if (type.equalsIgnoreCase("morefiltercampaign")) {


                    if (selectedleadsource.equalsIgnoreCase("Maturity")) {
                        if (Utils.isNotNullAndNotEmpty(stage)) {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                        getPSFLeadListSearchbymaturitydateBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage, teamuserlist, selectedleadsource, maturitydatemonth);
                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                        getPSFLeadListSearchbymaturitydate(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage, selectedleadsource, maturitydatemonth);
                                results.values = fileredCustList;
                                results.count = fileredCustList.size();
                            }

                        } else {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                        getPSFLeadListSearchbymaturitydateBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), teamuserlist, selectedleadsource, maturitydatemonth);
                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                        getPSFLeadListSearchbymaturitydate(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), selectedleadsource, maturitydatemonth);
                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            }

                        }


                    } else {


                        if (Utils.isNotNullAndNotEmpty(stage)) {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                        getPSFLeadListSearchbynonmaturityBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage, teamuserlist, selectedleadsource);
                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                        getPSFLeadListSearchbynonmaturity(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage, selectedleadsource);
                                results.values = fileredCustList;
                                results.count = fileredCustList.size();
                            }

                        } else {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                        getPSFLeadListSearchbynostagenonmaturityBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), teamuserlist, selectedleadsource);
                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                        getPSFLeadListSearchbynostagenonmaturity(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), selectedleadsource);
                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            }

                        }


                    }


                }else if (type.equalsIgnoreCase("morefiltercampaignflm")) {


                    if (selectedleadsource.equalsIgnoreCase("Maturity")) {
                        if (Utils.isNotNullAndNotEmpty(stage)) {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {


                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFcampaignFLMWiseListSearchnameBM(flsuserid,constraint.toString(),stage,selectedleadsource, maturitydatemonth);

                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            }

                        } else {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFcampaignFLMWiseListSearchnameBM(flsuserid,constraint.toString(),"",selectedleadsource, maturitydatemonth);
                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            }

                        }


                    } else {


                        if (Utils.isNotNullAndNotEmpty(stage)) {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                //List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFLeadListSearchbynonmaturityBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage, teamuserlist, selectedleadsource);
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFcampaignFLMWiseListSearchnameBM(flsuserid,constraint.toString(),stage,selectedleadsource, "");

                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            }

                        } else {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                                //List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFLeadListSearchbynostagenonmaturityBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), teamuserlist, selectedleadsource);
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFcampaignFLMWiseListSearchnameBM(flsuserid,constraint.toString(),"",selectedleadsource, "");

                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            }

                        }


                    }


                } else if (type.equalsIgnoreCase("morefiltercampaignmydirectleads")) {


                    if (selectedleadsource.equalsIgnoreCase("Maturity")) {
                        if (Utils.isNotNullAndNotEmpty(stage)) {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {


                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFcampaignMyDirectLeadsWiseListSearchnameBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""),constraint.toString(),stage,selectedleadsource, maturitydatemonth);

                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            }

                        } else {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFcampaignMyDirectLeadsWiseListSearchnameBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""),constraint.toString(),"",selectedleadsource, maturitydatemonth);
                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            }

                        }


                    } else {


                        if (Utils.isNotNullAndNotEmpty(stage)) {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                //List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFLeadListSearchbynonmaturityBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage, teamuserlist, selectedleadsource);
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFcampaignMyDirectLeadsWiseListSearchnameBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""),constraint.toString(),stage,selectedleadsource, "");

                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            }

                        } else {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                                //List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFLeadListSearchbynostagenonmaturityBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), teamuserlist, selectedleadsource);
                                List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).getPSFcampaignMyDirectLeadsWiseListSearchnameBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""),constraint.toString(),"",selectedleadsource, "");

                                results.values = fileredCustList;
                                results.count = fileredCustList.size();

                            }

                        }


                    }


                }else if (type.equalsIgnoreCase("dispositionwise")) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                            List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                    getLeadListSearchbyBanknameBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage, teamuserlist);
                            results.values = fileredCustList;
                            results.count = fileredCustList.size();

                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                    getLeadListSearchbyBankname(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                            results.values = fileredCustList;
                            results.count = fileredCustList.size();

                        }


                    } else {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                    getLeadListforSearchBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), teamuserlist);
                            results.values = fileredCustList;
                            results.count = fileredCustList.size();

                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                    getLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                            results.values = fileredCustList;
                            results.count = fileredCustList.size();

                        }


                    }
                } else if (type.equalsIgnoreCase("self")) {
                /*    if (Utils.isNotNullAndNotEmpty(stage)) {
                        List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getPSFLeadListforSearchbyLeadType(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getPSFLeadListforSearchbyLeadType(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }*/
                } else if (type.equalsIgnoreCase("leadidentification")) {
                /*    if (Utils.isNotNullAndNotEmpty(stage)) {
                        List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getPSFLeadListforSearchbyLeadType(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getPSFLeadListforSearchbyLeadType(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }*/
                } else if (type.equalsIgnoreCase("jointvisit")) {
                   /* if (Utils.isNotNullAndNotEmpty(stage)) {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListSearchbySUDCode(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), stage);
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    } else {
                        List<LeadModel> fileredCustList = LeadMgr.getInstance(context).
                                getLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                        results.values = fileredCustList;
                        results.count = fileredCustList.size();
                    }*/
                }
            } else {

                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                    List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                            getPSFLeadListforSearchBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString(), teamuserlist);
                    results.values = fileredCustList;
                    results.count = fileredCustList.size();

                } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                    List<PSFLeadModel> fileredCustList = LeadMgr.getInstance(context).
                            getPSFLeadListforSearch(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), constraint.toString());
                    results.values = fileredCustList;
                    results.count = fileredCustList.size();
                }


            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null) {
                leadModels = (List<PSFLeadModel>) results.values;
                notifyDataSetChanged();
            } else {
                leadModels = leadModelArrayList;
                notifyDataSetChanged();
            }
        }
    }


    private void getTeamHierarchyMembers(String teamid, String leadid, boolean isAllocate) {
        //showProgressDialog("Loading, please wait....");
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, teamid);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    // dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                        Log.i("shravan", "response = = =" + response.body().toString());

                        flsList = new ArrayList<>();
                        showflsList = new ArrayList<>();
                        ArrayList<Userprofile> rawflsList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                        }.getType());
                        if (Utils.isNotNullAndNotEmpty(rawflsList)) {

                            for (Userprofile up : rawflsList) {

                                flsList.add(up);

                                if (!(up.getUserid().equalsIgnoreCase(SharedPreferenceManager.getInstance().getString(Constants.USERID, "")))) {

                                    showflsList.add(up);
                                }

                            }


                            showDialog(leadid, isAllocate);


                        } else {
                            Toast.makeText(context, "FLS list empty!", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(context, "Data missing!", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(context, "Server error!", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

            }
        });
    }


    private void rejectLead(PSFFlsModel psfFlsModel) {
        //showProgressDialog("Loading, please wait....");
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getReject(userToken, psfFlsModel);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    // dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                        Log.i("shravan", "response = = =" + response.body().toString());

                        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
                        db.execSQL("delete from " + IvokoProvider.PSFLead.TABLE + " WHERE " + IvokoProvider.PSFLead.leadid + " = '" + psfFlsModel.getLeadid() + "'");
                       /* if (isTableExists(db, IvokoProvider.PSFLead.TABLE)) {
                        }*/

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("");
                        alertDialogBuilder.setMessage("Lead rejected successfully.");
                        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                if (context instanceof PSFLeadListActivity) {
                                    ((PSFLeadListActivity) context).refreshData();
                                }

                            }
                        });


                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();


                    } else {
                        Toast.makeText(context, "Data missing!", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(context, "Server error!", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

            }
        });
    }


    private void showDialog(String leadid, boolean isAllocate) {


        final Dialog dialog = new Dialog(context);

        //dialog.setTitle("Select FLS");

        View view = ((Activity) context).getLayoutInflater().inflate(R.layout.userteam_dialog, null);

        ListView lv = (ListView) view.findViewById(R.id.memberList);
        FlsAdapter clad = new FlsAdapter(context, showflsList);
        lv.setAdapter(clad);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                Userprofile dto = (Userprofile) parent.getItemAtPosition(position);
                String flsuserid = dto.getUserid();
                Log.i("shravan", "PSFLeadsAdapter  -- - flsuserid===" + flsuserid);
                Log.i("shravan", "PSFLeadsAdapter  -- - leadid===" + leadid);

                PSFFlsModel psfFlsModel = new PSFFlsModel();
                psfFlsModel.setLeadid(leadid);
                psfFlsModel.setAssignedtoid(flsuserid);
                psfFlsModel.setLeadstage("Pre-Fixed Appointment");
                //pushAllocated(psfFlsModel);

                if (isAllocate) {

                    List<PSFNotificationsModel> psfNotificationsModelList = new ArrayList();
                    psfNotificationsModelList = LeadMgr.getInstance(context).getPSFNotificationUserNames(leadid, flsuserid);
                    if (psfNotificationsModelList.size() > 0) {

                        String name = "";

                        for (PSFNotificationsModel not : psfNotificationsModelList) {

                            //name  = name.concat(","+not.getUsername());
                            name = not.getUsername();

                        }

                     /*   AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("Warning");
                        alertDialogBuilder.setMessage("This Lead was rejected earlier by " + name +". Do you wish to allocate again to the same FLS?");
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                pushAllocated(psfFlsModel);

                            }
                        });


                        alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                alertDialog.dismiss();

                            }
                        });*/


                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Warning");
                        builder.setMessage("This Lead was rejected earlier by " + name +". Do you wish to allocate again to the same FLS?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        pushAllocated(psfFlsModel);
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();





                    } else {
                        pushAllocated(psfFlsModel);
                    }

                } else {

                    pushAllocated(psfFlsModel);

                }


                dialog.cancel();
               /* UserTeams dto = (UserTeams) parent.getItemAtPosition(position);
                //Intent intent = new Intent(CalenderActivity.this, TeamHierarchyActivity.class);
                Intent intent = new Intent(CalenderActivity.this, ShowHierarchyActivity.class);
                intent.putExtra(Constants.PLAN_STATUS, 0);
                intent.putExtra(Constants.PLAN_DATE, displaydate);
                intent.putExtra("USERTEAMID", dto.getTeamid());
                startActivity(intent);
                dialog.cancel();
                bottomSheetDialog.dismiss();*/


            }
        });

        dialog.setContentView(view);

        dialog.show();

    }


    private void pushAllocated(PSFFlsModel psfFlsModel) {
        //showProgressDialog("Loading, please wait....");

        ProgressDialog pd = new ProgressDialog(context);
        pd.setMessage("Please wait...");
        pd.show();
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getAllocation(userToken, psfFlsModel);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    // dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                        Log.i("shravan", "response = = =" + response.body().toString());


                        if (Utils.isNotNullAndNotEmpty(response.body().getData().toString())) {


                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                            alertDialogBuilder.setTitle("");
                            alertDialogBuilder.setMessage("Lead allocated successfully.");
                            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    if (context instanceof PSFLeadListActivity) {
                                        ((PSFLeadListActivity) context).refreshData();
                                    }

                                }
                            });


                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                            if (pd!=null &&pd.isShowing() ){
                                pd.dismiss();
                            }


                        } else {
                            Toast.makeText(context, response.body().getStatus() + " - " + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        }


                    } else {

                        Toast.makeText(context, "Data missing!", Toast.LENGTH_SHORT).show();
                        if (pd!=null &&pd.isShowing() ){
                            pd.dismiss();
                        }
                    }
                } else {
                    Toast.makeText(context, "Server error!", Toast.LENGTH_SHORT).show();
                    if (pd!=null &&pd.isShowing() ){
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                Toast.makeText(context, "Server error!", Toast.LENGTH_SHORT).show();
                if (pd!=null &&pd.isShowing() ){
                    pd.dismiss();
                }
            }
        });
    }


}