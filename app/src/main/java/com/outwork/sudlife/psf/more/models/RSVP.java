package com.outwork.sudlife.psf.more.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RSVP {

    @SerializedName("contactphone")
    @Expose
    private String contactphone;
    @SerializedName("contactname")
    @Expose
    private String contactname;
    @SerializedName("contactemail")
    @Expose
    private String contactemail;

    @SerializedName("noofpeople")
    @Expose
    private String guestcount;


    public String getContactphone() {
        return contactphone;
    }
    public void setContactphone(String contactphone) {
        this.contactphone = contactphone;
    }
    public String getContactname() {
        return contactname;
    }
    public void setContactname(String contactname) {
        this.contactname = contactname;
    }
    public String getContactemail() {
        return contactemail;
    }
    public void setContactemail(String contactemail) {
        this.contactemail = contactemail;
    }
    public String getGuestcount() {
        return guestcount;
    }
    public void setGuestcount(String guestcount) {
        this.guestcount = guestcount;
    }
}
