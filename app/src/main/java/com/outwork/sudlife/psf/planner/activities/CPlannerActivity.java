package com.outwork.sudlife.psf.planner.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.BranchesMgr;
import com.outwork.sudlife.psf.branches.activities.ListCustomerActivity;
import com.outwork.sudlife.psf.branches.activities.ShowHierarchyActivity;
import com.outwork.sudlife.psf.branches.models.BranchesModel;
import com.outwork.sudlife.psf.branches.services.ContactsIntentService;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.adapter.BranchesPlannerAdapter;
import com.outwork.sudlife.psf.planner.adapter.DummyAdapter;
import com.outwork.sudlife.psf.planner.models.DataModel;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.activities.NotificationsListActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CPlannerActivity extends BaseActivity  {
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private CompactCalendarView compactCalendarView;
    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());
    private SimpleDateFormat dateFormatDisplaying = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private List<PlannerModel> plannerModelList = new ArrayList<>();
    private List<PlannerModel> finalPlannerModelList = new ArrayList<>();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView planRecyclerView;
    private ProgressBar msgListProgressBar;
    private TextView nomessageview, toolbar_title;
    private FloatingActionButton addplan;
    //private CalenderPlannerAdapter calenderPlannerAdapter;
    private DummyAdapter calenderPlannerAdapter;
    private BranchesPlannerAdapter branchesPlannerAdapter;
    private LinearLayout branches;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;
    private int day, monthno, year;
    private String toolbardate, plandate, displaydate;
    private List<BranchesModel> customerList = new ArrayList<>();
    private ArrayList<String> stringArrayList;
    private ArrayList<DataModel> branchNamesWithColorsArrayList;
    private ArrayList<DataModel> colorsDataModelArrayList;
    private ArrayList<Integer> tagArrayList;
    private boolean clicked = false;
    private Integer viewid;
    TextView textCartItemCount;
    int mCartItemCount = 10;



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cplanner);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        mgr = LocalBroadcastManager.getInstance(CPlannerActivity.this);
        if (SharedPreferenceManager.getInstance().getString(Constants.CUSTOMERS_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                ContactsIntentService.insertCustomersinDB(CPlannerActivity.this);
            }
        }
        if (isNetworkAvailable()) {
            if (PlannerMgr.getInstance(CPlannerActivity.this).getOfflineplannerList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline").size() > 0)
            LeadIntentService.syncLeadstoServer(CPlannerActivity.this);
        }
        planRecyclerView = (RecyclerView) findViewById(R.id.fflogRecycler);
        branches = (LinearLayout) findViewById(R.id.branches);
        branches.setVisibility(View.GONE);
        nomessageview = (TextView) findViewById(R.id.ffnomessages);
        msgListProgressBar = (ProgressBar) findViewById(R.id.ffmessageListProgressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.task_swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.topbarcolor, R.color.daycolor);
        addplan = (FloatingActionButton) findViewById(R.id.addplan);
        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        stringArrayList = new ArrayList<>();
        stringArrayList.add("#FF0000");
        stringArrayList.add("#008000");
        stringArrayList.add("#0000FF");
        stringArrayList.add("#9C27B0");
        stringArrayList.add("#3F51B5");
        stringArrayList.add("#009688");
        stringArrayList.add("#800000");
        stringArrayList.add("#808000");
        stringArrayList.add("#00FF00");
        stringArrayList.add("#008000");
        stringArrayList.add("#00FFFF");
        stringArrayList.add("#008080");
        stringArrayList.add("#0000FF");
        stringArrayList.add("#FF00FF");
        stringArrayList.add("#000080");
        stringArrayList.add("#800080");
        stringArrayList.add("#CD5C5C");
        stringArrayList.add("#FFA07A");
        stringArrayList.add("#85929E");
        stringArrayList.add("#73C6B6");
        stringArrayList.add("#E67E22");
        stringArrayList.add("#5DADE2");
        stringArrayList.add("#AF7AC5");
        stringArrayList.add("#F7DC6F");
        stringArrayList.add("#AF601A");
        stringArrayList.add("#D5DBDB");
        stringArrayList.add("#FF0000");
        stringArrayList.add("#800000");
        stringArrayList.add("#808000");
        stringArrayList.add("#00FF00");
        stringArrayList.add("#008000");
        stringArrayList.add("#00FFFF");
        stringArrayList.add("#008080");
        stringArrayList.add("#0000FF");
        stringArrayList.add("#FF00FF");
        stringArrayList.add("#000080");
        stringArrayList.add("#800080");
        stringArrayList.add("#CD5C5C");
        stringArrayList.add("#FFA07A");
        stringArrayList.add("#85929E");
        stringArrayList.add("#73C6B6");
        stringArrayList.add("#E67E22");
        stringArrayList.add("#5DADE2");
        stringArrayList.add("#AF7AC5");
        stringArrayList.add("#F7DC6F");
        stringArrayList.add("#AF601A");
        stringArrayList.add("#D5DBDB");
        stringArrayList.add("#FF0000");
        stringArrayList.add("#800000");
        stringArrayList.add("#808000");
        stringArrayList.add("#00FF00");
        stringArrayList.add("#008000");
        stringArrayList.add("#00FFFF");
        stringArrayList.add("#008080");
        stringArrayList.add("#0000FF");
        stringArrayList.add("#FF00FF");
        stringArrayList.add("#000080");
        stringArrayList.add("#800080");
        stringArrayList.add("#CD5C5C");
        stringArrayList.add("#FFA07A");
        stringArrayList.add("#85929E");
        stringArrayList.add("#73C6B6");
        stringArrayList.add("#E67E22");
        stringArrayList.add("#5DADE2");
        stringArrayList.add("#AF7AC5");
        stringArrayList.add("#F7DC6F");
        stringArrayList.add("#AF601A");
        stringArrayList.add("#D5DBDB");
        stringArrayList.add("#808000");
        stringArrayList.add("#008000");
        stringArrayList.add("#0000FF");
        stringArrayList.add("#9C27B0");
        stringArrayList.add("#3F51B5");
        stringArrayList.add("#009688");
        stringArrayList.add("#0000FF");
        stringArrayList.add("#9C27B0");
        stringArrayList.add("#3F51B5");
        stringArrayList.add("#009688");
        stringArrayList.add("#008000");
        stringArrayList.add("#0000FF");
        stringArrayList.add("#9C27B0");
        stringArrayList.add("#3F51B5");
        stringArrayList.add("#009688");
        stringArrayList.add("#0000FF");
        stringArrayList.add("#9C27B0");
        stringArrayList.add("#3F51B5");
        stringArrayList.add("#009688");
        toolbardate = TimeUtils.getCurrentDate("yyyy-MM-dd");
        displaydate = TimeUtils.getCurrentDate("dd/MM/yyyy hh:mm a");
        initToolBar();
        day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
        monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
        year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
        showlist(day, monthno, year);

        compactCalendarView.setUseThreeLetterAbbreviation(false);
        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);
        compactCalendarView.invalidate();
        logEventsByMonth(compactCalendarView);
        toolbar_title.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));
        plannerModelList = PlannerMgr.getInstance(CPlannerActivity.this).getPlannerList(userid);
        getBranches();
        setListener();
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoTypeface, nomessageview);
    }



    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("customer_broadcast"));
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("plan_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Utils.isNotNullAndNotEmpty(toolbardate)) {
            day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
            monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
            year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
            showlist(day, monthno, year);
        }
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        textCartItemCount = (TextView) findViewById(R.id.notification_badge);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showToast("Notification Clicked!!!");
                Intent in = new Intent(CPlannerActivity.this, NotificationsListActivity.class);
                startActivity(in);
            }
        });

        setupBadge();
    }
    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void setListener() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Date c = Calendar.getInstance().getTime();
                compactCalendarView.setCurrentDate(c);
                day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
                monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
                year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
                showlist(day, monthno, year);
                getBranches();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getBranches();
                if (Utils.isNotNullAndNotEmpty(toolbardate)) {
                    day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
                    monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
                    year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
                    showlist(day, monthno, year);
                }
            }
        };
        addplan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            /*    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) >= 1 &&
                        SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) < 4) {*/
                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) >= 1 &&
                            SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) < 5) {  // added on Feb 4
                    //Intent intent = new Intent(CPlannerActivity.this, TeamHierarchyActivity.class);
                        Intent intent = new Intent(CPlannerActivity.this, ShowHierarchyActivity.class);
                    intent.putExtra(Constants.PLAN_STATUS, 1);
                    intent.putExtra(Constants.PLAN_DATE, displaydate);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(CPlannerActivity.this, ListCustomerActivity.class);
                    intent.putExtra(Constants.PLAN_STATUS, 1);
                    intent.putExtra(Constants.PLAN_DATE, displaydate);
                    intent.putExtra("type", "select");
                    intent.putExtra("fromclass", "plan");
                    startActivity(intent);
                }
            }
        });
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                toolbar_title.setText(dateFormatForMonth.format(dateClicked));
                String date = (dateFormat.format(dateClicked));
                displaydate = dateFormatDisplaying.format(dateClicked);
                day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", date));
                monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", date));
                year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", date));
                getBranches();
                showlist(day, monthno, year);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                toolbar_title.setText(dateFormatForMonth.format(firstDayOfNewMonth));
                displaydate = dateFormatDisplaying.format(firstDayOfNewMonth);
//                String date = (dateFormat.format(firstDayOfNewMonth));
                SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                String[] date = dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()).split("-");
                showlist(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]));
            }
        });
    }

    private void getBranches() {
        if (clicked) {
            return;
        }
        customerList.clear();
        branches.removeAllViews();
        customerList = new BranchesMgr(CPlannerActivity.this).getonlineCustomerList(groupId, userid, "");
        if (customerList.size() > 0 && customerList.size() < 80) {
            branches.setVisibility(View.GONE);
            branchNamesWithColorsArrayList = new ArrayList<>();
            for (int i = 0; i < stringArrayList.size(); i++) {
                DataModel dataModel = new DataModel();
                dataModel.setColorName(stringArrayList.get(i));
                branchNamesWithColorsArrayList.add(dataModel);
            }
            for (int i = 0; i < customerList.size(); i++) {
                branchNamesWithColorsArrayList.get(i).setBranchName(customerList.get(i).getCustomername());
            }
            colorsDataModelArrayList = new ArrayList<>();
            for (int i = 0; i < branchNamesWithColorsArrayList.size(); i++) {
                if (branchNamesWithColorsArrayList.get(i).getBranchName() != null) {
                    colorsDataModelArrayList.add(branchNamesWithColorsArrayList.get(i));
                }
            }
            if (colorsDataModelArrayList != null && colorsDataModelArrayList.size() > 0) {
                tagArrayList = new ArrayList<>();
                for (int i = 0; i < colorsDataModelArrayList.size(); i++) {
                    final TextView textView = new TextView(this);
                    //   final ToggleButton textView = new ToggleButton(this);
                    textView.setText(colorsDataModelArrayList.get(i).getBranchName());
                    textView.setBackground(getResources().getDrawable(R.drawable.round_corner_button));
                    textView.setTextColor(Color.parseColor(colorsDataModelArrayList.get(i).getColorName()));
                    compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.cal_selected_color));
                    GradientDrawable titleDrawable = (GradientDrawable) textView.getBackground();
                    titleDrawable.setStroke(5, Color.parseColor(colorsDataModelArrayList.get(i).getColorName()));
                    titleDrawable.setColor(getResources().getColor(R.color.white));

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                    );
                    params.setMargins(5, 5, 5, 5);
                    textView.setLayoutParams(params);
                    /**new charan***/
                    //textView.setTag(colorsDataModelArrayList.get(i).getBranchName());

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        viewid = View.generateViewId();
                    }
                    // tagArrayList.add(colorsDataModelArrayList.get(i).getBranchName());
                    textView.setId(i);
                    tagArrayList.add(i);
                    /***End charan**/
                    Utils.setTypefaces(IvokoApplication.robotoTypeface, textView);
                    branches.addView(textView);
                    final int finalI = i;
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            for (int i = 0; i < tagArrayList.size(); i++) {
                                if (tagArrayList.get(i) == v.getId()) {
                                    compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.white));
                                    textView.setTextColor(getResources().getColor(R.color.white));
                                    GradientDrawable titleDrawable = (GradientDrawable) textView.getBackground();
                                    titleDrawable.setColor(Color.parseColor(colorsDataModelArrayList.get(i).getColorName()));
                                } else {
//                                    compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.cal_selected_color));
                                    TextView otherTextview = (TextView) branches.findViewById(tagArrayList.get(i));
                                    otherTextview.setTextColor(Color.parseColor(colorsDataModelArrayList.get(i).getColorName()));
                                    GradientDrawable titleDrawable = (GradientDrawable) otherTextview.getBackground();
                                    titleDrawable.setStroke(5, Color.parseColor(colorsDataModelArrayList.get(i).getColorName()));
                                    titleDrawable.setColor(getResources().getColor(R.color.white));
                                }
                            }
                            showActivityList(textView.getText().toString());
                        }
                    });
                }
            } else {
                branches.setVisibility(View.GONE);
            }
        } else {
            branches.setVisibility(View.GONE);
        }
        ArrayList<PlannerModel> finalPlannerModelList = new ArrayList();
        compactCalendarView.removeAllEvents();
        plannerModelList = PlannerMgr.getInstance(CPlannerActivity.this).getPlannerList(userid);
        for (int i = 0; i < plannerModelList.size(); i++) {
            finalPlannerModelList.add(plannerModelList.get(i));
            /*if (plannerModelList.get(i).getPlanstatus() != null) {
                if (plannerModelList.get(i).getPlanstatus() == 1) {
                    finalPlannerModelList.add(plannerModelList.get(i));
                }
            }*/
        }
        Calendar calendar = Calendar.getInstance();
        if (finalPlannerModelList.size() != 0) {
            ArrayList<PlannerModel> plannerModelArrayList = new ArrayList<>();
            for (int i = 0; i < finalPlannerModelList.size(); i++) {
                if (Utils.isNotNullAndNotEmpty(colorsDataModelArrayList))
                    for (int j = 0; j < colorsDataModelArrayList.size(); j++) {
                        if (Utils.isNotNullAndNotEmpty(finalPlannerModelList.get(i).getCustomername())) {
                            if (finalPlannerModelList.get(i).getCustomername().equalsIgnoreCase(colorsDataModelArrayList.get(j).getBranchName())) {
                                finalPlannerModelList.get(i).setColor(colorsDataModelArrayList.get(j).getColorName());
                                plannerModelArrayList.add(finalPlannerModelList.get(i));
                            }


                        }
                    }
            }
            if (plannerModelArrayList.size() != 0) {
                for (int i = 0; i < plannerModelArrayList.size(); i++) {
                    calendar.set(plannerModelArrayList.get(i).getScheduleyear(), plannerModelArrayList.get(i).getSchedulemonth() - 1, plannerModelArrayList.get(i).getScheduleday());
                    Event event = new Event(Color.parseColor(plannerModelArrayList.get(i).getColor()), calendar.getTimeInMillis());
                    compactCalendarView.addEvent(event);
                }
            }
        }
    }

    private void logEventsByMonth(CompactCalendarView compactCalendarView) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        currentCalender.set(Calendar.MONTH, Calendar.AUGUST);
        List<String> dates = new ArrayList<>();
        for (Event e : compactCalendarView.getEventsForMonth(new Date())) {
            dates.add(dateFormatForDisplaying.format(e.getTimeInMillis()));
        }
    }

    public void showActivityList(String branchname) {
        plannerModelList.clear();
        planRecyclerView.setAdapter(null);
        ArrayList<PlannerModel> plannerModelArrayList = new ArrayList<>();
        SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MM-yyyy", Locale.getDefault());
        String[] date = dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()).split("-");
        plannerModelList = PlannerMgr.getInstance(CPlannerActivity.this).getPlannerListByBranches(userid,
                branchname, Integer.parseInt(date[0]), Integer.parseInt(date[1]));
        for (int i = 0; i < plannerModelList.size(); i++) {
            plannerModelArrayList.add(plannerModelList.get(i));
        }
        if (plannerModelArrayList.size() > 0) {
            nomessageview.setVisibility(View.INVISIBLE);
            if (msgListProgressBar.VISIBLE == 0) {
                msgListProgressBar.setVisibility(View.INVISIBLE);
            }
            branchesPlannerAdapter = new BranchesPlannerAdapter(CPlannerActivity.this, plannerModelArrayList);
            planRecyclerView.setAdapter(branchesPlannerAdapter);
            planRecyclerView.setLayoutManager(new LinearLayoutManager(CPlannerActivity.this));
        } else {
            nomessageview.setText("No Plans");
            nomessageview.setVisibility(View.GONE);
        }
    }

    public void showlist(int day, int monthno, int year) {
        plannerModelList.clear();
        planRecyclerView.setAdapter(null);






        ArrayList<PlannerModel> plannerModelArrayList = new ArrayList<>();
        plannerModelList = PlannerMgr.getInstance(CPlannerActivity.this).getPlannerListByDate(userid, day, monthno, year);
        for (int i = 0; i < plannerModelList.size(); i++) {
            plannerModelArrayList.add(plannerModelList.get(i));
        }
        //if (plannerModelArrayList.size() > 0) {
            nomessageview.setVisibility(View.INVISIBLE);
            if (msgListProgressBar.VISIBLE == 0) {
                msgListProgressBar.setVisibility(View.INVISIBLE);
            }

        PlannerModel pm = new PlannerModel();
        pm.setActivitytype("abc");

        plannerModelArrayList.add(pm);

            calenderPlannerAdapter = new DummyAdapter(CPlannerActivity.this, plannerModelArrayList);
            planRecyclerView.setAdapter(calenderPlannerAdapter);
            planRecyclerView.setLayoutManager(new LinearLayoutManager(CPlannerActivity.this));
        //} else {
         //   nomessageview.setText("No Plans");
         //   nomessageview.setVisibility(View.GONE);
        //}
    }



}
