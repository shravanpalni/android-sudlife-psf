package com.outwork.sudlife.psf.targets.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.targets.models.TargetSummaryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panch on 2/23/2017.
 */

public class TargetsSummaryAdapter extends RecyclerView.Adapter<TargetsSummaryAdapter.TargetViewHolder> {
    private Context context;
    int layoutSize;
    private final LayoutInflater mInflater;
    private List<TargetSummaryModel> targetSummaryModelsList = new ArrayList<>();

    public static class TargetViewHolder extends RecyclerView.ViewHolder {
        public TextView tvBranchName;
        public CardView card_view;
        public TextView tvTotal;
        public TextView tvAchieved, planned;
        public TextView tvsummaryName;
        public TextView tvOverallTotal;
        public TextView tvOverAllAchieved;
        public RecyclerView lvData;
        public RelativeLayout rlCardview;

        public TargetViewHolder(View v) {
            super(v);
            tvsummaryName = (TextView) v.findViewById(R.id.tv_summary_name);
            //     tvBranchName = (TextView) v.findViewById(R.id.tv_branchname);
            tvOverallTotal = (TextView) v.findViewById(R.id.tv_overall_total);
            tvOverAllAchieved = (TextView) v.findViewById(R.id.tv_overall_achieved);
            planned = (TextView) v.findViewById(R.id.planned);
            //    tvAchieved = (TextView) v.findViewById(R.id.tv_achieved);
            lvData = (RecyclerView) v.findViewById(R.id.lv_data);
            card_view = (CardView) v.findViewById(R.id.card_view);
            rlCardview = (RelativeLayout) v.findViewById(R.id.rl_cardview);

        }
    }

    public TargetsSummaryAdapter(Context context, List<TargetSummaryModel> targetSummaryModelsList, int layoutSize) {
        this.context = context;
        this.targetSummaryModelsList = targetSummaryModelsList;
        this.layoutSize = layoutSize;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TargetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_for_targets_view, parent, false);
        return new TargetViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TargetViewHolder holder, final int position) {
        //  holder.lvData.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, targetSummaryModelsList.size() * layoutSize));
        holder.tvOverallTotal.setText(targetSummaryModelsList.get(position).getOverallTotal());
        holder.tvsummaryName.setText(targetSummaryModelsList.get(position).getSummaryName());
        if (targetSummaryModelsList.get(position).getSummaryName().equalsIgnoreCase("FTM")) {
            holder.tvOverAllAchieved.setText(targetSummaryModelsList.get(position).getAchievedBusiness());
        } else if (targetSummaryModelsList.get(position).getSummaryName().equalsIgnoreCase("Connections")) {
            holder.tvOverAllAchieved.setText(targetSummaryModelsList.get(position).getAchievedConnection());
            holder.planned.setText("Expected");
        } else if (targetSummaryModelsList.get(position).getSummaryName().equalsIgnoreCase("Leads")) {
            holder.tvOverAllAchieved.setText(targetSummaryModelsList.get(position).getOverallAchieved());
            holder.planned.setText("Expected");
        } else if (targetSummaryModelsList.get(position).getSummaryName().equalsIgnoreCase("Business")) {
            holder.tvOverAllAchieved.setText(targetSummaryModelsList.get(position).getOverallAchieved());
            holder.planned.setText("Expected");
        } else if (targetSummaryModelsList.get(position).getSummaryName().equalsIgnoreCase("Activities")) {
            holder.tvOverAllAchieved.setText(targetSummaryModelsList.get(position).getOverallAchieved());
        } else if (targetSummaryModelsList.get(position).getSummaryName().equalsIgnoreCase("Visits")) {
            holder.tvOverAllAchieved.setText(targetSummaryModelsList.get(position).getOverallAchieved());
        } else {
            holder.tvOverAllAchieved.setText("0");
        }
        ListItemAdapter listItemAdapter = new ListItemAdapter(targetSummaryModelsList.get(position).getTargetSummaryModelArrayList(), context);
        holder.lvData.setAdapter(listItemAdapter);
        holder.lvData.setLayoutManager(new LinearLayoutManager(context));

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return targetSummaryModelsList.size();
    }

}


   /* public class ListItemAdapter extends BaseAdapter {
        List<TargetSummaryModel> targetSummaryModelList;

        public ListItemAdapter(List<TargetSummaryModel> targetSummaryModelList) {
            this.targetSummaryModelList = targetSummaryModelList;
        }

        @Override
        public int getCount() {
            return targetSummaryModelList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View itemView = LayoutInflater.from(context)
                    .inflate(R.layout.list_item_in_listview, viewGroup, false);

            TextView tvBranchName = (TextView) itemView.findViewById(R.id.tv_branchname);
            TextView tvTotal = (TextView) itemView.findViewById(R.id.tv_total);
            TextView tvAchieved = (TextView) itemView.findViewById(R.id.tv_achieved);

            tvBranchName.setText(targetSummaryModelList.get(i).getBranchName());
            tvTotal.setText(targetSummaryModelList.get(i).getTotalValue());
            if (targetSummaryModelList.get(i).getAchievedValue() != null) {
                tvAchieved.setText(targetSummaryModelList.get(i).getAchievedValue());
            } else {
                tvAchieved.setText("0");

            }
            return itemView;
        }*/
