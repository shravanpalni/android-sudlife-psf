package com.outwork.sudlife.psf.planner.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Habi on 10-04-2018.
 */

public class SupervisorModel implements Serializable {

    @SerializedName("userid")
    @Expose
    private String userid;

    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("supervisorid")
    @Expose
    private String supervisorid;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("employeecode")
    @Expose
    private String employeecode;
    private int id;

    public String getSupervisorid() {
        return supervisorid;
    }

    public void setSupervisorid(String supervisorid) {
        this.supervisorid = supervisorid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmployeecode() {
        return employeecode;
    }

    public void setEmployeecode(String employeecode) {
        this.employeecode = employeecode;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
