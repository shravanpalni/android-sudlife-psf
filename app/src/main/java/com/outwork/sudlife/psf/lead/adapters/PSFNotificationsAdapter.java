package com.outwork.sudlife.psf.lead.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.gson.Gson;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.activities.PSFLeadListActivity;
import com.outwork.sudlife.psf.lead.activities.PSFViewLeadActivity;
import com.outwork.sudlife.psf.lead.activities.ViewActivityLeadActivity;
import com.outwork.sudlife.psf.lead.activities.ViewLeadActivity;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;
import com.outwork.sudlife.psf.planner.activities.OpportunityViewActivity;
import com.outwork.sudlife.psf.planner.activities.ViewActivity;
import com.outwork.sudlife.psf.planner.activities.ViewplannedActivity;
import com.outwork.sudlife.psf.planner.adapter.PlannerAdapter;
import com.outwork.sudlife.psf.planner.models.Customerobject;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shravanch on 27-12-2019.
 */

public class PSFNotificationsAdapter extends RecyclerView.Adapter<PSFNotificationsAdapter.TaskViewHolder> {
    private Context context;
    private final LayoutInflater mInflater;
    private List<PSFNotificationsModel> notificationsList = new ArrayList<>();
    private String userid;
    //private List<PSFNotificationsModel> plannerModelList = new ArrayList<>();
    //private String type;

    public static class TaskViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public TextView name;
        private CardView card_view;
        public TextView line2, status, datetime;



        public TaskViewHolder(View v) {
            super(v);
            picture = (ImageView) v.findViewById(R.id.profileImage);
            name = (TextView) v.findViewById(R.id.contactname);
            line2 = (TextView) v.findViewById(R.id.contactline2);
            status = (TextView) v.findViewById(R.id.status);
            card_view = (CardView) v.findViewById(R.id.card_view);
            datetime = v.findViewById(R.id.datetime);

            line2.setVisibility(View.GONE);
            status.setVisibility(View.GONE);
            datetime.setVisibility(View.VISIBLE);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, name);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, line2, status, datetime);
        }
    }

    public PSFNotificationsAdapter(Context context, List<PSFNotificationsModel> notificationModelList,String userid) {
        this.context = context;
        this.notificationsList = notificationModelList;
        this.userid = userid;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public PSFNotificationsAdapter.TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.psf_notification_item, parent, false);
        return new PSFNotificationsAdapter.TaskViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PSFNotificationsAdapter.TaskViewHolder holder, final int position) {

        final PSFNotificationsModel notificationsModel = (PSFNotificationsModel) this.notificationsList.get(position);

        if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifytext())) {

            holder.name.setText(notificationsModel.getNotifytext());

        }

        if (Utils.isNotNullAndNotEmpty(notificationsModel.getCreateddate())) {

            holder.datetime.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(notificationsModel.getCreateddate()), "dd/MM/yyyy hh:mm a"));

        }

        if (Utils.isNotNullAndNotEmpty(notificationsModel.getType())) {


            if (notificationsModel.getType().equalsIgnoreCase("Birthday")) {
                holder.picture.setImageResource(R.mipmap.psf_birthday);
            } else if (notificationsModel.getType().equalsIgnoreCase("Appointment")) {
                holder.picture.setImageResource(R.mipmap.psf_meeting);
            } else if (notificationsModel.getType().equalsIgnoreCase("Allocate Lead")) {
                holder.picture.setImageResource(R.mipmap.psf_allocate_lead);
            }else {
                holder.picture.setImageResource(R.mipmap.ic_message);
            }


        }


        //final PlannerModel plannerModel = (PlannerModel) this.plannerModelList.get(position);


        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<PSFLeadModel> list = LeadMgr.getInstance(context).getPSFLeadsBasedOnLeadId(userid,notificationsModel.getObjectid());
                if (list.size() > 0){

                        for (PSFLeadModel dto : list){

                            Intent intent = new Intent(context, PSFViewLeadActivity.class);
                            intent.putExtra("leadobj", new Gson().toJson(dto));
                            context.startActivity(intent);

                        }




                }else {

                    Toast.makeText(context, "Data not available", Toast.LENGTH_SHORT).show();
                }



            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return notificationsList.size();
    }
}
