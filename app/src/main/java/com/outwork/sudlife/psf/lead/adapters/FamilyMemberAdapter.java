package com.outwork.sudlife.psf.lead.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.notifications.GlobalData;
import com.outwork.sudlife.psf.lead.activities.CreateFamilyMemberActivity;
import com.outwork.sudlife.psf.lead.model.LeadModel;

import java.util.List;

public class FamilyMemberAdapter extends RecyclerView.Adapter<FamilyMemberAdapter.TargetViewHolder2> {
    private Context context;
    int layoutSize;
    private final LayoutInflater mInflater;
    private List<LeadModel> leadModels;

    public static class TargetViewHolder2 extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvAge;
        TextView tvRealtion;
        TextView tvWorking;
        TextView tvOccupation;
        CardView cardview;

        public TargetViewHolder2(View v) {
            super(v);
            tvName = (TextView) itemView.findViewById(R.id.tv_fm_name);
            tvAge = (TextView) itemView.findViewById(R.id.tv_fm_age);
            tvRealtion = (TextView) itemView.findViewById(R.id.tv_fm_relation);
            tvWorking = (TextView) itemView.findViewById(R.id.tv_lead_status);
            tvOccupation = (TextView) itemView.findViewById(R.id.tv_occupation);
            cardview = (CardView) itemView.findViewById(R.id.cv_lead);
        }
    }

    public FamilyMemberAdapter(List<LeadModel> leadModels, Context context) {
        this.context = context;
        this.leadModels = leadModels;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TargetViewHolder2 onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.family_member_list_item, parent, false);
        return new TargetViewHolder2(itemView);
    }

    @Override
    public void onBindViewHolder(TargetViewHolder2 holder, final int position) {

        final LeadModel dto = (LeadModel) this.leadModels.get(position);

//        holder.tvName.setText("Name  :" + dto.getName());
//        holder.tvAge.setText("Age   :" + String.valueOf(dto.getAge()));
//        holder.tvRealtion.setText("Relation  :" + dto.getRelation());
//
//        if (dto.isIsworking()) {
//            holder.tvWorking.setText("Working  :" + "Yes");
//            holder.tvOccupation.setText("Occupation  :" + dto.getOccupation());
//        } else {
//            holder.tvWorking.setText("Working  :" + "No");
//            holder.tvOccupation.setText("Occupation  :No");
//
//        }

        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalData.getInstance().setLeadModelForFm(dto);
                Intent intent = new Intent(context, CreateFamilyMemberActivity.class);
                intent.putExtra("type", "update");
                context.startActivity(intent);
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return leadModels.size();
    }
}