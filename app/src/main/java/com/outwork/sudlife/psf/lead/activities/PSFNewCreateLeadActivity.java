package com.outwork.sudlife.psf.lead.activities;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.activities.ListCustomerActivity;
import com.outwork.sudlife.psf.branches.models.BranchesModel;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.adapters.PSFLeadIdAutoCompleteAdapter;
import com.outwork.sudlife.psf.lead.db.LeadDao;
import com.outwork.sudlife.psf.lead.model.CountryCodeModel;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.localbase.ProductDataMgr;
import com.outwork.sudlife.psf.localbase.ProductMasterDto;
import com.outwork.sudlife.psf.localbase.StaticDataDto;
import com.outwork.sudlife.psf.localbase.StaticDataMgr;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.activities.DashBoardActivity;
import com.outwork.sudlife.psf.ui.activities.NotificationsListActivity;
import com.outwork.sudlife.psf.utilities.CSVFile;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.outwork.sudlife.psf.utilities.Utils.isValidPhoneNew;
import static com.outwork.sudlife.psf.utilities.Utils.isValidPhoneNri;

/**
 * Created by shravanch on 11-12-2019.
 */

public class PSFNewCreateLeadActivity extends BaseActivity {
    private BottomSheetDialog bottomSheetDialog;
    private ListView lvBottom;
    AlertDialog.Builder alertDialog;
    private List<String> myList;
    private TextView toolbar_title;
    private TextInputLayout tiLayoutothers, tiLayoutSubStatus;
    private CheckBox chk_NRI;
    private EditText etfName, et_campaigndate, etlName, etLeadStage, etContactNumber, etEmailId, etAlternativeContactNo, etAge, etExistingCustomer, et_policyNo, etProductName, etProductone, etProducttwo, etProductNameOther, etAddress,
            etCity, etPincode, etOccupation, etSourceOfLead, etOther, etBank, etBranchCode, etNoOfFamilyNumbers, etBranchName, etZccSupportRequried, etPreferredLang,
            etPreferredDate, etNotes, etStatus, etEducation, etSubStatus, etIncome, etPremiumExpected, etNextFollowupDate, etFirstAppointmentDate, etConversionPropensity,
            etProposalNumber, etLeadCreatedDate, etMarriedStatus, et_custname, etDOB, et_foccupation, et_fname, et_fpsfzone, et_fsud_branch_office, et_reference, et_self,
            et_feducation, et_fannual_income, et_landline_number, et_email_id, et_alt_contactno, et_source_lead, et_leadcode, et_date_time, et_meeting_address, et_remarks, et_psflead_status;
    private ArrayList<String> stringArrayList;
    private BranchesModel branchesModel = new BranchesModel();
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;
    //private CountryCodeAdapter itemArrayAdapter;
    private CountryCodeModel countryCodeModel;
    List<CountryCodeModel> countryCodeModelslist = new ArrayList<CountryCodeModel>();
    private AutoCompleteTextView fCountrycode;
    //private AppCompatSpinner fCountrycode;
    //private EditText fCountrycode;
    List<String> countryNamelist;
    private ArrayAdapter<String> custadapter;
    // private ArrayAdapter<CountryCodeModel> custadapter;
    private String countryName = "";
    private String countryCode = "";
    private boolean countryListValid = false;
    private boolean isFinishBranches = false;
    private boolean isfromCalendar = false;
    final Calendar myCalendar = Calendar.getInstance();
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private PSFLeadModel psfLeadModel;
    private AppCompatAutoCompleteTextView referencemobileno;
    TextView textCartItemCount;
    int mCartItemCount;


    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                etNextFollowupDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etNextFollowupDate.setText(visitdt);
                etNextFollowupDate.setEnabled(true);
            }
            if (customSelector == 1) {
                etFirstAppointmentDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etFirstAppointmentDate.setText(visitdt);
                etFirstAppointmentDate.setEnabled(true);
            }
            if (customSelector == 3) {
                etPreferredDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etPreferredDate.setText(visitdt);
                etPreferredDate.setEnabled(true);
            }

            if (customSelector == 4) {
                et_date_time.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                et_date_time.setText(visitdt);
                et_date_time.setEnabled(true);
            }


            if (customSelector == 5) {
                etDOB.setText(TimeUtils.getFormattedYearMonthDate(date));
                String visitdt = TimeUtils.getFormattedYearMonthDate(date);
                etDOB.setText(visitdt);
                etDOB.setEnabled(true);
            }


        }

        @Override
        public void onDateTimeCancel() {
            etNextFollowupDate.setEnabled(true);
            etFirstAppointmentDate.setEnabled(true);
            etPreferredDate.setEnabled(true);
            et_date_time.setEnabled(true);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_create_new_lead);
        mgr = LocalBroadcastManager.getInstance(this);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("customer")) {
            branchesModel = new Gson().fromJson(getIntent().getStringExtra("customer"), BranchesModel.class);
        }
        if (SharedPreferenceManager.getInstance().getString(Constants.STAGES_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                OpportunityIntentService.insertOpportunityStages(PSFNewCreateLeadActivity.this);
            }
        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("finishBranches")) {
            isFinishBranches = getIntent().getExtras().getBoolean("finishBranches");


        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("fromCalendar")) {
            isfromCalendar = getIntent().getExtras().getBoolean("fromCalendar");


        }

        initilizeViews();
        initToolBar();
        setListener();
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoMediumTypeface,
                (TextView) findViewById(R.id.personalinfo_lbl), (TextView) findViewById(R.id.pastinfo_lbl),
                (TextView) findViewById(R.id.otherinfo_lbl), (TextView) findViewById(R.id.bankinfo_lbl),
                (TextView) findViewById(R.id.zccinfo_lbl), (TextView) findViewById(R.id.updateinfo_lbl));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("customer_broadcast"));
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("oppstage_broadcast"));

        if (isNetworkAvailable()) {
            updateNotification();
        } else {
            List<PSFNotificationsModel> finalList = new ArrayList<>();
            List<PSFNotificationsModel> psfNotificationsModelList = LeadMgr.getInstance(PSFNewCreateLeadActivity.this).getPSFNotificationsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));

            if (psfNotificationsModelList.size() > 0) {

                for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                    if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                        finalList.add(notificationsModel);
                    }

                }

                mCartItemCount = finalList.size();
                setupBadge();
            } else {
                mCartItemCount = 0;
                setupBadge();
            }

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.order_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            //createLead();

            createPsfLead();
        }
        return super.onOptionsItemSelected(item);
    }


    private void setListener() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                final List<BranchesModel> customerModelList = BranchesMgr.getInstance(CreateNewLeadActivity.this).getonlineCustomerList(groupId, userid, "");
//                if (customerModelList.size() != 0) {
//                    List<BranchesModel> customerModelArrayList = new ArrayList<>();
//                    for (int i = 0; i < customerModelList.size(); i++) {
//                        customerModelArrayList.add(customerModelList.get(i));
//                    }
//                }
            }
        };


        et_fpsfzone.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                et_fpsfzone.setFocusableInTouchMode(false);
                String[] listItems = {"PSF - East", "PSF - North", "PSF - South", "PSF - West 1", "PSF - West 2"};

                AlertDialog.Builder builder = new AlertDialog.Builder(PSFNewCreateLeadActivity.this);
                builder.setTitle("Select PSFZone");

                builder.setItems(listItems, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(PSFNewCreateLeadActivity.this, "Position: " + which + " Value: " + listItems[which], Toast.LENGTH_LONG).show();

                        et_fpsfzone.setText(listItems[which]);
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });



       /* et_cutomer_salutation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_cutomer_salutation.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Mr");
                stringArrayList.add("Mrs");
                stringArrayList.add("Miss");
                stringArrayList.add("Dr");
                bottomSheetDialogforExistingCust(((EditText) findViewById(R.id.et_cutomer_salutation)), stringArrayList, "Customer Salutation");

            }
        });*/

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


       /* DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);







                                updateLabel2();
            }

        };*/


        etDOB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                etDOB.setFocusableInTouchMode(false);


                DatePickerDialog datePickerDialog = new DatePickerDialog(PSFNewCreateLeadActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                //following line to restrict future date selection
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();


               /* new DatePickerDialog(PSFNewCreateLeadActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();*/
            }
        });



       /* etDOB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                etDOB.setFocusableInTouchMode(false);



                *//*new DatePickerDialog(PSFNewCreateLeadActivity.this, date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();*//*

                etDOB.setEnabled(false);
                etDOB.setFocusableInTouchMode(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 10);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(5)
                        .build()
                        .show();
            }
        });*/


        et_date_time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                et_date_time.setFocusableInTouchMode(false);



                /*new DatePickerDialog(PSFNewCreateLeadActivity.this, date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();*/

                et_date_time.setEnabled(false);
                et_date_time.setFocusableInTouchMode(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 10);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(4)
                        .build()
                        .show();
            }
        });


        //int selectedId = radioGroup.getCheckedRadioButtonId();
        //radioButton = (RadioButton) findViewById(selectedId);
        //showToast( radioButton.getText().toString());

        etExistingCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etExistingCustomer.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Yes");
                stringArrayList.add("No");
                bottomSheetDialogforExistingCust(((EditText) findViewById(R.id.et_existing_sud_ife_customer)), stringArrayList, "Are You Existing SUD Life customer?");

            }
        });


     /*   et_reference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_reference.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Spouse");
                stringArrayList.add("Children");
                stringArrayList.add("Parents");
                stringArrayList.add("Friend");
                stringArrayList.add("Relative");
                stringArrayList.add("Colleage");
                stringArrayList.add("Neighbour");
                bottomSheetDialogforExistingCust(((EditText) findViewById(R.id.et_reference)), stringArrayList, "Reference");

            }
        });*/

        referencemobileno.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (count == 0){
                    et_leadcode.setText("");
                }

                Log.i("shravan",s.toString());

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.i("shravan after ",s.toString());
            }
        });




        referencemobileno.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (referencemobileno.getVisibility() == View.VISIBLE) {
                    PSFLeadModel contactModel = (PSFLeadModel) parent.getItemAtPosition(position);
                   /* if (Utils.isNotNullAndNotEmpty(contactModel.getContactid())) {
                        objectId = contactModel.getContactid();
                    } else {
                        localcontactid = contactModel.getId();
                    }
                    objectType = "contact";

                    TaskModel.Contact contact = new TaskModel.Contact(contactModel.getEmail(), contactModel.getPhoneno(), contactModel.getCdisplayname(), contactModel.getCustomername(), contactModel.getAddress(), contactModel.getCustomerid());
                    dto.setObjectinfo(new Gson().toJson(contact));
                    customerid = contactModel.getCustomerid();*/
                    referencemobileno.setText(contactModel.getPolicyHolderFirstName() + " - " + contactModel.getContactNo());
                    et_leadcode.setText(contactModel.getLeadCode());
                }
            }
        });


        et_reference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                et_reference.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Spouse");
                stringArrayList.add("Children");
                stringArrayList.add("Parents");
                stringArrayList.add("Friend");
                stringArrayList.add("Relative");
                stringArrayList.add("Colleage");
                stringArrayList.add("Neighbour");
                stringArrayList.add("Self");
                bottomSheetDialogforExistingCust(((EditText) findViewById(R.id.et_reference)), stringArrayList, "Select Reference Type");

            }
        });

        et_self.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_self.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Spouse");
                stringArrayList.add("Children");
                stringArrayList.add("Parents");
                stringArrayList.add("Friend");
                bottomSheetDialogforExistingCust(((EditText) findViewById(R.id.et_self)), stringArrayList, "Self");

            }
        });


        etMarriedStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Yes");
                stringArrayList.add("No");
                bottomSheetDialog(((EditText) findViewById(R.id.et_married_status)), stringArrayList, "Married (Yes/No)?");
            }
        });
        etSourceOfLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<StaticDataDto> staticList = new StaticDataMgr(PSFNewCreateLeadActivity.this).getStaticList(groupId, "leadsource");
                if (staticList.size() != 0) {
                    etSourceOfLead.setFocusableInTouchMode(false);
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i < staticList.size(); i++) {
                        strings.add(staticList.get(i).getName());
                    }
                    bottomSheetDialog(((EditText) findViewById(R.id.et_source_of_lead)), strings, "Select Source Of Lead");
                }
            }
        });
        etNoOfFamilyNumbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringArrayList = new ArrayList<>();
                for (int i = 1; i <= 10; i++) {
                    stringArrayList.add(String.valueOf(i));
                }
                bottomSheetDialog(((EditText) findViewById(R.id.et_no_of_family_members)), stringArrayList, "Select No Of Family Members");
            }
        });
        etZccSupportRequried.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etZccSupportRequried.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Yes");
                stringArrayList.add("No");
                bottomSheetDialogforExistingCust(((EditText) findViewById(R.id.et_zcc_support_requried)), stringArrayList, "ZCC Suport Required");
            }
        });
        etPreferredLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPreferredLang.setFocusableInTouchMode(false);
                List<StaticDataDto> staticList = new StaticDataMgr(PSFNewCreateLeadActivity.this).getStaticList(groupId, "preferredlanguage");
                if (staticList.size() != 0) {
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i < staticList.size(); i++) {
                        strings.add(staticList.get(i).getName());
                    }
                    bottomSheetDialog(((EditText) findViewById(R.id.et_preferred_language)), strings, "Select Preferred Langugae");
                }
            }
        });
        etIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<StaticDataDto> staticList = new StaticDataMgr(PSFNewCreateLeadActivity.this).getStaticList(groupId, "incomeband");
                if (staticList.size() != 0) {
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i < staticList.size(); i++) {
                        strings.add(staticList.get(i).getName());
                    }
                    bottomSheetDialog(((EditText) findViewById(R.id.et_income)), strings, "Select Income Band");

                }
            }
        });
        etNotes.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_notes) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        etNextFollowupDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNextFollowupDate.setEnabled(false);
                etNextFollowupDate.setFocusableInTouchMode(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 10);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
        etFirstAppointmentDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFirstAppointmentDate.setEnabled(false);
                etFirstAppointmentDate.setFocusableInTouchMode(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 10);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });

        etPreferredDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPreferredDate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 10);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(03)
                        .build()
                        .show();
            }
        });

        String currentDate = TimeUtils.getCurrentDate("dd/MM/yyyy hh:mm a");
        etLeadCreatedDate.setText(currentDate);
        etFirstAppointmentDate.setText(currentDate);
        etSubStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Appointment Fixed");
                stringArrayList.add("Meeting Done");
                stringArrayList.add("Converted");
                stringArrayList.add("Not Interested");
//                bottomSheetDialogForSubStatus(((EditText) findViewById(R.id.et_sub_status)), stringArrayList, "Select Sub-Status");
            }
        });
        etConversionPropensity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etConversionPropensity.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("RED");
                stringArrayList.add("AMBER");
                stringArrayList.add("GREEN");
                bottomSheetDialog(((EditText) findViewById(R.id.et_conversion_propensity)), stringArrayList, "Select Conversion propensity");
            }
        });
        etProductName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etProductName.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(PSFNewCreateLeadActivity.this).getProductList(groupId);
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(etProductName, stringArrayList, "Select Product");
                }
            }
        });
        etProductone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etProductone.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(PSFNewCreateLeadActivity.this).getProductList(groupId);
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(etProductone, stringArrayList, "Select Product");
                }
            }
        });
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Create Lead");
        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        textCartItemCount = (TextView) findViewById(R.id.notification_badge);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showToast("Notification Clicked!!!");
                Intent in = new Intent(PSFNewCreateLeadActivity.this, NotificationsListActivity.class);
                startActivity(in);
            }
        });

        setupBadge();
    }


    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void bottomSheetDialog(final EditText editText, ArrayList<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(PSFNewCreateLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(PSFNewCreateLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Others")) {
                    tiLayoutothers.setVisibility(View.VISIBLE);
                } else {
                    tiLayoutothers.setVisibility(View.GONE);
                }
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Walk-in")) {
                    showpopup("Visit");
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Lead Generation Activity")) {
                    showpopup("Activity");
                } else {
                    et_campaigndate.setText("");
                }
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Met")) {
//                    tiLayoutSubStatus.setVisibility(View.VISIBLE);
                    ((ScrollView) findViewById(R.id.scroll_view)).fullScroll(ScrollView.FOCUS_DOWN);
                } else {
                    tiLayoutSubStatus.setVisibility(View.GONE);
                }
                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    public void showpopup(String activitytype) {
        myList = new ArrayList<String>();
        myList = PlannerMgr.getInstance(PSFNewCreateLeadActivity.this).getPlannerDatesListByBranches(userid, branchesModel.getCustomername(), activitytype, Integer.parseInt(TimeUtils.getCurrentDate("MM")), Integer.parseInt(TimeUtils.getCurrentDate("yyyy")));
        myList.add(0, TimeUtils.getCurrentDate("dd/MM/yyyy"));
        alertDialog = new AlertDialog.Builder(PSFNewCreateLeadActivity.this);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PSFNewCreateLeadActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.layout_popup, null);

        alertDialog.setView(convertView);
        ListView lv = (ListView) convertView.findViewById(R.id.list);
        final AlertDialog alert = alertDialog.create();
        alert.setTitle(" Select Visit Date:");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, myList);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
                et_campaigndate.setText(arg0.getItemAtPosition(position).toString());
                alert.cancel();
            }
        });
        alert.show();
    }

    public void bottomSheetDialogforExistingCust(final EditText editText, ArrayList<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(PSFNewCreateLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(PSFNewCreateLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                if (editText.getId() == R.id.et_existing_sud_ife_customer) {
                    if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("No")) {
                        et_policyNo.setText("");
                        findViewById(R.id.et_premium_paid_lbl).setVisibility(View.GONE);
                        findViewById(R.id.et_product_name_lbl).setVisibility(View.GONE);
                        et_policyNo.setVisibility(View.GONE);
                        etProductName.setText("");
                        etProductName.setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.et_premium_paid_lbl).setVisibility(View.VISIBLE);
                        findViewById(R.id.et_product_name_lbl).setVisibility(View.VISIBLE);
                        et_policyNo.setVisibility(View.VISIBLE);
                        etProductName.setVisibility(View.VISIBLE);
                    }
                } else if (editText.getId() == R.id.et_zcc_support_requried) {
                    if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("No")) {
                        etPreferredLang.setText("");
                        findViewById(R.id.et_preferred_language_lbl).setVisibility(View.GONE);
                        findViewById(R.id.et_preferred_Date_lbl).setVisibility(View.GONE);
                        etPreferredLang.setVisibility(View.GONE);
                        etPreferredDate.setText("");
                        etPreferredDate.setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.et_preferred_language_lbl).setVisibility(View.VISIBLE);
                        findViewById(R.id.et_preferred_Date_lbl).setVisibility(View.VISIBLE);
                        etPreferredLang.setVisibility(View.VISIBLE);
                        etPreferredDate.setVisibility(View.VISIBLE);
                    }
                }
                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    public void initilizeViews() {
        tiLayoutothers = (TextInputLayout) findViewById(R.id.tilayout_other);
        tiLayoutSubStatus = (TextInputLayout) findViewById(R.id.til_sub_status);
        etfName = (EditText) findViewById(R.id.et_fname);
        etlName = (EditText) findViewById(R.id.et_lname);
        chk_NRI = (CheckBox) findViewById(R.id.checkbox_nri);
        etEmailId = (EditText) findViewById(R.id.et_email_id);
        etAlternativeContactNo = (EditText) findViewById(R.id.et_alt_contactno);
        etAge = (EditText) findViewById(R.id.et_age);
        etMarriedStatus = (EditText) findViewById(R.id.et_married_status);
        etProductone = (EditText) findViewById(R.id.et_product_one);
//        etProducttwo = (EditText) findViewById(R.id.et_product_two);
        etProductNameOther = (EditText) findViewById(R.id.et_other_product_name);
        etAddress = (EditText) findViewById(R.id.et_address);
        etCity = (EditText) findViewById(R.id.et_city);
        etPincode = (EditText) findViewById(R.id.et_pincode);
        etOccupation = (EditText) findViewById(R.id.et_occupation);
        etSourceOfLead = (EditText) findViewById(R.id.et_source_of_lead);
        etOther = (EditText) findViewById(R.id.et_other);
        etBank = (EditText) findViewById(R.id.et_bank);
        etBranchCode = (EditText) findViewById(R.id.et_branch_code);
        et_campaigndate = (EditText) findViewById(R.id.et_campaigndate);
        etNoOfFamilyNumbers = (EditText) findViewById(R.id.et_no_of_family_members);
        etBranchName = (EditText) findViewById(R.id.et_branch_name);
        etZccSupportRequried = (EditText) findViewById(R.id.et_zcc_support_requried);
        etPreferredLang = (EditText) findViewById(R.id.et_preferred_language);
        etPreferredDate = (EditText) findViewById(R.id.et_preferred_Date);
        etLeadStage = (EditText) findViewById(R.id.et_lead_status);
        etStatus = (EditText) findViewById(R.id.et_metting_status);
        etSubStatus = (EditText) findViewById(R.id.et_sub_status);
        etPremiumExpected = (EditText) findViewById(R.id.et_expected_actpremium);
        etNextFollowupDate = (EditText) findViewById(R.id.et_next_followup_date);
        etFirstAppointmentDate = (EditText) findViewById(R.id.et_first_appointment_date);
        etConversionPropensity = (EditText) findViewById(R.id.et_conversion_propensity);
        etProposalNumber = (EditText) findViewById(R.id.et_proposal_no);
        etLeadCreatedDate = (EditText) findViewById(R.id.et_lead_created_date);
        etEducation = (EditText) findViewById(R.id.et_education);
        etIncome = (EditText) findViewById(R.id.et_income);
        etNotes = (EditText) findViewById(R.id.et_notes);
        //et_cutomer_salutation = (EditText) findViewById(R.id.et_cutomer_salutation);
        et_custname = (EditText) findViewById(R.id.et_custname);
        //et_custname.requestFocus();
        etDOB = (EditText) findViewById(R.id.et_dob);
        et_feducation = (EditText) findViewById(R.id.et_feducation);
        et_foccupation = (EditText) findViewById(R.id.et_foccupation);
        et_fname = (EditText) findViewById(R.id.et_fname);
        et_fannual_income = (EditText) findViewById(R.id.et_fannual_income);
        etContactNumber = (EditText) findViewById(R.id.et_contact_number);
        //etContactNumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});
        radioGroup = (RadioGroup) findViewById(R.id.radioGrp);
        et_landline_number = (EditText) findViewById(R.id.et_landline_number);
        et_email_id = (EditText) findViewById(R.id.et_email_id);
        et_alt_contactno = (EditText) findViewById(R.id.et_alt_contactno);
        et_reference = (EditText) findViewById(R.id.et_reference);
        et_self = (EditText) findViewById(R.id.et_self);
        et_fpsfzone = (EditText) findViewById(R.id.et_fpsfzone);
        et_fsud_branch_office = (EditText) findViewById(R.id.et_fsud_branch_office);
        etExistingCustomer = (EditText) findViewById(R.id.et_existing_sud_ife_customer);
        et_policyNo = (EditText) findViewById(R.id.et_premium_paid);
        etProductName = (EditText) findViewById(R.id.et_product_name);
        et_source_lead = (EditText) findViewById(R.id.et_source_lead);
        et_source_lead.setText("Reference/Self");
        referencemobileno = (AppCompatAutoCompleteTextView) findViewById(R.id.referencemobileno);
        et_leadcode = (EditText) findViewById(R.id.et_leadcode);
        et_date_time = (EditText) findViewById(R.id.et_date_time);
        et_psflead_status = (EditText) findViewById(R.id.et_psflead_status);
        et_meeting_address = (EditText) findViewById(R.id.et_meeting_address);
        et_remarks = (EditText) findViewById(R.id.et_remarks);
        et_psflead_status.setText("Open");
        setLeadIdAdatper();


        etLeadStage.setText("Open");


    }


    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etDOB.setText(sdf.format(myCalendar.getTime()));
       /* String dateTime = (etDOB.getText().toString());
        showToast(dateTime);*/
    }


    private void checkValidations() {

        if (!Utils.isNotNullAndNotEmpty(et_custname.getText().toString())) {
            et_custname.setError("Customer Name is mandatory");
            et_custname.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etDOB.getText().toString())) {
            etDOB.setFocusableInTouchMode(true);
            etDOB.setError("Please Select DOB");
            etDOB.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(et_foccupation.getText().toString())) {
            et_foccupation.setError("Occupation is mandatory");
            et_foccupation.requestFocus();
        } else if (etContactNumber.getText().length() != 10) {
            etContactNumber.setError("Contact number must be 10 digits ");
            etContactNumber.requestFocus();
            //etContactNumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});

        } else if ( et_email_id.getText().length() >0 && !Utils.isValidEmail(et_email_id.getText().toString().trim())) {
            et_email_id.setError("Pleae Enter Valid Email");
            et_email_id.requestFocus();
        }

        /* else if (*//*et_email_id.getText().toString().length() != 0*//* (!Utils.isNotNullAndNotEmpty(et_email_id.getText().toString()) ) && (!Utils.isValidEmail(et_email_id.getText().toString().trim()))) {


            //if ((!Utils.isNotNullAndNotEmpty(et_email_id.getText().toString()) ) && !Utils.isValidEmail(et_email_id.getText().toString().trim())) {


           // if (!Utils.isNotNullAndNotEmpty(et_email_id.getText().toString()) ) {
          et_email_id.setError("Email is mandatory");
          et_email_id.requestFocus();
      }*/
        else if (etAlternativeContactNo.getText().length() > 10 || (etAlternativeContactNo.getText().length() < 10 && etAlternativeContactNo.getText().length() > 0)) {


            etAlternativeContactNo.setError("Alt Contact number must be 10 digits ");
            etAlternativeContactNo.requestFocus();


            //etContactNumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});

        }/* else if (!Utils.isNotNullAndNotEmpty(et_fpsfzone.getText().toString())) {
            et_fpsfzone.setFocusableInTouchMode(true);
            et_fpsfzone.setError("PSF Zone is mandatory");
            et_fpsfzone.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(et_fsud_branch_office.getText().toString())) {
            et_fsud_branch_office.setError("SUD Branch Office is mandatory");
            et_fsud_branch_office.requestFocus();
        }*/ else if (!Utils.isNotNullAndNotEmpty(etExistingCustomer.getText().toString())) {
            etExistingCustomer.setFocusableInTouchMode(true);
            etExistingCustomer.setError("Please Select Existing or Not");
            etExistingCustomer.requestFocus();
        } else {
            postLeadDataModel();
        }


    }


    public void createPsfLead() {


        checkValidations();


    }


    private void postLeadDataModel() {
        psfLeadModel = new PSFLeadModel();
        psfLeadModel.setUserId(userid);
        psfLeadModel.setGroupId(groupId);
        String currentDate = TimeUtils.getCurrentDate("dd/MM/yyyy hh:mm a");
        psfLeadModel.setCreatedDate(TimeUtils.convertDatetoUnix(currentDate));
        psfLeadModel.setModifiedDate(TimeUtils.convertDatetoUnix(currentDate));
        //psfLeadModel.setCreatedDate(currentDate);
        psfLeadModel.setAssignedToId(userid);
        //psfLeadModel.setPolicyHolderSalutation(et_cutomer_salutation.getText().toString());
        psfLeadModel.setPolicyHolderFirstName(et_custname.getText().toString());
        psfLeadModel.setPolicyHolderDob(etDOB.getText().toString());
        int selectedId = radioGroup.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        if (Utils.isNotNullAndNotEmpty(radioButton.getText().toString()))
            psfLeadModel.setGender(radioButton.getText().toString());
        if (Utils.isNotNullAndNotEmpty(et_feducation.getText().toString()))
            psfLeadModel.setEducation(et_feducation.getText().toString());
        psfLeadModel.setOccupation(et_foccupation.getText().toString());
        if (Utils.isNotNullAndNotEmpty(et_fannual_income.getText().toString()))
            psfLeadModel.setAnnualIncome(Integer.parseInt(et_fannual_income.getText().toString()));
        psfLeadModel.setContactNo(etContactNumber.getText().toString());

        if (Utils.isNotNullAndNotEmpty(referencemobileno.getText().toString()))
            psfLeadModel.setReferenceMobileNo(referencemobileno.getText().toString());


        if (Utils.isNotNullAndNotEmpty(et_landline_number.getText().toString()))
            psfLeadModel.setLandLineNo(et_landline_number.getText().toString());
        if (Utils.isNotNullAndNotEmpty(et_email_id.getText().toString()))
            psfLeadModel.setEmailId(et_email_id.getText().toString());
        if (Utils.isNotNullAndNotEmpty(et_alt_contactno.getText().toString()))
            psfLeadModel.setAlternateMobileNo(et_alt_contactno.getText().toString());
        psfLeadModel.setPsfZone(et_fpsfzone.getText().toString());
        psfLeadModel.setSudBranchOffice(et_fsud_branch_office.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etExistingCustomer.getText().toString()))
            if (etExistingCustomer.getText().toString().equalsIgnoreCase("yes")) {
                psfLeadModel.setIsExistingCustomer(true);
            } else {
                psfLeadModel.setIsExistingCustomer(false);

            }

        if (Utils.isNotNullAndNotEmpty(et_policyNo.getText().toString()))
            psfLeadModel.setPolicyNo((et_policyNo.getText().toString()));
        if (Utils.isNotNullAndNotEmpty(etProductName.getText().toString()))
            psfLeadModel.setProductName(etProductName.getText().toString());

       /* if (Utils.isNotNullAndNotEmpty(et_date_time.getText().toString())) {
            String dateTime = TimeUtils.getTimeStampFromDDMMYYHHMMSS(et_date_time.getText().toString());
            psfLeadModel.setProductName(dateTime);

        }*/

        psfLeadModel.setAppointmentDate(TimeUtils.convertDatetoUnix(et_date_time.getText().toString()));
        if (Utils.isNotNullAndNotEmpty(et_meeting_address.getText().toString()))
            psfLeadModel.setMeetingAddress(et_meeting_address.getText().toString());
        if (Utils.isNotNullAndNotEmpty(et_remarks.getText().toString()))
            psfLeadModel.setRemarks(et_remarks.getText().toString());

        if (Utils.isNotNullAndNotEmpty(et_reference.getText().toString()))
            psfLeadModel.setReferenceType(et_reference.getText().toString());

       /* if (Utils.isNotNullAndNotEmpty(et_source_lead.getText().toString()))
            psfLeadModel.setReferenceType(et_source_lead.getText().toString());*/

        /*if (Utils.isNotNullAndNotEmpty(et_source_lead.getText().toString()))
            psfLeadModel.setLeadSource(et_source_lead.getText().toString());*/


        if (Utils.isNotNullAndNotEmpty(et_leadcode.getText().toString()))
            psfLeadModel.setReferenceLeadCode(et_leadcode.getText().toString());

        /*if (Utils.isNotNullAndNotEmpty(et_source_lead.getText().toString()))
            psfLeadModel.setLeadSource(et_source_lead.getText().toString());*/

        //if (Utils.isNotNullAndNotEmpty(et_source_lead.getText().toString()))
        psfLeadModel.setLeadSource("Reference/Self");
        psfLeadModel.setEmpCode(SharedPreferenceManager.getInstance().getString(Constants.EMPLOYEECODE,""));

        if (Utils.isNotNullAndNotEmpty(et_psflead_status.getText().toString()))
            psfLeadModel.setLeadStage(et_psflead_status.getText().toString());

        psfLeadModel.setIsjointVisit(false);


        LeadMgr.getInstance(PSFNewCreateLeadActivity.this).insertPSFLocalLead(psfLeadModel, "offline");
        LeadIntentService.syncPSFLeadstoServer(PSFNewCreateLeadActivity.this);
        //syncLeadToServer( psfLeadModel);
        showAlert("", "Your Lead is saved successfully",
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);


    }


    private void setLeadIdAdatper() {
        PSFLeadIdAutoCompleteAdapter leadidAdapter = new PSFLeadIdAutoCompleteAdapter(this, groupId, userid);
        referencemobileno.setThreshold(1);
        referencemobileno.setAdapter(leadidAdapter);
        referencemobileno.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                referencemobileno.showDropDown();
                return false;
            }
        });
    }


    private void updateNotification() {
        //showProgressDialog("Refreshing data . . .");
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getProposalCodes = client.getPsfNotifications(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        getProposalCodes.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFNotificationsModel>>() {
                        }.getType();
                        List<PSFNotificationsModel> psfNotificationsModelList = new Gson().fromJson(response.body().getData(), listType);
                        List<PSFNotificationsModel> finalList = new ArrayList<>();
                        if (psfNotificationsModelList.size() > 0) {
                            for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                                if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                                    finalList.add(notificationsModel);
                                }

                            }

                            mCartItemCount = finalList.size();
                            setupBadge();
                            //dismissProgressDialog();

                        } else {
                            mCartItemCount = 0;
                            setupBadge();
                            //dismissProgressDialog();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                //dismissProgressDialog();
            }
        });


    }
}

