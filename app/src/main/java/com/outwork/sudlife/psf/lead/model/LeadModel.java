package com.outwork.sudlife.psf.lead.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bvlbh on 4/14/2018.
 */

public class LeadModel implements Serializable {

    private int id;
    private String network_status;
    //    @SerializedName("id")
//    @Expose
//    private Integer id;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("leadid")
    @Expose
    private String leadid;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("isnri")
    @Expose
    private String isnri;

    @SerializedName("countrycode")
    @Expose
    private String countrycode;



    @SerializedName("countryname")
    @Expose
    private String countryname;

    @SerializedName("contactno")
    @Expose
    private String contactno;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("alternatecontactno")
    @Expose
    private String alternatecontactno;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("isexistingcustomer")
    @Expose
    private Boolean isexistingcustomer;
    @SerializedName("ispremiumpaid")
    @Expose
    private Boolean ispremiumpaid;
    @SerializedName("productname")
    @Expose
    private String productname;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("occupation")
    @Expose
    private String occupation;
    @SerializedName("leadsource")
    @Expose
    private String leadsource;
    @SerializedName("otherleadsource")
    @Expose
    private String otherleadsource;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("bankbranchcode")
    @Expose
    private String bankbranchcode;
    @SerializedName("campaigndate")
    @Expose
    private String campaigndate;
    @SerializedName("familymemberscount")
    @Expose
    private Integer familymemberscount;
    @SerializedName("bankbranchname")
    @Expose
    private String bankbranchname;
    @SerializedName("zccsupportrequired")
    @Expose
    private Boolean zccsupportrequired;
    @SerializedName("preferredlanguage")
    @Expose
    private String preferredlanguage;
    @SerializedName("preferreddatetime")
    @Expose
    private Integer preferreddatetime;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("substatus")
    @Expose
    private String substatus;
    @SerializedName("expectedpremium")
    @Expose
    private String expectedpremium;
    @SerializedName("premiumpaid")
    @Expose
    private Integer premiumpaid;
    @SerializedName("nextfollowupdate")
    @Expose
    private Integer nextfollowupdate;
    @SerializedName("firstappointmentdate")
    @Expose
    private Integer firstappointmentdate;
    @SerializedName("conversionpropensity")
    @Expose
    private String conversionpropensity;
    @SerializedName("visitingdatetime")
    @Expose
    private Integer visitingdatetime;
    @SerializedName("proposalnumber")
    @Expose
    private String proposalnumber;
    @SerializedName("leadcreatedon")
    @Expose
    private Integer leadcreatedon;
    @SerializedName("createdby")
    @Expose
    private String createdby;
    @SerializedName("createdon")
    @Expose
    private Integer createdon;
    @SerializedName("modifiedby")
    @Expose
    private String modifiedby;
    @SerializedName("modifiedon")
    @Expose
    private Integer modifiedon;
    @SerializedName("isdeleted")
    @Expose
    private Boolean isdeleted;
    @SerializedName("ismarried")
    @Expose
    private Boolean ismarried;
    @SerializedName("incomeband")
    @Expose
    private String incomeband;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("iscustomercallconnected")
    @Expose
    private Boolean iscustomercallconnected;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("financialplanningfor")
    @Expose
    private String financialplanningfor;
    @SerializedName("tentaiveinvestmentyears")
    @Expose
    private Integer tentaiveinvestmentyears;
    @SerializedName("financialplanningfuturefor")
    @Expose
    private String financialplanningfuturefor;
    @SerializedName("tentativeamount")
    @Expose
    private Integer tentativeamount;
    @SerializedName("leadcode")
    @Expose
    private String leadcode;
    @SerializedName("leadstage")
    @Expose
    private String leadstage;
    @SerializedName("actualpremiumpaid")
    @Expose
    private Integer actualpremiumpaid;
    @SerializedName("product1")
    @Expose
    private String product1;
    @SerializedName("product2")
    @Expose
    private String product2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getLeadid() {
        return leadid;
    }

    public void setLeadid(String leadid) {
        this.leadid = leadid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNetwork_status() {
        return network_status;
    }

    public void setNetwork_status(String network_status) {
        this.network_status = network_status;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlternatecontactno() {
        return alternatecontactno;
    }

    public void setAlternatecontactno(String alternatecontactno) {
        this.alternatecontactno = alternatecontactno;
    }

    public String getCampaigndate() {
        return campaigndate;
    }

    public void setCampaigndate(String campaigndate) {
        this.campaigndate = campaigndate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getIsexistingcustomer() {
        return isexistingcustomer;
    }

    public void setIsexistingcustomer(Boolean isexistingcustomer) {
        this.isexistingcustomer = isexistingcustomer;
    }

    public Boolean getIspremiumpaid() {
        return ispremiumpaid;
    }

    public void setIspremiumpaid(Boolean ispremiumpaid) {
        this.ispremiumpaid = ispremiumpaid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {   return lastname;   }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getIsnri() {   return isnri;   }

    public void setIsnri(String isnri) {
        this.isnri = isnri;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getCountryname() {
        return countryname;
    }

    public void setCountryname(String countryname) {
        this.countryname = countryname;
    }

    public String getLeadsource() {
        return leadsource;
    }

    public void setLeadsource(String leadsource) {
        this.leadsource = leadsource;
    }

    public String getOtherleadsource() {
        return otherleadsource;
    }

    public void setOtherleadsource(String otherleadsource) {
        this.otherleadsource = otherleadsource;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankbranchcode() {
        return bankbranchcode;
    }

    public void setBankbranchcode(String bankbranchcode) {
        this.bankbranchcode = bankbranchcode;
    }

    public Integer getFamilymemberscount() {
        return familymemberscount;
    }

    public void setFamilymemberscount(Integer familymemberscount) {
        this.familymemberscount = familymemberscount;
    }

    public Integer getActualpremiumpaid() {
        return actualpremiumpaid;
    }

    public void setActualpremiumpaid(Integer actualpremiumpaid) {
        this.actualpremiumpaid = actualpremiumpaid;
    }


    public String getBankbranchname() {
        return bankbranchname;
    }

    public void setBankbranchname(String bankbranchname) {
        this.bankbranchname = bankbranchname;
    }

    public Boolean getZccsupportrequired() {
        return zccsupportrequired;
    }

    public void setZccsupportrequired(Boolean zccsupportrequired) {
        this.zccsupportrequired = zccsupportrequired;
    }

    public String getPreferredlanguage() {
        return preferredlanguage;
    }

    public void setPreferredlanguage(String preferredlanguage) {
        this.preferredlanguage = preferredlanguage;
    }

    public Integer getPreferreddatetime() {
        return preferreddatetime;
    }

    public void setPreferreddatetime(Integer preferreddatetime) {
        this.preferreddatetime = preferreddatetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubstatus() {
        return substatus;
    }

    public void setSubstatus(String substatus) {
        this.substatus = substatus;
    }

    public String getExpectedpremium() {
        return expectedpremium;
    }

    public void setExpectedpremium(String expectedpremium) {
        this.expectedpremium = expectedpremium;
    }

    public Integer getPremiumpaid() {
        return premiumpaid;
    }

    public void setPremiumpaid(Integer premiumpaid) {
        this.premiumpaid = premiumpaid;
    }

    public Integer getFirstappointmentdate() {
        return firstappointmentdate;
    }

    public void setFirstappointmentdate(Integer firstappointmentdate) {
        this.firstappointmentdate = firstappointmentdate;
    }

    public Integer getNextfollowupdate() {
        return nextfollowupdate;
    }

    public void setNextfollowupdate(Integer nextfollowupdate) {
        this.nextfollowupdate = nextfollowupdate;
    }

    public String getConversionpropensity() {
        return conversionpropensity;
    }

    public void setConversionpropensity(String conversionpropensity) {
        this.conversionpropensity = conversionpropensity;
    }

    public Integer getVisitingdatetime() {
        return visitingdatetime;
    }

    public void setVisitingdatetime(Integer visitingdatetime) {
        this.visitingdatetime = visitingdatetime;
    }

    public String getProposalnumber() {
        return proposalnumber;
    }

    public void setProposalnumber(String proposalnumber) {
        this.proposalnumber = proposalnumber;
    }

    public Integer getLeadcreatedon() {
        return leadcreatedon;
    }

    public void setLeadcreatedon(Integer leadcreatedon) {
        this.leadcreatedon = leadcreatedon;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Integer getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Integer createdon) {
        this.createdon = createdon;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Integer getModifiedon() {
        return modifiedon;
    }

    public void setModifiedon(Integer modifiedon) {
        this.modifiedon = modifiedon;
    }

    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public Boolean getIsmarried() {
        return ismarried;
    }

    public void setIsmarried(Boolean ismarried) {
        this.ismarried = ismarried;
    }

    public String getIncomeband() {
        return incomeband;
    }

    public void setIncomeband(String incomeband) {
        this.incomeband = incomeband;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean getIscustomercallconnected() {
        return iscustomercallconnected;
    }

    public void setIscustomercallconnected(Boolean iscustomercallconnected) {
        this.iscustomercallconnected = iscustomercallconnected;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getFinancialplanningfor() {
        return financialplanningfor;
    }

    public void setFinancialplanningfor(String financialplanningfor) {
        this.financialplanningfor = financialplanningfor;
    }

    public Integer getTentaiveinvestmentyears() {
        return tentaiveinvestmentyears;
    }

    public void setTentaiveinvestmentyears(Integer tentaiveinvestmentyears) {
        this.tentaiveinvestmentyears = tentaiveinvestmentyears;
    }

    public String getFinancialplanningfuturefor() {
        return financialplanningfuturefor;
    }

    public void setFinancialplanningfuturefor(String financialplanningfuturefor) {
        this.financialplanningfuturefor = financialplanningfuturefor;
    }

    public Integer getTentativeamount() {
        return tentativeamount;
    }

    public void setTentativeamount(Integer tentativeamount) {
        this.tentativeamount = tentativeamount;
    }

    public String getLeadcode() {
        return leadcode;
    }

    public void setLeadcode(String leadcode) {
        this.leadcode = leadcode;
    }

    public String getLeadstage() {
        return leadstage;
    }

    public void setLeadstage(String leadstage) {
        this.leadstage = leadstage;
    }

    public String getProduct1() {
        return product1;
    }

    public void setProduct1(String product1) {
        this.product1 = product1;
    }

    public String getProduct2() {
        return product2;
    }

    public void setProduct2(String product2) {
        this.product2 = product2;
    }
}
