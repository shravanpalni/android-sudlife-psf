package com.outwork.sudlife.psf.planner;

import android.content.Context;

import com.outwork.sudlife.psf.planner.models.SupervisorModel;

import java.util.List;


public class SupervisorMgr {

    private static Context context;
    private static SupervisorDao supervisorDao;

    public SupervisorMgr(Context context) {
        this.context = context;
        supervisorDao = supervisorDao.getInstance(context);
    }

    private static SupervisorMgr instance;

    public static SupervisorMgr getInstance(Context context) {
        if (instance == null)
            instance = new SupervisorMgr(context);
        return instance;
    }

    public List<SupervisorModel> getsupervisorList(String userId) {
        return supervisorDao.getsupervisorList(userId);
    }

    public void insertSupevisorList(List<SupervisorModel> list, String userId, String groupid) {
        supervisorDao.insertSupevisorList(list, userId, groupid);
    }

}