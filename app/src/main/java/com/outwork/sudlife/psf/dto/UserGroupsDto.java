package com.outwork.sudlife.psf.dto;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Panch on 1/16/2016.
 */
public class UserGroupsDto {

    private String groupId;
    private String groupCode;
    private String groupName;
    private String title;
    private String groupImage;
    private String groupIcon;
    private String userId;
    private String loginId;
    private String password;
    private String userToken;
    private String lastLoginDate;
    private String userName;
    private String profileImage;
    private boolean isFeatureEnabled;
    private String isHealthCare;
    private boolean isorderproductavailable;


    public boolean isorderproductavailable() {
        return isorderproductavailable;
    }

    public void setIsorderproductavailable(boolean isorderproductavailable) {
        this.isorderproductavailable = isorderproductavailable;
    }

    public boolean isProductEnabled() {
        return isProductEnabled;
    }

    public void setProductEnabled(boolean productEnabled) {
        isProductEnabled = productEnabled;
    }

    private boolean isProductEnabled;

    public boolean isFeatureEnabled() {
        return isFeatureEnabled;
    }

    public void setFeatureEnabled(boolean featureEnabled) {
        isFeatureEnabled = featureEnabled;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    private String memberType;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String displayName) {
        this.groupName = displayName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGroupImage() {
        return groupImage;
    }

    public void setGroupImage(String groupImage) {
        this.groupImage = groupImage;
    }

    public String getGroupIcon() {
        return groupIcon;
    }

    public void setGroupIcon(String groupIcon) {
        this.groupIcon = groupIcon;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getIsHealthCare() {
        return isHealthCare;
    }

    public void setIsHealthCare(String isHealthCare) {
        this.isHealthCare = isHealthCare;
    }


    public String getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(String lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public static final String GROUPTYPE = "groupType";
    public static final String GROUPCODE = "groupcode";
    public static final String GROUPNAME = "groupname";
    public static final String GROUPID = "groupid";
    public static final String GROUPIMAGE = "groupimage";
    public static final String GROUPICON = "groupicon";
    public static final String GROUPTITLE = "Title";
    public static final String MEMBERTYPE = "objecttype";
    public static final String FEATURED = "featured";
    public static final String PRODUCT = "isproductavailable";
    public static final String HEALTHCARE = "ishealthcare";
    public static final String ORDERPRODUCT = "isorderproductavailable";


    public static UserGroupsDto parse(JSONObject groupObject) throws JSONException {

        UserGroupsDto dto = new UserGroupsDto();
        // dto.setGroupType(groupObject.getString(GROUPTYPE));
        dto.setGroupCode(groupObject.getString(GROUPCODE));
        dto.setGroupName(groupObject.getString(GROUPNAME));
        dto.setGroupId(groupObject.getString(GROUPID));
        dto.setTitle(groupObject.getString(GROUPTITLE));
        dto.setGroupImage(groupObject.getString(GROUPIMAGE));
        dto.setGroupIcon(groupObject.getString(GROUPICON));
        dto.setMemberType(groupObject.getString(MEMBERTYPE));
        dto.setIsHealthCare(groupObject.getString(HEALTHCARE));

        if (!TextUtils.isEmpty(groupObject.getString(PRODUCT)) &&
                groupObject.has(PRODUCT)) {
            if (groupObject.getString(PRODUCT).equalsIgnoreCase("1")) {
                dto.setProductEnabled(true);
            } else {
                dto.setProductEnabled(false);
            }
        }
        if (!TextUtils.isEmpty(groupObject.getString(ORDERPRODUCT)) &&
                groupObject.has(ORDERPRODUCT)) {
            if (groupObject.getString(ORDERPRODUCT).equalsIgnoreCase("1")) {
                dto.setIsorderproductavailable(true);
            } else {
                dto.setIsorderproductavailable(false);
            }
        }
        if (!TextUtils.isEmpty(groupObject.getString(FEATURED)) &&
                groupObject.has(FEATURED)) {
            if (groupObject.getString(FEATURED).equalsIgnoreCase("1")) {
                dto.setFeatureEnabled(true);
            } else {
                dto.setFeatureEnabled(false);
            }
        }
        return dto;
    }
}
