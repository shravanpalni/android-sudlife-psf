package com.outwork.sudlife.psf.lead.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.gson.Gson;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.db.LeadDao;
import com.outwork.sudlife.psf.lead.db.ProposalCodesDao;
import com.outwork.sudlife.psf.lead.model.ProposalCodesModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.localbase.ProductDataMgr;
import com.outwork.sudlife.psf.localbase.ProductMasterDto;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.opportunity.OpportunityMgr;
import com.outwork.sudlife.psf.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ViewActivityLeadActivity extends BaseActivity {

    private EditText leadStage, etNotes,
            conversion_propensity, product_one, product_two, expected_actpremium, actpremium, proposal_no, etFirstAppointmentDate, etNextFollowupDate;
    private TextView vfplandate, name, phno, branchname, toolbar_title;
    private Button submit;
    private PlannerModel plannerModel = new PlannerModel();
    private LeadModel leadModel = new LeadModel();
    private BottomSheetDialog bottomSheetDialog;
    private ListView lvBottom;
    private String pvisitdate, fappointmentDate;
    private ArrayList<String> stringArrayList;
    private List<String> stagesList = new ArrayList<>();
    private List<ProposalCodesModel> proposalCodesModelList = new ArrayList<>();
    private LocalBroadcastManager mgr;
    private BroadcastReceiver mBroadcastReceiver;


    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                etNextFollowupDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etNextFollowupDate.setText(visitdt);
                etNextFollowupDate.setEnabled(true);
                pvisitdate = TimeUtils.convertDatetoUnix(visitdt);
            }
            if (customSelector == 1) {
                etFirstAppointmentDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etFirstAppointmentDate.setText(visitdt);
                etFirstAppointmentDate.setEnabled(true);
                fappointmentDate = TimeUtils.convertDatetoUnix(visitdt);
            }
        }

        @Override
        public void onDateTimeCancel() {
            etNextFollowupDate.setEnabled(true);
            etFirstAppointmentDate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_activity_view);

        mgr = LocalBroadcastManager.getInstance(this);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("plannerModel")) {
            plannerModel = new Gson().fromJson(getIntent().getStringExtra("plannerModel"), PlannerModel.class);
        }
        if (SharedPreferenceManager.getInstance().getString(Constants.STAGES_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                OpportunityIntentService.insertOpportunityStages(ViewActivityLeadActivity.this);
            }
        }
        if (SharedPreferenceManager.getInstance().getString(Constants.PROPOSAL_CODES_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                LeadIntentService.insertProposalCodes(ViewActivityLeadActivity.this);
            }
        }
        initToolBar();
        initViews();
        setListeners();
        Utils.setTypefaces(IvokoApplication.robotoTypeface, leadStage, etNotes,
                conversion_propensity, product_one, expected_actpremium, proposal_no, etFirstAppointmentDate, etNextFollowupDate,
                vfplandate, name, phno, branchname, product_two, actpremium);
        Utils.setTypefaces(IvokoApplication.robotoLightTypeface, (TextView) findViewById(R.id.plandatelbl));
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, submit, toolbar_title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("pcodes_broadcast"));
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("oppstage_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Lead Activity");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private void initViews() {
        leadStage = (EditText) findViewById(R.id.et_lead_status);
        conversion_propensity = (EditText) findViewById(R.id.et_conversion_propensity);
        product_one = (EditText) findViewById(R.id.et_product_one);
        product_two = (EditText) findViewById(R.id.et_product_two);
        actpremium = (EditText) findViewById(R.id.et_actpremium);
        expected_actpremium = (EditText) findViewById(R.id.et_expected_actpremium);
        proposal_no = (EditText) findViewById(R.id.et_proposal_no);
        vfplandate = (TextView) findViewById(R.id.vfplandate);
        name = (TextView) findViewById(R.id.uName);
        phno = (TextView) findViewById(R.id.phno);
        branchname = (TextView) findViewById(R.id.branchname);
        etNotes = (EditText) findViewById(R.id.et_notes);
        etNextFollowupDate = (EditText) findViewById(R.id.et_next_followup_date);
        etFirstAppointmentDate = (EditText) findViewById(R.id.et_first_appointment_date);
        submit = (Button) findViewById(R.id.submit);


        if (Utils.isNotNullAndNotEmpty(plannerModel.getLeadid())) {
            leadModel = LeadMgr.getInstance(ViewActivityLeadActivity.this).getLeadbyId(plannerModel.getLeadid(), userid);
        } else if (plannerModel.getLocalleadid() != null && plannerModel.getLocalleadid() != 0) {
            leadModel = LeadMgr.getInstance(ViewActivityLeadActivity.this).getLeadbylocalId(plannerModel.getLocalleadid(), userid);
        }
        StringBuilder nameBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(leadModel.getFirstname())) {
            nameBuilder.append(leadModel.getFirstname());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLastname())) {
            if (Utils.isNotNullAndNotEmpty(nameBuilder.toString())) {
                nameBuilder.append(" " + leadModel.getLastname());
            } else {
                nameBuilder.append(leadModel.getLastname());
            }
        }
        name.setText(nameBuilder.toString());
        if (leadModel.getAge() != null) {
            nameBuilder.append("/" + leadModel.getAge());
        }
        if (Utils.isNotNullAndNotEmpty(nameBuilder.toString())) {
            phno.setText(leadModel.getContactno());
        } else {
            phno.setVisibility(View.GONE);
        }
        StringBuilder branchBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(leadModel.getBankbranchname())) {
            branchBuilder.append(leadModel.getBankbranchname());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getBank())) {
            branchBuilder.append(" " + "(" + leadModel.getBank() + ")");
        }
        branchname.setText(branchBuilder.toString());
        if (plannerModel.getScheduletime() != null) {
            if (plannerModel.getScheduletime() == 0) {
                vfplandate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(plannerModel.getScheduletime()))) {
                vfplandate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy hh:mm a"));
            } else {
                vfplandate.setVisibility(View.GONE);
            }
        } else {
            findViewById(R.id.plandate).setVisibility(View.GONE);
            vfplandate.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadstage())) {
            leadStage.setText(leadModel.getLeadstage());
            if (leadModel.getLeadstage().equalsIgnoreCase("Open")) {
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
            } else if (leadModel.getLeadstage().equalsIgnoreCase("Contacted & meeting fixed")) {
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
            } else if (leadModel.getLeadstage().equalsIgnoreCase("Proposition presented")) {
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
            } else if (leadModel.getLeadstage().equalsIgnoreCase("Not interested")) {
                findViewById(R.id.et_notes_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
            } else if (leadModel.getLeadstage().equalsIgnoreCase("Converted")) {
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.VISIBLE);
            }
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getConversionpropensity()))
            conversion_propensity.setText(leadModel.getConversionpropensity());
        if (Utils.isNotNullAndNotEmpty(leadModel.getExpectedpremium()))
            expected_actpremium.setText(leadModel.getExpectedpremium());
        if (Utils.isNotNullAndNotEmpty(leadModel.getProposalnumber()))
            proposal_no.setText(leadModel.getProposalnumber());
        if (Utils.isNotNullAndNotEmpty(leadModel.getProduct1()))
            product_one.setText(leadModel.getProduct1());
        if (Utils.isNotNullAndNotEmpty(leadModel.getRemarks())) {
            etNotes.setText(leadModel.getRemarks());
        }
    }

    private void setListeners() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                stagesList = OpportunityMgr.getInstance(ViewActivityLeadActivity.this).getOpportunitystagesnamesList(userid);
                proposalCodesModelList = ProposalCodesDao.getInstance(ViewActivityLeadActivity.this).getProposalCodesList(groupId);
            }
        };
        etNotes.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_notes) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        etNextFollowupDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNextFollowupDate.setEnabled(false);
                etNextFollowupDate.setFocusableInTouchMode(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 10);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
        etFirstAppointmentDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFirstAppointmentDate.setEnabled(false);
                etFirstAppointmentDate.setFocusableInTouchMode(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 10);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });

        leadStage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stagesList = OpportunityMgr.getInstance(ViewActivityLeadActivity.this).getOpportunitystagesnamesList(userid);
                if (stagesList.size() != 0) {
                    bottomSheetDialogForSubStatus(((EditText) findViewById(R.id.et_lead_status)), stagesList, "Select Lead Stage");
                }
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.isNotNullAndNotEmpty(leadStage.getText().toString())) {
                    leadStage.setFocusableInTouchMode(true);
                    leadStage.setError("Please Select Lead Stage");
                    leadStage.requestFocus();
                } else if (leadStage.getText().toString().equalsIgnoreCase("Converted") && !Utils.isNotNullAndNotEmpty(product_two.getText().toString())) {
                    product_two.setFocusableInTouchMode(true);
                    product_two.setError("Please Select a Product");
                    product_two.requestFocus();
                } else if (leadStage.getText().toString().equalsIgnoreCase("Converted") && !Utils.isNotNullAndNotEmpty(actpremium.getText().toString())) {
                    actpremium.setError("Please Enter Actual Premium Paid");
                    actpremium.requestFocus();
                } else if (leadStage.getText().toString().equalsIgnoreCase("Converted") && !Utils.isNotNullAndNotEmpty(proposal_no.getText().toString())) {
                    proposal_no.setError("Please Enter Proposal Number");
                    proposal_no.requestFocus();
                } else if (leadStage.getText().toString().equalsIgnoreCase("Converted") && LeadDao.getInstance(ViewActivityLeadActivity.this).isProposalNoExist(proposal_no.getText().toString(), userid)) {
                    proposal_no.setError("Proposal Number already exists");
                    proposal_no.requestFocus();
                } else if (leadStage.getText().toString().equalsIgnoreCase("Not interested") && !Utils.isNotNullAndNotEmpty(etNotes.getText().toString())) {
                    etNotes.setError("Please Specify Reason");
                    etNotes.requestFocus();
                } else if (leadStage.getText().toString().equalsIgnoreCase("Contacted & meeting fixed") && !Utils.isNotNullAndNotEmpty(etNextFollowupDate.getText().toString())) {
                    etNextFollowupDate.setFocusableInTouchMode(true);
                    etNextFollowupDate.setError("Please Select Appointment Date");
                    etNextFollowupDate.requestFocus();
                } else if (leadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(etNextFollowupDate.getText().toString())) {
                    etNextFollowupDate.setFocusableInTouchMode(true);
                    etNextFollowupDate.setError("Please Select Appointment Date");
                    etNextFollowupDate.requestFocus();
                } else if (leadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(expected_actpremium.getText().toString())) {
                    expected_actpremium.setError("Please Specify Expected Premium");
                    expected_actpremium.requestFocus();
                } else if (leadStage.getText().toString().equalsIgnoreCase("Proposition presented") && (expected_actpremium.getText().toString().equalsIgnoreCase("0") || expected_actpremium.getText().toString().length() > 7)) {
                    expected_actpremium.setError("Please Enter Valid Expected Premium");
                    expected_actpremium.requestFocus();
                } else if (leadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(product_one.getText().toString())) {
                    product_one.setFocusableInTouchMode(true);
                    product_one.setError("Please Select a Product");
                    product_one.requestFocus();
                } else if (leadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(conversion_propensity.getText().toString())) {
                    conversion_propensity.setError("Please Select Convertion Propensity");
                    conversion_propensity.requestFocus();
                } else {
                    if (leadStage.getText().toString().equalsIgnoreCase("Converted") && Utils.isNotNullAndNotEmpty(proposal_no.getText().toString())) {
                        proposalCodesModelList = ProposalCodesDao.getInstance(ViewActivityLeadActivity.this).getProposalCodesList(groupId);
                        for (int i = 0; i < proposalCodesModelList.size(); i++) {
                            long pno = Long.parseLong(proposal_no.getText().toString());
                            if (pno >= proposalCodesModelList.get(i).getMinvalue() && pno <= proposalCodesModelList.get(i).getMaxvalue()) {
                                proposal_no.setError(null);
                                updatePlannLead();
                                break;
                            } else {
                                proposal_no.setError("Please Enter Valid Proposal Number");
                                proposal_no.requestFocus();
                            }
                        }
                    } else {
                        updatePlannLead();
                    }
                }
            }
        });
        conversion_propensity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                conversion_propensity.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("RED");
                stringArrayList.add("AMBER");
                stringArrayList.add("GREEN");
                bottomSheetDialog(((EditText) findViewById(R.id.et_conversion_propensity)), stringArrayList, "Select Conversion propensity");
            }
        });
        product_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product_one.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(ViewActivityLeadActivity.this).getProductList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(product_one, stringArrayList, "Select Product");
                }
            }
        });
        product_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product_two.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(ViewActivityLeadActivity.this).getProductList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(product_two, stringArrayList, "Select Product");
                }
            }
        });
    }

    public void bottomSheetDialogForSubStatus(final EditText editText, List<
            String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(ViewActivityLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(ViewActivityLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Open")) {
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Contacted & meeting fixed")) {
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Proposition presented")) {
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Not interested")) {
                    findViewById(R.id.et_notes_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Converted")) {
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_next_followup_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.VISIBLE);
                }
                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    public void bottomSheetDialog(final EditText editText, ArrayList<String> strings, String
            title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(ViewActivityLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(ViewActivityLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    private void updatePlannLead() {
        plannerModel.setGroupid(groupId);
        plannerModel.setUserid(userid);
        if (Utils.isNotNullAndNotEmpty(leadModel.getStatus()))
            plannerModel.setSubtype(leadModel.getStatus());
        plannerModel.setActivitytype("Activity");
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadid())) {
            plannerModel.setLeadid(leadModel.getLeadid());
            plannerModel.setLocalleadid(leadModel.getId());
        } else {
            plannerModel.setLocalleadid(leadModel.getId());
        }
        plannerModel.setCompletestatus(2);
        plannerModel.setNetwork_status("offline");
        plannerModel.setDevicetimestamp(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
        PlannerMgr.getInstance(ViewActivityLeadActivity.this).updateleadPlan(plannerModel);

        leadModel.setUserid(userid);
        if (Utils.isNotNullAndNotEmpty(leadStage.getText().toString()))
            leadModel.setLeadstage(leadStage.getText().toString());
        if (Utils.isNotNullAndNotEmpty(leadStage.getText().toString()))
            if (leadStage.getText().toString().equalsIgnoreCase("Contacted & meeting fixed")) {
                if (leadModel.getFirstappointmentdate() != null) {
                    if (leadModel.getFirstappointmentdate() == 0)
                        leadModel.setFirstappointmentdate(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
                } else {
                    leadModel.setFirstappointmentdate(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
                }
            }
        if (Utils.isNotNullAndNotEmpty(etNextFollowupDate.getText().toString()))
            leadModel.setNextfollowupdate(Integer.parseInt(TimeUtils.convertDatetoUnix(etNextFollowupDate.getText().toString())));
        if (Utils.isNotNullAndNotEmpty(conversion_propensity.getText().toString()))
            leadModel.setConversionpropensity(conversion_propensity.getText().toString());
        if (Utils.isNotNullAndNotEmpty(expected_actpremium.getText().toString()))
            leadModel.setExpectedpremium(expected_actpremium.getText().toString());
        if (Utils.isNotNullAndNotEmpty(proposal_no.getText().toString()))
            leadModel.setProposalnumber(proposal_no.getText().toString());
        if (Utils.isNotNullAndNotEmpty(product_one.getText().toString()))
            leadModel.setProduct1(product_one.getText().toString());
        if (Utils.isNotNullAndNotEmpty(product_two.getText().toString()))
            leadModel.setProduct2(product_two.getText().toString());
        if (Utils.isNotNullAndNotEmpty(actpremium.getText().toString()))
            leadModel.setActualpremiumpaid(Integer.parseInt(actpremium.getText().toString()));
        if (Utils.isNotNullAndNotEmpty(etNotes.getText().toString()))
            leadModel.setRemarks(etNotes.getText().toString());
        leadModel.setNetwork_status("offline");
        LeadMgr.getInstance(ViewActivityLeadActivity.this).updateLead(leadModel, "offline");
        if (Utils.isNotNullAndNotEmpty(leadStage.getText().toString()))
            if (leadStage.getText().toString().equalsIgnoreCase("Contacted & meeting fixed")) {
                postcreateplan(TimeUtils.convertDatetoUnix(etNextFollowupDate.getText().toString()));
            } else if (leadStage.getText().toString().equalsIgnoreCase("Proposition presented")) {
                postcreateplan(TimeUtils.convertDatetoUnix(etNextFollowupDate.getText().toString()));
            }
        LeadIntentService.syncLeadstoServer(ViewActivityLeadActivity.this);
        showAlert("", "Your Plan is saved successfully",
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);
    }

    private void postcreateplan(String sdate) {
        plannerModel.setGroupid(groupId);
        plannerModel.setUserid(userid);
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadid())) {
            plannerModel.setLeadid(leadModel.getLeadid());
            plannerModel.setLocalleadid(leadModel.getId());
        } else {
            plannerModel.setLocalleadid(leadModel.getId());
        }
        if (Utils.isNotNullAndNotEmpty(sdate)) {
            plannerModel.setScheduleday(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", TimeUtils.getFormattedDatefromUnix(sdate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setSchedulemonth(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", TimeUtils.getFormattedDatefromUnix(sdate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setScheduleyear(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", TimeUtils.getFormattedDatefromUnix(sdate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setScheduletime(Integer.parseInt(sdate));
        }
        plannerModel.setFirstname(leadModel.getFirstname());
        plannerModel.setLastname(leadModel.getLastname());
        plannerModel.setPlanstatus(1);
        plannerModel.setCompletestatus(0);
        plannerModel.setType("Appointment");
        plannerModel.setActivitytype("Appointment");
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadstage()))
            plannerModel.setSubtype(leadModel.getLeadstage());
        plannerModel.setNetwork_status("offline");
        PlannerMgr.getInstance(ViewActivityLeadActivity.this).insertPlanner(plannerModel, leadModel.getUserid(), leadModel.getGroupid());
    }
}
