package com.outwork.sudlife.psf.more;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.more.models.Attachment;
import com.outwork.sudlife.psf.more.models.Comment;
import com.outwork.sudlife.psf.more.models.CommentDto;
import com.outwork.sudlife.psf.more.models.Email;
import com.outwork.sudlife.psf.more.models.MailDto;
import com.outwork.sudlife.psf.more.models.PhotoDto;
import com.outwork.sudlife.psf.more.models.S3Credentials;
import com.outwork.sudlife.psf.more.service.AdminService;
import com.outwork.sudlife.psf.more.service.MemberPostService;
import com.outwork.sudlife.psf.restinterfaces.GeneralService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class ViewPostMessageActivity extends BaseActivity {

    private ImageView cancel, userTile;
    private View seperator2, separator3;

    private TextView mailSubject, mailDate, viewDetailsLink, fromUser, fromUseremail, commentsHeader;
    private WebView mailDescription;
    private LinearLayout attachmentsLayout, imageLayout, commentLayout, viewdetails, toEmailLayout, ccEmailLayout;
    private EditText comment;
    private ImageButton sendComment;
    private ScrollView sv;

    private MailDto mailDto;
    private List<Attachment> attachmentList = new ArrayList<Attachment>();

    private IvokoApplication application;
    private List<Attachment> imageList = new ArrayList<Attachment>();
    private ArrayList<PhotoDto> photoList = new ArrayList<PhotoDto>();

    private S3Credentials credentials = new S3Credentials();
    private String bucketName, userName, formattedDate;

    private List<CommentDto> commentDtoList;
    private CommentDto commentDto;

    private List<Email> toEmailList, ccEmailList;
    private ProgressBar progressBar;
    private String objectId, userToken;
    private String messageType, messageSubType;
    private ImageView share;
    private StringBuilder plaintext;
    private View spaceView;
    private boolean isCommentEnabled;


    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_admin_message);

        userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");


        objectId = getIntent().getStringExtra("OBJECTID");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("MessageType")) {
            messageType = getIntent().getStringExtra("MessageType");
        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("MessageSubType")) {
            messageSubType = getIntent().getStringExtra("MessageSubType");
        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("commentflag")) {
            isCommentEnabled = getIntent().getBooleanExtra("commentflag", false);
        }

        initializeToolBar();
        initViews();
        if (isNetworkAvailable()) {
            getEmailMessages();
        } else {
            progressBar.setVisibility(View.GONE);
            sv.setVisibility(View.VISIBLE);
            showToast("Oops....!No Internet");
        }

    }


    private void initViews() {

        mailSubject = (TextView) findViewById(R.id.vMailTitle);
        sv = (ScrollView) findViewById(R.id.mscrollView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        mailDate = (TextView) findViewById(R.id.vMailDate);
        mailDescription = (WebView) findViewById(R.id.vMailDescription);
        fromUser = (TextView) findViewById(R.id.fromuser);
        fromUseremail = (TextView) findViewById(R.id.fromusermail);
        viewDetailsLink = (TextView) findViewById(R.id.vViewdetails);
        viewdetails = (LinearLayout) findViewById(R.id.viewdetails);
        spaceView = (View) findViewById(R.id.commentspace);

        userTile = (ImageView) findViewById(R.id.mUserTile);

        //   cancel = (ImageView)findViewById(R.id.vMailCancel);

/*        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // share = (ImageView)findViewById(R.id.share);
        viewdetails.setVisibility(GONE);

        if (!(messageType.equalsIgnoreCase("email"))) {
            viewDetailsLink.setVisibility(GONE);
        }

    }

    private void updateImageLayout() {

        imageLayout.removeAllViews();
        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        HorizontalScrollView scroll = new HorizontalScrollView(this);
        LinearLayout layout = new LinearLayout(this);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layout.setLayoutParams(params);
        layout.setOrientation(LinearLayout.HORIZONTAL);


        for (final Attachment attachment : imageList) {

            // ImageView img = (ImageView)li.inflate(R.layout.image_view, null);
            ImageView img = (ImageView) li.inflate(R.layout.image_view, layout, false);
            img.setTag(R.layout.image_view, attachment.getUrl());
            android.view.ViewGroup.LayoutParams layoutParams = img.getLayoutParams();
            layoutParams.width = 200;
            layoutParams.height = 200;
            img.setLayoutParams(layoutParams);


           // Glide.with(this).load(attachment.getUrl()).override(200, 200).centerCrop().into(img);

            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // viewFile(attachment.getFileId(), attachment.getExtn(), attachment.getFileName());
                  /*  Intent intent = new Intent(ViewPostMessageActivity.this, ViewPhotoActivity.class);
                    intent.putParcelableArrayListExtra("data",photoList);
                    intent.putExtra("pos", 0);
                    startActivity(intent);*/
                }
            });

            layout.addView(img);

        }
        scroll.addView(layout);
        imageLayout.addView(scroll);

    }


    private void updateAttachmentLayout(boolean whatever) {

        attachmentsLayout.removeAllViews();
        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (final Attachment attachment : attachmentList) {

            if (Utils.getFileExtn(attachment.getUrl()).equalsIgnoreCase("image") || getExtensionType(attachment.getExtn()).equalsIgnoreCase("image")) {
                imageList.add(attachment);
                PhotoDto dto = new PhotoDto();
                dto.setUrl(attachment.getUrl());
                dto.setName("");//attachment.getFileName());
                photoList.add(dto);
            } else {
                View fileView = li.inflate(R.layout.attachment_view_copy, null);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);

                params.setMargins(0, 8, 24, 0);

                fileView.setLayoutParams(params);

                TextView fileText = ((TextView) fileView.findViewById(R.id.filename));

                ImageView imageView = (ImageView) fileView.findViewById(R.id.fileImage);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewFile(attachment.getFileId(), attachment.getExtn(), attachment.getFileName());
                    }
                });
                fileText.setText(attachment.getFileName());

                fileText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewFile(attachment.getFileId(), attachment.getExtn(), attachment.getFileName());
                    }
                });
                attachmentsLayout.addView(fileView);


            }

        }

    }

    private void viewFile(final String fileId, final String extn, final String fileName) {

        String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/IvokoImages/" + fileName;
        File f = new File(filePath);
        if (f.exists()) {
            openFile(fileName);
        } else {

            try {
               // boolean downlaoded = downloadS3Async(fileId, extn, fileName);
            } catch (Exception e) {

            }

        }
    }

    /*public boolean downloadS3Async(String fileId, String extn, String fileName) throws IOException {

        credentials = getAmazonS3Credentials();
        AmazonAsyncTask amazonAsyncTask = new AmazonAsyncTask(this, credentials, fileId, fileName, extn, credentials.getBucketName());
        final String filename = fileName;
        amazonAsyncTask.setDownloadCompleted(new IDownloadCompleted() {
            @Override
            public boolean setSuccessResponse() {
                openFile(filename);
                return true;
            }

            @Override
            public boolean setError() {
                return false;
            }
        });


        amazonAsyncTask.execute();
        return false;
    }*/


    protected void openFile(String fileName) {
        try {
            Intent install = new Intent(Intent.ACTION_VIEW);
            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/IvokoImages/" + fileName;
            Uri fileUri = Uri.fromFile(new File(filePath));

            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(fileUri.toString());
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

            install.setDataAndType(fileUri, mimeType);
            startActivity(install);
        } catch (ActivityNotFoundException e) {

            new AlertDialog.Builder(this)
                    .setTitle("Failed to display " + fileName)
                    .setMessage("Please install the application which can open " + fileName)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert).show();
        }
    }


    private S3Credentials getAmazonS3Credentials() {

        credentials = application.getS3Credentials();

        if (!TextUtils.isEmpty(credentials.getExpiryTime())) {
            String expirytime = credentials.getExpiryTime();

            DateTime dt = new DateTime(expirytime);
            LocalDateTime dt1 = new LocalDateTime();
            DateTimeZone tz = DateTimeZone.UTC;
            DateTime dt2 = dt1.toDateTime(tz);

            if ((dt2.isBefore(dt)) && !TextUtils.isEmpty(credentials.getAccessKey()) &&
                    !TextUtils.isEmpty(credentials.getSecretKey()) && !TextUtils.isEmpty(credentials.getSessionToken())
                    && !TextUtils.isEmpty(credentials.getBucketName()))
                return credentials;

        }

        GeneralService client = RestService.createService(GeneralService.class);
        Call<RestResponse> s3Cred = client.getS3Credentials(userToken);
        s3Cred.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                if (response.isSuccessful()) {

                    RestResponse jsonResponse = response.body();
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse.getData());
                        JSONObject s3credentials = new JSONObject(jsonObject.getString("credentials"));

                        bucketName = jsonObject.getString("bucketName");
                        credentials = S3Credentials.parseS3Credentials(s3credentials);
                        credentials.setBucketName(bucketName);
                        application.writeS3Credentials(credentials);

                    } catch (Exception e) {

                    }
                }

            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

            }
        });

        return credentials;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //   getMenuInflater().inflate(R.menu.menu_mail_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    private void setWebViewContent() {
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        mailDescription.getSettings().setUseWideViewPort(true);
        mailDescription.getSettings().setLoadWithOverviewMode(true);
        mailDescription.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        mailDescription.clearCache(true);
        mailDescription.getSettings().setJavaScriptEnabled(true);

        mailDescription.getSettings().setBlockNetworkImage(false);
        mailDescription.getSettings().setDomStorageEnabled(true);
        mailDescription.getSettings().setBuiltInZoomControls(true);
        mailDescription.getSettings().setSupportZoom(true);
        mailDescription.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        mailDescription.getSettings().setDisplayZoomControls(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mailDescription.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        } else {
            mailDescription.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        }
        //String content = "<html><head><meta name=\"viewport\" content=\"width=device-width,initial-scale=1\"/>";


        /*content += "<style type=\"text/css\">" +
                "* { background: #FFFFFF ! important; color: #4D4D4D !important;}" +
                ":link, :link * { color: #2196F3 !important }" +
                ":visited, :visited * { color: #551A8B !important } img { max-width:100% !important;}</style> ";*/
        String content = "<html><head><meta name=\"viewport\" content=\"width=device-width\"/>";

        content += "<style type=\"text/css\">" +
                "* { background: #FFFFFF ! important; color: #4D4D4D !important; font-family:'Helvetica', 'Arial', sans-serif;word-break:break-word;}" +
                "p { text-align: justify;}" +
                ":link, :link * { color: #2196F3 !important }" +
                ":visited, :visited * { color: #551A8B !important } img { max-width:100% !important;height:auto !important;width:auto !important;}</style> ";

        String Description = "";
        String bodyText = "";
        if (!TextUtils.isEmpty(mailDto.getDescription())) {
            Description = mailDto.getDescription().toString();
            bodyText = Html.fromHtml(Description).toString();
        }

        //    bodyText = Html.fromHtml(Description).toString();

        //  Pattern bodyPattern = Pattern.compile("(.*<\s*body[^>]*>)|(<\s*/\s*body\s*\>.+)");
        //original---  content += "</head><body>" + bodyText + "</body></html>";

        if (mailDto.getMessageType().equalsIgnoreCase("email")) {
            content += "</head><body>" + bodyText + "</body></html>";
        } else {
            content += "</head><body>" + Description + "</body></html>";
        }

        /* Email fix
        if (mailDto.getMessageType().equalsIgnoreCase("announce")||mailDto.getMessageType().equalsIgnoreCase("promotion")){
            content += "</head><body>" + Description + "</body></html>";
        } else {

            content += "</head><body>" + Description + "</body></html>";
        } */
        mailDescription.loadDataWithBaseURL("http://", content, mimeType, encoding, "");
    }

    public void setUserTile() {

        final Resources res = this.getResources();
        //tileSize = res.getDimensionPixelSize(R.dimen.letter_tile_size);
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT

        int color2 = generator.getColor(userName.substring(0, 1));
        TextDrawable drawable = TextDrawable.builder().buildRound(userName.substring(0, 1).toUpperCase(), color2);

        if (drawable != null) {
            userTile.setImageDrawable(drawable);
        }

    }


    private void getEmailMessages() {
        progressBar.setVisibility(View.VISIBLE);
        sv.setVisibility(View.GONE);

        AdminService client = RestService.createService(AdminService.class);
        Call<RestResponse> getMessage = client.getAdminMessage(userToken, messageType, messageSubType, objectId);
        getMessage.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    RestResponse jsonResponse = response.body();
                    try {
                        JSONObject messageObject = new JSONObject(jsonResponse.getData());
                        mailDto = MailDto.parse(messageObject);
                        initial();
                        progressBar.setVisibility(View.GONE);
                        sv.setVisibility(View.VISIBLE);
                    } catch (JSONException e) {
                        progressBar.setVisibility(View.GONE);
                        sv.setVisibility(View.VISIBLE);
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                sv.setVisibility(View.VISIBLE);
            }
        });
    }

    private void initial() {

        viewDetailsLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewdetails.getVisibility() == View.VISIBLE) {
                    viewDetailsLink.setText("View Details");
                    viewdetails.setVisibility(GONE);
                } else {
                    viewDetailsLink.setText("Hide Details");
                    setEmailLayout();
                    viewdetails.setVisibility(View.VISIBLE);
                }

            }
        });

        ///Share implement the same in Member Posts

      /*  if (mailDto.isShareEnabled()){
            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShareContent();
                }
            });
        }else {
            share.setVisibility(View.INVISIBLE);
        }*/

        attachmentList = mailDto.getAttachmentList();
        mailSubject.setText(mailDto.getTitle());

        if (!TextUtils.isEmpty(mailDto.getUserName())) {
            fromUser.setText(mailDto.getUserName());
            userName = mailDto.getUserName();

        } else if (!TextUtils.isEmpty(mailDto.getUserEmail())) {
            fromUser.setText(mailDto.getUserEmail());
            userName = mailDto.getUserEmail();

        }
        setUserTile();

        if (!TextUtils.isEmpty(mailDto.getUserEmail())) {
            fromUseremail.setText(mailDto.getUserEmail());
        }

        if (!TextUtils.isEmpty(mailDto.getPostedDate())) {
            formattedDate = Utils.getFormattedDate(mailDto.getPostedDate(), true, 2);
            mailDate.setText(formattedDate);
        } else {
            mailDate.setText("Today");
        }

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);

        attachmentsLayout = (LinearLayout) findViewById(R.id.mattachments);

        imageLayout = (LinearLayout) findViewById(R.id.mImages);


        separator3 = (View) findViewById(R.id.separator3);

        if (attachmentList.size() > 0) {
            updateAttachmentLayout(true);

            updateImageLayout();


        }

        setWebViewContent();

        if (isCommentEnabled) {
            commentsHeader = (TextView) findViewById(R.id.commentsheader);
            setSendComment();
            commentDtoList = new ArrayList<CommentDto>();
            commentLayout = (LinearLayout) findViewById(R.id.commentLayout);
            GetComments();
        }


     /*   share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareContent();

            }
        });*/

    }

    private void setEmailLayout() {
        LinearLayout toEmailLayout = (LinearLayout) findViewById(R.id.toemailLayout);
        LinearLayout ccEmailLayout = (LinearLayout) findViewById(R.id.ccemailLayout);
        TextView fromUsermail = (TextView) findViewById(R.id.fromUserEmail);
        fromUsermail.setText(mailDto.getUserEmail());

        toEmailLayout.removeAllViews();
        ccEmailLayout.removeAllViews();

        toEmailList = mailDto.getToemailList();
        ccEmailList = mailDto.getccemailList();

        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (toEmailList != null && toEmailList.size() > 0) {
            for (final Email email : toEmailList) {
                //  View emailView = li.inflate(R.layout.list_item_email,toEmailLayout,false);
                View emailView = li.inflate(R.layout.list_item_email, null);
                TextView emailid = (TextView) emailView.findViewById(R.id.emailId);
                emailid.setText(email.getEmail());
                toEmailLayout.addView(emailView);
            }
        }

        if (ccEmailList != null && ccEmailList.size() > 0) {
            for (final Email email : ccEmailList) {
                View emailView = li.inflate(R.layout.list_item_email, ccEmailLayout, false);
                TextView emailid = (TextView) emailView.findViewById(R.id.emailId);
                emailid.setText(email.getEmail());
                ccEmailLayout.addView(emailView);
            }
        } else {
            TextView ccEmail = (TextView) findViewById(R.id.ccemail);
            ccEmail.setVisibility(GONE);
        }

    }

    private void initializeToolBar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.vadminToolBar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private String getExtensionType(String extension) {
        String outextension;
        switch (extension.toLowerCase()) {
            case ".png":
            case "png":
            case ".jpeg":
            case ".jpg":
            case "jpeg":
            case "jpg":
            case ".gif":
            case "gif":
                outextension = "image";
                break;
            default:
                outextension = "noimage";
        }
        return outextension;

    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void ShareContent() {

        plaintext = new StringBuilder(mailDto.getTitle());
        //  plaintext.append("\n");
        //   plaintext.append(mailDto.getShareText());
        if (!TextUtils.isEmpty(mailDto.getShareUrl())) {
            plaintext.append("\n");
            plaintext.append(mailDto.getShareUrl());
        }

        String subject = mailDto.getTitle();

        Resources resources = getResources();

        Intent emailIntent = new Intent();
        emailIntent.setAction(Intent.ACTION_SEND);
        // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
        emailIntent.putExtra(Intent.EXTRA_TEXT, plaintext.toString());
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.setType("message/rfc822");

        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");

        Intent openInChooser = Intent.createChooser(emailIntent, "Share Via");
        PackageManager pm = getPackageManager();
        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
        for (int i = 0; i < resInfo.size(); i++) {
            // Extract the label, append it, and repackage it in a LabeledIntent
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            if (packageName.contains("android.email")) {
                emailIntent.setPackage(packageName);
            } else if (packageName.contains("mms") || packageName.contains("android.gm") || packageName.contains("whatsapp")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                if (packageName.contains("mms") || packageName.contains("whatsapp")) {
                    intent.putExtra(Intent.EXTRA_TEXT, plaintext.toString());
                } else if (packageName.contains("android.gm")) { // If Gmail shows up twice, try removing this else-if clause and the reference to "android.gm" above
                    intent.putExtra(Intent.EXTRA_TEXT, plaintext.toString());
                    intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                    intent.setType("message/rfc822");
                }

                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
            }
        }

        // convert intentList to array
        LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);

        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
        startActivity(openInChooser);
    }

    private void GetComments() {

        MemberPostService client = RestService.createService(MemberPostService.class);
        Call<RestResponse> getcomments = client.getComments(userToken, mailDto.getMessageId(), "", "asc", "", "");
        getcomments.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                if (response.isSuccessful()) {
                    RestResponse jsonResponse = response.body();
                    JSONArray jsonArray = null;
                    try {
                        if (!TextUtils.isEmpty(jsonResponse.getData())) {
                            jsonArray = new JSONArray(jsonResponse.getData());
                            displayCommentList(jsonArray);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
            }
        });
    }

    private void displayCommentList(JSONArray commentArray) {

        commentDtoList.clear();

        try {
            for (int i = 0; i < commentArray.length(); i++) {
                commentDto = null;
                commentDto = CommentDto.parseServerRequest(commentArray.getJSONObject(i));
                commentDtoList.add(commentDto);
                addCommentView(commentDto, false);
            }

            if (commentDtoList.size() > 0 && commentsHeader.getVisibility() == View.GONE) {
                spaceView.setVisibility(View.VISIBLE);
                commentsHeader.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addCommentView(CommentDto cDto, boolean top) {

        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View commentView = li.inflate(R.layout.list_item_comment_copy, commentLayout, false);
        TextView userName = (TextView) commentView.findViewById(R.id.cTitle);
        TextView commentText = (TextView) commentView.findViewById(R.id.cDescription);
        TextView commentDate = (TextView) commentView.findViewById(R.id.cPostedDate);
        ImageView userImage = (ImageView) commentView.findViewById(R.id.commentUserTile);

        commentText.setText(cDto.getDescription());
        if (cDto.getCommentDate() != null) {
            String formattedDate = Utils.getFormattedDate(cDto.getCommentDate(), false, 2);
            commentDate.setText(formattedDate);
        } else {
            commentDate.setText("Now");
        }
        if (!TextUtils.isEmpty(cDto.getUserName())) {
            userName.setText(cDto.getUserName());
        }
        setUserTile(userImage, cDto.getUserName());
        if (top) {
            commentLayout.addView(commentView, 0);
        } else {
            commentLayout.addView(commentView);
        }

    }

    private final void focusOnView() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                sv.smoothScrollTo(0, commentLayout.getTop());
            }
        });
    }

    private void postCommenttoServer(final CommentDto dto) {
        Comment newcomment = new Comment();
        newcomment.setSubjectid(dto.getSubjectId());
        newcomment.setDescription(dto.getDescription());

        MemberPostService client = RestService.createService(MemberPostService.class);
        Call<RestResponse> dialogues = client.postComment(userToken, newcomment);
        dialogues.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                if (response.isSuccessful()) {
                    RestResponse jsonResponse = response.body();
                    if (commentsHeader.getVisibility() == View.GONE) {
                        spaceView.setVisibility(View.VISIBLE);
                        commentsHeader.setVisibility(View.VISIBLE);
                    }

                    addCommentView(dto, true);
                    comment.setText("");
                    focusOnView();
                    sendComment.setEnabled(true);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(comment.getWindowToken(), 0);
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                sendComment.setEnabled(true);
            }
        });
    }

    private void setSendComment() {

        LinearLayout sendCommentLayout = (LinearLayout) findViewById(R.id.chatfooter);
        sendCommentLayout.setVisibility(View.VISIBLE);

        sendComment = (ImageButton) findViewById(R.id.listChatSend);
        comment = (EditText) findViewById(R.id.listChatEdit);
        sendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String commentText = comment.getText().toString();

                if (!TextUtils.isEmpty(commentText)) {
                    sendComment.setEnabled(false);
                    postComment(commentText, mailDto.getMessageId(), "msg");

                }
            }
        });
    }

    private void postComment(String commentText, String subjectId, String subjectType) {

        commentDto = new CommentDto();
        commentDto.setDescription(commentText);
        commentDto.setSubjectId(subjectId);
        commentDto.setUserId(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        commentDto.setCommentDate(null);
        commentDto.setUserName(SharedPreferenceManager.getInstance().getString(Constants.FIRSTNAME, "") + " " + SharedPreferenceManager.getInstance().getString(Constants.LASTNAME, ""));
        postCommenttoServer(commentDto);

    }

    public void setUserTile(ImageView imgView, String profileName) {

        final Resources res = this.getResources();
        //tileSize = res.getDimensionPixelSize(R.dimen.letter_tile_size);
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT

        int color2 = generator.getColor(profileName.substring(0, 1));
        TextDrawable drawable = TextDrawable.builder().buildRound(profileName.substring(0, 1).toUpperCase(), color2);

        if (drawable != null) {
            imgView.setImageDrawable(drawable);
        }

    }


}
