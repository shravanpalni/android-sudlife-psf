package com.outwork.sudlife.psf.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;


import org.json.JSONObject;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.dto.UserDetailsDto;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.restinterfaces.UserRestInterface;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.fragments.ConfirmPasswordFragment;
import com.outwork.sudlife.psf.ui.fragments.SignupErrorFragment;
import com.outwork.sudlife.psf.utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignupActivity extends BaseActivity {

    private EditText userNameField,passwordField,groupCode;
    private TextView errorText,signUpBtn;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        userNameField = (EditText)findViewById(R.id.userNameField);
        passwordField = (EditText)findViewById(R.id.passwordField);
        groupCode = (EditText)findViewById(R.id.groupCode);
        errorText = (TextView)findViewById(R.id.errorText);
        mProgressBar = (ProgressBar)findViewById(R.id.signUpProgress);
        signUpBtn = (TextView)findViewById(R.id.join);



        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.getIndeterminateDrawable().
                            setColorFilter(Color.WHITE,android.graphics.PorterDuff.Mode.SRC_IN);
                    signUpBtn.setText("");
                    final UserDetailsDto userDetails = new UserDetailsDto();
                    userDetails.setUserName(userNameField.getText().toString());
                    //userDetails.setPassword(passwordField.getText().toString().trim());
                    userDetails.setGroupCode(groupCode.getText().toString());
                    hideKeyboard();
                    signupUser(SignupActivity.this, userDetails);
                }

            }
        });

        findViewById(R.id.signin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignupActivity.this, SignInActvity.class));
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }



    private boolean validate(){
        boolean isValid = true;

        if(groupCode.getText().toString().length()==0){
            isValid = false;
            groupCode.setError("Please enter your Group Code");
            return isValid;
        }

        if(userNameField.getText().toString().length()==0){
            isValid = false;
            userNameField.setError("Please enter your email");
            return isValid;
        }

        if (!Utils.isValidEmail(userNameField.getText().toString()
                .trim())) {
            userNameField.setError("Please enter a valid email");
            isValid = false;
            return isValid;
        }




      /*  if(groupCode.getText().toString().length()==0){
            isValid = false;
            groupCode.setError("Please enter Group Code provided by your Administrator");
        }*/

        return isValid;
    }


    private void signupUser(Context context, final UserDetailsDto userDetailsDto) {

        UserRestInterface client = RestService.createService(UserRestInterface.class);
        Call<RestResponse> user = client.signUpValidate(userDetailsDto.getUserName(),
                                                        userDetailsDto.getGroupCode());
        user.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                if (response.isSuccessful()) {
                    RestResponse jsonResponse = response.body();
                    String code = jsonResponse.getCode();
                    String status = jsonResponse.getStatus();
                    String message = jsonResponse.getMessage();

                    if (code.equalsIgnoreCase("USR-200")){

                        FragmentManager manager = getSupportFragmentManager();
                        ConfirmPasswordFragment fragment  = new ConfirmPasswordFragment();
                        Bundle userBundle = new Bundle();
                        userBundle.putString("groupcode",userDetailsDto.getGroupCode());
                        userBundle.putString("loginid",userDetailsDto.getUserName());
                        fragment.setArguments(userBundle);
                        FragmentTransaction ft = manager.beginTransaction();
                        ft.replace(android.R.id.content,fragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                    if (code.equalsIgnoreCase("USR-1003")||code.equalsIgnoreCase("USR-1002")){
                        try {
                            JSONObject subscriptionData = new JSONObject(jsonResponse.getData());
                            final String groupId = subscriptionData.getString("groupid");
                            final String userId = subscriptionData.getString("userid");
                            final String errorCode = code;
                            subscribetoGroup(groupId, userId, userDetailsDto, errorCode);

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    }
                    if (code.equalsIgnoreCase("USR-103")){
                        Intent userVerify = new Intent(SignupActivity.this, UserVerifyActivity.class);
                        userVerify.putExtra("fromsignup", true);
                        userVerify.putExtra("message",message);
                        startActivity(userVerify);
                        finish();
                    }
                    if (code.equals("USR-105") ||code.equalsIgnoreCase("USR-106") || code.equalsIgnoreCase("USR-107")) {
                        Utils.displayToast(SignupActivity.this, message);
                        setSignupText();
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<RestResponse> call, Throwable t) {
                Utils.displayToast(SignupActivity.this, "Please check your network connection and try again");
                setSignupText();

            }
        });
    }

    private void subscribetoGroup(String groupId, String userId, final UserDetailsDto userDetailsDto, final String errorCode){

        UserRestInterface client = RestService.createService(UserRestInterface.class);
        Call<RestResponse> user = client.subscribeUsertoGroup(groupId,userId);
        user.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                if (response.isSuccessful()){
                    if (errorCode.equalsIgnoreCase("USR-1003")||errorCode.equalsIgnoreCase("USR-1002")){

                        FragmentManager manager = getSupportFragmentManager();
                        SignupErrorFragment fragment  = new SignupErrorFragment();
                        Bundle errorBundle = new Bundle();
                        errorBundle.putString("errorMsg","Congratulations. You are now member of the group. Login with your email and password.");
                        errorBundle.putString("errorCode",errorCode);
                        fragment.setArguments(errorBundle);
                        FragmentTransaction ft = manager.beginTransaction();
                        ft.replace(android.R.id.content,fragment);
                        ft.addToBackStack(null);
                        ft.commit();

                    }
                }

            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                Utils.displayToast(SignupActivity.this, "Please check your network connection and try again");

            }
        });

    }


    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void setSignupText(){
        mProgressBar.setVisibility(View.INVISIBLE);
        signUpBtn.setText("Sign Up");
    }



}
