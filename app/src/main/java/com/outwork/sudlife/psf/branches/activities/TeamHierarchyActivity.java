package com.outwork.sudlife.psf.branches.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.adapter.SOSpinnerAdapter;
import com.outwork.sudlife.psf.branches.adapter.TeamHierarchtFirstLevelAdapter;
import com.outwork.sudlife.psf.branches.adapter.TeamHierarchtFourthLevelAdapter;
import com.outwork.sudlife.psf.branches.adapter.TeamHierarchtSecondLevelAdapter;
import com.outwork.sudlife.psf.branches.adapter.TeamHierarchtThirdLevelAdapter;
import com.outwork.sudlife.psf.branches.adapter.UserBranchesAdapter;
import com.outwork.sudlife.psf.branches.models.BranchesModel;
import com.outwork.sudlife.psf.branches.models.Childlayer;
import com.outwork.sudlife.psf.branches.models.RecyclerTouchListener;
import com.outwork.sudlife.psf.branches.models.TeamHierarchyModel;
import com.outwork.sudlife.psf.branches.services.CustomerService;
import com.outwork.sudlife.psf.planner.activities.CreatePlanActivity;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.models.Userprofile;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shravanch on 13-07-2018.
 */
public class TeamHierarchyActivity extends BaseActivity {
    private TeamHierarchtFirstLevelAdapter teamHierarchtFirstLevelAdapter;
    private TeamHierarchtSecondLevelAdapter teamHierarchtSecondLevelAdapter;
    private TeamHierarchtThirdLevelAdapter teamHierarchtThirdLevelAdapter;
    private TeamHierarchtFourthLevelAdapter teamHierarchtFourthLevelAdapter;
    private SOSpinnerAdapter soSpinnerAdapter;
    private fr.ganfra.materialspinner.MaterialSpinner firstspinnerList, secondspinnerList, thirdspinnerList,fourthspinnerList, SOSpinner;
    private List<Childlayer> listChildlayer1 = new ArrayList<>();
    private List<Childlayer> listChildlayer2 = new ArrayList<>();
    private List<Childlayer> listChildlayer3 = new ArrayList<>();
    private List<BranchesModel> branchesModelList;
    private RecyclerView listview;
    private ArrayList<Userprofile> teamMembersHeirarchyArrayList = new ArrayList();
    private List<TeamHierarchyModel> teamHierarchyModelList = new ArrayList<>();
    private int planststatus;
    private String plandate,userteamid;
    private TextView user_branches, no_data;
    private RelativeLayout view_layout;
    private LinearLayout main_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_hierarchy);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(Constants.PLAN_STATUS)) {
            planststatus = getIntent().getIntExtra(Constants.PLAN_STATUS, 0);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(Constants.PLAN_DATE)) {
            plandate = getIntent().getStringExtra(Constants.PLAN_DATE);
        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("USERTEAMID")) {
            userteamid = getIntent().getStringExtra("USERTEAMID");
        }
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        firstspinnerList = (fr.ganfra.materialspinner.MaterialSpinner) findViewById(R.id.spinner);
        secondspinnerList = (fr.ganfra.materialspinner.MaterialSpinner) findViewById(R.id.spinner_child1);
        thirdspinnerList = (fr.ganfra.materialspinner.MaterialSpinner) findViewById(R.id.spinner_child2);
        fourthspinnerList = (fr.ganfra.materialspinner.MaterialSpinner) findViewById(R.id.spinner_child4);

        SOSpinner = (fr.ganfra.materialspinner.MaterialSpinner) findViewById(R.id.spinner_so);
        listview = (RecyclerView) findViewById(R.id.listview);
        listview.addOnItemTouchListener(new RecyclerTouchListener(TeamHierarchyActivity.this, listview, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Intent intent = new Intent(TeamHierarchyActivity.this, CreatePlanActivity.class);
                intent.putExtra("customer", new Gson().toJson(branchesModelList.get(position)));
                intent.putExtra("customerid", branchesModelList.get(position).getCustomerid());
                intent.putExtra(Constants.PLAN_STATUS, planststatus);
                intent.putExtra(Constants.PLAN_DATE, plandate);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
        view_layout = (RelativeLayout) findViewById(R.id.view1);
        user_branches = (TextView) findViewById(R.id.user_branches);
        no_data = (TextView) findViewById(R.id.no_data);
        if (isNetworkAvailable()) {
            getTeamHierarchy(userteamid);
        } else {
            showSimpleAlert("", "Oops...No Internet connection.");
        }
        initToolBar();
        setListeners();
    }

    private void setListeners() {
        firstspinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {
                    Log.i("++++++" + position, "++++" + teamHierarchyModelList.get(0).getChildlayers().get(position).getName());
                    Log.i("++++++" + position, "++++" + teamHierarchyModelList.get(0).getChildlayers().get(position).getHaschildren());
                    Log.i("++++++" + position, "++++" + teamHierarchyModelList.get(0).getChildlayers().get(position).getChildlayers().size());
                    Log.i("++++++" + position, "++++" + teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid());
                    listChildlayer1 = teamHierarchyModelList.get(0).getChildlayers().get(position).getChildlayers();
                    if (listChildlayer1.size() > 0) {
                        setAdapterList(listChildlayer1);
                    } else {
                       // showToast("No  levels available");

                        String teamid = teamHierarchyModelList.get(0).getChildlayers().get(position).getTeamid();
                        if (isNetworkAvailable()) {
                            getTeamHierarchyMembers(teamid);
                        } else {
                            showSimpleAlert("", "Oops...No Internet connection.");
                        }
                        Log.i("Habi", "secondspinnerList selected");
                    }
                   /* else {
                        String teamid = listChildlayer1.get(position).getTeamid();
                        if (isNetworkAvailable()) {
                            getTeamHierarchyMembers(teamid);
                        } else {
                            showSimpleAlert("", "Oops...No Internet connection.");
                        }
                        Log.i("Habi", "secondspinnerList selected");
                    }*/
                } else {
                    // firstspinnerList.setSelection(0);
                    secondspinnerList.setVisibility(View.GONE);
                    thirdspinnerList.setVisibility(View.GONE);
                    fourthspinnerList.setVisibility(View.GONE);
                    SOSpinner.setVisibility(View.GONE);
                    listview.setVisibility(View.GONE);
                    view_layout.setVisibility(View.GONE);
                    Log.i("Habi first spinner", "position <0 selected");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        secondspinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {
                    Log.i("++++++" + position, "++++" + listChildlayer1.get(position).getName());
                    Log.i("++++++" + position, "++++" + listChildlayer1.get(position).getHaschildren());
                    Log.i("++++++" + position, "++++" + listChildlayer1.get(position).getChildlayers().size());
                    listChildlayer2 = listChildlayer1.get(position).getChildlayers();
                    if (listChildlayer2.size() > 0) {
                        setAdapterList2(listChildlayer2);
                    } else {
                        String teamid = listChildlayer1.get(position).getTeamid();
                        if (isNetworkAvailable()) {
                            getTeamHierarchyMembers(teamid);
                        } else {
                            showSimpleAlert("", "Oops...No Internet connection.");
                        }
                        Log.i("Habi", "secondspinnerList selected");
                    }
                } else {
                    thirdspinnerList.setVisibility(View.GONE);
                    fourthspinnerList.setVisibility(View.GONE);
                    SOSpinner.setVisibility(View.GONE);
                    listview.setVisibility(View.GONE);
                    view_layout.setVisibility(View.GONE);
                    Log.i("Habi second spinner", "position <0 selected");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });



        thirdspinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {
                    Log.i("++++++" + position, "++++" + listChildlayer2.get(position).getName());
                    Log.i("++++++" + position, "++++" + listChildlayer2.get(position).getHaschildren());
                    Log.i("++++++" + position, "++++" + listChildlayer2.get(position).getChildlayers().size());
                    listChildlayer3 = listChildlayer2.get(position).getChildlayers();
                    if (listChildlayer3.size() > 0) {
                        setAdapterList3(listChildlayer3);
                    } else {
                        String teamid = listChildlayer2.get(position).getTeamid();
                        if (isNetworkAvailable()) {
                            getTeamHierarchyMembers(teamid);
                        } else {
                            showSimpleAlert("", "Oops...No Internet connection.");
                        }
                        Log.i("Habi", "secondspinnerList selected");
                    }
                } else {

                    fourthspinnerList.setVisibility(View.GONE);
                    SOSpinner.setVisibility(View.GONE);
                    listview.setVisibility(View.GONE);
                    view_layout.setVisibility(View.GONE);
                    Log.i("Habi second spinner", "position <0 selected");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        fourthspinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.i("++++++" + position, "++++" + listChildlayer2.get(position).getName());
                if (position >= 0) {
                    String teamid = listChildlayer3.get(position).getTeamid();
                    if (isNetworkAvailable()) {
                        getTeamHierarchyMembers(teamid);
                    } else {
                        showSimpleAlert("", "Oops...No Internet connection.");
                    }
                } else {
                    //fourthspinnerList.setVisibility(View.GONE);
                    SOSpinner.setVisibility(View.GONE);
                    listview.setVisibility(View.GONE);
                    view_layout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        /*thirdspinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.i("++++++" + position, "++++" + listChildlayer2.get(position).getName());
                if (position >= 0) {
                    String teamid = listChildlayer2.get(position).getTeamid();
                    if (isNetworkAvailable()) {
                        getTeamHierarchyMembers(teamid);
                    } else {
                        showSimpleAlert("", "Oops...No Internet connection.");
                    }
                } else {
                    fourthspinnerList.setVisibility(View.GONE);
                    SOSpinner.setVisibility(View.GONE);
                    listview.setVisibility(View.GONE);
                    view_layout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/


        /*fourthspinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.i("++++++" + position, "++++" + listChildlayer2.get(position).getName());
                if (position >= 0) {
                    String teamid = listChildlayer3.get(position).getTeamid();
                    if (isNetworkAvailable()) {
                        getTeamHierarchyMembers(teamid);
                    } else {
                        showSimpleAlert("", "Oops...No Internet connection.");
                    }
                } else {
                    fourthspinnerList.setVisibility(View.GONE);
                    SOSpinner.setVisibility(View.GONE);
                    listview.setVisibility(View.GONE);
                    view_layout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/

    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Select Branch");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    /*****************************************Callbacks********************************/
    private void getTeamHierarchy(String userteamid) {
        showProgressDialog("Loading, please wait....");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchy(userToken,userteamid);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        Type listType = new TypeToken<ArrayList<TeamHierarchyModel>>() {
                        }.getType();
                        teamHierarchyModelList = new Gson().fromJson(response.body().getData(), listType);
                        if (Utils.isNotNullAndNotEmpty(teamHierarchyModelList)) {
//                            if (Utils.isNotNullAndNotEmpty(teamHierarchyModelList.get(0).getSupervisordisplayrole()))
//                                firstspinnerList.setHint(teamHierarchyModelList.get(0).getSupervisordisplayrole());
                            firstspinnerList.setVisibility(View.VISIBLE);
                            teamHierarchtFirstLevelAdapter = new TeamHierarchtFirstLevelAdapter(TeamHierarchyActivity.this, teamHierarchyModelList.get(0).getChildlayers());
                            firstspinnerList.setAdapter(teamHierarchtFirstLevelAdapter);
                        } else {
                            main_layout.setVisibility(View.GONE);
                            firstspinnerList.setVisibility(View.GONE);
                            no_data.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    dismissProgressDialog();
                    main_layout.setVisibility(View.GONE);
                    firstspinnerList.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
                main_layout.setVisibility(View.GONE);
                firstspinnerList.setVisibility(View.GONE);
                no_data.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setAdapterList(final List<Childlayer> childlayers) {
        secondspinnerList.setVisibility(View.VISIBLE);
//        secondspinnerList.setHint(list.get(0).getSupervisordisplayrole());
        teamHierarchtSecondLevelAdapter = new TeamHierarchtSecondLevelAdapter(TeamHierarchyActivity.this, childlayers);
        secondspinnerList.setAdapter(teamHierarchtSecondLevelAdapter);
        teamHierarchtSecondLevelAdapter.notifyDataSetChanged();
    }

    private void setAdapterList2(final List<Childlayer> list2) {
        thirdspinnerList.setVisibility(View.VISIBLE);
        teamHierarchtThirdLevelAdapter = new TeamHierarchtThirdLevelAdapter(TeamHierarchyActivity.this, list2);
        thirdspinnerList.setAdapter(teamHierarchtThirdLevelAdapter);
        teamHierarchtThirdLevelAdapter.notifyDataSetChanged();
    }


    private void setAdapterList3(final List<Childlayer> list3) {
        fourthspinnerList.setVisibility(View.VISIBLE);
        teamHierarchtFourthLevelAdapter = new TeamHierarchtFourthLevelAdapter(TeamHierarchyActivity.this, list3);
        fourthspinnerList.setAdapter(teamHierarchtFourthLevelAdapter);
        teamHierarchtFourthLevelAdapter.notifyDataSetChanged();
    }

    private void getTeamHierarchyMembers(String teamid) {
        showProgressDialog("Loading, please wait....");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, teamid);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        teamMembersHeirarchyArrayList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                        }.getType());
                        if (Utils.isNotNullAndNotEmpty(teamMembersHeirarchyArrayList)) {
                            SOSpinner.setVisibility(View.VISIBLE);
//                            SOSpinner.setHint("SO members");
                            soSpinnerAdapter = new SOSpinnerAdapter(TeamHierarchyActivity.this, teamMembersHeirarchyArrayList);
                            SOSpinner.setAdapter(soSpinnerAdapter);
                            soSpinnerAdapter.notifyDataSetChanged();
                            SOSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    if (position >= 0) {
                                        String uid = teamMembersHeirarchyArrayList.get(position).getUserid();
                                        Log.i("Habi", "userId - - - " + uid);
                                        String orgid = teamMembersHeirarchyArrayList.get(position).getOrganizationid();
                                        Log.i("Habi", "orgid - - - " + orgid);
                                        if (isNetworkAvailable()) {
                                            getBranchesForUserId(uid);
                                        } else {
                                            showSimpleAlert("", "Oops...No Internet connection.");
                                        }
                                    } else {
                                        Log.i("Habi so spinner", "position <0 ");
                                        listview.setVisibility(View.GONE);
                                        view_layout.setVisibility(View.GONE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });
                        } else {
                            showToast("No SO available");
                            SOSpinner.setVisibility(View.GONE);
                        }
                    }
                } else {
                    dismissProgressDialog();
                    showToast("No SO available");
                    SOSpinner.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
                showToast("No SO available");
                SOSpinner.setVisibility(View.GONE);
            }
        });
    }

    private void getBranchesForUserId(String uid) {
        showProgressDialog("Loading, please wait....");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getBranchesForUserId(userToken, uid);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    dismissProgressDialog();
                    final List<BranchesModel> branchesList = new ArrayList<>();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        branchesModelList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<BranchesModel>>() {
                        }.getType());
//                        for (BranchesModel branchesModel : branchesModelList) {
//                            if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomername())) {
//                                branchesList.add(branchesModel);
//                            }
//                        }
                        if (Utils.isNotNullAndNotEmpty(branchesModelList)) {
                            UserBranchesAdapter userBranchesAdapter = new UserBranchesAdapter(TeamHierarchyActivity.this, branchesModelList);
                            //listview = (RecyclerView) findViewById(R.id.listview);
                            view_layout.setVisibility(View.VISIBLE);
                            listview.setVisibility(View.VISIBLE);
                            listview.setAdapter(userBranchesAdapter);
                            listview.getAdapter().notifyDataSetChanged();
                            listview.setLayoutManager(new LinearLayoutManager(TeamHierarchyActivity.this));


                        } else {
                            dismissProgressDialog();
                            listview.setVisibility(View.GONE);
                            user_branches.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    dismissProgressDialog();
                    listview.setVisibility(View.GONE);
                    user_branches.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
                listview.setVisibility(View.GONE);
                user_branches.setVisibility(View.VISIBLE);
            }
        });
    }
}
