package com.outwork.sudlife.psf.opportunity.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Habi on 22-12-2017.
 */

public class OpportunityItems implements Serializable {
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("orderid")
    @Expose
    private Integer orderid;
    @SerializedName("productid")
    @Expose
    private String productid;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @SerializedName("rate")
    @Expose
    private Double rate;
    @SerializedName("notes")
    @Expose
    private Object notes;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("contactname")
    @Expose
    private String contactname;
    @SerializedName("customername")
    @Expose
    private String customername;
    @SerializedName("contactid")
    @Expose
    private String contactid;
    @SerializedName("customerid")
    @Expose
    private String customerid;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("createddate")
    @Expose
    private Integer createddate;
    @SerializedName("modifieddate")
    @Expose
    private Integer modifieddate;
    @SerializedName("opportunityid")
    @Expose
    private Integer opportunityid;

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Object getNotes() {
        return notes;
    }

    public void setNotes(Object notes) {
        this.notes = notes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getContactid() {
        return contactid;
    }

    public void setContactid(String contactid) {
        this.contactid = contactid;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Integer createddate) {
        this.createddate = createddate;
    }

    public Integer getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(Integer modifieddate) {
        this.modifieddate = modifieddate;
    }

    public Integer getOpportunityid() {
        return opportunityid;
    }

    public void setOpportunityid(Integer opportunityid) {
        this.opportunityid = opportunityid;
    }

}
