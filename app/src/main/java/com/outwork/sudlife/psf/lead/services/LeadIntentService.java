package com.outwork.sudlife.psf.lead.services;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.db.ProposalCodesDao;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;
import com.outwork.sudlife.psf.lead.model.ProposalCodesModel;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.service.PlannerIntentService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.activities.NotificationsListActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadIntentService extends JobIntentService {
    public static final String TAG = LeadIntentService.class.getSimpleName();

    public static final String ACTION_FORMS_OFFLINE_LEADS_SYNC = "com.outwork.sudlife.lite.lead.services.action.FORMS_OFFLINE_LEADS_SYNC";
    public static final String ACTION_FORMS_OFFLINE_PSF_LEADS_SYNC = "com.outwork.sudlife.lite.lead.services.action.ACTION_FORMS_OFFLINE_PSF_LEADS_SYNC";
    public static final String ACTION_FORMS_INSERT_LEADS = "com.outwork.sudlife.lite.lead.services.action.FORMS_INSERT_LEADS";
    public static final String ACTION_FORMS_INSERT_PSF_LEADS = "com.outwork.sudlife.lite.lead.services.action.ACTION_FORMS_INSERT_PSF_LEADS";
    public static final String ACTION_FORMS_INSERT_PSF_NOTIFICATIONS = "com.outwork.sudlife.lite.lead.services.action.ACTION_FORMS_INSERT_PSF_NOTIFICATIONS";
    public static final String ACTION_FORMS_UPDATE_PSF_NOTIFICATIONS = "com.outwork.sudlife.lite.lead.services.action.ACTION_FORMS_UPDATE_PSF_NOTIFICATIONS";

    public static final String ACTION_INSERT_LEADS_BYUSERID = "com.outwork.sudlife.lite.lead.services.action.INSERT_LEADS_BYUSERID";
    public static final String ACTION_INSERT_PROPOSAL_CODES = "com.outwork.sudlife.lite.lead.services.action.INSERT_PROPOSAL_CODES";
    public static final String USERID = "com.outwork.sudlife.lite.lead.services.extra.USERID";
    private static final Integer JOBID = 1003;

    private LocalBroadcastManager mgr;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FORMS_OFFLINE_LEADS_SYNC.equals(action)) {
                syncOfflineLeads();
            }


            if (ACTION_FORMS_OFFLINE_PSF_LEADS_SYNC.equals(action)) {
                syncOfflinePSFLeads();
            }


            //leads
            if (ACTION_FORMS_INSERT_LEADS.equals(action)) {
                getServerLeads();
            }
            if (ACTION_FORMS_INSERT_PSF_LEADS.equals(action)) {
                getServerPSFLeads();

            }

            if (ACTION_FORMS_INSERT_PSF_NOTIFICATIONS.equals(action)) {
                getServerPSFNotifications();

            }
            if (ACTION_FORMS_UPDATE_PSF_NOTIFICATIONS.equals(action)) {
                updateAllPSFNotifications();

            }


            if (ACTION_INSERT_LEADS_BYUSERID.equals(action)) {
                final String userid = intent.getStringExtra(USERID);
//                getServerLeadsbyUserID(userid);
            }
            if (ACTION_INSERT_PROPOSAL_CODES.equals(action)) {
                getProposalCodes();
            }
        }
    }

    public static void syncLeadstoServer(Context context) {
        Intent intent = new Intent(context, LeadIntentService.class);
        intent.setAction(ACTION_FORMS_OFFLINE_LEADS_SYNC);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, LeadIntentService.class, JOBID, intent);
        } else {
            context.startService(intent);
        }
    }


    public static void syncPSFLeadstoServer(Context context) {
        Intent intent = new Intent(context, LeadIntentService.class);
        intent.setAction(ACTION_FORMS_OFFLINE_PSF_LEADS_SYNC);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, LeadIntentService.class, JOBID, intent);
        } else {
            context.startService(intent);
        }
    }

    public static void insertLeadstoServer(Context context) {
        Intent intent = new Intent(context, LeadIntentService.class);
        intent.setAction(ACTION_FORMS_INSERT_LEADS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, LeadIntentService.class, JOBID, intent);
        } else {
            context.startService(intent);
        }
    }


    //psf

    public static void insertPSFLeadstoServer(Context context) {
        Intent intent = new Intent(context, LeadIntentService.class);
        intent.setAction(ACTION_FORMS_INSERT_PSF_LEADS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, LeadIntentService.class, JOBID, intent);
        } else {
            context.startService(intent);
        }
    }


    public static void insertPSFNotifications(Context context) {
        Intent intent = new Intent(context, LeadIntentService.class);
        intent.setAction(ACTION_FORMS_INSERT_PSF_NOTIFICATIONS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, LeadIntentService.class, JOBID, intent);
        } else {
            context.startService(intent);
        }
    }


    public static void updatePSFNotifications(Context context) {
        Intent intent = new Intent(context, LeadIntentService.class);
        intent.setAction(ACTION_FORMS_UPDATE_PSF_NOTIFICATIONS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, LeadIntentService.class, JOBID, intent);
        } else {
            context.startService(intent);
        }
    }

    public static void getLeadsByUserID(Context context, String userid) {
        Intent intent = new Intent(context, LeadIntentService.class);
        intent.setAction(ACTION_INSERT_LEADS_BYUSERID);
        intent.putExtra(USERID, userid);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, LeadIntentService.class, JOBID, intent);
        } else {
            context.startService(intent);
        }
    }

    public static void insertProposalCodes(Context context) {
        Intent intent = new Intent(context, LeadIntentService.class);
        intent.setAction(ACTION_INSERT_PROPOSAL_CODES);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, LeadIntentService.class, JOBID, intent);
        } else {
            context.startService(intent);
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }


    //psf leads


    private void syncOfflinePSFLeads() {
        final List<PSFLeadModel> leadModelList = LeadMgr.getInstance(LeadIntentService.this).getOfflinePSFLeadsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");
        if (leadModelList.size() > 0) {
            for (final PSFLeadModel leadModel : leadModelList) {
                if (Utils.isNotNullAndNotEmpty(leadModel.getNetwork_status())) {
                    if (isNetworkAvailable()) {
                        if (TextUtils.isEmpty(leadModel.getLeadId())) {
                            postPSFLead(leadModel);
                        } else {
                            updatePSFLead(leadModel);
                        }
                    }
                }
            }
            // PlannerIntentService.syncPlanstoServer(LeadIntentService.this);
        } else {
            //PlannerIntentService.syncPlanstoServer(LeadIntentService.this);
        }
    }


    private void updatePSFLead(final PSFLeadModel leadModel) {
        //leadModel.setLeadId("djgsfkldjgfkdg");
        //leadModel.setLeadCode("");

        //try{

           // leadModel.setWarning_message("This is testing phase");
           // LeadMgr.getInstance(LeadIntentService.this).updateFailedPSFLead(leadModel, "offline","This is testing phase");
       /* leadModel.setWarning_message("Test Warning Message");
        leadModel.setStatus_code(301);
        LeadMgr.getInstance(LeadIntentService.this).updateFailedPSFLead(leadModel, "offline","Test Warning Message");
*/

        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> updateLead = client.updatePSFLead(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), leadModel);
        try {
            Response<RestResponse> restResponse = updateLead.execute();
            String statusCode = restResponse.body().getCode();


            if (restResponse.code() == 200) {

                if (restResponse.body().getCode().equalsIgnoreCase("1")) {//dequick
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                        leadModel.setWarning_message("");
                        LeadMgr.getInstance(LeadIntentService.this).updatePSFLeadStatus(leadModel, "");
                        LeadMgr.getInstance(LeadIntentService.this).updateRetryCount(leadModel,leadModel.getLeadId(),0);
                        Intent intent = new Intent("lead_broadcast");
                        mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                        mgr.sendBroadcast(intent);
                    }

                }else{

                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getCode()) ){


                        if (restResponse.body().getCode().equalsIgnoreCase("3")){
                            int count = 0;
                            count = LeadMgr.getInstance(LeadIntentService.this).getRetryCount(leadModel);
                            count = count +1;
                            //LeadMgr.getInstance(LeadIntentService.this).updateRetryCount(leadModel,leadModel.getLeadId(),count);
                            leadModel.setWarning_message(restResponse.body().getMessage().toLowerCase());
                            leadModel.setStatus_code(Integer.valueOf(restResponse.body().getCode()));
                            leadModel.setRetry_count(count);
                            LeadMgr.getInstance(LeadIntentService.this).updateFailedPSFLead(leadModel, "offline",restResponse.body().getMessage().toLowerCase());

                        }else  if (restResponse.body().getCode().equalsIgnoreCase("2")){

                            leadModel.setWarning_message(restResponse.body().getMessage().toLowerCase());
                            leadModel.setStatus_code(Integer.valueOf(restResponse.body().getCode()));
                            LeadMgr.getInstance(LeadIntentService.this).updateFailedPSFLead(leadModel, "offline",restResponse.body().getMessage().toLowerCase());

                        }else  if (restResponse.body().getCode().equalsIgnoreCase("4")){
                            leadModel.setWarning_message(restResponse.body().getMessage().toLowerCase());
                            leadModel.setStatus_code(Integer.valueOf(restResponse.body().getCode()));
                            LeadMgr.getInstance(LeadIntentService.this).updateFailedPSFLead(leadModel, "offline",restResponse.body().getMessage().toLowerCase());

                        }





                    }




                }

            }




            //if (statusCode == 200) {
                //if (restResponse.body().getStatus().equalsIgnoreCase("Success.")) {
            //if (restResponse.body().getCode().equalsIgnoreCase("1")) { //dequick
          /*  if (restResponse.code() == 200) {
                        if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                            leadModel.setWarning_message("");
                            LeadMgr.getInstance(LeadIntentService.this).updatePSFLeadStatus(leadModel, "");
                            LeadMgr.getInstance(LeadIntentService.this).updateRetryCount(leadModel,leadModel.getLeadId(),0);
                            Intent intent = new Intent("lead_broadcast");
                            mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                            mgr.sendBroadcast(intent);
                        }
                }*//*else{

                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getCode()) ){


                        if (restResponse.body().getCode().equalsIgnoreCase("3")){
                            int count = 0;
                             count = LeadMgr.getInstance(LeadIntentService.this).getRetryCount(leadModel);
                            count = count +1;
                            //LeadMgr.getInstance(LeadIntentService.this).updateRetryCount(leadModel,leadModel.getLeadId(),count);
                            leadModel.setWarning_message(restResponse.body().getMessage().toLowerCase());
                            leadModel.setStatus_code(Integer.valueOf(restResponse.body().getCode()));
                            leadModel.setRetry_count(count);
                            LeadMgr.getInstance(LeadIntentService.this).updateFailedPSFLead(leadModel, "offline",restResponse.body().getMessage().toLowerCase());

                        }else  if (restResponse.body().getCode().equalsIgnoreCase("2")){

                            leadModel.setWarning_message(restResponse.body().getMessage().toLowerCase());
                            leadModel.setStatus_code(Integer.valueOf(restResponse.body().getCode()));
                            LeadMgr.getInstance(LeadIntentService.this).updateFailedPSFLead(leadModel, "offline",restResponse.body().getMessage().toLowerCase());

                        }else  if (restResponse.body().getCode().equalsIgnoreCase("4")){
                            leadModel.setWarning_message(restResponse.body().getMessage().toLowerCase());
                            leadModel.setStatus_code(Integer.valueOf(restResponse.body().getCode()));
                            LeadMgr.getInstance(LeadIntentService.this).updateFailedPSFLead(leadModel, "offline",restResponse.body().getMessage().toLowerCase());

                        }





                    }




                }*/ //dequick






                /*else if (restResponse.body().getStatus().equalsIgnoreCase(Constants.failed)){

                    if (statusCode == 301){

                    }else if (statusCode == 302){

                    }else if (statusCode == 303){


                    }



                    leadModel.setWarning_message(restResponse.body().getMessage());
                    leadModel.setStatus_code(301);
                    LeadMgr.getInstance(LeadIntentService.this).updateFailedPSFLead(leadModel, "offline",restResponse.body().getMessage());
                }*/

            //LeadMgr.getInstance(LeadIntentService.this).updateFailedPSFLead(leadModel, "offline","Test Warning");
            //}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void postPSFLead(final PSFLeadModel leadModel) {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> postLead = client.postPSFLead(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), leadModel);
        try {
            Response<RestResponse> restResponse = postLead.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                            try {
                                JSONObject jsonObject = new JSONObject(restResponse.body().getData());
                                if (jsonObject != null) {
                                    String leadid = jsonObject.getString("leadid");
                                    String leadcode = jsonObject.getString("leadcode");
                                    if (Utils.isNotNullAndNotEmpty(leadcode))
                                        leadModel.setLeadCode(leadcode);
                                    if (Utils.isNotNullAndNotEmpty(leadid)) {
                                        LeadMgr.getInstance(LeadIntentService.this).updatePSFLeadOnline(leadModel, userid, leadid);
                                        //PlannerMgr.getInstance(LeadIntentService.this).updateLeadid(leadModel.getId(), userid, leadid);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Intent intent = new Intent("lead_broadcast");
                            mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                            mgr.sendBroadcast(intent);
                        } else {
                            //LeadMgr.getInstance(LeadIntentService.this).updateLeaddumyOnline(leadModel, userid);
                        }
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //leads
    private void syncOfflineLeads() {
        final List<LeadModel> leadModelList = LeadMgr.getInstance(LeadIntentService.this).getOfflineLeadsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");
        if (leadModelList.size() > 0) {
            for (final LeadModel leadModel : leadModelList) {
                if (Utils.isNotNullAndNotEmpty(leadModel.getNetwork_status())) {
                    if (isNetworkAvailable()) {
                        if (TextUtils.isEmpty(leadModel.getLeadid())) {
                            postLead(leadModel);
                        } else {
                            updateLead(leadModel);
                        }
                    }
                }
            }
            PlannerIntentService.syncPlanstoServer(LeadIntentService.this);
        } else {
            PlannerIntentService.syncPlanstoServer(LeadIntentService.this);
        }
    }

    private void postLead(final LeadModel leadModel) {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> postLead = client.postLead(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), leadModel);
        try {
            Response<RestResponse> restResponse = postLead.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                            try {
                                JSONObject jsonObject = new JSONObject(restResponse.body().getData());
                                if (jsonObject != null) {
                                    String leadid = jsonObject.getString("leadid");
                                    String leadcode = jsonObject.getString("leadcode");
                                    if (Utils.isNotNullAndNotEmpty(leadcode))
                                        leadModel.setLeadcode(leadcode);
                                    if (Utils.isNotNullAndNotEmpty(leadid)) {
                                        LeadMgr.getInstance(LeadIntentService.this).updateLeadOnline(leadModel, userid, leadid);
                                        PlannerMgr.getInstance(LeadIntentService.this).updateLeadid(leadModel.getId(), userid, leadid);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Intent intent = new Intent("lead_broadcast");
                            mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                            mgr.sendBroadcast(intent);
                        } else {
                            LeadMgr.getInstance(LeadIntentService.this).updateLeaddumyOnline(leadModel, userid);
                        }
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateLead(final LeadModel leadModel) {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> updateLead = client.updateLead(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), leadModel.getLeadid(), leadModel);
        try {
            Response<RestResponse> restResponse = updateLead.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                        if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                            LeadMgr.getInstance(LeadIntentService.this).updateLead(leadModel, "");
                            Intent intent = new Intent("lead_broadcast");
                            mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                            mgr.sendBroadcast(intent);
                        }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getServerLeads() {
        String lastfetchtime = SharedPreferenceManager.getInstance().getString(Constants.LEAD_LAST_FETCH_TIME, "");
        List<LeadModel> leadsList = LeadMgr.getInstance(LeadIntentService.this).getLeadsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        if (leadsList.size() == 0) {
            lastfetchtime = "";
        }
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getLeads = client.getServerLeads(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), lastfetchtime);
        final String newFetchTime = TimeUtils.getCurrentUnixTimeStamp();
        getLeads.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<LeadModel>>() {
                        }.getType();
                        List<LeadModel> leadModelList = new Gson().fromJson(response.body().getData(), listType);
                        if (leadModelList.size() > 0) {
                            LeadMgr.getInstance(LeadIntentService.this).insertLeadsList(leadModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        SharedPreferenceManager.getInstance().putString(Constants.LEAD_LAST_FETCH_TIME, newFetchTime);
                        SharedPreferenceManager.getInstance().putString(Constants.LEADS_LOADED, "loaded");
                        Intent intent = new Intent("lead_broadcast");
                        mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                SharedPreferenceManager.getInstance().putString(Constants.LEADS_LOADED, "notloaded");
            }
        });
    }


    private void getServerPSFNotifications() {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getProposalCodes = client.getPsfNotifications(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        getProposalCodes.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFNotificationsModel>>() {
                        }.getType();
                        List<PSFNotificationsModel> psfNotificationsModelList = new Gson().fromJson(response.body().getData(), listType);
                        if (psfNotificationsModelList.size() > 0) {

                            LeadMgr.getInstance(LeadIntentService.this).insertPSFNotificationsList(psfNotificationsModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        SharedPreferenceManager.getInstance().putString(Constants.PSF_NOTIFICATIONS_LOADED, "loaded");
                        Intent intent = new Intent("psfnotifications_broadcast");
                        mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                SharedPreferenceManager.getInstance().putString(Constants.PSF_NOTIFICATIONS_LOADED, "notloaded");
            }
        });
    }


    private void updateAllPSFNotifications() {

        final List<PSFNotificationsModel> psfNotificationsModelList = LeadMgr.getInstance(LeadIntentService.this).getPSFNotificationsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        if (psfNotificationsModelList.size() > 0) {
            for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                if (isNetworkAvailable()) {

                    if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {


                        FormsService client = RestService.createServicev1(FormsService.class);
                        Call<RestResponse> getProposalCodes = client.updatePsfNotifications(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), notificationsModel.getNotifyid());
                        getProposalCodes.enqueue(new Callback<RestResponse>() {
                            @Override
                            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                                if (response.isSuccessful()) {
                                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {


                                    /*Type listType = new TypeToken<ArrayList<PSFNotificationsModel>>() {
                                    }.getType();
                                    List<PSFNotificationsModel>psfNotificationsModelList = new Gson().fromJson(response.body().getData(), listType);
                                    if (psfNotificationsModelList.size() > 0) {

                                        LeadMgr.getInstance(LeadIntentService.this).insertPSFNotificationsList(psfNotificationsModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                                    }
                                    SharedPreferenceManager.getInstance().putString(Constants.PSF_NOTIFICATIONS_LOADED, "loaded");
                                    Intent intent = new Intent("psfnotifications_broadcast");
                                    mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                                    mgr.sendBroadcast(intent);*/


                                       /* SharedPreferenceManager.getInstance().putString(Constants.PSF_NOTIFICATIONS_UPDATE, "updated");
                                        Intent intent = new Intent("psfnotificationsupdate_broadcast");
                                        mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                                        mgr.sendBroadcast(intent);*/
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<RestResponse> call, Throwable t) {
                                t.printStackTrace();
                                SharedPreferenceManager.getInstance().putString(Constants.PSF_NOTIFICATIONS_UPDATE, "notupdated");
                            }
                        });

                    }

                }


            }
        }


    }


    private void getServerPSFLeads() {
        String lastfetchtime = SharedPreferenceManager.getInstance().getString(Constants.LEAD_LAST_FETCH_TIME, "");
        List<LeadModel> leadsList = LeadMgr.getInstance(LeadIntentService.this).getLeadsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        if (leadsList.size() == 0) {
            lastfetchtime = "";
        }
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getLeads = client.getServerPSFLeads(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), SharedPreferenceManager.getInstance().getString(Constants.USERID, "")
                , SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), lastfetchtime);
        final String newFetchTime = TimeUtils.getCurrentUnixTimeStamp();
        getLeads.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFLeadModel>>() {
                        }.getType();
                        List<PSFLeadModel> leadModelList = new Gson().fromJson(response.body().getData(), listType);
                        if (leadModelList.size() > 0) {
                            LeadMgr.getInstance(LeadIntentService.this).insertPSFLeadsList(leadModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        SharedPreferenceManager.getInstance().putString(Constants.LEAD_LAST_FETCH_TIME, newFetchTime);
                        SharedPreferenceManager.getInstance().putString(Constants.LEADS_LOADED, "loaded");
                        Intent intent = new Intent("lead_broadcast");
                        mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                SharedPreferenceManager.getInstance().putString(Constants.LEADS_LOADED, "notloaded");
            }
        });
    }

    private void getProposalCodes() {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getProposalCodes = client.getProposalCodes(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        getProposalCodes.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<ProposalCodesModel>>() {
                        }.getType();
                        List<ProposalCodesModel> proposalCodesModelList = new Gson().fromJson(response.body().getData(), listType);
                        if (proposalCodesModelList.size() > 0) {
                            ProposalCodesDao.getInstance(LeadIntentService.this).insertProposalCodesList(proposalCodesModelList, SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
                        }
                        SharedPreferenceManager.getInstance().putString(Constants.PROPOSAL_CODES_LOADED, "loaded");
                        Intent intent = new Intent("pcodes_broadcast");
                        mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                SharedPreferenceManager.getInstance().putString(Constants.PROPOSAL_CODES_LOADED, "notloaded");
            }
        });
    }

}