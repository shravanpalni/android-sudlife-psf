package com.outwork.sudlife.psf.lead.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.services.CustomerService;
import com.outwork.sudlife.psf.dao.DBHelper;
import com.outwork.sudlife.psf.dao.IvokoProvider;
import com.outwork.sudlife.psf.lead.activities.PSFLeadListActivity;
import com.outwork.sudlife.psf.lead.activities.PSFViewLeadActivity;
import com.outwork.sudlife.psf.lead.model.CalendarSectionHeader;
import com.outwork.sudlife.psf.lead.model.ChildViewHolder;
import com.outwork.sudlife.psf.lead.model.PSFFlsModel;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;
import com.outwork.sudlife.psf.lead.model.SectionViewHolder;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.models.Userprofile;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by apple on 11/7/16.
 */

public class AdapterSectionRecycler extends SectionRecyclerViewAdapter<CalendarSectionHeader, PSFLeadModel, SectionViewHolder, ChildViewHolder> {

    Context context;
    private ArrayList<Userprofile> flsList = new ArrayList();
    private ArrayList<Userprofile> showflsList = new ArrayList();
    private String selectedTeamId;

    public AdapterSectionRecycler(Context context, List<CalendarSectionHeader> sectionHeaderItemList,String selectedTeamId) {
        super(context, sectionHeaderItemList);
        this.context = context;
        this.selectedTeamId = selectedTeamId;
    }

    @Override
    public SectionViewHolder onCreateSectionViewHolder(ViewGroup sectionViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.calendar_section, sectionViewGroup, false);
        return new SectionViewHolder(view);
    }

    @Override
    public ChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.psflead_list_item, childViewGroup, false);
        return new ChildViewHolder(view);
    }

    @Override
    public void onBindSectionViewHolder(SectionViewHolder sectionViewHolder, int sectionPosition, CalendarSectionHeader sectionHeader) {
        sectionViewHolder.name.setText(sectionHeader.getSectionText());
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder childViewHolder, int sectionPosition, int childPosition, PSFLeadModel child) {
        //childViewHolder.name.setText(child.getProductName());



        StringBuilder stringBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(child.getPolicyHolderFirstName()))
            stringBuilder.append(child.getPolicyHolderFirstName());
        if (Utils.isNotNullAndNotEmpty(child.getPolicyHolderLastName()))
            stringBuilder.append(" " + child.getPolicyHolderLastName());
        StringBuilder branchBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(child.getLeadSource()))
            branchBuilder.append(child.getLeadSource());
      /*  if (Utils.isNotNullAndNotEmpty(dto.getBank()))
            branchBuilder.append(" ( " + dto.getBank() + " )");*/
        childViewHolder.tvName.setText(stringBuilder.toString());
        childViewHolder.tvCityName.setText(branchBuilder.toString());
        if (Utils.isNotNullAndNotEmpty(child.getLeadStage()))
            childViewHolder.tvLeadStatus.setText(child.getLeadStage());

        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
            if (Utils.isNotNullAndNotEmpty(child.getLeadStage())) {

                if (child.getLeadStage().equalsIgnoreCase("Unallocated")) {
                    if (!(child.getLeadSource().equalsIgnoreCase("Reference/Self"))) {

                        childViewHolder.allocate.setVisibility(View.VISIBLE);
                    }


                }

                if (child.getLeadStage().equalsIgnoreCase("Pre-Fixed Appointment")) {

                    if (!(child.getLeadSource().equalsIgnoreCase("Reference/Self"))) {

                        childViewHolder.reallocate.setVisibility(View.VISIBLE);
                    }


                }

            }

        }


        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {

            if (Utils.isNotNullAndNotEmpty(child.getLeadStage())) {

                if (child.getLeadStage().equalsIgnoreCase("Pre-Fixed Appointment")) {

                    if (!(child.getLeadSource().equalsIgnoreCase("Reference/Self"))) {
                        childViewHolder.reject.setVisibility(View.VISIBLE);
                    }


                }

            }

        }


        if (Utils.isNotNullAndNotEmpty(child.getLeadCode()))
            childViewHolder.leadcode.setText(child.getLeadCode());
        if (Utils.isNotNullAndNotEmpty(child.getNetwork_status())) {
            if (child.getNetwork_status().equalsIgnoreCase("offline")) {
                childViewHolder.status.setText("Offline");
                childViewHolder.status.setVisibility(View.VISIBLE);

            } else {
                childViewHolder.status.setVisibility(View.GONE);
            }
        } else {
            childViewHolder.status.setVisibility(View.GONE);
        }


        childViewHolder.allocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!(selectedTeamId.equalsIgnoreCase("") && selectedTeamId != null)){

                    getTeamHierarchyMembers(selectedTeamId, child.getLeadId().toString());
                }else {


                }


            }
        });

        childViewHolder.reallocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!(selectedTeamId.equalsIgnoreCase("") && selectedTeamId != null)){

                    getTeamHierarchyMembers(selectedTeamId, child.getLeadId().toString());
                }else {

                }


            }
        });
        childViewHolder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PSFFlsModel psfFlsModel = new PSFFlsModel();
                psfFlsModel.setLeadid(child.getLeadId());
                psfFlsModel.setAssignedtoid(child.getBmUserid());
                psfFlsModel.setLeadstage("Unallocated");
                rejectLead(psfFlsModel);


            }
        });


        childViewHolder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PSFViewLeadActivity.class);
                intent.putExtra("leadobj", new Gson().toJson(child));
                context.startActivity(intent);
            }
        });




    }



    private void getTeamHierarchyMembers(String teamid, String leadid) {
        //showProgressDialog("Loading, please wait....");
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, teamid);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    // dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                        Log.i("shravan", "response = = =" + response.body().toString());

                        flsList = new ArrayList();
                        showflsList =  new ArrayList();
                        ArrayList<Userprofile> rawflsList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                        }.getType());
                        if (Utils.isNotNullAndNotEmpty(rawflsList)) {
                            for (Userprofile up : rawflsList) {

                                flsList.add(up);

                                if (!(up.getUserid().equalsIgnoreCase(SharedPreferenceManager.getInstance().getString(Constants.USERID, "")))) {

                                    showflsList.add(up);
                                }
                            }

                            showDialog(leadid);


                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

            }
        });
    }

    private void rejectLead(PSFFlsModel psfFlsModel) {
        //showProgressDialog("Loading, please wait....");
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getReject(userToken, psfFlsModel);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    // dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                        Log.i("shravan", "response = = =" + response.body().toString());

                        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
                        db.execSQL("delete from " + IvokoProvider.PSFLead.TABLE + " WHERE " + IvokoProvider.PSFLead.leadid + " = '" + psfFlsModel.getLeadid() + "'");
                       /* if (isTableExists(db, IvokoProvider.PSFLead.TABLE)) {
                        }*/


                        if (context instanceof PSFLeadListActivity) {
                            ((PSFLeadListActivity) context).refreshData();
                        }


                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

            }
        });
    }


    private void showDialog(String leadid) {


        final Dialog dialog = new Dialog(context);

        //dialog.setTitle("Select FLS");

        View view = ((Activity) context).getLayoutInflater().inflate(R.layout.userteam_dialog, null);

        ListView lv = (ListView) view.findViewById(R.id.memberList);
        FlsAdapter clad = new FlsAdapter(context, showflsList);
        lv.setAdapter(clad);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                Userprofile dto = (Userprofile) parent.getItemAtPosition(position);
                String flsuserid = dto.getUserid();
                Log.i("shravan", "PSFLeadsAdapter  -- - flsuserid===" + flsuserid);
                Log.i("shravan", "PSFLeadsAdapter  -- - leadid===" + leadid);

                PSFFlsModel psfFlsModel = new PSFFlsModel();
                psfFlsModel.setLeadid(leadid);
                psfFlsModel.setAssignedtoid(flsuserid);
                psfFlsModel.setLeadstage("Pre-Fixed Appointment");
                pushAllocated(psfFlsModel);
                dialog.cancel();
               /* UserTeams dto = (UserTeams) parent.getItemAtPosition(position);
                //Intent intent = new Intent(CalenderActivity.this, TeamHierarchyActivity.class);
                Intent intent = new Intent(CalenderActivity.this, ShowHierarchyActivity.class);
                intent.putExtra(Constants.PLAN_STATUS, 0);
                intent.putExtra(Constants.PLAN_DATE, displaydate);
                intent.putExtra("USERTEAMID", dto.getTeamid());
                startActivity(intent);
                dialog.cancel();
                bottomSheetDialog.dismiss();*/


            }
        });

        dialog.setContentView(view);

        dialog.show();

    }


    private void pushAllocated(PSFFlsModel psfFlsModel) {
        //showProgressDialog("Loading, please wait....");
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getAllocation(userToken, psfFlsModel);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    // dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                        Log.i("shravan", "response = = =" + response.body().toString());


                        if (context instanceof PSFLeadListActivity) {
                            ((PSFLeadListActivity) context).refreshData();
                        }


                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

            }
        });
    }

}
