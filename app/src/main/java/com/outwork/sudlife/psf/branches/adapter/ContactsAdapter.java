package com.outwork.sudlife.psf.branches.adapter;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.models.ContactModel;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder> {

    private List<ContactModel> contactsList;
    private Activity activity;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, designation, phone, email;
        public TextView editbutton;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.namefield);
            designation = (TextView) view.findViewById(R.id.designfield);
            phone = (TextView) view.findViewById(R.id.mobilefield);
            email = (TextView) view.findViewById(R.id.emailfield);
            editbutton = (TextView) view.findViewById(R.id.contacteditbutton);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, name);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, designation, phone, email);
            Utils.setTypefaces(IvokoApplication.robotoThinTypeface, (TextView) view.findViewById(R.id.designationlbl),
                    (TextView) view.findViewById(R.id.phonelbl),
                    (TextView) view.findViewById(R.id.emaillbl));
        }
    }

    public ContactsAdapter(Activity activity, List<ContactModel> contactModelsList) {
        this.activity = activity;
        this.contactsList = contactModelsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ContactModel contactModel = contactsList.get(position);
        String name = "";

        if (Utils.isNotNullAndNotEmpty(contactModel.getCdisplayname())) {
            name = contactModel.getCdisplayname();
        } else if (Utils.isNotNullAndNotEmpty(contactModel.getCfirstname())) {
            name = contactModel.getCfirstname();
        } else if (Utils.isNotNullAndNotEmpty(contactModel.getClname())) {
            name = contactModel.getClname();
        }
        if (Utils.isNotNullAndNotEmpty(contactModel.getSalutation())) {
            holder.name.setText(contactModel.getSalutation() + " " + name);
        } else {
            holder.name.setText(name);
        }
        holder.designation.setText(contactModel.getDesignation());
        holder.phone.setText(contactModel.getPhoneno());
        holder.email.setText(contactModel.getEmail());
//        holder.editbutton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.i("ContactsAdapter","position = = = ="+position);
//                String namefield=  holder.name.getText().toString();
//                String destfield =  holder.designation.getText().toString();
//                String phfield =  holder.phone.getText().toString();
//                String emaifield =  holder.email.getText().toString();
//                Log.i("ContactsAdapter","namefield = = = ="+namefield);
//                Log.i("ContactsAdapter","destfield = = = ="+destfield);
//                Log.i("ContactsAdapter","phfield = = = ="+phfield);
//                Log.i("ContactsAdapter","emaifield = = = ="+emaifield);
//                Intent intent = new Intent(activity, ContactEditActivity.class);
//                intent.putExtra("namefield",namefield);
//                intent.putExtra("destfield",destfield);
//                intent.putExtra("phfield",phfield);
//                intent.putExtra("emaifield",emaifield);
//
//                activity.startActivity(intent);
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }
}
