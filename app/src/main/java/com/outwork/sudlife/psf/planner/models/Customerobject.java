package com.outwork.sudlife.psf.planner.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Customerobject implements Serializable {
    @SerializedName("branchcode")
    @Expose
    private String branchcode;
    @SerializedName("customername")
    @Expose
    private String customername;
    @SerializedName("customertype")
    @Expose
    private String customertype;
    @SerializedName("customerid")
    @Expose
    private String customerid;

    public Customerobject(String branchcode, String customername, String customertype, String customerid) {
        this.branchcode = branchcode;
        this.customername = customername;
        this.customertype = customertype;
        this.customerid = customerid;
    }

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomertype() {
        return customertype;
    }

    public void setCustomertype(String customertype) {
        this.customertype = customertype;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }
}