package com.outwork.sudlife.psf.more.models;

import android.os.Parcel;
import android.os.Parcelable;

public class PollAnswer implements Parcelable {
    private String optionNr;
    private String optionText;
    private int voteCount;
    private float optionPercentage;


    public int getHighest() {
        return highest;
    }

    public void setHighest(int highest) {
        this.highest = highest;
    }

    private int highest;

    public PollAnswer(){
    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    public String getOptionNr() {
        return optionNr;
    }

    public void setOptionNr(String optionNr) {
        this.optionNr = optionNr;
    }

    public float getOptionPercentage() {
        return optionPercentage;
    }

    public void setOptionPercentage(float optionPercentage) {
        this.optionPercentage = optionPercentage;
    }

    public int getVoteCount() {
        return voteCount;
    }
    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    protected PollAnswer(Parcel in) {
        this.optionNr = in.readString();
        this.optionText = in.readString();
        this.voteCount = in.readInt();
        this.optionPercentage = in.readFloat();
        this.highest = in.readInt();
    }

    public static final Creator<PollAnswer> CREATOR = new Creator<PollAnswer>() {
        @Override
        public PollAnswer createFromParcel(Parcel in) {
            return new PollAnswer(in);
        }


        @Override
        public PollAnswer[] newArray(int size) {
            return new PollAnswer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(optionNr);
        dest.writeString(optionText);
        dest.writeInt(voteCount);
        dest.writeFloat(optionPercentage);
        dest.writeInt(highest);
    }
}
