package com.outwork.sudlife.psf.opportunity.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.opportunity.OpportunityMgr;
import com.outwork.sudlife.psf.opportunity.models.OpportunityModel;
import com.outwork.sudlife.psf.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

public class EditOpportunityActivity extends BaseActivity {

    private TextView oppstage;
    private EditText firstname, lastname, age, occupation, income, familymem, remarks, opprtValue, closingDate;
    private AppCompatSpinner fstage;
    private OpportunityModel opportunityModel;
    private String sOrderDate, sclosingDate;
    private List<String> stagelist = new ArrayList<>();
    private RadioGroup gender;
    private RadioButton male, female;
    private ArrayAdapter<String> stageAdapter;
    private BroadcastReceiver mBroadcastReceiver;
    private boolean isupdated = false;
    private LocalBroadcastManager mgr;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                String orderdt = TimeUtils.getFormattedDate(date);
                sOrderDate = TimeUtils.convertDatetoUnix(orderdt);
            } else if (customSelector == 1) {
                closingDate.setText(TimeUtils.getDateFormat(date));
                closingDate.setEnabled(true);
                sclosingDate = TimeUtils.convertDatetoUnix(TimeUtils.getDateFormat(date));
            }
        }

        // Optional cancel listener
        @Override
        public void onDateTimeCancel() {
            closingDate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opport_form);

        mgr = LocalBroadcastManager.getInstance(this);
        userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("FORMDTO")) {
            opportunityModel = (OpportunityModel) getIntent().getSerializableExtra("FORMDTO");
        }

        firstname = (EditText) findViewById(R.id.firstName);
        lastname = (EditText) findViewById(R.id.lastName);
        gender = (RadioGroup) findViewById(R.id.gender);
        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);
        age = (EditText) findViewById(R.id.age);
        occupation = (EditText) findViewById(R.id.occupation);
        income = (EditText) findViewById(R.id.income);
        familymem = (EditText) findViewById(R.id.familymem);
        remarks = (EditText) findViewById(R.id.fRemarks);
        closingDate = (EditText) findViewById(R.id.closingDate);
        oppstage = (TextView) findViewById(R.id.oppstage);
        opprtValue = (EditText) findViewById(R.id.fOpprtValue);
        fstage = (AppCompatSpinner) findViewById(R.id.fstage);

        initToolBar();
        initValues();
        setListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("oppstage_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        saveOpportunity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.order_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            saveOpportunity();
            return true;
        }
        if (item.getItemId() == R.id.save) {
            saveOpportunity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initValues() {
        opportunityModel = OpportunityMgr.getInstance(EditOpportunityActivity.this).getOfflineOpportunityByID(opportunityModel.getId(), userid);
        String navlblText = "NA";

        if (!TextUtils.isEmpty(opportunityModel.getFirstname())) {
            firstname.setText(opportunityModel.getFirstname());
        } else {
            firstname.setText(navlblText);
        }
        if (!TextUtils.isEmpty(opportunityModel.getLastname())) {
            lastname.setText(opportunityModel.getLastname());
        } else {
            lastname.setText(navlblText);
        }
        if (opportunityModel.getAge() != null) {
            age.setText(opportunityModel.getAge());
        } else {
            age.setText(navlblText);
        }
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getGender())) {
            if (opportunityModel.getGender().equalsIgnoreCase("Male"))
                male.setChecked(true);
            if (opportunityModel.getGender().equalsIgnoreCase("Female"))
                female.setChecked(true);
        }
        if (!TextUtils.isEmpty(opportunityModel.getOccupation())) {
            occupation.setText(opportunityModel.getOccupation());
        } else {
            occupation.setText(navlblText);
        }
        if (opportunityModel.getIncome() != null) {
            income.setText(opportunityModel.getIncome());
        } else {
            income.setText(navlblText);
        }
        if (opportunityModel.getFamilymembers() != null) {
            familymem.setText(opportunityModel.getFamilymembers());
        } else {
            familymem.setText(navlblText);
        }
        if (!TextUtils.isEmpty(opportunityModel.getRemarks())) {
            remarks.setText(opportunityModel.getRemarks());
        }
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getValue())) {
            opprtValue.setText(opportunityModel.getValue());
        } else {
            opprtValue.setText("0");
        }
        if (!TextUtils.isEmpty(opportunityModel.getClosingdate())) {
            closingDate.setText(TimeUtils.getFormattedDatefromUnix(opportunityModel.getClosingdate(), "dd/MM/yyyy"));
        }
        getStages();
    }

    private void getStages() {
        stagelist = OpportunityMgr.getInstance(EditOpportunityActivity.this).getOpportunitystagesnamesList(userid);
        if (stagelist.size() > 0) {
            stageAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, stagelist);
            stageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            fstage.setAdapter(stageAdapter);
            for (int i = 0; i < stagelist.size(); i++)
                if (Utils.isNotNullAndNotEmpty(opportunityModel.getStage()))
                    if (opportunityModel.getStage().equalsIgnoreCase(stagelist.get(i).toString())) {
                        fstage.setSelection(i);
                    }
            fstage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    fstage.setSelection(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } else {
            oppstage.setVisibility(View.GONE);
            fstage.setVisibility(View.GONE);
        }
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Edit Opportunity");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private void setListeners() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getStages();
            }
        };
        remarks.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.fRemarks) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        closingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closingDate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 0);
                Date backDate = calendar.getTime();
                calendar.add(Calendar.DAY_OF_YEAR, +15);
                Date initDate = calendar.getTime();
                calendar.add(Calendar.DAY_OF_YEAR, 100);
                Date maxDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(backDate)
                        .setInitialDate(initDate)
                        .setMaxDate(maxDate)
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });
        gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                RadioButton rb = (RadioButton) findViewById(checkedId);
                opportunityModel.setGender(rb.getText().toString());
            }
        });
    }

    private boolean validate() {
        if (!Utils.isNotNullAndNotEmpty(firstname.getText().toString())) {
            firstname.setError("FirstName is mandatory");
            firstname.requestFocus();
            return false;
        } else if (opprtValue.getText().toString().isEmpty()) {
            opprtValue.setError("value is mandatory");
            opprtValue.requestFocus();
            return false;
        } else if (closingDate.getText().toString().isEmpty()) {
            closingDate.setError("Closing date is mandatory");
            closingDate.setFocusableInTouchMode(true);
            closingDate.requestFocus();
            return false;
        } else if (remarks.getText().toString().isEmpty()) {
            remarks.setError("Description is mandtory");
            remarks.requestFocus();
            return false;
        }
        return true;
    }

    private void saveOpportunity() {
        if (validate()) {
            updateOpportunity();
            if (isupdated) {
                showAlert("", "Your changes are updated successfully",
                        "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                OpportunityMgr.getInstance(EditOpportunityActivity.this).updateLocalOpportunity(opportunityModel, userid);
                                OpportunityIntentService.syncOfflineRecords(EditOpportunityActivity.this);
                                finish();
                            }
                        },
                        "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        }, false);
            } else {
                finish();
            }

        }
    }

    private void updateOpportunity() {
        opportunityModel.setUserid(userid);
        if (Utils.isNotNullAndNotEmpty(firstname.getText().toString())) {
            if (!firstname.getText().toString().equals(opportunityModel.getFirstname())) {
                isupdated = true;
                opportunityModel.setFirstname(firstname.getText().toString());
            }
        }
        if (Utils.isNotNullAndNotEmpty(lastname.getText().toString())) {
            if (!lastname.getText().toString().equals(opportunityModel.getLastname())) {
                isupdated = true;
                opportunityModel.setLastname(lastname.getText().toString());
            }
        }
        if (Utils.isNotNullAndNotEmpty(opprtValue.getText().toString())) {
            if (!opprtValue.getText().toString().equals(opportunityModel.getValue())) {
                isupdated = true;
                opportunityModel.setValue(opprtValue.getText().toString());
            }
        }
        if (stagelist.size() > 0)
            if (Utils.isNotNullAndNotEmpty(fstage.getSelectedItem().toString())) {
                if (!fstage.getSelectedItem().toString().equals(opportunityModel.getStage())) {
                    isupdated = true;
                    opportunityModel.setStage(fstage.getSelectedItem().toString());
                }
            }
        if (Utils.isNotNullAndNotEmpty(sclosingDate)) {
            if (!sclosingDate.equals(opportunityModel.getClosingdate())) {
                isupdated = true;
                opportunityModel.setClosingdate(sclosingDate);
            }
        }
        if (Utils.isNotNullAndNotEmpty(remarks.getText().toString())) {
            if (!remarks.getText().toString().equals(opportunityModel.getRemarks())) {
                isupdated = true;
                opportunityModel.setRemarks(remarks.getText().toString());
            }
        }
    }
}
