package com.outwork.sudlife.psf.planner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.planner.models.DataModel;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Tatson on 28-Mar-17.
 */
//http://www.tatnorix.in/android-listview-checkbox-scroll-problem/
public class MultipleItemsSelectionAdapter extends BaseAdapter {

    Context mContext;
    List<DataModel> linkedList;
    protected LayoutInflater vi;

    private HashMap<DataModel, Boolean> checkedForCountry = new HashMap<>();

    public MultipleItemsSelectionAdapter(Context context, List<DataModel> linkedList) {
        this.mContext = context;
        this.linkedList = linkedList;
        this.vi = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return linkedList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = vi.inflate(R.layout.list_item_of_multi_select, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.tv_title);
            holder.selectionBox = (CheckBox) convertView.findViewById(R.id.cb_itemselect);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, holder.title);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final DataModel dataModel = linkedList.get(position);
        holder.title.setText(dataModel.getName());
        holder.selectionBox.setChecked(dataModel.isEnabledValue());
        holder.selectionBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    isdataUpdated(position, true);
                } else {
                    isdataUpdated(position, false);
                }
            }
        });

        if (dataModel != null) {
            holder.selectionBox.setChecked(dataModel.isEnabledValue());
        }
        holder.selectionBox.setTag(dataModel);
        return convertView;
    }


    private class ViewHolder {
        TextView title;
        CheckBox selectionBox;
    }

    public void ischecked(int position, boolean flag) {
        checkedForCountry.put(this.linkedList.get(position), flag);
    }

    public void isdataUpdated(int position, boolean updatedValue) {
        linkedList.get(position).setEnabledValue(updatedValue);
    }

    public LinkedList<String> getSelectedCountry() {
        LinkedList<String> List = new LinkedList<>();
        for (Map.Entry<DataModel, Boolean> pair : checkedForCountry.entrySet()) {
            if (pair.getValue()) {
                List.add(pair.getKey().getName());
            }
        }
        return List;
    }

    public List<DataModel> getSelectedItems() {
        return linkedList;
    }


}