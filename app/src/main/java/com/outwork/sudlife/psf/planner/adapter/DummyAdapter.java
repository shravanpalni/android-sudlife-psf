package com.outwork.sudlife.psf.planner.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shravanch on 14-12-2019.
 */

public class DummyAdapter extends RecyclerView.Adapter<DummyAdapter.TaskViewHolder> {
    private Context context;
    private final LayoutInflater mInflater;
    private List<PlannerModel> plannerModelList = new ArrayList<>();

    public static class TaskViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public TextView name;
        private CardView card_view;
        public TextView line2, status,offline;
        //View offline;

        public TaskViewHolder(View v) {
            super(v);
            picture = (ImageView) v.findViewById(R.id.profileImage);
            name = (TextView) v.findViewById(R.id.name1);
            line2 = (TextView) v.findViewById(R.id.name2);
            status = (TextView) v.findViewById(R.id.status);
            card_view = (CardView) v.findViewById(R.id.card_view);
            offline = (TextView) v.findViewById(R.id.networkstatus);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, name);
        }
    }

    public DummyAdapter(Context context, List<PlannerModel> plannerModelList) {
        this.context = context;
        this.plannerModelList = plannerModelList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public DummyAdapter.TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.calender_dummy_item, parent, false);
        return new DummyAdapter.TaskViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DummyAdapter.TaskViewHolder holder, final int position) {
        final PlannerModel plannerModel = (PlannerModel) this.plannerModelList.get(position);



       /* holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNotNullAndNotEmpty(plannerModel.getOpportunityid()) || (plannerModel.getLocalopportunityid() != null && plannerModel.getLocalopportunityid() > 0)) {
                    Intent intent = new Intent(context, OpportunityViewActivity.class);
                    intent.putExtra("plannerModel", new Gson().toJson(plannerModel));
                    context.startActivity(intent);
                } else if (Utils.isNotNullAndNotEmpty(plannerModel.getLeadid()) || (plannerModel.getLocalleadid() != null && plannerModel.getLocalleadid() > 0)) {
                    if (plannerModel.getCompletestatus() == 0) {
                        Intent intent = new Intent(context, ViewActivityLeadActivity.class);
                        intent.putExtra("plannerModel", new Gson().toJson(plannerModel));
                        context.startActivity(intent);
                    }
                    if (plannerModel.getCompletestatus() == 2) {
                        LeadModel leadModel = new LeadModel();
                        if (Utils.isNotNullAndNotEmpty(plannerModel.getLeadid())) {
                            leadModel = LeadMgr.getInstance(context).getLeadbyId(plannerModel.getLeadid(), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        } else if (plannerModel.getLocalleadid() != null && plannerModel.getLocalleadid() != 0) {
                            leadModel = LeadMgr.getInstance(context).getLeadbylocalId(plannerModel.getLocalleadid(), SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        Intent intent = new Intent(context, ViewLeadActivity.class);
                        intent.putExtra("leadobj", new Gson().toJson(leadModel));
                        context.startActivity(intent);
                    }
                } else {
                    if (plannerModel.getCompletestatus() == 0 || plannerModel.getCompletestatus() == 1) {
                        if (plannerModel.getScheduletime() != null && plannerModel.getScheduletime() != 0)
                            if (TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy")
                                    .equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
                                Intent intent = new Intent(context, ViewActivity.class);
                                intent.putExtra("plannerModel", new Gson().toJson(plannerModel));
                                context.startActivity(intent);
                            } else {
                                if (Utils.isNotNullAndNotEmpty(plannerModel.getActivityid())) {
                                    Intent intent = new Intent(context, ViewplannedActivity.class);
                                    intent.putExtra("plannerModel", new Gson().toJson(plannerModel));
                                    context.startActivity(intent);
                                }
                            }
                    } else {
                        if (Utils.isNotNullAndNotEmpty(plannerModel.getActivityid())) {
                            Intent intent = new Intent(context, ViewplannedActivity.class);
                            intent.putExtra("plannerModel", new Gson().toJson(plannerModel));
                            context.startActivity(intent);
                        }
                    }
                }
            }
        });*/
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return plannerModelList.size();
    }
}
