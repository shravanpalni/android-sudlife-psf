package com.outwork.sudlife.psf.branches.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.BranchesMgr;
import com.outwork.sudlife.psf.branches.models.BranchesModel;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

/**
 * Created by Panch on 2/23/2017.
 */
public class CustomerOnlineAdapter extends BaseAdapter implements Filterable {
    private Context context;
    private final LayoutInflater mInflater;
    private List<BranchesModel> customerList = new ArrayList<>();
    private List<BranchesModel> originalcustList = new ArrayList<>();
    private String groupid, userid;
    private CustomerFilter mCustomerFilter;

    static class ViewHolder {
        public TextView name;
        public ImageView picture;
        public TextView line2, offline;
    }

    public CustomerOnlineAdapter(Context context, List<BranchesModel> custList, String groupId, String userId) {
        this.context = context;
        this.customerList = custList;
        this.originalcustList = custList;
        this.groupid = groupId;
        this.userid = userId;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.customerList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this.customerList.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item_customer, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.picture = (ImageView) view.findViewById(R.id.profileImage);
            viewHolder.name = (TextView) view.findViewById(R.id.custname);
            viewHolder.line2 = (TextView) view.findViewById(R.id.custLine2);
            viewHolder.offline = (TextView) view.findViewById(R.id.offline);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, viewHolder.name);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, viewHolder.line2, viewHolder.offline);
            view.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        BranchesModel dto = (BranchesModel) this.customerList.get(position);
        holder.name.setText(dto.getCustomername());
        if (dto.getCustomername().length() > 0) {
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate color based on a key (same key returns the same color), useful for list/grid views
            int color2 = generator.getColor(dto.getCustomername().substring(0, 1));
            TextDrawable drawable = TextDrawable.builder().buildRound(dto.getCustomername().substring(0, 1), color2);

            if (drawable != null) {
                holder.picture.setImageDrawable(drawable);
            }
        }
        if (Utils.isNotNullAndNotEmpty(dto.getCustomertype())) {
            holder.line2.setVisibility(View.VISIBLE);
            holder.line2.setText(dto.getCustomertype());
        } else {
            holder.line2.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(dto.getNetwork_status())) {
            holder.offline.setVisibility(View.VISIBLE);
        } else {
            holder.offline.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        if (mCustomerFilter == null)
            mCustomerFilter = new CustomerFilter();
        return mCustomerFilter;
    }

    private class CustomerFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {


            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();

            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) >= 1 &&
                    SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) < 5) {


                if (constraint == null || constraint.length() == 0 || TextUtils.isEmpty(constraint)) {
                    results.values = originalcustList;
                    results.count = originalcustList.size();
                } else {
                    final List<BranchesModel> list = originalcustList;
                    int count = list.size();
                    final ArrayList<BranchesModel> nlist = new ArrayList<BranchesModel>();

                    for (BranchesModel branchesModelList : list) {

                        if ((branchesModelList.getCustomername().toLowerCase()).contains(constraint.toString().toLowerCase())) {

                            nlist.add(branchesModelList);

                        }
                    }

                    results.values = nlist;
                    results.count = nlist.size();
                }


            } else {

                if (constraint == null || constraint.length() == 0 || TextUtils.isEmpty(constraint)) {
                    results.values = originalcustList;
                    results.count = originalcustList.size();
                } else {
                    List<BranchesModel> fileredCustList = new BranchesMgr(context)
                            .getonlineCustomers(groupid, constraint.toString(), userid, "");
                    results.values = fileredCustList;
                    results.count = fileredCustList.size();
                }


            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (results != null) {
                customerList = (ArrayList<BranchesModel>) results.values;
                notifyDataSetChanged();
            } else {
                customerList = originalcustList;
                notifyDataSetChanged();
            }
        }
    }
}