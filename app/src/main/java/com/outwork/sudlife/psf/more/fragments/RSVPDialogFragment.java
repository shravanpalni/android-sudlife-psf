package com.outwork.sudlife.psf.more.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.outwork.sudlife.psf.R;

public class RSVPDialogFragment extends DialogFragment {

    private EditText guestCount;
    private Button btndismiss,rsvp;
    private SwitchCompat rsvpSwitch;
    private String guests;
    private boolean rsvpresponse;

    public interface RsvpDialogListener {
        void onRsvp(String guestcount, boolean going);
    }

    public RSVPDialogFragment() {
        // Empty constructor is required for QBDialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static RSVPDialogFragment newInstance(String title) {
        RSVPDialogFragment frag = new RSVPDialogFragment();

        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = getArguments();

        if (b != null){
            if (b.containsKey("rsvpresponse")){
                rsvpresponse = b.getBoolean("rsvpresponse",false);
            }
            if (b.containsKey("guests")){
                guests = b.getString("guests","");

            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_rsvp, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
      //  String title = getArguments().getString("rsvpTitle", "RSVP");
        getDialog().setTitle("RSVP");
        // Get field from view
        CharSequence text = "1000";


        guestCount = (EditText) view.findViewById(R.id.guestCount);
        float width = guestCount.getPaint().measureText(text, 0, text.length());
        guestCount.setMaxWidth(200);
        btndismiss   = (Button)view.findViewById(R.id.dismiss);
        rsvp = (Button)view.findViewById(R.id.rsvp);
        rsvpSwitch = (SwitchCompat)view.findViewById(R.id.rsvpSwitch);

        if (rsvpresponse){
            rsvpSwitch.setChecked(true);
        }

        if (!TextUtils.isEmpty(guests)){
            guestCount.setText(guests);
        }

        btndismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        rsvp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RsvpDialogListener listener = (RsvpDialogListener)getActivity();
                String rsvpguest="0";
                if (guestCount.getText().toString().length()>0){
                    rsvpguest = guestCount.getText().toString();
                }
                listener.onRsvp(rsvpguest,rsvpSwitch.isChecked());
                dismiss();
            }
        });
        // Fetch arguments from bundle and set title

        // Show soft keyboard automatically and request focus to field
       // mEditText.requestFocus();
        /*getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);*/
    }
}