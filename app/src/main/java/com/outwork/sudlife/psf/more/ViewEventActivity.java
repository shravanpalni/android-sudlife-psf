package com.outwork.sudlife.psf.more;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.more.fragments.RSVPDialogFragment;
import com.outwork.sudlife.psf.more.models.EventDto;
import com.outwork.sudlife.psf.more.models.RSVP;
import com.outwork.sudlife.psf.more.service.EventService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewEventActivity extends BaseActivity implements RSVPDialogFragment.RsvpDialogListener {

    private ImageView cancel;
    private TextView eventTitle, eventOrganizer, eventDate, eventTime, eventAddress, eventLocation, rsvpText;
    private WebView eventDescription;
    private Button btnrsvp;
    private String eventId, userToken, userId;
    private EventDto eventDto;
    private String eventdisplayDate, eventdisplayTime;
    private int rsvp;

    //  private BranchUniversalObject eventBranchObject;
    private String branchImage;
    private StringBuilder plaintext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event);

        userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        userId = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
        branchImage = SharedPreferenceManager.getInstance().getString(Constants.GRP_IMAGE, "");
        Bundle b = getIntent().getExtras();
        eventId = b.getString("eventid");

        initializeToolBar();

        eventTitle = (TextView) findViewById(R.id.veventTitle);
        eventOrganizer = (TextView) findViewById(R.id.veventorganiser);
        eventDate = (TextView) findViewById(R.id.veventDate);
        eventTime = (TextView) findViewById(R.id.veventTime);
        eventLocation = (TextView) findViewById(R.id.veventLocation);
        eventAddress = (TextView) findViewById(R.id.veventAddress);
        eventDescription = (WebView) findViewById(R.id.veventDescription);
        btnrsvp = (Button) findViewById(R.id.rsvpButton);
        rsvpText = (TextView) findViewById(R.id.rsvpText);

        showProgressDialog("please wait");
        if (isNetworkAvailable()) {
            getEventInfo(eventId);
        } else {
            dismissProgressDialog();
            showToast("Oops....!No Internet");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.share_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (item.getItemId() == R.id.share) {
            ShareContent();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getEventInfo(String eventId) {

        EventService client = RestService.createService(EventService.class);
        Call<RestResponse> eventList = client.getEvent(userToken, eventId);
        eventList.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                if (response.isSuccessful()) {
                    dismissProgressDialog();
                    RestResponse jsonResponse = response.body();
                    try {
                        if (!TextUtils.isEmpty(jsonResponse.getData())) {

                            JSONObject eventObject = new JSONObject(jsonResponse.getData());
                            eventDto = null;
                            eventDto = EventDto.parse(eventObject);
                            initializeValues();
                        }

                    } catch (JSONException e) {

                    }
                } else {
                    dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
            }
        });

    }


    private void initializeValues() {

        if (eventDto == null) {
            return;
        }

        String title = eventDto.getTitle();
        String eventId = eventDto.getEventId();


        eventTitle.setText(title);
        if (eventDto.getOrganizerList().size() > 0) {
            if (!TextUtils.isEmpty(eventDto.getOrganizerList().get(0).getName())) {
                eventOrganizer.setText(eventDto.getOrganizerList().get(0).getName());
            } else {
                eventOrganizer.setVisibility(View.GONE);
            }
        } else {
            eventOrganizer.setVisibility(View.GONE);
        }


        setEventDateandTime(eventDto.getEventStartDate(), eventDto.getEventEndDate());
        eventDate.setText(eventdisplayDate);
        eventTime.setText(eventdisplayTime);

        if (TextUtils.isEmpty(eventDto.getEventLocation().getVenuename())) {
            eventLocation.setVisibility(View.GONE);
        } else {
            eventLocation.setText(eventDto.getEventLocation().getVenuename());
        }

        String addressText = "";

        if (!TextUtils.isEmpty(eventDto.getEventLocation().getAddressline1())) {
            addressText += eventDto.getEventLocation().getAddressline1();
        }

        if (!TextUtils.isEmpty(eventDto.getEventLocation().getAddressline2())) {
            addressText += "," + eventDto.getEventLocation().getAddressline2();
        }

        if (!TextUtils.isEmpty(eventDto.getEventLocation().getLocName())) {
            addressText += "," + eventDto.getEventLocation().getLocName();
        }

        if (!TextUtils.isEmpty(eventDto.getEventLocation().getCity())) {
            addressText += "," + eventDto.getEventLocation().getCity();
        }

        if (!TextUtils.isEmpty(eventDto.getEventLocation().getZipCode())) {
            addressText += "," + eventDto.getEventLocation().getZipCode();
        }

        if (!TextUtils.isEmpty(eventDto.getEventLocation().getState())) {
            addressText += "," + eventDto.getEventLocation().getState();
        }

        eventAddress.setText(addressText);

        String contentDescription = "On " + eventdisplayDate + " " + eventdisplayTime + " at " + addressText;

       /* eventBranchObject = new BranchUniversalObject()
                .setCanonicalIdentifier("event/"+eventId)
                .setTitle(title)
                .setContentDescription(contentDescription)
                .setContentImageUrl(branchImage)
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .addContentMetadata("property1", "blue")
                .addContentMetadata("property2", "red");*/
        setWebViewContent();

        btnrsvp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                RSVPDialogFragment rsvpDialog = RSVPDialogFragment.newInstance("Some Title");
                Bundle rsvpBundle = new Bundle();
                if (eventDto.getResponse().getResponse() == 1) {
                    rsvpBundle.putBoolean("rsvpresponse", true);
                } else {
                    rsvpBundle.putBoolean("rsvpresponse", false);
                }
                if (!TextUtils.isEmpty(eventDto.getResponse().getGuestcount())) {
                    rsvpBundle.putString("guests", eventDto.getResponse().getGuestcount());
                }
                rsvpDialog.setArguments(rsvpBundle);
                rsvpDialog.show(fm, "dialog_fragment_rsvp");
            }
        });

    }


    private void setWebViewContent() {

        final String mimeType = "text/html";
        final String encoding = "UTF-8";

        eventDescription.getSettings().setUseWideViewPort(true);
        eventDescription.getSettings().setLoadWithOverviewMode(true);
        eventDescription.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        eventDescription.clearCache(true);
        eventDescription.getSettings().setJavaScriptEnabled(true);

        eventDescription.getSettings().setBlockNetworkImage(false);
        eventDescription.getSettings().setDomStorageEnabled(true);

        eventDescription.getSettings().setBuiltInZoomControls(true);
        eventDescription.getSettings().setSupportZoom(true);
        eventDescription.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        eventDescription.getSettings().setDisplayZoomControls(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            eventDescription.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        } else {
            eventDescription.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        }

        String content = "<html><head><meta name=\"viewport\" content=\"width=device-width,initial-scale=1\"/>";


        content += "<style type=\"text/css\">" +
                "* { background: #FFFFFF ! important; color: #4D4D4D !important;}" +
                ":link, :link * { color: #2196F3 !important }" +
                ":visited, :visited * { color: #551A8B !important } img { max-width:100% !important;}</style> ";

        String Description = "";
        String bodyText = "";
        if (!TextUtils.isEmpty(eventDto.getDescription())) {
            Description = eventDto.getDescription().toString();
            //  bodyText = Html.fromHtml(Description).toString();

        }

        //  bodyText = Html.fromHtml(Description).toString();


        //  Pattern bodyPattern = Pattern.compile("(.*<\s*body[^>]*>)|(<\s*/\s*body\s*\>.+)");
        content += "</head><body>" + Description + "</body></html>";

        content = content.replace("<img", "<img height=\"auto\"");
        eventDescription.loadDataWithBaseURL("http://", content, mimeType, encoding, "");

    }

    private void setEventDateandTime(String startDate, String endDate) {

        Long startDateTimeStamp = Long.parseLong(startDate) * 1000;
        Long endDateTimeStamp = Long.parseLong(endDate) * 1000;
        eventdisplayDate = "";
        eventdisplayTime = "";

        SimpleDateFormat sf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        Date eventstartDate = new Date(startDateTimeStamp);
        Date eventendDate = new Date(endDateTimeStamp);


        DateFormat outputDate = new SimpleDateFormat("EEE MMM dd");
        DateFormat outputDate1 = new SimpleDateFormat("dd");
        DateFormat hrmin = new SimpleDateFormat("HH:mm");
        DateFormat outputDateYr = new SimpleDateFormat("EEEE MMM dd, yyyy");

        DateFormat daymonthday = new SimpleDateFormat("EEEE, MMMM dd");
        DateFormat dayandtime = new SimpleDateFormat("EEEE, MMMM dd, HH:mm");


        Calendar startCal = Calendar.getInstance();
        startCal.setTime(eventstartDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(eventendDate);

        Calendar current = Calendar.getInstance();
        current.setTime(new Date());

        //  String eventDate = DateUtils.formatDateRange(this,startDateTimeStamp,endDateTimeStamp,DateUtils.FORMAT_ABBREV_RELATIVE);

        if (eventendDate.before(new Date())) {
            btnrsvp.setVisibility(View.INVISIBLE);
            if (eventDto.getResponse().getResponse() == 1) {
                rsvpText.setText("You RSVPd Yes");
            } else if (eventDto.getResponse().getResponse() == 0) {
                rsvpText.setText("You RSVPd No");
            } else {
                rsvpText.setText("You have not RSVPd");
            }
        }

        if (eventendDate.after(new Date())) {
            if (eventDto.getResponse().getResponse() == 1) {
                rsvpText.setText("You are going");
                btnrsvp.setText("Update");
            } else if (eventDto.getResponse().getResponse() == 0) {
                rsvpText.setText("You are not going");
                btnrsvp.setText("Update");
            }
        }

        if (startCal.get(Calendar.DAY_OF_MONTH) == endCal.get(Calendar.DAY_OF_MONTH)) {
            eventdisplayDate = daymonthday.format(eventstartDate);
            eventdisplayTime = hrmin.format(eventstartDate) + " - " + hrmin.format(eventendDate);
        } else {
            eventdisplayDate = dayandtime.format(eventstartDate);
            eventdisplayTime = dayandtime.format(eventendDate);
        }
        /*
        if (startCal.get(Calendar.MONTH)!= endCal.get(Calendar.MONTH)){
            eventdisplayDate = outputDate.format(eventstartDate)+" - "+outputDate.format(eventendDate);
            eventdisplayTime = hrmin.format(eventstartDate)+ " onwards";

        }else if ((startCal.get(Calendar.MONTH) == endCal.get(Calendar.MONTH)) && (startCal.get(Calendar.DAY_OF_MONTH)==endCal.get(Calendar.DAY_OF_MONTH))) {
            eventdisplayDate = outputDateYr.format(eventstartDate);
            eventdisplayTime = hrmin.format(eventstartDate)+ " - "+hrmin.format(eventendDate);
        } else {
            eventdisplayDate = outputDate.format(eventstartDate)+" - "+outputDate.format(eventendDate);
            eventdisplayTime = hrmin.format(eventstartDate)+ " onwards";
        } */
    }

    @Override
    public void onRsvp(String guestcount, boolean going) {
        final String goingString;
        EventDto.rsvpresponse rsvp = new EventDto.rsvpresponse();

        if (going) {
            goingString = "yes";
            rsvpText.setText("You are going");
            btnrsvp.setText("Update");
            rsvp.setResponse(1);


        } else {
            goingString = "no";
            rsvpText.setText("You are not going");
            rsvp.setResponse(0);
        }
        rsvp.setGuestcount(guestcount);
        eventDto.setResponse(rsvp);
        RSVP rsvpPojo = new RSVP();
        rsvpPojo.setContactemail("");
        rsvpPojo.setContactname("");
        rsvpPojo.setContactphone("");
        rsvpPojo.setGuestcount(guestcount);
        //JSONObject rsvpObject = EventDto.createRSVPObject(guestcount);
        submitRSVP(eventId, guestcount, goingString, rsvpPojo);
    }


    private void submitRSVP(String eventId, String guestcount, String goingText, RSVP rsvp) {


        EventService eventClient = RestService.createService(EventService.class);
        Call<RestResponse> eventRsvp = eventClient.submitrsvp(userToken, eventId, goingText, rsvp);
        eventRsvp.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                if (response.isSuccessful()) {
                    RestResponse jsonResponse = response.body();
                    try {
                        if (!TextUtils.isEmpty(jsonResponse.getData())) {

                            JSONObject eventObject = new JSONObject(jsonResponse.getData());

                        }

                    } catch (JSONException e) {

                    }
                } else {
                    Log.d("here", "here");
                }

            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                Log.d("here", "here");
            }
        });
    }


    private void initializeToolBar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.veventToolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }

//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            //getSupportActionBar().setTitle(groupName);
//            this.getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_clear_black_24dp);
//        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void ShareContent() {
      /*  Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "AndroidSolved");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Now Learn Android with AndroidSolved clicke here to visit https://androidsolved.wordpress.com/ ");
        startActivity(Intent.createChooser(sharingIntent, "Share via"));*/

        plaintext = new StringBuilder(eventDto.getTitle());
        // plaintext.append("\n");
        // plaintext.append(eventDto.getShareText());
        if (!TextUtils.isEmpty(eventDto.getShareUrl())) {
            plaintext.append("\n");
            plaintext.append(eventDto.getShareUrl());
        }

        String subject = eventDto.getTitle();

        Resources resources = getResources();

        Intent emailIntent = new Intent();
        emailIntent.setAction(Intent.ACTION_SEND);
        // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
        emailIntent.putExtra(Intent.EXTRA_TEXT, plaintext.toString());
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.setType("message/rfc822");

        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");

        Intent openInChooser = Intent.createChooser(emailIntent, "Share Via");
        PackageManager pm = getPackageManager();
        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
        for (int i = 0; i < resInfo.size(); i++) {
            // Extract the label, append it, and repackage it in a LabeledIntent
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            if (packageName.contains("android.email")) {
                emailIntent.setPackage(packageName);
            } else if (packageName.contains("mms") || packageName.contains("android.gm") || packageName.contains("whatsapp")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                if (packageName.contains("mms") || packageName.contains("whatsapp")) {
                    intent.putExtra(Intent.EXTRA_TEXT, plaintext.toString());
                } else if (packageName.contains("android.gm")) { // If Gmail shows up twice, try removing this else-if clause and the reference to "android.gm" above
                    intent.putExtra(Intent.EXTRA_TEXT, plaintext.toString());
                    intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                    intent.setType("message/rfc822");
                }

                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
            }
        }

        // convert intentList to array
        LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);

        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
        startActivity(openInChooser);
    }


    private void ShareContentOld() {

        //final String body =profileDto.getName()+"\n"+profileDto.getEmail()+"\n"+profileDto.getPhoneNo();
        String content = "<html><head><meta name=\"viewport\" content=\"width=device-width\"/>";

        content += "<style type=\"text/css\">" +
                "* { background: #FFFFFF ! important; color: #4D4D4D !important; font-size:150%;}" +
                ":link, :link * { color: #2196F3 !important }" +
                ":visited, :visited * { color: #551A8B !important }</style> ";

        String Description = "";
        content = "";
        String bodyText = "";
        if (!TextUtils.isEmpty(eventDto.getDescription())) {
            Description = eventDto.getDescription().toString();
        }

        //   bodyText = Html.fromHtml(Description).toString();


        //  Pattern bodyPattern = Pattern.compile("(.*<\s*body[^>]*>)|(<\s*/\s*body\s*\>.+)");
        content += "</head><body>" + Description + "</body></html>";

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:?subject=" + eventDto.getTitle() + "&body=" + content);
        intent.setData(data);
        startActivity(intent);

    }

    private void ShareContentViaBranch() {
       /* ShareSheetStyle shareSheetStyle = new ShareSheetStyle(ViewEventActivity.this, "Check this out!", "This stuff is awesome: ")
                .setCopyUrlStyle(getResources().getDrawable(android.R.drawable.ic_menu_send), "Copy", "Added to clipboard")
                .setMoreOptionStyle(getResources().getDrawable(android.R.drawable.ic_menu_search), "Show more")
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.WHATS_APP)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.TWITTER);


        LinkProperties linkProperties = new LinkProperties()
                .setChannel("facebook")
                .setFeature("sharing");

            .addControlParameter("$android_url","http://www.yahoo.com");
                .addControlParameter("$desktop_url", "http://example.com/home")
                .addControlParameter("$ios_url", "http://example.com/ios");

        eventBranchObject.showShareSheet(this,
                linkProperties,
                shareSheetStyle,
                new Branch.BranchLinkShareListener() {
                    @Override
                    public void onShareLinkDialogLaunched() {
                    }

                    @Override
                    public void onShareLinkDialogDismissed() {
                    }

                    @Override
                    public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {
                    }

                    @Override
                    public void onChannelSelected(String channelName) {
                    }
                });*/
    }


}
