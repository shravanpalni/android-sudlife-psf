package com.outwork.sudlife.psf.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.adapters.PSFNotificationsAdapter;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.base.SplashActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.MyListAdapter;
import com.outwork.sudlife.psf.utilities.MyListData;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shravanch on 10-12-2019.
 */

public class NotificationsListActivity extends BaseActivity {

    private CollapsingToolbarLayout collapsingToolbar;
    private List<PSFNotificationsModel> psfNotificationsModelList = new ArrayList();
    private PSFNotificationsAdapter psfNotificationsAdapter;
    private RecyclerView notificationsrecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications_list);


      /*  if (SharedPreferenceManager.getInstance().getString(Constants.PSF_NOTIFICATIONS_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                LeadIntentService.insertPSFNotifications(NotificationsListActivity.this);
            }
        }*/

        initToolbar();
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Notifications");


        notificationsrecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        if (isNetworkAvailable()) {
            //LeadIntentService.insertPSFNotifications(NotificationsListActivity.this);
            getServerPSFNotifications();
        }else {
            psfNotificationsModelList = LeadMgr.getInstance(NotificationsListActivity.this).getPSFNotificationsList(userid);
            if (psfNotificationsModelList.size() > 0) {
                // nomessageview.setVisibility(View.INVISIBLE);
           /* if (msgListProgressBar.VISIBLE == 0) {
                msgListProgressBar.setVisibility(View.INVISIBLE);
            }*/
                Collections.reverse(psfNotificationsModelList);

                psfNotificationsAdapter = new PSFNotificationsAdapter(NotificationsListActivity.this, psfNotificationsModelList, userid);
                notificationsrecyclerView.setAdapter(psfNotificationsAdapter);
                notificationsrecyclerView.setLayoutManager(new LinearLayoutManager(NotificationsListActivity.this));
            } else {
                //nomessageview.setText("No Plans");
                //nomessageview.setVisibility(View.VISIBLE);
            }

        }






   /*     MyListAdapter adapter = new MyListAdapter(myListData);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);*/


    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }


    private void getServerPSFNotifications() {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getProposalCodes = client.getPsfNotifications(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        getProposalCodes.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFNotificationsModel>>() {
                        }.getType();
                        List<PSFNotificationsModel> psfNotificationsModelList = new Gson().fromJson(response.body().getData(), listType);
                        if (psfNotificationsModelList.size() > 0) {
                            LeadMgr.getInstance(NotificationsListActivity.this).insertPSFNotificationsList(psfNotificationsModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));

                            LeadIntentService.updatePSFNotifications(NotificationsListActivity.this);
                            Collections.reverse(psfNotificationsModelList);

                            psfNotificationsAdapter = new PSFNotificationsAdapter(NotificationsListActivity.this, psfNotificationsModelList, userid);
                            notificationsrecyclerView.setAdapter(psfNotificationsAdapter);
                            notificationsrecyclerView.setLayoutManager(new LinearLayoutManager(NotificationsListActivity.this));


                        }else {


                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                //SharedPreferenceManager.getInstance().putString(Constants.PSF_NOTIFICATIONS_LOADED, "notloaded");
            }
        });
    }


}
