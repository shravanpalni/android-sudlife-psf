package com.outwork.sudlife.psf.targets.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by bvlbh on 3/24/2018.
 */

public class MSBModel implements Serializable {


    @SerializedName("activitiesplanned")
    @Expose
    private String activitiesplanned;

    @SerializedName("visitsplanned")
    @Expose
    private String visitsplanned;

    @SerializedName("expectedconnections")
    @Expose
    private String expectedconncetions;

    @SerializedName("expectedleads")
    @Expose
    private String expectedleads;

    @SerializedName("expectedbussiness")

    @Expose
    private String expectedbusiness;

    @SerializedName("memberid")
    @Expose
    private String memberid;
    @SerializedName("activitiescompleted")
    @Expose
    private String activitiescompleted;


    @SerializedName("visitscompleted")
    @Expose
    private String vistiscompleted;

    @SerializedName("achievedconnections")
    @Expose
    private String achievedconnection;


    @SerializedName("achievedleads")
    @Expose
    private String achievedleads;

    @SerializedName("achievedbussiness")
    @Expose
    private String achievedbusiness;

    public String getGaptotarget() {
        return gaptotarget;
    }

    public void setGaptotarget(String gaptotarget) {
        this.gaptotarget = gaptotarget;
    }

    @SerializedName("gaptotarget")
    @Expose
    private String gaptotarget;


    @SerializedName("statusofsuccessorfail")
    @Expose
    private String statusofsuccessorfail;

    @SerializedName("networkstatus")
    @Expose
    private String networkstatus;

    @SerializedName("offlineoronlinestatus")
    @Expose
    private String offlineoronlinestatus;


    @SerializedName("ftm")
    @Expose
    private String ftm;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("customername")
    @Expose
    private String customername;


    @SerializedName("year")
    @Expose
    private String year;

    public String getObjectid() {
        return objectid;
    }

    public void setObjectid(String objectid) {
        this.objectid = objectid;
    }

    @SerializedName("objectid")
    @Expose
    private String objectid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @SerializedName("userid")
    @Expose
    private String userid;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    @SerializedName("month")
    @Expose
    private String month;

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getTargetid() {
        return targetid;
    }

    public void setTargetid(String targetid) {
        this.targetid = targetid;
    }

    @SerializedName("targetid")
    @Expose
    private String targetid;


    public String getActivitiesplanned() {
        return activitiesplanned;
    }

    public void setActivitiesplanned(String activitiesplanned) {
        this.activitiesplanned = activitiesplanned;
    }

    public String getVisitsplanned() {
        return visitsplanned;
    }

    public void setVisitsplanned(String visitsplanned) {
        this.visitsplanned = visitsplanned;
    }

    public String getExpectedconncetions() {
        return expectedconncetions;
    }

    public void setExpectedconncetions(String expectedconncetions) {
        this.expectedconncetions = expectedconncetions;
    }

    public String getExpectedleads() {
        return expectedleads;
    }

    public void setExpectedleads(String expectedleads) {
        this.expectedleads = expectedleads;
    }

    public String getExpectedbusiness() {
        return expectedbusiness;
    }

    public void setExpectedbusiness(String expectedbusiness) {
        this.expectedbusiness = expectedbusiness;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getActivitiescompleted() {
        return activitiescompleted;
    }

    public void setActivitiescompleted(String activitiescompleted) {
        this.activitiescompleted = activitiescompleted;
    }

    public String getVistiscompleted() {
        return vistiscompleted;
    }

    public void setVistiscompleted(String vistiscompleted) {
        this.vistiscompleted = vistiscompleted;
    }

    public String getAchievedconnection() {
        return achievedconnection;
    }

    public void setAchievedconnection(String achievedconnection) {
        this.achievedconnection = achievedconnection;
    }

    public String getAchievedleads() {
        return achievedleads;
    }

    public void setAchievedleads(String achievedleads) {
        this.achievedleads = achievedleads;
    }

    public String getAchievedbusiness() {
        return achievedbusiness;
    }

    public void setAchievedbusiness(String achievedbusiness) {
        this.achievedbusiness = achievedbusiness;
    }

    public String getStatusofsuccessorfail() {
        return statusofsuccessorfail;
    }

    public void setStatusofsuccessorfail(String statusofsuccessorfail) {
        this.statusofsuccessorfail = statusofsuccessorfail;
    }

    public String getNetworkstatus() {
        return networkstatus;
    }

    public void setNetworkstatus(String networkstatus) {
        this.networkstatus = networkstatus;
    }

    public String getOfflineoronlinestatus() {
        return offlineoronlinestatus;
    }

    public void setOfflineoronlinestatus(String offlineoronlinestatus) {
        this.offlineoronlinestatus = offlineoronlinestatus;
    }

    public String getFtm() {
        return ftm;
    }

    public void setFtm(String ftm) {
        this.ftm = ftm;
    }
}
