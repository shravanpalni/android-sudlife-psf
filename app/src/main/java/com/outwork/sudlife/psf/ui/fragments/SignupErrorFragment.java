package com.outwork.sudlife.psf.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.ui.activities.SignInActvity;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignupErrorFragment extends Fragment {

    private TextView errorText,goback,resend;
    private String errorMsg,errorCode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        errorMsg = getArguments().getString("errorMsg","Oops something went wrong!");
        errorCode = getArguments().getString("errorCode","10000");


    }

    public SignupErrorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_signup_error, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        errorText = (TextView)v.findViewById(R.id.errorText);
        goback = (TextView)v.findViewById(R.id.goback);
        errorText.setText(errorMsg);

        if (errorCode.equalsIgnoreCase("USR-103")){
            goback.setText("Verify");
        }

        if (errorCode.equalsIgnoreCase("USR-105")||errorCode.equalsIgnoreCase("USR-1002")|| errorCode.equalsIgnoreCase("USR-1003")){
            goback.setText("Sign In");
        }


        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*if (errorCode.equalsIgnoreCase("USR-103")){
                    Intent userVerify= new Intent(getActivity(), UserVerifyActivity.class);
                    userVerify.putExtra("firstTimeRegister",false);
                    startActivity(userVerify);
                    getActivity().finish();
                }*/
                if (errorCode.equalsIgnoreCase("USR-105") || errorCode.equalsIgnoreCase("USR-1002")||errorCode.equalsIgnoreCase("USR-1003")){
                    Intent signIn= new Intent(getActivity(), SignInActvity.class);
                    startActivity(signIn);
                    getActivity().finish();
                }

                if (errorCode.equalsIgnoreCase("USR-106") || errorCode.equalsIgnoreCase("USR-107")){
                  getActivity().onBackPressed();
                }
            }
        });



        return v;
    }





}
