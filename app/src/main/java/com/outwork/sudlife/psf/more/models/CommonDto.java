package com.outwork.sudlife.psf.more.models;


import com.outwork.sudlife.psf.IvokoApplication;

public class CommonDto {

	private IvokoApplication.POST_TYPE ivokoType;
	private String postId;
	private String feedDate;
	private String objectId;
	private String objectType;

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public IvokoApplication.POST_TYPE getIvokoType() {
		return ivokoType;
	}

	public void setIvokoType(IvokoApplication.POST_TYPE type) {
		this.ivokoType = type;
	}

	public String getFeedDate() {
		return feedDate;
	}

	public void setFeedDate(String feedDate) {
		this.feedDate = feedDate;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}



}
