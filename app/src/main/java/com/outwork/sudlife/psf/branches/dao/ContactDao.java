package com.outwork.sudlife.psf.branches.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.psf.branches.models.ContactModel;
import com.outwork.sudlife.psf.dao.DBHelper;
import com.outwork.sudlife.psf.dao.IvokoProvider;
import com.outwork.sudlife.psf.dao.IvokoProvider.Contact;
import com.outwork.sudlife.psf.utilities.Utils;

/**
 * Created by Panch on 2/22/2017.
 */

public class ContactDao {
    public static final String TAG = ContactDao.class.getSimpleName();

    private static ContactDao instance;
    private Context applicationContext;

    public static ContactDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new ContactDao(applicationContext);
        }
        return instance;
    }

    private ContactDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void insertContact(ContactModel contactModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        String internaltype = "0";
        ContentValues values = new ContentValues();
        values.put(Contact.groupid, contactModel.getGroupid());
        values.put(Contact.userid, contactModel.getUserid());
        values.put(Contact.contactid, contactModel.getContactid());
        values.put(Contact.customerid, contactModel.getCustomerid());
        values.put(Contact.customername, contactModel.getCustomername());
        values.put(Contact.firstname, contactModel.getCfirstname());
        values.put(Contact.lastname, contactModel.getClname());
        values.put(Contact.displayname, contactModel.getCdisplayname());
        values.put(Contact.designation, contactModel.getDesignation());
        values.put(Contact.phoneno, contactModel.getPhoneno());
        values.put(Contact.cemail, contactModel.getEmail());
        values.put(Contact.modifieddate, contactModel.getModifieddate());
        if (!TextUtils.isEmpty(contactModel.getCustomerid())) {
            internaltype = BranchesDao.getInstance(this.applicationContext).getInternalType(db, contactModel.getCustomerid());
        }
//        if (contactModel.getAddress() != null)
//            values.put(Contact.address, new Gson().toJson(contactModel.getAddress()));
        values.put(Contact.internaltype, internaltype);
        boolean createSuccessful = db.insert(Contact.TABLE, null, values) > 0;
        if (createSuccessful) {
            Log.e(TAG, " created.");
        }
    }

    public void insertContact(ContactModel contactModel, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        String internaltype = "0";
        values.put(Contact.groupid, contactModel.getGroupid());
        values.put(Contact.userid, contactModel.getUserid());
        if (Utils.isNotNullAndNotEmpty(contactModel.getContactid()))
            values.put(Contact.contactid, contactModel.getContactid());
        if (Utils.isNotNullAndNotEmpty(contactModel.getCustomerid()))
            values.put(Contact.customerid, contactModel.getCustomerid());
        if (Utils.isNotNullAndNotEmpty(contactModel.getCustomername()))
            values.put(Contact.customername, contactModel.getCustomername().trim());
        if (Utils.isNotNullAndNotEmpty(contactModel.getCfirstname()))
            values.put(Contact.firstname, contactModel.getCfirstname());
        if (Utils.isNotNullAndNotEmpty(contactModel.getClname()))
            values.put(Contact.lastname, contactModel.getClname());
        if (Utils.isNotNullAndNotEmpty(contactModel.getCdisplayname()))
            values.put(Contact.displayname, contactModel.getCdisplayname().trim());
        if (Utils.isNotNullAndNotEmpty(contactModel.getDesignation()))
            values.put(Contact.designation, contactModel.getDesignation().trim());
        if (Utils.isNotNullAndNotEmpty(contactModel.getClassification()))
            values.put(Contact.classification, contactModel.getClassification().trim());
        if (Utils.isNotNullAndNotEmpty(contactModel.getSpeciality()))
            values.put(Contact.speciality, contactModel.getSpeciality().trim());
        if (Utils.isNotNullAndNotEmpty(contactModel.getPhoneno()))
            values.put(Contact.phoneno, contactModel.getPhoneno());
        if (Utils.isNotNullAndNotEmpty(contactModel.getEmail()))
            values.put(Contact.cemail, contactModel.getEmail());
        if (Utils.isNotNullAndNotEmpty(contactModel.getModifieddate()))
            values.put(Contact.modifieddate, contactModel.getModifieddate());
        values.put(Contact.status, "");
        if (Utils.isNotNullAndNotEmpty(contactModel.getPracticing()))
            values.put(Contact.practicing, contactModel.getPracticing());
        if (Utils.isNotNullAndNotEmpty(contactModel.getSalutation()))
            values.put(Contact.salutation, contactModel.getSalutation());
        if (Utils.isNotNullAndNotEmpty(contactModel.getGender()))
            values.put(Contact.gender, contactModel.getGender());
//        if (contactModel.getAddress() != null)
//            values.put(Contact.address, new Gson().toJson(contactModel.getAddress()));
        if (Utils.isNotNullAndNotEmpty(contactModel.getCustomerid())) {
            internaltype = BranchesDao.getInstance(this.applicationContext).getInternalType(db, contactModel.getCustomerid());
        }
        values.put(Contact.internaltype, internaltype);
        boolean createSuccessful = db.insert(Contact.TABLE, null, values) > 0;
        if (createSuccessful) {
            Log.e(TAG, " created.");
        }
    }

    public void insertLocalContact(ContactModel contactModel, String network_status) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        String internaltype = "0";
        values.put(Contact.groupid, contactModel.getGroupid());
        values.put(Contact.userid, contactModel.getUserid());
        if (Utils.isNotNullAndNotEmpty(contactModel.getCustomerid()))
            values.put(Contact.customerid, contactModel.getCustomerid());
        if (Utils.isNotNullAndNotEmpty(contactModel.getCustomername()))
            values.put(Contact.customername, contactModel.getCustomername().trim());
        if (Utils.isNotNullAndNotEmpty(contactModel.getCfirstname()))
            values.put(Contact.firstname, contactModel.getCfirstname());
        if (Utils.isNotNullAndNotEmpty(contactModel.getCdisplayname()))
            values.put(Contact.displayname, contactModel.getCdisplayname().trim());
        if (Utils.isNotNullAndNotEmpty(contactModel.getDesignation()))
            values.put(Contact.designation, contactModel.getDesignation().trim());
        if (Utils.isNotNullAndNotEmpty(contactModel.getClassification()))
            values.put(Contact.classification, contactModel.getClassification().trim());
        if (Utils.isNotNullAndNotEmpty(contactModel.getSpeciality()))
            values.put(Contact.speciality, contactModel.getSpeciality().trim());
        if (Utils.isNotNullAndNotEmpty(contactModel.getPhoneno()))
            values.put(Contact.phoneno, contactModel.getPhoneno());
        if (Utils.isNotNullAndNotEmpty(contactModel.getEmail()))
            values.put(Contact.cemail, contactModel.getEmail());
        values.put(Contact.status, network_status);
        if (Utils.isNotNullAndNotEmpty(contactModel.getPracticing()))
            values.put(Contact.practicing, contactModel.getPracticing());
        if (Utils.isNotNullAndNotEmpty(contactModel.getSalutation()))
            values.put(Contact.salutation, contactModel.getSalutation());
        if (Utils.isNotNullAndNotEmpty(contactModel.getGender()))
            values.put(Contact.gender, contactModel.getGender());
//        if (contactModel.getAddress() != null)
//            values.put(Contact.address, new Gson().toJson(contactModel.getAddress()));
        if (Utils.isNotNullAndNotEmpty(contactModel.getCustomerid())) {
            internaltype = BranchesDao.getInstance(this.applicationContext).getInternalType(db, contactModel.getCustomerid());
        }
        values.put(Contact.internaltype, internaltype);
        boolean createSuccessful = db.insert(Contact.TABLE, null, values) > 0;
        if (createSuccessful) {
            Log.e(TAG, " created.");
        }
    }

    public void insertContactList(List<ContactModel> contactModelList) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        try {
            for (int j = 0; j < contactModelList.size(); j++) {
                ContactModel contactModel = contactModelList.get(j);
                if (Utils.isNotNullAndNotEmpty(contactModel.getContactid()))
                    if (isRecordExist(db, contactModel.getContactid())) {
                        deleteContactRecordID(contactModel.getContactid());
                        insertContact(contactModel, db);
                    } else {
                        insertContact(contactModel, db);
                    }
            }
        } finally {
        }
    }

    public void updateContact(ContactModel contactModel, String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Contact.designation, contactModel.getDesignation());
        values.put(Contact.phoneno, contactModel.getPhoneno());
        values.put(Contact.cemail, contactModel.getEmail());
        values.put(Contact.speciality, contactModel.getSpeciality());
        values.put(Contact.classification, contactModel.getClassification());
        values.put(Contact.practicing, contactModel.getPracticing());
        if (!TextUtils.isEmpty(contactModel.getContactid())) {
            db.update(Contact.TABLE, values, Contact.contactid + " = " + contactModel.getContactid() + " AND " +
                    Contact.userid + " = ?", new String[]{userId});
        }
    }

    public void updateContactOnline(ContactModel contactModel, String userId, String Contactid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Contact.contactid, Contactid);
        values.put(Contact.status, "");
        try {
            db.update(Contact.TABLE, values, Contact._ID + " = " + contactModel.getId() + " AND " + Contact.userid + " = ?", new String[]{userId});
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<ContactModel> getContactByCustomerID(String groupId, String userId, String customerID) {
        List<ContactModel> contactList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;

            // security
            /*String sql = "";
            sql += "SELECT * FROM " + Contact.TABLE;
            sql += " WHERE " + Contact.userid + " = '" + userId + "'";
            sql += " AND " + Contact.customerid + " = '" + customerID + "'";
            sql += " ORDER BY " + Contact.displayname + " COLLATE NOCASE ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + Contact.TABLE + " WHERE " + Contact.userid + " = ?" + " AND " +Contact.customerid + " = ?" + " ORDER BY " +Contact.displayname + " = ?" , new String[]{userId,customerID," COLLATE NOCASE "});

                if (cursor.moveToFirst()) {
                    do {
                        ContactModel dto = new ContactModel();
                        dto.setContactid(cursor.getString(cursor.getColumnIndex(Contact.contactid)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Contact.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Contact.customername)));
                        dto.setCfirstname(cursor.getString(cursor.getColumnIndex(Contact.firstname)));
                        dto.setClname(cursor.getString(cursor.getColumnIndex(Contact.lastname)));
                        dto.setCdisplayname(cursor.getString(cursor.getColumnIndex(Contact.displayname)));
                        dto.setDesignation(cursor.getString(cursor.getColumnIndex(Contact.designation)));
                        dto.setSalutation(cursor.getString(cursor.getColumnIndex(Contact.salutation)));
                        dto.setSpeciality(cursor.getString(cursor.getColumnIndex(Contact.speciality)));
                        dto.setClassification(cursor.getString(cursor.getColumnIndex(Contact.classification)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Contact.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Contact.cemail)));
                        dto.setInternaltype(cursor.getString(cursor.getColumnIndex(Contact.internaltype)));
                        dto.setUserid(cursor.getString(cursor.getColumnIndex(Contact.userid)));
                        dto.setGroupid(cursor.getString(cursor.getColumnIndex(Contact.groupid)));
//                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Contact.address)), GeoAddress.class));
                        contactList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return contactList;
    }

    public List<ContactModel> getContactByStatus(String userId, String network_status) {
        List<ContactModel> contactList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;

            // security
            /*String sql = "";
            sql += "SELECT * FROM " + Contact.TABLE;
            sql += " WHERE " + Contact.userid + " = '" + userId + "'";
            sql += " AND " + Contact.status + " = '" + network_status + "'";
            sql += " ORDER BY " + Contact.displayname + " COLLATE NOCASE ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + Contact.TABLE + " WHERE " + Contact.userid + " = ?" + " AND " +Contact.status + " = ?" + " ORDER BY " +Contact.displayname + " = ?" , new String[]{userId,network_status," COLLATE NOCASE "});

                //::FIXED CODE::
                /* cursor=db.rawQuery("select * from "+Contact.TABLE+" where "+ "Contact.userid=? and Contact.status=? order by Contact.displayname=?",
                        new String [] {userId, network_status," COLLATE NOCASE "});*/


                if (cursor.moveToFirst()) {
                    do {
                        ContactModel dto = new ContactModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Contact._ID)));
                        dto.setUserid(cursor.getString(cursor.getColumnIndex(Contact.userid)));
                        dto.setGroupid(cursor.getString(cursor.getColumnIndex(Contact.groupid)));
                        dto.setContactid(cursor.getString(cursor.getColumnIndex(Contact.contactid)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Contact.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Contact.customername)));
                        dto.setCfirstname(cursor.getString(cursor.getColumnIndex(Contact.firstname)));
                        dto.setClname(cursor.getString(cursor.getColumnIndex(Contact.lastname)));
                        dto.setCdisplayname(cursor.getString(cursor.getColumnIndex(Contact.displayname)));
                        dto.setDesignation(cursor.getString(cursor.getColumnIndex(Contact.designation)));
                        dto.setSpeciality(cursor.getString(cursor.getColumnIndex(Contact.speciality)));
                        dto.setPracticing(cursor.getString(cursor.getColumnIndex(Contact.practicing)));
                        dto.setSalutation(cursor.getString(cursor.getColumnIndex(Contact.salutation)));
                        dto.setGender(cursor.getString(cursor.getColumnIndex(Contact.gender)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Contact.status)));
                        dto.setClassification(cursor.getString(cursor.getColumnIndex(Contact.classification)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Contact.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Contact.cemail)));
                        dto.setInternaltype(cursor.getString(cursor.getColumnIndex(Contact.internaltype)));
//                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Contact.address)), GeoAddress.class));
                        contactList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return contactList;
    }

    public ContactModel getContact(String customerId) {
        ContactModel dto = new ContactModel();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;

            // security
            /*String sql = "";
            sql += "SELECT * FROM " + Contact.TABLE;
            sql += " WHERE " + Contact.customerid + " = '" + customerId + "'";
            sql += " LIMIT 1 ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + Contact.TABLE + " WHERE " + Contact.customerid + " = ?" + " LIMIT 1 "  , new String[]{customerId});
                if (cursor.moveToFirst()) {
                    do {
                        dto.setContactid(cursor.getString(cursor.getColumnIndex(Contact.contactid)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Contact.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Contact.customername)));
                        dto.setCfirstname(cursor.getString(cursor.getColumnIndex(Contact.firstname)));
                        dto.setClname(cursor.getString(cursor.getColumnIndex(Contact.lastname)));
                        dto.setSalutation(cursor.getString(cursor.getColumnIndex(Contact.salutation)));
                        dto.setCdisplayname(cursor.getString(cursor.getColumnIndex(Contact.displayname)));
                        dto.setDesignation(cursor.getString(cursor.getColumnIndex(Contact.designation)));
                        dto.setSpeciality(cursor.getString(cursor.getColumnIndex(Contact.speciality)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Contact.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Contact.cemail)));
                        dto.setInternaltype(cursor.getString(cursor.getColumnIndex(Contact.internaltype)));
//                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Contact.address)), GeoAddress.class));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return dto;
    }

    public ContactModel getContactbylocalId(int contactId) {
        ContactModel dto = new ContactModel();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;

            // security
            /*String sql = "";
            sql += "SELECT * FROM " + Contact.TABLE;
            sql += " WHERE " + Contact._ID + " = '" + contactId + "'";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + Contact.TABLE + " WHERE " + Contact._ID + " = ?"   , new String[]{String.valueOf(contactId)});
                if (cursor.moveToFirst()) {
                    do {
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Contact._ID)));
                        dto.setContactid(cursor.getString(cursor.getColumnIndex(Contact.contactid)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Contact.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Contact.customername)));
                        dto.setCfirstname(cursor.getString(cursor.getColumnIndex(Contact.firstname)));
                        dto.setClname(cursor.getString(cursor.getColumnIndex(Contact.lastname)));
                        dto.setCdisplayname(cursor.getString(cursor.getColumnIndex(Contact.displayname)));
                        dto.setDesignation(cursor.getString(cursor.getColumnIndex(Contact.designation)));
                        dto.setSpeciality(cursor.getString(cursor.getColumnIndex(Contact.speciality)));
                        dto.setSalutation(cursor.getString(cursor.getColumnIndex(Contact.salutation)));
                        dto.setClassification(cursor.getString(cursor.getColumnIndex(Contact.classification)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Contact.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Contact.cemail)));
                        dto.setInternaltype(cursor.getString(cursor.getColumnIndex(Contact.internaltype)));
//                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Contact.address)), GeoAddress.class));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return dto;
    }

    public ContactModel getContactbyId(String contactId) {
        ContactModel dto = new ContactModel();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;

            // security
          /*  String sql = "";
            sql += "SELECT * FROM " + Contact.TABLE;
            sql += " WHERE " + Contact.contactid + " = '" + contactId + "'";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + Contact.TABLE + " WHERE " + Contact.contactid + " = ?"   , new String[]{contactId});
                if (cursor.moveToFirst()) {
                    do {
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Contact._ID)));
                        dto.setContactid(cursor.getString(cursor.getColumnIndex(Contact.contactid)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Contact.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Contact.customername)));
                        dto.setCfirstname(cursor.getString(cursor.getColumnIndex(Contact.firstname)));
                        dto.setClname(cursor.getString(cursor.getColumnIndex(Contact.lastname)));
                        dto.setCdisplayname(cursor.getString(cursor.getColumnIndex(Contact.displayname)));
                        dto.setDesignation(cursor.getString(cursor.getColumnIndex(Contact.designation)));
                        dto.setSpeciality(cursor.getString(cursor.getColumnIndex(Contact.speciality)));
                        dto.setSalutation(cursor.getString(cursor.getColumnIndex(Contact.salutation)));
                        dto.setClassification(cursor.getString(cursor.getColumnIndex(Contact.classification)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Contact.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Contact.cemail)));
                        dto.setInternaltype(cursor.getString(cursor.getColumnIndex(Contact.internaltype)));
//                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Contact.address)), GeoAddress.class));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return dto;
    }

    public List<String> getContactNamesList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<String> contatNamesList = new ArrayList<>();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;
            // security

           /* String sql = "";
            sql += "SELECT DISTINCT " + Contact.displayname;
            sql += " FROM " + Contact.TABLE;
            sql += " WHERE " + Contact.userid + " = '" + userId + "'";*/
            try {
                // cursor = db.rawQuery(sql, null);
                cursor = db.rawQuery(" SELECT DISTINCT " +Contact.displayname + " FROM " + Contact.TABLE + " WHERE " + Contact.userid + " = ?"   , new String[]{userId});

                if (cursor.moveToFirst()) {
                    do {
                        contatNamesList.add(cursor.getString(cursor.getColumnIndex(Contact.displayname)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return contatNamesList;
    }

    public List<ContactModel> getContactList(String groupId, String userId) {
        List<ContactModel> contactList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;
            //security

           /* String sql = "";
            sql += "SELECT * FROM " + Contact.TABLE;
            sql += " WHERE " + Contact.userid + " = '" + userId + "'";
            sql += " ORDER BY " + Contact.displayname + " COLLATE NOCASE ";*/
            try {
                //cursor = db.rawQuery(sql, null);
                cursor = db.rawQuery("SELECT * FROM " + Contact.TABLE + " WHERE " + Contact.userid + " = ?" + " ORDER BY " + Contact.displayname + " = ?" , new String[]{userId," COLLATE NOCASE " });

                if (cursor.moveToFirst()) {
                    do {
                        ContactModel dto = new ContactModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Contact._ID)));
                        dto.setContactid(cursor.getString(cursor.getColumnIndex(Contact.contactid)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Contact.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Contact.customername)));
                        dto.setCfirstname(cursor.getString(cursor.getColumnIndex(Contact.firstname)));
                        dto.setClname(cursor.getString(cursor.getColumnIndex(Contact.lastname)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Contact.status)));
                        dto.setPracticing(cursor.getString(cursor.getColumnIndex(Contact.practicing)));
                        dto.setCdisplayname(cursor.getString(cursor.getColumnIndex(Contact.displayname)));
                        dto.setDesignation(cursor.getString(cursor.getColumnIndex(Contact.designation)));
                        dto.setSpeciality(cursor.getString(cursor.getColumnIndex(Contact.speciality)));
                        dto.setClassification(cursor.getString(cursor.getColumnIndex(Contact.classification)));
                        dto.setSalutation(cursor.getString(cursor.getColumnIndex(Contact.salutation)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Contact.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Contact.cemail)));
                        dto.setInternaltype(cursor.getString(cursor.getColumnIndex(Contact.internaltype)));
                        dto.setUserid(cursor.getString(cursor.getColumnIndex(Contact.userid)));
                        dto.setGroupid(cursor.getString(cursor.getColumnIndex(Contact.groupid)));
//                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Contact.address)), GeoAddress.class));
                        contactList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return contactList;
    }

    public List<ContactModel> getContactList(String groupId, String userId, String searchString) {
        List<ContactModel> contactList = new ArrayList<ContactModel>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;

            // security

            /*String sql = "";
            sql += "SELECT * FROM " + Contact.TABLE;
            sql += " WHERE " + Contact.displayname + " LIKE '" + searchString + "%'";
            sql += " AND " + Contact.userid + " = '" + userId + "'";
            sql += " ORDER BY " + Contact.displayname + " COLLATE NOCASE ";*/
            try {
                //cursor = db.rawQuery(sql, null);
                cursor=db.rawQuery("select * from "+IvokoProvider.Contact.TABLE+" where "+ "displayname like ? and userid=? order by displayname=?",
                        new String [] {searchString + '%',userId," COLLATE NOCASE "});


                if (cursor.moveToFirst()) {
                    do {
                        ContactModel dto = new ContactModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Contact._ID)));
                        dto.setContactid(cursor.getString(cursor.getColumnIndex(Contact.contactid)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Contact.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Contact.customername)));
                        dto.setCfirstname(cursor.getString(cursor.getColumnIndex(Contact.firstname)));
                        dto.setClname(cursor.getString(cursor.getColumnIndex(Contact.lastname)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Contact.status)));
                        dto.setCdisplayname(cursor.getString(cursor.getColumnIndex(Contact.displayname)));
                        dto.setDesignation(cursor.getString(cursor.getColumnIndex(Contact.designation)));
                        dto.setSpeciality(cursor.getString(cursor.getColumnIndex(Contact.speciality)));
                        dto.setClassification(cursor.getString(cursor.getColumnIndex(Contact.classification)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Contact.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Contact.cemail)));
                        dto.setSalutation(cursor.getString(cursor.getColumnIndex(Contact.salutation)));
                        dto.setInternaltype(cursor.getString(cursor.getColumnIndex(Contact.internaltype)));
                        dto.setUserid(cursor.getString(cursor.getColumnIndex(Contact.userid)));
                        dto.setGroupid(cursor.getString(cursor.getColumnIndex(Contact.groupid)));
//                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Customer.address)), GeoAddress.class));
                        contactList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return contactList;
    }

    public List<ContactModel> getContactSearchList(String groupId, String userId, String searchString) {
        List<ContactModel> contactList = new ArrayList<ContactModel>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;
            // security

            /*String sql = "";
            sql += "SELECT * FROM " + Contact.TABLE;
            sql += " WHERE " + Contact.userid + " = '" + userId + "'";
            sql += " AND (" + Contact.displayname + " LIKE '" + searchString + "%'";
            sql += " OR " + Contact.customername + " LIKE '" + searchString + "%')";
            sql += " ORDER BY " + Contact.displayname + " COLLATE NOCASE ";*/
            try {
                //cursor = db.rawQuery(sql, null);
                cursor=db.rawQuery("select * from "+IvokoProvider.Contact.TABLE+" where "+ "userid=? and (displayname like ? or customername like ?) order by displayname=? ",
                        new String [] {userId,searchString + '%', " COLLATE NOCASE "});


                if (cursor.moveToFirst()) {
                    do {
                        ContactModel dto = new ContactModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Contact._ID)));
                        dto.setContactid(cursor.getString(cursor.getColumnIndex(Contact.contactid)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Contact.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Contact.customername)));
                        dto.setCfirstname(cursor.getString(cursor.getColumnIndex(Contact.firstname)));
                        dto.setClname(cursor.getString(cursor.getColumnIndex(Contact.lastname)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Contact.status)));
                        dto.setCdisplayname(cursor.getString(cursor.getColumnIndex(Contact.displayname)));
                        dto.setDesignation(cursor.getString(cursor.getColumnIndex(Contact.designation)));
                        dto.setSpeciality(cursor.getString(cursor.getColumnIndex(Contact.speciality)));
                        dto.setClassification(cursor.getString(cursor.getColumnIndex(Contact.classification)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Contact.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Contact.cemail)));
                        dto.setSalutation(cursor.getString(cursor.getColumnIndex(Contact.salutation)));
                        dto.setInternaltype(cursor.getString(cursor.getColumnIndex(Contact.internaltype)));
                        dto.setUserid(cursor.getString(cursor.getColumnIndex(Contact.userid)));
                        dto.setGroupid(cursor.getString(cursor.getColumnIndex(Contact.groupid)));
//                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Customer.address)), GeoAddress.class));
                        contactList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return contactList;
    }

    public List<ContactModel> getContactOnlineList(String groupId, String userId, String searchString, String status) {
        List<ContactModel> contactList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;
            // security

            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Customer.TABLE;
            sql += " WHERE " + IvokoProvider.Customer.customername + " LIKE '%" + searchString + "%'";
            sql += " AND " + IvokoProvider.Customer.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Customer.status + " = '" + status + "'";*/

            try {
                // cursor = db.rawQuery(sql, null);


                cursor=db.rawQuery("select * from "+IvokoProvider.Customer.TABLE+" where "+ "customername like ? and userid=? and status=?",
                        new String [] { '%' + searchString + '%',userId,status});



                if (cursor.moveToFirst()) {
                    do {
                        ContactModel dto = new ContactModel();
                        dto.setContactid(cursor.getString(cursor.getColumnIndex(Contact.contactid)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Contact.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Contact.customername)));
                        dto.setCfirstname(cursor.getString(cursor.getColumnIndex(Contact.firstname)));
                        dto.setClname(cursor.getString(cursor.getColumnIndex(Contact.lastname)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Contact.status)));
                        dto.setCdisplayname(cursor.getString(cursor.getColumnIndex(Contact.displayname)));
                        dto.setDesignation(cursor.getString(cursor.getColumnIndex(Contact.designation)));
                        dto.setSpeciality(cursor.getString(cursor.getColumnIndex(Contact.speciality)));
                        dto.setClassification(cursor.getString(cursor.getColumnIndex(Contact.classification)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Contact.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Contact.cemail)));
                        dto.setSalutation(cursor.getString(cursor.getColumnIndex(Contact.salutation)));
                        dto.setInternaltype(cursor.getString(cursor.getColumnIndex(Contact.internaltype)));
                        dto.setUserid(cursor.getString(cursor.getColumnIndex(Contact.userid)));
                        dto.setGroupid(cursor.getString(cursor.getColumnIndex(Contact.groupid)));
//                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(IvokoProvider.Customer.address)), GeoAddress.class));
                        contactList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return contactList;
    }

/*    public List<ContactModel> getContactList(String groupId, String userId, String searchString, boolean isCustomer) {
        List<ContactModel> contactList = new ArrayList<ContactModel>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT " + Contact.contactid + "," + Contact.displayname + ",";
            sql += Contact.customerid + "," + Contact.customername + ",";
            sql += Contact.designation + "," + Contact.phoneno + ",";
            sql += Contact.cemail + "," + IvokoProvider.Customer.address + ",";
            sql += IvokoProvider.Customer.organizationtype;
            sql += Contact.customerid + "," + Contact.customername + ",";
            sql += " FROM " + Contact.TABLE;
            sql += " WHERE " + Contact.displayname + " LIKE '" + searchString + "%'";
            sql += " AND " + Contact.userid + " = '" + userId + "'";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        ContactModel dto = new ContactModel();
                        dto.setContactid(cursor.getString(cursor.getColumnIndex(Contact.contactid)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Contact.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Contact.customername)));
                        dto.setCfirstname(cursor.getString(cursor.getColumnIndex(Contact.firstname)));
                        dto.setClname(cursor.getString(cursor.getColumnIndex(Contact.lastname)));
                        dto.setCdisplayname(cursor.getString(cursor.getColumnIndex(Contact.displayname)));
                        dto.setSalutation(cursor.getString(cursor.getColumnIndex(Contact.salutation)));
                        dto.setDesignation(cursor.getString(cursor.getColumnIndex(Contact.designation)));
                        dto.setSpeciality(cursor.getString(cursor.getColumnIndex(Contact.speciality)));
                        dto.setClassification(cursor.getString(cursor.getColumnIndex(Contact.classification)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Contact.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Contact.cemail)));
                        dto.setInternaltype(cursor.getString(cursor.getColumnIndex(Contact.internaltype)));
//                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Contact.address)), GeoAddress.class));
                        contactList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return contactList;
    }*/

    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            else{
                cursor.close();
            }
        }
        return false;
    }

    public String getMaxModifiedDate(String groupId, String userId) {
        String maxDate = "";
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            Cursor cursor = null;

            // pending security checked
          /*  String sql = "";
            sql += "SELECT MAX" + "(" + IvokoProvider.Contact.modifieddate + ") FROM " + IvokoProvider.Contact.TABLE;
            sql += " AND " + Contact.userid + " = '" + userId + "'";*/
            try {
                //cursor = db.rawQuery(sql, null);


                cursor = db.rawQuery("SELECT MAX "+ "(" + IvokoProvider.Contact.modifieddate + ")" + " FROM " + IvokoProvider.Contact.TABLE + " WHERE " + IvokoProvider.Contact.userid + " = ?"  , new String[]{userId});


                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    maxDate = cursor.getString(cursor.getColumnIndex("MAX(modifieddate)"));
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                if (cursor != null)
                    cursor.close();
            } finally {
                if (cursor != null)
                    cursor.close();
            }
        }
        return maxDate;
    }

    public boolean updateContactInfo(String phoneNo, String eMail, String designation, String speciality, String classification, String contactid) {
        boolean isSuccess = true;
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Contact.designation, designation);
        values.put(Contact.phoneno, phoneNo);
        values.put(Contact.cemail, eMail);
        values.put(Contact.speciality, speciality);
        values.put(Contact.classification, classification);
        if (!TextUtils.isEmpty(contactid)) {
            db.update(Contact.TABLE, values, Contact.contactid + "=?", new String[]{contactid});
        }
        return isSuccess;
    }


    public boolean updateContactInfo(SQLiteDatabase db, String phoneNo, String eMail, String designation, String speciality, String classification, String contactid) {
        boolean isSuccess = true;
        ContentValues values = new ContentValues();
        values.put(Contact.designation, designation);
        values.put(Contact.phoneno, phoneNo);
        values.put(Contact.cemail, eMail);
        values.put(Contact.speciality, speciality);
        values.put(Contact.classification, classification);
        if (!TextUtils.isEmpty(contactid)) {
            db.update(Contact.TABLE, values, Contact.contactid + "=?", new String[]{contactid});
        }
        return isSuccess;
    }

    public void deleteContactRecordID(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, Contact.TABLE)) {
            db.execSQL("delete from " + Contact.TABLE + " WHERE " + Contact.contactid + " = '" + id + "'");
        }
    }

    private boolean isRecordExist(SQLiteDatabase db, String contactId) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + Contact.TABLE + " WHERE " + Contact.contactid + " = ?", new String[]{contactId});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
}
