package com.outwork.sudlife.psf.targets.ui;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.targets.adapters.TargetsSummaryAdapter;
import com.outwork.sudlife.psf.targets.adapters.UsersAdapter;
import com.outwork.sudlife.psf.targets.models.MSBModel;
import com.outwork.sudlife.psf.targets.models.TargetSummaryModel;
import com.outwork.sudlife.psf.targets.services.RestServicesForTragets;
import com.outwork.sudlife.psf.targets.targetsmngr.TargetsMgr;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.models.Userprofile;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.lang.reflect.Type;
import java.text.DateFormatSymbols;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamPerformanceViewActivity extends BaseActivity {

    long currentmnth;
    String monthsArray[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    TextView tvMonth;
    private RecyclerView rcvForAll;
    ArrayList<Userprofile> teamMembersArrayList;
    TextView tvNodata;
    Spinner spItems;
    LinearLayout llrcv;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_performance);
        initToolBar();
        tvMonth = (TextView) findViewById(R.id.toolbar_title);
        llrcv = (LinearLayout) findViewById(R.id.ll_in_rcv);
        tvNodata = (TextView) findViewById(R.id.tv_nodata);
        spItems = (Spinner) findViewById(R.id.sp_spinner_items);
        rcvForAll = (RecyclerView) findViewById(R.id.rcv_ftm);
        getTeamMembers();

        spItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (teamMembersArrayList.get(i).getFirstname().equalsIgnoreCase("Team")) {
                    getTeamSummary(String.valueOf(currentmnth));
                } else {
                    getUserTargets(teamMembersArrayList.get(i).getUserid(), String.valueOf(currentmnth));
                    userId = teamMembersArrayList.get(i).getUserid();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.calender_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (id == R.id.calender) {
            monthsCalendarDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        currentmnth = Long.parseLong(TimeUtils.getCurrentDate("MM"));
        textView.setText(getMonth(currentmnth));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        }
    }


    public void monthsCalendarDialog() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.list_item_for_month, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        final ListView lvItems = (ListView) deleteDialogView.findViewById(R.id.lv_month);
        ArrayAdapter arrayAdapter = new ArrayAdapter(TeamPerformanceViewActivity.this, android.R.layout.simple_list_item_1, monthsArray);
        lvItems.setAdapter(arrayAdapter);

        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvMonth.setText(adapterView.getItemAtPosition(i).toString());
                currentmnth = i + 1;
                getUserTargetsBasedonMonth(userId, i + 1);
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    public String getMonth(long month) {
        return new DateFormatSymbols().getMonths()[(int) month - 1];
    }

    public void getListOfMsbRecordsBasedOnMonth(int monthno) {
        ArrayList<TargetSummaryModel> ftmArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> activitiesPlannesArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> visitsPlannedArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> expectedConncetoinsArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> leadsExpectedArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> expectedBusienssArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> gapToTargetArrayList = new ArrayList<>();
        int ftm = 0, activitiesPlanned = 0, achievedConnection = 0, achievedLeads = 0, achievedBusiness = 0, visitsPlanned = 0, expectedConnections = 0, leadsExpected = 0, expectedBusiness = 0, gapToTarget = 0;
        TargetSummaryModel targetSummaryModel;
        ArrayList<MSBModel> msbModelArrayList = TargetsMgr.getInstance(TeamPerformanceViewActivity.this).getMSBRecordsBasedOnMonth(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), monthno);
        if (msbModelArrayList.size() > 0) {
            rcvForAll.setVisibility(View.VISIBLE);
            tvNodata.setVisibility(View.GONE);
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                ftm += Integer.parseInt(msbModelArrayList.get(i).getFtm());
                activitiesPlanned += Integer.parseInt(msbModelArrayList.get(i).getActivitiesplanned());
                visitsPlanned += Integer.parseInt(msbModelArrayList.get(i).getVisitsplanned());
                achievedConnection += Integer.parseInt(msbModelArrayList.get(i).getAchievedconnection());
                achievedLeads += Integer.parseInt(msbModelArrayList.get(i).getAchievedleads());
                achievedBusiness += Integer.parseInt(msbModelArrayList.get(i).getAchievedbusiness());
                expectedConnections += Integer.parseInt(msbModelArrayList.get(i).getExpectedconncetions());
                leadsExpected += Integer.parseInt(msbModelArrayList.get(i).getExpectedleads());
                expectedBusiness += Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                gapToTarget += Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
            }
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(ftm));
                targetSummaryModel.setSummaryName("FTM");
                targetSummaryModel.setAchievedBusiness(String.valueOf(achievedBusiness));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getFtm());
                ftmArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(activitiesPlanned));
                targetSummaryModel.setSummaryName("Activity Planned");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getActivitiesplanned());
                activitiesPlannesArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(visitsPlanned));
                targetSummaryModel.setSummaryName("Visits Planned");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getVisitsplanned());
                visitsPlannedArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(expectedConnections));
                targetSummaryModel.setSummaryName("Expected Connections");
                targetSummaryModel.setAchievedConnection(String.valueOf(achievedConnection));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedconncetions());
                expectedConncetoinsArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(leadsExpected));
                targetSummaryModel.setSummaryName("Expected Leads");
                targetSummaryModel.setAchievedLeads(String.valueOf(achievedLeads));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedleads());
                leadsExpectedArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(expectedBusiness));
                targetSummaryModel.setSummaryName("Expected Business");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedbusiness());
                expectedBusienssArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(gapToTarget));
                targetSummaryModel.setSummaryName("Gap To Target");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                int gaptoTarget = Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                targetSummaryModel.setTotalValue(String.valueOf(gaptoTarget));
                gapToTargetArrayList.add(targetSummaryModel);
            }
            TargetSummaryModel targetSummaryModelFtm = new TargetSummaryModel();
            targetSummaryModelFtm.setSummaryName("FTM");
            targetSummaryModelFtm.setOverallTotal(String.valueOf(ftm));
            targetSummaryModelFtm.setAchievedBusiness(String.valueOf(achievedBusiness));
            targetSummaryModelFtm.setTargetSummaryModelArrayList(ftmArrayList);

            TargetSummaryModel targetSummaryModelap = new TargetSummaryModel();
            targetSummaryModelap.setSummaryName("Activity Planned");
            targetSummaryModelap.setOverallTotal(String.valueOf(activitiesPlanned));
            targetSummaryModelap.setTargetSummaryModelArrayList(activitiesPlannesArrayList);


            TargetSummaryModel targetSummaryModelvp = new TargetSummaryModel();
            targetSummaryModelvp.setSummaryName("Visits Planned");
            targetSummaryModelvp.setOverallTotal(String.valueOf(visitsPlanned));
            targetSummaryModelvp.setTargetSummaryModelArrayList(visitsPlannedArrayList);

            TargetSummaryModel targetSummaryModelec = new TargetSummaryModel();
            targetSummaryModelec.setSummaryName("Expected Connections");
            targetSummaryModelec.setAchievedConnection(String.valueOf(achievedConnection));
            targetSummaryModelec.setOverallTotal(String.valueOf(expectedConnections));
            targetSummaryModelec.setTargetSummaryModelArrayList(expectedConncetoinsArrayList);

            TargetSummaryModel targetSummaryModelel = new TargetSummaryModel();
            targetSummaryModelel.setSummaryName("Expected Leads");
            targetSummaryModelel.setAchievedLeads(String.valueOf(achievedLeads));
            targetSummaryModelel.setOverallTotal(String.valueOf(leadsExpected));
            targetSummaryModelel.setTargetSummaryModelArrayList(leadsExpectedArrayList);

            TargetSummaryModel targetSummaryModeleb = new TargetSummaryModel();
            targetSummaryModeleb.setSummaryName("Expected Business");
            targetSummaryModeleb.setOverallTotal(String.valueOf(expectedBusiness));
            targetSummaryModeleb.setTargetSummaryModelArrayList(expectedBusienssArrayList);

            TargetSummaryModel targetSummaryModelgt = new TargetSummaryModel();
            targetSummaryModelgt.setSummaryName("Gap To Target");
            targetSummaryModelgt.setOverallTotal(String.valueOf(gapToTarget));
            targetSummaryModelgt.setTargetSummaryModelArrayList(gapToTargetArrayList);

            ArrayList<TargetSummaryModel> targetSummaryModels = new ArrayList<>();
            targetSummaryModels.add(targetSummaryModelFtm);
            targetSummaryModels.add(targetSummaryModelap);
            targetSummaryModels.add(targetSummaryModelvp);
            targetSummaryModels.add(targetSummaryModelec);
            targetSummaryModels.add(targetSummaryModelel);
            targetSummaryModels.add(targetSummaryModeleb);
            targetSummaryModels.add(targetSummaryModelgt);

            TargetsSummaryAdapter targetsSummaryAdapter = new TargetsSummaryAdapter(TeamPerformanceViewActivity.this, targetSummaryModels, targetSummaryModelFtm.getTargetSummaryModelArrayList().size() * 10);
            rcvForAll.setAdapter(targetsSummaryAdapter);
            rcvForAll.setLayoutManager(new LinearLayoutManager(TeamPerformanceViewActivity.this));
        } else {
            rcvForAll.setVisibility(View.GONE);
            tvNodata.setVisibility(View.VISIBLE);
        }
    }

    public void getTeamMembers() {
        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamMembers(userToken);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful())
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        Type listType = new TypeToken<ArrayList<Userprofile>>() {
                        }.getType();
                        ArrayList<Userprofile> userprofileArrayList = new Gson().fromJson(response.body().getData(), listType);
                        if (userprofileArrayList != null) {
                            if (userprofileArrayList.size() > 0) {
                                teamMembersArrayList = new ArrayList();
                                Userprofile userprofile = new Userprofile();
                                userprofile.setFirstname("Team");
                                userprofile.setLastname("Summary");
                                userprofile.setUserid("qweq");
                                teamMembersArrayList.add(userprofile);
                                for (int i = 0; i < userprofileArrayList.size(); i++) {
                                    if (userprofileArrayList.get(i).getRole().equalsIgnoreCase("member")) {
                                        teamMembersArrayList.add(userprofileArrayList.get(i));
                                    }
                                }
                                if (teamMembersArrayList.size() != 0) {
                                    spItems.setVisibility(View.VISIBLE);
                                    UsersAdapter usersAdapter = new UsersAdapter(TeamPerformanceViewActivity.this, teamMembersArrayList);
                                    spItems.setAdapter(usersAdapter);
                                }
                            } else {
                                spItems.setVisibility(View.GONE);
                            }
                        } else {
                            spItems.setVisibility(View.GONE);
                        }
                    }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
            }
        });
    }

    public void getUserTargets(String userId, String month) {
        String year = Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd"));

        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getUserTargets(userToken, userId, month, year);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        Type listType = new TypeToken<ArrayList<MSBModel>>() {
                        }.getType();
                        ArrayList<MSBModel> userTargetsArrrayList = new Gson().fromJson(response.body().getData(), listType);
                        if (userTargetsArrrayList != null) {
                            if (userTargetsArrrayList.size() > 0) {
                                getListOfMsbRecordsBasedOnUser(userTargetsArrrayList);
                            }
                        } else {
                            ArrayList<MSBModel> msbModels = new ArrayList<>();
                            getListOfMsbRecordsBasedOnUser(msbModels);
                        }
                    }
                } else {
                    ArrayList<MSBModel> msbModels = new ArrayList<>();
                    getListOfMsbRecordsBasedOnUser(msbModels);
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                ArrayList<MSBModel> msbModels = new ArrayList<>();
                getListOfMsbRecordsBasedOnUser(msbModels);
            }
        });
    }

    public void getTeamSummary(String month) {
        String monthno = Utils.formatDateFromString("yyyy-MM-dd", "MM", TimeUtils.getCurrentDate("yyyy-MM-dd"));
        String year = Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd"));
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");

        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamSummary(userToken, year, month);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        Type listType = new TypeToken<ArrayList<MSBModel>>() {
                        }.getType();
                        ArrayList<MSBModel> userTargetsArrrayList = new Gson().fromJson(response.body().getData(), listType);
                        if (userTargetsArrrayList != null) {
                            if (userTargetsArrrayList.size() > 0) {
                                ArrayList<MSBModel> teamPerformanceList = new ArrayList<>();
                                for (int i = 0; i < teamMembersArrayList.size(); i++) {
                                    for (int j = 0; j < userTargetsArrrayList.size(); j++) {
                                        if (teamMembersArrayList.get(i).getUserid().equalsIgnoreCase(userTargetsArrrayList.get(j).getMemberid())) {
                                            userTargetsArrrayList.get(j).setCustomername(teamMembersArrayList.get(i).getFirstname() + " " + teamMembersArrayList.get(i).getLastname());
                                            teamPerformanceList.add(userTargetsArrrayList.get(j));
                                        }
                                    }
                                }
                                getListOfMsbRecordsBasedOnUser(teamPerformanceList);
                            }
                        } else {
                            ArrayList<MSBModel> msbModels = new ArrayList<>();
                            getListOfMsbRecordsBasedOnUser(msbModels);
                        }
                    } else {
                        ArrayList<MSBModel> msbModels = new ArrayList<>();
                        getListOfMsbRecordsBasedOnUser(msbModels);
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                ArrayList<MSBModel> msbModels = new ArrayList<>();
                getListOfMsbRecordsBasedOnUser(msbModels);
            }
        });
    }

    public void getUserTargetsBasedonMonth(String userId, int monthno) {
        String year = Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd"));
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");

        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getUserTargets(userToken, userId, String.valueOf(monthno), year);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        Type listType = new TypeToken<ArrayList<MSBModel>>() {
                        }.getType();
                        ArrayList<MSBModel> userTargetsArrrayList = new Gson().fromJson(response.body().getData(), listType);
                        if (userTargetsArrrayList != null) {
                            if (userTargetsArrrayList.size() > 0) {
                                getListOfMsbRecordsBasedOnUser(userTargetsArrrayList);
                            }
                        } else {
                            ArrayList<MSBModel> msbModels = new ArrayList<>();
                            getListOfMsbRecordsBasedOnUser(msbModels);
                        }
                    }
                } else {
                    ArrayList<MSBModel> msbModels = new ArrayList<>();
                    getListOfMsbRecordsBasedOnUser(msbModels);
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                ArrayList<MSBModel> msbModels = new ArrayList<>();
                getListOfMsbRecordsBasedOnUser(msbModels);
            }
        });
    }

    public void getListOfMsbRecordsBasedOnUser(ArrayList<MSBModel> msbModelArrayList) {
        ArrayList<TargetSummaryModel> ftmArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> activitiesPlannesArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> visitsPlannedArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> expectedConncetoinsArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> leadsExpectedArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> expectedBusienssArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> gapToTargetArrayList = new ArrayList<>();
        int ftm = 0, activitiesPlanned = 0, achievedConnection = 0, achievedLeads = 0, achievedBusiness = 0, visitsPlanned = 0, expectedConnections = 0, leadsExpected = 0, expectedBusiness = 0, gapToTarget = 0;
        TargetSummaryModel targetSummaryModel;
        if (msbModelArrayList.size() > 0) {
            rcvForAll.setVisibility(View.VISIBLE);
            tvNodata.setVisibility(View.GONE);
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                ftm += Integer.parseInt(msbModelArrayList.get(i).getFtm());
                activitiesPlanned += Integer.parseInt(msbModelArrayList.get(i).getActivitiesplanned());
                visitsPlanned += Integer.parseInt(msbModelArrayList.get(i).getVisitsplanned());
                achievedConnection += Integer.parseInt(msbModelArrayList.get(i).getAchievedconnection());
                achievedLeads += Integer.parseInt(msbModelArrayList.get(i).getAchievedleads());
                achievedBusiness += Integer.parseInt(msbModelArrayList.get(i).getAchievedbusiness());
                expectedConnections += Integer.parseInt(msbModelArrayList.get(i).getExpectedconncetions());
                leadsExpected += Integer.parseInt(msbModelArrayList.get(i).getExpectedleads());
                expectedBusiness += Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                gapToTarget += Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
            }
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(ftm));
                targetSummaryModel.setSummaryName("FTM");
                targetSummaryModel.setAchievedBusiness(String.valueOf(achievedBusiness));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getFtm());
                ftmArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(activitiesPlanned));
                targetSummaryModel.setSummaryName("Activity Planned");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getActivitiesplanned());
                activitiesPlannesArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(visitsPlanned));
                targetSummaryModel.setSummaryName("Visits Planned");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getVisitsplanned());
                visitsPlannedArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(expectedConnections));
                targetSummaryModel.setSummaryName("Expected Connections");
                targetSummaryModel.setAchievedConnection(String.valueOf(achievedConnection));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedconncetions());
                expectedConncetoinsArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(leadsExpected));
                targetSummaryModel.setSummaryName("Expected Leads");
                targetSummaryModel.setAchievedLeads(String.valueOf(achievedLeads));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedleads());
                leadsExpectedArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(expectedBusiness));
                targetSummaryModel.setSummaryName("Expected Business");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedbusiness());
                expectedBusienssArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(gapToTarget));
                targetSummaryModel.setSummaryName("Gap To Target");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                int gaptoTarget = Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                targetSummaryModel.setTotalValue(String.valueOf(gaptoTarget));
                gapToTargetArrayList.add(targetSummaryModel);
            }
            TargetSummaryModel targetSummaryModelFtm = new TargetSummaryModel();
            targetSummaryModelFtm.setSummaryName("FTM");
            targetSummaryModelFtm.setOverallTotal(String.valueOf(ftm));
            targetSummaryModelFtm.setAchievedBusiness(String.valueOf(achievedBusiness));
            targetSummaryModelFtm.setTargetSummaryModelArrayList(ftmArrayList);

            TargetSummaryModel targetSummaryModelap = new TargetSummaryModel();
            targetSummaryModelap.setSummaryName("Activity Planned");
            targetSummaryModelap.setOverallTotal(String.valueOf(activitiesPlanned));
            targetSummaryModelap.setTargetSummaryModelArrayList(activitiesPlannesArrayList);


            TargetSummaryModel targetSummaryModelvp = new TargetSummaryModel();
            targetSummaryModelvp.setSummaryName("Visits Planned");
            targetSummaryModelvp.setOverallTotal(String.valueOf(visitsPlanned));
            targetSummaryModelvp.setTargetSummaryModelArrayList(visitsPlannedArrayList);

            TargetSummaryModel targetSummaryModelec = new TargetSummaryModel();
            targetSummaryModelec.setSummaryName("Expected Connections");
            targetSummaryModelec.setAchievedConnection(String.valueOf(achievedConnection));
            targetSummaryModelec.setOverallTotal(String.valueOf(expectedConnections));
            targetSummaryModelec.setTargetSummaryModelArrayList(expectedConncetoinsArrayList);

            TargetSummaryModel targetSummaryModelel = new TargetSummaryModel();
            targetSummaryModelel.setSummaryName("Expected Leads");
            targetSummaryModelel.setAchievedLeads(String.valueOf(achievedLeads));
            targetSummaryModelel.setOverallTotal(String.valueOf(leadsExpected));
            targetSummaryModelel.setTargetSummaryModelArrayList(leadsExpectedArrayList);

            TargetSummaryModel targetSummaryModeleb = new TargetSummaryModel();
            targetSummaryModeleb.setSummaryName("Expected Business");
            targetSummaryModeleb.setOverallTotal(String.valueOf(expectedBusiness));
            targetSummaryModeleb.setTargetSummaryModelArrayList(expectedBusienssArrayList);

            TargetSummaryModel targetSummaryModelgt = new TargetSummaryModel();
            targetSummaryModelgt.setSummaryName("Gap To Target");
            targetSummaryModelgt.setOverallTotal(String.valueOf(gapToTarget));
            targetSummaryModelgt.setTargetSummaryModelArrayList(gapToTargetArrayList);

            ArrayList<TargetSummaryModel> targetSummaryModels = new ArrayList<>();
            targetSummaryModels.add(targetSummaryModelFtm);
            targetSummaryModels.add(targetSummaryModelap);
            targetSummaryModels.add(targetSummaryModelvp);
            targetSummaryModels.add(targetSummaryModelec);
            targetSummaryModels.add(targetSummaryModelel);
            targetSummaryModels.add(targetSummaryModeleb);
            targetSummaryModels.add(targetSummaryModelgt);

            TargetsSummaryAdapter targetsSummaryAdapter = new TargetsSummaryAdapter(TeamPerformanceViewActivity.this, targetSummaryModels, targetSummaryModelFtm.getTargetSummaryModelArrayList().size() * 10);
            rcvForAll.setAdapter(targetsSummaryAdapter);
            rcvForAll.setLayoutManager(new LinearLayoutManager(TeamPerformanceViewActivity.this));
        } else {
            rcvForAll.setVisibility(View.GONE);
            tvNodata.setVisibility(View.VISIBLE);
        }
    }
}







