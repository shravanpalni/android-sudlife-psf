package com.outwork.sudlife.psf.more.models;

import android.content.Context;
import android.text.TextUtils;

import com.outwork.sudlife.psf.dto.UserObjectDto;

import org.json.JSONObject;

public class S3Credentials {

    private String AccessKey;
    private String SecretKey;
    private String SessionToken;
    private String ExpiryTime;
    private String bucketName;

    public String getSessionToken() {
        return SessionToken;
    }

    public void setSessionToken(String sessionToken) {
        SessionToken = sessionToken;
    }

    public String getSecretKey() {
        return SecretKey;
    }

    public void setSecretKey(String secretKey) {
        SecretKey = secretKey;
    }

    public String getAccessKey() {
        return AccessKey;
    }

    public void setAccessKey(String accessKey) {
        AccessKey = accessKey;
    }

    public String getExpiryTime() {
        return ExpiryTime;
    }

    public void setExpiryTime(String expiryTime) {
        ExpiryTime = expiryTime;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public static S3Credentials getS3Credentials(Context context, UserObjectDto userObjectDto)  {

        S3Credentials s3Credentials = new S3Credentials();

        JSONObject response = getTempCredentialsfromServer(context,userObjectDto);
        try {
            if (!TextUtils.isEmpty(response.getString(JSON_ACCESS_KEY))){
                s3Credentials.setAccessKey(response.getString(JSON_ACCESS_KEY));
               s3Credentials.setSecretKey(response.getString(JSON_SECRETACCESS_KEY));
                s3Credentials.setSessionToken(response.getString(JSON_SESSION_TOKEN));
                s3Credentials.setExpiryTime(response.getString(JSON_EXPIRYTIME));
            }
        } catch (Exception e){
        }
        return s3Credentials;
    }

    public static final String JSON_ACCESS_KEY = "AccessKeyId";
    public static final String JSON_SECRETACCESS_KEY = "SecretAccessKey";
    public static final String JSON_SESSION_TOKEN ="SessionToken";
    public static final String JSON_EXPIRYTIME = "Expiration";
    public static final String JSON_BUCKETNAME = "bucketname";

    private static JSONObject getTempCredentialsfromServer(Context context, UserObjectDto userObjectDto){
        JSONObject response = new JSONObject();

       /*String serverBaseUrl = context.getString(R.string.main_url);

        ConnectionClient client = new ConnectionClient(serverBaseUrl+"FileService/temporarycredentials2");
        client.addHeaders("utoken", userObjectDto.getUserToken());
        try {
			//client.addParams("fileName", "Ravi");
			//client.addParams("extension", "jpg");
            client.execute(ConnectionClient.ConnectionType.URI_PARAMS, "");
             response = new JSONObject(client.getResponse());

           return  response;


        }catch (Exception e){

        }*/
        return response;
    }


    public static S3Credentials parseS3Credentials(JSONObject credentials){
        S3Credentials s3Credentials = new S3Credentials();
        JSONObject response = credentials;
        try {
            if (!TextUtils.isEmpty(response.getString(JSON_ACCESS_KEY))){
                s3Credentials.setAccessKey(response.getString(JSON_ACCESS_KEY));
                s3Credentials.setSecretKey(response.getString(JSON_SECRETACCESS_KEY));
                s3Credentials.setSessionToken(response.getString(JSON_SESSION_TOKEN));
                s3Credentials.setExpiryTime(response.getString(JSON_EXPIRYTIME));
            }
        } catch (Exception e){
        }
        return s3Credentials;
    }
}
