package com.outwork.sudlife.psf.ui.models;

/**
 * Created by Habi on 01-08-2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Deviceinfo {

    @SerializedName("deviceid")
    @Expose
    private Object deviceid;
    @SerializedName("deviceos")
    @Expose
    private Object deviceos;
    @SerializedName("osversion")
    @Expose
    private Object osversion;

    public Object getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(Object deviceid) {
        this.deviceid = deviceid;
    }

    public Object getDeviceos() {
        return deviceos;
    }

    public void setDeviceos(Object deviceos) {
        this.deviceos = deviceos;
    }

    public Object getOsversion() {
        return osversion;
    }

    public void setOsversion(Object osversion) {
        this.osversion = osversion;
    }

}