package com.outwork.sudlife.psf.restinterfaces.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Panch on 1/12/2017.
 */

public class PermissionSettings {

        @SerializedName("finelocation")
        @Expose
        private String finelocation;

        @SerializedName("coarselocation")
        @Expose
        private String coarselocation;

        public String getCoarselocation() {
                return coarselocation;
        }

        public void setCoarselocation(String coarselocation) {
                this.coarselocation = coarselocation;
        }

        public String getFinelocation() {
                return finelocation;
        }

        public void setFinelocation(String finelocation) {
                this.finelocation = finelocation;
        }


}
