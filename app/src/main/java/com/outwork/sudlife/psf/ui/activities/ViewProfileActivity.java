package com.outwork.sudlife.psf.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.receiver.ConnectivityReceiver;
import com.outwork.sudlife.psf.ui.base.AppBaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

public class ViewProfileActivity extends AppBaseActivity {
    private TextView userName, userEmail, userLocation, userPhoneNo, appfeedback, logout, invite, email, whatsapp, cancel, toolbar_title, empid;
    private boolean myProfile = true;
    private String requestType, userId, userToken;
    //private ImageView profileImage;
    private View emailseperator, contactseperator, locationseperator;
    private View modalbottomsheet;
    private BottomSheetDialog bottomSheetDialog;
    private String loginType;
    private RelativeLayout inviteLayout, changepwdlayout;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;
    private boolean doubleBackToExitPressedOnce = true;

    private ConnectivityReceiver mNetworkReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewprofile);

        mgr = LocalBroadcastManager.getInstance(this);
        mNetworkReceiver = new ConnectivityReceiver();
        userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        userId = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
        loginType = SharedPreferenceManager.getInstance().getString(Constants.LOGINTYPE, "");
        myProfile = getIntent().getBooleanExtra("SELF", true);
        requestType = "OTHERS";
        if (!myProfile) {
            userId = getIntent().getStringExtra("USERID");
        }
        if (SharedPreferenceManager.getInstance().getString(Constants.PROFILE_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
//                startService(new Intent(ViewProfileActivity.this, ProfileIntentService.class));
            }
        }
        initializeToolBar();
        viewInitialize();
        initializeValues();
        initlisteners();
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoTypeface, userName, userEmail, userLocation, userPhoneNo, appfeedback, logout, empid,
                (TextView) findViewById(R.id.version_tv));
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeValues();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("profile_broadcast"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = false;
            showToast("Please click BACK again to exit.");
        } else {
            finish();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
        super.onStop();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (mNetworkReceiver != null)
                unregisterReceiver(mNetworkReceiver);
        }
    }

    private void viewInitialize() {
        userName = (TextView) findViewById(R.id.uName);
        empid = (TextView) findViewById(R.id.empid);
        userEmail = (TextView) findViewById(R.id.uEmail);
        emailseperator = (View) findViewById(R.id.emailseperator);
        userPhoneNo = (TextView) findViewById(R.id.uContact);
        contactseperator = (View) findViewById(R.id.contactseperator);
        userLocation = (TextView) findViewById(R.id.uLocation);
        locationseperator = (View) findViewById(R.id.locationseperator);
        //profileImage = (ImageView) findViewById(R.id.profileImage);
        appfeedback = (TextView) findViewById(R.id.appfeedback);
        changepwdlayout = (RelativeLayout) findViewById(R.id.changepwdlayout);
        logout = (TextView) findViewById(R.id.logout);
    }

    private void initlisteners() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                initializeValues();
            }
        };

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferenceManager.getInstance().clear();
                Intent intent = new Intent(ViewProfileActivity.this, SignInActvity.class);
                intent.putExtra("finish", true); // if you are checking for this in your other Activities
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        changepwdlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent feedback = new Intent(ViewProfileActivity.this, ResetPasswordActivity.class);
                startActivity(feedback);
            }
        });
        appfeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent feedback = new Intent(ViewProfileActivity.this, FeedBackActivity.class);
                feedback.putExtra("SELF", true);
                startActivity(feedback);
            }
        });
    }

    private void initializeValues() {
       /* if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.PROFILE_URL, ""))) {
            Glide.with(ViewProfileActivity.this).load(SharedPreferenceManager.getInstance().getString(Constants.PROFILE_URL, ""))
                    .transform(new CircleTransform(ViewProfileActivity.this)).placeholder(R.drawable.ic_user)
                    .crossFade()
                    .into(profileImage);
        }*/
        userName.setText(SharedPreferenceManager.getInstance().getString(Constants.FIRSTNAME, "") + " " + SharedPreferenceManager.getInstance().getString(Constants.LASTNAME, ""));
        if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGINID, ""))) {
            empid.setVisibility(View.VISIBLE);
            empid.setText("EmpID : " + SharedPreferenceManager.getInstance().getString(Constants.LOGINID, ""));
        } else {
            empid.setVisibility(View.GONE);
        }
        userEmail.setText(SharedPreferenceManager.getInstance().getString(Constants.EMAIL, ""));
        userEmail.setVisibility(View.VISIBLE);
        emailseperator.setVisibility(View.VISIBLE);

        userPhoneNo.setText(SharedPreferenceManager.getInstance().getString(Constants.PHONENO, ""));
        userPhoneNo.setVisibility(View.VISIBLE);
        contactseperator.setVisibility(View.VISIBLE);

        userLocation.setText(SharedPreferenceManager.getInstance().getString(Constants.CITY, ""));
        userLocation.setVisibility(View.VISIBLE);
        locationseperator.setVisibility(View.VISIBLE);
    }

    private void initializeToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.vProfileToolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("My Profile");
        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        notification.setVisibility(View.GONE);
        TextView  textCartItemCount = (TextView) findViewById(R.id.notification_badge);
        textCartItemCount.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_menu);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == android.R.id.home) {
//            return true;
//        }
        if (item.getItemId() == R.id.edit) {
            Intent in = new Intent(ViewProfileActivity.this, EditProfileActivity.class);
            startActivity(in);
//            return true;
        }
        if (item.getItemId() == R.id.share) {
            bottomSheetDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    public void bottomSheetDialog() {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_sheet, null);
        email = (TextView) modalbottomsheet.findViewById(R.id.mail);
        //sms = (TextView) modalbottomsheet.findViewById(R.id.message);
        whatsapp = (TextView) modalbottomsheet.findViewById(R.id.whatsapp);
        cancel = (TextView) modalbottomsheet.findViewById(R.id.cancel);
        bottomSheetDialog = new BottomSheetDialog(ViewProfileActivity.this);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(true);
        bottomSheetDialog.setCancelable(true);
        bottomSheetDialog.show();
        Utils.setTypefaces(IvokoApplication.robotoTypeface, email, whatsapp, cancel);
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, (TextView) modalbottomsheet.findViewById(R.id.select_tv));
        final String body = SharedPreferenceManager.getInstance().getString(Constants.FIRSTNAME, "") + " " + SharedPreferenceManager.getInstance().getString(Constants.LASTNAME, "")
                + "\n" + SharedPreferenceManager.getInstance().getString(Constants.EMAIL, "")
                + "\n" + SharedPreferenceManager.getInstance().getString(Constants.PHONENO, "");
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:?subject=" + "Please find my contact details" + "&body=" + body);
                intent.setData(data);
                try {
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException ex) {
                    showToast("Mail app have not been installed.");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
      /*  sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                dispatchsmsIntent();
            }
        });*/
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.setPackage("com.whatsapp");
//                    intent.putExtra(Intent.EXTRA_SUBJECT, "Please find my contact details");
                intent.putExtra(Intent.EXTRA_TEXT, "Please find my contact details" + "\n" + body);
                try {
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException ex) {
                    showToast("Whatsapp have not been installed.");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }



}
