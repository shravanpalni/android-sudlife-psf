package com.outwork.sudlife.psf.more.service;

import com.outwork.sudlife.psf.more.models.Msg;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AdminService {

    @GET("Admin/posts")
    Call<RestResponse> getAdminFeed(@Header("utoken") String userToken, @Query("groupid") String groupid,
                                    @Query("initialid") String initialid,
                                    @Query("count") String count,
                                    @Query("direction") String direction,
                                    @Query("date") String date);

    @POST("Admin/announcements")
    Call<RestResponse> postAdminMessage (@Header("utoken") String userToken,
                                         @Query("groupid") String groupid,
                                         @Query("type") String type,
                                         @Body Msg sJsonBody);


    @GET("Message/message")
    Call<RestResponse> getAdminMessage(@Header("utoken") String userToken,
                                       @Query("type") String type,
                                       @Query("subtype") String subtype,
                                       @Query("messageid") String messageid);


}
