package com.outwork.sudlife.psf.lead.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.localbase.StaticDataDto;
import com.outwork.sudlife.psf.localbase.StaticDataMgr;
import com.outwork.sudlife.psf.notifications.GlobalData;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateFamilyMemberActivity extends AppCompatActivity {
    EditText etName, etRelation, etOccupation, etLeadType, etAge, etWorking;
    TextInputLayout tilOccupation;

    LeadModel leadModel;
    ListView lvBottom;
    Button btnFmCreate;
    BottomSheetDialog bottomSheetDialog;
    ArrayList<String> stringArrayList;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_family_member);
        tilOccupation = (TextInputLayout) findViewById(R.id.til_occupation);
        etName = (EditText) findViewById(R.id.et_fm_name);
        etRelation = (EditText) findViewById(R.id.et_fm_relation);
        etAge = (EditText) findViewById(R.id.et_fm_age);
        etWorking = (EditText) findViewById(R.id.et_fm_working);
        etOccupation = (EditText) findViewById(R.id.et_fm_occupation);
        leadModel = GlobalData.getInstance().getLeadModelForFm();

        type = getIntent().getStringExtra("type");

        initToolBar();

//        if (type.equalsIgnoreCase("update")) {
//            etName.setText(leadModel.getName());
//            etRelation.setText(leadModel.getRelation());
//            etAge.setText(String.valueOf(leadModel.getAge()));
//            if (leadModel.isIsworking()) {
//                etWorking.setText("Yes");
//                tilOccupation.setVisibility(View.VISIBLE);
//                etOccupation.setText(leadModel.getOccupation());
//            } else {
//            }
//        }

        etRelation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<StaticDataDto> staticList = new StaticDataMgr(CreateFamilyMemberActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "memberrelation");
                if (staticList.size() != 0) {
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i < staticList.size(); i++) {
                        strings.add(staticList.get(i).getName());
                    }
                    bottomSheetDialog(etRelation, strings, "Select Relation");
                }
            }
        });

        etWorking.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Yes");
                stringArrayList.add("No");
                bottomSheetDialog(etWorking, stringArrayList, "Select Working Yes or No");
            }

        });
        findViewById(R.id.btn_fm_create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equalsIgnoreCase("update")) {
                    if (isNetworkAvailable()) {
                        if (etAge.getText().toString().length() == 0 && Integer.parseInt(etAge.getText().toString()) > 120) {
                            etAge.setError("Age is below 120");
                        } else {
//                            LeadModel fmLeadModel = new LeadModel();
//                            fmLeadModel.setAge(Integer.parseInt(etAge.getText().toString()));
//                            fmLeadModel.setFamilymemberid(leadModel.getFamilymemberid());
//                            fmLeadModel.setLeadid(leadModel.getLeadid());
//                            fmLeadModel.setName(etName.getText().toString());
//                            fmLeadModel.setRelation(etRelation.getText().toString());
//                            if (etWorking.getText().toString().length() != 0) {
//                                if (etWorking.getText().toString().equalsIgnoreCase("Yes")) {
//                                    fmLeadModel.setIsworking(true);
//                                } else {
//                                    fmLeadModel.setIsworking(true);
//                                }
//                                fmLeadModel.setOccupation(etOccupation.getText().toString());
//                            }
//                            updateFamilyMember(fmLeadModel);
                        }
                    } else {
                        showAlertDialog("No Internet Conncection,Make sure Wi-Fi or Cellular data is turend on");
                    }

                } else {
                    if (isNetworkAvailable()) {
                        if (etAge.getText().toString().length() == 0 && Integer.parseInt(etAge.getText().toString()) > 120) {
                            etAge.setError("Age is below 120");
                        } else {
                            LeadModel fmLeadModel = new LeadModel();
                            fmLeadModel.setAge(Integer.parseInt(etAge.getText().toString()));
                            fmLeadModel.setLeadid(GlobalData.getInstance().getLeadModel().getLeadid());
//                            fmLeadModel.setName(etName.getText().toString());
//                            fmLeadModel.setRelation(etRelation.getText().toString());
//                            if (etWorking.getText().toString().length() != 0) {
//                                if (etWorking.getText().toString().equalsIgnoreCase("Yes")) {
//                                    fmLeadModel.setIsworking(true);
//                                } else {
//                                    fmLeadModel.setIsworking(false);
//                                }
//                                fmLeadModel.setOccupation(etOccupation.getText().toString());
//                            }
                            postFamilyMember(fmLeadModel);
                        }
                    } else {
                        showAlertDialog("No Internet Conncection,Make sure Wi-Fi or Cellular data is turend on");
                    }
                }
            }
        });

    }

    public void postFamilyMember(LeadModel leadModel) {
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> postFM = client.postFM(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), leadModel);
        postFM.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        showAlertDialog(response.body().getMessage());
                    }
                } else {
                    showAlertDialog("Failed to Create");
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                showAlertDialog("Failed to Create");

            }
        });
    }


    public void updateFamilyMember(LeadModel leadModel) {
        FormsService client = RestService.createServicev1(FormsService.class);
//        Call<RestResponse> postFM = client.updateFamilyMember(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), leadModel.getFamilymemberid(), leadModel);
//        postFM.enqueue(new Callback<RestResponse>() {
//            @Override
//            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
//                int statusCode = response.code();
//                if (statusCode == 200) {
//                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
//                        showAlertDialog(response.body().getMessage());
//                    }
//                } else {
//                    showAlertDialog("Failed to Update");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<RestResponse> call, Throwable t) {
//                showAlertDialog("Failed to Update");
//
//            }
//        });
      /*  try {
            Response<RestResponse> restResponse = postFM.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                    showAlertDialog(restResponse.message());
                }
            } else {
                showAlertDialog("Failed to Create");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }


    public void bottomSheetDialog(final EditText editText, ArrayList<String> strings, String title) {
        View modalbottomsheet = CreateFamilyMemberActivity.this.getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(CreateFamilyMemberActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(CreateFamilyMemberActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Yes")) {
                    tilOccupation.setVisibility(View.VISIBLE);
                } else {
                    tilOccupation.setVisibility(View.GONE);
                }

                bottomSheetDialog.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });

    }

    public void showAlertDialog(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(CreateFamilyMemberActivity.this).create();
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.show();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) CreateFamilyMemberActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }


    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        if (type.equalsIgnoreCase("create")) {
            textView.setText("Add Family Member");

        } else {
            textView.setText("Update Family Member");
        }
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }
}