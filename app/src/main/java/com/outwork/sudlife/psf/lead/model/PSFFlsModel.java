package com.outwork.sudlife.psf.lead.model;

/**
 * Created by shravanch on 20-12-2019.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "leadid",
        "assignedtoid",
        "leadstage",
        "leadsource",
        "leadcode"
})
public class PSFFlsModel {

    @JsonProperty("leadid")
    private String leadid;
    @JsonProperty("assignedtoid")
    private String assignedtoid;
    @JsonProperty("leadstage")
    private String leadstage;
    @JsonProperty("leadcode")
    private String leadcode;

    public String getLeadsource() {
        return leadsource;
    }

    public void setLeadsource(String leadsource) {
        this.leadsource = leadsource;
    }

    @JsonProperty("leadsource")
    private String leadsource;
    @JsonProperty("leadid")
    public String getLeadid() {
        return leadid;
    }

    @JsonProperty("leadid")
    public void setLeadid(String leadid) {
        this.leadid = leadid;
    }

    @JsonProperty("assignedtoid")
    public String getAssignedtoid() {
        return assignedtoid;
    }

    @JsonProperty("assignedtoid")
    public void setAssignedtoid(String assignedtoid) {
        this.assignedtoid = assignedtoid;
    }

    @JsonProperty("leadstage")
    public String getLeadstage() {
        return leadstage;
    }

    @JsonProperty("leadstage")
    public void setLeadstage(String leadstage) {
        this.leadstage = leadstage;
    }

    @JsonProperty("leadcode")
    public String getLeadcode() {
        return leadcode;
    }
    @JsonProperty("leadcode")
    public void setLeadcode(String leadcode) {
        this.leadcode = leadcode;
    }

}

















