package com.outwork.sudlife.psf.lead.activities;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.BranchesMgr;
import com.outwork.sudlife.psf.branches.activities.ListCustomerActivity;
import com.outwork.sudlife.psf.branches.activities.ShowHierarchyActivity;
import com.outwork.sudlife.psf.branches.models.BranchesModel;
import com.outwork.sudlife.psf.branches.services.ContactsIntentService;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.adapters.PSFLeadsAdapter;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.activities.CPlannerActivity;
import com.outwork.sudlife.psf.planner.adapter.BranchesPlannerAdapter;
import com.outwork.sudlife.psf.planner.adapter.DummyAdapter;
import com.outwork.sudlife.psf.planner.models.DataModel;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.activities.NotificationsListActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


/**
 * Created by shravanch on 26-12-2019.
 */

public class PSFCalenderActivity extends BaseActivity {


    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private CalendarView compactCalendarView;
    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());
    private SimpleDateFormat dateFormatDisplaying = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private List<PlannerModel> plannerModelList = new ArrayList<>();
    private List<PlannerModel> finalPlannerModelList = new ArrayList<>();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView planRecyclerView;
    private ProgressBar msgListProgressBar;
    private TextView nomessageview, toolbar_title;
    private FloatingActionButton addplan;
    //private CalenderPlannerAdapter calenderPlannerAdapter;
    private PSFLeadsAdapter calenderPlannerAdapter;
    private BranchesPlannerAdapter branchesPlannerAdapter;
    private LinearLayout branches;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;
    private int day, monthno, year;
    private String toolbardate, plandate, displaydate;
    private List<BranchesModel> customerList = new ArrayList<>();
    private ArrayList<String> stringArrayList;
    private ArrayList<DataModel> branchNamesWithColorsArrayList;
    private ArrayList<DataModel> colorsDataModelArrayList;
    private ArrayList<Integer> tagArrayList;
    private boolean clicked = false;
    private Integer viewid;
    TextView textCartItemCount;
    int mCartItemCount = 10;
    List<PSFLeadModel> leadModels = new ArrayList<>();
    private List<PSFLeadModel> leadsModelList = new ArrayList<>();


    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psf_calender);

        final CalendarView datePicker = (CalendarView) findViewById(R.id.compactcalendar_view);

    }*/



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psf_calender);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        mgr = LocalBroadcastManager.getInstance(PSFCalenderActivity.this);
        if (SharedPreferenceManager.getInstance().getString(Constants.CUSTOMERS_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                ContactsIntentService.insertCustomersinDB(PSFCalenderActivity.this);
            }
        }
        if (isNetworkAvailable()) {
            if (PlannerMgr.getInstance(PSFCalenderActivity.this).getOfflineplannerList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline").size() > 0)
                LeadIntentService.syncLeadstoServer(PSFCalenderActivity.this);
        }
        planRecyclerView = (RecyclerView) findViewById(R.id.fflogRecycler);
        branches = (LinearLayout) findViewById(R.id.branches);
        branches.setVisibility(View.GONE);
        nomessageview = (TextView) findViewById(R.id.ffnomessages);
        msgListProgressBar = (ProgressBar) findViewById(R.id.ffmessageListProgressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.task_swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.topbarcolor, R.color.daycolor);
        addplan = (FloatingActionButton) findViewById(R.id.addplan);
        //compactCalendarView = (CalendarView) findViewById(R.id.compactcalendar_view);



        compactCalendarView = (CalendarView) findViewById(R.id.calendarView);

         leadModels = LeadMgr.getInstance(PSFCalenderActivity.this).getPSFLeadsList(userid);


        toolbardate = TimeUtils.getCurrentDate("yyyy-MM-dd");
        displaydate = TimeUtils.getCurrentDate("dd/MM/yyyy hh:mm a");
        initToolBar();
        day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
        monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
        year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
        showlist(day, monthno, year);

        //compactCalendarView.setUseThreeLetterAbbreviation(false);
        //compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);
        //compactCalendarView.invalidate();
        //logEventsByMonth(compactCalendarView);
       // toolbar_title.setText(dateFormatForMonth.format(compactCalendarView.getSelectedDate()));// review
       // plannerModelList = PlannerMgr.getInstance(PSFCalenderActivity.this).getPlannerList(userid);
        //getBranches();
        setListener();
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoTypeface, nomessageview);
    }



    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("customer_broadcast"));
        //mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("plan_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Utils.isNotNullAndNotEmpty(toolbardate)) {
            day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
            monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
            year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
            showlist(day, monthno, year);
        }
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        textCartItemCount = (TextView) findViewById(R.id.notification_badge);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showToast("Notification Clicked!!!");
                Intent in = new Intent(PSFCalenderActivity.this, NotificationsListActivity.class);
                startActivity(in);
            }
        });

        setupBadge();
    }
    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void setListener() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Date c = Calendar.getInstance().getTime();
                //compactCalendarView.setCurrentDate(c);
                day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
                monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
                year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
                showlist(day, monthno, year);
               // getBranches();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //getBranches();
                if (Utils.isNotNullAndNotEmpty(toolbardate)) {
                    day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
                    monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
                    year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
                    showlist(day, monthno, year);
                }
            }
        };
        addplan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            /*    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) >= 1 &&
                        SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) < 4) {*/
                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) >= 1 &&
                        SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) < 5) {  // added on Feb 4
                    //Intent intent = new Intent(PSFCalenderActivity.this, TeamHierarchyActivity.class);
                    Intent intent = new Intent(PSFCalenderActivity.this, ShowHierarchyActivity.class);
                    intent.putExtra(Constants.PLAN_STATUS, 1);
                    intent.putExtra(Constants.PLAN_DATE, displaydate);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(PSFCalenderActivity.this, ListCustomerActivity.class);
                    intent.putExtra(Constants.PLAN_STATUS, 1);
                    intent.putExtra(Constants.PLAN_DATE, displaydate);
                    intent.putExtra("type", "select");
                    intent.putExtra("fromclass", "plan");
                    startActivity(intent);
                }
            }
        });





       /* compactCalendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Calendar clickedDayCalendar = eventDay.getCalendar();
                Toast.makeText(getApplicationContext(), "Selected Date:\n" + "Day = " + clickedDayCalendar.get(Calendar.DAY_OF_MONTH) + "\n" + "Month = " + clickedDayCalendar.get(Calendar.MONTH) + "\n" + "Year = " + clickedDayCalendar.get(Calendar.YEAR), Toast.LENGTH_LONG).show();
            }
        });*/


        compactCalendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {


                String t = eventDay.getCalendar().getTime().toString();
                Log.i("shravan","t - - -"+t);
                SimpleDateFormat sdf3 = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
                Date d1 = null;
                try{
                    d1 = sdf3.parse(t);

                }catch (Exception e){ e.printStackTrace(); }
                System.out.println("check..." + d1);
                Log.i("shravan","d1 - - -"+d1);

               int year =  eventDay.getCalendar().get(Calendar.YEAR);
                int month = eventDay.getCalendar().get(Calendar.MONTH);
                int day = eventDay.getCalendar().get(Calendar.DAY_OF_MONTH);
                Log.i("shravan","year - - -"+year);
                Log.i("shravan","month - - -"+month);
                Log.i("shravan","day - - -"+day);


                toolbar_title.setText(dateFormatForMonth.format(d1));
                String date = (dateFormat.format(d1));
                displaydate = dateFormatDisplaying.format(d1);
                day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", date));
                monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", date));
                year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", date));
                //getBranches();
                showlist(day, monthno, year);

            }





        });















        /*compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                toolbar_title.setText(dateFormatForMonth.format(dateClicked));
                String date = (dateFormat.format(dateClicked));
                displaydate = dateFormatDisplaying.format(dateClicked);
                day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", date));
                monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", date));
                year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", date));
                getBranches();
                showlist(day, monthno, year);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                toolbar_title.setText(dateFormatForMonth.format(firstDayOfNewMonth));
                displaydate = dateFormatDisplaying.format(firstDayOfNewMonth);
//                String date = (dateFormat.format(firstDayOfNewMonth));
                SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                String[] date = dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()).split("-");
                showlist(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]));
            }
        });*/
    }

/*    private void getBranches() {
        if (clicked) {
            return;
        }
        customerList.clear();
        branches.removeAllViews();
        customerList = new BranchesMgr(PSFCalenderActivity.this).getonlineCustomerList(groupId, userid, "");
        if (customerList.size() > 0 && customerList.size() < 80) {
            branches.setVisibility(View.GONE);
            branchNamesWithColorsArrayList = new ArrayList<>();
            for (int i = 0; i < stringArrayList.size(); i++) {
                DataModel dataModel = new DataModel();
                dataModel.setColorName(stringArrayList.get(i));
                branchNamesWithColorsArrayList.add(dataModel);
            }
            for (int i = 0; i < customerList.size(); i++) {
                branchNamesWithColorsArrayList.get(i).setBranchName(customerList.get(i).getCustomername());
            }
            colorsDataModelArrayList = new ArrayList<>();
            for (int i = 0; i < branchNamesWithColorsArrayList.size(); i++) {
                if (branchNamesWithColorsArrayList.get(i).getBranchName() != null) {
                    colorsDataModelArrayList.add(branchNamesWithColorsArrayList.get(i));
                }
            }
            if (colorsDataModelArrayList != null && colorsDataModelArrayList.size() > 0) {
                tagArrayList = new ArrayList<>();
                for (int i = 0; i < colorsDataModelArrayList.size(); i++) {
                    final TextView textView = new TextView(this);
                    //   final ToggleButton textView = new ToggleButton(this);
                    textView.setText(colorsDataModelArrayList.get(i).getBranchName());
                    textView.setBackground(getResources().getDrawable(R.drawable.round_corner_button));
                    textView.setTextColor(Color.parseColor(colorsDataModelArrayList.get(i).getColorName()));
                    compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.cal_selected_color));
                    GradientDrawable titleDrawable = (GradientDrawable) textView.getBackground();
                    titleDrawable.setStroke(5, Color.parseColor(colorsDataModelArrayList.get(i).getColorName()));
                    titleDrawable.setColor(getResources().getColor(R.color.white));

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                    );
                    params.setMargins(5, 5, 5, 5);
                    textView.setLayoutParams(params);
                    *//**new charan***//*
                    //textView.setTag(colorsDataModelArrayList.get(i).getBranchName());

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        viewid = View.generateViewId();
                    }
                    // tagArrayList.add(colorsDataModelArrayList.get(i).getBranchName());
                    textView.setId(i);
                    tagArrayList.add(i);
                    *//***End charan**//*
                    Utils.setTypefaces(IvokoApplication.robotoTypeface, textView);
                    branches.addView(textView);
                    final int finalI = i;
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            for (int i = 0; i < tagArrayList.size(); i++) {
                                if (tagArrayList.get(i) == v.getId()) {
                                    compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.white));
                                    textView.setTextColor(getResources().getColor(R.color.white));
                                    GradientDrawable titleDrawable = (GradientDrawable) textView.getBackground();
                                    titleDrawable.setColor(Color.parseColor(colorsDataModelArrayList.get(i).getColorName()));
                                } else {
//                                    compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.cal_selected_color));
                                    TextView otherTextview = (TextView) branches.findViewById(tagArrayList.get(i));
                                    otherTextview.setTextColor(Color.parseColor(colorsDataModelArrayList.get(i).getColorName()));
                                    GradientDrawable titleDrawable = (GradientDrawable) otherTextview.getBackground();
                                    titleDrawable.setStroke(5, Color.parseColor(colorsDataModelArrayList.get(i).getColorName()));
                                    titleDrawable.setColor(getResources().getColor(R.color.white));
                                }
                            }
                            showActivityList(textView.getText().toString());
                        }
                    });
                }
            } else {
                branches.setVisibility(View.GONE);
            }
        } else {
            branches.setVisibility(View.GONE);
        }
        ArrayList<PlannerModel> finalPlannerModelList = new ArrayList();
        compactCalendarView.removeAllEvents();
        plannerModelList = PlannerMgr.getInstance(PSFCalenderActivity.this).getPlannerList(userid);
        for (int i = 0; i < plannerModelList.size(); i++) {
            finalPlannerModelList.add(plannerModelList.get(i));
            *//*if (plannerModelList.get(i).getPlanstatus() != null) {
                if (plannerModelList.get(i).getPlanstatus() == 1) {
                    finalPlannerModelList.add(plannerModelList.get(i));
                }
            }*//*
        }
        Calendar calendar = Calendar.getInstance();
        if (finalPlannerModelList.size() != 0) {
            ArrayList<PlannerModel> plannerModelArrayList = new ArrayList<>();
            for (int i = 0; i < finalPlannerModelList.size(); i++) {
                if (Utils.isNotNullAndNotEmpty(colorsDataModelArrayList))
                    for (int j = 0; j < colorsDataModelArrayList.size(); j++) {
                        if (Utils.isNotNullAndNotEmpty(finalPlannerModelList.get(i).getCustomername())) {
                            if (finalPlannerModelList.get(i).getCustomername().equalsIgnoreCase(colorsDataModelArrayList.get(j).getBranchName())) {
                                finalPlannerModelList.get(i).setColor(colorsDataModelArrayList.get(j).getColorName());
                                plannerModelArrayList.add(finalPlannerModelList.get(i));
                            }


                        }
                    }
            }
            if (plannerModelArrayList.size() != 0) {
                for (int i = 0; i < plannerModelArrayList.size(); i++) {
                    calendar.set(plannerModelArrayList.get(i).getScheduleyear(), plannerModelArrayList.get(i).getSchedulemonth() - 1, plannerModelArrayList.get(i).getScheduleday());
                    Event event = new Event(Color.parseColor(plannerModelArrayList.get(i).getColor()), calendar.getTimeInMillis());
                    compactCalendarView.addEvent(event);
                }
            }
        }
    }*/

 /*   private void logEventsByMonth(CompactCalendarView compactCalendarView) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        currentCalender.set(Calendar.MONTH, Calendar.AUGUST);
        List<String> dates = new ArrayList<>();
        for (Event e : compactCalendarView.getEventsForMonth(new Date())) {
            dates.add(dateFormatForDisplaying.format(e.getTimeInMillis()));
        }
    }*/

   /* public void showActivityList(String branchname) {
        plannerModelList.clear();
        planRecyclerView.setAdapter(null);
        ArrayList<PlannerModel> plannerModelArrayList = new ArrayList<>();
        SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MM-yyyy", Locale.getDefault());
        String[] date = dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()).split("-");
        plannerModelList = PlannerMgr.getInstance(PSFCalenderActivity.this).getPlannerListByBranches(userid,
                branchname, Integer.parseInt(date[0]), Integer.parseInt(date[1]));
        for (int i = 0; i < plannerModelList.size(); i++) {
            plannerModelArrayList.add(plannerModelList.get(i));
        }
        if (plannerModelArrayList.size() > 0) {
            nomessageview.setVisibility(View.INVISIBLE);
            if (msgListProgressBar.VISIBLE == 0) {
                msgListProgressBar.setVisibility(View.INVISIBLE);
            }
            branchesPlannerAdapter = new BranchesPlannerAdapter(PSFCalenderActivity.this, plannerModelArrayList);
            planRecyclerView.setAdapter(branchesPlannerAdapter);
            planRecyclerView.setLayoutManager(new LinearLayoutManager(PSFCalenderActivity.this));
        } else {
            nomessageview.setText("No Plans");
            nomessageview.setVisibility(View.GONE);
        }
    }*/





    public void showlist(int day, int monthno, int year) {
        //plannerModelList.clear();
        //planRecyclerView.setAdapter(null);






        ArrayList<PSFLeadModel> plannerModelArrayList = new ArrayList<>();
        leadsModelList = LeadMgr.getInstance(PSFCalenderActivity.this).getPSFLeadListByDate(userid, day, monthno, year);
        for (int i = 0; i < leadsModelList.size(); i++) {
            plannerModelArrayList.add(leadsModelList.get(i));
        }
        if (plannerModelArrayList.size() > 0) {
            planRecyclerView.setVisibility(View.VISIBLE);
        nomessageview.setVisibility(View.INVISIBLE);
        if (msgListProgressBar.VISIBLE == 0) {
            msgListProgressBar.setVisibility(View.INVISIBLE);
        }

        PSFLeadModel pm = new PSFLeadModel();
        //pm.setActivitytype("abc");

        //plannerModelArrayList.add(pm);
        //leadsAdapter = new PSFLeadsAdapter(leadModels, PSFLeadListActivity.this, type, stage);
            List<String> dummylist = new ArrayList<>();
        calenderPlannerAdapter = new PSFLeadsAdapter(leadModels,PSFCalenderActivity.this, "","",dummylist,"","","","");
        planRecyclerView.setAdapter(calenderPlannerAdapter);
        planRecyclerView.setLayoutManager(new LinearLayoutManager(PSFCalenderActivity.this));
        } else {
           nomessageview.setText("No Plans");
           nomessageview.setVisibility(View.VISIBLE);
            planRecyclerView.setVisibility(View.GONE);
        }
    }





}
