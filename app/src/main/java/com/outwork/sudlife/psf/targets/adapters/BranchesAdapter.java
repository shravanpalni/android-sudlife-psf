package com.outwork.sudlife.psf.targets.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.targets.models.MSBModel;
import com.outwork.sudlife.psf.targets.ui.BranchesViewDetailsActivity;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panch on 2/23/2017.
 */

public class BranchesAdapter extends RecyclerView.Adapter<BranchesAdapter.TargetViewHolder> {
    private Context context;
    private final LayoutInflater mInflater;
    private List<MSBModel> msbModelArrayList = new ArrayList<>();

    public static class TargetViewHolder extends RecyclerView.ViewHolder {

        public CardView cardView;
        public ImageView imgedit;
        public TextView tvBranchName;
        TextView etFtm, etActivityPlanned, etVisitsPlanned,
                etExpectedConnections, etLeadsExpected, etExpectedBusiness, etGapToTraget;

        public TargetViewHolder(View v) {
            super(v);
            tvBranchName = (TextView) v.findViewById(R.id.tv_branchname);
            imgedit = (ImageView) v.findViewById(R.id.img_edit);
            cardView = (CardView) v.findViewById(R.id.card_view);
            etFtm = (TextView) v.findViewById(R.id.tv_ftm);
            etActivityPlanned = (TextView) v.findViewById(R.id.tv_activities_planed);
            etVisitsPlanned = (TextView) v.findViewById(R.id.tv_visits_planned);
            etExpectedConnections = (TextView) v.findViewById(R.id.tv_expected_connections);
            etLeadsExpected = (TextView) v.findViewById(R.id.tv_leads_expected);
            etExpectedBusiness = (TextView) v.findViewById(R.id.tv_expected_business);
           /* etGapToTraget = (TextView) v.findViewById(R.id.tv_gap_target);*/
            Utils.setTypefaces(IvokoApplication.robotoTypeface, etFtm, etActivityPlanned, etVisitsPlanned,
                    etExpectedConnections, etLeadsExpected, etExpectedBusiness,
                    (TextView) v.findViewById(R.id.tv_ftmlbl), (TextView) v.findViewById(R.id.tv_activities_planedlbl),
                    (TextView) v.findViewById(R.id.tv_visits_plannedlbl), (TextView) v.findViewById(R.id.tv_expected_connectionslbl),
                    (TextView) v.findViewById(R.id.tv_leads_expectedlbl), (TextView) v.findViewById(R.id.tv_expected_businesslbl));
            Utils.setTypefaces(IvokoApplication.robotoCondensedBoldTypeface, tvBranchName);
        }
    }

    public BranchesAdapter(Context context, List<MSBModel> msbModelArrayList) {
        this.context = context;
        this.msbModelArrayList = msbModelArrayList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TargetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_of_branch, parent, false);
        return new TargetViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TargetViewHolder holder, final int position) {

        if (msbModelArrayList.get(position).getCustomername().equalsIgnoreCase("summary")) {
            holder.imgedit.setVisibility(View.GONE);
        }
        holder.tvBranchName.setText(msbModelArrayList.get(position).getCustomername());
        holder.etActivityPlanned.setText(msbModelArrayList.get(position).getActivitiesplanned());
        holder.etVisitsPlanned.setText(msbModelArrayList.get(position).getVisitsplanned());
        holder.etExpectedConnections.setText(msbModelArrayList.get(position).getExpectedconncetions());
        holder.etLeadsExpected.setText(msbModelArrayList.get(position).getExpectedleads());
        holder.etExpectedBusiness.setText(msbModelArrayList.get(position).getExpectedbusiness());
        //holder.etGapToTraget.setText(String.valueOf(Integer.parseInt(msbModelArrayList.get(position).getFtm()) - Integer.parseInt(msbModelArrayList.get(position).getExpectedbusiness())));
        holder.etFtm.setText(msbModelArrayList.get(position).getFtm());
        holder.imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MSBModel msbModel = (MSBModel) msbModelArrayList.get(position);
                Intent intent = new Intent(context, BranchesViewDetailsActivity.class);
                intent.putExtra("msbObj", new Gson().toJson(msbModel));
                context.startActivity(intent);
            }
        });
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return msbModelArrayList.size();
    }


}
