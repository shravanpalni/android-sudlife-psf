package com.outwork.sudlife.psf.lead.model;


import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by shravanch on 02-01-2020.
 */


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "leadstage",
        "leadcount"
})

public class PSFPerformanceModel {
    @JsonProperty("leadstage")
    private String leadstage;
    @JsonProperty("leadcount")
    private String leadcount;


    @JsonProperty("leadstage")
    public String getLeadstage() {
        return leadstage;
    }

    @JsonProperty("leadstage")
    public void setLeadstage(String leadstage) {
        this.leadstage = leadstage;
    }

    @JsonProperty("leadcount")
    public String getLeadcount() {
        return leadcount;
    }

    @JsonProperty("leadcount")
    public void setLeadcount(String leadcount) {
        this.leadcount = leadcount;
    }



}
