package com.outwork.sudlife.psf.lead.model;

/**
 * Created by bvlbh on 4/15/2018.
 */
public class OtherBranchModel {
    private String branchname;
    private String bankname;

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    private String branchcode;
}
