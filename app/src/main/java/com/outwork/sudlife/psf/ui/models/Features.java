package com.outwork.sudlife.psf.ui.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Habi on 04-04-2018.
 */
public class Features {

    @SerializedName("featured")
    @Expose
    private Boolean featured;
    @SerializedName("ishealthcare")
    @Expose
    private Boolean ishealthcare;
    @SerializedName("isproductavailable")
    @Expose
    private Boolean isproductavailable;
    @SerializedName("isorderproductavailable")
    @Expose
    private Boolean isorderproductavailable;
    @SerializedName("istaskavailable")
    @Expose
    private Boolean istaskavailable;

    public Boolean getFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public Boolean getIshealthcare() {
        return ishealthcare;
    }

    public void setIshealthcare(Boolean ishealthcare) {
        this.ishealthcare = ishealthcare;
    }

    public Boolean getIsproductavailable() {
        return isproductavailable;
    }

    public void setIsproductavailable(Boolean isproductavailable) {
        this.isproductavailable = isproductavailable;
    }

    public Boolean getIsorderproductavailable() {
        return isorderproductavailable;
    }

    public void setIsorderproductavailable(Boolean isorderproductavailable) {
        this.isorderproductavailable = isorderproductavailable;
    }

    public Boolean getIstaskavailable() {
        return istaskavailable;
    }

    public void setIstaskavailable(Boolean istaskavailable) {
        this.istaskavailable = istaskavailable;
    }
}
