package com.outwork.sudlife.psf.lead.activities;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.adapters.LeadAppointmentAdapter;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.activities.DashBoardActivity;
import com.outwork.sudlife.psf.ui.activities.NotificationsListActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shravanch on 19-12-2019.
 */

public class PSFViewLeadActivity extends BaseActivity {
    private PSFLeadModel leadModel = new PSFLeadModel();
    private TabLayout tabLayout;
    private String teamlead;
    private RecyclerView planRecyclerView;
    private String scheduledate;
    private LeadAppointmentAdapter leadAppointmentAdapter;
    private PlannerModel plannerModel = new PlannerModel();
    private List<PlannerModel> plannerModelList = new ArrayList<>();
    private View infoView, contactView;
    private Button schedileview;
    TextView warningmessage, leadcode, tv_source_of_psflead, /*tv_fsalutation,*/
            tv_fname, tvlName, tv_fdob, tv_gender, tv_education, tvOccupation, tvIncome, tvContactNumber, tv_landlineno,
            tvEmailid, tvAltContactno, tv_psfzone,
            tv_sudbranchoffice, etExistingCustomer, tv_policyno, etProductName, tv_policy_status, tv_bank_name, tv_sum_assured, tv_frequency, tv_total_received_premium, tv_ppt, tv_paid_to_date, tv_maturity_date, tv_lapsed_date, tv_maturity_value, tv_ppt_lastdate, tv_pt, tv_fund_value, tv_instalment_premium, tv_risk_comm_date, tv_reference_type, tv_reference_mobileno, tv_reference, jointcall,
            tv_jointcall, tv_allocatedcustomerleadid, tv_datetime, tv_meeting_address, tv_remarks, tv_lead_stage, toolbar_title, noMembers, tv_leadcreateddate, tv_notes, tv_proposalnumber, tv_actualpremiumpaid, tv_actual_product, tv_conversionpropensity, tv_product, tv_expectedactualpremium, tv_appointmentdate, tv_nominee_name, tv_nominee_dob, tv_la_name, tv_la_dob, tv_nominee_contactno, maturitydate_psflead, tv_maturity_date_psflead;

    private ImageButton call, sms;
    protected static String[] PERMISSIONS_CALL = {
            Manifest.permission.CALL_PHONE
    };
    public static final int REQUEST_PERMISSION_CALL = 131;

    /*TextView tvfName, tv_campaigndate, tvlName, etLeadStatus, isnri, tv_nri, tvContactNumber, tvEmailid, tvAltContactno, tvAge, etExistingCustomer, etPremiumPaid, etProductName, etProductNameOther, tvaddress,
            etCity, etPincode, tvOccupation, etSourceOfLead, etOther, etBank, etBranchCode, familymembers, etBranchName, etZccSupportRequried, etPreferredLang,
            etPreferredDate, tvNotes, etStatus, tvEducation, etSubStatus, tvIncome, etPremiumExpected, etNextFollowupDate, etFirstAppointmentDate, etConversionPropensity,
            etProposalNumber, etLeadCreatedDate, tvMarriedStatus, tvProduct1, tvProduct2, noMembers, leadcode, uname, bankname, branchname, toolbar_title, tvactpremium, tv_countrycode, tv_countryname, countrycode, countryname;
    private BroadcastReceiver mBroadcastReceiver;*/
    private LocalBroadcastManager mgr;
    private BroadcastReceiver mBroadcastReceiver;
    TextView textCartItemCount;
    int mCartItemCount = 0;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {

        }

        @Override
        public void onDateTimeCancel() {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_view_psfleadform);

        mgr = LocalBroadcastManager.getInstance(this);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("leadobj")) {
            leadModel = new Gson().fromJson(getIntent().getStringExtra("leadobj"), PSFLeadModel.class);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("teamlead")) {
            teamlead = getIntent().getStringExtra("teamlead");
        }

        initToolBar();
        initializeViews();
        populateData(leadModel);
        if (Utils.isNotNullAndNotEmpty(teamlead)) {
            if (teamlead.equalsIgnoreCase("team")) {
                schedileview.setVisibility(View.GONE);
            }
        }
        setListener();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("plan_broadcast"));


        if (isNetworkAvailable()) {
            updateNotification();
        } else {
            List<PSFNotificationsModel> finalList = new ArrayList<>();
            List<PSFNotificationsModel> psfNotificationsModelList = LeadMgr.getInstance(PSFViewLeadActivity.this).getPSFNotificationsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));

            if (psfNotificationsModelList.size() > 0) {

                for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                    if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                        finalList.add(notificationsModel);
                    }

                }

                mCartItemCount = finalList.size();
                setupBadge();
            } else {
                mCartItemCount = 0;
                setupBadge();
            }

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    private void setListener() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().toString().equalsIgnoreCase("Lead Details")) {
                    contactView.setVisibility(View.GONE);
                    infoView.setVisibility(View.VISIBLE);
                } else {
                    infoView.setVisibility(View.GONE);
                    contactView.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

            }
        };

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (leadModel != null) {
            if (Utils.isNotNullAndNotEmpty(teamlead)) {
                if (teamlead.equalsIgnoreCase("team")) {
                    menu.findItem(R.id.edit).setVisible(false);
                }
            } else {
                //if (Utils.isNotNullAndNotEmpty(leadModel.getLeadStage()))
                //if (leadModel.getLeadStage().equalsIgnoreCase("Not interested") || leadModel.getLeadStage().equalsIgnoreCase("Converted"))
                //menu.findItem(R.id.edit).setVisible(false);
            }
        } else {
            if (Utils.isNotNullAndNotEmpty(teamlead)) {
                if (teamlead.equalsIgnoreCase("team")) {
                    menu.findItem(R.id.edit).setVisible(false);
                }
            } else {
                menu.findItem(R.id.edit).setVisible(true);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.edit) {

            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                if (isNetworkAvailable()) {

                    Log.i("PSFViewLeadActivity", "AssignedToId = = = =" + leadModel.getAssignedToId());

                    //if (leadModel.getAssignedToId().equalsIgnoreCase(leadModel.getBmUserid())) {

                    if ((leadModel.getLeadStage().equalsIgnoreCase("Converted") || leadModel.getLeadStage().equalsIgnoreCase("Not Interested") || leadModel.getLeadStage().equalsIgnoreCase("Unallocated"))) {

                        showToast("Cannot edit the lead for this stage.");
                    } else {

                        if (leadModel.getAssignedToId() != null & !leadModel.getAssignedToId().equalsIgnoreCase("")){
                            if (leadModel.getBmUserid() != null & !leadModel.getBmUserid().equalsIgnoreCase("")){

                                if (leadModel.getAssignedToId().equalsIgnoreCase(leadModel.getBmUserid())) {
                                    Intent intent = new Intent(PSFViewLeadActivity.this, PSFUpdateLeadActivity.class);
                                    intent.putExtra("leadobj", new Gson().toJson(leadModel));
                                    startActivity(intent);
                                    finish();
                                }else {
                                    showToast("Cannot edit the lead for this stage.");

                                }



                            }else {
                                showToast("Cannot edit the lead for this stage.");

                            }


                        }else {
                            showToast("Cannot edit the lead for this stage.");

                        }







                      /*  if (leadModel.getAssignedToId() != null & !leadModel.getAssignedToId().equalsIgnoreCase("") & leadModel.getBmUserid() != null & !leadModel.getBmUserid().equalsIgnoreCase("")) {
                            if (leadModel.getAssignedToId().equalsIgnoreCase(leadModel.getBmUserid())) {
                                Intent intent = new Intent(PSFViewLeadActivity.this, PSFUpdateLeadActivity.class);
                                intent.putExtra("leadobj", new Gson().toJson(leadModel));
                                startActivity(intent);
                                finish();
                            }else {
                                showToast("Please check your internet connection.");

                            }


                        } else {
                            showToast("Please check your internet connection.");

                        }*/


                    }


                   /* } else {
                        showToast("Cannot edit the lead.");
                    }*/

                } else {
                    showToast("Please check your internet connection.");
                }


                //

            } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {

/*
                if () {

                    showToast("Cannot edit the lead for this stage.");
                } else*/
                if ((leadModel.getLeadStage().equalsIgnoreCase("Converted") && !Utils.isNotNullAndNotEmpty(leadModel.getWarning_message())) || leadModel.getLeadStage().equalsIgnoreCase("Not Interested")) {

                    showToast("Cannot edit the lead for this stage.");
                } else {

                    if ((leadModel.getLeadStage().equalsIgnoreCase("Converted") && Utils.isNotNullAndNotEmpty(leadModel.getWarning_message()) && leadModel.getStatus_code() == 2)) {
                        if (isNetworkAvailable()) {
                            Intent intent = new Intent(PSFViewLeadActivity.this, PSFUpdateLeadActivity.class);
                            intent.putExtra("leadobj", new Gson().toJson(leadModel));
                            startActivity(intent);
                            finish();

                        } else {
                            showToast("Please check your internet connection");
                        }

                    } else {
                        Intent intent = new Intent(PSFViewLeadActivity.this, PSFUpdateLeadActivity.class);
                        intent.putExtra("leadobj", new Gson().toJson(leadModel));
                        startActivity(intent);
                        finish();

                    }


                }


            }


        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeViews() {
        warningmessage = (TextView) findViewById(R.id.warningmessage);
        leadcode = (TextView) findViewById(R.id.leadcode);
        tv_source_of_psflead = (TextView) findViewById(R.id.tv_source_of_psflead);
        //  tv_fsalutation = (TextView) findViewById(R.id.tv_fsalutation);
        tv_fname = (TextView) findViewById(R.id.tv_fname);
        tv_fdob = (TextView) findViewById(R.id.tv_fdob);
        tv_gender = (TextView) findViewById(R.id.tv_gender);
        tv_education = (TextView) findViewById(R.id.tv_education);
        tvOccupation = (TextView) findViewById(R.id.tv_occupation);
        tvIncome = (TextView) findViewById(R.id.tv_income);
        tvContactNumber = (TextView) findViewById(R.id.tv_contact_number);
        call = (ImageButton) findViewById(R.id.call);
        sms = (ImageButton) findViewById(R.id.sms);
        tv_landlineno = (TextView) findViewById(R.id.tv_landlineno);
        tvEmailid = (TextView) findViewById(R.id.tv_email_id);
        tvAltContactno = (TextView) findViewById(R.id.tv_alt_contactno);
        tv_psfzone = (TextView) findViewById(R.id.tv_psfzone);
        tv_sudbranchoffice = (TextView) findViewById(R.id.tv_sudbranchoffice);
        etExistingCustomer = (TextView) findViewById(R.id.tv_existing_sud_ife_customer);
        tv_policyno = (TextView) findViewById(R.id.tv_policyno);
        etProductName = (TextView) findViewById(R.id.tv_product_name);

        tv_policy_status = (TextView) findViewById(R.id.tv_policy_status);
        tv_bank_name = (TextView) findViewById(R.id.tv_bank_name);
        tv_sum_assured = (TextView) findViewById(R.id.tv_sum_assured);
        tv_risk_comm_date = (TextView) findViewById(R.id.tv_risk_comm_date);
        tv_frequency = (TextView) findViewById(R.id.tv_frequency);
        tv_instalment_premium = (TextView) findViewById(R.id.tv_instalment_premium);
        tv_total_received_premium = (TextView) findViewById(R.id.tv_total_received_premium);
        tv_fund_value = (TextView) findViewById(R.id.tv_fund_value);
        tv_ppt = (TextView) findViewById(R.id.tv_ppt);
        tv_pt = (TextView) findViewById(R.id.tv_pt);
        tv_paid_to_date = (TextView) findViewById(R.id.tv_paid_to_date);
        tv_ppt_lastdate = (TextView) findViewById(R.id.tv_ppt_lastdate);
        tv_maturity_date = (TextView) findViewById(R.id.tv_maturity_date);
        tv_maturity_value = (TextView) findViewById(R.id.tv_maturity_value);
        tv_lapsed_date = (TextView) findViewById(R.id.tv_lapsed_date);


        tv_reference = (TextView) findViewById(R.id.tv_reference);
        tv_reference_type = (TextView) findViewById(R.id.tv_reference_type);
        tv_reference_mobileno = (TextView) findViewById(R.id.tv_reference_mobileno);
        tv_allocatedcustomerleadid = (TextView) findViewById(R.id.tv_allocatedcustomerleadid);
        tv_datetime = (TextView) findViewById(R.id.tv_datetime);
        tv_meeting_address = (TextView) findViewById(R.id.tv_meeting_address);
        tv_remarks = (TextView) findViewById(R.id.tv_remarks);
        tv_lead_stage = (TextView) findViewById(R.id.tv_lead_stage);
        jointcall = (TextView) findViewById(R.id.jointcall);
        tv_jointcall = (TextView) findViewById(R.id.tv_jointcall);
        tv_appointmentdate = (TextView) findViewById(R.id.tv_appointmentdate);
        tv_expectedactualpremium = (TextView) findViewById(R.id.tv_expectedactualpremium);
        tv_product = (TextView) findViewById(R.id.tv_product);
        tv_conversionpropensity = (TextView) findViewById(R.id.tv_conversionpropensity);
        tv_actual_product = (TextView) findViewById(R.id.tv_actual_product);
        tv_actualpremiumpaid = (TextView) findViewById(R.id.tv_actualpremiumpaid);
        tv_proposalnumber = (TextView) findViewById(R.id.tv_proposalnumber);
        tv_notes = (TextView) findViewById(R.id.tv_notes);
        tv_leadcreateddate = (TextView) findViewById(R.id.tv_leadcreateddate);
        tv_nominee_name = (TextView) findViewById(R.id.tv_nominee_name);
        tv_nominee_dob = (TextView) findViewById(R.id.tv_nominee_dob);
        tv_la_name = (TextView) findViewById(R.id.tv_la_name);
        tv_la_dob = (TextView) findViewById(R.id.tv_la_dob);
        tv_nominee_contactno = (TextView) findViewById(R.id.tv_nominee_contactno);
        tv_maturity_date_psflead = (TextView) findViewById(R.id.tv_maturity_date_psflead);
        maturitydate_psflead = (TextView) findViewById(R.id.maturitydate_psflead);






/*



        tvMarriedStatus = (TextView) findViewById(R.id.tv_married_status);

        tvProduct1 = (TextView) findViewById(R.id.tv_product_one);
        tvProduct2 = (TextView) findViewById(R.id.tv_product_two);
        tvactpremium = (TextView) findViewById(R.id.tv_actpremium);
        etProductNameOther = (TextView) findViewById(R.id.tv_other_product_name);
        tvaddress = (TextView) findViewById(R.id.tv_address);
        etCity = (TextView) findViewById(R.id.tv_city);
        etPincode = (TextView) findViewById(R.id.tv_pincode);
        etSourceOfLead = (TextView) findViewById(R.id.tv_source_of_lead);
        etOther = (TextView) findViewById(R.id.tv_other);
        etBank = (TextView) findViewById(R.id.tv_bank);
        etBranchCode = (TextView) findViewById(R.id.tv_branch_code);
        familymembers = (TextView) findViewById(R.id.tv_no_of_family_members);
        etBranchName = (TextView) findViewById(R.id.tv_branch_name);
        etZccSupportRequried = (TextView) findViewById(R.id.tv_zcc_support_requried);
        etPreferredLang = (TextView) findViewById(R.id.tv_preferred_language);
        etPreferredDate = (TextView) findViewById(R.id.tv_preferred_Date);
        etStatus = (TextView) findViewById(R.id.tv_metting_status);
        etSubStatus = (TextView) findViewById(R.id.tv_sub_status);
        etPremiumExpected = (TextView) findViewById(R.id.tv_expected_actpremium);
        etFirstAppointmentDate = (TextView) findViewById(R.id.tv_first_appointment_date);
        etNextFollowupDate = (TextView) findViewById(R.id.tv_next_followup_date);
        etConversionPropensity = (TextView) findViewById(R.id.tv_conversion_propensity);
        etProposalNumber = (TextView) findViewById(R.id.tv_proposal_no);
        etLeadCreatedDate = (TextView) findViewById(R.id.tv_lead_created_date);
        tvNotes = (TextView) findViewById(R.id.tv_notes);

        uname = (TextView) findViewById(R.id.uName);
        bankname = (TextView) findViewById(R.id.bankname);
        branchname = (TextView) findViewById(R.id.branchname);
        schedileview = (Button) findViewById(R.id.submit);
        planRecyclerView = (RecyclerView) findViewById(R.id.memberList);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        noMembers = (TextView) findViewById(R.id.noMembers);*/
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Lead Details"));
        tabLayout.addTab(tabLayout.newTab().setText("Appointments"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        infoView = findViewById(R.id.details);
        contactView = findViewById(R.id.contactview);
    }

    public void populateData(PSFLeadModel leadModel) {

        if (Utils.isNotNullAndNotEmpty(leadModel.getWarning_message())) {

            if (leadModel.getStatus_code() == 3) {
                int count = LeadMgr.getInstance(PSFViewLeadActivity.this).getRetryCount(leadModel);
                if (count < 4) {
                    warningmessage.setVisibility(View.VISIBLE);
                    warningmessage.setText("* Warning: " + leadModel.getWarning_message());

                } else if (count >= 4) {
                    warningmessage.setVisibility(View.VISIBLE);
                    warningmessage.setText("* Warning: There was a failure to upload your lead id" + leadModel.getLeadCode() + "to Digi-Quick due to some error. Please contact Outwork Helpdesk 7995591183.");
                }


            } else {
                warningmessage.setVisibility(View.GONE);
            }


        } else {
            warningmessage.setVisibility(View.GONE);
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadCode())) {
            leadcode.setText(leadModel.getLeadCode());
        } else {
            leadcode.setVisibility(View.GONE);
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadSource())) {
            tv_source_of_psflead.setText(leadModel.getLeadSource());
        } else {
            tv_source_of_psflead.setText("Not Available");
        }


      /*  if (Utils.isNotNullAndNotEmpty(leadModel.getLeadSource())) {

            if (leadModel.getLeadSource().equalsIgnoreCase("Maturity")){

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date = null;
                try {
                    date = sdf.parse(leadModel.getMaturityDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                sdf = new SimpleDateFormat("MMM , yyyy");
                String formatedDateString = sdf.format(date);

                maturitydate_psflead.setVisibility(View.VISIBLE);
                tv_maturity_date_psflead.setVisibility(View.VISIBLE);
                tv_maturity_date_psflead.setText(formatedDateString);

            }else {

            }



        }*/


       /* if (Utils.isNotNullAndNotEmpty(leadModel.getPolicyHolderSalutation())) {
            tv_fsalutation.setText(leadModel.getPolicyHolderSalutation());
        } else {
            tv_fsalutation.setText("Not Available");
        }*/

        if (Utils.isNotNullAndNotEmpty(leadModel.getPolicyHolderFirstName())) {
            tv_fname.setText(leadModel.getPolicyHolderFirstName());
        } else {
            tv_fname.setText("Not Available");
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getPolicyHolderDob())) {
            tv_fdob.setText(leadModel.getPolicyHolderDob());
        } else {
            tv_fdob.setText("Not Available");
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getGender())) {
            tv_gender.setText(leadModel.getGender());
        } else {
            tv_gender.setText("Not Available");
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getEducation())) {
            tv_education.setText(leadModel.getEducation());
        } else {
            tv_education.setText("Not Available");
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getOccupation())) {
            tvOccupation.setText(leadModel.getOccupation());
        } else {
            tvOccupation.setText("Not Available");
        }

        if (Utils.isNotNullAndNotEmpty(String.valueOf(leadModel.getAnnualIncome()))) {
            tvIncome.setText(String.valueOf(leadModel.getAnnualIncome()));
        } else {
            tvIncome.setText("Not Available");
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getContactNo())) {
            tvContactNumber.setText(leadModel.getContactNo());
            call.setVisibility(View.VISIBLE);
            sms.setVisibility(View.VISIBLE);
            call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (Utils.isValidPhoneNumber(tvContactNumber.getText().toString())) {
                            setCall(tvContactNumber.getText().toString());
                        } else {
                            showToast("Check the number...");
                        }
                    } catch (android.content.ActivityNotFoundException ex) {
                        showToast("Call failed, please try again later!");
                    }
                }
            });


            sms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (Utils.isNotNullAndNotEmpty(tvContactNumber.getText().toString())) {
                        sendSMS(tvContactNumber.getText().toString().trim());
                    } else {

                        Toast.makeText(PSFViewLeadActivity.this, "Phone number not available.", Toast.LENGTH_SHORT).show();

                    }

                }
            });


        } else {
            call.setVisibility(View.GONE);
            sms.setVisibility(View.GONE);
            tvContactNumber.setText("Not Available");
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getLandLineNo())) {
            tv_landlineno.setText(leadModel.getLandLineNo());
        } else {
            tv_landlineno.setText("Not Available");
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getEmailId())) {
            tvEmailid.setText(leadModel.getEmailId());
        } else {
            tvEmailid.setText("Not Available");
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getAlternateMobileNo())) {
            tvAltContactno.setText(leadModel.getAlternateMobileNo());
        } else {
            tvAltContactno.setText("Not Available");
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getPsfZone())) {
            tv_psfzone.setText(leadModel.getPsfZone());
        } else {
            //tv_psfzone.setText("Not Available");
            tv_psfzone.setVisibility(View.GONE);
            findViewById(R.id.psfzone).setVisibility(View.GONE);


        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getSudBranchOffice())) {
            tv_sudbranchoffice.setText(leadModel.getSudBranchOffice());
        } else {
            //tv_sudbranchoffice.setText("Not Available");
            tv_sudbranchoffice.setVisibility(View.GONE);
            findViewById(R.id.sudbranchoffice).setVisibility(View.GONE);
        }


        if (leadModel.getIsExistingCustomer() != null) {
            if (leadModel.getIsExistingCustomer()) {
                etExistingCustomer.setText("Yes");
                findViewById(R.id.policyno).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_policyno).setVisibility(View.VISIBLE);
                findViewById(R.id.product_name).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_product_name).setVisibility(View.VISIBLE);

                if (leadModel.getLeadSource().equalsIgnoreCase("Reference/Self")) {

                    findViewById(R.id.policy_status).setVisibility(View.GONE);
                    findViewById(R.id.tv_policy_status).setVisibility(View.GONE);
                    findViewById(R.id.bank_name).setVisibility(View.GONE);
                    findViewById(R.id.tv_bank_name).setVisibility(View.GONE);
                    findViewById(R.id.sum_assured).setVisibility(View.GONE);
                    findViewById(R.id.tv_sum_assured).setVisibility(View.GONE);
                    findViewById(R.id.risk_comm_date).setVisibility(View.GONE);
                    findViewById(R.id.tv_risk_comm_date).setVisibility(View.GONE);
                    findViewById(R.id.frequency).setVisibility(View.GONE);
                    findViewById(R.id.tv_frequency).setVisibility(View.GONE);
                    findViewById(R.id.instalment_premium).setVisibility(View.GONE);
                    findViewById(R.id.tv_instalment_premium).setVisibility(View.GONE);
                    findViewById(R.id.total_received_premium).setVisibility(View.GONE);
                    findViewById(R.id.tv_total_received_premium).setVisibility(View.GONE);
                    findViewById(R.id.fund_value).setVisibility(View.GONE);
                    findViewById(R.id.tv_fund_value).setVisibility(View.GONE);
                    findViewById(R.id.ppt).setVisibility(View.GONE);
                    findViewById(R.id.tv_ppt).setVisibility(View.GONE);
                    findViewById(R.id.pt).setVisibility(View.GONE);
                    findViewById(R.id.tv_pt).setVisibility(View.GONE);
                    findViewById(R.id.paid_to_date).setVisibility(View.GONE);
                    findViewById(R.id.tv_paid_to_date).setVisibility(View.GONE);
                    findViewById(R.id.ppt_lastdate).setVisibility(View.GONE);
                    findViewById(R.id.tv_ppt_lastdate).setVisibility(View.GONE);
                    findViewById(R.id.maturity_date).setVisibility(View.GONE);
                    findViewById(R.id.tv_maturity_date).setVisibility(View.GONE);
                    findViewById(R.id.maturity_value).setVisibility(View.GONE);
                    findViewById(R.id.tv_maturity_value).setVisibility(View.GONE);
                    findViewById(R.id.lapsed_date).setVisibility(View.GONE);
                    findViewById(R.id.tv_lapsed_date).setVisibility(View.GONE);


                } else {
                    findViewById(R.id.policy_status).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_policy_status).setVisibility(View.VISIBLE);
                    findViewById(R.id.bank_name).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_bank_name).setVisibility(View.VISIBLE);
                    findViewById(R.id.sum_assured).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_sum_assured).setVisibility(View.VISIBLE);
                    findViewById(R.id.risk_comm_date).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_risk_comm_date).setVisibility(View.VISIBLE);
                    findViewById(R.id.frequency).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_frequency).setVisibility(View.VISIBLE);
                    findViewById(R.id.instalment_premium).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_instalment_premium).setVisibility(View.VISIBLE);
                    findViewById(R.id.total_received_premium).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_total_received_premium).setVisibility(View.VISIBLE);
                    findViewById(R.id.fund_value).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_fund_value).setVisibility(View.VISIBLE);
                    findViewById(R.id.ppt).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_ppt).setVisibility(View.VISIBLE);
                    findViewById(R.id.pt).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_pt).setVisibility(View.VISIBLE);
                    findViewById(R.id.paid_to_date).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_paid_to_date).setVisibility(View.VISIBLE);
                    findViewById(R.id.ppt_lastdate).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_ppt_lastdate).setVisibility(View.VISIBLE);
                    findViewById(R.id.maturity_date).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_maturity_date).setVisibility(View.VISIBLE);
                    findViewById(R.id.maturity_value).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_maturity_value).setVisibility(View.VISIBLE);
                    findViewById(R.id.lapsed_date).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_lapsed_date).setVisibility(View.VISIBLE);

                }


            } else {
                etExistingCustomer.setText("No");
                findViewById(R.id.policyno).setVisibility(View.GONE);
                findViewById(R.id.tv_policyno).setVisibility(View.GONE);
                findViewById(R.id.product_name).setVisibility(View.GONE);
                findViewById(R.id.tv_product_name).setVisibility(View.GONE);
                findViewById(R.id.policy_status).setVisibility(View.GONE);
                findViewById(R.id.tv_policy_status).setVisibility(View.GONE);
                findViewById(R.id.bank_name).setVisibility(View.GONE);
                findViewById(R.id.tv_bank_name).setVisibility(View.GONE);
                findViewById(R.id.sum_assured).setVisibility(View.GONE);
                findViewById(R.id.tv_sum_assured).setVisibility(View.GONE);
                findViewById(R.id.risk_comm_date).setVisibility(View.GONE);
                findViewById(R.id.tv_risk_comm_date).setVisibility(View.GONE);
                findViewById(R.id.frequency).setVisibility(View.GONE);
                findViewById(R.id.tv_frequency).setVisibility(View.GONE);
                findViewById(R.id.instalment_premium).setVisibility(View.GONE);
                findViewById(R.id.tv_instalment_premium).setVisibility(View.GONE);
                findViewById(R.id.total_received_premium).setVisibility(View.GONE);
                findViewById(R.id.tv_total_received_premium).setVisibility(View.GONE);
                findViewById(R.id.fund_value).setVisibility(View.GONE);
                findViewById(R.id.tv_fund_value).setVisibility(View.GONE);
                findViewById(R.id.ppt).setVisibility(View.GONE);
                findViewById(R.id.tv_ppt).setVisibility(View.GONE);
                findViewById(R.id.pt).setVisibility(View.GONE);
                findViewById(R.id.tv_pt).setVisibility(View.GONE);
                findViewById(R.id.paid_to_date).setVisibility(View.GONE);
                findViewById(R.id.tv_paid_to_date).setVisibility(View.GONE);
                findViewById(R.id.ppt_lastdate).setVisibility(View.GONE);
                findViewById(R.id.tv_ppt_lastdate).setVisibility(View.GONE);
                findViewById(R.id.maturity_date).setVisibility(View.GONE);
                findViewById(R.id.tv_maturity_date).setVisibility(View.GONE);
                findViewById(R.id.maturity_value).setVisibility(View.GONE);
                findViewById(R.id.tv_maturity_value).setVisibility(View.GONE);
                findViewById(R.id.lapsed_date).setVisibility(View.GONE);
                findViewById(R.id.tv_lapsed_date).setVisibility(View.GONE);
            }
        } else {
            etExistingCustomer.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getPolicyNo() != null) {
            tv_policyno.setText(String.valueOf(leadModel.getPolicyNo()));
        } else {
            tv_policyno.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProductName())) {
            etProductName.setText(leadModel.getProductName());
        } else {
            etProductName.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getPolicyStatus())) {
            tv_policy_status.setText(leadModel.getPolicyStatus());
        } else {
            tv_policy_status.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getBankName())) {
            tv_bank_name.setText(leadModel.getBankName());
        } else {
            tv_bank_name.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getSumAssured() != null) {
            tv_sum_assured.setText(String.valueOf(leadModel.getSumAssured()));
        } else {
            tv_sum_assured.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getRiskCommDate())) {
            tv_risk_comm_date.setText(leadModel.getRiskCommDate());
        } else {
            tv_risk_comm_date.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getFrequency())) {
            tv_frequency.setText(leadModel.getFrequency());
        } else {
            tv_frequency.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getInstallmentPremium() != null) {
            tv_instalment_premium.setText(String.valueOf(leadModel.getInstallmentPremium()));
        } else {
            tv_instalment_premium.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getTotalReceivedPremium() != null) {
            tv_total_received_premium.setText(String.valueOf(leadModel.getTotalReceivedPremium()));
        } else {
            tv_total_received_premium.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getFundValue() != null) {
            tv_fund_value.setText(String.valueOf(leadModel.getFundValue()));
        } else {
            tv_fund_value.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getPpt() != null) {
            tv_ppt.setText(String.valueOf(leadModel.getPpt()));
        } else {
            tv_ppt.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getPt() != null) {
            tv_pt.setText(String.valueOf(leadModel.getPt()));
        } else {
            tv_pt.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getPaidToDate())) {
            tv_paid_to_date.setText(leadModel.getPaidToDate());
        } else {
            tv_paid_to_date.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getPptLastDate())) {
            tv_ppt_lastdate.setText(leadModel.getPptLastDate());
        } else {
            tv_ppt_lastdate.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getMaturityDate())) {
            tv_maturity_date.setText(leadModel.getMaturityDate());
        } else {
            tv_maturity_date.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getMaturityValue() != null) {
            tv_maturity_value.setText(String.valueOf(leadModel.getMaturityValue()));
        } else {
            tv_maturity_value.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getLapsedDate())) {
            tv_lapsed_date.setText(leadModel.getLapsedDate());
        } else {
            tv_lapsed_date.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getReferenceType())) {
            tv_reference_type.setText(leadModel.getReferenceType());
        } else {
            tv_reference_type.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getReferenceMobileNo())) {
            tv_reference_mobileno.setText(leadModel.getReferenceMobileNo());
        } else {
            tv_reference_mobileno.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getReferenceLeadCode())) {
            tv_allocatedcustomerleadid.setText(leadModel.getReferenceLeadCode());
        } else {
            tv_allocatedcustomerleadid.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getAppointmentDate())) {
            tv_datetime.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getAppointmentDate()), "dd/MM/yyyy hh:mm a"));
        } else {
            tv_datetime.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getMeetingAddress())) {
            tv_meeting_address.setText(leadModel.getMeetingAddress());
        } else {
            tv_meeting_address.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getRemarks())) {
            tv_remarks.setText(leadModel.getRemarks());
        } else {
            tv_remarks.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadStage())) {
            tv_lead_stage.setText(leadModel.getLeadStage());
        } else {
            tv_lead_stage.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getNomineeName())) {
            tv_nominee_name.setText(leadModel.getNomineeName());
        } else {
            tv_nominee_name.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getNomineeDob())) {
            tv_nominee_dob.setText(leadModel.getNomineeDob());
        } else {
            tv_nominee_dob.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getLaName())) {
            tv_la_name.setText(leadModel.getLaName());
        } else {
            tv_la_name.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getLaDob())) {
            tv_la_dob.setText(leadModel.getLaDob());
        } else {
            tv_la_dob.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getNomineeContactNo())) {
            tv_nominee_contactno.setText(leadModel.getNomineeContactNo());
        } else {
            tv_nominee_contactno.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadStage())) {
            if (leadModel.getLeadStage().equalsIgnoreCase("Open")) {


                findViewById(R.id.appointmentdate).setVisibility(View.GONE);
                findViewById(R.id.tv_appointmentdate).setVisibility(View.GONE);

                findViewById(R.id.expectedactualpremium).setVisibility(View.GONE);
                findViewById(R.id.tv_expectedactualpremium).setVisibility(View.GONE);

                findViewById(R.id.product).setVisibility(View.GONE);
                findViewById(R.id.tv_product).setVisibility(View.GONE);

                findViewById(R.id.conversionpropensity).setVisibility(View.GONE);
                findViewById(R.id.tv_conversionpropensity).setVisibility(View.GONE);

                findViewById(R.id.actual_product).setVisibility(View.GONE);
                findViewById(R.id.tv_actual_product).setVisibility(View.GONE);

                findViewById(R.id.actualpremiumpaid).setVisibility(View.GONE);
                findViewById(R.id.tv_actualpremiumpaid).setVisibility(View.GONE);

                findViewById(R.id.proposalnumber).setVisibility(View.GONE);
                findViewById(R.id.tv_proposalnumber).setVisibility(View.GONE);

                findViewById(R.id.notes).setVisibility(View.GONE);
                findViewById(R.id.tv_notes).setVisibility(View.GONE);

                findViewById(R.id.leadcreateddate).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_leadcreateddate).setVisibility(View.VISIBLE);

                findViewById(R.id.jointcall).setVisibility(View.GONE);
                findViewById(R.id.tv_jointcall).setVisibility(View.GONE);


                if (Utils.isNotNullAndNotEmpty(leadModel.getCreatedDate())) {
                    tv_leadcreateddate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getCreatedDate()), "dd/MM/yyyy hh:mm a"));

                }


            } else if (leadModel.getLeadStage().equalsIgnoreCase("Unallocated")) {


                findViewById(R.id.appointmentdate).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_appointmentdate).setVisibility(View.VISIBLE);

                findViewById(R.id.expectedactualpremium).setVisibility(View.GONE);
                findViewById(R.id.tv_expectedactualpremium).setVisibility(View.GONE);

                findViewById(R.id.product).setVisibility(View.GONE);
                findViewById(R.id.tv_product).setVisibility(View.GONE);

                findViewById(R.id.conversionpropensity).setVisibility(View.GONE);
                findViewById(R.id.tv_conversionpropensity).setVisibility(View.GONE);

                findViewById(R.id.actual_product).setVisibility(View.GONE);
                findViewById(R.id.tv_actual_product).setVisibility(View.GONE);

                findViewById(R.id.actualpremiumpaid).setVisibility(View.GONE);
                findViewById(R.id.tv_actualpremiumpaid).setVisibility(View.GONE);

                findViewById(R.id.proposalnumber).setVisibility(View.GONE);
                findViewById(R.id.tv_proposalnumber).setVisibility(View.GONE);

                findViewById(R.id.notes).setVisibility(View.GONE);
                findViewById(R.id.tv_notes).setVisibility(View.GONE);

                findViewById(R.id.leadcreateddate).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_leadcreateddate).setVisibility(View.VISIBLE);

                findViewById(R.id.jointcall).setVisibility(View.GONE);
                findViewById(R.id.tv_jointcall).setVisibility(View.GONE);


                if (Utils.isNotNullAndNotEmpty(leadModel.getAppointmentDate())) {
                    tv_appointmentdate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getAppointmentDate()), "dd/MM/yyyy hh:mm a"));

                }

                if (Utils.isNotNullAndNotEmpty(leadModel.getCreatedDate())) {
                    tv_leadcreateddate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getCreatedDate()), "dd/MM/yyyy hh:mm a"));

                }


            } else if (leadModel.getLeadStage().equalsIgnoreCase("Proposition presented")) {


                findViewById(R.id.appointmentdate).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_appointmentdate).setVisibility(View.VISIBLE);

                findViewById(R.id.expectedactualpremium).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_expectedactualpremium).setVisibility(View.VISIBLE);

                findViewById(R.id.product).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_product).setVisibility(View.VISIBLE);

                findViewById(R.id.conversionpropensity).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_conversionpropensity).setVisibility(View.VISIBLE);

                findViewById(R.id.actual_product).setVisibility(View.GONE);
                findViewById(R.id.tv_actual_product).setVisibility(View.GONE);

                findViewById(R.id.actualpremiumpaid).setVisibility(View.GONE);
                findViewById(R.id.tv_actualpremiumpaid).setVisibility(View.GONE);

                findViewById(R.id.proposalnumber).setVisibility(View.GONE);
                findViewById(R.id.tv_proposalnumber).setVisibility(View.GONE);

                findViewById(R.id.notes).setVisibility(View.GONE);
                findViewById(R.id.tv_notes).setVisibility(View.GONE);

                findViewById(R.id.leadcreateddate).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_leadcreateddate).setVisibility(View.VISIBLE);

                findViewById(R.id.jointcall).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_jointcall).setVisibility(View.VISIBLE);


                if (leadModel.getIsjointVisit() != null) {

                    if (leadModel.getIsjointVisit()) {
                        tv_jointcall.setText("Yes");
                    } else {
                        tv_jointcall.setText("No");
                    }


                }


                if (Utils.isNotNullAndNotEmpty(leadModel.getAppointmentDate())) {
                    tv_appointmentdate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getAppointmentDate()), "dd/MM/yyyy hh:mm a"));

                }
                if ((leadModel.getExpectedPremiumPaid() != null)) {
                    tv_expectedactualpremium.setText(String.valueOf(leadModel.getExpectedPremiumPaid()));
                }
                if (Utils.isNotNullAndNotEmpty(leadModel.getProduct1())) {
                    tv_product.setText(leadModel.getProduct1());
                }
                if (Utils.isNotNullAndNotEmpty(leadModel.getConversionPropensity())) {
                    tv_conversionpropensity.setText(leadModel.getConversionPropensity());
                }
                if (Utils.isNotNullAndNotEmpty(leadModel.getCreatedDate())) {
                    tv_leadcreateddate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getCreatedDate()), "dd/MM/yyyy hh:mm a"));

                }


            } else if (leadModel.getLeadStage().equalsIgnoreCase("Not Interested")) {
                findViewById(R.id.appointmentdate).setVisibility(View.GONE);
                findViewById(R.id.tv_appointmentdate).setVisibility(View.GONE);

                findViewById(R.id.expectedactualpremium).setVisibility(View.GONE);
                findViewById(R.id.tv_expectedactualpremium).setVisibility(View.GONE);

                findViewById(R.id.product).setVisibility(View.GONE);
                findViewById(R.id.tv_product).setVisibility(View.GONE);

                findViewById(R.id.conversionpropensity).setVisibility(View.GONE);
                findViewById(R.id.tv_conversionpropensity).setVisibility(View.GONE);

                findViewById(R.id.actual_product).setVisibility(View.GONE);
                findViewById(R.id.tv_actual_product).setVisibility(View.GONE);

                findViewById(R.id.actualpremiumpaid).setVisibility(View.GONE);
                findViewById(R.id.tv_actualpremiumpaid).setVisibility(View.GONE);

                findViewById(R.id.proposalnumber).setVisibility(View.GONE);
                findViewById(R.id.tv_proposalnumber).setVisibility(View.GONE);

                findViewById(R.id.notes).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_notes).setVisibility(View.VISIBLE);

                findViewById(R.id.leadcreateddate).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_leadcreateddate).setVisibility(View.VISIBLE);

                findViewById(R.id.jointcall).setVisibility(View.GONE);
                findViewById(R.id.tv_jointcall).setVisibility(View.GONE);


                if (Utils.isNotNullAndNotEmpty(leadModel.getCreatedDate())) {
                    tv_leadcreateddate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getCreatedDate()), "dd/MM/yyyy hh:mm a"));

                }

                if (Utils.isNotNullAndNotEmpty(leadModel.getRemarks())) {
                    tv_notes.setText(leadModel.getRemarks());

                }


            } else if (leadModel.getLeadStage().equalsIgnoreCase("Converted")) {
                findViewById(R.id.appointmentdate).setVisibility(View.GONE);
                findViewById(R.id.tv_appointmentdate).setVisibility(View.GONE);

                findViewById(R.id.expectedactualpremium).setVisibility(View.GONE);
                findViewById(R.id.tv_expectedactualpremium).setVisibility(View.GONE);

                findViewById(R.id.product).setVisibility(View.GONE);
                findViewById(R.id.tv_product).setVisibility(View.GONE);

                findViewById(R.id.conversionpropensity).setVisibility(View.GONE);
                findViewById(R.id.tv_conversionpropensity).setVisibility(View.GONE);

                findViewById(R.id.actual_product).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_actual_product).setVisibility(View.VISIBLE);

                findViewById(R.id.actualpremiumpaid).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_actualpremiumpaid).setVisibility(View.VISIBLE);

                findViewById(R.id.proposalnumber).setVisibility(View.GONE);
                findViewById(R.id.tv_proposalnumber).setVisibility(View.GONE);

                findViewById(R.id.notes).setVisibility(View.GONE);
                findViewById(R.id.tv_notes).setVisibility(View.GONE);

                findViewById(R.id.leadcreateddate).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_leadcreateddate).setVisibility(View.VISIBLE);

                findViewById(R.id.jointcall).setVisibility(View.GONE);
                findViewById(R.id.tv_jointcall).setVisibility(View.GONE);


                if ((leadModel.getActualPremiumPaid() != null)) {
                    tv_actualpremiumpaid.setText(String.valueOf(leadModel.getActualPremiumPaid()));
                }

                if (Utils.isNotNullAndNotEmpty(leadModel.getProduct2())) {
                    tv_actual_product.setText(leadModel.getProduct2());
                }
                if (Utils.isNotNullAndNotEmpty(leadModel.getCreatedDate())) {
                    tv_leadcreateddate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getCreatedDate()), "dd/MM/yyyy hh:mm a"));

                }


            } else if (leadModel.getLeadStage().equalsIgnoreCase("Pre-Fixed Appointment")) {
                findViewById(R.id.appointmentdate).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_appointmentdate).setVisibility(View.VISIBLE);

                findViewById(R.id.expectedactualpremium).setVisibility(View.GONE);
                findViewById(R.id.tv_expectedactualpremium).setVisibility(View.GONE);

                findViewById(R.id.product).setVisibility(View.GONE);
                findViewById(R.id.tv_product).setVisibility(View.GONE);

                findViewById(R.id.conversionpropensity).setVisibility(View.GONE);
                findViewById(R.id.tv_conversionpropensity).setVisibility(View.GONE);

                findViewById(R.id.actual_product).setVisibility(View.GONE);
                findViewById(R.id.tv_actual_product).setVisibility(View.GONE);

                findViewById(R.id.actualpremiumpaid).setVisibility(View.GONE);
                findViewById(R.id.tv_actualpremiumpaid).setVisibility(View.GONE);

                findViewById(R.id.proposalnumber).setVisibility(View.GONE);
                findViewById(R.id.tv_proposalnumber).setVisibility(View.GONE);

                findViewById(R.id.notes).setVisibility(View.GONE);
                findViewById(R.id.tv_notes).setVisibility(View.GONE);

                findViewById(R.id.leadcreateddate).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_leadcreateddate).setVisibility(View.VISIBLE);

                findViewById(R.id.jointcall).setVisibility(View.GONE);
                findViewById(R.id.tv_jointcall).setVisibility(View.GONE);


                if (Utils.isNotNullAndNotEmpty(leadModel.getAppointmentDate())) {
                    tv_appointmentdate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getAppointmentDate()), "dd/MM/yyyy hh:mm a"));

                }

                if (Utils.isNotNullAndNotEmpty(leadModel.getCreatedDate())) {
                    tv_leadcreateddate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getCreatedDate()), "dd/MM/yyyy hh:mm a"));

                }


            } else if (leadModel.getLeadStage().equalsIgnoreCase("Follow-Up")) {
                findViewById(R.id.appointmentdate).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_appointmentdate).setVisibility(View.VISIBLE);

                findViewById(R.id.expectedactualpremium).setVisibility(View.GONE);
                findViewById(R.id.tv_expectedactualpremium).setVisibility(View.GONE);

                findViewById(R.id.product).setVisibility(View.GONE);
                findViewById(R.id.tv_product).setVisibility(View.GONE);

                findViewById(R.id.conversionpropensity).setVisibility(View.GONE);
                findViewById(R.id.tv_conversionpropensity).setVisibility(View.GONE);

                findViewById(R.id.actual_product).setVisibility(View.GONE);
                findViewById(R.id.tv_actual_product).setVisibility(View.GONE);

                findViewById(R.id.actualpremiumpaid).setVisibility(View.GONE);
                findViewById(R.id.tv_actualpremiumpaid).setVisibility(View.GONE);

                findViewById(R.id.proposalnumber).setVisibility(View.GONE);
                findViewById(R.id.tv_proposalnumber).setVisibility(View.GONE);

                findViewById(R.id.notes).setVisibility(View.GONE);
                findViewById(R.id.tv_notes).setVisibility(View.GONE);

                findViewById(R.id.leadcreateddate).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_leadcreateddate).setVisibility(View.VISIBLE);

                findViewById(R.id.jointcall).setVisibility(View.GONE);
                findViewById(R.id.tv_jointcall).setVisibility(View.GONE);


                if (Utils.isNotNullAndNotEmpty(leadModel.getAppointmentDate())) {
                    tv_appointmentdate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getAppointmentDate()), "dd/MM/yyyy hh:mm a"));

                }

                if (Utils.isNotNullAndNotEmpty(leadModel.getCreatedDate())) {
                    tv_leadcreateddate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getCreatedDate()), "dd/MM/yyyy hh:mm a"));


                }
            }


        }

    }

  /*  private void postcreateplan() {
        plannerModel.setGroupid(groupId);
        plannerModel.setUserid(userid);
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadid())) {
            plannerModel.setLeadid(leadModel.getLeadid());
            plannerModel.setLocalleadid(leadModel.getId());
        } else {
            plannerModel.setLocalleadid(leadModel.getId());
        }
        if (Utils.isNotNullAndNotEmpty(scheduledate)) {
            plannerModel.setScheduleday(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", TimeUtils.getFormattedDatefromUnix(scheduledate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setSchedulemonth(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", TimeUtils.getFormattedDatefromUnix(scheduledate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setScheduleyear(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", TimeUtils.getFormattedDatefromUnix(scheduledate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setScheduletime(Integer.parseInt(scheduledate));
        }
        plannerModel.setFirstname(leadModel.getFirstname());
        plannerModel.setLastname(leadModel.getLastname());
        plannerModel.setPlanstatus(1);
        plannerModel.setCompletestatus(0);
        plannerModel.setType("Appointment");
        plannerModel.setActivitytype("Appointment");
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadstage()))
            plannerModel.setSubtype(leadModel.getLeadstage());
        plannerModel.setNetwork_status("offline");
        PlannerMgr.getInstance(PSFViewLeadActivity.this).insertPlanner(plannerModel, leadModel.getUserid(), leadModel.getGroupid());
        List<PlannerModel> plannerModelArrayList = new ArrayList<>();
        plannerModelArrayList.add(plannerModel);
        PlannerMgr.getInstance(PSFViewLeadActivity.this).insertPlannerNotificationList(plannerModelArrayList, leadModel.getUserid(), leadModel.getGroupid());
        LeadIntentService.syncLeadstoServer(PSFViewLeadActivity.this);
        finish();
    }*/

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.vftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("View Lead");
        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        textCartItemCount = (TextView) findViewById(R.id.notification_badge);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setTitle("");
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showToast("Notification Clicked!!!");
                Intent in = new Intent(PSFViewLeadActivity.this, NotificationsListActivity.class);
                startActivity(in);
            }
        });

        setupBadge();
    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    public void setCall(String number) {
        int camPermission = ActivityCompat.checkSelfPermission(PSFViewLeadActivity.this, Manifest.permission.CALL_PHONE);
        if (camPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    PSFViewLeadActivity.this,
                    PERMISSIONS_CALL,
                    REQUEST_PERMISSION_CALL);
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_CALL: {
                if (grantResults != null) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (Utils.isNotNullAndNotEmpty(leadModel.getContactNo()))
                            setCall(leadModel.getContactNo().toString());
                    } else {
                        showToast(getResources().getString(R.string.locationtoast));
                    }
                } else {
                    showToast(getResources().getString(R.string.locationtoast));
                }
                return;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == 200 || requestCode == REQUEST_PERMISSION_CALL) {
                    if (data != null && data.getData() != null) {
                    } else { // Otherwise get the image stored by the camera
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(PSFViewLeadActivity.this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void sendSMS(String pno) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
        {

            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this); // Need to change the build to API 19
            Intent intent = new Intent("android.intent.action.VIEW");
            Uri data = Uri.parse("sms:" + pno);
            intent.setData(data);
            //startActivity(intent);
            if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
            // any app that support this intent.
            {
                intent.setPackage(defaultSmsPackageName);
            }
            startActivity(intent);

        } else {
            Intent intent = new Intent("android.intent.action.VIEW");
            Uri data = Uri.parse("sms:" + pno);
            intent.setData(data);
            startActivity(intent);
        }


    }


    private void updateNotification() {
        //showProgressDialog("Refreshing data . . .");
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getProposalCodes = client.getPsfNotifications(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        getProposalCodes.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFNotificationsModel>>() {
                        }.getType();
                        List<PSFNotificationsModel> psfNotificationsModelList = new Gson().fromJson(response.body().getData(), listType);
                        List<PSFNotificationsModel> finalList = new ArrayList<>();
                        if (psfNotificationsModelList.size() > 0) {
                            for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                                if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                                    finalList.add(notificationsModel);
                                }

                            }

                            mCartItemCount = finalList.size();
                            setupBadge();
                            //dismissProgressDialog();

                        } else {
                            mCartItemCount = 0;
                            setupBadge();
                            //dismissProgressDialog();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                //dismissProgressDialog();
            }
        });


    }
}
