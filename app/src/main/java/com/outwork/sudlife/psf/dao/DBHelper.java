package com.outwork.sudlife.psf.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.dao.IvokoProvider.AlarmTable;
import com.outwork.sudlife.psf.dao.IvokoProvider.Category;
import com.outwork.sudlife.psf.dao.IvokoProvider.Contact;
import com.outwork.sudlife.psf.dao.IvokoProvider.Customer;
import com.outwork.sudlife.psf.dao.IvokoProvider.ImagesProvider;
import com.outwork.sudlife.psf.dao.IvokoProvider.LocalProducts;
import com.outwork.sudlife.psf.dao.IvokoProvider.Localforms;
import com.outwork.sudlife.psf.dao.IvokoProvider.StaticList;
import com.outwork.sudlife.psf.dao.IvokoProvider.SubCategory;
import com.outwork.sudlife.psf.dao.IvokoProvider.SyncTable;
import com.outwork.sudlife.psf.dao.IvokoProvider.Transaxion;
import com.outwork.sudlife.psf.dao.IvokoProvider.TxnDay;
import com.outwork.sudlife.psf.dao.IvokoProvider.TxnMonth;

public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper instance;
    private Context context;

    public static synchronized DBHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DBHelper(context);
        }
        return instance;
    }

    //last changes to database done on version 5.
    //No changes to database in 23 and 24
    private DBHelper(Context context) {
        super(context, context.getString(R.string.db_name), null, Integer
                .parseInt(context.getString(R.string.db_version)));
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.Settings.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.Settings.TABLE + "(" + IvokoProvider.Settings._ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.Settings.KEY + "TEXT NOT NULL, " + IvokoProvider.Settings.VALUE + " TEXT)");

        db.execSQL("DROP TABLE IF EXISTS " + SubCategory.TABLE);
        db.execSQL("CREATE TABLE " + SubCategory.TABLE + "(" + SubCategory._ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + SubCategory.SUB_CATEGORY_CODE + " TEXT NOT NULL,"
                + SubCategory.SUB_CATEGORY_NAME + " TEXT NOT NULL,"
                + SubCategory.CATEGORY_CODE + " TEXT NOT NULL, FOREIGN KEY ("
                + SubCategory.CATEGORY_CODE + ") REFERENCES "
                + Category.TABLE + " (" + Category.CATEGORY_CODE
                + ") ON DELETE CASCADE ON UPDATE CASCADE)");

        db.execSQL("DROP TABLE IF EXISTS " + ImagesProvider.TABLE);
        db.execSQL("CREATE TABLE " + ImagesProvider.TABLE + "("
                + ImagesProvider._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + ImagesProvider.REFERENCEID + " TEXT NOT NULL,"
                + ImagesProvider.IMAGEPATH + " TEXT NOT NULL,"
                + ImagesProvider.STATUS + " INTEGER,"
                + ImagesProvider.FILEID + " TEXT NOT NULL,"
                + ImagesProvider.BUCKET_NAME + " TEXT,"
                + ImagesProvider.REFERENCETYPE + " TEXT,"
                + ImagesProvider.CREDENTIALS + " TEXT)");

        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.SyncTable.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.SyncTable.TABLE + "("
                + SyncTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + SyncTable.SYNC_GROUPID + " TEXT NULL, "
                + SyncTable.SYNC_USERID + " TEXT NULL, "
                + SyncTable.SYNC_USERTOKEN + " TEXT NULL, "
                + SyncTable.SYNC_OBJECTID + " TEXT NULL, "
                + SyncTable.SYNC_OBJECTTYPE + " TEXT NULL, "
                + SyncTable.SYNC_OBJECTDATA + " TEXT NULL, "
                + SyncTable.SYNC_CREATEDDATE + " TEXT NULL, "
                + SyncTable.SYNC_SYNCDATE + " TEXT NULL, "
                + SyncTable.SYNC_FORMNAME + " TEXT NULL, "
                + SyncTable.SYNC_STATUS + " INTEGER DEFAULT 0)");

        db.execSQL("DROP TABLE IF EXISTS " + Contact.TABLE);
        db.execSQL("CREATE TABLE " + Contact.TABLE + "("
                + Contact._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + Contact.groupid + " TEXT NULL, "
                + Contact.userid + " TEXT NULL, "
                + Contact.contactid + " TEXT NULL, "
                + Contact.customerid + " TEXT NULL, "
                + Contact.customername + " TEXT NULL, "
                + Contact.firstname + " TEXT NULL, "
                + Contact.salutation + " TEXT NULL, "
                + Contact.practicing + " TEXT NULL, "
                + Contact.gender + " TEXT NULL, "
                + Contact.status + " TEXT NULL, "
                + Contact.lastname + " TEXT NULL, "
                + Contact.displayname + " TEXT NULL, "
                + Contact.designation + " TEXT NULL, "
                + Contact.speciality + " TEXT NULL, "
                + Contact.classification + " TEXT NULL, "
                + Contact.phoneno + " TEXT NULL, "
                + Contact.createddate + " TEXT NULL, "
                + Contact.modifieddate + " TEXT NULL, "
                + Contact.internaltype + " TEXT NULL, "
                + Contact.contacttype + " TEXT NULL, "
                + Contact.cemail + " TEXT NULL, "
                + Contact.address + " TEXT NULL)");

        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.Opportunity.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.Opportunity.TABLE + "("
                + IvokoProvider.Opportunity._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.Opportunity.groupid + " TEXT NULL, "
                + IvokoProvider.Opportunity.userid + " TEXT NULL, "
                + IvokoProvider.Opportunity.firstname + " TEXT NULL, "
                + IvokoProvider.Opportunity.lastname + " TEXT NULL, "
                + IvokoProvider.Opportunity.gender + " TEXT NULL, "
                + IvokoProvider.Opportunity.opportunityid + " TEXT NULL, "
                + IvokoProvider.Opportunity.age + " TEXT NULL, "
                + IvokoProvider.Opportunity.occupation + " TEXT NULL, "
                + IvokoProvider.Opportunity.income + " TEXT NULL, "
                + IvokoProvider.Opportunity.stage + " TEXT NULL, "
                + IvokoProvider.Opportunity.value + " TEXT NULL, "
                + IvokoProvider.Opportunity.items + " TEXT NULL, "
                + IvokoProvider.Opportunity.familymembers + " TEXT NULL, "
                + IvokoProvider.Opportunity.notes + " TEXT NULL, "
                + IvokoProvider.Opportunity.closingdate + " TEXT NULL, "
                + IvokoProvider.Opportunity.network_status + " TEXT NULL)");

        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.Customer.TABLE);
        db.execSQL("CREATE TABLE " + Customer.TABLE + "("
                + Customer._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + Customer.groupid + " TEXT NULL, "
                + Customer.userid + " TEXT NULL, "
                + Customer.customerid + " TEXT NULL, "
                + Customer.customername + " TEXT NULL, "
                + Customer.customercode + " TEXT NULL, "
                + Customer.phoneno + " TEXT NULL, "
                + Customer.sudlat + " TEXT NULL, "
                + Customer.sudlong + " TEXT NULL, "
                + Customer.banklat + " TEXT NULL, "
                + Customer.banklong + " TEXT NULL, "
                + Customer.soname + " TEXT NULL, "
                + Customer.email + " TEXT NULL, "
                + Customer.region + " TEXT NULL, "
                + Customer.zone + " TEXT NULL, "
                + Customer.organizationtype + " TEXT NULL, "
                + Customer.isdelete + " TEXT NULL, "
                + Customer.createddate + " TEXT NULL, "
                + Customer.modifieddate + " TEXT NULL, "
                + Customer.address + " TEXT NULL, "
                + Customer.addressline1 + " TEXT NULL, "
                + Customer.locality + " TEXT NULL, "
                + Customer.branchfoundationday + " TEXT NULL, "
                + Customer.status + " TEXT NULL, "
                + Customer.internaltype + " TEXT NULL)");

        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.TxnMonth.TABLE);
        db.execSQL("CREATE TABLE " + TxnMonth.TABLE + "("
                + TxnMonth._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + TxnMonth.groupid + " TEXT NULL, "
                + TxnMonth.userid + " TEXT NULL, "
                + TxnMonth.monthno + " INT NOT NULL, "
                + TxnMonth.year + " INT NOT NULL, "
                + TxnMonth.monthname + " TEXT NULL, "
                + TxnMonth.createddate + " TEXT NULL, "
                + TxnMonth.modifieddate + " TEXT NULL, "
                + TxnMonth.amount + " DECIMAL NULL, "
                + TxnMonth.currency + " TEXT NULL)");

        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.TxnDay.TABLE);
        db.execSQL("CREATE TABLE " + TxnDay.TABLE + "("
                + TxnDay._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + TxnDay.groupid + " TEXT NULL, "
                + TxnDay.userid + " TEXT NULL, "
                + TxnDay.monthno + " INT NOT NULL, "
                + TxnDay.year + " INT NOT NULL, "
                + TxnDay.dayno + " INT NOT NULL, "
                + TxnDay.dayname + " TEXT NULL, "
                + TxnDay.monthname + " TEXT NULL, "
                + TxnDay.createddate + " TEXT NULL, "
                + TxnDay.modifieddate + " TEXT NULL, "
                + TxnDay.amount + " DECIMAL NULL, "
                + TxnDay.currency + " TEXT NULL)");


        db.execSQL("DROP TABLE IF EXISTS " + Transaxion.TABLE);
        db.execSQL("CREATE TABLE " + Transaxion.TABLE + "("
                + Transaxion._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + Transaxion.groupid + " TEXT NULL, "
                + Transaxion.userid + " TEXT NULL, "
                + Transaxion.monthno + " INT NOT NULL, "
                + Transaxion.year + " INT NOT NULL, "
                + Transaxion.dayno + " INT NOT NULL, "
                + Transaxion.category + " TEXT NULL, "
                + Transaxion.subcategory + " TEXT NULL, "
                + Transaxion.files + " TEXT NULL, "
                + Transaxion.description + " TEXT NULL, "
                + Transaxion.fromlocation + " TEXT NULL, "
                + Transaxion.tolocation + " TEXT NULL, "
                + Transaxion.distance + " TEXT NULL, "
                + Transaxion.createddate + " TEXT NULL, "
                + Transaxion.modifieddate + " TEXT NULL, "
                + Transaxion.amount + " DECIMAL NULL, "
                + Transaxion.txnrefno + " DECIMAL NULL, "
                + Transaxion.currency + " TEXT NULL)");

        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.Forms.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.Forms.TABLE + "("
                + IvokoProvider.Forms._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.Forms.groupid + " TEXT NULL, "
                + IvokoProvider.Forms.userid + " TEXT NULL, "
                + IvokoProvider.Forms.fid + " TEXT NULL, "
                + IvokoProvider.Forms.formid + " TEXT NULL, "
                + IvokoProvider.Forms.orderdate + " TEXT NULL, "
                + IvokoProvider.Forms.value + " TEXT NULL, "
                + IvokoProvider.Forms.totalunits + " TEXT NULL, "
                + IvokoProvider.Forms.formtype + " TEXT NULL, "
                + IvokoProvider.Forms.firstname + " TEXT NULL, "
                + IvokoProvider.Forms.formname + " TEXT NULL, "
                + IvokoProvider.Forms.status + " TEXT NULL, "
                + IvokoProvider.Forms.createddate + " TEXT NULL, "
                + IvokoProvider.Forms.visiteddate + " TEXT NULL, "
                + IvokoProvider.Forms.date + " TEXT NULL, "
                + IvokoProvider.Forms.name + " TEXT NULL, "
                + IvokoProvider.Forms.email + " TEXT NULL, "
                + IvokoProvider.Forms.phone + " TEXT NULL, "
                + IvokoProvider.Forms.visitedby + " TEXT NULL, "
                + IvokoProvider.Forms.customername + " TEXT NULL, "
                + IvokoProvider.Forms.products + " TEXT NULL, "
                + IvokoProvider.Forms.title + " TEXT NULL, "
                + IvokoProvider.Forms.contactname + " TEXT NULL, "
                + IvokoProvider.Forms.notes + " TEXT NULL, "
                + IvokoProvider.Forms.followupdate + " TEXT NULL, "
                + IvokoProvider.Forms.followupaction + " TEXT NULL, "
                + IvokoProvider.Forms.organizationtype + " TEXT NULL, "
                + IvokoProvider.Forms.localcontactid + " TEXT NULL, "
                + IvokoProvider.Forms.contactid + " TEXT NULL, "
                + IvokoProvider.Forms.customerid + " TEXT NULL, "
                + IvokoProvider.Forms.designation + " TEXT NULL, "
                + IvokoProvider.Forms.ctclassification + " TEXT NULL, "
                + IvokoProvider.Forms.speciality + " TEXT NULL, "
                + IvokoProvider.Forms.outcome + " TEXT NULL, "
                + IvokoProvider.Forms.source + " TEXT NULL, "
                + IvokoProvider.Forms.remarks + " TEXT NULL, "
                + IvokoProvider.Forms.address + " TEXT NULL, "
                + IvokoProvider.Forms.orderobject + " TEXT NULL, "
                + IvokoProvider.Forms.localattachment + " TEXT NULL, "
                + IvokoProvider.Forms.attachment + " TEXT NULL, "
                + IvokoProvider.Forms.orderitems + " TEXT NULL)");

        db.execSQL("DROP TABLE IF EXISTS " + Localforms.TABLE);
        db.execSQL("CREATE TABLE " + Localforms.TABLE + "("
                + Localforms._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + Localforms.groupid + " TEXT NULL, "
                + Localforms.formid + " TEXT NULL, "
                + Localforms.status + " INT DEFAULT 0, "
                + Localforms.formtype + " TEXT NULL, "
                + Localforms.formname + " TEXT NULL, "
                + Localforms.description + " TEXT NULL, "
                + Localforms.createddate + " TEXT NULL, "
                + Localforms.modifieddate + " TEXT NULL, "
                + Localforms.createdby + " TEXT NULL)");

        db.execSQL("DROP TABLE IF EXISTS " + LocalProducts.TABLE);
        db.execSQL("CREATE TABLE " + LocalProducts.TABLE + "("
                + LocalProducts._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + LocalProducts.groupid + " TEXT NULL, "
                + LocalProducts.productid + " TEXT NULL, "
                + LocalProducts.productname + " TEXT NULL, "
                + LocalProducts.categoryid + " TEXT NULL, "
                + LocalProducts.categoryname + " TEXT NULL, "
                + LocalProducts.createddate + " TEXT NULL, "
                + LocalProducts.modifieddate + " TEXT NULL, "
                + LocalProducts.createdby + " TEXT NULL)");

        db.execSQL("CREATE TABLE " + StaticList.TABLE + "("
                + StaticList._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + StaticList.listid + " TEXT NULL, "
                + StaticList.groupid + " TEXT NULL, "
                + StaticList.listtype + " TEXT NULL, "
                + StaticList.listcode + " TEXT NULL, "
                + StaticList.name + " TEXT NULL, "
                + StaticList.createddate + " TEXT NULL, "
                + StaticList.modifieddate + " TEXT NULL, "
                + StaticList.createdby + " TEXT NULL)");

        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.AlarmTable.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.AlarmTable.TABLE + "("
                + AlarmTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + AlarmTable.objectid + " TEXT NULL, "
                + AlarmTable.objecttype + " TEXT NULL, "
                + AlarmTable.alarmtime + " TEXT NULL, "
                + AlarmTable.alarmdaytime + " TEXT NULL)");

        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.OpportunityStages.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.OpportunityStages.TABLE + "("
                + IvokoProvider.OpportunityStages._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.OpportunityStages.groupid + " TEXT NULL, "
                + IvokoProvider.OpportunityStages.userid + " TEXT NULL, "
                + IvokoProvider.OpportunityStages.stageid + " TEXT NULL, "
                + IvokoProvider.OpportunityStages.name + " TEXT NULL, "
                + IvokoProvider.OpportunityStages.description + " TEXT NULL, "
                + IvokoProvider.OpportunityStages.defaultorder + " INT DEFAULT 0, "
                + IvokoProvider.OpportunityStages.isdefault + " BOOLEAN DEFAULT FALSE, "
                + IvokoProvider.OpportunityStages.colorcode + " TEXT NULL, "
                + IvokoProvider.OpportunityStages.modifiedon + " TEXT NULL)");
        db.execSQL("CREATE TABLE " + IvokoProvider.MonthlySummeryForBranch.TABLE_NAME + "("
                + IvokoProvider.MonthlySummeryForBranch._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.FTM + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.USER_ID + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.ACTIVITIES_PLANNED + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.MONTH + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.DATE + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.YEAR + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.CUSTOMER_NAME + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.OBJECT_ID + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.TARGET_ID + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.VISITS_PLANNED + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.EXPECTED_CONNECTIONS + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.EXPECTED_LEADS + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.EXPECTED_BUSINESS + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.ACTIVITES_COMPLETED + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.VISITS_COMPLETED + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.ACHIEVED_CONNECTION + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.ACHIEVED_LEADS + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.ACHIEVED_BUSINESS + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.STATUS_OF_SUCCESS_OR_FAIL + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.NETWORK_STATUS + " TEXT NULL, "
                + IvokoProvider.MonthlySummeryForBranch.OFFLINE_OR_ONLINE_STATUS + " TEXT NULL)");

        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.Planner.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.Planner.TABLE + "("
                + IvokoProvider.Planner._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.Planner.groupid + " TEXT NULL, "
                + IvokoProvider.Planner.userid + " TEXT NULL, "
                + IvokoProvider.Planner.id + " TEXT NULL, "
                + IvokoProvider.Planner.activityid + " TEXT NULL, "
                + IvokoProvider.Planner.customerid + " TEXT NULL, "
                + IvokoProvider.Planner.customername + " TEXT NULL, "
                + IvokoProvider.Planner.customertype + " TEXT NULL, "
                + IvokoProvider.Planner.branchcode + " TEXT NULL, "
                + IvokoProvider.Planner.opportunityid + " TEXT NULL, "
                + IvokoProvider.Planner.localcontactid + " TEXT NULL, "
                + IvokoProvider.Planner.localcustomerid + " TEXT NULL, "
                + IvokoProvider.Planner.localopportunityid + " TEXT NULL, "
                + IvokoProvider.Planner.contactid + " TEXT NULL, "
                + IvokoProvider.Planner.activitytype + " TEXT NULL, "
                + IvokoProvider.Planner.subtype + " TEXT NULL, "
                + IvokoProvider.Planner.type + " TEXT NULL, "
                + IvokoProvider.Planner.purpose + " TEXT NULL, "
                + IvokoProvider.Planner.otheractions + " TEXT NULL, "
                + IvokoProvider.Planner.otherfollowupactions + " TEXT NULL, "
                + IvokoProvider.Planner.planstatus + " TEXT NULL, "
                + IvokoProvider.Planner.createddate + " TEXT NULL, "
                + IvokoProvider.Planner.completestatus + " TEXT NULL, "
                + IvokoProvider.Planner.scheduletime + " TEXT NULL, "
                + IvokoProvider.Planner.checkintime + " TEXT NULL, "
                + IvokoProvider.Planner.checkinlocation + " TEXT NULL, "
                + IvokoProvider.Planner.checkinlat + " TEXT NULL, "
                + IvokoProvider.Planner.checkinlon + " TEXT NULL, "
                + IvokoProvider.Planner.checkouttime + " TEXT NULL, "
                + IvokoProvider.Planner.checkoutlocation + " TEXT NULL, "
                + IvokoProvider.Planner.checkoutlat + " TEXT NULL, "
                + IvokoProvider.Planner.checkoutlon + " TEXT NULL, "
                + IvokoProvider.Planner.devicetimestamp + " TEXT NULL, "
                + IvokoProvider.Planner.notes + " TEXT NULL, "
                + IvokoProvider.Planner.followupdate + " TEXT NULL, "
                + IvokoProvider.Planner.leadid + " TEXT NULL, "
                + IvokoProvider.Planner.localleadid + " TEXT NULL, "
                + IvokoProvider.Planner.firstname + " TEXT NULL, "
                + IvokoProvider.Planner.lastname + " TEXT NULL, "
                + IvokoProvider.Planner.followupaction + " TEXT NULL, "
                + IvokoProvider.Planner.scheduleday + " TEXT NULL, "
                + IvokoProvider.Planner.schedulemonth + " TEXT NULL, "
                + IvokoProvider.Planner.scheduleyear + " TEXT NULL, "
                + IvokoProvider.Planner.outcome + " TEXT NULL, "
                + IvokoProvider.Planner.productspromoted + " TEXT NULL, "
                + IvokoProvider.Planner.modifieddate + " TEXT NULL, "
                + IvokoProvider.Planner.createdby + " TEXT NULL, "
                + IvokoProvider.Planner.modifiedby + " TEXT NULL, "
                + IvokoProvider.Planner.plancreatedby + " TEXT NULL, "
                + IvokoProvider.Planner.title + " TEXT NULL, "
                + IvokoProvider.Planner.jointvisit + " TEXT NULL, "
                + IvokoProvider.Planner.customerobject + " TEXT NULL, "
                + IvokoProvider.Planner.supervisorid + " TEXT NULL, "
                + IvokoProvider.Planner.supervisorname + " TEXT NULL, "
                + IvokoProvider.Planner.reschduledate + " TEXT NULL, "
                + IvokoProvider.Planner.leadobject + " TEXT NULL, "
                + IvokoProvider.Planner.resReason + " TEXT NULL, "
                + IvokoProvider.Planner.networkstatus + " TEXT NULL)");


        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.PlannerNotification.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.PlannerNotification.TABLE + "("
                + IvokoProvider.PlannerNotification._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.PlannerNotification.groupid + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.userid + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.id + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.activityid + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.customerid + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.customername + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.opportunityid + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.localcontactid + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.localcustomerid + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.localopportunityid + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.contactid + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.activitytype + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.subtype + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.type + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.purpose + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.planstatus + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.createddate + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.completestatus + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.scheduletime + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.checkintime + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.checkinlocation + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.checkouttime + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.checkoutlocation + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.notes + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.followupdate + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.followupaction + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.scheduleday + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.notificationstatus + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.schedulemonth + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.scheduleyear + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.outcome + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.productspromoted + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.modifieddate + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.createdby + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.modifiedby + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.plancreatedby + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.title + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.jointvisit + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.supervisor + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.reschduledate + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.resReason + " TEXT NULL, "
                + IvokoProvider.PlannerNotification.networkstatus + " TEXT NULL)");


        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.Lead.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.Lead.TABLE + "("
                + IvokoProvider.Lead._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.Lead.groupid + " TEXT NULL, "
                + IvokoProvider.Lead.leadid + " TEXT NULL, "
                + IvokoProvider.Lead.leadcode + " TEXT NULL, "
                + IvokoProvider.Lead.userid + " TEXT NULL, "
                + IvokoProvider.Lead.firstname + " TEXT NULL, "
                + IvokoProvider.Lead.lastname + " TEXT NULL, "
                + IvokoProvider.Lead.isnri + " TEXT NULL, "
                + IvokoProvider.Lead.countryname + " TEXT NULL, "       //country code
                + IvokoProvider.Lead.countrycode + " TEXT NULL, "       //country code
                + IvokoProvider.Lead.contactno + " TEXT NULL, "
                + IvokoProvider.Lead.emailid + " TEXT NULL, "
                + IvokoProvider.Lead.alternative_contactno + " TEXT NULL, "
                + IvokoProvider.Lead.existing_sublife_customer + " TEXT NULL, "
                + IvokoProvider.Lead.premium_paid + " TEXT NULL, "
                + IvokoProvider.Lead.ispremium_paid + " TEXT NULL, "
                + IvokoProvider.Lead.product_name + " TEXT NULL, "
                + IvokoProvider.Lead.product_two + " TEXT NULL, "
                + IvokoProvider.Lead.product_one + " TEXT NULL, "
                + IvokoProvider.Lead.address + " TEXT NULL, "
                + IvokoProvider.Lead.campaigndate + " TEXT NULL, "
                + IvokoProvider.Lead.city + " TEXT NULL, "
                + IvokoProvider.Lead.pincode + " TEXT NULL, "
                + IvokoProvider.Lead.occupation + " TEXT NULL, "
                + IvokoProvider.Lead.source_of_lead + " TEXT NULL, "
                + IvokoProvider.Lead.other + " TEXT NULL, "
                + IvokoProvider.Lead.bank + " TEXT NULL, "
                + IvokoProvider.Lead.branch_code + " TEXT NULL, "
                + IvokoProvider.Lead.no_of_family_members + " TEXT NULL, "
                + IvokoProvider.Lead.branch_name + " TEXT NULL, "
                + IvokoProvider.Lead.zcc_support_requried + " TEXT NULL, "
                + IvokoProvider.Lead.gender + " TEXT NULL, "
                + IvokoProvider.Lead.preferred_language + " TEXT NULL, "
                + IvokoProvider.Lead.preferred_date + " TEXT NULL, "
                + IvokoProvider.Lead.preferred_time + " TEXT NULL, "
                + IvokoProvider.Lead.status + " TEXT NULL, "
                + IvokoProvider.Lead.lead_stage + " TEXT NULL, "
                + IvokoProvider.Lead.sub_status + " TEXT NULL, "
                + IvokoProvider.Lead.premium_expected + " TEXT NULL, "
                + IvokoProvider.Lead.next_followup_date + " TEXT NULL, "
                + IvokoProvider.Lead.first_appointment_date + " TEXT NULL, "
                + IvokoProvider.Lead.conversion_propensity + " TEXT NULL, "
                + IvokoProvider.Lead.visiting_time + " TEXT NULL, "
                + IvokoProvider.Lead.visiting_date + " TEXT NULL, "
                + IvokoProvider.Lead.proposal_number + " TEXT NULL, "
                + IvokoProvider.Lead.lead_created_date + " TEXT NULL, "
                + IvokoProvider.Lead.region + " TEXT NULL, "
                + IvokoProvider.Lead.married + " TEXT NULL, "
                + IvokoProvider.Lead.education + " TEXT NULL, "
                + IvokoProvider.Lead.customer_call_connected + " TEXT NULL, "
                + IvokoProvider.Lead.income_band + " TEXT NULL, "
                + IvokoProvider.Lead.lead_type + " TEXT NULL, "
                + IvokoProvider.Lead.remarks + " TEXT NULL, "
                + IvokoProvider.Lead.relation + " TEXT NULL, "
                + IvokoProvider.Lead.working + " TEXT NULL, "
                + IvokoProvider.Lead.age + " TEXT NULL, "
                + IvokoProvider.Lead.appointement_date + " TEXT NULL, "
                + IvokoProvider.Lead.appointement_time + " TEXT NULL, "
                + IvokoProvider.Lead.financial_planning_done_for + " TEXT NULL, "
                + IvokoProvider.Lead.tentative_investment_years + " TEXT NULL, "
                + IvokoProvider.Lead.financial_planning_in_future_for + " TEXT NULL, "
                + IvokoProvider.Lead.createdby + " TEXT NULL, "
                + IvokoProvider.Lead.createdon + " TEXT NULL, "
                + IvokoProvider.Lead.modifiedon + " TEXT NULL, "
                + IvokoProvider.Lead.modifiedby + " TEXT NULL, "
                + IvokoProvider.Lead.ismarried + " TEXT NULL, "
                + IvokoProvider.Lead.tentative_amount + " TEXT NULL, "
                + IvokoProvider.Lead.expected_actual_premium + " TEXT NULL, "
                + IvokoProvider.Lead.actual_premium + " TEXT NULL, "
                + IvokoProvider.Lead.network_status + " TEXT NULL)");

        //PSFLeads


        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.PSFLead.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.PSFLead.TABLE + "("
                + IvokoProvider.PSFLead._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.PSFLead.groupid + " TEXT NULL, "
                + IvokoProvider.PSFLead.userid + " TEXT NULL, "
                + IvokoProvider.PSFLead.leadid + " TEXT NULL, "
                + IvokoProvider.PSFLead.assignedtoid + " TEXT NULL, "
                + IvokoProvider.PSFLead.branchid + " TEXT NULL, "
                + IvokoProvider.PSFLead.branchname + " TEXT NULL, "
                + IvokoProvider.PSFLead.isdeleted + " TEXT NULL, "
                + IvokoProvider.PSFLead.createdby + " TEXT NULL, "
                + IvokoProvider.PSFLead.modifiedby + " TEXT NULL, "
                + IvokoProvider.PSFLead.leadcode + " TEXT NULL, "
                + IvokoProvider.PSFLead.policyholderid + " TEXT NULL, "
                + IvokoProvider.PSFLead.policyholdersalutation + " TEXT NULL, "
                + IvokoProvider.PSFLead.policyholderfirstname + " TEXT NULL, "
                + IvokoProvider.PSFLead.policyholderlastname + " TEXT NULL, "
                + IvokoProvider.PSFLead.policyholderdob + " TEXT NULL, "
                + IvokoProvider.PSFLead.gender + " TEXT NULL, "
                + IvokoProvider.PSFLead.education + " TEXT NULL, "
                + IvokoProvider.PSFLead.occupation + " TEXT NULL, "
                + IvokoProvider.PSFLead.annualincome + " TEXT NULL, "
                + IvokoProvider.PSFLead.contactno + " TEXT NULL, "
                + IvokoProvider.PSFLead.referencemobileno + " TEXT NULL, "
                + IvokoProvider.PSFLead.landlineno + " TEXT NULL, "
                + IvokoProvider.PSFLead.emailid + " TEXT NULL, "
                + IvokoProvider.PSFLead.alternatemobileno + " TEXT NULL, "
                + IvokoProvider.PSFLead.alternatelandline1 + " TEXT NULL, "
                + IvokoProvider.PSFLead.alternatelandline2 + " TEXT NULL, "
                + IvokoProvider.PSFLead.nomineename + " TEXT NULL, "
                + IvokoProvider.PSFLead.nomineedob + " TEXT NULL, "
                + IvokoProvider.PSFLead.laname + " TEXT NULL, "
                + IvokoProvider.PSFLead.leadtype + " TEXT NULL, "
                + IvokoProvider.PSFLead.ladob + " TEXT NULL, "
                + IvokoProvider.PSFLead.nomineecontactno + " TEXT NULL, "
                + IvokoProvider.PSFLead.alternateemailid + " TEXT NULL, "
                + IvokoProvider.PSFLead.psfzone + " TEXT NULL, "
                + IvokoProvider.PSFLead.psfregion + " TEXT NULL, "
                + IvokoProvider.PSFLead.sudbranchoffice + " TEXT NULL, "
                + IvokoProvider.PSFLead.leadstage + " TEXT NULL, "
                + IvokoProvider.PSFLead.isexistingcustomer + " TEXT NULL, "
                + IvokoProvider.PSFLead.policyno + " TEXT NULL, "
                + IvokoProvider.PSFLead.policystatus + " TEXT NULL, "
                + IvokoProvider.PSFLead.bankname + " TEXT NULL, "
                + IvokoProvider.PSFLead.productname + " TEXT NULL, "
                + IvokoProvider.PSFLead.sumassured + " TEXT NULL, "
                + IvokoProvider.PSFLead.riskcommdate + " TEXT NULL, "
                + IvokoProvider.PSFLead.frequency + " TEXT NULL, "
                + IvokoProvider.PSFLead.installmentpremium + " TEXT NULL, "
                + IvokoProvider.PSFLead.totalreceivedpremium + " TEXT NULL, "
                + IvokoProvider.PSFLead.fundvalue + " TEXT NULL, "
                + IvokoProvider.PSFLead.ppt + " TEXT NULL, "
                + IvokoProvider.PSFLead.pt + " TEXT NULL, "
                + IvokoProvider.PSFLead.paidtodate + " TEXT NULL, "
                + IvokoProvider.PSFLead.pptlastdate + " TEXT NULL, "
                + IvokoProvider.PSFLead.maturitydate + " TEXT NULL, "
                + IvokoProvider.PSFLead.maturityvalue + " TEXT NULL, "
                + IvokoProvider.PSFLead.lapseddate + " TEXT NULL, "
                + IvokoProvider.PSFLead.leadsource + " TEXT NULL, "
                + IvokoProvider.PSFLead.appointmentdate + " TEXT NULL, "
                + IvokoProvider.PSFLead.appointmentday + " TEXT NULL, "
                + IvokoProvider.PSFLead.appointmentmonth + " TEXT NULL, "
                + IvokoProvider.PSFLead.appointmentyear + " TEXT NULL, "
                + IvokoProvider.PSFLead.remarks + " TEXT NULL, "
                + IvokoProvider.PSFLead.meetingaddress + " TEXT NULL, "
                + IvokoProvider.PSFLead.expectedpremiumpaid + " TEXT NULL, "
                + IvokoProvider.PSFLead.actualpremiumpaid + " TEXT NULL, "
                + IvokoProvider.PSFLead.conversionpropensity + " TEXT NULL, "
                + IvokoProvider.PSFLead.proposalnumber + " TEXT NULL, "
                + IvokoProvider.PSFLead.createddate + " TEXT NULL, "
                + IvokoProvider.PSFLead.createdday + " TEXT NULL, "
                + IvokoProvider.PSFLead.createdmonth + " TEXT NULL, "
                + IvokoProvider.PSFLead.createdyear + " TEXT NULL, "
                + IvokoProvider.PSFLead.modifieddate + " TEXT NULL, "
                + IvokoProvider.PSFLead.isjointvisit + " TEXT NULL, "
                + IvokoProvider.PSFLead.supervisorempcode + " TEXT NULL, "
                + IvokoProvider.PSFLead.supervisorname + " TEXT NULL, "
                + IvokoProvider.PSFLead.empcode + " TEXT NULL, "
                + IvokoProvider.PSFLead.emptype + " TEXT NULL, "
                + IvokoProvider.PSFLead.empname + " TEXT NULL, "
                + IvokoProvider.PSFLead.crmleadid + " TEXT NULL, "
                + IvokoProvider.PSFLead.referencetype + " TEXT NULL, "
                + IvokoProvider.PSFLead.referenceleadcode + " TEXT NULL, "
                + IvokoProvider.PSFLead.supervisorid + " TEXT NULL, "
                + IvokoProvider.PSFLead.bmempcode + " TEXT NULL, "
                + IvokoProvider.PSFLead.bmname + " TEXT NULL, "
                + IvokoProvider.PSFLead.bmuserid + " TEXT NULL, "
                + IvokoProvider.PSFLead.cmempcode + " TEXT NULL, "
                + IvokoProvider.PSFLead.cmuserid + " TEXT NULL, "
                + IvokoProvider.PSFLead.cmname + " TEXT NULL, "
                + IvokoProvider.PSFLead.vpempcode + " TEXT NULL, "
                + IvokoProvider.PSFLead.vpname + " TEXT NULL, "
                + IvokoProvider.PSFLead.vpuserid + " TEXT NULL, "
                + IvokoProvider.PSFLead.product1 + " TEXT NULL, "
                + IvokoProvider.PSFLead.product2 + " TEXT NULL, "
                + IvokoProvider.PSFLead.warning_message + " TEXT NULL, "
                + IvokoProvider.PSFLead.status_code + " INTEGER DEFAULT 0,"
                + IvokoProvider.PSFLead.retry_count + " INTEGER DEFAULT 0,"
                + IvokoProvider.PSFLead.network_status + " TEXT NULL)");


        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.Superior.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.Superior.TABLE + "("
                + IvokoProvider.Superior._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.Superior.groupid + " TEXT NULL, "
                + IvokoProvider.Superior.userid + " TEXT NULL, "
                + IvokoProvider.Superior.supervisorid + " TEXT NULL, "
                + IvokoProvider.Superior.supervisorname + " TEXT NULL, "
                + IvokoProvider.Superior.employeecode + " TEXT NULL)");

        db.execSQL("DROP TABLE IF EXISTS " + IvokoProvider.ProposalCodes.TABLE);
        db.execSQL("CREATE TABLE " + IvokoProvider.ProposalCodes.TABLE + "("
                + IvokoProvider.ProposalCodes._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.ProposalCodes.groupid + " TEXT NULL, "
                + IvokoProvider.ProposalCodes.userid + " TEXT NULL, "
                + IvokoProvider.ProposalCodes.bandid + " TEXT NULL, "
                + IvokoProvider.ProposalCodes.bandname + " TEXT NULL, "
                + IvokoProvider.ProposalCodes.minvalue + " TEXT NULL, "
                + IvokoProvider.ProposalCodes.maxvalue + " TEXT NULL)");


        db.execSQL("CREATE TABLE " + IvokoProvider.UserTeam.TABLE + "("
                + IvokoProvider.UserTeam._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.UserTeam.userid + " TEXT NULL, "
                + IvokoProvider.UserTeam.groupid + " TEXT NULL, "
                + IvokoProvider.UserTeam.teamid + " TEXT NULL, "
                + IvokoProvider.UserTeam.type + " TEXT NULL, "
                + IvokoProvider.UserTeam.name + " TEXT NULL, "
                + IvokoProvider.UserTeam.internalrole + " TEXT NULL)");


        db.execSQL("CREATE TABLE " + IvokoProvider.PSFNotifications.TABLE + "("
                + IvokoProvider.PSFNotifications._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + IvokoProvider.PSFNotifications.userid + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.groupid + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.objectid + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.notifyid + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.notifytext + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.modulename + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.type + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.eventdate + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.createddate + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.isread + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.username + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.relationuserid + " TEXT NULL, "
                + IvokoProvider.PSFNotifications.isdeleted + " TEXT NULL)");


    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        switch (oldVersion) {
            case 12:
                break;
            case 11:
                break;
            case 10:

                executeUpgradeto10(db);

                break;


            case 9:
                executeUpgradeto9(db);
                executeUpgradeto10(db);
                break;
            case 8:
            case 7:
            case 6:
            case 5:
            case 4:
            case 3:
            case 2:
            case 1:
                // no modifications in db
                executeUpgradeto6(db);
                executeUpgradeto9(db);
                executeUpgradeto10(db);

                break;

            default:
                break;
        }
    }



    private void executeUpgradeto9(SQLiteDatabase db) {
        final String ALTER_PSFNOTIFICATIONS_TABLE_USER_NAME =
                "ALTER TABLE " + IvokoProvider.PSFNotifications.TABLE + " ADD " + IvokoProvider.PSFNotifications.username + " TEXT NULL";

        if (!isFieldExist(db, IvokoProvider.PSFNotifications.TABLE, IvokoProvider.PSFNotifications.username)) {
            db.execSQL(ALTER_PSFNOTIFICATIONS_TABLE_USER_NAME);
        }

        final String ALTER_PSFNOTIFICATIONS_TABLE_RELATIONAL_USERID =
                "ALTER TABLE " + IvokoProvider.PSFNotifications.TABLE + " ADD " + IvokoProvider.PSFNotifications.relationuserid + " TEXT NULL";

        if (!isFieldExist(db, IvokoProvider.PSFNotifications.TABLE, IvokoProvider.PSFNotifications.relationuserid)) {
            db.execSQL(ALTER_PSFNOTIFICATIONS_TABLE_RELATIONAL_USERID);
        }

    }



    private void executeUpgradeto10(SQLiteDatabase db) {
        final String ALTER_PSFLEAD_TABLE_BRANCHNAME =
                "ALTER TABLE " + IvokoProvider.PSFLead.TABLE + " ADD " + IvokoProvider.PSFLead.branchname + " TEXT NULL";

        if (!isFieldExist(db, IvokoProvider.PSFLead.TABLE, IvokoProvider.PSFLead.branchname)) {
            db.execSQL(ALTER_PSFLEAD_TABLE_BRANCHNAME);
        }

        final String ALTER_PSFLEAD_TABLE_ISDELETED =
                "ALTER TABLE " + IvokoProvider.PSFLead.TABLE + " ADD " + IvokoProvider.PSFLead.isdeleted + " TEXT NULL";

        if (!isFieldExist(db, IvokoProvider.PSFLead.TABLE, IvokoProvider.PSFLead.isdeleted)) {
            db.execSQL(ALTER_PSFLEAD_TABLE_ISDELETED);
        }


        final String ALTER_PSFLEAD_TABLE_CREATEDBY =
                "ALTER TABLE " + IvokoProvider.PSFLead.TABLE + " ADD " + IvokoProvider.PSFLead.createdby + " TEXT NULL";

        if (!isFieldExist(db, IvokoProvider.PSFLead.TABLE, IvokoProvider.PSFLead.createdby)) {
            db.execSQL(ALTER_PSFLEAD_TABLE_CREATEDBY);
        }

        final String ALTER_PSFLEAD_TABLE_MODIFIEDBY =
                "ALTER TABLE " + IvokoProvider.PSFLead.TABLE + " ADD " + IvokoProvider.PSFLead.modifiedby + " TEXT NULL";

        if (!isFieldExist(db, IvokoProvider.PSFLead.TABLE, IvokoProvider.PSFLead.modifiedby)) {
            db.execSQL(ALTER_PSFLEAD_TABLE_MODIFIEDBY);
        }

    }


    private void executeUpgradeto6(SQLiteDatabase db) {
        final String ALTER_PSFLEAD_TABLE_WARNING_MESSAGE =
                "ALTER TABLE " + IvokoProvider.PSFLead.TABLE + " ADD " + IvokoProvider.PSFLead.warning_message + " TEXT NULL";

        if (!isFieldExist(db, IvokoProvider.PSFLead.TABLE, IvokoProvider.PSFLead.warning_message)) {
            db.execSQL(ALTER_PSFLEAD_TABLE_WARNING_MESSAGE);
        }


        final String ALTER_PSFLEAD_TABLE_STATUS_CODE =
                "ALTER TABLE " + IvokoProvider.PSFLead.TABLE + " ADD " + IvokoProvider.PSFLead.status_code + " INTEGER DEFAULT 0 ";

        if (!isFieldExist(db, IvokoProvider.PSFLead.TABLE, IvokoProvider.PSFLead.status_code)) {
            db.execSQL(ALTER_PSFLEAD_TABLE_STATUS_CODE);
        }

        final String ALTER_PSFLEAD_TABLE_RETRY_COUNT =
                "ALTER TABLE " + IvokoProvider.PSFLead.TABLE + " ADD " + IvokoProvider.PSFLead.retry_count + " INTEGER DEFAULT 0 ";

        if (!isFieldExist(db, IvokoProvider.PSFLead.TABLE, IvokoProvider.PSFLead.retry_count)) {
            db.execSQL(ALTER_PSFLEAD_TABLE_RETRY_COUNT);
        }
    }









    public boolean isFieldExist(SQLiteDatabase db, String tableName, String fieldName) {
        boolean isExist = false;

        Cursor res = db.rawQuery("PRAGMA table_info(" + tableName + ")", null);
        if (res.moveToFirst()) {
            do {
                int value = res.getColumnIndex("name");
                if (value != -1 && res.getString(value).equals(fieldName)) {
                    isExist = true;
                }
                // Add book to books
            } while (res.moveToNext());
        }
        if (res != null)
            res.close();
        return isExist;
    }
}
