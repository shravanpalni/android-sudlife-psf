package com.outwork.sudlife.psf.planner.horizontalpicker;

import android.view.View;

public interface OnItemClickedListener {
    void onClickView(View v, int adapterPosition);
}