package com.outwork.sudlife.psf.more;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Utils;

/**
 * Created by Habi on 18-05-2017.
 */

public class ViewVideoActivity extends BaseActivity {

    private Toolbar toolbar;

    private VideoView videoView;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);
        toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        if (Utils.isNotNullAndNotEmpty(getIntent().getStringExtra("Name"))) {
            textView.setText(getIntent().getStringExtra("Name"));
        } else {
            textView.setText("");
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }

        videoView = (VideoView) findViewById(R.id.video);
        videoView.setVisibility(View.VISIBLE);
        // Create a progressbar
        pDialog = new ProgressDialog(ViewVideoActivity.this);
        // Set progressbar title
        pDialog.setTitle("Loading......");
        // Set progressbar message
        pDialog.setMessage("Buffering...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        // Show progressbar
        pDialog.show();

        try {
            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                    ViewVideoActivity.this);
            mediacontroller.setAnchorView(videoView);
            // Get the URL from String VideoURL
            Uri video = Uri.parse(getIntent().getStringExtra("FILEURL"));
            videoView.setMediaController(mediacontroller);
            videoView.setVideoURI(video);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                pDialog.dismiss();
                videoView.start();
            }
        });

    }
}