package com.outwork.sudlife.psf.more.models;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class

CommentDto {
    private String Id;
    private String SubjectId;
    private String Description;
    private String CommentDate;
    private String userId;
    private String userName;
    private String userEmail;
    private boolean isSeperator;
    private String seperatorDate;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(String subjectId) {
        this.SubjectId = subjectId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getCommentDate() {
        return CommentDate;
    }

    public void setCommentDate(String commentDate) {
        this.CommentDate = commentDate;
    }

    public String getSeperatorDate() {
        return seperatorDate;
    }

    public void setSeperatorDate(String seperatorDate) {
        this.seperatorDate = seperatorDate;
    }

    public boolean isSeperator() {
        return isSeperator;
    }

    public void setIsSeperator(boolean isSeperator) {
        this.isSeperator = isSeperator;
    }


    public static final String JSON_ID = "id";
    public static final String JSON_COMMENT = "description";
    public static final String JSON_SUBJECT_ID = "subjectID";
    public static final String JSON_COMMENT_DATE = "createdDate";
    public static final String JSON_USER_ID = "ownerid";
    public static final String JSON_USER_NAME = "userName";
    public static final String JSON_USER_EMAIL = "userEmail";

    public static CommentDto parseServerRequest(JSONObject jsonObject) throws JSONException {

        CommentDto dto = new CommentDto();
        dto.setSubjectId(jsonObject.getString(JSON_SUBJECT_ID));
        dto.setDescription(jsonObject.getString(JSON_COMMENT));
        dto.setCommentDate(jsonObject.getString(JSON_COMMENT_DATE));
        dto.setUserId(jsonObject.getString(JSON_USER_ID));
        dto.setUserName(jsonObject.getString(JSON_USER_NAME));
        dto.setUserEmail(jsonObject.getString(JSON_USER_EMAIL));

        if (TextUtils.isEmpty(dto.getUserName()) || dto.getUserName().equals("null")) {
            dto.setUserName(dto.getUserEmail());
        }

        return dto;

    }

    public static final String JSON_CHAT_POST = "chatPost";

    public JSONObject generateJson(String subjectId, String Description) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(CommentDto.JSON_ID, subjectId);
            jsonObject.put(CommentDto.JSON_COMMENT, Description);
        } catch (Exception e) {
            // e.printStackTrace();
        }


        return jsonObject;
    }
}
