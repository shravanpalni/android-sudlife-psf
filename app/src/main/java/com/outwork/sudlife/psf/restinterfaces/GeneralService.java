package com.outwork.sudlife.psf.restinterfaces;

import com.outwork.sudlife.psf.restinterfaces.POJO.Feedback;
import com.outwork.sudlife.psf.restinterfaces.POJO.PermissionSettings;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Panch on 3/22/2016.
 */
public interface GeneralService {
    @POST("FileService/temporarycredentials2")
    Call<RestResponse> getS3Credentials(@Header("utoken") String userToken);

    @POST("AppUserService/user/feedback")
    Call<RestResponse> feedback(@Header("utoken") String userToken,
                                @Query("groupid") String groupid,
                                @Body Feedback sJsonBody);

    @POST("AppUserService/user/{userid}/permission")
    Call<RestResponse> updatepermissions(@Header("utoken") String userToken,
                                         @Path("userid") String userid,
                                         @Body PermissionSettings sJsonBody);
}
