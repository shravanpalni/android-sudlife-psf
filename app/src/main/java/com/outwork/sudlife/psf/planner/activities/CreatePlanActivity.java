package com.outwork.sudlife.psf.planner.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.activities.ListCustomerActivity;
import com.outwork.sudlife.psf.branches.activities.ShowHierarchyActivity;
import com.outwork.sudlife.psf.branches.models.BranchesModel;
import com.outwork.sudlife.psf.branches.services.ContactsIntentService;
import com.outwork.sudlife.psf.dto.Geocode;
import com.outwork.sudlife.psf.dto.geocode.STATUS;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.localbase.StaticDataMgr;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.SupervisorMgr;
import com.outwork.sudlife.psf.planner.adapter.SupervisorListAdapter;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.planner.models.SupervisorModel;
import com.outwork.sudlife.psf.restinterfaces.ReverseGeoInterface;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreatePlanActivity extends BaseActivity  {

    private EditText vplandate;
    private TextView typelabel, suprvisorlabel, toolbar_title, branch_display; //newly added
    private AppCompatSpinner activitytype, supervisor;
    private CheckBox checkBox;
    private String pvisitdate, plandate, onclick;
    private EditText others;
    private Button submit, checkin;
    private RadioGroup type;
    private RadioButton visit, activity;
    private Boolean isothersMandatory = false;
    private List<SupervisorModel> supervisorModelList = new ArrayList<>();
    private List<String> activitytypeList = new ArrayList<>();
    private BranchesModel branchesModel = new BranchesModel();
    private SupervisorListAdapter supervisorListAdapter;
    private ArrayAdapter<String> activityTypeAdapter;
    private PlannerModel plannerModel = new PlannerModel();
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
   // private LocationProvider mLocationProvider;
    //private double latitude, longitude;
    private String addressline1, addressline2, locality, city, state, zip, addressline = "";
    private int planststatus, completestatus;
    private TextInputLayout otherslabel;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;
    private String cname, ctype, branchCode, customerId; //newly added
    private boolean isFinish = false;
    private boolean isFinishBranches = false;


    private int counter = 0;
    int flag;
    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                vplandate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                pvisitdate = TimeUtils.convertDatetoUnix(visitdt);
                if (planststatus == 0) {
                    if (TimeUtils.getFormattedDatefromUnix(pvisitdate, "dd/MM/yyyy")
                            .equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
                        checkin.setVisibility(View.VISIBLE);
                        submit.setVisibility(View.VISIBLE);
                        submit.setEnabled(false);
                        checkin.setEnabled(true);
                        submit.setTextColor(getResources().getColor(R.color.main_color_grey_500));
                        checkin.setTextColor(getResources().getColor(R.color.white));
                        vplandate.setText(TimeUtils.getCurrentDate());
                        completestatus = 1;
                    } else if (Integer.parseInt(pvisitdate) > Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp())) {
                        submit.setVisibility(View.VISIBLE);
                        checkin.setVisibility(View.VISIBLE);
                        vplandate.setText(plandate);
                        checkin.setEnabled(false);
                        submit.setEnabled(true);
                        checkin.setTextColor(getResources().getColor(R.color.main_color_grey_500));
                        submit.setTextColor(getResources().getColor(R.color.white));
                        completestatus = 0;
                    }
                } else {
                    checkin.setVisibility(View.GONE);
                    submit.setVisibility(View.VISIBLE);
                    completestatus = 0;
                    if (TimeUtils.getFormattedDatefromUnix(pvisitdate, "dd/MM/yyyy")
                            .equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
                        vplandate.setText(TimeUtils.getCurrentDate());
                    } else if (Integer.parseInt(TimeUtils.convertDatetoUnix(plandate)) > Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp())) {
                        vplandate.setText(plandate);
                    }
                }
                vplandate.setText(visitdt);
                vplandate.setEnabled(true);
            }
        }

        @Override
        public void onDateTimeCancel() {
            vplandate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        Log.e("customer ::::",getIntent().getStringExtra("customer"));
        Log.e("customer id::::",getIntent().getStringExtra("customerid"));
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(Constants.PLAN_STATUS)) {
            planststatus = getIntent().getIntExtra(Constants.PLAN_STATUS, 0);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(Constants.PLAN_DATE)) {
            plandate = getIntent().getStringExtra(Constants.PLAN_DATE);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("customer")) {
            branchesModel = new Gson().fromJson(getIntent().getStringExtra("customer"), BranchesModel.class);
        }
      //  mLocationProvider = new LocationProvider(CreatePlanActivity.this, this);
        //mLocationProvider.connect();
        mgr = LocalBroadcastManager.getInstance(CreatePlanActivity.this);
        if (SharedPreferenceManager.getInstance().getString(Constants.CUSTOMERS_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                ContactsIntentService.insertCustomersinDB(CreatePlanActivity.this);
            }
        }
        initToolBar();
        initViews();
        getBranches();
        getActivitytypes();
        setListeners();
        if (planststatus == 0) {
            if (TimeUtils.getFormattedDatefromUnix(TimeUtils.convertDatetoUnix(plandate), "dd/MM/yyyy")
                    .equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
                checkin.setVisibility(View.VISIBLE);
                submit.setVisibility(View.VISIBLE);
                submit.setEnabled(false);
                checkin.setEnabled(true);
                submit.setTextColor(getResources().getColor(R.color.main_color_grey_500));
                checkin.setTextColor(getResources().getColor(R.color.white));
                vplandate.setText(TimeUtils.getCurrentDate());
                completestatus = 1;
            } else if (Integer.parseInt(TimeUtils.convertDatetoUnix(plandate)) > Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp())) {
                submit.setVisibility(View.VISIBLE);
                checkin.setVisibility(View.VISIBLE);
                vplandate.setText(plandate);
                checkin.setEnabled(false);
                submit.setEnabled(true);
                checkin.setTextColor(getResources().getColor(R.color.main_color_grey_500));
                submit.setTextColor(getResources().getColor(R.color.white));
                completestatus = 0;
            } else {
                checkin.setVisibility(View.VISIBLE);
                submit.setVisibility(View.VISIBLE);
                submit.setEnabled(false);
                checkin.setEnabled(true);
                submit.setTextColor(getResources().getColor(R.color.main_color_grey_500));
                checkin.setTextColor(getResources().getColor(R.color.white));
                vplandate.setText(TimeUtils.getCurrentDate());
                completestatus = 1;
            }
        } else {
            checkin.setVisibility(View.GONE);
            submit.setVisibility(View.VISIBLE);
            completestatus = 0;
            if (Utils.isNotNullAndNotEmpty(plandate)) {
                if (TimeUtils.getFormattedDatefromUnix(TimeUtils.convertDatetoUnix(plandate), "dd/MM/yyyy")
                        .equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
                    vplandate.setText(TimeUtils.getCurrentDate());
                } else if (Integer.parseInt(TimeUtils.convertDatetoUnix(plandate)) > Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp())) {
                    vplandate.setText(plandate);
                } else {
                    vplandate.setText(TimeUtils.getCurrentDate());
                }
            } else {
                vplandate.setText(TimeUtils.getCurrentDate());
            }
        }
        Utils.setTypefaces(IvokoApplication.robotoTypeface, visit, activity, vplandate,
                (TextView) findViewById(R.id.others), branch_display);
        Utils.setTypefaces(IvokoApplication.robotoLightTypeface,
                (TextView) findViewById(R.id.name), (TextView) findViewById(R.id.textView1),
                (TextView) findViewById(R.id.suprvisorlbl), (TextView) findViewById(R.id.atype));
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title, checkin, submit);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            //finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("customer_broadcast"));
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("supervisor_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (mLocationProvider != null)
                    mLocationProvider.connect();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        new Thread(new Runnable() {
            @Override
            public void run() {
              //  if (mLocationProvider != null)
                //    mLocationProvider.disconnect();
            }
        });

    }

   /* @Override
    public void handleNewLocation(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            Log.e("lat long ","lat long "+latitude +" :::"+ longitude);
            LatLng latLng = new LatLng(latitude, longitude);
            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                getLocation(latitude, longitude);
        }
    }*/




    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Create New");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private void initViews() {
        branch_display = (TextView) findViewById(R.id.branch_display);  //newly added
        typelabel = (TextView) findViewById(R.id.atype);
        suprvisorlabel = (TextView) findViewById(R.id.suprvisorlbl);
        vplandate = (EditText) findViewById(R.id.vplandate);
        submit = (Button) findViewById(R.id.submit);
        checkin = (Button) findViewById(R.id.checkin);
        type = (RadioGroup) findViewById(R.id.type);
        visit = (RadioButton) findViewById(R.id.visit);
        activity = (RadioButton) findViewById(R.id.activity);
        supervisor = (AppCompatSpinner) findViewById(R.id.supervisor);
        activitytype = (AppCompatSpinner) findViewById(R.id.activitytype);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        otherslabel = (TextInputLayout) findViewById(R.id.otherslabel);
        others = (EditText) findViewById(R.id.others);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getBranches();
                getSupervisors();
            }
        };

        visit.setChecked(true);
        pvisitdate = TimeUtils.convertDatetoUnix(TimeUtils.getCurrentDate());
        if (visit.isChecked()) {
            typelabel.setVisibility(View.GONE);
            activitytype.setVisibility(View.GONE);
        }
    }

    private void getBranches() {
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("customerName")) {
            cname = getIntent().getStringExtra("customerName");
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("customerType")) {
            ctype = getIntent().getStringExtra("customerType");
        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("branchCode")) {
            branchCode = getIntent().getStringExtra("branchCode");
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("customerId")) {
            customerId = getIntent().getStringExtra("customerId");


        }


        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("finish")) {
            isFinish = getIntent().getExtras().getBoolean("finish");


        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("finishBranches")) {
            isFinishBranches = getIntent().getExtras().getBoolean("finishBranches");


        }






        if (branchesModel != null) {
            StringBuilder stringBuilder = new StringBuilder();
            if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomername())) {
                stringBuilder.append(branchesModel.getCustomername());
            }
            if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomertype())) {
                stringBuilder.append("(" + branchesModel.getCustomertype() + ")");
            }
            branch_display.setText(stringBuilder.toString());
            plannerModel.setCustomername(branchesModel.getCustomername());
            plannerModel.setCustomertype(branchesModel.getCustomertype());
            plannerModel.setBranchcode(branchesModel.getBranchcode());
            plannerModel.setCustomerid(branchesModel.getCustomerid());
        }
    }

    private void getActivitytypes() {
        activitytypeList = StaticDataMgr.getInstance(CreatePlanActivity.this).getListNames(groupId, "subtype");
        if (activitytypeList.size() > 0) {
            activityTypeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, activitytypeList);
            activityTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            activitytype.setAdapter(activityTypeAdapter);
            activitytype.setSelection(0);
            activitytype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    activitytype.setSelection(position);
                    plannerModel.setSubtype(activitytype.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } else {
            typelabel.setVisibility(View.GONE);
            activitytype.setVisibility(View.GONE);
        }
    }

    private void getSupervisors() {
        supervisorModelList.clear();
        supervisorModelList = SupervisorMgr.getInstance(CreatePlanActivity.this).getsupervisorList(userid);
        if (supervisorModelList.size() > 0) {
            supervisorListAdapter = new SupervisorListAdapter(this, supervisorModelList);
            supervisor.setAdapter(supervisorListAdapter);
            supervisor.setSelection(0);
            supervisor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (supervisorModelList.get(position).getUsername().equalsIgnoreCase("Others")) {
                        otherslabel.setVisibility(View.VISIBLE);
                        isothersMandatory = true;
                        plannerModel.setSupervisorid(supervisorModelList.get(position).getSupervisorid());
                    } else {
                        otherslabel.setVisibility(View.GONE);
                        supervisor.setSelection(position);
                        isothersMandatory = false;
                        plannerModel.setSupervisorid(supervisorModelList.get(position).getSupervisorid());
                        plannerModel.setSupervisorname(supervisorModelList.get(position).getUsername());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }

    private void setListeners() {
        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.visit) {
                    plannerModel.setType("Visit");
                    plannerModel.setActivitytype("Visit");
                    typelabel.setVisibility(View.GONE);
                    activitytype.setVisibility(View.GONE);
                } else {
                    plannerModel.setType("Activity");
                    plannerModel.setActivitytype("Activity");
                    typelabel.setVisibility(View.VISIBLE);
                    activitytype.setVisibility(View.VISIBLE);
                }
            }
        });
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    suprvisorlabel.setVisibility(View.VISIBLE);
                    supervisor.setVisibility(View.VISIBLE);
                    getSupervisors();
                } else {
                    isothersMandatory = false;
                    suprvisorlabel.setVisibility(View.GONE);
                    supervisor.setVisibility(View.GONE);
                    otherslabel.setVisibility(View.GONE);
                }
            }
        });

        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 onclick = "checkin";
                if (counter==0) {
                    hasConnection();
                    //Log.e("counter is ", String.valueOf(counter));
                }
                else
                {
                    hasConnection();
                }
                if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP) {
                    dispatchlocationIntent(checkin);
                } else {

                    boolean gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (!gpsStatus) {
                         showSettingDialog();
                    } else {

                        if (!Utils.isNotNullAndNotEmpty(branch_display.getText().toString())) {            // newly added
                            branch_display.setError("Branch selection is mandatory");
                            branch_display.requestFocus();
                        } else if (!Utils.isNotNullAndNotEmpty(vplandate.getText().toString())) {
                            vplandate.setError("Date required");
                            vplandate.requestFocus();
                        } else {
                           // Log.e("location captured ",latitude +" : " + longitude );
                            if (latitude > 0.0 || longitude > 0.0) {
                                if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                    getLocation(latitude, longitude);
                                //Log.e("before posting data",latitude +"and " +longitude);
                                postData();
                            } else {
                                if (mLocationProvider != null) {
                                    mLocationProvider.connect();
                                    showProgressDialog("Searching for location....");
                                    new Handler().postDelayed(new Runnable() {
                                        public void run() {
                                            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                getLocation(latitude, longitude);
                                           // Log.e("before posting data",latitude +"and " +longitude);
                                            postData();
                                            dismissProgressDialog();
                                        }
                                    }, LOCATION_FETCH_TIME);
                                } else {
                                    showProgressDialog("Searching for location....");
                                    new Handler().postDelayed(new Runnable() {
                                        public void run() {
                                            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                getLocation(latitude, longitude);
                                           // Log.e("before posting data",latitude +"and " +longitude);
                                            postData();
                                            dismissProgressDialog();
                                        }
                                    }, LOCATION_FETCH_TIME);
                                }
                            }
                        }
                    }
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclick = "submit";
                dispatchlocationIntent(submit);
            }
        });
        vplandate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vplandate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 60);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
    }

//    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void postData() {
//        SimpleDateFormat spf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
//        Date newDate = null;
//        try {
//            newDate = spf.parse(getCurrentNetworkTime());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        spf = new SimpleDateFormat("dd/MM/yyyy");
//        String newDateString = spf.format(newDate);
//        Time today = new Time(Time.getCurrentTimezone());
//        today.setToNow();
//        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
//        if (Utils.isNotNullAndNotEmpty(newDateString))
//            if (newDateString.equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
//        try {
//            if ((Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) == 1)) {
//            Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME_ZONE);
                plannerModel.setCheckintime(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
                if (Utils.isNotNullAndNotEmpty(addressline))
                    plannerModel.setCheckinlocation(addressline);
                plannerModel.setCheckinlat(String.valueOf(latitude));
                plannerModel.setCheckinlong(String.valueOf(longitude));
                plannerModel.setDevicetimestamp(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));

                postcreateplan(1, "You have checked in successfully");
//            } else {
//                showSimpleAlert("Date is Inaccurate", "Your phone date is inaccurate! Adjust your clock and try again.");
//            }
//        } catch (Settings.SettingNotFoundException e) {
//            e.printStackTrace();
//        }
    }

    private void showSettingDialog() {
        initGoogleAPIClient();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        //locationRequest.setInterval(30 * 1000);
       // locationRequest.setFastestInterval(5 * 1000);
        locationRequest.setInterval(30 * 1000);
         locationRequest.setFastestInterval(3 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        if (onclick.equalsIgnoreCase("checkin")) {
                            if (!Utils.isNotNullAndNotEmpty(branch_display.getText().toString())) {           //newly added
                                branch_display.setError("Branch selection is mandatory");
                                branch_display.requestFocus();
                            } else if (!Utils.isNotNullAndNotEmpty(vplandate.getText().toString())) {
                                vplandate.setError("Date required");
                                vplandate.requestFocus();
                            } else {
                                List<PlannerModel> plannerModels = PlannerMgr.getInstance(CreatePlanActivity.this).getPlannerCreationCondition(userid, plannerModel.getCustomername(),
                                        Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", vplandate.getText().toString())),
                                        Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", vplandate.getText().toString())),
                                        Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", vplandate.getText().toString())));
                                List<PlannerModel> plannersModelList = new ArrayList<>();
                                for (PlannerModel plannerModel : plannerModels) {
                                    if (plannerModel != null)
                                        if (!Utils.isNotNullAndNotEmpty(plannerModel.getLeadid())) {
                                            plannersModelList.add(plannerModel);
                                        }
                                }
                                if (plannersModelList.size() > 0) {
                                    //showAlert("", "Please check-out the prior activity for this branch in order to create a new unplanned activity for the same branch for the same day.", "ok", new DialogInterface.OnClickListener() {



                                        showAlert("", "Please check-out the prior activity for this day in order to create a new unplanned activity.", "ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    }, "", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    }, false);
                                } else {
                                    if (latitude > 0.0 || longitude > 0.0) {
                                        if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                        getLocation(latitude, longitude);
                                        //Log.e("before posting data",latitude +"and " +longitude);
                                        postData();
                                    } else {
                                        if (mLocationProvider != null) {
                                            mLocationProvider.connect();
                                            showProgressDialog("Searching for location....");
                                            new Handler().postDelayed(new Runnable() {
                                                public void run() {
                                                    if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                        getLocation(latitude, longitude);
                                                   // Log.e("before posting data",latitude +"and " +longitude);
                                                    postData();
                                                    dismissProgressDialog();
                                                }
                                            }, LOCATION_FETCH_TIME);
                                        } else {
                                            showProgressDialog("Searching for location....");
                                            new Handler().postDelayed(new Runnable() {
                                                public void run() {
                                                    if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                        getLocation(latitude, longitude);
                                                   // Log.e("before posting data",latitude +"and " +longitude);
                                                    postData();
                                                    dismissProgressDialog();
                                                }
                                            }, LOCATION_FETCH_TIME);
                                        }
                                    }
                                }
                            }

                        }
                        if (onclick.equalsIgnoreCase("submit")) {
                            if (!Utils.isNotNullAndNotEmpty(branch_display.getText().toString())) {         //newly added
                                branch_display.setError("Branch selection is mandatory");
                                branch_display.requestFocus();
                            } else if (!Utils.isNotNullAndNotEmpty(vplandate.getText().toString())) {
                                vplandate.setError("Date required");
                                vplandate.requestFocus();
                            } else if (isothersMandatory && !Utils.isNotNullAndNotEmpty(others.getText().toString())) {
                                others.setError("Mention Other Supervisor Name");
                                others.requestFocus();
                            } else {
                                List<PlannerModel> plannerModels = PlannerMgr.getInstance(CreatePlanActivity.this).getPlannerCreationCondition(userid, plannerModel.getCustomername(),
                                        Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", vplandate.getText().toString())),
                                        Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", vplandate.getText().toString())),
                                        Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", vplandate.getText().toString())));
                                List<PlannerModel> plannersModelList = new ArrayList<>();
                                for (PlannerModel plannerModel : plannerModels) {
                                    if (plannerModel != null)
                                        if (!Utils.isNotNullAndNotEmpty(plannerModel.getLeadid())) {
                                            plannersModelList.add(plannerModel);
                                        }
                                }
                                if (plannersModelList.size() > 0) {
                                    //showAlert("", "Please check-out the prior activity for this branch in order to create a new unplanned activity for the same branch for the same day.", "ok", new DialogInterface.OnClickListener() {
                                        showAlert("", "Please check-out the prior activity for this day in order to create a new unplanned activity.", "ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    }, "", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    }, false);
                                } else {
                                    postcreateplan(0, "Your Plan is submitted successfully");
                                }
                            }
                        }
                        updateGPSStatus("GPS is Enabled in your device");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(CreatePlanActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });

    }

   @SuppressLint("LongLogTag")
   public boolean hasConnection() {
       ConnectivityManager cm = (ConnectivityManager) this.getSystemService(
               Context.CONNECTIVITY_SERVICE);

       NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
       if (wifiNetwork != null && wifiNetwork.isConnected()) {
           return true;
       }

       NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
       if (mobileNetwork != null && mobileNetwork.isConnected()) {
           return true;
       }

       NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
       if (activeNetwork != null && activeNetwork.isConnected()) {
           return true;
       } else {


           /* showAlert("", "Please check your Internet Connection", "ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }, "", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            }, false);*/
           if (counter == 0) {
               flag = 0;                     //GPS OFF and Internet OFF
               Toast.makeText(CreatePlanActivity.this, "No internet connection....", Toast.LENGTH_LONG).show();
               counter++;
               return false;
           }
           else{
                flag = 1;                    // GPS ON and Internet OFF
               Log.e("checkin with no internet  " +"", String.valueOf(flag));
               return false;
           }
       }

    }

    public void dispatchlocationIntent(Button button) {
        int fineLocationPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int coarseLocationPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (fineLocationPermission != PackageManager.PERMISSION_GRANTED || coarseLocationPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        } else {
            boolean gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!gpsStatus) {
                showSettingDialog();
            } else {
                if (button.getId() == R.id.checkin) {
                    if (!Utils.isNotNullAndNotEmpty(branch_display.getText().toString())) {       //newly added
                        branch_display.setError("Branch selection is mandatory");
                        branch_display.requestFocus();
                    } else if (!Utils.isNotNullAndNotEmpty(vplandate.getText().toString())) {
                        vplandate.setError("Date required");
                        vplandate.requestFocus();
                    } else {
                        List<PlannerModel> plannerModels = PlannerMgr.getInstance(CreatePlanActivity.this).getPlannerCreationCondition(userid, plannerModel.getCustomername(),
                                Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", vplandate.getText().toString())),
                                Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", vplandate.getText().toString())),
                                Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", vplandate.getText().toString())));
                        List<PlannerModel> plannersModelList = new ArrayList<>();
                        for (PlannerModel plannerModel : plannerModels) {
                            if (plannerModel != null)
                                if (!Utils.isNotNullAndNotEmpty(plannerModel.getLeadid())) {
                                    plannersModelList.add(plannerModel);
                                }
                        }
                        if (plannersModelList.size() > 0) {
                            //showAlert("", "Please check-out the prior activity for this branch in order to create a new unplanned activity for the same branch for the same day.", "ok", new DialogInterface.OnClickListener() {

                            showAlert("", "Please check-out the prior activity for this day in order to create a new unplanned activity.", "ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }, "", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }, false);
                        } else {
                            if (latitude > 0.0 || longitude > 0.0) {
                                if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                 getLocation(latitude, longitude);
                               // Log.e("before posting data",latitude +"and " +longitude);
                                postData();
                            } else {
                                if (mLocationProvider != null) {
                                    mLocationProvider.connect();
                                    showProgressDialog("Searching for location....");
                                    new Handler().postDelayed(new Runnable() {
                                        public void run() {
                                            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                getLocation(latitude, longitude);
                                            postData();
                                            dismissProgressDialog();
                                        }
                                    }, LOCATION_FETCH_TIME);
                                } else {
                                    showProgressDialog("Searching for location....");
                                    new Handler().postDelayed(new Runnable() {
                                        public void run() {
                                            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                                                getLocation(latitude, longitude);
                                            postData();
                                            dismissProgressDialog();
                                        }
                                    }, LOCATION_FETCH_TIME);
                                }
                            }
                        }
                    }
                }
                if (button.getId() == R.id.submit) {
                    if (!Utils.isNotNullAndNotEmpty(branch_display.getText().toString())) {
                        branch_display.setError("Branch selection is mandatory");
                        branch_display.requestFocus();
                    } else if (!Utils.isNotNullAndNotEmpty(vplandate.getText().toString())) {
                        vplandate.setError("Date required");
                        vplandate.requestFocus();
                    } else if (isothersMandatory && !Utils.isNotNullAndNotEmpty(others.getText().toString())) {
                        others.setError("Mention Other Supervisor Name");
                        others.requestFocus();
                    } else {
                        postcreateplan(0, "Your Plan is submitted successfully");

                       /* List<PlannerModel> plannerModels = PlannerMgr.getInstance(CreatePlanActivity.this).getPlannerCreationCondition(userid, plannerModel.getCustomername(),
                                Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", vplandate.getText().toString())),
                                Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", vplandate.getText().toString())),
                                Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", vplandate.getText().toString())));
                        List<PlannerModel> plannersModelList = new ArrayList<>();
                        for (PlannerModel plannerModel : plannerModels) {
                            if (plannerModel != null)
                                if (!Utils.isNotNullAndNotEmpty(plannerModel.getLeadid())) {
                                    plannersModelList.add(plannerModel);
                                }
                        }
                        if (plannersModelList.size() > 0) {
                            //showAlert("", "Please check-out the prior activity for this branch in order to create a new unplanned activity for the same branch for the same day.", "ok", new DialogInterface.OnClickListener() {
                            showAlert("", "Please check-out the prior activity for this day in order to create a new unplanned activity.", "ok", new DialogInterface.OnClickListener() {
                            @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }, "", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }, false);
                        } else {
                            postcreateplan(0, "Your Plan is submitted successfully");
                        }*/
                    }
                }
            }
        }
    }

    private void postcreateplan(int compltestatus, String msg) {
        plannerModel.setGroupid(groupId);
        plannerModel.setUserid(userid);
        if (visit.isChecked()) {
            plannerModel.setType("Visit");
            plannerModel.setActivitytype("Visit");
        } else {
            plannerModel.setType("Activity");
            plannerModel.setActivitytype("Activity");
            if (activitytypeList.size() > 0)
                plannerModel.setSubtype(activitytype.getSelectedItem().toString());
        }
        plannerModel.setJointvisit(checkBox.isChecked());
        plannerModel.setScheduleday(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", vplandate.getText().toString())));
        plannerModel.setSchedulemonth(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", vplandate.getText().toString())));
        plannerModel.setScheduleyear(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", vplandate.getText().toString())));
        plannerModel.setScheduletime(Integer.parseInt(TimeUtils.convertDatetoUnix(vplandate.getText().toString())));
        plannerModel.setPlanstatus(planststatus);
        plannerModel.setCompletestatus(compltestatus);
        plannerModel.setDevicetimestamp(Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp()));
        if (Utils.isNotNullAndNotEmpty(others.getText().toString())) {
            plannerModel.setSupervisorname(others.getText().toString());
        }
        plannerModel.setNetwork_status("offline");
        if (plannerModel.getScheduletime() != null)
            if (compltestatus == 1) {
                if (TimeUtils.getFormattedDatefromUnix(String.valueOf(plannerModel.getScheduletime()), "dd/MM/yyyy")
                        .equalsIgnoreCase(TimeUtils.getCurrentDate("dd/MM/yyyy"))) {
                    PlannerMgr.getInstance(CreatePlanActivity.this).insertPlanner(plannerModel, userid, groupId);
                    List<PlannerModel> plannerModelArrayList = new ArrayList<>();
                    plannerModelArrayList.add(plannerModel);
                    PlannerMgr.getInstance(CreatePlanActivity.this).insertPlannerNotificationList(plannerModelArrayList, userid, groupId);
                    LeadIntentService.syncLeadstoServer(CreatePlanActivity.this);
                    showAlert("", msg,
                            "OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {


                                    if (isFinish){
                                        ShowHierarchyActivity.showHierarchyActivity.finish();
                                        ShowHierarchyActivity.showHierarchyActivity = null;
                                    }else if (isFinishBranches){

                                        ListCustomerActivity.listCustomerActivity.finish();
                                        ListCustomerActivity.listCustomerActivity = null;
                                    }


                                    finish();
                                }
                            },
                            "",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                }
                            }, false);
                } else {
                    showToast(getResources().getString(R.string.cantcheckin));
                }
            } else {
                PlannerMgr.getInstance(CreatePlanActivity.this).insertPlanner(plannerModel, userid, groupId);
                List<PlannerModel> plannerModelArrayList = new ArrayList<>();
                plannerModelArrayList.add(plannerModel);
                PlannerMgr.getInstance(CreatePlanActivity.this).insertPlannerNotificationList(plannerModelArrayList, userid, groupId);
                LeadIntentService.syncLeadstoServer(CreatePlanActivity.this);
                showAlert("", msg,
                        "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                if (isFinish){
                                    ShowHierarchyActivity.showHierarchyActivity.finish();
                                    ShowHierarchyActivity.showHierarchyActivity = null;
                                }else if (isFinishBranches){

                                    ListCustomerActivity.listCustomerActivity.finish();
                                    ListCustomerActivity.listCustomerActivity = null;
                                }

                                finish();
                            }
                        },
                        "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        }, false);
            }
    }

    private void getLocation(double lat, double lng) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ReverseGeoInterface.MAP_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String latlong = lat + "," + lng;
        ReverseGeoInterface service = retrofit.create(ReverseGeoInterface.class);
        Call<Geocode> locationdetails = service.getLocation("GEOMETRIC_CENTER", latlong, "AIzaSyB6QdommPXh8xlvGJL9IkauaajCpKfNWpc");

        locationdetails.enqueue(new Callback<Geocode>() {
            @Override
            public void onResponse(Call<Geocode> call, Response<Geocode> response) {
                Geocode geocode = response.body();
                if (geocode != null)
                    if (geocode.getResults() != null)
                        if (geocode.getStatus() == STATUS.OK) {
                            if (geocode.getResults().size() > 0) {
                                if (geocode.getResults().get(0).getAddressComponents() != null) {
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(0).getLongName()))
                                        addressline1 = geocode.getResults().get(0).getAddressComponents().get(0).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(1).getLongName()))
                                        addressline2 = geocode.getResults().get(0).getAddressComponents().get(1).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(2).getLongName()))
                                        locality = geocode.getResults().get(0).getAddressComponents().get(2).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(3).getLongName()))
                                        city = geocode.getResults().get(0).getAddressComponents().get(3).getLongName();
                                    addressline = addressline1 + " " + addressline2 + " " + locality + " " + city;
                                }
                            }
                        }
            }

            @Override
            public void onFailure(Call<Geocode> call, Throwable t) {
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        dismissProgressDialog();
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_INTENT_ID: {
                if (grantResults != null) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            initGoogleAPIClient();
                        } else {
                            if (onclick.equalsIgnoreCase("checkin")) {
                                dispatchlocationIntent(checkin);
                            } else if (onclick.equalsIgnoreCase("submit")) {
                                dispatchlocationIntent(submit);
                            }
                        }
                        updateGPSStatus("GPS is Enabled in your device");
                    } else {
                        showToast(getResources().getString(R.string.locationtoast));
                    }
                } else {
                    showToast(getResources().getString(R.string.locationtoast));
                }
                return;
            }
        }
    }
}
