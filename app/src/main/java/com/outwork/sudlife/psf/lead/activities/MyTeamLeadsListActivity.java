package com.outwork.sudlife.psf.lead.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.adapters.TeamLeadsAdapter;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.planner.adapter.TeamMembersAdapter;
import com.outwork.sudlife.psf.planner.models.TeamMembers;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.targets.services.RestServicesForTragets;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyTeamLeadsListActivity extends BaseActivity {

    private RecyclerView rcvLeads;
    private RelativeLayout searchLayout;
    private EditText searchText;
    private TabLayout tabLayout;
    private AppCompatSpinner teamSpinner;
    private String searchby = "", stage = "", teamMemberUserid;
    private int position = 5;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ImageView filter;
    private EditText startdate, enddate;
    private Button submit;
    private String sdate, edate;
    private TeamLeadsAdapter teamLeadsAdapter;
    private boolean doubleBackToExitPressedOnce = true;
    private TextView fetcchtime, toolbar_title;
    private Boolean isfiltered = false;
    private List<LeadModel> leadModels = new ArrayList<>();
    private ArrayList<TeamMembers> teamMembersArrayList = new ArrayList<>();
    private RadioButton name, branchname, bankname, contactno, sudcode;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                startdate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                sdate = TimeUtils.convertDatetoUnix(visitdt);
                startdate.setText(visitdt);
                startdate.setEnabled(true);
                startdate.setError(null);
            }
            if (customSelector == 1) {
                enddate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                edate = TimeUtils.convertDatetoUnix(visitdt);
                enddate.setText(visitdt);
                enddate.setEnabled(true);
                enddate.setError(null);
            }
        }

        @Override
        public void onDateTimeCancel() {
            startdate.setEnabled(true);
            enddate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_leads);
        initToolBar();
        rcvLeads = (RecyclerView) findViewById(R.id.rcv_leads);
        teamSpinner = (AppCompatSpinner) findViewById(R.id.team_spinner);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.lead_swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.topbarcolor, R.color.daycolor);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Open"));
        tabLayout.addTab(tabLayout.newTab().setText("Contacted &\n meeting fixed"));
        tabLayout.addTab(tabLayout.newTab().setText("Proposition\n presented"));
        tabLayout.addTab(tabLayout.newTab().setText("Converted"));
        tabLayout.addTab(tabLayout.newTab().setText("Not\n Interested"));
        tabLayout.addTab(tabLayout.newTab().setText("All"));
        findViewById(R.id.fb_lead_add).setVisibility(View.GONE);
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        searchText = (EditText) findViewById(R.id.searchText);
        filter = (ImageView) findViewById(R.id.filter);
        fetcchtime = (TextView) findViewById(R.id.fetchtime);
        getTeamMembers();
        setListener();
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, (TextView) findViewById(R.id.tv_nodata), toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == R.id.filter) {
            dateFilter();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = false;
            showToast("Please click BACK again to exit.");
        } else {
            finish();
        }
    }

    private void setListener() {
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showpopup();
            }
        });
        teamSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                teamMemberUserid = teamMembersArrayList.get(position).getUserid();
                if (isNetworkAvailable()) {
                    if (Utils.isNotNullAndNotEmpty(teamMemberUserid)) {
                        leadModels.clear();
                        rcvLeads.setAdapter(null);
                        tabLayout.getTabAt(5).select();
                        getServerLeadsbyUserID(teamMemberUserid);
                    }
                } else {
                    showAlert("", "Oops No Internet connection..", "Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }, "", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }, false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isNetworkAvailable()) {
                    if (Utils.isNotNullAndNotEmpty(teamMemberUserid)) {
                        leadModels.clear();
                        searchText.setText("");
                        rcvLeads.setAdapter(null);
                        tabLayout.getTabAt(5).select();
                        getServerLeadsbyUserID(teamMemberUserid);
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                } else {
                    showAlert("", "Oops No Internet connection..", "Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }, "", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }, false);
                }
            }
        });
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().toString().equalsIgnoreCase("Open")) {
                    stage = "Open";
                    position = 0;
                    isfiltered = false;
                } else if (tab.getText().toString().equalsIgnoreCase("Contacted &\n meeting fixed")) {
                    stage = "Contacted & meeting fixed";
                    position = 1;
                    isfiltered = false;
                } else if (tab.getText().toString().equalsIgnoreCase("Proposition\n presented")) {
                    stage = "Proposition presented";
                    position = 2;
                    isfiltered = false;
                } else if (tab.getText().toString().equalsIgnoreCase("Converted")) {
                    stage = "Converted";
                    position = 3;
                    isfiltered = false;
                } else if (tab.getText().toString().equalsIgnoreCase("Not\n Interested")) {
                    stage = "Not Interested";
                    position = 4;
                    isfiltered = false;
                } else {
                    stage = "";
                    position = 5;
                    isfiltered = false;
                }
                List<LeadModel> leadModelList = new ArrayList<LeadModel>();
                for (LeadModel leadModel : leadModels) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
                        if (leadModel.getLeadstage().equals(stage)) {
                            leadModelList.add(leadModel);
                        }
                    } else {
                        leadModelList = leadModels;
                    }
                }
                showList(leadModelList, searchby);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Team Leads");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setUpSearchLayout() {
        searchLayout.setVisibility(View.VISIBLE);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (teamLeadsAdapter != null && s != null) {
                    teamLeadsAdapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void showList(List<LeadModel> leadModels, String type) {
        ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.GONE);
        if (leadModels.size() != 0) {
            ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.GONE);
            rcvLeads.setVisibility(View.VISIBLE);
            teamLeadsAdapter = new TeamLeadsAdapter(leadModels, MyTeamLeadsListActivity.this, type, stage);
            rcvLeads.setAdapter(teamLeadsAdapter);
            rcvLeads.setLayoutManager(new LinearLayoutManager(MyTeamLeadsListActivity.this));
            if (leadModels.size() > 10) {
                setUpSearchLayout();
            }
        } else {
            ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.VISIBLE);
            rcvLeads.setVisibility(View.GONE);
        }
        fetcchtime.setText("Last Refreshed Time : " + TimeUtils.getFormattedDatefromUnix(SharedPreferenceManager.getInstance().getString(Constants.LEAD_LAST_FETCH_TIME, ""), "dd/MM/yyyy hh:mm a"));
    }

    public void showpopup() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyTeamLeadsListActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.search_popup_layout, null);

        alertDialog.setView(convertView);
        RadioGroup type = (RadioGroup) convertView.findViewById(R.id.type);
        name = (RadioButton) convertView.findViewById(R.id.name);
        branchname = (RadioButton) convertView.findViewById(R.id.branchname);
        bankname = (RadioButton) convertView.findViewById(R.id.bankname);
        contactno = (RadioButton) convertView.findViewById(R.id.contactno);
        sudcode = (RadioButton) convertView.findViewById(R.id.sudcode);
        if (Utils.isNotNullAndNotEmpty(searchby)) {
            if (searchby.equalsIgnoreCase("name")) {
                name.setChecked(true);
            } else if (searchby.equalsIgnoreCase("branchname")) {
                branchname.setChecked(true);
            } else if (searchby.equalsIgnoreCase("bankname")) {
                bankname.setChecked(true);
            } else if (searchby.equalsIgnoreCase("contactno")) {
                contactno.setChecked(true);
            } else if (searchby.equalsIgnoreCase("sudcode")) {
                sudcode.setChecked(true);
            }
        }
        Utils.setTypefaces(IvokoApplication.robotoTypeface, name, branchname, bankname, contactno, sudcode);

        final AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.setTitle("Select Filter Type for search.....");
        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                isfiltered = false;
                searchText.setText("");
                if (checkedId == R.id.name) {
                    searchby = "name";
                    searchText.setHint("Search with Name");
                    alert.cancel();
                } else if (checkedId == R.id.branchname) {
                    searchby = "branchname";
                    searchText.setHint("Search with Branch Name");
                    alert.cancel();
                } else if (checkedId == R.id.bankname) {
                    searchby = "bankname";
                    searchText.setHint("Search with Bank Name");
                    alert.cancel();
                } else if (checkedId == R.id.contactno) {
                    searchby = "contactno";
                    searchText.setHint("Search with Contactno.");
                    alert.cancel();
                } else if (checkedId == R.id.sudcode) {
                    searchby = "sudcode";
                    searchText.setHint("Search with SUD Code");
                    alert.cancel();
                }
                List<LeadModel> leadModelList = new ArrayList<LeadModel>();
                for (LeadModel leadModel : leadModels) {
                    if (Utils.isNotNullAndNotEmpty(stage)) {
                        if (leadModel.getLeadstage().equals(stage)) {
                            leadModelList.add(leadModel);
                        }
                    } else {
                        leadModelList = leadModels;
                    }
                }
                showList(leadModelList, searchby);
            }
        });
        alert.show();
    }

    public void dateFilter() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyTeamLeadsListActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.layout_dates_filter, null);

        alertDialog.setView(convertView);
        startdate = (EditText) convertView.findViewById(R.id.strtdate);
        enddate = (EditText) convertView.findViewById(R.id.enddate);
        submit = (Button) convertView.findViewById(R.id.submit);
        final AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.setTitle("Date Filter");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -7);
        startdate.setText(TimeUtils.getFormattedDate(calendar.getTime()));
        sdate = TimeUtils.convertDatetoUnix(TimeUtils.getFormattedDate(calendar.getTime()));
        enddate.setText(TimeUtils.getCurrentDate());
        edate = TimeUtils.convertDatetoUnix(TimeUtils.getCurrentDate());

        startdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startdate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -60);
                Date mindate = calendar.getTime();
                calendar.add(Calendar.DAY_OF_YEAR, -7);
                Date initdate = calendar.getTime();
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initdate)
                        .setMinDate(mindate)
                        .setMaxDate(new Date())
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enddate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -60);
                Date mindate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(mindate)
                        .setMaxDate(new Date())
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNotNullAndNotEmpty(startdate.getText().toString())) {
                    startdate.setError("Date required");
                    startdate.requestFocus();
                } else if (!Utils.isNotNullAndNotEmpty(enddate.getText().toString())) {
                    enddate.setError("Date required");
                    enddate.requestFocus();
                } else {
                    if (Integer.parseInt(sdate) > Integer.parseInt(edate)) {
                        showToast("Startdate should not be greater than Endnddate");
                    } else {
                        isfiltered = true;
                        List<LeadModel> leadModelList = new ArrayList<LeadModel>();
                        for (LeadModel leadModel : leadModels) {
                            if (Utils.isNotNullAndNotEmpty(stage)) {
                                if (leadModel.getLeadstage().equals(stage)) {
                                    if (leadModel.getCreatedon() >= Integer.parseInt(sdate) && leadModel.getCreatedon() <= Integer.parseInt(edate))
                                        leadModelList.add(leadModel);
                                }
                            } else {
                                if (leadModel.getCreatedon() >= Integer.parseInt(sdate) && leadModel.getCreatedon() <= Integer.parseInt(edate))
                                    leadModelList = leadModels;
                            }
                        }
                        showList(leadModelList, searchby);
                        alert.cancel();
                    }
                }
            }
        });
        alert.show();
    }

    /*****************************************Callbacks********************************/
    public void getTeamMembers() {
        showProgressDialog("Please Wait . . .");
        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamMembers(userToken);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                dismissProgressDialog();
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        Type listType = new TypeToken<ArrayList<TeamMembers>>() {
                        }.getType();
                        ArrayList<TeamMembers> tmArrayList = new Gson().fromJson(response.body().getData(), listType);
                        if (tmArrayList != null) {
                            if (tmArrayList.size() > 0) {
                                teamMembersArrayList = new ArrayList();
                                for (int i = 0; i < tmArrayList.size(); i++) {
                                    if (tmArrayList.get(i).getRole().equalsIgnoreCase("member")) {
                                        teamMembersArrayList.add(tmArrayList.get(i));
                                    }
                                }
                                if (teamMembersArrayList.size() != 0) {
                                    findViewById(R.id.spinnerview).setVisibility(View.VISIBLE);
                                    TeamMembersAdapter teamMembersAdapter = new TeamMembersAdapter(MyTeamLeadsListActivity.this, teamMembersArrayList);
                                    teamSpinner.setAdapter(teamMembersAdapter);
                                }
                            } else {
                                findViewById(R.id.spinnerview).setVisibility(View.GONE);
                                ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.VISIBLE);
                            }
                        } else {
                            findViewById(R.id.spinnerview).setVisibility(View.GONE);
                            ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.VISIBLE);
                        }
                    } else {
                        findViewById(R.id.spinnerview).setVisibility(View.GONE);
                        ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.VISIBLE);
                    }
                } else {
                    dismissProgressDialog();
                    findViewById(R.id.spinnerview).setVisibility(View.GONE);
                    ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }

    private void getServerLeadsbyUserID(String userid) {
        showProgressDialog("Please Wait . . .");
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getLeadsbyuser = client.getServerLeadsbyUserID(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""),
                userid, "");
        getLeadsbyuser.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                dismissProgressDialog();
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<LeadModel>>() {
                        }.getType();
                        tabLayout.getTabAt(position).select();
                        leadModels = new Gson().fromJson(response.body().getData(), listType);
                        showList(leadModels, searchby);
                    }
                } else {
                    ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.VISIBLE);
                    rcvLeads.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                dismissProgressDialog();
                ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.VISIBLE);
                rcvLeads.setVisibility(View.GONE);
            }
        });
    }

}
