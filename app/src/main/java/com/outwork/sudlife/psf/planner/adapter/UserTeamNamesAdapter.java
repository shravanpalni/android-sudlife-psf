package com.outwork.sudlife.psf.planner.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.ui.models.UserTeams;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shravanch on 25-04-2019.
 */

public class UserTeamNamesAdapter extends BaseAdapter{

    private Context context;
    private final LayoutInflater mInflater;
    private List<UserTeams> userTeamsList = new ArrayList<>();
    private String groupid, userid;

    static class ViewHolder {
        public TextView name;
        public ImageView picture;
        //public TextView line2, offline;
    }

    public UserTeamNamesAdapter(Context context, List<UserTeams> userteamList, String groupId, String userId) {
        this.context = context;
        this.userTeamsList = userteamList;
        this.groupid = groupId;
        this.userid = userId;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.userTeamsList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this.userTeamsList.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_team_selection, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.picture = (ImageView) view.findViewById(R.id.profileImage);
            viewHolder.name = (TextView) view.findViewById(R.id.custname);
            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, viewHolder.name);
            view.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        UserTeams dto = (UserTeams) this.userTeamsList.get(position);
        holder.name.setText(dto.getName());
        if (dto.getName().length() > 0) {
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate color based on a key (same key returns the same color), useful for list/grid views
            int color2 = generator.getColor(dto.getName().substring(0, 1));
            TextDrawable drawable = TextDrawable.builder().buildRound(dto.getName().substring(0, 1), color2);

            if (drawable != null) {
                holder.picture.setImageDrawable(drawable);
            }
        }

        return view;
    }




}
