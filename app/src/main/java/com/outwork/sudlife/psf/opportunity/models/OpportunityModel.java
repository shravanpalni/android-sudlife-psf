package com.outwork.sudlife.psf.opportunity.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


/**
 * Created by Habi on 29-08-2017.
 */

public class OpportunityModel implements Serializable {
    private int id;
    private String network_status;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("contactid")
    @Expose
    private String contactid;
    @SerializedName("customerid")
    @Expose
    private String customerid;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("ordervalue")
    @Expose
    private String ordervalue;
    @SerializedName("orderdate")
    @Expose
    private String orderdate;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("contactname")
    @Expose
    private String contactname;
    @SerializedName("customername")
    @Expose
    private String customername;
    @SerializedName("opportunityid")
    @Expose
    private String opportunityid;
    @SerializedName("referenceno")
    @Expose
    private String referenceno;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("totalunits")
    @Expose
    private String totalunits;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("stage")
    @Expose
    private String stage;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("closingdate")
    @Expose
    private String closingdate;
    @SerializedName("createddate")
    @Expose
    private String createddate;
    @SerializedName("modifieddate")
    @Expose
    private String modifieddate;
    @SerializedName("ownedbyid")
    @Expose
    private String ownedbyid;
    @SerializedName("ownedby")
    @Expose
    private String ownedby;
    @SerializedName("isdeleted")
    @Expose
    private Boolean isdeleted;

    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("income")
    @Expose
    private Integer income;
    @SerializedName("occupation")
    @Expose
    private String occupation;
    @SerializedName("familymembers")
    @Expose
    private Integer familymembers;
    @SerializedName("opportunityitems")
    @Expose
    private List<OpportunityItems> items = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNetwork_status() {
        return network_status;
    }

    public void setNetwork_status(String network_status) {
        this.network_status = network_status;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


    public String getOrdervalue() {
        return ordervalue;
    }

    public void setOrdervalue(String ordervalue) {
        this.ordervalue = ordervalue;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(String orderdate) {
        this.orderdate = orderdate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOpportunityid() {
        return opportunityid;
    }

    public void setOpportunityid(String opportunityid) {
        this.opportunityid = opportunityid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getContactid() {
        return contactid;
    }

    public void setContactid(String contactid) {
        this.contactid = contactid;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public List<OpportunityItems> getItems() {
        return items;
    }

    public void setItems(List<OpportunityItems> items) {
        this.items = items;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getReferenceno() {
        return referenceno;
    }

    public void setReferenceno(String referenceno) {
        this.referenceno = referenceno;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTotalunits() {
        return totalunits;
    }

    public void setTotalunits(String totalunits) {
        this.totalunits = totalunits;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getClosingdate() {
        return closingdate;
    }

    public void setClosingdate(String closingdate) {
        this.closingdate = closingdate;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(String modifieddate) {
        this.modifieddate = modifieddate;
    }

    public String getOwnedbyid() {
        return ownedbyid;
    }

    public void setOwnedbyid(String ownedbyid) {
        this.ownedbyid = ownedbyid;
    }

    public String getOwnedby() {
        return ownedby;
    }

    public void setOwnedby(String ownedby) {
        this.ownedby = ownedby;
    }

    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getIncome() {
        return income;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Integer getFamilymembers() {
        return familymembers;
    }

    public void setFamilymembers(Integer familymembers) {
        this.familymembers = familymembers;
    }
}