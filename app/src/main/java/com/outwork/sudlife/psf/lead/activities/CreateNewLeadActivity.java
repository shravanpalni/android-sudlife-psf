package com.outwork.sudlife.psf.lead.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.gson.Gson;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.activities.ListCustomerActivity;
import com.outwork.sudlife.psf.branches.models.BranchesModel;
import com.outwork.sudlife.psf.lead.LeadMgr;

import com.outwork.sudlife.psf.lead.db.LeadDao;
import com.outwork.sudlife.psf.lead.model.CountryCodeModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.localbase.ProductDataMgr;
import com.outwork.sudlife.psf.localbase.ProductMasterDto;
import com.outwork.sudlife.psf.localbase.StaticDataDto;
import com.outwork.sudlife.psf.localbase.StaticDataMgr;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.CSVFile;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static com.outwork.sudlife.psf.utilities.Utils.isValidPhoneNew;
import static com.outwork.sudlife.psf.utilities.Utils.isValidPhoneNri;

public class CreateNewLeadActivity extends BaseActivity {
    private BottomSheetDialog bottomSheetDialog;
    private ListView lvBottom;
    AlertDialog.Builder alertDialog;
    private List<String> myList;
    private TextView toolbar_title;
    private TextInputLayout tiLayoutothers, tiLayoutSubStatus;
    private CheckBox chk_NRI;
    private EditText etfName, et_campaigndate, etlName, etLeadStage, etContactNumber, etEmailId, etAlternativeContactNo, etAge, etExistingCustomer, etPremiumPaid, etProductName, etProductone, etProducttwo, etProductNameOther, etAddress,
            etCity, etPincode, etOccupation, etSourceOfLead, etOther, etBank, etBranchCode, etNoOfFamilyNumbers, etBranchName, etZccSupportRequried, etPreferredLang,
            etPreferredDate, etNotes, etStatus, etEducation, etSubStatus, etIncome, etPremiumExpected, etNextFollowupDate, etFirstAppointmentDate, etConversionPropensity,
            etProposalNumber, etLeadCreatedDate, etMarriedStatus;
    private ArrayList<String> stringArrayList;
    private BranchesModel branchesModel = new BranchesModel();
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;
    //private CountryCodeAdapter itemArrayAdapter;
    private CountryCodeModel countryCodeModel;
    List<CountryCodeModel> countryCodeModelslist = new ArrayList<CountryCodeModel>();
    private AutoCompleteTextView fCountrycode;
    //private AppCompatSpinner fCountrycode;
    //private EditText fCountrycode;
    List<String> countryNamelist;
    private ArrayAdapter<String> custadapter;
    // private ArrayAdapter<CountryCodeModel> custadapter;
    private String countryName = "";
    private String countryCode = "";
    private boolean countryListValid = false;
    private boolean isFinishBranches = false;
    private boolean isfromCalendar = false;


    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                etNextFollowupDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etNextFollowupDate.setText(visitdt);
                etNextFollowupDate.setEnabled(true);
            }
            if (customSelector == 1) {
                etFirstAppointmentDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etFirstAppointmentDate.setText(visitdt);
                etFirstAppointmentDate.setEnabled(true);
            }
            if (customSelector == 3) {
                etPreferredDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etPreferredDate.setText(visitdt);
                etPreferredDate.setEnabled(true);
            }
        }

        @Override
        public void onDateTimeCancel() {
            etNextFollowupDate.setEnabled(true);
            etFirstAppointmentDate.setEnabled(true);
            etPreferredDate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_lead);
        mgr = LocalBroadcastManager.getInstance(this);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("customer")) {
            branchesModel = new Gson().fromJson(getIntent().getStringExtra("customer"), BranchesModel.class);
        }
        if (SharedPreferenceManager.getInstance().getString(Constants.STAGES_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                OpportunityIntentService.insertOpportunityStages(CreateNewLeadActivity.this);
            }
        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("finishBranches")) {
            isFinishBranches = getIntent().getExtras().getBoolean("finishBranches");


        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("fromCalendar")) {
            isfromCalendar = getIntent().getExtras().getBoolean("fromCalendar");


        }


        // itemArrayAdapter = new CountryCodeAdapter(getApplicationContext(), R.layout.country_code_items);

        InputStream inputStream = getResources().openRawResource(R.raw.country_codes);  //country code
        CSVFile csvFile = new CSVFile(inputStream);
        List scoreList = csvFile.read();


        for (Object scoreData : scoreList) {

            String[] s = (String[]) scoreData;

            String code = s[0];
            String cName = s[1];
            String country = s[0] + " " + s[1];
            countryCodeModel = new CountryCodeModel();
            countryCodeModel.setCountryName(cName);
            countryCodeModel.setCountryCode(code);
            countryCodeModel.setCountry(country);
            Log.i("shravan", "CountryName = = " + cName);
            Log.i("shravan", "CountryCode = = " + code);
            Log.i("shravan", "CountryCodeandName = = " + s[0] + " " + s[1]);

         /*   int numberOfItems = s.length;
            for (int i=0; i<numberOfItems-1; i++)
            {
                String code =s[0];
                String cName =s[1];
                String country = s[1]+s[0];

                countryCodeModel.setCountryName(cName);
                countryCodeModel.setCountryCode(code);
                countryCodeModel.setCountry(country);
                Log.i("shravan","CountryName = = "+cName);
                Log.i("shravan","CountryCode = = "+code);
                Log.i("shravan","CountryCodeandName = = "+s[1]+s[0]);

            }*/

            countryCodeModelslist.add(countryCodeModel);

        }
        countryNamelist = new ArrayList<>();

        for (CountryCodeModel s : countryCodeModelslist) {


            String name = s.getCountry();

            countryNamelist.add(name);

        }


        Log.i("shravan", "countryCodeModelslist = = " + countryCodeModelslist.size());

        Log.i("shravan", "countryNamelist = = " + countryNamelist.size());


        initilizeViews();
        initToolBar();
        setListener();
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoMediumTypeface,
                (TextView) findViewById(R.id.personalinfo_lbl), (TextView) findViewById(R.id.pastinfo_lbl),
                (TextView) findViewById(R.id.otherinfo_lbl), (TextView) findViewById(R.id.bankinfo_lbl),
                (TextView) findViewById(R.id.zccinfo_lbl), (TextView) findViewById(R.id.updateinfo_lbl));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("customer_broadcast"));
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("oppstage_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.order_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            createLead();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setListener() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                final List<BranchesModel> customerModelList = BranchesMgr.getInstance(CreateNewLeadActivity.this).getonlineCustomerList(groupId, userid, "");
//                if (customerModelList.size() != 0) {
//                    List<BranchesModel> customerModelArrayList = new ArrayList<>();
//                    for (int i = 0; i < customerModelList.size(); i++) {
//                        customerModelArrayList.add(customerModelList.get(i));
//                    }
//                }
            }
        };
        etExistingCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etExistingCustomer.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Yes");
                stringArrayList.add("No");
                bottomSheetDialogforExistingCust(((EditText) findViewById(R.id.et_existing_sud_ife_customer)), stringArrayList, "Are You Existing SUD Life customer?");

            }
        });
        etMarriedStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Yes");
                stringArrayList.add("No");
                bottomSheetDialog(((EditText) findViewById(R.id.et_married_status)), stringArrayList, "Married (Yes/No)?");
            }
        });
        etSourceOfLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<StaticDataDto> staticList = new StaticDataMgr(CreateNewLeadActivity.this).getStaticList(groupId, "leadsource");
                if (staticList.size() != 0) {
                    etSourceOfLead.setFocusableInTouchMode(false);
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i < staticList.size(); i++) {
                        strings.add(staticList.get(i).getName());
                    }
                    bottomSheetDialog(((EditText) findViewById(R.id.et_source_of_lead)), strings, "Select Source Of Lead");
                }
            }
        });
        etNoOfFamilyNumbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringArrayList = new ArrayList<>();
                for (int i = 1; i <= 10; i++) {
                    stringArrayList.add(String.valueOf(i));
                }
                bottomSheetDialog(((EditText) findViewById(R.id.et_no_of_family_members)), stringArrayList, "Select No Of Family Members");
            }
        });
        etZccSupportRequried.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etZccSupportRequried.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Yes");
                stringArrayList.add("No");
                bottomSheetDialogforExistingCust(((EditText) findViewById(R.id.et_zcc_support_requried)), stringArrayList, "ZCC Suport Required");
            }
        });
        etPreferredLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPreferredLang.setFocusableInTouchMode(false);
                List<StaticDataDto> staticList = new StaticDataMgr(CreateNewLeadActivity.this).getStaticList(groupId, "preferredlanguage");
                if (staticList.size() != 0) {
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i < staticList.size(); i++) {
                        strings.add(staticList.get(i).getName());
                    }
                    bottomSheetDialog(((EditText) findViewById(R.id.et_preferred_language)), strings, "Select Preferred Langugae");
                }
            }
        });
        etIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<StaticDataDto> staticList = new StaticDataMgr(CreateNewLeadActivity.this).getStaticList(groupId, "incomeband");
                if (staticList.size() != 0) {
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i < staticList.size(); i++) {
                        strings.add(staticList.get(i).getName());
                    }
                    bottomSheetDialog(((EditText) findViewById(R.id.et_income)), strings, "Select Income Band");

                }
            }
        });
        etNotes.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_notes) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        etNextFollowupDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNextFollowupDate.setEnabled(false);
                etNextFollowupDate.setFocusableInTouchMode(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 10);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
        etFirstAppointmentDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFirstAppointmentDate.setEnabled(false);
                etFirstAppointmentDate.setFocusableInTouchMode(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 10);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });

        etPreferredDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPreferredDate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 10);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(03)
                        .build()
                        .show();
            }
        });

        String currentDate = TimeUtils.getCurrentDate("dd/MM/yyyy hh:mm a");
        etLeadCreatedDate.setText(currentDate);
        etFirstAppointmentDate.setText(currentDate);
        etSubStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Appointment Fixed");
                stringArrayList.add("Meeting Done");
                stringArrayList.add("Converted");
                stringArrayList.add("Not Interested");
//                bottomSheetDialogForSubStatus(((EditText) findViewById(R.id.et_sub_status)), stringArrayList, "Select Sub-Status");
            }
        });
        etConversionPropensity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etConversionPropensity.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("RED");
                stringArrayList.add("AMBER");
                stringArrayList.add("GREEN");
                bottomSheetDialog(((EditText) findViewById(R.id.et_conversion_propensity)), stringArrayList, "Select Conversion propensity");
            }
        });
        etProductName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etProductName.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(CreateNewLeadActivity.this).getProductList(groupId);
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(etProductName, stringArrayList, "Select Product");
                }
            }
        });
        etProductone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etProductone.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(CreateNewLeadActivity.this).getProductList(groupId);
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(etProductone, stringArrayList, "Select Product");
                }
            }
        });
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Create Lead");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    public void bottomSheetDialog(final EditText editText, ArrayList<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(CreateNewLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(CreateNewLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Others")) {
                    tiLayoutothers.setVisibility(View.VISIBLE);
                } else {
                    tiLayoutothers.setVisibility(View.GONE);
                }
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Walk-in")) {
                    showpopup("Visit");
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Lead Generation Activity")) {
                    showpopup("Activity");
                } else {
                    et_campaigndate.setText("");
                }
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Met")) {
//                    tiLayoutSubStatus.setVisibility(View.VISIBLE);
                    ((ScrollView) findViewById(R.id.scroll_view)).fullScroll(ScrollView.FOCUS_DOWN);
                } else {
                    tiLayoutSubStatus.setVisibility(View.GONE);
                }
                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    public void showpopup(String activitytype) {
        myList = new ArrayList<String>();
        myList = PlannerMgr.getInstance(CreateNewLeadActivity.this).getPlannerDatesListByBranches(userid, branchesModel.getCustomername(), activitytype, Integer.parseInt(TimeUtils.getCurrentDate("MM")), Integer.parseInt(TimeUtils.getCurrentDate("yyyy")));
        myList.add(0, TimeUtils.getCurrentDate("dd/MM/yyyy"));
        alertDialog = new AlertDialog.Builder(CreateNewLeadActivity.this);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CreateNewLeadActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.layout_popup, null);

        alertDialog.setView(convertView);
        ListView lv = (ListView) convertView.findViewById(R.id.list);
        final AlertDialog alert = alertDialog.create();
        alert.setTitle(" Select Visit Date:");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, myList);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
                et_campaigndate.setText(arg0.getItemAtPosition(position).toString());
                alert.cancel();
            }
        });
        alert.show();
    }

    public void bottomSheetDialogforExistingCust(final EditText editText, ArrayList<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(CreateNewLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(CreateNewLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                if (editText.getId() == R.id.et_existing_sud_ife_customer) {
                    if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("No")) {
                        etPremiumPaid.setText("");
                        findViewById(R.id.et_premium_paid_lbl).setVisibility(View.GONE);
                        findViewById(R.id.et_product_name_lbl).setVisibility(View.GONE);
                        etPremiumPaid.setVisibility(View.GONE);
                        etProductName.setText("");
                        etProductName.setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.et_premium_paid_lbl).setVisibility(View.VISIBLE);
                        findViewById(R.id.et_product_name_lbl).setVisibility(View.VISIBLE);
                        etPremiumPaid.setVisibility(View.VISIBLE);
                        etProductName.setVisibility(View.VISIBLE);
                    }
                } else if (editText.getId() == R.id.et_zcc_support_requried) {
                    if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("No")) {
                        etPreferredLang.setText("");
                        findViewById(R.id.et_preferred_language_lbl).setVisibility(View.GONE);
                        findViewById(R.id.et_preferred_Date_lbl).setVisibility(View.GONE);
                        etPreferredLang.setVisibility(View.GONE);
                        etPreferredDate.setText("");
                        etPreferredDate.setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.et_preferred_language_lbl).setVisibility(View.VISIBLE);
                        findViewById(R.id.et_preferred_Date_lbl).setVisibility(View.VISIBLE);
                        etPreferredLang.setVisibility(View.VISIBLE);
                        etPreferredDate.setVisibility(View.VISIBLE);
                    }
                }
                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    public void initilizeViews() {
        tiLayoutothers = (TextInputLayout) findViewById(R.id.tilayout_other);
        tiLayoutSubStatus = (TextInputLayout) findViewById(R.id.til_sub_status);
        etfName = (EditText) findViewById(R.id.et_fname);
        etlName = (EditText) findViewById(R.id.et_lname);
        chk_NRI = (CheckBox) findViewById(R.id.checkbox_nri);
        etContactNumber = (EditText) findViewById(R.id.et_contact_number);
        etEmailId = (EditText) findViewById(R.id.et_email_id);
        etAlternativeContactNo = (EditText) findViewById(R.id.et_alt_contactno);
        etAge = (EditText) findViewById(R.id.et_age);
        etExistingCustomer = (EditText) findViewById(R.id.et_existing_sud_ife_customer);
        etMarriedStatus = (EditText) findViewById(R.id.et_married_status);
        etPremiumPaid = (EditText) findViewById(R.id.et_premium_paid);
        etProductName = (EditText) findViewById(R.id.et_product_name);
        etProductone = (EditText) findViewById(R.id.et_product_one);
//        etProducttwo = (EditText) findViewById(R.id.et_product_two);
        etProductNameOther = (EditText) findViewById(R.id.et_other_product_name);
        etAddress = (EditText) findViewById(R.id.et_address);
        etCity = (EditText) findViewById(R.id.et_city);
        etPincode = (EditText) findViewById(R.id.et_pincode);
        etOccupation = (EditText) findViewById(R.id.et_occupation);
        etSourceOfLead = (EditText) findViewById(R.id.et_source_of_lead);
        etOther = (EditText) findViewById(R.id.et_other);
        etBank = (EditText) findViewById(R.id.et_bank);
        etBranchCode = (EditText) findViewById(R.id.et_branch_code);
        et_campaigndate = (EditText) findViewById(R.id.et_campaigndate);
        etNoOfFamilyNumbers = (EditText) findViewById(R.id.et_no_of_family_members);
        etBranchName = (EditText) findViewById(R.id.et_branch_name);
        etZccSupportRequried = (EditText) findViewById(R.id.et_zcc_support_requried);
        etPreferredLang = (EditText) findViewById(R.id.et_preferred_language);
        etPreferredDate = (EditText) findViewById(R.id.et_preferred_Date);
        etLeadStage = (EditText) findViewById(R.id.et_lead_status);
        etStatus = (EditText) findViewById(R.id.et_metting_status);
        etSubStatus = (EditText) findViewById(R.id.et_sub_status);
        etPremiumExpected = (EditText) findViewById(R.id.et_expected_actpremium);
        etNextFollowupDate = (EditText) findViewById(R.id.et_next_followup_date);
        etFirstAppointmentDate = (EditText) findViewById(R.id.et_first_appointment_date);
        etConversionPropensity = (EditText) findViewById(R.id.et_conversion_propensity);
        etProposalNumber = (EditText) findViewById(R.id.et_proposal_no);
        etLeadCreatedDate = (EditText) findViewById(R.id.et_lead_created_date);
        etEducation = (EditText) findViewById(R.id.et_education);
        etIncome = (EditText) findViewById(R.id.et_income);
        etNotes = (EditText) findViewById(R.id.et_notes);
        //country code
        fCountrycode = (AutoCompleteTextView) findViewById(R.id.fCountrycode);

        fCountrycode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {


                String selection = (String) adapterView.getItemAtPosition(pos);
                int position = -1;

                for (int i = 0; i < countryNamelist.size(); i++) {
                    if (countryNamelist.get(i).equals(selection)) {
                        position = i;
                        break;
                    }
                }
                System.out.println("Position " + position); //check it now in Logcat

                //CountryCodeModel c = (CountryCodeModel) adapterView.getAdapter().getItem(pos);
                CountryCodeModel c = countryCodeModelslist.get(position);
                countryName = c.getCountryName();
                countryCode = c.getCountryCode();
                Log.i("shravan", " adapter selected pos---" + position);
                Log.i("shravan", " adapter selected cname---" + countryName);
                Log.i("shravan", " adapter selected ccode---" + countryCode);
            }
        });
        //fCountrycode = (AppCompatSpinner) findViewById(R.id.fCountrycode);
        fCountrycode.setVisibility(View.GONE);
        setCountryCodeAdapter();
        //fCountrycode = (EditText) findViewById(R.id.fCountrycode);
        //chk_NRI.setOnCheckedChangeListener();
        etContactNumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});

        chk_NRI.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {

                    fCountrycode.setVisibility(View.VISIBLE);
                    etContactNumber.setText("");
                    etContactNumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(15)});
                    //Case 1
                } else {
                    fCountrycode.setVisibility(View.GONE);
                    etContactNumber.setText("");
                    etContactNumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});

                }
                //case 2

            }
        });


        if (branchesModel != null) {
            if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomername())) {
                etBranchName.setText(branchesModel.getCustomername());
            }
            if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomertype())) {
                etBank.setText(branchesModel.getCustomertype());
            }
            if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomercode())) {
                etBranchCode.setText(branchesModel.getCustomercode());
            } else {
                etBranchCode.setText("");
            }
        }
        etLeadStage.setText("Open");


    }


    InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; ++i) {
                if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                    return "";
                }
            }

            return null;
        }
    };

    private void setCountryCodeAdapter() {

        custadapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, countryNamelist);
        //custadapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, countryCodeModelslist);
        //custadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //CountryCodeAdapter custadapter = new CountryCodeAdapter(this, countryCodeModelslist);
        fCountrycode.setThreshold(1);

        // fCountrycode.setSelection(0);
        fCountrycode.setAdapter(custadapter);


    }


    private void validateAllFields() {

        if (!Utils.isNotNullAndNotEmpty(etfName.getText().toString())) {
            etfName.setError("Name is mandatory");
            etfName.requestFocus();
        } else if (etEmailId.getText().toString().length() != 0 && !Utils.isValidEmail(etEmailId.getText().toString().trim())) {
            etEmailId.setError("Pleae Enter Valid Email");
            etEmailId.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etAge.getText().toString())) {
            etAge.setError("Age is mandatory");
            etAge.requestFocus();
        } else if (Integer.parseInt(etAge.getText().toString()) > 120) {
            etAge.setError("Age should be below 120");
            etAge.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etAddress.getText().toString())) {
            etAddress.setError("Address is mandatory");
            etAddress.requestFocus();
        } else if (etAddress.getText().toString().length() < 5) {
            etAddress.setError("Enter valid Address");
            etAddress.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etCity.getText().toString())) {
            etCity.setError("City is mandatory");
            etCity.requestFocus();
        } else if (etCity.getText().toString().length() < 3) {
            etCity.setError("Enter valid City Name");
            etCity.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etPincode.getText().toString())) {
            etPincode.setError("PinCode is mandatory");
            etPincode.requestFocus();
        } else if (etPincode.getText().toString().length() != 6) {
            etPincode.setError("Enter valid PinCode");
            etPincode.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etExistingCustomer.getText().toString())) {
            etExistingCustomer.setFocusableInTouchMode(true);
            etExistingCustomer.setError("Please Select Existing or Not");
            etExistingCustomer.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etSourceOfLead.getText().toString())) {
            etSourceOfLead.setFocusableInTouchMode(true);
            etSourceOfLead.setError("Source of Lead is mandatory");
            etSourceOfLead.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(etLeadStage.getText().toString())) {
            etLeadStage.setFocusableInTouchMode(true);
            etLeadStage.setError("Please Select Lead Stage");
            etLeadStage.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Converted") && !Utils.isNotNullAndNotEmpty(etProposalNumber.getText().toString())) {
            etProposalNumber.setError("Please Enter Proposal Number");
            etProposalNumber.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Not interested") && !Utils.isNotNullAndNotEmpty(etNotes.getText().toString())) {
            etNotes.setError("Please Specify Reason");
            etNotes.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Contacted & meeting fixed") && !Utils.isNotNullAndNotEmpty(etFirstAppointmentDate.getText().toString())) {
            etFirstAppointmentDate.setFocusableInTouchMode(true);
            etFirstAppointmentDate.setError("Please Select First Appointment Date");
            etFirstAppointmentDate.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(etNextFollowupDate.getText().toString())) {
            etNextFollowupDate.setFocusableInTouchMode(true);
            etNextFollowupDate.setError("Please Select Next Appointment Date");
            etNextFollowupDate.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(etPremiumExpected.getText().toString())) {
            etPremiumExpected.setError("Please Specify Expected Premium");
            etPremiumExpected.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(etProductone.getText().toString())) {
            etProductone.setFocusableInTouchMode(true);
            etProductone.setError("Please Select a Product");
            etProductone.requestFocus();
        } else if (etLeadStage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(etConversionPropensity.getText().toString())) {
            etConversionPropensity.setFocusableInTouchMode(true);
            etConversionPropensity.setError("Please Select Convertion Propensity");
            etConversionPropensity.requestFocus();
        } else {
            List<LeadModel> leadswithContactNolist = LeadDao.getInstance(CreateNewLeadActivity.this).getLeadwithContactNo(userid, etContactNumber.getText().toString());
            if (leadswithContactNolist.size() > 0) {
                showAlert("", "Lead with this contact no. is in process. So you cannot create another lead.",
                        "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                etContactNumber.requestFocus();
                            }
                        },
                        "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        }, false);
            } else {
                List<LeadModel> convertedLeadwithContactNolist = LeadDao.getInstance(CreateNewLeadActivity.this).getConvertedLeadwithContactNo(userid, etContactNumber.getText().toString());
                if (convertedLeadwithContactNolist.size() > 0) {
                    showAlert("", "This Contact no. is an Existing SUD Life customer. Do you want to proceed?..",
                            "OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    etExistingCustomer.setText("yes");
                                    postLeadData();
                                }
                            },
                            "Cancel",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                }
                            }, true);
                } else {
                    postLeadData();
                }
            }

        }


    }


    private boolean getIsValidCountry() {


        ///List<String> countryNames = new ArrayList<>();


        for (int i = 0; i < countryNamelist.size(); i++) {

            if (countryNamelist.get(i).toString().equalsIgnoreCase(fCountrycode.getText().toString())) {

                countryListValid = true;
                break;

            } else {
                countryListValid = false;

            }

        }


        return countryListValid;
    }


    public void createLead() {
        // If Leadstatus(stage) is converted, Proposal number is mandatory
        //If zcc support is required, Language is mandatory


        if (chk_NRI.isChecked()) {

            // etContactNumber.setError(null);

            if ((fCountrycode.getText().toString().trim()).equalsIgnoreCase("+91 India")) {


                if (etContactNumber.getText().length() != 10) {
                    etContactNumber.setError("Contact number must be 10 digits for this country");
                    etContactNumber.requestFocus();
                    //etContactNumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});

                } else {


                    boolean validnri = isValidPhoneNri(etContactNumber.getText().toString());

                    if (!validnri) {
                        etContactNumber.setError("Contact number must be 8 to 15 digits");
                        etContactNumber.requestFocus();
                    } else {

                        if (getIsValidCountry()) {
                            validateAllFields();
                        } else {
                            fCountrycode.setError("Invalid Country name. Please select from the dropdown list.");
                            fCountrycode.requestFocus();

                        }
                    }

                }


            } else {

                // etContactNumber.setError(null);

                if (getIsValidCountry()) {
                    boolean valid = isValidPhoneNri(etContactNumber.getText().toString());

                    if (!valid) {
                        etContactNumber.setError("Contact number must be 8 to 15 digits");
                        etContactNumber.requestFocus();

                    } else {
                        validateAllFields();

                    }

                } else {
                    fCountrycode.setError("Invalid Country name. Please select from the dropdown list.");
                    fCountrycode.requestFocus();

                }


            }


        } else {


            boolean valid = isValidPhoneNew(etContactNumber.getText().toString());

            if (!valid) {
                etContactNumber.setError("Contact number must be 10 digits");
                etContactNumber.requestFocus();

            } else {
                validateAllFields();

            }

            // Log.i("Activity","phoneNo valid===="+valid);


        }


    }

    private void postLeadData() {
        LeadModel leadModel = new LeadModel();
        leadModel.setUserid(userid);
        leadModel.setGroupid(groupId);
        leadModel.setFirstname(etfName.getText().toString());
        leadModel.setLastname(etlName.getText().toString());
        leadModel.setIsnri(String.valueOf(chk_NRI.isChecked()));
        Log.e("nri value ", String.valueOf(chk_NRI.isChecked()));
       /* String country = fCountrycode.getSelectedItem().toString();//country code
        Log.i("shravan ", "country = = = "+country);*/
        leadModel.setCountryname(countryName);//country code
        leadModel.setCountrycode(countryCode);//country code
        leadModel.setContactno(etContactNumber.getText().toString());
        leadModel.setEmail(etEmailId.getText().toString());
        leadModel.setAlternatecontactno(etAlternativeContactNo.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etAge.getText().toString()))
            leadModel.setAge(Integer.parseInt(etAge.getText().toString()));
        if (Utils.isNotNullAndNotEmpty(et_campaigndate.getText().toString()))
            leadModel.setCampaigndate(et_campaigndate.getText().toString());
        leadModel.setEducation(etEducation.getText().toString());
        leadModel.setOccupation(etOccupation.getText().toString());
        leadModel.setIncomeband(etIncome.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etNoOfFamilyNumbers.getText().toString()))
            leadModel.setFamilymemberscount(Integer.parseInt(etNoOfFamilyNumbers.getText().toString()));
        if (Utils.isNotNullAndNotEmpty(etMarriedStatus.getText().toString()))
            if (etMarriedStatus.getText().toString().equalsIgnoreCase("yes")) {
                leadModel.setIsmarried(true);
            } else {
                leadModel.setIsmarried(false);
            }
        if (Utils.isNotNullAndNotEmpty(etNotes.getText().toString()))
            leadModel.setRemarks(etNotes.getText().toString());
        leadModel.setAddress(etAddress.getText().toString());
        leadModel.setCity(etCity.getText().toString());
        leadModel.setPincode(etPincode.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etExistingCustomer.getText().toString()))
            if (etExistingCustomer.getText().toString().equalsIgnoreCase("yes")) {
                leadModel.setIsexistingcustomer(true);
            } else {
                leadModel.setIsexistingcustomer(false);

            }
        if (Utils.isNotNullAndNotEmpty(etPremiumPaid.getText().toString()))
            leadModel.setPremiumpaid(Integer.parseInt(etPremiumPaid.getText().toString()));
        leadModel.setProductname(etProductName.getText().toString());
        leadModel.setLeadsource(etSourceOfLead.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etOther.getText().toString()))
            leadModel.setOtherleadsource(etOther.getText().toString());
        leadModel.setBank(etBank.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etBranchCode.getText().toString())) {
            leadModel.setBankbranchcode(etBranchCode.getText().toString());
        } else {
            leadModel.setBankbranchcode("");
        }
        leadModel.setBankbranchname(etBranchName.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etZccSupportRequried.getText().toString()))
            if (etZccSupportRequried.getText().toString().equalsIgnoreCase("yes")) {
                leadModel.setZccsupportrequired(true);
            } else {
                leadModel.setZccsupportrequired(false);
            }
        leadModel.setPreferredlanguage(etPreferredLang.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etPreferredDate.getText().toString()))
            leadModel.setPreferreddatetime(Integer.parseInt(TimeUtils.convertDatetoUnix(etPreferredDate.getText().toString())));
        leadModel.setStatus(etStatus.getText().toString());
        leadModel.setLeadstage(etLeadStage.getText().toString());
        leadModel.setProduct1(etProductone.getText().toString());
//            leadModel.setProduct2(etProducttwo.getText().toString());
        leadModel.setSubstatus(etSubStatus.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etPremiumExpected.getText().toString()))
            leadModel.setExpectedpremium(etPremiumExpected.getText().toString());
        leadModel.setConversionpropensity(etConversionPropensity.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etProposalNumber.getText().toString()))
            leadModel.setProposalnumber(etProposalNumber.getText().toString());
        if (Utils.isNotNullAndNotEmpty(etLeadCreatedDate.getText().toString()))
            leadModel.setLeadcreatedon(Integer.parseInt(TimeUtils.convertDatetoUnix(etLeadCreatedDate.getText().toString())));
//            if (Utils.isNotNullAndNotEmpty(etFirstAppointmentDate.getText().toString()))
//                leadModel.setFirstappointmentdate(Integer.parseInt(TimeUtils.convertDatetoUnix(etFirstAppointmentDate.getText().toString())));
        if (Utils.isNotNullAndNotEmpty(etNextFollowupDate.getText().toString()))
            leadModel.setNextfollowupdate(Integer.parseInt(TimeUtils.convertDatetoUnix(etNextFollowupDate.getText().toString())));
        LeadMgr.getInstance(CreateNewLeadActivity.this).insertLocalLead(leadModel, "offline");
        LeadIntentService.syncLeadstoServer(CreateNewLeadActivity.this);
        showAlert("", "Your Lead is saved successfully",
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (isFinishBranches) {


                            if (isfromCalendar) {
                                ListCustomerActivity.listCustomerActivity.finish();
                                ListCustomerActivity.listCustomerActivity = null;
                                startActivity(new Intent(CreateNewLeadActivity.this, LeadsListActivity.class));


                            } else {

                                ListCustomerActivity.listCustomerActivity.finish();
                                ListCustomerActivity.listCustomerActivity = null;


                            }

                        }


                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);
    }
}
