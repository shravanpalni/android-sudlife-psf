package com.outwork.sudlife.psf.planner.service;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.targets.services.TargetsIntentService;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class PlannerIntentService extends JobIntentService {
    public static final String TAG = PlannerIntentService.class.getSimpleName();

    public static final String ACTION_GET_PLAN = "com.outwork.sudlife.lite.planner.service.action.GET_PLAN";
    public static final String ACTION_POST_PLAN = "com.outwork.sudlife.lite.planner.service.action.POST_PLAN";
    public static final String ACTION_PLAN_OFFLINE_SYNC = "com.outwork.sudlife.lite.planner.service.action.TOUR_OFFLINE_SYNC";
    public static final String ACTION_UPDATE_PLAN = "com.outwork.sudlife.lite.planner.service.action.UPDATE_PLAN";

    public static final String PLAN_MODEL = "com.outwork.sudlife.lite.planner.service.extra.PLAN_MODEL";
    public static final String MONTH = "com.outwork.sudlife.lite.planner.service.extra.MONTH";
    public static final String YEAR = "com.outwork.sudlife.lite.planner.service.extra.YEAR";
    private static final Integer JOBID = 1004;
    private LocalBroadcastManager mgr;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_PLAN.equals(action)) {
                final String month = intent.getStringExtra(MONTH);
                final String year = intent.getStringExtra(YEAR);
                getServerPlannerList(month, year);
            }
            if (ACTION_POST_PLAN.equals(action)) {
                final String plannerModel = intent.getStringExtra(PLAN_MODEL);
                postPlan(new Gson().fromJson(plannerModel, PlannerModel.class));
            }
            if (ACTION_UPDATE_PLAN.equals(action)) {
                final String plannerModel = intent.getStringExtra(PLAN_MODEL);
                updatePlannedPlan(new Gson().fromJson(plannerModel, PlannerModel.class));
            }
            if (ACTION_PLAN_OFFLINE_SYNC.equals(action)) {
                syncOfflinePlans();
            }
        }
    }

    public static void insertPlanList(Context context, String month, String year) {
        Intent intent = new Intent(context, PlannerIntentService.class);
        intent.setAction(ACTION_GET_PLAN);
        intent.putExtra(MONTH, month);
        intent.putExtra(YEAR, year);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, PlannerIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public static void syncPlanstoServer(Context context) {
        Intent intent = new Intent(context, PlannerIntentService.class);
        intent.setAction(ACTION_PLAN_OFFLINE_SYNC);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, PlannerIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    private void syncOfflinePlans() {
        final List<PlannerModel> offlineplannerList = PlannerMgr.getInstance(PlannerIntentService.this).getOfflineplannerList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");
        if (offlineplannerList.size() > 0) {
            for (final PlannerModel plannerModel : offlineplannerList) {
                if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status())) {
                    if (isNetworkAvailable()) {
                        if (!Utils.isNotNullAndNotEmpty(plannerModel.getActivityid())) {
                            postPlan(plannerModel);
                        } else {
                            if (plannerModel.getCompletestatus() == 0) {
                                updatePlannedPlan(plannerModel);
                            } else if (plannerModel.getCompletestatus() == 1) {
                                updateCheckin(plannerModel);
                            } else if (plannerModel.getCompletestatus() == 2) {
                                updateCheckout(plannerModel);
                            } else if (plannerModel.getCompletestatus() == 3) {
                                updateReschedule(plannerModel);
                            }
                        }
                    }
                }
            }
        }
    }

    private void postPlan(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> postPlan = client.postPlan(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel);
        try {
            Response<RestResponse> restResponse = postPlan.execute();
            int statusCode = restResponse.code();
            String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                            PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerID(String.valueOf(plannerModel.getLid()), userid, restResponse.body().getData());
                        } else {
                            PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerdummyOnline(String.valueOf(plannerModel.getLid()), userid);
                        }
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateCheckout(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.checkout(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerStatus(String.valueOf(plannerModel.getLid()), userid, "2");
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                TargetsIntentService.insertTargetRecords(PlannerIntentService.this);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateReschedule(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.reschdule(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerStatus(String.valueOf(plannerModel.getLid()), userid, "3");
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateCheckin(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.checkin(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerStatus(String.valueOf(plannerModel.getLid()), userid, "1");
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updatePlannedPlan(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.updatePlan(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerID(String.valueOf(plannerModel.getLid()), userid, restResponse.body().getData());
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateApproval(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.updateApprove(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), String.valueOf(plannerModel.getPlanstatus()));
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            if (Utils.isNotNullAndNotEmpty(restResponse.body().toString())) {
                int statusCode = restResponse.code();
                if (statusCode == 200) {
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                        if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                            String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                            Intent intent = new Intent("plan_broadcast");
                            mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                            mgr.sendBroadcast(intent);
                        }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getServerPlannerList(final String month, String year) {
        String lastfetchtime = SharedPreferenceManager.getInstance().getString(Constants.PLANNER_LAST_FETCH_TIME, "");
        List<PlannerModel> plannerModelList = PlannerMgr.getInstance(PlannerIntentService.this).getOfflineplannerList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "");
        if (plannerModelList.size() == 0) {
            lastfetchtime = "";
        }
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> getplans = client.getPlans(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), month, year, lastfetchtime);
        final String newFetchTime = TimeUtils.getCurrentUnixTimeStamp();
        try {
            Response<RestResponse> restResponse = getplans.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                try {
                    if (!TextUtils.isEmpty(restResponse.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PlannerModel>>() {
                        }.getType();
                        List<PlannerModel> plannerModelList1 = new Gson().fromJson(restResponse.body().getData(), listType);
                        if (plannerModelList1.size() > 0) {
                            PlannerMgr.getInstance(PlannerIntentService.this).insertPlannerList(plannerModelList1, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
                        }
                        LeadIntentService.insertLeadstoServer(PlannerIntentService.this);
                        SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LOADED, "loaded");
                        SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LAST_FETCH_TIME, newFetchTime);
                        TargetsIntentService.insertTargetRecords(PlannerIntentService.this);
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LOADED, "notloaded");
                }
            }
        } catch (IOException e) {
            SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LOADED, "notloaded");
            e.printStackTrace();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }
}