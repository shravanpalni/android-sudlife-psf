package com.outwork.sudlife.psf.targets.models;

import java.util.ArrayList;

/**
 * Created by bvlbh on 3/31/2018.
 */

public class TargetSummaryModel {


    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }

    public String getAchievedValue() {
        return achievedValue;
    }

    public void setAchievedValue(String achievedValue) {
        this.achievedValue = achievedValue;
    }

    private String branchName;
    private String totalValue;
    private String achievedValue;
    private String summaryName;
    private String achievedConnection;

    public String getAchievedConnection() {
        return achievedConnection;
    }

    public void setAchievedConnection(String achievedConnection) {
        this.achievedConnection = achievedConnection;
    }

    public String getAchievedBusiness() {
        return achievedBusiness;
    }

    public void setAchievedBusiness(String achievedBusiness) {
        this.achievedBusiness = achievedBusiness;
    }

    public String getAchievedLeads() {
        return achievedLeads;
    }

    public void setAchievedLeads(String achievedLeads) {
        this.achievedLeads = achievedLeads;
    }

    private String achievedBusiness;
    private String achievedLeads;
    private String overallTotal;

    public String getSummaryName() {
        return summaryName;
    }

    public void setSummaryName(String summaryName) {
        this.summaryName = summaryName;
    }

    public String getOverallTotal() {
        return overallTotal;
    }

    public void setOverallTotal(String overallTotal) {
        this.overallTotal = overallTotal;
    }

    public String getOverallAchieved() {
        return overallAchieved;
    }

    public void setOverallAchieved(String overallAchieved) {
        this.overallAchieved = overallAchieved;
    }

    public ArrayList<TargetSummaryModel> getTargetSummaryModelArrayList() {
        return targetSummaryModelArrayList;
    }

    public void setTargetSummaryModelArrayList(ArrayList<TargetSummaryModel> targetSummaryModelArrayList) {
        this.targetSummaryModelArrayList = targetSummaryModelArrayList;
    }

    private ArrayList<TargetSummaryModel> targetSummaryModelArrayList;

    private String overallAchieved;
}
