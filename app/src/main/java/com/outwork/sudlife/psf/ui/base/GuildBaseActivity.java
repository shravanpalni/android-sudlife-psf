package com.outwork.sudlife.psf.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.ui.activities.DashBoardActivity;

public class GuildBaseActivity extends CoreBaseActivity {
    private static final String TAG = GuildBaseActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private String groupName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_guild_base);
        initUI();
    }

    @Override
    public void onBackPressed() {
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() != 0) {
            fm.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    protected boolean useToolbar() {
        return true;
    }

    protected boolean useDrawerToggle() {
        return true;
    }

    private void initUI() {

        mToolbar = (Toolbar) findViewById(R.id.base_toolBar);
        setSupportActionBar(mToolbar);

        if (useToolbar()) {
            setSupportActionBar(mToolbar);
            setTitle(groupName);
        } else {
            mToolbar.setVisibility(View.GONE);
        }
        Intent intent = new Intent(GuildBaseActivity.this, DashBoardActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 191) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.base_fragment_container);
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == 165 || requestCode == 181 || requestCode == 174) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.base_fragment_container);
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
