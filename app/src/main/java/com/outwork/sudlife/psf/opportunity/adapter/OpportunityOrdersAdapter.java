package com.outwork.sudlife.psf.opportunity.adapter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.opportunity.listener.ItemClickListener;
import com.outwork.sudlife.psf.opportunity.models.OpportunityItems;

/**
 * Created by Habi on 30-06-2017.
 */
public class OpportunityOrdersAdapter extends RecyclerView.Adapter<OpportunityOrdersAdapter.OrderHolder> {

    private List<OpportunityItems> dataSet;
    Context mContext;
    private String type;
    private ItemClickListener clickListener;

    public OpportunityOrdersAdapter(Context context, List<OpportunityItems> data, String type) {
        this.dataSet = data;
        this.mContext = context;
        this.type = type;
    }

    @Override
    public OrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_product_item, parent, false);
        return new OrderHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final OrderHolder holder, final int position) {
        holder.prodname.setText(dataSet.get(position).getProduct());
        holder.quantity.setText(String.valueOf(dataSet.get(position).getQuantity()));
        holder.cost.setText(String.valueOf(dataSet.get(position).getRate()));
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equalsIgnoreCase("add")) {
                    dataSet.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, dataSet.size());
                    notifyDataSetChanged();
                } else if (type.equalsIgnoreCase("update")) {
                    if (dataSet.get(position).getOrderid() != null) {
                        if (dataSet.get(position).getOrderid() > 0) {
                            if (isNetworkAvailable()) {
                                if (clickListener != null) clickListener.onClick(v, position);
//                            OpportunityIntentService.deleteOrderOpportunity(mContext, String.valueOf(dataSet.get(position).getOpportunityid()), String.valueOf(dataSet.get(position).getOrderid()));
                            } else {
                                Toast.makeText(mContext, "No Internet connection to delete order item", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            holder.delete.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                dataSet.remove(position);
//                notifyItemRemoved(position);
//                dataSet.add(position, productModel);
//                notifyItemChanged(position);
//                notifyDataSetChanged();
            }
        });
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public class OrderHolder extends RecyclerView.ViewHolder {
        private TextView prodname, quantity, cost;
        private ImageView delete;
        private View layout;

        public OrderHolder(View view) {
            super(view);
            prodname = (TextView) view.findViewById(R.id.prodname);
            quantity = (TextView) view.findViewById(R.id.quantity);
            cost = (TextView) view.findViewById(R.id.value);
            delete = (ImageView) view.findViewById(R.id.delete);
            layout = view.findViewById(R.id.detail_layout);
            if (type.equals("add") || type.equals("update")) {
                delete.setVisibility(View.VISIBLE);
            } else {
                delete.setVisibility(View.GONE);
            }
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }
}
