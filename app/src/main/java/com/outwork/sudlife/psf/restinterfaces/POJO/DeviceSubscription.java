package com.outwork.sudlife.psf.restinterfaces.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Panch on 9/13/2016.
 */

public class DeviceSubscription {

    @SerializedName("externalid")
    @Expose
    private String externalid;

    @SerializedName("registrationid")
    @Expose
    private String registrationid;

    @SerializedName("olddevicetoken")
    @Expose
    private String olddevicetoken;

    @SerializedName("devicetype")
    @Expose
    private String devicetype;

    public String getExternalid() {
        return externalid;
    }

    public void setExternalid(String externalid) {
        this.externalid = externalid;
    }

    public String getRegistrationid() {
        return registrationid;
    }

    public void setRegistrationid(String registrationid) {
        this.registrationid = registrationid;
    }

    public String getOlddevicetoken() {
        return olddevicetoken;
    }

    public void setOlddevicetoken(String olddevicetoken) {
        this.olddevicetoken = olddevicetoken;
    }

    public String getDevicetype() {
        return devicetype;
    }

    public void setDevicetype(String devicetype) {
        this.devicetype = devicetype;
    }
}

