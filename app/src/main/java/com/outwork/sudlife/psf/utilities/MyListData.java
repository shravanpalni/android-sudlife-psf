package com.outwork.sudlife.psf.utilities;

/**
 * Created by shravanch on 10-12-2019.
 */

public class MyListData{
    private String description;

    public MyListData(String description) {
        this.description = description;

    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

}
