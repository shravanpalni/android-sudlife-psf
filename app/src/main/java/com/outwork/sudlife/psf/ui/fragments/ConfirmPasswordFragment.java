package com.outwork.sudlife.psf.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;




import org.json.JSONException;
import org.json.JSONObject;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.dto.UserDetailsDto;
import com.outwork.sudlife.psf.dto.UserObjectDto;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.restinterfaces.UserRestInterface;
import com.outwork.sudlife.psf.ui.activities.SignInActvity;
import com.outwork.sudlife.psf.ui.base.GuildBaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * Created by Panch on 1/20/2016.
 */
public class ConfirmPasswordFragment extends Fragment {

    private TextView confirmButton;
    private EditText passwordField;
    private String groupCode,loginId;
    private TextView errorText;
    private IvokoApplication application;
    private ProgressBar mProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        groupCode = getArguments().getString("groupcode","10000");
        loginId = getArguments().getString("loginid","10000");
        application = (IvokoApplication)getActivity().getApplication();



    }

    public ConfirmPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_signup_password, container, false);

        passwordField = (EditText)v.findViewById(R.id.passwordField);
        confirmButton = (TextView)v.findViewById(R.id.confirm);
        mProgressBar = (ProgressBar)v.findViewById(R.id.passwordProgress);

        errorText = (TextView)v.findViewById(R.id.errorText);

        if(passwordField.getText().toString().length()==0){
            passwordField.setError("Please enter password");
        }

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(passwordField.getText().toString().length()==0){
                    passwordField.setError("Please enter password");
                } else if (passwordField.getText().toString().length()<8){
                    passwordField.setError("Please re-enter your password. Min no of characters is 8.");
                } else {
                    final UserDetailsDto userDetails = new UserDetailsDto();
                    userDetails.setUserName(loginId);
                    userDetails.setPassword(passwordField.getText().toString().trim());
                    userDetails.setGroupCode(groupCode);
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.getIndeterminateDrawable().setColorFilter(Color.WHITE,android.graphics.PorterDuff.Mode.SRC_IN);
                    confirmButton.setText("");
                    signUpServiceCall(getActivity(), userDetails);
                }
            }
        });

        return v;
    }



    private void signUpServiceCall(Context context, final UserDetailsDto userDetailsDto) {

        UserRestInterface client = RestService.createService(UserRestInterface.class);
        Call<RestResponse> user = client.signUp(userDetailsDto.getUserName(), userDetailsDto.getPassword(), userDetailsDto.getGroupCode());
        user.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                if (response.isSuccessful()) {
                    RestResponse restResponse = response.body();
                    String status = restResponse.getStatus();
                    if (Constants.STATUS_SUCCESS.equalsIgnoreCase(status)) {
                        try {
                            JSONObject userObject = new JSONObject(restResponse.getData());
                            processResponse(userObject, userDetailsDto);
                        } catch (JSONException e) {
                            Utils.displayToast(getActivity(), "Your account has been setup. Please login");
                            Intent in = new Intent(getActivity(), SignInActvity.class);
                            startActivity(in);
                            getActivity().finish();
                            // setSigninProgress();
                        }

                        /*application.writePreference(userDetails.getUserName(), userDetails.getPassword());
                        application.generateUserDetails();
                        Intent userVerify = new Intent(getActivity(), UserVerifyActivity.class);
                        userVerify.putExtra("firstTimeRegister", true);
                        startActivity(userVerify);
                        getActivity().finish();*/
                    }
                }

            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

                Utils.displayToast(getActivity(), "Please check your network connection and try again");
                setPasswordConfirmText();

            }
        });
    }


    private void processResponse(JSONObject userObject, UserDetailsDto userDetailsDto){



        application.writePreference(userDetailsDto.getUserName(), userDetailsDto.getPassword());
        UserObjectDto userObjectDto = new UserObjectDto(getActivity());
        application.setUserObject(userObjectDto);
        UserObjectDto dto = application.getUserObject();
        try {
            dto.parse(userObject, getActivity());
        } catch (JSONException e) {
            Utils.displayToast(getActivity(), "Please check your network connection and try again");
            //setSigninProgress();
        }

        if (dto.getUserGroups().size()==0){
            Toast.makeText(getActivity(), "Please contact your administrator ", Toast.LENGTH_LONG).show();
            Intent in = new Intent(getActivity(), SignInActvity.class);
            startActivity(in);
            getActivity().finish();

        } else {

            application.setUserObject(dto);
            application.writePreference(userDetailsDto.getUserName(),userDetailsDto.getPassword());
           // nextStep(dto);
             startMainActivity();

        }


    }





    private void startMainActivity(){
      Intent in = new Intent(getActivity(), GuildBaseActivity.class);
        startActivity(in);
        getActivity().finish();
    }

    private void setPasswordConfirmText(){
        mProgressBar.setVisibility(View.INVISIBLE);
        confirmButton.setText("Confirm Password");
    }

    private void nextStep(final UserObjectDto dto){

      /*  UserLoginTask.TaskListener listener = new UserLoginTask.TaskListener() {

            @Override
            public void onSuccess(RegistrationResponse registrationResponse, Context context) {
                //After successful registration with Applozic server the callback will come here
                startMainActivity();
            }

            @Override
            public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                //If any failure in registration the callback  will come here
            }};

        User user = new User();
        user.setUserId(dto.getUserId()); //userId it can be any unique user identifier
        user.setDisplayName(dto.getFirstName()); //displayName is the name of the user which will be shown in chat messages
        user.setEmail(dto.getEmail()); //optional
        user.setImageLink("");//optional,pass your image link

        new UserLoginTask(user, listener, getActivity()).execute((Void) null);*/
    }


}