package com.outwork.sudlife.psf.lead.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.gson.Gson;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.adapters.LeadAppointmentAdapter;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ViewLeadActivity extends BaseActivity {
    private LeadModel leadModel = new LeadModel();
    private TabLayout tabLayout;
    private String teamlead;
    private RecyclerView planRecyclerView;
    private String scheduledate;
    private LeadAppointmentAdapter leadAppointmentAdapter;
    private PlannerModel plannerModel = new PlannerModel();
    private List<PlannerModel> plannerModelList = new ArrayList<>();
    private View infoView, contactView;
    private Button schedileview;
    TextView tvfName, tv_campaigndate, tvlName, etLeadStatus, isnri, tv_nri, tvContactNumber, tvEmailid, tvAltContactno, tvAge, etExistingCustomer, etPremiumPaid, etProductName, etProductNameOther, tvaddress,
            etCity, etPincode, tvOccupation, etSourceOfLead, etOther, etBank, etBranchCode, familymembers, etBranchName, etZccSupportRequried, etPreferredLang,
            etPreferredDate, tvNotes, etStatus, tvEducation, etSubStatus, tvIncome, etPremiumExpected, etNextFollowupDate, etFirstAppointmentDate, etConversionPropensity,
            etProposalNumber, etLeadCreatedDate, tvMarriedStatus, tvProduct1, tvProduct2, noMembers, leadcode, uname, bankname, branchname, toolbar_title, tvactpremium, tv_countrycode, tv_countryname, countrycode, countryname;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                schedileview.setEnabled(true);
                scheduledate = TimeUtils.convertDatetoUnix(TimeUtils.getFormattedDate(date));
                if (Utils.isNotNullAndNotEmpty(scheduledate) && Utils.isNotNullAndNotEmpty(String.valueOf(leadModel.getCreatedon())))
                    if (Integer.parseInt(scheduledate) < Integer.parseInt(TimeUtils.getCurrentUnixTimeStamp())) {
                        showToast("Appointment Date should not be less than Current Time");
                    } else {
                        showAlert("", "Your Appointment will be fixed with "
                                + leadModel.getFirstname() + " " + leadModel.getLastname() +
                                " on " + TimeUtils.getFormattedDatefromUnix(scheduledate, "dd/MM/yyyy"), "Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                postcreateplan();
                            }
                        }, "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dismissProgressDialog();
//                    finish();
                            }
                        }, true);
                    }
            }
        }

        @Override
        public void onDateTimeCancel() {
            schedileview.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_leadform);

        mgr = LocalBroadcastManager.getInstance(this);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("leadobj")) {
            leadModel = new Gson().fromJson(getIntent().getStringExtra("leadobj"), LeadModel.class);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("teamlead")) {
            teamlead = getIntent().getStringExtra("teamlead");
        }
        initToolBar();
        initializeViews();
        populateData(leadModel);
        if (Utils.isNotNullAndNotEmpty(teamlead)) {
            if (teamlead.equalsIgnoreCase("team")) {
                schedileview.setVisibility(View.GONE);
            }
        }
        setListener();
//        Snackbar snackbar = Snackbar
//                .make(findViewById(R.id.viewlead), "This is an offline record, would you like to do a resubmit", Snackbar.LENGTH_LONG)
//                .setAction("Submit", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                    }
//                });
//        // Changing message text color
//        snackbar.setActionTextColor(Color.RED);
//
//// Changing action button text color
//        View sbView = snackbar.getView();
//        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.YELLOW);
//        snackbar.show();

        Utils.setTypefaces(IvokoApplication.robotoTypeface, uname, bankname, branchname, tvfName, tv_campaigndate, tvlName, etLeadStatus, tvContactNumber, tvEmailid, tvAltContactno, tvAge, etExistingCustomer, etPremiumPaid, etProductName, etProductNameOther, tvaddress,
                etCity, etPincode, tvOccupation, etSourceOfLead, etOther, etBank, etBranchCode, familymembers, etBranchName, etZccSupportRequried, etPreferredLang,
                etPreferredDate, tvNotes, etStatus, tvEducation, etSubStatus, tvIncome, etPremiumExpected, etNextFollowupDate, etFirstAppointmentDate, etConversionPropensity,
                etProposalNumber, etLeadCreatedDate, tvMarriedStatus, tvProduct1, tvProduct2, tvactpremium);
        Utils.setTypefaces(IvokoApplication.robotoLightTypeface, (TextView) findViewById(R.id.fname),
                (TextView) findViewById(R.id.lname), (TextView) findViewById(R.id.contact_number),
                (TextView) findViewById(R.id.alt_contactno), (TextView) findViewById(R.id.email_id),
                (TextView) findViewById(R.id.age), (TextView) findViewById(R.id.address),
                (TextView) findViewById(R.id.city), (TextView) findViewById(R.id.pincode),
                (TextView) findViewById(R.id.education), (TextView) findViewById(R.id.occupation),
                (TextView) findViewById(R.id.income), (TextView) findViewById(R.id.no_of_family_members),
                (TextView) findViewById(R.id.married_status), (TextView) findViewById(R.id.existing_sud_ife_customer),
                (TextView) findViewById(R.id.premium_paid), (TextView) findViewById(R.id.product_name),
                (TextView) findViewById(R.id.other_product_name), (TextView) findViewById(R.id.source_of_lead),
                (TextView) findViewById(R.id.other), (TextView) findViewById(R.id.bank),
                (TextView) findViewById(R.id.branch_code), (TextView) findViewById(R.id.branch_name),
                (TextView) findViewById(R.id.campaigndate), (TextView) findViewById(R.id.zcc_support_requried),
                (TextView) findViewById(R.id.preferred_language), (TextView) findViewById(R.id.preferred_Date),
                (TextView) findViewById(R.id.metting_status), (TextView) findViewById(R.id.lead_status),
                (TextView) findViewById(R.id.first_appointment_date), (TextView) findViewById(R.id.next_followup_date),
                (TextView) findViewById(R.id.expected_actpremium), (TextView) findViewById(R.id.product_one),
                (TextView) findViewById(R.id.conversion_propensity), (TextView) findViewById(R.id.proposal_no),
                (TextView) findViewById(R.id.notes), (TextView) findViewById(R.id.lead_created_date),
                (TextView) findViewById(R.id.product_two), (TextView) findViewById(R.id.actpremium));
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, leadcode, noMembers, toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoMediumTypeface,
                (TextView) findViewById(R.id.personalinfo_lbl), (TextView) findViewById(R.id.pastinfo_lbl),
                (TextView) findViewById(R.id.otherinfo_lbl), (TextView) findViewById(R.id.bankinfo_lbl),
                (TextView) findViewById(R.id.zccinfo_lbl), (TextView) findViewById(R.id.updateinfo_lbl));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("plan_broadcast"));
    }

    @Override
    public void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    private void setListener() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().toString().equalsIgnoreCase("Lead Details")) {
                    contactView.setVisibility(View.GONE);
                    infoView.setVisibility(View.VISIBLE);
                } else {
                    infoView.setVisibility(View.GONE);
                    contactView.setVisibility(View.VISIBLE);
                    if (Utils.isNotNullAndNotEmpty(leadModel.getLeadid())) {
                        plannerModelList = PlannerMgr.getInstance(ViewLeadActivity.this).getPlannerListByLeadID(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), leadModel.getLeadid());
                        if (plannerModelList.size() > 0) {
                            leadAppointmentAdapter = new LeadAppointmentAdapter(ViewLeadActivity.this, plannerModelList);
                            planRecyclerView.setAdapter(leadAppointmentAdapter);
                            planRecyclerView.setLayoutManager(new LinearLayoutManager(ViewLeadActivity.this));
                        } else {
                            noMembers.setVisibility(View.VISIBLE);
                        }
                    } else if (leadModel.getId() > 0) {
                        plannerModelList = PlannerMgr.getInstance(ViewLeadActivity.this).getPlannerListBylocalLeadID(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), leadModel.getId());
                        if (plannerModelList.size() > 0) {
                            leadAppointmentAdapter = new LeadAppointmentAdapter(ViewLeadActivity.this, plannerModelList);
                            planRecyclerView.setAdapter(leadAppointmentAdapter);
                            planRecyclerView.setLayoutManager(new LinearLayoutManager(ViewLeadActivity.this));
                        } else {
                            noMembers.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Utils.isNotNullAndNotEmpty(leadModel.getLeadid())) {
                    plannerModelList = PlannerMgr.getInstance(ViewLeadActivity.this).getPlannerListByLeadID(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), leadModel.getLeadid());
                    if (plannerModelList.size() > 0) {
                        leadAppointmentAdapter = new LeadAppointmentAdapter(ViewLeadActivity.this, plannerModelList);
                        planRecyclerView.setAdapter(leadAppointmentAdapter);
                        planRecyclerView.setLayoutManager(new LinearLayoutManager(ViewLeadActivity.this));
                    } else {
                        noMembers.setVisibility(View.VISIBLE);
                    }
                } else if (leadModel.getId() > 0) {
                    plannerModelList = PlannerMgr.getInstance(ViewLeadActivity.this).getPlannerListBylocalLeadID(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), leadModel.getId());
                    if (plannerModelList.size() > 0) {
                        leadAppointmentAdapter = new LeadAppointmentAdapter(ViewLeadActivity.this, plannerModelList);
                        planRecyclerView.setAdapter(leadAppointmentAdapter);
                        planRecyclerView.setLayoutManager(new LinearLayoutManager(ViewLeadActivity.this));
                    } else {
                        noMembers.setVisibility(View.VISIBLE);
                    }
                }
            }
        };
        schedileview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                schedileview.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                Date initialDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initialDate)
                        .setMinDate(new Date())
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (leadModel != null) {
            if (Utils.isNotNullAndNotEmpty(teamlead)) {
                if (teamlead.equalsIgnoreCase("team")) {
                    menu.findItem(R.id.edit).setVisible(false);
                }
            } else {
                if (Utils.isNotNullAndNotEmpty(leadModel.getLeadstage()))
                    if (leadModel.getLeadstage().equalsIgnoreCase("Not interested") || leadModel.getLeadstage().equalsIgnoreCase("Converted"))
                        menu.findItem(R.id.edit).setVisible(false);
            }
        } else {
            if (Utils.isNotNullAndNotEmpty(teamlead)) {
                if (teamlead.equalsIgnoreCase("team")) {
                    menu.findItem(R.id.edit).setVisible(false);
                }
            } else {
                menu.findItem(R.id.edit).setVisible(true);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.edit) {
            Intent intent = new Intent(ViewLeadActivity.this, UpdateLeadActivity.class);
            intent.putExtra("leadobj", new Gson().toJson(leadModel));
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeViews() {
        tvfName = (TextView) findViewById(R.id.tv_fname);
        tv_campaigndate = (TextView) findViewById(R.id.tv_campaigndate);
        tvlName = (TextView) findViewById(R.id.tv_lname);
        tvContactNumber = (TextView) findViewById(R.id.tv_contact_number);
        tvEmailid = (TextView) findViewById(R.id.tv_email_id);
        isnri = (TextView) findViewById(R.id.isnri);
        tv_nri = (TextView) findViewById(R.id.tv_isnri);
        countrycode = (TextView) findViewById(R.id.countrycode);
        tv_countrycode = (TextView) findViewById(R.id.tv_countrycode);
        countryname = (TextView) findViewById(R.id.countryname);
        tv_countryname = (TextView) findViewById(R.id.tv_countryname);
        tvAltContactno = (TextView) findViewById(R.id.tv_alt_contactno);
        tvAge = (TextView) findViewById(R.id.tv_age);
        etExistingCustomer = (TextView) findViewById(R.id.tv_existing_sud_ife_customer);
        tvMarriedStatus = (TextView) findViewById(R.id.tv_married_status);
        etPremiumPaid = (TextView) findViewById(R.id.tv_premium_paid);
        etProductName = (TextView) findViewById(R.id.tv_product_name);
        tvProduct1 = (TextView) findViewById(R.id.tv_product_one);
        tvProduct2 = (TextView) findViewById(R.id.tv_product_two);
        tvactpremium = (TextView) findViewById(R.id.tv_actpremium);
        etProductNameOther = (TextView) findViewById(R.id.tv_other_product_name);
        tvaddress = (TextView) findViewById(R.id.tv_address);
        etCity = (TextView) findViewById(R.id.tv_city);
        etPincode = (TextView) findViewById(R.id.tv_pincode);
        tvOccupation = (TextView) findViewById(R.id.tv_occupation);
        etSourceOfLead = (TextView) findViewById(R.id.tv_source_of_lead);
        etOther = (TextView) findViewById(R.id.tv_other);
        etBank = (TextView) findViewById(R.id.tv_bank);
        etBranchCode = (TextView) findViewById(R.id.tv_branch_code);
        familymembers = (TextView) findViewById(R.id.tv_no_of_family_members);
        etBranchName = (TextView) findViewById(R.id.tv_branch_name);
        etZccSupportRequried = (TextView) findViewById(R.id.tv_zcc_support_requried);
        etPreferredLang = (TextView) findViewById(R.id.tv_preferred_language);
        etPreferredDate = (TextView) findViewById(R.id.tv_preferred_Date);
        etLeadStatus = (TextView) findViewById(R.id.tv_lead_status);
        etStatus = (TextView) findViewById(R.id.tv_metting_status);
        etSubStatus = (TextView) findViewById(R.id.tv_sub_status);
        etPremiumExpected = (TextView) findViewById(R.id.tv_expected_actpremium);
        etFirstAppointmentDate = (TextView) findViewById(R.id.tv_first_appointment_date);
        etNextFollowupDate = (TextView) findViewById(R.id.tv_next_followup_date);
        etConversionPropensity = (TextView) findViewById(R.id.tv_conversion_propensity);
        etProposalNumber = (TextView) findViewById(R.id.tv_proposal_no);
        etLeadCreatedDate = (TextView) findViewById(R.id.tv_lead_created_date);
        tvEducation = (TextView) findViewById(R.id.tv_education);
        tvIncome = (TextView) findViewById(R.id.tv_income);
        tvNotes = (TextView) findViewById(R.id.tv_notes);
        leadcode = (TextView) findViewById(R.id.leadcode);
        uname = (TextView) findViewById(R.id.uName);
        bankname = (TextView) findViewById(R.id.bankname);
        branchname = (TextView) findViewById(R.id.branchname);
        schedileview = (Button) findViewById(R.id.submit);
        planRecyclerView = (RecyclerView) findViewById(R.id.memberList);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        noMembers = (TextView) findViewById(R.id.noMembers);
        tabLayout.addTab(tabLayout.newTab().setText("Lead Details"));
        tabLayout.addTab(tabLayout.newTab().setText("Appointments"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        infoView = findViewById(R.id.details);
        contactView = findViewById(R.id.contactview);
    }

    public void populateData(LeadModel leadModel) {
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadstage())) {
            if (leadModel.getLeadstage().equalsIgnoreCase("Not interested")) {
                schedileview.setVisibility(View.GONE);
            } else if (leadModel.getLeadstage().equalsIgnoreCase("Converted")) {
                schedileview.setVisibility(View.GONE);
            } else {
                schedileview.setVisibility(View.VISIBLE);
            }
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getFirstname())) {
            tvfName.setText(leadModel.getFirstname());
        } else {
            tvfName.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getCampaigndate())) {
            tv_campaigndate.setText(leadModel.getCampaigndate());
        } else {
            tv_campaigndate.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLastname())) {
            tvlName.setText(leadModel.getLastname());
        } else {
            tvlName.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getIsnri())) {

            if (leadModel.getIsnri().equalsIgnoreCase("true")) {
                tv_nri.setText("YES");

            } else {
                tv_nri.setText("NO");
            }
        } else {
            tv_nri.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getCountrycode())) {
            tv_countrycode.setText(leadModel.getCountrycode());
        } else {
            tv_countrycode.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getCountryname())) {
            tv_countryname.setText(leadModel.getCountryname());
        } else {
            tv_countryname.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getContactno())) {
            tvContactNumber.setText(leadModel.getContactno());
        } else {
            tvContactNumber.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadcode())) {
            leadcode.setText(leadModel.getLeadcode());
        } else {
            leadcode.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getEmail())) {
            tvEmailid.setText(leadModel.getEmail());
        } else {
            tvEmailid.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getAlternatecontactno())) {
            tvAltContactno.setText(leadModel.getAlternatecontactno());
        } else {
            tvAltContactno.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getAge() != null) {
            tvAge.setText(String.valueOf(leadModel.getAge()));
        } else {
            tvAge.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getIsexistingcustomer() != null) {
            if (leadModel.getIsexistingcustomer()) {
                etExistingCustomer.setText("Yes");
                findViewById(R.id.premium_paid).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_premium_paid).setVisibility(View.VISIBLE);
                findViewById(R.id.product_name).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_product_name).setVisibility(View.VISIBLE);
            } else {
                etExistingCustomer.setText("No");
                findViewById(R.id.premium_paid).setVisibility(View.GONE);
                findViewById(R.id.tv_premium_paid).setVisibility(View.GONE);
                findViewById(R.id.product_name).setVisibility(View.GONE);
                findViewById(R.id.tv_product_name).setVisibility(View.GONE);
            }
        } else {
            etExistingCustomer.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getPremiumpaid() != null) {
            etPremiumPaid.setText(String.valueOf(leadModel.getPremiumpaid()));
        } else {
            etPremiumPaid.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProductname())) {
            etProductName.setText(leadModel.getProductname());
        } else {
            etProductName.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProduct1())) {
            tvProduct1.setText(leadModel.getProduct1());
        } else {
            tvProduct1.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProduct2())) {
            tvProduct2.setText(leadModel.getProduct2());
        } else {
            tvProduct2.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getActualpremiumpaid() != null) {
            tvactpremium.setText(String.valueOf(leadModel.getActualpremiumpaid()));
        } else {
            tvactpremium.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getAddress())) {
            tvaddress.setText(leadModel.getAddress());
        } else {
            tvaddress.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getCity())) {
            etCity.setText(leadModel.getCity());
        } else {
            etCity.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getPincode())) {
            etPincode.setText(leadModel.getPincode());
        } else {
            etPincode.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getOccupation())) {
            tvOccupation.setText(leadModel.getOccupation());
        } else {
            tvOccupation.setText(getResources().getString(R.string.notAvailable));
        }

        StringBuilder nameBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(leadModel.getFirstname())) {
            nameBuilder.append(leadModel.getFirstname());
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLastname())) {
            if (Utils.isNotNullAndNotEmpty(nameBuilder.toString())) {
                nameBuilder.append(" " + leadModel.getLastname());
            } else {
                nameBuilder.append(leadModel.getLastname());
            }
        }
        uname.setText(nameBuilder.toString());
        if (Utils.isNotNullAndNotEmpty(leadModel.getBankbranchname())) {
            branchname.setText(leadModel.getBankbranchname());
        } else {
            branchname.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getBank())) {
            bankname.setText(leadModel.getBank());
        } else {
            bankname.setVisibility(View.GONE);
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadsource())) {
            etSourceOfLead.setText(leadModel.getLeadsource());
        } else {
            etSourceOfLead.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getOtherleadsource())) {
            etOther.setText(leadModel.getOtherleadsource());
        } else {
            etOther.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getBank())) {
            etBank.setText(leadModel.getBank());
        } else {
            etBank.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getBankbranchcode())) {
            etBranchCode.setText(leadModel.getBankbranchcode());
        } else {
            etBranchCode.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getFamilymemberscount() != null) {
            familymembers.setText(String.valueOf(leadModel.getFamilymemberscount()));
        } else {
            familymembers.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getBankbranchname())) {
            etBranchName.setText(leadModel.getBankbranchname());
        } else {
            etBranchName.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getZccsupportrequired() != null) {
            if (leadModel.getZccsupportrequired()) {
                etZccSupportRequried.setText("Yes");
                findViewById(R.id.preferred_language).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_preferred_language).setVisibility(View.VISIBLE);
                findViewById(R.id.preferred_Date).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_preferred_Date).setVisibility(View.VISIBLE);
            } else {
                etZccSupportRequried.setText("No");
                findViewById(R.id.preferred_language).setVisibility(View.GONE);
                findViewById(R.id.tv_preferred_language).setVisibility(View.GONE);
                findViewById(R.id.preferred_Date).setVisibility(View.GONE);
                findViewById(R.id.tv_preferred_Date).setVisibility(View.GONE);
            }
        } else {
            etZccSupportRequried.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getIsmarried() != null) {
            if (leadModel.getIsmarried()) {
                tvMarriedStatus.setText("Yes");
            } else {
                tvMarriedStatus.setText("No");
            }
        } else {
            tvMarriedStatus.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getPreferreddatetime() != null) {
            if (leadModel.getPreferreddatetime() == 0) {
                etPreferredDate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(leadModel.getPreferreddatetime()))) {
                etPreferredDate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getPreferreddatetime()), "dd/MM/yyyy hh:mm a"));
            } else {
                etPreferredDate.setText(getResources().getString(R.string.notAvailable));
            }
        } else {
            etPreferredDate.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getPreferredlanguage())) {
            etPreferredLang.setText(leadModel.getPreferredlanguage());
        } else {
            etPreferredLang.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getStatus())) {
            etStatus.setText(leadModel.getStatus());
        } else {
            etStatus.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getSubstatus())) {
            etSubStatus.setText(leadModel.getSubstatus());
        } else {
            etSubStatus.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getExpectedpremium())) {
            etPremiumExpected.setText(leadModel.getExpectedpremium());
        } else {
            etPremiumExpected.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getFirstappointmentdate() != null) {
            if (leadModel.getFirstappointmentdate() == 0) {
                etFirstAppointmentDate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(leadModel.getFirstappointmentdate()))) {
                etFirstAppointmentDate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getFirstappointmentdate()), "dd/MM/yyyy hh:mm a"));
            } else {
                etFirstAppointmentDate.setText(getResources().getString(R.string.notAvailable));
            }
        } else {
            etFirstAppointmentDate.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getNextfollowupdate() != null) {
            if (leadModel.getNextfollowupdate() == 0) {
                etNextFollowupDate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(leadModel.getNextfollowupdate()))) {
                etNextFollowupDate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getNextfollowupdate()), "dd/MM/yyyy hh:mm a"));
            } else {
                etNextFollowupDate.setText(getResources().getString(R.string.notAvailable));
            }
        } else {
            etNextFollowupDate.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getConversionpropensity())) {
            etConversionPropensity.setText(leadModel.getConversionpropensity());
        } else {
            etConversionPropensity.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProposalnumber())) {
            etProposalNumber.setText(String.valueOf(leadModel.getProposalnumber()));
        } else {
            etProposalNumber.setText(getResources().getString(R.string.notAvailable));
        }
        if (leadModel.getLeadcreatedon() != null) {
            if (leadModel.getLeadcreatedon() == 0) {
                etLeadCreatedDate.setText(getResources().getString(R.string.notAvailable));
            } else if (Utils.isNotNullAndNotEmpty(String.valueOf(leadModel.getLeadcreatedon()))) {
                etLeadCreatedDate.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getLeadcreatedon()), "dd/MM/yyyy hh:mm a"));
            } else {
                etLeadCreatedDate.setText(getResources().getString(R.string.notAvailable));
            }
        } else {
            etLeadCreatedDate.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getEducation())) {
            tvEducation.setText(leadModel.getEducation());
        } else {
            tvEducation.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getIncomeband())) {
            tvIncome.setText(leadModel.getIncomeband());
        } else {
            tvIncome.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getRemarks())) {
            tvNotes.setText(leadModel.getRemarks());
        } else {
            tvNotes.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadstage())) {
            etLeadStatus.setText(leadModel.getLeadstage());
        } else {
            etLeadStatus.setText(getResources().getString(R.string.notAvailable));
        }
    }

    private void postcreateplan() {
        plannerModel.setGroupid(groupId);
        plannerModel.setUserid(userid);
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadid())) {
            plannerModel.setLeadid(leadModel.getLeadid());
            plannerModel.setLocalleadid(leadModel.getId());
        } else {
            plannerModel.setLocalleadid(leadModel.getId());
        }
        if (Utils.isNotNullAndNotEmpty(scheduledate)) {
            plannerModel.setScheduleday(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", TimeUtils.getFormattedDatefromUnix(scheduledate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setSchedulemonth(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", TimeUtils.getFormattedDatefromUnix(scheduledate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setScheduleyear(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", TimeUtils.getFormattedDatefromUnix(scheduledate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setScheduletime(Integer.parseInt(scheduledate));
        }
        plannerModel.setFirstname(leadModel.getFirstname());
        plannerModel.setLastname(leadModel.getLastname());
        plannerModel.setPlanstatus(1);
        plannerModel.setCompletestatus(0);
        plannerModel.setType("Appointment");
        plannerModel.setActivitytype("Appointment");
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadstage()))
            plannerModel.setSubtype(leadModel.getLeadstage());
        plannerModel.setNetwork_status("offline");
        PlannerMgr.getInstance(ViewLeadActivity.this).insertPlanner(plannerModel, leadModel.getUserid(), leadModel.getGroupid());
        List<PlannerModel> plannerModelArrayList = new ArrayList<>();
        plannerModelArrayList.add(plannerModel);
        PlannerMgr.getInstance(ViewLeadActivity.this).insertPlannerNotificationList(plannerModelArrayList, leadModel.getUserid(), leadModel.getGroupid());
        LeadIntentService.syncLeadstoServer(ViewLeadActivity.this);
        finish();
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.vftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("View Lead");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setTitle("");
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }
}

