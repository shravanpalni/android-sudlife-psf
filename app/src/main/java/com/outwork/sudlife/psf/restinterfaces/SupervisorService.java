package com.outwork.sudlife.psf.restinterfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface SupervisorService {
    @GET("mobile/v1/user/teamsuperviors")
    Call<RestResponse> getteamsupersiors(@Header("utoken") String userToken, @Path("userid") String userid);

    @GET("admin/v1/teams/{teamId}/hierarchy/members")
    Call<RestResponse> getTeamHierarchy(@Header("utoken") String userToken, @Path("teamId") String teamId);
}