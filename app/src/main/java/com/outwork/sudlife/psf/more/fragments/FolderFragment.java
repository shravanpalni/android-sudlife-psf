package com.outwork.sudlife.psf.more.fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.listener.RecyclerTouchListener;
import com.outwork.sudlife.psf.more.ViewPhotoActivity;
import com.outwork.sudlife.psf.more.ViewResourceActivity;
import com.outwork.sudlife.psf.more.ViewVideoActivity;
import com.outwork.sudlife.psf.more.adapter.FolderRecyclerAdapter;
import com.outwork.sudlife.psf.more.models.PhotoModel;
import com.outwork.sudlife.psf.more.models.ResourceModel;
import com.outwork.sudlife.psf.more.service.ResourceLibraryService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class FolderFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    private static final int ADD_MSG = 191;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView folderView;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar msgListProgressBar;
    private List<ResourceModel> resourceList = new ArrayList<>();
    private FolderRecyclerAdapter folderRecyclerAdapter;
    private TextView nomessageview;
    private String resourceType;
    private String libraryid;
    private boolean isLibrary = false;
    private Call<RestResponse> restLoaderClient;
    private OnFragmentInteractionListener mListener;
    private String header = "Resource Library";

    public FolderFragment() {
    }

    public static FolderFragment newInstance(String param1, boolean param2, String folderName) {
        FolderFragment fragment = new FolderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putBoolean(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, folderName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            libraryid = getArguments().getString(ARG_PARAM1);
            isLibrary = getArguments().getBoolean(ARG_PARAM2, false);
            header = getArguments().getString(ARG_PARAM3, "Resource Library");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View activityView = inflater.inflate(R.layout.fragment_folder, container, false);
        initToolBar();
        initUI(activityView);
        initListeners(activityView);
        initializeList();
        return activityView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            // throw new RuntimeException(context.toString()
            //       + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initToolBar();
    }

    @Override
    public void onPause() {
        super.onPause();
        initToolBar();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void initUI(View activityView) {
        nomessageview = (TextView) activityView.findViewById(R.id.nomessages);
        mSwipeRefreshLayout = (SwipeRefreshLayout) activityView.findViewById(R.id.activity_msg_swipe_refresh_layout);
        msgListProgressBar = (ProgressBar) activityView.findViewById(R.id.messageListProgressBar);
        folderView = (RecyclerView) activityView.findViewById(R.id.folderRecyclerView);
        linearLayoutManager = new LinearLayoutManager((getActivity()));
        folderView.setLayoutManager(linearLayoutManager);
        folderRecyclerAdapter = new FolderRecyclerAdapter(getActivity(), resourceList);
        folderView.setAdapter(folderRecyclerAdapter);
    }

    private void initListeners(View activeView) {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        folderView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), new RecyclerTouchListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ResourceModel resourceModel = (ResourceModel) resourceList.get(position);
                performAction(resourceModel);
            }
        }));
    }

    private void initializeList() {
        refreshList();
    }

    @Override
    public void onRefresh() {
        // TODO Auto-generated method stub
        mSwipeRefreshLayout.setRefreshing(true);
        refreshList();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void refreshList() {

        nomessageview.setVisibility(View.INVISIBLE);

        ListFolderThread folderThread = new ListFolderThread("listadmin");
        Runnable task = new Runnable() {
            @Override
            public void run() {
                if (isLibrary) {
                    createLoaderClient(libraryid, true);
                } else {
                    createLoaderClient(libraryid, false);
                }
                restLoaderClient.enqueue(new Callback<RestResponse>() {
                    @Override
                    public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                        if (response.isSuccessful()) {
                            try {
                                if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                                    resourceList.clear();
                                    Type listType = new TypeToken<ArrayList<ResourceModel>>() {
                                    }.getType();
                                    resourceList = new Gson().fromJson(response.body().getData(), listType);
                                    if (resourceList.size() > 0) {
                                        if (msgListProgressBar.VISIBLE == 0) {
                                            msgListProgressBar.setVisibility(View.INVISIBLE);
                                        }
                                        folderRecyclerAdapter.setItems(resourceList);
                                        folderRecyclerAdapter.notifyDataSetChanged();
                                    } else {
                                        setNoMessages();
                                    }
                                } else {
                                    setNoMessages();
                                }
                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                                setNoMessages();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RestResponse> call, Throwable t) {
                        setNoMessages();
                    }
                });
            }
        };
        folderThread.start();
        folderThread.prepareHandler();
        folderThread.postTask(task);
    }

    private void setNoMessages() {
        if (resourceList.size() == 0) {
            nomessageview.setVisibility(View.VISIBLE);
        }
        if (msgListProgressBar.VISIBLE == 0) {
            msgListProgressBar.setVisibility(View.INVISIBLE);
        }
    }


    private void performAction(ResourceModel resourceModel) {

        if (!((BaseActivity) getActivity()).isNetworkAvailable()) {
            Utils.displayToast(getActivity(), "Please check your internet connection and try again");
            return;
        }
        resourceType = resourceModel.getType();
        if (resourceType.equalsIgnoreCase("folder")) {
            FragmentManager manager = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            FolderFragment folderFragment = new FolderFragment().newInstance(resourceModel.getLibraryid(), true, resourceModel.getTitle());
            ft.add(R.id.fragment_content_frame, folderFragment);
            ft.addToBackStack(null);
            ft.commit();
        } else {
            String fileUrl = resourceModel.getFileobject().getFileurl();
            if (TextUtils.isEmpty(fileUrl)) {
                return;
            }
            String extension = resourceModel.getFileobject().getExtension();
            if (extension != null) {
                switch (extension.toLowerCase()) {
                    case "jpeg":
                    case "jpg":
                    case "png":
                        ArrayList<PhotoModel> photoList = new ArrayList<PhotoModel>();
                        PhotoModel photoDto = new PhotoModel();
                        photoDto.setName(resourceModel.getFileobject().getFileName());
                        photoDto.setUrl(resourceModel.getFileobject().getFileurl());
                        photoDto.setMainurl("");
                        photoDto.setThumburl(resourceModel.getFileobject().getAltfileurl());
                        photoList.add(photoDto);
                        Intent in = new Intent(getActivity(), ViewPhotoActivity.class);
                        in.putParcelableArrayListExtra("data", photoList);
                        in.putExtra("pos", 0);
                        startActivity(in);
                        break;
                    case "3gp":
                    case "mp4":
                    case "wmv":
                    case "flv":
                    case "avi":
                        try {
                            Intent intentUrl = new Intent(Intent.ACTION_VIEW);
                            intentUrl.setDataAndType(Uri.parse(fileUrl), "video/*");
                            intentUrl.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            getActivity().startActivity(intentUrl);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getActivity(), "No video player Installed", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case "pptx":
                    case "ppt":
                    case "vnd.openxmlformats-officedocument.presentationml.presentation":
                        try {
                            Intent intentUrl = new Intent(Intent.ACTION_VIEW);
                            intentUrl.setDataAndType(Uri.parse(fileUrl), "application/vnd.ms-powerpoint");
                            intentUrl.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            getActivity().startActivity(intentUrl);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getActivity(), "No ms-powerpoint Installed", Toast.LENGTH_LONG).show();
                            Intent in1 = new Intent(getActivity(), ViewResourceActivity.class);
                            in1.putExtra("FILEURL", fileUrl);
                            in1.putExtra("Name", resourceModel.getFileobject().getFileName());
                            in1.putExtra("EXTENSION", resourceModel.getFileobject().getExtension());
                            startActivity(in1);
                        }
                        break;
                    case "xls":
                    case "xlsx":
//                    case "vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                        try {
                            Intent intentUrl = new Intent(Intent.ACTION_VIEW);
                            intentUrl.setDataAndType(Uri.parse(fileUrl), "application/vnd.ms-excel");
                            intentUrl.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            getActivity().startActivity(intentUrl);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getActivity(), "No ms-xcel Installed", Toast.LENGTH_LONG).show();
                            Intent in1 = new Intent(getActivity(), ViewResourceActivity.class);
                            in1.putExtra("FILEURL", fileUrl);
                            in1.putExtra("Name", resourceModel.getFileobject().getFileName());
                            in1.putExtra("EXTENSION", resourceModel.getFileobject().getExtension());
                            startActivity(in1);
                        }
                        break;
                    case "pdf":
                        try {
                            Intent intentUrl = new Intent(Intent.ACTION_VIEW);
                            intentUrl.setDataAndType(Uri.parse(fileUrl), "application/pdf");
                            intentUrl.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            getActivity().startActivity(intentUrl);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getActivity(), "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
                            Intent in1 = new Intent(getActivity(), ViewResourceActivity.class);
                            in1.putExtra("FILEURL", fileUrl);
                            in1.putExtra("Name", resourceModel.getFileobject().getFileName());
                            in1.putExtra("EXTENSION", resourceModel.getFileobject().getExtension());
                            startActivity(in1);
                        }
                        break;
                    default:
                        Intent in1 = new Intent(getActivity(), ViewResourceActivity.class);
                        in1.putExtra("FILEURL", fileUrl);
                        in1.putExtra("Name", resourceModel.getFileobject().getFileName());
                        in1.putExtra("EXTENSION", resourceModel.getFileobject().getExtension());
                        startActivity(in1);
                        break;
                }
            } else {
                Intent in2 = new Intent(getActivity(), ViewVideoActivity.class);
                in2.putExtra("FILEURL", fileUrl);
                in2.putExtra("Name", resourceModel.getFileobject().getFileName());
                startActivity(in2);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_MSG) {
            if (resultCode == RESULT_OK && data != null) {
                refreshList();
            }
        }
    }

    public static void openFile(Context context, File url) throws IOException {
        // Create URI
        File file = url;
        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword");
        } else if (url.toString().contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf");
        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav");
        } else if (url.toString().contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf");
        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav");
        } else if (url.toString().contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif");
        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg");
        } else if (url.toString().contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain");
        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*");
        } else {
            intent.setDataAndType(uri, "*/*");
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public class ListFolderThread extends HandlerThread {

        private Handler mWorkerHandler;

        public ListFolderThread(String name) {
            super(name);
        }

        public void postTask(Runnable task) {
            mWorkerHandler.post(task);
        }

        public void prepareHandler() {
            mWorkerHandler = new Handler(getLooper());
        }
    }

    private void initToolBar() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            TextView textView = (TextView) getActivity().findViewById(R.id.toolbar_title);
            textView.setText(header);
        }
    }

    private void createLoaderClient(String resourceId, boolean isLibrary) {
        ResourceLibraryService client = RestService.createService(ResourceLibraryService.class);
        //***Admin***/
        if (isLibrary) {
            restLoaderClient = client.getResourceList(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""),
                    SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), resourceId);
        } else {
            restLoaderClient = client.getResourceList(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""),
                    SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
        }

    }
}
