package com.outwork.sudlife.psf.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.onesignal.OneSignal;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.activities.PSFCalenderActivity;
import com.outwork.sudlife.psf.lead.activities.PSFCalenderNewActivity;
import com.outwork.sudlife.psf.lead.activities.PSFFactFinderActivity;
import com.outwork.sudlife.psf.lead.activities.PSFLeadListActivity;
import com.outwork.sudlife.psf.lead.activities.PSFPerformanceChartActivity;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.localbase.services.ProductListDownloadService;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.planner.activities.CPlannerActivity;
import com.outwork.sudlife.psf.receiver.ConnectivityReceiver;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.base.AppBaseActivity;
import com.outwork.sudlife.psf.ui.base.SplashActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.outwork.sudlife.psf.restinterfaces.RestService.API_BASE_URL;

public class DashBoardActivity extends AppBaseActivity {

    //private ImageView groupImage;
    private LinearLayout proposaltracker, calendar, factfinder, leads, performance, morelayout;
    private boolean isFeatureEnabled = false;
    private boolean doubleBackToExitPressedOnce = true;
    private ConnectivityReceiver mNetworkReceiver;
    TextView textCartItemCount,environment;
    int mCartItemCount;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board1);



        isFeatureEnabled = SharedPreferenceManager.getInstance().getBoolean(Constants.isFeatureEnabled, false);
        mNetworkReceiver = new ConnectivityReceiver();
        mgr = LocalBroadcastManager.getInstance(this);
        OneSignal.sendTag("userid", SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        OneSignal.syncHashedEmail(SharedPreferenceManager.getInstance().getString(Constants.USER_EMAIL, ""));
        OneSignal.sendTag("group", SharedPreferenceManager.getInstance().getString(Constants.GRP_CODE, ""));
        OneSignal.sendTag("grouptitle", SharedPreferenceManager.getInstance().getString(Constants.GRP_DISPLAYNAME, ""));
        OneSignal.sendTag("email", SharedPreferenceManager.getInstance().getString(Constants.USER_EMAIL, ""));
        OneSignal.sendTag("EMPID", SharedPreferenceManager.getInstance().getString(Constants.LOGINID, ""));
        OneSignal.sendTag("appVersion", getResources().getString(R.string.app_version));
        initUI();
        initToolBar();
        setListeners();
        Utils.setTypefaces(IvokoApplication.robotoTypeface, (TextView) findViewById(R.id.textChat),
                (TextView) findViewById(R.id.texttask), (TextView) findViewById(R.id.textexpense),
                (TextView) findViewById(R.id.aText), (TextView) findViewById(R.id.textView4), (TextView) findViewById(R.id.textmore));




        /*mBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                if (SharedPreferenceManager.getInstance().getString(Constants.PSF_NOTIFICATIONS_UPDATE, "").equals("notupdated")) {
                    if (isNetworkAvailable()) {
                        LeadIntentService.insertPSFNotifications(DashBoardActivity.this);
                    }
                }



                List<PSFNotificationsModel> finalList = new ArrayList<>();
                List<PSFNotificationsModel> psfNotificationsModelList = LeadMgr.getInstance(DashBoardActivity.this).getPSFNotificationsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));

                if (psfNotificationsModelList.size() > 0) {

                    for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                        if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                            finalList.add(notificationsModel);
                        }

                    }

                    mCartItemCount = finalList.size();
                } else {
                    mCartItemCount = 0;
                }

            }
        };*/


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isNetworkAvailable()) {
            Log.i("Dashboard","onStart called----");
            if (LeadMgr.getInstance(DashBoardActivity.this).getOfflinePSFLeadsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline").size() > 0) {

                LeadIntentService.syncPSFLeadstoServer(DashBoardActivity.this);
            }
            ProductListDownloadService.getProductListinDB(DashBoardActivity.this);
            ProductListDownloadService.getMasterListinDB(DashBoardActivity.this);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }

        if (isNetworkAvailable()) {
            updateNotification();
        } else {
            List<PSFNotificationsModel> finalList = new ArrayList<>();
            List<PSFNotificationsModel> psfNotificationsModelList = LeadMgr.getInstance(DashBoardActivity.this).getPSFNotificationsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));

            if (psfNotificationsModelList.size() > 0) {

                for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                    if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                        finalList.add(notificationsModel);
                    }

                }

                mCartItemCount = finalList.size();
                setupBadge();
            } else {
                mCartItemCount = 0;
                setupBadge();
            }

        }


        //mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("psfnotificationsupdate_broadcast"));
    }

    private void initUI() {
        performance = (LinearLayout) findViewById(R.id.customers);
        calendar = (LinearLayout) findViewById(R.id.planner);
        leads = (LinearLayout) findViewById(R.id.leads);
        proposaltracker = (LinearLayout) findViewById(R.id.targets);
        factfinder = (LinearLayout) findViewById(R.id.factfinder);
        //performance = (LinearLayout) findViewById(R.id.performance);
        morelayout = (LinearLayout) findViewById(R.id.morelayout);
        environment = (TextView) findViewById(R.id.environment);
        environment.setVisibility(View.GONE);

        if (API_BASE_URL.equalsIgnoreCase("https://psfapi.outwork.in:443/")){
            environment.setVisibility(View.GONE);
        }else if(API_BASE_URL.equalsIgnoreCase("http://sudlifeservices.outwork.in:7000/")){
            environment.setVisibility(View.VISIBLE);
            environment.setText("Development mode");

        }else {
            environment.setVisibility(View.GONE);

        }
        //groupImage = (ImageView) findViewById(R.id.groupImage);

        /*Glide.with(DashBoardActivity.this).load(SharedPreferenceManager.getInstance().getString(Constants.GRP_IMAGE, ""))
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(groupImage);*/
    }

    private void setListeners() {
        performance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashBoardActivity.this, PSFPerformanceChartActivity.class);
                startActivity(intent);
                //showSimpleAlert("", "Please contact us to enable");
                //showToast("To Be Enabled");
               /* if (isFeatureEnabled) {
                    Intent intent = new Intent(DashBoardActivity.this, ListCustomerActivity.class);
                    startActivity(intent);
                } else {
                    showSimpleAlert("", "Please contact us to enable");
                }*/
            }
        });
        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(DashBoardActivity.this, CPlannerActivity.class));
                //startActivity(new Intent(DashBoardActivity.this, PSFCalenderActivity.class));
                startActivity(new Intent(DashBoardActivity.this, PSFCalenderNewActivity.class));
            }
        });
        leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashBoardActivity.this, PSFLeadListActivity.class));
                //startActivity(new Intent(DashBoardActivity.this, PSFNewCreateLeadActivity.class));
            }
        });
        proposaltracker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showToast("To Be Enabled");
                //   startActivity(new Intent(DashBoardActivity.this, ListOfTargetsWithCardsActivity.class));
                //startActivity(new Intent(DashBoardActivity.this, PSFFactFinderActivity.class));
                if (isNetworkAvailable()) {
                    Intent sales_illustration_intent = new Intent(getApplicationContext(), WebviewLibActivity.class);
                    sales_illustration_intent.putExtra("url", "https://apps.sudlife.in/sudbpos/projecttracker.do?key=1&login=BankPortal");
                    startActivity(sales_illustration_intent);

                } else {
                    showToast("No Internet! Please connect to internet.");
                }
            }
        });
        factfinder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showToast("To Be Enabled");
                startActivity(new Intent(DashBoardActivity.this, PSFFactFinderActivity.class));
            }
        });
        morelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("To Be Enabled");
                //  startActivity(new Intent(DashBoardActivity.this, ListMoreActivity.class));
            }
        });
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("");
        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        textCartItemCount = (TextView) findViewById(R.id.notification_badge);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showToast("Notification Clicked!!!");
                Intent in = new Intent(DashBoardActivity.this, NotificationsListActivity.class);
                startActivity(in);
            }
        });

        setupBadge();
    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = false;
            showToast("Please click BACK again to exit.");
        } else {
            finish();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (mNetworkReceiver != null)
                unregisterReceiver(mNetworkReceiver);
        }

        mgr.unregisterReceiver(mBroadcastReceiver);
    }


    private void updateNotification() {
        // LeadIntentService.insertPSFNotifications(DashBoardActivity.this);
        //showProgressDialog("Refreshing data . . .");
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getProposalCodes = client.getPsfNotifications(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        getProposalCodes.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFNotificationsModel>>() {
                        }.getType();
                        List<PSFNotificationsModel> psfNotificationsModelList = new Gson().fromJson(response.body().getData(), listType);
                        List<PSFNotificationsModel> finalList = new ArrayList<>();
                        if (psfNotificationsModelList.size() > 0) {
                            for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                                if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                                    finalList.add(notificationsModel);
                                }

                            }

                            mCartItemCount = finalList.size();
                            setupBadge();
                            //dismissProgressDialog();

                        } else {
                            mCartItemCount = 0;
                            setupBadge();
                            //dismissProgressDialog();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                //dismissProgressDialog();
                //SharedPreferenceManager.getInstance().putString(Constants.PSF_NOTIFICATIONS_LOADED, "notloaded");
            }
        });

    }
}

