package com.outwork.sudlife.psf.lead.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Habi on 07-06-2018.
 */

public class ProposalCodesModel implements Serializable {
    @SerializedName("organizationid")
    @Expose
    private String organizationid;
    @SerializedName("bandid")
    @Expose
    private String bandid;
    @SerializedName("bandname")
    @Expose
    private String bandname;
    @SerializedName("minvalue")
    @Expose
    private Integer minvalue;
    @SerializedName("maxvalue")
    @Expose
    private Integer maxvalue;

    public String getOrganizationid() {
        return organizationid;
    }

    public void setOrganizationid(String organizationid) {
        this.organizationid = organizationid;
    }

    public String getBandid() {
        return bandid;
    }

    public void setBandid(String bandid) {
        this.bandid = bandid;
    }

    public String getBandname() {
        return bandname;
    }

    public void setBandname(String bandname) {
        this.bandname = bandname;
    }

    public Integer getMinvalue() {
        return minvalue;
    }

    public void setMinvalue(Integer minvalue) {
        this.minvalue = minvalue;
    }

    public Integer getMaxvalue() {
        return maxvalue;
    }

    public void setMaxvalue(Integer maxvalue) {
        this.maxvalue = maxvalue;
    }
}
