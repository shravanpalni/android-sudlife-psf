package com.outwork.sudlife.psf.branches.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.psf.branches.models.BranchesModel;
import com.outwork.sudlife.psf.dao.DBHelper;
import com.outwork.sudlife.psf.dao.IvokoProvider;
import com.outwork.sudlife.psf.dao.IvokoProvider.Customer;
import com.outwork.sudlife.psf.restinterfaces.POJO.GeoAddress;
import com.outwork.sudlife.psf.utilities.Utils;

/**
 * Created by Panch on 2/22/2017.
 */
public class BranchesDao {
    public static final String TAG = BranchesDao.class.getSimpleName();

    private static BranchesDao instance;
    private Context applicationContext;

    public static BranchesDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new BranchesDao(applicationContext);
        }
        return instance;
    }

    private BranchesDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void insertCustomer(BranchesModel branchesModel, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(Customer.groupid, branchesModel.getGroupid());
        values.put(Customer.userid, branchesModel.getUserid());
        values.put(Customer.customerid, branchesModel.getCustomerid());
        values.put(Customer.customername, branchesModel.getCustomername());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBranchcode()))
            values.put(Customer.customercode, branchesModel.getBranchcode());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getPhoneno()))
            values.put(Customer.phoneno, branchesModel.getPhoneno());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBranchfoundationday()))
            values.put(Customer.branchfoundationday, branchesModel.getBranchfoundationday());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getEmail()))
            values.put(Customer.email, branchesModel.getEmail());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSudlat()))
            values.put(Customer.sudlat, branchesModel.getSudlat());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSudlong()))
            values.put(Customer.sudlong, branchesModel.getSudlong());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBanklat()))
            values.put(Customer.banklat, branchesModel.getBanklat());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBanklong()))
            values.put(Customer.banklong, branchesModel.getBanklong());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSoname()))
            values.put(Customer.soname, branchesModel.getSoname());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getRegion()))
            values.put(Customer.region, branchesModel.getRegion());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getZone()))
            values.put(Customer.zone, branchesModel.getZone());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomertype()))
            values.put(Customer.organizationtype, branchesModel.getCustomertype());
        if (branchesModel.getClassification() != null) {
            values.put(Customer.internaltype, branchesModel.getClassification());
        }
        if (Utils.isNotNullAndNotEmpty(branchesModel.getCreateddate()))
            values.put(Customer.createddate, branchesModel.getCreateddate());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getModifieddate()))
            values.put(Customer.modifieddate, branchesModel.getModifieddate());
        values.put(Customer.status, "");
        if (Utils.isNotNullAndNotEmpty(branchesModel.getAddressline1())) {
            values.put(Customer.addressline1, branchesModel.getAddressline1());
        }
        if (Utils.isNotNullAndNotEmpty(branchesModel.getLocality())) {
            values.put(Customer.locality, branchesModel.getLocality());
        }
        if (branchesModel.getAddress() != null)
            values.put(Customer.address, new Gson().toJson(branchesModel.getAddress()));
        if (branchesModel.getIsdeleted() != null) {
            if (branchesModel.getIsdeleted()) {
                values.put(Customer.isdelete, branchesModel.getIsdeleted());
            } else {
                values.put(Customer.isdelete, false);
            }
        } else {
            values.put(Customer.isdelete, false);
        }
        boolean createSuccessful = db.insert(Customer.TABLE, null, values) > 0;
        if (createSuccessful) {
            Log.e(TAG, " created.");
        }
    }

    public void insertLocalCustomer(BranchesModel branchesModel, String network_status) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Customer.groupid, branchesModel.getGroupid());
        values.put(Customer.userid, branchesModel.getUserid());
        values.put(Customer.customerid, branchesModel.getCustomerid());
        values.put(Customer.customername, branchesModel.getCustomername());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBranchcode()))
            values.put(Customer.customercode, branchesModel.getBranchcode());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getPhoneno()))
            values.put(Customer.phoneno, branchesModel.getPhoneno());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getEmail()))
            values.put(Customer.email, branchesModel.getEmail());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSudlat()))
            values.put(Customer.sudlat, branchesModel.getSudlat());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSudlong()))
            values.put(Customer.sudlong, branchesModel.getSudlong());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBanklat()))
            values.put(Customer.banklat, branchesModel.getBanklat());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBanklong()))
            values.put(Customer.banklong, branchesModel.getBanklong());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSoname()))
            values.put(Customer.soname, branchesModel.getSoname());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getRegion()))
            values.put(Customer.region, branchesModel.getRegion());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getZone()))
            values.put(Customer.zone, branchesModel.getZone());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBranchfoundationday()))
            values.put(Customer.branchfoundationday, branchesModel.getBranchfoundationday());
        values.put(Customer.organizationtype, branchesModel.getCustomertype());
        values.put(Customer.internaltype, branchesModel.getClassification());
        values.put(Customer.status, network_status);
        if (Utils.isNotNullAndNotEmpty(branchesModel.getAddressline1())) {
            values.put(Customer.addressline1, branchesModel.getAddressline1());
        }
        if (Utils.isNotNullAndNotEmpty(branchesModel.getLocality())) {
            values.put(Customer.locality, branchesModel.getLocality());
        }
        if (branchesModel.getAddress() != null)
            values.put(Customer.address, new Gson().toJson(branchesModel.getAddress()));
        boolean createSuccessful = db.insert(Customer.TABLE, null, values) > 0;
        if (createSuccessful) {
            Log.e(TAG, " created.");
        }
    }

    public void insertCustomerList(List<BranchesModel> branchesModelList) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        try {
            for (int j = 0; j < branchesModelList.size(); j++) {
                BranchesModel branchesModel = branchesModelList.get(j);
//                if (branchesModel.getIsdeleted()) {
//                    deleteCustomerRecordID(branchesModel.getCustomerid());
//                } else {
                if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomerid()))
                    if (isCustomerExist(db, branchesModel.getCustomerid())) {
                        deleteCustomerRecordID(branchesModel.getCustomerid());
                        insertCustomer(branchesModel, db);
                    } else {
                        insertCustomer(branchesModel, db);
                    }
//                }
            }
        } finally {
        }
    }


    public void insertNewCustomerList(List<BranchesModel> branchesModelList) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        try {

            deleteAllCustomerRecords();


            for (int j = 0; j < branchesModelList.size(); j++) {
                BranchesModel branchesModel = branchesModelList.get(j);
                if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomerid()))

                    insertCustomer(branchesModel, db);

            }
        } finally {
        }
    }


    public void deleteAllCustomerRecords() {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            db.execSQL("delete from " + Customer.TABLE);
            //db.delete(Customer.TABLE, Customer.customerid + "=?", new String[]{id});
//            db.execSQL("delete from " + Customer.TABLE + " WHERE " + Customer.customerid + " = '" + id + "'");
        }
    }


    public BranchesModel getCustomerData(String customerId) {
        BranchesModel dto = new BranchesModel();
        SQLiteDatabase db1 = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db1, Customer.TABLE)) {
            Cursor cursor = null;

            // security
           /* String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.customerid + " = '" + customerId + "'";*/
            try {
                //cursor = db1.rawQuery(sql, null);

                cursor = db1.rawQuery("SELECT * FROM " + Customer.TABLE + " WHERE " + Customer.customerid + " = ?" , new String[]{customerId});

                if (cursor.moveToFirst()) {
                    do {
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)) != null)
                            if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equals("1")) {
                                dto.setIsdeleted(true);
                            } else if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equalsIgnoreCase("true")) {
                                dto.setIsdeleted(true);
                            } else {
                                dto.setIsdeleted(false);
                            }
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return dto;
    }

    public List<BranchesModel> getonlineCustomersList(String groupId, String userId, String status) {
        List<BranchesModel> customerList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;

            // security
           /* String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";
            sql += " ORDER BY " + Customer.customername + " COLLATE NOCASE ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Customer.TABLE + " WHERE " + IvokoProvider.Customer.userid + " = ?" + " AND " + IvokoProvider.Customer.isdelete + " = ?" + "  ORDER BY " + IvokoProvider.Customer.customername + " = ?" , new String[]{userId ,"0"," COLLATE NOCASE "});


                if (cursor.moveToFirst()) {
                    do {
                        BranchesModel dto = new BranchesModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday))))
                            dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)) != null)
                            if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equals("1")) {
                                dto.setIsdeleted(true);
                            } else if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equalsIgnoreCase("true")) {
                                dto.setIsdeleted(true);
                            } else {
                                dto.setIsdeleted(false);
                            }
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));
                        customerList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customerList;
    }

    public List<BranchesModel> getCustomers(String groupId, String userId) {
        List<BranchesModel> customerList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;

            // security
           /* String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";
            sql += " ORDER BY " + Customer.customername + " COLLATE NOCASE ";*/
            try {
                //cursor = db.rawQuery(sql, null);


                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Customer.TABLE + " WHERE " + IvokoProvider.Customer.userid + " = ?" + " AND " + IvokoProvider.Customer.isdelete + " = ?" + "  ORDER BY " + IvokoProvider.Customer.customername + " = ?" , new String[]{userId ,"0"," COLLATE NOCASE "});


                if (cursor.moveToFirst()) {
                    do {
                        BranchesModel dto = new BranchesModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)) != null)
                            if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equals("1")) {
                                dto.setIsdeleted(true);
                            } else if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equalsIgnoreCase("true")) {
                                dto.setIsdeleted(true);
                            } else {
                                dto.setIsdeleted(false);
                            }
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));
                        customerList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customerList;
    }

    public List<BranchesModel> getCustomersbyStatus(String userId, String network_status) {
        List<BranchesModel> customerList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;

            // security
           /* String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.status + " = '" + network_status + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";
            sql += " ORDER BY " + Customer.customername + " COLLATE NOCASE ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Customer.TABLE + " WHERE " + IvokoProvider.Customer.userid + " = ?" + " AND " + IvokoProvider.Customer.status + " = ?" + " AND " + IvokoProvider.Customer.isdelete + " = ?" + "  ORDER BY " + IvokoProvider.Customer.customername + " = ?" , new String[]{userId,network_status,"0"," COLLATE NOCASE "});

                if (cursor.moveToFirst()) {
                    do {
                        BranchesModel dto = new BranchesModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)) != null)
                            if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equals("1")) {
                                dto.setIsdeleted(true);
                            } else if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equalsIgnoreCase("true")) {
                                dto.setIsdeleted(true);
                            } else {
                                dto.setIsdeleted(false);
                            }
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));
                        customerList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customerList;
    }

    public List<String> getCustomerNamesList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<String> customernamesList = new ArrayList<>();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;

            // security
            /*String sql = "";
            sql += "SELECT DISTINCT " + Customer.customername;
            sql += " FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";*/

            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT DISTINCT "+ IvokoProvider.Customer.customername + " FROM " + IvokoProvider.Customer.TABLE + " WHERE " + IvokoProvider.Customer.userid + " = ?"  + " AND " + IvokoProvider.Customer.isdelete + " = ?"  , new String[]{userId ,"0"});
                if (cursor.moveToFirst()) {
                    do {
                        customernamesList.add(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customernamesList;
    }

    public List<String> getCustomerTypesList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<String> customertypesList = new ArrayList<>();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            // security

            /*String sql = "";
            sql += "SELECT DISTINCT " + Customer.organizationtype;
            sql += " FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";*/

            try {
                //cursor = db.rawQuery(sql, null);
                cursor = db.rawQuery("SELECT DISTINCT "+ IvokoProvider.Customer.organizationtype + " FROM " + IvokoProvider.Customer.TABLE + " WHERE " + IvokoProvider.Customer.userid + " = ?"  + " AND " + IvokoProvider.Customer.isdelete + " = ?"  , new String[]{userId ,"0"});

                if (cursor.moveToFirst()) {
                    do {
                        customertypesList.add(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customertypesList;
    }

    public List<BranchesModel> getonlineCustomers(String groupId, String userInput, String
            userId, String net_status) {
        List<BranchesModel> customerList = new ArrayList<>();
        boolean deleted = false;
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;

            // security
            /*String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.customername + " LIKE '%" + userInput + "%'";
            sql += " AND " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.status + " = '" + net_status + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor=db.rawQuery("select * from "+IvokoProvider.Customer.TABLE+" where "+ "customername like ? and userid=? and status=? and isdelete=?",
                        new String [] {'%' + userInput + '%',userId,net_status,"0"});

                if (cursor.moveToFirst()) {
                    do {
                        BranchesModel dto = new BranchesModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));

                        customerList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        return customerList;
    }

    public List<BranchesModel> getCustomers(String groupId, String userId, String userInput) {
        List<BranchesModel> customerList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            /*String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.customername + " LIKE '%" + userInput + "%'";
            sql += " AND " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";*/
            try {
                //cursor = db.rawQuery(sql, null);


                cursor=db.rawQuery("select * from "+IvokoProvider.Customer.TABLE+" where "+ "customername like ? and userid=? and isdelete=?",
                        new String [] {'%' + userInput + '%',userId,"0"});


                if (cursor.moveToFirst()) {
                    do {
                        BranchesModel dto = new BranchesModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));

                        customerList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customerList;
    }

    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            else{
                cursor.close();
            }
        }
        return false;
//        Cursor cursor = db.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", new String[]{"table", tableName});
//        if (cursor.getCount() <= 0) {
//            cursor.close();
//            return false;
//        }
//        cursor.close();
//        return true;
    }


    public String getMaxModifiedDate(String groupId, String userId) {
        String maxDate = "";
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            // pending security checked

            /*String sql = "";
            sql += "SELECT MAX" + "(" + Customer.modifieddate + ") FROM " + IvokoProvider.Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";*/

            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT MAX "+ "(" + IvokoProvider.Customer.modifieddate + ")" + " FROM " + IvokoProvider.Customer.TABLE + " WHERE " + IvokoProvider.Customer.userid + " = ?"  , new String[]{userId});



                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    maxDate = cursor.getString(cursor.getColumnIndex("MAX(modifieddate)"));
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                if (cursor != null)
                    cursor.close();
            } finally {
                if (cursor != null)
                    cursor.close();
            }

        }
        return maxDate;
    }

    public String getInternalType(SQLiteDatabase db, String customerId) {
        String internaltype = "0";

        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;

            //security
            /*String sql = "";
            sql += "SELECT " + Customer.internaltype + " FROM " + IvokoProvider.Customer.TABLE;
            sql += " WHERE " + Customer.customerid + " = '" + customerId + "'";*/

            try {
                //cursor = db.rawQuery(sql, null);

                cursor = db.rawQuery("SELECT " +IvokoProvider.Customer.internaltype +  " FROM " + IvokoProvider.Customer.TABLE + " WHERE " + Customer.customerid + " = ?"  , new String[]{customerId});
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    internaltype = cursor.getString(cursor.getColumnIndex(Customer.internaltype));
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                if (cursor != null)
                    cursor.close();
            } finally {
                if (cursor != null)
                    cursor.close();
            }
        }
        return internaltype;
    }

    public void updateCustomerOnlline(BranchesModel branchesModel, String userId, String
            customerId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Customer.customerid, customerId);
        values.put(Customer.status, "");

        try {
            db.update(Customer.TABLE, values, Customer._ID + " = " + branchesModel.getId() + " AND " +
                    Customer.userid + " = ?", new String[]{userId});
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateCustomerInfo(BranchesModel branchesModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Customer.organizationtype, branchesModel.getCustomertype());
        if (branchesModel.getAddress() != null)
            values.put(Customer.address, new Gson().toJson(branchesModel.getAddress()));

        if (!TextUtils.isEmpty(branchesModel.getCustomerid())) {
            db.update(Customer.TABLE, values, Customer.customerid + "=?", new String[]{branchesModel.getCustomerid()});
        }
    }

    public void deleteCustomerRecordID(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            db.delete(Customer.TABLE, Customer.customerid + "=?", new String[]{id});
//            db.execSQL("delete from " + Customer.TABLE + " WHERE " + Customer.customerid + " = '" + id + "'");
        }
    }

    private boolean isCustomerExist(SQLiteDatabase db, String customerID) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Customer.TABLE + " WHERE " + IvokoProvider.Customer.customerid + " = ?", new String[]{customerID});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
}





/*
package com.outwork.sudlife.lite.branches.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.lite.branches.models.BranchesModel;
import com.outwork.sudlife.lite.dao.DBHelper;
import com.outwork.sudlife.lite.dao.IvokoProvider;
import com.outwork.sudlife.lite.dao.IvokoProvider.Customer;
import com.outwork.sudlife.lite.restinterfaces.POJO.GeoAddress;
import com.outwork.sudlife.lite.utilities.Utils;

*/
/**
 * Created by Panch on 2/22/2017.
 *//*

public class BranchesDao {
    public static final String TAG = BranchesDao.class.getSimpleName();

    private static BranchesDao instance;
    private Context applicationContext;

    public static BranchesDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new BranchesDao(applicationContext);
        }
        return instance;
    }

    private BranchesDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void insertCustomer(BranchesModel branchesModel, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(Customer.groupid, branchesModel.getGroupid());
        values.put(Customer.userid, branchesModel.getUserid());
        values.put(Customer.customerid, branchesModel.getCustomerid());
        values.put(Customer.customername, branchesModel.getCustomername());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBranchcode()))
            values.put(Customer.customercode, branchesModel.getBranchcode());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getPhoneno()))
            values.put(Customer.phoneno, branchesModel.getPhoneno());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBranchfoundationday()))
            values.put(Customer.branchfoundationday, branchesModel.getBranchfoundationday());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getEmail()))
            values.put(Customer.email, branchesModel.getEmail());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSudlat()))
            values.put(Customer.sudlat, branchesModel.getSudlat());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSudlong()))
            values.put(Customer.sudlong, branchesModel.getSudlong());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBanklat()))
            values.put(Customer.banklat, branchesModel.getBanklat());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBanklong()))
            values.put(Customer.banklong, branchesModel.getBanklong());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSoname()))
            values.put(Customer.soname, branchesModel.getSoname());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getRegion()))
            values.put(Customer.region, branchesModel.getRegion());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getZone()))
            values.put(Customer.zone, branchesModel.getZone());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomertype()))
            values.put(Customer.organizationtype, branchesModel.getCustomertype());
        if (branchesModel.getClassification() != null) {
            values.put(Customer.internaltype, branchesModel.getClassification());
        }
        if (Utils.isNotNullAndNotEmpty(branchesModel.getCreateddate()))
            values.put(Customer.createddate, branchesModel.getCreateddate());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getModifieddate()))
            values.put(Customer.modifieddate, branchesModel.getModifieddate());
        values.put(Customer.status, "");
        if (Utils.isNotNullAndNotEmpty(branchesModel.getAddressline1())) {
            values.put(Customer.addressline1, branchesModel.getAddressline1());
        }
        if (Utils.isNotNullAndNotEmpty(branchesModel.getLocality())) {
            values.put(Customer.locality, branchesModel.getLocality());
        }
        if (branchesModel.getAddress() != null)
            values.put(Customer.address, new Gson().toJson(branchesModel.getAddress()));
        if (branchesModel.getIsdeleted() != null) {
            if (branchesModel.getIsdeleted()) {
                values.put(Customer.isdelete, branchesModel.getIsdeleted());
            } else {
                values.put(Customer.isdelete, false);
            }
        } else {
            values.put(Customer.isdelete, false);
        }
        boolean createSuccessful = db.insert(Customer.TABLE, null, values) > 0;
        if (createSuccessful) {
            Log.e(TAG, " created.");
        }
    }

    public void insertLocalCustomer(BranchesModel branchesModel, String network_status) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Customer.groupid, branchesModel.getGroupid());
        values.put(Customer.userid, branchesModel.getUserid());
        values.put(Customer.customerid, branchesModel.getCustomerid());
        values.put(Customer.customername, branchesModel.getCustomername());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBranchcode()))
            values.put(Customer.customercode, branchesModel.getBranchcode());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getPhoneno()))
            values.put(Customer.phoneno, branchesModel.getPhoneno());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getEmail()))
            values.put(Customer.email, branchesModel.getEmail());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSudlat()))
            values.put(Customer.sudlat, branchesModel.getSudlat());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSudlong()))
            values.put(Customer.sudlong, branchesModel.getSudlong());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBanklat()))
            values.put(Customer.banklat, branchesModel.getBanklat());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBanklong()))
            values.put(Customer.banklong, branchesModel.getBanklong());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getSoname()))
            values.put(Customer.soname, branchesModel.getSoname());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getRegion()))
            values.put(Customer.region, branchesModel.getRegion());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getZone()))
            values.put(Customer.zone, branchesModel.getZone());
        if (Utils.isNotNullAndNotEmpty(branchesModel.getBranchfoundationday()))
            values.put(Customer.branchfoundationday, branchesModel.getBranchfoundationday());
        values.put(Customer.organizationtype, branchesModel.getCustomertype());
        values.put(Customer.internaltype, branchesModel.getClassification());
        values.put(Customer.status, network_status);
        if (Utils.isNotNullAndNotEmpty(branchesModel.getAddressline1())) {
            values.put(Customer.addressline1, branchesModel.getAddressline1());
        }
        if (Utils.isNotNullAndNotEmpty(branchesModel.getLocality())) {
            values.put(Customer.locality, branchesModel.getLocality());
        }
        if (branchesModel.getAddress() != null)
            values.put(Customer.address, new Gson().toJson(branchesModel.getAddress()));
        boolean createSuccessful = db.insert(Customer.TABLE, null, values) > 0;
        if (createSuccessful) {
            Log.e(TAG, " created.");
        }
    }

    public void insertCustomerList(List<BranchesModel> branchesModelList) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        try {
            for (int j = 0; j < branchesModelList.size(); j++) {
                BranchesModel branchesModel = branchesModelList.get(j);
//                if (branchesModel.getIsdeleted()) {
//                    deleteCustomerRecordID(branchesModel.getCustomerid());
//                } else {
                if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomerid()))
                    if (isCustomerExist(db, branchesModel.getCustomerid())) {
                        deleteCustomerRecordID(branchesModel.getCustomerid());
                        insertCustomer(branchesModel, db);
                    } else {
                        insertCustomer(branchesModel, db);
                    }
//                }
            }
        } finally {
        }
    }


    public void insertNewCustomerList(List<BranchesModel> branchesModelList) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        try {

            deleteAllCustomerRecords();


            for (int j = 0; j < branchesModelList.size(); j++) {
                BranchesModel branchesModel = branchesModelList.get(j);
                if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomerid()))

                insertCustomer(branchesModel, db);

            }
        } finally {
        }
    }


    public void deleteAllCustomerRecords() {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            db.execSQL("delete from " + Customer.TABLE);
            //db.delete(Customer.TABLE, Customer.customerid + "=?", new String[]{id});
//            db.execSQL("delete from " + Customer.TABLE + " WHERE " + Customer.customerid + " = '" + id + "'");
        }
    }


    public BranchesModel getCustomerData(String customerId) {
        BranchesModel dto = new BranchesModel();
        SQLiteDatabase db1 = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db1, Customer.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.customerid + " = '" + customerId + "'";
            try {
                cursor = db1.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)) != null)
                            if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equals("1")) {
                                dto.setIsdeleted(true);
                            } else if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equalsIgnoreCase("true")) {
                                dto.setIsdeleted(true);
                            } else {
                                dto.setIsdeleted(false);
                            }
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return dto;
    }

    public List<BranchesModel> getonlineCustomersList(String groupId, String userId, String status) {
        List<BranchesModel> customerList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";
            sql += " ORDER BY " + Customer.customername + " COLLATE NOCASE ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        BranchesModel dto = new BranchesModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        if (Utils.isNotNullAndNotEmpty(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday))))
                            dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)) != null)
                            if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equals("1")) {
                                dto.setIsdeleted(true);
                            } else if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equalsIgnoreCase("true")) {
                                dto.setIsdeleted(true);
                            } else {
                                dto.setIsdeleted(false);
                            }
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));
                        customerList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customerList;
    }

    public List<BranchesModel> getCustomers(String groupId, String userId) {
        List<BranchesModel> customerList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";
            sql += " ORDER BY " + Customer.customername + " COLLATE NOCASE ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        BranchesModel dto = new BranchesModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)) != null)
                            if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equals("1")) {
                                dto.setIsdeleted(true);
                            } else if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equalsIgnoreCase("true")) {
                                dto.setIsdeleted(true);
                            } else {
                                dto.setIsdeleted(false);
                            }
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));
                        customerList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customerList;
    }

    public List<BranchesModel> getCustomersbyStatus(String userId, String network_status) {
        List<BranchesModel> customerList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.status + " = '" + network_status + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";
            sql += " ORDER BY " + Customer.customername + " COLLATE NOCASE ";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        BranchesModel dto = new BranchesModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)) != null)
                            if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equals("1")) {
                                dto.setIsdeleted(true);
                            } else if (cursor.getString(cursor.getColumnIndex(Customer.isdelete)).equalsIgnoreCase("true")) {
                                dto.setIsdeleted(true);
                            } else {
                                dto.setIsdeleted(false);
                            }
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));
                        customerList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customerList;
    }

    public List<String> getCustomerNamesList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<String> customernamesList = new ArrayList<>();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT DISTINCT " + Customer.customername;
            sql += " FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";

            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        customernamesList.add(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customernamesList;
    }

    public List<String> getCustomerTypesList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<String> customertypesList = new ArrayList<>();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT DISTINCT " + Customer.organizationtype;
            sql += " FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";

            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        customertypesList.add(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customertypesList;
    }

    public List<BranchesModel> getonlineCustomers(String groupId, String userInput, String
            userId, String net_status) {
        List<BranchesModel> customerList = new ArrayList<>();
        boolean deleted = false;
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.customername + " LIKE '%" + userInput + "%'";
            sql += " AND " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.status + " = '" + net_status + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        BranchesModel dto = new BranchesModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));

                        customerList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        return customerList;
    }

    public List<BranchesModel> getCustomers(String groupId, String userId, String userInput) {
        List<BranchesModel> customerList = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT * FROM " + Customer.TABLE;
            sql += " WHERE " + Customer.customername + " LIKE '%" + userInput + "%'";
            sql += " AND " + Customer.userid + " = '" + userId + "'";
            sql += " AND " + Customer.isdelete + " = '" + 0 + "'";
            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {
                    do {
                        BranchesModel dto = new BranchesModel();
                        dto.setId(cursor.getInt(cursor.getColumnIndex(Customer._ID)));
                        dto.setCustomerid(cursor.getString(cursor.getColumnIndex(Customer.customerid)));
                        dto.setCustomername(cursor.getString(cursor.getColumnIndex(Customer.customername)));
                        dto.setCustomercode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setBranchcode(cursor.getString(cursor.getColumnIndex(Customer.customercode)));
                        dto.setPhoneno(cursor.getString(cursor.getColumnIndex(Customer.phoneno)));
                        dto.setEmail(cursor.getString(cursor.getColumnIndex(Customer.email)));
                        dto.setSudlat(cursor.getString(cursor.getColumnIndex(Customer.sudlat)));
                        dto.setSudlong(cursor.getString(cursor.getColumnIndex(Customer.sudlong)));
                        dto.setBanklat(cursor.getString(cursor.getColumnIndex(Customer.banklat)));
                        dto.setBanklong(cursor.getString(cursor.getColumnIndex(Customer.banklong)));
                        dto.setRegion(cursor.getString(cursor.getColumnIndex(Customer.region)));
                        dto.setSoname(cursor.getString(cursor.getColumnIndex(Customer.soname)));
                        dto.setZone(cursor.getString(cursor.getColumnIndex(Customer.zone)));
                        dto.setCustomertype(cursor.getString(cursor.getColumnIndex(Customer.organizationtype)));
                        dto.setClassification(cursor.getInt(cursor.getColumnIndex(Customer.internaltype)));
                        dto.setModifieddate(cursor.getString(cursor.getColumnIndex(Customer.modifieddate)));
                        dto.setNetwork_status(cursor.getString(cursor.getColumnIndex(Customer.status)));
                        dto.setAddressline1(cursor.getString(cursor.getColumnIndex(Customer.addressline1)));
                        dto.setLocality(cursor.getString(cursor.getColumnIndex(Customer.locality)));
                        dto.setBranchfoundationday(cursor.getString(cursor.getColumnIndex(Customer.branchfoundationday)));
                        dto.setAddress(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(Customer.address)), GeoAddress.class));

                        customerList.add(dto);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return customerList;
    }

    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            } else {
                cursor.close();
            }
        }
        return false;
//        Cursor cursor = db.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", new String[]{"table", tableName});
//        if (cursor.getCount() <= 0) {
//            cursor.close();
//            return false;
//        }
//        cursor.close();
//        return true;
    }


    public String getMaxModifiedDate(String groupId, String userId) {
        String maxDate = "";
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT MAX" + "(" + Customer.modifieddate + ") FROM " + IvokoProvider.Customer.TABLE;
            sql += " WHERE " + Customer.userid + " = '" + userId + "'";

            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    maxDate = cursor.getString(cursor.getColumnIndex("MAX(modifieddate)"));
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                if (cursor != null)
                    cursor.close();
            } finally {
                if (cursor != null)
                    cursor.close();
            }

        }
        return maxDate;
    }

    public String getInternalType(SQLiteDatabase db, String customerId) {
        String internaltype = "0";

        if (isTableExists(db, Customer.TABLE)) {
            Cursor cursor = null;
            String sql = "";
            sql += "SELECT " + Customer.internaltype + " FROM " + IvokoProvider.Customer.TABLE;
            sql += " WHERE " + Customer.customerid + " = '" + customerId + "'";

            try {
                cursor = db.rawQuery(sql, null);
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    internaltype = cursor.getString(cursor.getColumnIndex(Customer.internaltype));
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                if (cursor != null)
                    cursor.close();
            } finally {
                if (cursor != null)
                    cursor.close();
            }
        }
        return internaltype;
    }

    public void updateCustomerOnlline(BranchesModel branchesModel, String userId, String
            customerId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Customer.customerid, customerId);
        values.put(Customer.status, "");

        try {
            db.update(Customer.TABLE, values, Customer._ID + " = " + branchesModel.getId() + " AND " +
                    Customer.userid + " = ?", new String[]{userId});
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateCustomerInfo(BranchesModel branchesModel) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Customer.organizationtype, branchesModel.getCustomertype());
        if (branchesModel.getAddress() != null)
            values.put(Customer.address, new Gson().toJson(branchesModel.getAddress()));

        if (!TextUtils.isEmpty(branchesModel.getCustomerid())) {
            db.update(Customer.TABLE, values, Customer.customerid + "=?", new String[]{branchesModel.getCustomerid()});
        }
    }

    public void deleteCustomerRecordID(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, Customer.TABLE)) {
            db.delete(Customer.TABLE, Customer.customerid + "=?", new String[]{id});
//            db.execSQL("delete from " + Customer.TABLE + " WHERE " + Customer.customerid + " = '" + id + "'");
        }
    }

    private boolean isCustomerExist(SQLiteDatabase db, String customerID) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Customer.TABLE + " WHERE " + IvokoProvider.Customer.customerid + " = ?", new String[]{customerID});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
}*/
