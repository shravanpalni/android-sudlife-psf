package com.outwork.sudlife.psf.ui.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.services.ContactsIntentService;
import com.outwork.sudlife.psf.dto.DeviceInfo;
import com.outwork.sudlife.psf.dto.UserObjectDto;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.planner.service.PlannerIntentService;
import com.outwork.sudlife.psf.planner.service.SupervisorIntentService;
import com.outwork.sudlife.psf.localbase.services.ProductListDownloadService;
import com.outwork.sudlife.psf.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.restinterfaces.UserRestInterface;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.activities.DashBoardActivity;
import com.outwork.sudlife.psf.ui.activities.SignInActvity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {
    public static final String TAG = SplashActivity.class.getSimpleName();

    private IvokoApplication application;
    private static int SPLASH_TIME_OUT = 3000;
    private UserObjectDto userObjectDto;
    //private MixpanelAPI mixpanel;
    private DeviceInfo deviceInfo;
    private String apprelease = "";
    private String appversion = "";
    private String libversion = "";
    private String os = "";
    private String osversion = "";
    private String model = "";
    private String manufacturer = "";
    private String currentVersion = null;
    private String playstoreVersion = null;
    private String isMandate = null;
    private Dialog updatedialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        application = (IvokoApplication) getApplication();
        String projectToken = application.getApplicationContext().getString(R.string.mixpaneltoken);
        //mixpanel = MixpanelAPI.getInstance(SplashActivity.this, projectToken);
        Log.i("shravan", "utoken====" + SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        userObjectDto = new UserObjectDto(SplashActivity.this);
        deviceInfo = Utils.getInfosAboutDevice(SplashActivity.this, 1);
        apprelease = deviceInfo.getApprelease();
        appversion = deviceInfo.getAppversioncode();
        libversion = deviceInfo.getLibversion();
        os = deviceInfo.getOs();
        osversion = deviceInfo.getOsversion();
        model = deviceInfo.getModel();
        manufacturer = deviceInfo.getManufacturer();
        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            //Log.i("Splash","Playstore version = = = ="+currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (isNetworkAvailable()) {
            getVersionHistory(currentVersion, "android");

            new GetVersionCode().execute();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                        initServices();
                        Intent in = new Intent(SplashActivity.this, DashBoardActivity.class);
                        startActivity(in);
                        finish();
                    } else {
                        Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                        startActivity(in);
                        finish();
                    }
                }
            }, SPLASH_TIME_OUT);
        }
    }

    private void initServices() {
        if (isNetworkAvailable()) {

            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                LeadIntentService.insertPSFLeadstoServer(SplashActivity.this);
            }
            //LeadIntentService.insertPSFLeadstoServer(SplashActivity.this);
            LeadIntentService.insertPSFNotifications(SplashActivity.this);
           // LeadIntentService.insertLeadstoServer(SplashActivity.this);
            /*LeadIntentService.insertProposalCodes(SplashActivity.this);
            ProductListDownloadService.getProductListinDB(SplashActivity.this);
            ProductListDownloadService.getMasterListinDB(SplashActivity.this);
            ContactsIntentService.insertCustomersinDB(SplashActivity.this);
            SupervisorIntentService.insertSupervisorsList(SplashActivity.this);
            OpportunityIntentService.insertOpportunityStages(SplashActivity.this);*/


        }
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName() + "&hl=en")
                        .timeout(6000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                    initServices();
                    Intent in = new Intent(SplashActivity.this, DashBoardActivity.class);
                    startActivity(in);
                    finish();

                } else {
                    Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                    startActivity(in);
                    finish();
                }
            }
            return newVersion;
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                playstoreVersion = onlineVersion;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                            initServices();
                            if (currentVersion != null && playstoreVersion != null && isMandate != null && !currentVersion.equalsIgnoreCase(playstoreVersion) && isMandate.equalsIgnoreCase("true")) {
                                showUpdateDialog();
                            } else {
                                Intent in = new Intent(SplashActivity.this, DashBoardActivity.class);
                                startActivity(in);
                                finish();
                            }
                        } else {
                            if (currentVersion != null && playstoreVersion != null && isMandate != null && !currentVersion.equalsIgnoreCase(playstoreVersion) && isMandate.equalsIgnoreCase("true")) {
                                showUpdateDialog();
                            } else {
                                Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                                startActivity(in);
                                finish();
                            }
                        }
                    }
                }, SPLASH_TIME_OUT);
            }
        }
    }

    private void showUpdateDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle("A New Update is Available");
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferenceManager.getInstance().putString(Constants.BRANCHES_LAST_FETCH_TIME, "");
                SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LAST_FETCH_TIME, "");
                SharedPreferenceManager.getInstance().putString(Constants.LEAD_LAST_FETCH_TIME, "");
                /*startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                        ("market://details?id=com.outwork.sudlife.psf&hl=en")));*/
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                        ("https://play.google.com/store/apps/details?id=com.outwork.sudlife.psf&hl=en")));
                dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        updatedialog = builder.show();
    }

    private void getVersionHistory(String versioncode, String opSys) {
        UserRestInterface client = RestService.createServicev1(UserRestInterface.class);
        Call<RestResponse> user = client.versionHistory(versioncode, opSys);

        user.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    RestResponse restResponse = response.body();
                    String status = restResponse.getStatus();
                    String data = restResponse.getData();
                    if (status.equalsIgnoreCase("Success")) {
                        try {
                            JSONObject jsonObject = new JSONObject(data);
                            isMandate = jsonObject.getString("isupgrademandatory");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                            initServices();
                            Intent in = new Intent(SplashActivity.this, DashBoardActivity.class);
                            startActivity(in);
                            finish();
                        } else {
                            Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                            startActivity(in);
                            finish();
                        }
                    }
                } else {
                    if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                        initServices();
                        Intent in = new Intent(SplashActivity.this, DashBoardActivity.class);
                        startActivity(in);
                        finish();
                    } else {
                        Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                        startActivity(in);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                    initServices();
                    Intent in = new Intent(SplashActivity.this, DashBoardActivity.class);
                    startActivity(in);
                    finish();
                } else {
                    Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                    startActivity(in);
                    finish();
                }
            }
        });
    }
}
