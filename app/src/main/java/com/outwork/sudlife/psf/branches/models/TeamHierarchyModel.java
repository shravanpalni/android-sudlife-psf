package com.outwork.sudlife.psf.branches.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TeamHierarchyModel implements Serializable {
    @SerializedName("supervisordisplayrole")
    @Expose
    private String supervisordisplayrole;
    @SerializedName("teampathlevel")
    @Expose
    private Integer teampathlevel;
    @SerializedName("supervisorid")
    @Expose
    private String supervisorid;
    @SerializedName("supervisorname")
    @Expose
    private String supervisorname;
    @SerializedName("teamid")
    @Expose
    private String teamid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("hasparent")
    @Expose
    private Boolean hasparent;
    @SerializedName("parentteamid")
    @Expose
    private String parentteamid;
    @SerializedName("haschildren")
    @Expose
    private Boolean haschildren;
    @SerializedName("iconurl")
    @Expose
    private String iconurl;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("childlayers")
    @Expose
    private List<Childlayer> childlayers = null;

    public String getSupervisordisplayrole() {
        return supervisordisplayrole;
    }

    public void setSupervisordisplayrole(String supervisordisplayrole) {
        this.supervisordisplayrole = supervisordisplayrole;
    }

    public Integer getTeampathlevel() {
        return teampathlevel;
    }

    public void setTeampathlevel(Integer teampathlevel) {
        this.teampathlevel = teampathlevel;
    }

    public String getSupervisorid() {
        return supervisorid;
    }

    public void setSupervisorid(String supervisorid) {
        this.supervisorid = supervisorid;
    }

    public String getSupervisorname() {
        return supervisorname;
    }

    public void setSupervisorname(String supervisorname) {
        this.supervisorname = supervisorname;
    }

    public String getTeamid() {
        return teamid;
    }

    public void setTeamid(String teamid) {
        this.teamid = teamid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getHasparent() {
        return hasparent;
    }

    public void setHasparent(Boolean hasparent) {
        this.hasparent = hasparent;
    }

    public String getParentteamid() {
        return parentteamid;
    }

    public void setParentteamid(String parentteamid) {
        this.parentteamid = parentteamid;
    }

    public Boolean getHaschildren() {
        return haschildren;
    }

    public void setHaschildren(Boolean haschildren) {
        this.haschildren = haschildren;
    }

    public String getIconurl() {
        return iconurl;
    }

    public void setIconurl(String iconurl) {
        this.iconurl = iconurl;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public List<Childlayer> getChildlayers() {
        return childlayers;
    }

    public void setChildlayers(List<Childlayer> childlayers) {
        this.childlayers = childlayers;
    }
}
