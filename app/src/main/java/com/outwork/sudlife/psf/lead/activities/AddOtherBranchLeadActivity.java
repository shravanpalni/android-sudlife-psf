package com.outwork.sudlife.psf.lead.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.models.BranchesModel;
import com.outwork.sudlife.psf.lead.model.OtherBranchModel;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class AddOtherBranchLeadActivity extends BaseActivity {
    public static final String TAG = AddOtherBranchLeadActivity.class.getSimpleName();

    private EditText branchcode, branchname;
    private AppCompatSpinner bankname;
    private Button submit;
    private BranchesModel branchesModel = new BranchesModel();
    private List<OtherBranchModel> otherBranchModelList = new ArrayList<>();
    private String[] bankNamesList = {"BOI", "UBI"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_branch_lead);

        initToolBar();
        initViews();
        setListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("New Branch");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private void initViews() {
        branchname = (EditText) findViewById(R.id.branchName);
        bankname = (AppCompatSpinner) findViewById(R.id.bankName);
        branchcode = (EditText) findViewById(R.id.branchcode);
//        readWeatherData();
//        List<String> branchnameList = new ArrayList<>();
//        for (OtherBranchModel branchname : otherBranchModelList) {
//            branchnameList.add(branchname.getBranchname());
//        }
//        ArrayAdapter<String> branchnameadapter = new ArrayAdapter<String>
//                (this, android.R.layout.select_dialog_item, branchnameList);
//        branchname.setThreshold(1);//will start working from first character
//        branchname.setAdapter(branchnameadapter);
//
//        List<String> banknameList = new ArrayList<>();
//        for (OtherBranchModel bankname : otherBranchModelList) {
//            banknameList.add(bankname.getBankname());
//        }
//        ArrayAdapter<String> banknameadapter = new ArrayAdapter<String>
//                (this, android.R.layout.select_dialog_item, banknameList);
//        branchname.setThreshold(1);//will start working from first character
//        branchname.setAdapter(banknameadapter);

        submit = (Button) findViewById(R.id.submit);

        ArrayAdapter<String> customerTypeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, bankNamesList);
        customerTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bankname.setAdapter(customerTypeAdapter);
        bankname.setSelection(0);
        bankname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bankname.setSelection(position);
                branchesModel.setCustomertype(bankname.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setListeners() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNotNullAndNotEmpty(branchname.getText().toString())) {
                    branchname.setError("Branch Name is Mandatory");
                    branchname.requestFocus();
                } else if (!Utils.isNotNullAndNotEmpty(branchcode.getText().toString())) {
                    branchcode.setError("Branch Code is Mandatory");
                    branchcode.requestFocus();
                } else if (branchcode.getText().toString().startsWith("0")) {
                    branchcode.setError("Invalid Branch Code");
                    branchcode.requestFocus();
                } else {
                    branchesModel.setGroupid(groupId);
                    branchesModel.setUserid(userid);
                    branchesModel.setCustomername(branchname.getText().toString());
//                    branchesModel.setCustomertype(bankname.getText().toString());
                    branchesModel.setCustomercode(branchcode.getText().toString());
                    Intent in = new Intent(AddOtherBranchLeadActivity.this, CreateNewLeadActivity.class);
                    in.putExtra("customer", new Gson().toJson(branchesModel));
                    startActivity(in);
                    finish();
                }
            }
        });
    }

//    private void readWeatherDataByColumn() {
//        InputStream is = getResources().openRawResource(R.raw.branchlist);
//        BufferedReader br = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
//        String line = "";
//        try {
//            while ((line = br.readLine()) != null) {
//                String[] cols = line.split(",");
//
//                System.out.println("Coulmn 0 = '" + cols[0] + "', Column 1 = '" + cols[1] + "', Column 2: '" + cols[2] + "'");
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    private void readWeatherData() {
//        InputStream is = getResources().openRawResource(R.raw.branchlist);
//        BufferedReader reader = new BufferedReader(
//                new InputStreamReader(is, Charset.forName("UTF-8")));
//        String line = "";
//        try {
//            reader.readLine();
//            while ((line = reader.readLine()) != null) {
//                Log.i(TAG, "Line: " + line);
//                String[] tokens = line.split(",");
//                OtherBranchModel otherBranchModel = new OtherBranchModel();
//                otherBranchModel.setBranchname(tokens[0]);
//                otherBranchModel.setBranchcode(String.valueOf(tokens[1]));
//                otherBranchModel.setBankname(tokens[2]);
//                otherBranchModelList.add(otherBranchModel);
//                Log.i(TAG, "Just created: " + otherBranchModelList);
//            }
//        } catch (IOException e) {
//            Log.wtf(TAG, "Error reading data file on line" + line, e);
//            e.printStackTrace();
//        }
//    }
}
