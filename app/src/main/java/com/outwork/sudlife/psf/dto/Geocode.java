package com.outwork.sudlife.psf.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.psf.dto.geocode.Result;
import com.outwork.sudlife.psf.dto.geocode.STATUS;

/**
 * Created by Panch on 2/17/2017.
 */
public class Geocode {
    @SerializedName("results")
    @Expose
    private List<Result> results = new ArrayList<>();
    @SerializedName("status")
    @Expose
    private STATUS status;

    /**
     * @return The results
     */
    public List<Result> getResults() {
        return results;
    }

    /**
     * @param results The results
     */
    public void setResults(List<Result> results) {
        this.results = results;
    }

    /**
     * @return The status
     */
    public STATUS getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(STATUS status) {
        this.status = status;
    }
}
