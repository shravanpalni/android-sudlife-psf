package com.outwork.sudlife.psf.opportunity.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.notifications.GlobalData;
import com.outwork.sudlife.psf.planner.models.PlannerModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Panch on 2/23/2017.
 */

public class ActivitiesAdapter extends RecyclerView.Adapter<ActivitiesAdapter.TaskViewHolder> {
    private Context context;
    private final LayoutInflater mInflater;
    private List<PlannerModel> plannerModelList = new ArrayList<>();

    public static class TaskViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public TextView name;
        private CardView card_view;
        public TextView line2, status;

        public TaskViewHolder(View v) {
            super(v);
            picture = (ImageView) v.findViewById(R.id.profileImage);
            name = (TextView) v.findViewById(R.id.contactname);
            line2 = (TextView) v.findViewById(R.id.contactline2);
            status = (TextView) v.findViewById(R.id.status);
            card_view = (CardView) v.findViewById(R.id.card_view);
        }
    }

    public ActivitiesAdapter(Context context, List<PlannerModel> plannerModelList) {
        this.context = context;
        this.plannerModelList = plannerModelList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_plan, parent, false);
        return new TaskViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, final int position) {
        final PlannerModel plannerModel = (PlannerModel) this.plannerModelList.get(position);
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color2 = generator.getColor(plannerModel.getSubtype().substring(0, 1));
        TextDrawable drawable = TextDrawable.builder().buildRound(plannerModel.getSubtype().substring(0, 1), color2);
        if (drawable != null) {
            holder.picture.setImageDrawable(drawable);
        }
        holder.name.setText(plannerModel.getSubtype());
        holder.line2.setText(GlobalData.getInstance().getLeadModel().getTitle());

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return plannerModelList.size();
    }
}