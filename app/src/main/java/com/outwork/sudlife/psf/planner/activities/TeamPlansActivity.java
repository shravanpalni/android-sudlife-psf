package com.outwork.sudlife.psf.planner.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.planner.adapter.TeamMembersAdapter;
import com.outwork.sudlife.psf.planner.adapter.TeamPlanAdapter;
import com.outwork.sudlife.psf.planner.models.TeamMemberPlannerModel;
import com.outwork.sudlife.psf.planner.models.TeamMembers;
import com.outwork.sudlife.psf.planner.service.PlannerService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.targets.services.RestServicesForTragets;
import com.outwork.sudlife.psf.ui.base.AppBaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;


/**
 * Created by shravanch on 29-05-2018.
 */

public class TeamPlansActivity extends AppBaseActivity {

    private RecyclerView tpRecyclerView;
    private SwipeRefreshLayout tpSwipeRefreshLayout;
    private TeamPlanAdapter teamPlanAdapter = null;
    ArrayList<TeamMembers> teamMembersArrayList = new ArrayList<>();
    private Spinner dynamicSpinner;
    private TextView toolbar_title, team_task_nomessages, toolbar_month;
    private ImageButton toolbar_calendar_icon;
    private LinearLayout toolbar_calendar_layout;
    private List<TeamMemberPlannerModel> teamMemberplannerMList = new ArrayList<>();
    private String userId = "";
    private ProgressDialog progressBar;
    String[] listContent = {
            "January", "February", "March", "April",
            "May", "June", "July", "August", "September",
            "October", "November", "December"};
    static final int CUSTOM_DIALOG_ID = 0;
    ListView dialog_ListView;
    private List<String> calendarMonthdatesList = new ArrayList<>();
    private String selectedMonthName = null;
    private boolean doubleBackToExitPressedOnce = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teamplan);
        initToolBar();
        team_task_nomessages = (TextView) findViewById(R.id.team_task_nomessages);
        tpRecyclerView = (RecyclerView) findViewById(R.id.team_task_recyclerview);
        tpSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.team_task_swipe_refresh_layout);


        tpSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (selectedMonthName == null) {
                    if (!userId.equalsIgnoreCase("")) {
                        Calendar cal = Calendar.getInstance();
                        SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
                        String monthNumber = outputFormat.format(cal.getTime());
                        Log.i("Team", "monthNumber" + monthNumber);
                        getCalendarMonthDates(monthNumber);
                        // dateList = Utils.printDatesInMonth(year, monthno + 1);
                        if (isNetworkAvailable()) {
                            getTeamPlannerData(userId, Utils.formatDateFromString("yyyy-MM-dd", "MM", TimeUtils.getCurrentDate("yyyy-MM-dd")),
                                    Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd")));
                        } else {
                            Utils.noNetworkDialog(TeamPlansActivity.this);
                        }

                    }

                    Calendar mCalendar = Calendar.getInstance();
                    String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                    toolbar_month.setText(month);

                } else {

                    if (!userId.equalsIgnoreCase("")) {


                        SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
                        Calendar cal = Calendar.getInstance();
                        try {
                            cal.setTime(inputFormat.parse(selectedMonthName));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
                        String monthNumber = outputFormat.format(cal.getTime());
                        Log.i("Team", "monthNumber" + monthNumber);
                        int year = Calendar.getInstance().get(Calendar.YEAR);
                        String currentYear = String.valueOf(year);
                        Log.i("Team", "year" + year);

                        getCalendarMonthDates(monthNumber);
                        if (isNetworkAvailable()) {
                            getTeamPlannerData(userId, monthNumber, currentYear);
                        } else {
                            Utils.noNetworkDialog(TeamPlansActivity.this);
                        }
                        toolbar_month.setText(selectedMonthName);


                    }


                }


                tpSwipeRefreshLayout.setRefreshing(false);
            }
        });


        dynamicSpinner = (Spinner) findViewById(R.id.dynamic_spinner);

        getTeamMembers();

        dynamicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i("Team plans", "item selected====" + position);
                userId = teamMembersArrayList.get(position).getUserid();
                if (selectedMonthName == null) {
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
                    String monthNumber = outputFormat.format(cal.getTime());
                    Log.i("Team", "monthNumber" + monthNumber);

                    getCalendarMonthDates(monthNumber);

                    if (isNetworkAvailable()) {
                        getTeamPlannerData(userId, Utils.formatDateFromString("yyyy-MM-dd", "MM", TimeUtils.getCurrentDate("yyyy-MM-dd")), Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd")));

                    } else {
                        Utils.noNetworkDialog(TeamPlansActivity.this);
                    }

                    Calendar mCalendar = Calendar.getInstance();
                    String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                    toolbar_month.setText(month);
                } else {

                    SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
                    Calendar cal = Calendar.getInstance();
                    try {
                        cal.setTime(inputFormat.parse(selectedMonthName));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
                    String monthNumber = outputFormat.format(cal.getTime());
                    Log.i("Team", "monthNumber" + monthNumber);
                    int year = Calendar.getInstance().get(Calendar.YEAR);
                    String currentYear = String.valueOf(year);
                    Log.i("Team", "year" + year);

                    getCalendarMonthDates(monthNumber);
                    if (isNetworkAvailable()) {
                        getTeamPlannerData(userId, monthNumber, currentYear);
                    } else {
                        Utils.noNetworkDialog(TeamPlansActivity.this);
                    }
                    toolbar_month.setText(selectedMonthName);


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = false;
            showToast("Please click BACK again to exit.");
        } else {
            finish();
        }
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tmtoolbar);
        toolbar_calendar_layout = (LinearLayout) findViewById(R.id.calendar_layout);
        toolbar_title = (TextView) findViewById(R.id.tm_toolbar_title);
        toolbar_title.setText("Team Plans");
        toolbar_month = (TextView) findViewById(R.id.tm_month_display);

        toolbar_calendar_icon = (ImageButton) findViewById(R.id.calender_icon);

        Calendar mCalendar = Calendar.getInstance();
        String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        toolbar_month.setText(month);

        toolbar_calendar_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog(CUSTOM_DIALOG_ID);

            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }


    @Override
    protected Dialog onCreateDialog(int id) {

        Dialog dialog = null;

        switch (id) {
            case CUSTOM_DIALOG_ID:
                dialog = new Dialog(TeamPlansActivity.this);
                dialog.setContentView(R.layout.dialoglayout);
                dialog.setTitle("Custom Dialog");

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);

                dialog.setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });

                dialog.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {

                    }
                });

                //Prepare ListView in dialog
                dialog_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
                ArrayAdapter<String> adapter
                        = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, listContent);
                dialog_ListView.setAdapter(adapter);
                dialog_ListView.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {


                        toolbar_month.setText(parent.getItemAtPosition(position).toString());
                        selectedMonthName = parent.getItemAtPosition(position).toString();

                        SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
                        Calendar cal = Calendar.getInstance();
                        try {
                            cal.setTime(inputFormat.parse(selectedMonthName));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
                        String monthNumber = outputFormat.format(cal.getTime());
                        Log.i("Team", "monthNumber" + monthNumber);
                        int year = Calendar.getInstance().get(Calendar.YEAR);
                        String currentYear = String.valueOf(year);
                        Log.i("Team", "year" + year);

                        getCalendarMonthDates(monthNumber);
                        if (isNetworkAvailable()) {
                            getTeamPlannerData(userId, monthNumber, currentYear);
                        } else {
                            Utils.noNetworkDialog(TeamPlansActivity.this);
                        }

                        dismissDialog(CUSTOM_DIALOG_ID);

                    }
                });

                break;
        }

        return dialog;
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog, Bundle bundle) {
        super.onPrepareDialog(id, dialog, bundle);

        switch (id) {

            case CUSTOM_DIALOG_ID:

                break;
        }

    }


    private void getTeamPlannerData(String userId, String month, String year) {
        Log.i("TEST", "" + userid + month + year);
        showProgressDialog("loading . . .");

        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> getTeamMemberplans = client.getTeamMemberPlans(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), userId, month, year);
        try {
            getTeamMemberplans.enqueue(new Callback<RestResponse>() {
                @Override
                public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                    if (response.isSuccessful())

                        if (teamMemberplannerMList.size() > 0) {
                            teamMemberplannerMList.clear();
                        }

                    if (Utils.isNotNullAndNotEmpty(response.body().getData().toString())) {

                        Log.i("TEST", "++++" + response.body().getData().toString());

                        try {
                            Type listType = new TypeToken<ArrayList<TeamMemberPlannerModel>>() {
                            }.getType();

                            teamMemberplannerMList = new Gson().fromJson(response.body().getData(), listType);
                            Log.i("TEST", "++++" + teamMemberplannerMList.size());
                            if (teamMemberplannerMList.size() > 0) {
                                team_task_nomessages.setVisibility(View.GONE);

                                Log.i("TeamPlanActivity", "+++++++++++++");

                                Collections.sort(teamMemberplannerMList, new Comparator<TeamMemberPlannerModel>() {
                                    @Override
                                    public int compare(TeamMemberPlannerModel lhs, TeamMemberPlannerModel rhs) {
                                        return lhs.getScheduleday().compareTo(rhs.getScheduleday()); // ascending date
                                        //return rhs.getScheduleday().compareTo(lhs.getScheduleday()); // descending date
                                    }
                                });

                                tpRecyclerView.setVisibility(View.VISIBLE);
                                for (int j = 0; teamMemberplannerMList.size() > j; j++) {
                                    Log.i("TeamPlanActivity", "+++++++++++++" + teamMemberplannerMList.get(j).getScheduleday());
                                    int size = calendarMonthdatesList.size();
                                    for (int i = 0; size > i; i++) {
                                        if (calendarMonthdatesList.get(i).equalsIgnoreCase(teamMemberplannerMList.get(j).getScheduleday().toString())) {
                                            calendarMonthdatesList.remove(i);
                                            break;
                                        }
                                    }
                                }

                                for (int k = 0; calendarMonthdatesList.size() > k; k++) {
                                    TeamMemberPlannerModel teamMemberPlannerModel = new TeamMemberPlannerModel();
                                    teamMemberPlannerModel.setScheduleday(Integer.valueOf(calendarMonthdatesList.get(k)));
                                    teamMemberPlannerModel.setActivitytype("No plans for this date");
                                    teamMemberPlannerModel.setSubtype("No plans for this date");
                                    teamMemberplannerMList.add(teamMemberPlannerModel);
                                }

                                Collections.sort(teamMemberplannerMList, new Comparator<TeamMemberPlannerModel>() {
                                    @Override
                                    public int compare(TeamMemberPlannerModel lhs, TeamMemberPlannerModel rhs) {
                                        return lhs.getScheduleday().compareTo(rhs.getScheduleday()); // ascending date
                                        //return rhs.getScheduleday().compareTo(lhs.getScheduleday()); // descending date
                                    }
                                });


                                teamPlanAdapter = new TeamPlanAdapter(TeamPlansActivity.this, teamMemberplannerMList, calendarMonthdatesList);
                                tpRecyclerView.setAdapter(teamPlanAdapter);
                                tpRecyclerView.getAdapter().notifyDataSetChanged();
                                tpRecyclerView.setLayoutManager(new LinearLayoutManager(TeamPlansActivity.this));
                                dismissProgressDialog();
                            } else {
                                team_task_nomessages.setVisibility(View.VISIBLE);
                                //tpRecyclerView.getAdapter().notifyDataSetChanged();
                                tpRecyclerView.setVisibility(View.GONE);
                                tpRecyclerView.invalidate();
                                dismissProgressDialog();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            dismissProgressDialog();
                        }

                    } else {
                        team_task_nomessages.setVisibility(View.VISIBLE);
                        if (teamPlanAdapter != null) {
                            tpRecyclerView.getAdapter().notifyDataSetChanged();
                        }

                        //tpRecyclerView.invalidate();
                        // tpRecyclerView.setVisibility(View.GONE);
                        dismissProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<RestResponse> call, Throwable t) {
                    dismissProgressDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getTeamMembers() {
        showProgressDialog("loading . . .");
        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamMembers(userToken);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful())
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        Type listType = new TypeToken<ArrayList<TeamMembers>>() {
                        }.getType();
                        ArrayList<TeamMembers> tmArrayList = new Gson().fromJson(response.body().getData(), listType);
                        if (tmArrayList != null) {
                            if (tmArrayList.size() > 0) {
                                teamMembersArrayList = new ArrayList();
                                for (int i = 0; i < tmArrayList.size(); i++) {
                                    if (tmArrayList.get(i).getRole().equalsIgnoreCase("member")) {
                                        teamMembersArrayList.add(tmArrayList.get(i));
                                    }
                                }
                                if (teamMembersArrayList.size() != 0) {
                                    dynamicSpinner.setVisibility(View.VISIBLE);
                                    TeamMembersAdapter teamMembersAdapter = new TeamMembersAdapter(TeamPlansActivity.this, teamMembersArrayList);
                                    dynamicSpinner.setAdapter(teamMembersAdapter);
                                    dismissProgressDialog();
                                }
                            } else {
                                dynamicSpinner.setVisibility(View.GONE);
                                team_task_nomessages.setVisibility(View.VISIBLE);
                                dismissProgressDialog();
                            }
                        } else {
                            dynamicSpinner.setVisibility(View.GONE);
                            team_task_nomessages.setVisibility(View.VISIBLE);
                            dismissProgressDialog();
                        }
                    }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }

    private void getCalendarMonthDates(String monthNumber) {


        Calendar cal = Calendar.getInstance();
        int month = Integer.parseInt(monthNumber);
        //int month = cal.get(Calendar.MONTH);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(df.format(cal.getTime()));
        Log.i("Team", "df = = =" + df.format(cal.getTime()));
        calendarMonthdatesList.clear();
        for (int i = 0; i < maxDay; i++) {
            cal.set(Calendar.DAY_OF_MONTH, i + 1);
            System.out.println(df.format(cal.getTime()));

            String date = df.format(cal.getTime());
            String subdate = date.substring(8);
            System.out.println(date.substring(8));


            if (subdate.substring(0, 1).equalsIgnoreCase("0")) {
                calendarMonthdatesList.add(subdate.substring(1));
            } else {
                calendarMonthdatesList.add(subdate);
            }

            for (int z = 0; calendarMonthdatesList.size() > z; z++) {
                Log.i("", "+++++" + calendarMonthdatesList.get(z));
            }

            Collections.sort(calendarMonthdatesList, Collections.reverseOrder());

            Log.i("Team", "df for loop = = =" + df.format(cal.getTime()));
            Log.i("Team", "subdate for loop = = =" + subdate);
            Log.i("Team", "month = = =" + String.valueOf(month));
        }
    }


}














/*
package com.outwork.sudlife.lite.planner.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.lite.R;
import com.outwork.sudlife.lite.planner.adapter.TeamMembersAdapter;
import com.outwork.sudlife.lite.planner.adapter.TeamPlanAdapter;
import com.outwork.sudlife.lite.planner.models.TeamMemberPlannerModel;
import com.outwork.sudlife.lite.planner.models.TeamMembers;
import com.outwork.sudlife.lite.planner.service.PlannerService;
import com.outwork.sudlife.lite.restinterfaces.RestResponse;
import com.outwork.sudlife.lite.restinterfaces.RestService;
import com.outwork.sudlife.lite.targets.services.RestServicesForTragets;
import com.outwork.sudlife.lite.ui.base.AppBaseActivity;
import com.outwork.sudlife.lite.utilities.Constants;
import com.outwork.sudlife.lite.utilities.SharedPreferenceManager;
import com.outwork.sudlife.lite.utilities.TimeUtils;
import com.outwork.sudlife.lite.utilities.Utils;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Toast;


*/
/**
 * Created by shravanch on 29-05-2018.
 *//*


public class TeamPlansActivity extends AppBaseActivity {

    private RecyclerView tpRecyclerView;
    private SwipeRefreshLayout tpSwipeRefreshLayout;
    private TeamPlanAdapter teamPlanAdapter;
    ArrayList<TeamMembers> teamMembersArrayList = new ArrayList<>();
    private Spinner dynamicSpinner;
    private TextView toolbar_title, team_task_nomessages, toolbar_month;
    private ImageButton toolbar_calendar_icon;
    private LinearLayout toolbar_calendar_layout;
    private List<TeamMemberPlannerModel> teamMemberplannerMList = new ArrayList<>();
    private String userId = "";
    private ProgressDialog progressBar;
    String[] listContent = {
            "January", "February", "March", "April",
            "May", "June", "July", "August", "September",
            "October", "November", "December"};
    static final int CUSTOM_DIALOG_ID = 0;
    ListView dialog_ListView;
    private List<String> calendarMonthdatesList = new ArrayList<>();
    private  String selectedMonthName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teamplan);
        initToolBar();
        team_task_nomessages = (TextView) findViewById(R.id.team_task_nomessages);
        tpRecyclerView = (RecyclerView) findViewById(R.id.team_task_recyclerview);
        tpSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.team_task_swipe_refresh_layout);


        tpSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {


                if (selectedMonthName == null){

                    if (!userId.equalsIgnoreCase("")) {

                        Calendar cal = Calendar.getInstance();
                        SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
                        String monthNumber = outputFormat.format(cal.getTime());
                        Log.i("Team", "monthNumber" + monthNumber);
                        getCalendarMonthDates(monthNumber);
                        // dateList = Utils.printDatesInMonth(year, monthno + 1);

                        getTeamPlannerData(userId, Utils.formatDateFromString("yyyy-MM-dd", "MM", TimeUtils.getCurrentDate("yyyy-MM-dd")),
                                Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd")));
                    }

                    Calendar mCalendar = Calendar.getInstance();
                    String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                    toolbar_month.setText(month);

                }else {

                    if (!userId.equalsIgnoreCase("")) {


                        SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
                        Calendar cal = Calendar.getInstance();
                        try {
                            cal.setTime(inputFormat.parse(selectedMonthName));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
                        String monthNumber = outputFormat.format(cal.getTime());
                        Log.i("Team", "monthNumber" + monthNumber);
                        int year = Calendar.getInstance().get(Calendar.YEAR);
                        String currentYear = String.valueOf(year);
                        Log.i("Team", "year" + year);

                        getCalendarMonthDates(monthNumber);
                        getTeamPlannerData(userId,monthNumber,currentYear);
                        toolbar_month.setText(selectedMonthName);




                    }






                }







                tpSwipeRefreshLayout.setRefreshing(false);
            }
        });

        dynamicSpinner = (Spinner) findViewById(R.id.dynamic_spinner);

        getTeamMembers();

        dynamicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                Log.i("Team plans", "item selected====" + position);



                userId = teamMembersArrayList.get(position).getUserid();



                if (selectedMonthName ==null ) {

                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
                    String monthNumber = outputFormat.format(cal.getTime());
                    Log.i("Team", "monthNumber" + monthNumber);

                    getCalendarMonthDates(monthNumber);


                    getTeamPlannerData(userId, Utils.formatDateFromString("yyyy-MM-dd", "MM", TimeUtils.getCurrentDate("yyyy-MM-dd")), Utils.formatDateFromString("yyyy-MM-dd", "yyyy", TimeUtils.getCurrentDate("yyyy-MM-dd")));
                    Calendar mCalendar = Calendar.getInstance();
                    String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                    toolbar_month.setText(month);
                } else {

                    SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
                    Calendar cal = Calendar.getInstance();
                    try {
                        cal.setTime(inputFormat.parse(selectedMonthName));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
                    String monthNumber = outputFormat.format(cal.getTime());
                    Log.i("Team", "monthNumber" + monthNumber);
                    int year = Calendar.getInstance().get(Calendar.YEAR);
                    String currentYear = String.valueOf(year);
                    Log.i("Team", "year" + year);

                    getCalendarMonthDates(monthNumber);
                    getTeamPlannerData(userId,monthNumber,currentYear);
                    toolbar_month.setText(selectedMonthName);


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }


    private void showProgresDialog(String message) {
        progressBar = new ProgressDialog(this);
        progressBar.setMessage(message);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setIndeterminate(true);
        progressBar.show();
    }

    private void dissmissProgressDialod() {
        if (progressBar != null && progressBar.isShowing()) {
            progressBar.dismiss();
        }
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tmtoolbar);
        toolbar_calendar_layout = (LinearLayout) findViewById(R.id.calendar_layout);
        toolbar_title = (TextView) findViewById(R.id.tm_toolbar_title);
        toolbar_title.setText("Team Plans");
        toolbar_month = (TextView) findViewById(R.id.tm_month_display);

        toolbar_calendar_icon = (ImageButton) findViewById(R.id.calender_icon);


        Calendar mCalendar = Calendar.getInstance();
        String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        toolbar_month.setText(month);

        toolbar_calendar_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog(CUSTOM_DIALOG_ID);

            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }


    @Override
    protected Dialog onCreateDialog(int id) {

        Dialog dialog = null;

        switch (id) {
            case CUSTOM_DIALOG_ID:
                dialog = new Dialog(TeamPlansActivity.this);
                dialog.setContentView(R.layout.dialoglayout);
                dialog.setTitle("Custom Dialog");

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);

                dialog.setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });

                dialog.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {

                    }
                });

                //Prepare ListView in dialog
                dialog_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
                ArrayAdapter<String> adapter
                        = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, listContent);
                dialog_ListView.setAdapter(adapter);
                dialog_ListView.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {


                        toolbar_month.setText(parent.getItemAtPosition(position).toString());
                         selectedMonthName = parent.getItemAtPosition(position).toString();

                        SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
                        Calendar cal = Calendar.getInstance();
                        try {
                            cal.setTime(inputFormat.parse(selectedMonthName));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat outputFormat = new SimpleDateFormat("MM"); // 01-12
                        String monthNumber = outputFormat.format(cal.getTime());
                        Log.i("Team", "monthNumber" + monthNumber);
                        int year = Calendar.getInstance().get(Calendar.YEAR);
                        String currentYear = String.valueOf(year);
                        Log.i("Team", "year" + year);

                        getCalendarMonthDates(monthNumber);
                        getTeamPlannerData(userId, monthNumber, currentYear);

                        dismissDialog(CUSTOM_DIALOG_ID);

                    }
                });

                break;
        }

        return dialog;
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog, Bundle bundle) {
        super.onPrepareDialog(id, dialog, bundle);

        switch (id) {

            case CUSTOM_DIALOG_ID:

                break;
        }

    }


    private void getTeamPlannerData(String userId, String month, String year) {

        Log.i("TEST", "" + userid + month + year);
        showProgresDialog("loading . . .");

        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> getTeamMemberplans = client.getTeamMemberPlans(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), userId, month, year);

        try {

            getTeamMemberplans.enqueue(new Callback<RestResponse>() {
                @Override
                public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                    if (response.isSuccessful())

                        if (teamMemberplannerMList.size() > 0) {
                            teamMemberplannerMList.clear();
                        }

                    if (Utils.isNotNullAndNotEmpty(response.body().getData().toString())) {

                        Log.i("TEST", "++++" + response.body().getData().toString());

                        try {
                            Type listType = new TypeToken<ArrayList<TeamMemberPlannerModel>>() {
                            }.getType();

                            teamMemberplannerMList = new Gson().fromJson(response.body().getData(), listType);
                            Log.i("TEST", "++++" + teamMemberplannerMList.size());
                            if (teamMemberplannerMList.size() > 0) {
                                team_task_nomessages.setVisibility(View.GONE);

                                Collections.sort(teamMemberplannerMList, new Comparator<TeamMemberPlannerModel>() {
                                    @Override
                                    public int compare(TeamMemberPlannerModel lhs, TeamMemberPlannerModel rhs) {
                                        //return lhs.getScheduleday().compareTo(rhs.getScheduleday()); // ascending date
                                        return rhs.getScheduleday().compareTo(lhs.getScheduleday()); // descending date
                                    }
                                });

                                tpRecyclerView.setVisibility(View.VISIBLE);
                                teamPlanAdapter = new TeamPlanAdapter(TeamPlansActivity.this, teamMemberplannerMList, calendarMonthdatesList);
                                tpRecyclerView.setAdapter(teamPlanAdapter);
                                tpRecyclerView.getAdapter().notifyDataSetChanged();
                                tpRecyclerView.setLayoutManager(new LinearLayoutManager(TeamPlansActivity.this));
                                dissmissProgressDialod();
                            } else {

                                team_task_nomessages.setVisibility(View.VISIBLE);
                                //tpRecyclerView.getAdapter().notifyDataSetChanged();
                                tpRecyclerView.setVisibility(View.GONE);
                                tpRecyclerView.invalidate();
                                dissmissProgressDialod();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        } finally {

                            dissmissProgressDialod();

                        }

                    } else {

                        team_task_nomessages.setVisibility(View.VISIBLE);
                        tpRecyclerView.invalidate();
                        tpRecyclerView.setVisibility(View.GONE);
                        dissmissProgressDialod();
                    }
                }

                @Override
                public void onFailure(Call<RestResponse> call, Throwable t) {
                    dissmissProgressDialod();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getTeamMembers() {
        showProgresDialog("loading . . .");
        RestServicesForTragets servcieForTragets = RestService.createServicev1(RestServicesForTragets.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamMembers(userToken);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful())
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        Type listType = new TypeToken<ArrayList<TeamMembers>>() {
                        }.getType();
                        ArrayList<TeamMembers> tmArrayList = new Gson().fromJson(response.body().getData(), listType);
                        if (tmArrayList != null) {
                            if (tmArrayList.size() > 0) {
                                teamMembersArrayList = new ArrayList();
                                for (int i = 0; i < tmArrayList.size(); i++) {
                                    if (tmArrayList.get(i).getRole().equalsIgnoreCase("member")) {
                                        teamMembersArrayList.add(tmArrayList.get(i));
                                    }
                                }
                                if (teamMembersArrayList.size() != 0) {
                                    dynamicSpinner.setVisibility(View.VISIBLE);
                                    TeamMembersAdapter teamMembersAdapter = new TeamMembersAdapter(TeamPlansActivity.this, teamMembersArrayList);
                                    dynamicSpinner.setAdapter(teamMembersAdapter);
                                    dissmissProgressDialod();
                                }
                            } else {
                                dynamicSpinner.setVisibility(View.GONE);
                                team_task_nomessages.setVisibility(View.VISIBLE);
                                dissmissProgressDialod();
                            }
                        } else {
                            dynamicSpinner.setVisibility(View.GONE);
                            team_task_nomessages.setVisibility(View.VISIBLE);
                            dissmissProgressDialod();
                        }
                    }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dissmissProgressDialod();
            }
        });
    }

    private void getCalendarMonthDates(String monthNumber) {

        Calendar cal = Calendar.getInstance();
        int month = Integer.parseInt(monthNumber);
        //int month = cal.get(Calendar.MONTH);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(df.format(cal.getTime()));
        Log.i("Team", "df = = =" + df.format(cal.getTime()));
        calendarMonthdatesList.clear();
        for (int i = 0; i < maxDay; i++) {
            cal.set(Calendar.DAY_OF_MONTH, i + 1);
            System.out.println(df.format(cal.getTime()));
            String date = df.format(cal.getTime());
            String subdate = date.substring(8);
            System.out.println(date.substring(8));
            calendarMonthdatesList.add(subdate);
            Collections.sort(calendarMonthdatesList, Collections.reverseOrder());

            Log.i("Team", "df for loop = = =" + df.format(cal.getTime()));
            Log.i("Team", "subdate for loop = = =" + subdate);
            Log.i("Team", "month = = =" + String.valueOf(month));
        }
    }


}


*/
