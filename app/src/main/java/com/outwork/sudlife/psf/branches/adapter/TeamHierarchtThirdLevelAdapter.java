package com.outwork.sudlife.psf.branches.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.models.Childlayer;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.List;

/**
 * Created by shravanch on 20-07-2018.
 */

public class TeamHierarchtThirdLevelAdapter extends BaseAdapter {

    Activity c;
    List<Childlayer> objects;

    public TeamHierarchtThirdLevelAdapter(Activity context, List<Childlayer> objects) {
        super();
        this.c = context;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        Childlayer cur_obj = objects.get(position);
        LayoutInflater inflater = (c).getLayoutInflater();
        View row = inflater.inflate(R.layout.team_hierarchy_row, parent, false);
        TextView label = (TextView) row.findViewById(R.id.types);
        Utils.setTypefaces(IvokoApplication.robotoTypeface,label);
        StringBuilder stringBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(cur_obj.getName())) {
            stringBuilder.append(cur_obj.getName().toUpperCase());
            if (Utils.isNotNullAndNotEmpty(cur_obj.getSupervisorname())) {
                stringBuilder.append(" - " + cur_obj.getSupervisorname().toUpperCase());
            }
        }
        label.setText(stringBuilder);
        Log.i("Shravan", "child teamid===" + cur_obj.getTeamid());
        Log.i("Shravan", "child name===" + cur_obj.getName());


        return row;
    }
}
