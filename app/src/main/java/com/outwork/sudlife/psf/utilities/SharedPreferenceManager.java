package com.outwork.sudlife.psf.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.outwork.sudlife.psf.ui.models.ProfileModel;

/**
 * Created by Panch on 8/18/2017.
 */

public class SharedPreferenceManager {

    private static final SharedPreferenceManager ourInstance = new SharedPreferenceManager();
    private SharedPreferences sharedPreferences;

    public static SharedPreferenceManager getInstance() {
        return ourInstance;
    }

    private SharedPreferenceManager() {
    }

    public void init(Context context) {
        sharedPreferences = context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
    }

    public void putString(String key, String value) {
        (sharedPreferences.edit()).putString(key, value).apply();
    }

    public String getString(String key, String defaultVal) {
        return sharedPreferences.getString(key, defaultVal);
    }

    public void putBoolean(String key, boolean value) {
        (sharedPreferences.edit()).putBoolean(key, value).apply();
    }

    public boolean getBoolean(String key, boolean defaultVal) {
        if (sharedPreferences != null) {
            return sharedPreferences.getBoolean(key, defaultVal);
        } else {
            return defaultVal;
        }
    }
    public int getInt(String key, int defaultVal) {
        return sharedPreferences.getInt(key, defaultVal);
    }
    public void putInt(String key, int defaultVal) {
        (sharedPreferences.edit()).putInt(key, defaultVal).apply();
    }


    public void storeProfileData(ProfileModel profileModel) {
        SharedPreferenceManager.getInstance().putString(Constants.FIRSTNAME, profileModel.getFirstname());
        SharedPreferenceManager.getInstance().putString(Constants.LASTNAME, profileModel.getLastname());
        SharedPreferenceManager.getInstance().putString(Constants.EMAIL, profileModel.getEmail());
        SharedPreferenceManager.getInstance().putString(Constants.PROFILE_URL, profileModel.getUrl1());
        SharedPreferenceManager.getInstance().putString(Constants.PHONENO, profileModel.getPhonenumber());
        SharedPreferenceManager.getInstance().putString(Constants.CITY, profileModel.getCity());
        if (profileModel.getEmailvisibility() != null)
            if (profileModel.getEmailvisibility().equalsIgnoreCase("1")) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.emailvisibility, true);
            }
        if (profileModel.getNumbervisibility() != null)
            if (profileModel.getNumbervisibility().equalsIgnoreCase("1")) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.numbervisibility, true);
            }
        if (profileModel.getLinkedinvisibility() != null)
            if (profileModel.getLinkedinvisibility().equalsIgnoreCase("1")) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.linkedinvisibility, true);
            }
        if (profileModel.getFbvisibility() != null)
            if (profileModel.getFbvisibility().equalsIgnoreCase("1")) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.fbvisibility, true);
            }
        if (profileModel.getTwittervisibility() != null)
            if (profileModel.getTwittervisibility().equalsIgnoreCase("1")) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.twittervisibility, true);
            }
        if (profileModel.getOrgvisibility() != null)
            if (profileModel.getOrgvisibility().equalsIgnoreCase("1")) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.orgvisibility, true);
            }
        if (profileModel.getLocationvisibility() != null)
            if (profileModel.getLocationvisibility().equalsIgnoreCase("1")) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.locationvisibility, true);
            }
        if (profileModel.getDesignationvisibility() != null)
            if (profileModel.getDesignationvisibility().equalsIgnoreCase("1")) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.designationvisibility, true);
            }
        if (profileModel.getWebsitevisibility() != null)
            if (profileModel.getWebsitevisibility().equalsIgnoreCase("1")) {
                SharedPreferenceManager.getInstance().putBoolean(Constants.websitevisibility, true);
            }
    }

    public void clear() {
        sharedPreferences.edit().clear().apply();
    }

}
