package com.outwork.sudlife.psf.more.models;

import com.outwork.sudlife.psf.dto.LocationDto;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PostMessageDto extends CommonDto {

    private String messageId;
    private String messageType;
    private String postedDate;
    private String title;
    private String description;
    private ArrayList<String> attachments = new ArrayList<String>();
    private LocationDto availLocation = new LocationDto();
    private String userName;
    private String userEmail;



    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }



    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments.add(attachments);
    }

    public LocationDto getAvailLocation() {
        return availLocation;
    }

    public void setAvailLocation(LocationDto availLocation) {
        this.availLocation = availLocation;
    }



    public String getUserName(){
        return userName;
    }

    public void setUserName(String userName){
        this.userName =userName;
    }

    public String getUserEmail(){
        return userEmail;
    }

    public void setUserEmail(String userEmail){
        this.userEmail =userEmail;
    }


    private static final String JSON_TYPE = "type";
    private static final String JSON_TITLE = "title";
    private static final String JSON_DESC = "description";
    private static final String JSON_AVAIL_LOCATION = "availableLocation";
    private static final String JSON_COUNTRY_CODE = "countrycode";



    public JSONObject getServerRequest()
            throws JSONException {

        JSONObject serverObject = new JSONObject();
        JSONObject mainObject = new JSONObject();
        serverObject.put(JSON_TITLE, title);
        serverObject.put(JSON_TYPE, messageType);
        serverObject.put(JSON_DESC, description);
        serverObject.put(JSON_AVAIL_LOCATION, getLocationJSON());
        serverObject.put(JSON_COUNTRY_CODE, "1");
        return serverObject;
    }

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public static final String JSON_FILE_ID = "fileid";
    public static final String JSON_FILE_URL = "fileurl";




    public JSONObject getFile() throws JSONException {

        JSONObject obj = new JSONObject();
        obj.put(JSON_FILE_ID, "");
        obj.put(JSON_FILE_URL, "");

        return obj;
    }

    public static final String JSON_LOC_LAT = "avllocationlat";
    public static final String JSON_LOC_LONG = "avllocationlong";
    public static final String JSON_LOC_NAME = "avllocationname";
    public static final String JSON_LOC_CITY = "avllocationcity";
    public static final String JSON_LOC_ZIP = "avllocationzipcode";
    public static final String JSON_LOC_COUNTRY = "avllocationcountry";
    public static final String JSON_LOC_STATE = "avllocationstate";


    public JSONObject getLocationJSON() throws JSONException {

        JSONObject location = new JSONObject();
        location.put(JSON_LOC_LAT, availLocation.getLatitude());
        location.put(JSON_LOC_LONG, availLocation.getLongitude());
        location.put(JSON_LOC_NAME, availLocation.getLocName());
        location.put(JSON_LOC_CITY, availLocation.getCity());
        location.put(JSON_LOC_ZIP, availLocation.getZipCode() == null ? ""
                : availLocation.getZipCode());
        location.put(JSON_LOC_COUNTRY, availLocation.getCountry());
        location.put(JSON_LOC_STATE, availLocation.getState());

        return location;
    }
}
