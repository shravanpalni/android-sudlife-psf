package com.outwork.sudlife.psf.targets.ui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.targets.services.TargetsIntentService;
import com.outwork.sudlife.psf.targets.targetsmngr.TargetsMgr;
import com.outwork.sudlife.psf.targets.models.MSBModel;
import com.outwork.sudlife.psf.utilities.Utils;

public class BranchesViewDetailsActivity extends AppCompatActivity {

    EditText etFtm, etActivityPlanned, etVisitsPlanned, etExpectedConnections, etLeadsExpected, etExpectedBusiness/* ,etGapToTraget*/;

    MSBModel msbModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branches_view_details);

        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        etFtm = (EditText) findViewById(R.id.et_ftm);
        etActivityPlanned = (EditText) findViewById(R.id.et_activity_planed);
        etVisitsPlanned = (EditText) findViewById(R.id.et_visits_planned);
        etExpectedConnections = (EditText) findViewById(R.id.et_expected_connections);
        etLeadsExpected = (EditText) findViewById(R.id.et_leads_expected);
        etExpectedBusiness = (EditText) findViewById(R.id.et_expected_business);
        //etGapToTraget = (EditText) findViewById(R.id.et_gap_target);
        Intent intent = getIntent();
        msbModel = new Gson().fromJson(intent.getStringExtra("msbObj"), MSBModel.class);
        // BranchesModel customerModel = new Gson().fromJson(intent.getStringExtra("customerObj"), BranchesModel.class);
      /*  ArrayList<MSBModel> msbModelArrayList = TargetsMgr.getInstance(BranchesViewDetailsActivity.this).getMSBRecoreds(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        if (msbModelArrayList.size() > 0) {
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                if (msbModelArrayList.get(i).getObjectid().equalsIgnoreCase(customerModel.getCustomerid())) {
                    msbModel = msbModelArrayList.get(i);
                }
            }
        }*/
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, textView, (Button) findViewById(R.id.btn_submit));
        Utils.setTypefaces(IvokoApplication.robotoTypeface, etFtm, etActivityPlanned, etVisitsPlanned,
                etExpectedConnections, etLeadsExpected, etExpectedBusiness,
                (TextView) findViewById(R.id.tv_ftm), (TextView) findViewById(R.id.tv_activity_planed),
                (TextView) findViewById(R.id.tv_visits_planned), (TextView) findViewById(R.id.tv_expected_connections),
                (TextView) findViewById(R.id.tv_leads_expected), (TextView) findViewById(R.id.tv_expected_business));
        if (msbModel.getAchievedbusiness() != null) {
            if (Utils.isNotNullAndNotEmpty(msbModel.getCustomername()))
                textView.setText(msbModel.getCustomername());
            etActivityPlanned.setText(msbModel.getActivitiesplanned());
            etVisitsPlanned.setText(msbModel.getVisitsplanned());
            if (Utils.isNotNullAndNotEmpty(msbModel.getExpectedconncetions())) {
                etExpectedConnections.setHint(msbModel.getExpectedconncetions());
                etExpectedConnections.setHintTextColor(getResources().getColor(R.color.main_color_grey_500));
//                etExpectedConnections.setSelection(etExpectedConnections.getText().length());
            }
            if (Utils.isNotNullAndNotEmpty(msbModel.getExpectedleads())) {
                etLeadsExpected.setHint(msbModel.getExpectedleads());
                etLeadsExpected.setHintTextColor(getResources().getColor(R.color.main_color_grey_500));
//                etLeadsExpected.setSelection(etLeadsExpected.getText().length());
            }
            if (Utils.isNotNullAndNotEmpty(msbModel.getExpectedbusiness())) {
                etExpectedBusiness.setHint(msbModel.getExpectedbusiness());
                etExpectedBusiness.setHintTextColor(getResources().getColor(R.color.main_color_grey_500));
//                etLeadsExpected.setSelection(etLeadsExpected.getText().length());
            }
            //etGapToTraget.setText(String.valueOf(Integer.parseInt(msbModel.getFtm()) - Integer.parseInt(msbModel.getExpectedbusiness())));
            etFtm.setText(msbModel.getFtm());
        } else {
            ((Button) findViewById(R.id.btn_submit)).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.edit_target_record)).setVisibility(View.GONE);
            Toast.makeText(this, "No Data", Toast.LENGTH_SHORT).show();
            finish();
        }
        initToolBar();
        findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                msbModel.setOfflineoronlinestatus("offline");
                if (etLeadsExpected.getText().toString().length() != 0) {
                    msbModel.setExpectedleads(etLeadsExpected.getText().toString());
                } else {
                    msbModel.setExpectedleads("0");
                }
                if (etExpectedBusiness.getText().toString().length() != 0) {
                    msbModel.setExpectedbusiness(etExpectedBusiness.getText().toString());
                } else {
                    msbModel.setExpectedbusiness("0");
                }
                if (etExpectedConnections.getText().toString().length() != 0) {
                    msbModel.setExpectedconncetions(etExpectedConnections.getText().toString());
                } else {
                    msbModel.setExpectedconncetions("0");
                }
                TargetsMgr.getInstance(BranchesViewDetailsActivity.this).updateTargetRecord(msbModel);
                TargetsIntentService.updateRecord(BranchesViewDetailsActivity.this);
                showAlertMessage("Your Target Has Updated Successfully", BranchesViewDetailsActivity.this);
            }
        });
        findViewById(R.id.edit_target_record).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etExpectedConnections.setEnabled(true);
                etLeadsExpected.setEnabled(true);
                etExpectedBusiness.setEnabled(true);
                etExpectedConnections.setFocusable(true);
                etLeadsExpected.setFocusable(true);
                etExpectedBusiness.setFocusable(true);
                etExpectedConnections.setFocusableInTouchMode(true);
                etLeadsExpected.setFocusableInTouchMode(true);
                etExpectedBusiness.setFocusableInTouchMode(true);

                etExpectedConnections.setTextColor(Color.BLACK);
                etLeadsExpected.setTextColor(Color.BLACK);
                etExpectedBusiness.setTextColor(Color.BLACK);

            }
        });
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        }
    }


    public static void showAlertMessage(String message, final Activity context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }
}