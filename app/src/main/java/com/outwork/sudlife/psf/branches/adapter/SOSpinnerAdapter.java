package com.outwork.sudlife.psf.branches.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.ui.models.Userprofile;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;

/**
 * Created by shravanch on 17-07-2018.
 */

public class SOSpinnerAdapter extends BaseAdapter {

    Activity c;
    ArrayList<Userprofile> objects;

    public SOSpinnerAdapter(Activity context, ArrayList<Userprofile> objects) {
        super();
        this.c = context;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Userprofile cur_obj = objects.get(position);
        LayoutInflater inflater = (c).getLayoutInflater();
        View row = inflater.inflate(R.layout.team_hierarchy_row, parent, false);
        TextView label = (TextView) row.findViewById(R.id.types);
        View layout = row.findViewById(R.id.row);
        Utils.setTypefaces(IvokoApplication.robotoTypeface, label);
        StringBuilder stringBuilder = new StringBuilder();
        if (Utils.isNotNullAndNotEmpty(cur_obj.getFirstname())) {
            layout.setVisibility(View.VISIBLE);
            stringBuilder.append(cur_obj.getFirstname().toUpperCase());
            if (Utils.isNotNullAndNotEmpty(cur_obj.getLastname())) {
                stringBuilder.append(" " + cur_obj.getLastname().toUpperCase());
            }
        } else {
            layout.setVisibility(View.GONE);
        }
        label.setText(stringBuilder.toString());
        return row;
    }
}
