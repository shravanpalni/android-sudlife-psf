package com.outwork.sudlife.psf.lead.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.db.ProposalCodesDao;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;
import com.outwork.sudlife.psf.lead.model.ProposalCodesModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.localbase.ProductDataMgr;
import com.outwork.sudlife.psf.localbase.ProductMasterDto;
import com.outwork.sudlife.psf.localbase.StaticDataDto;
import com.outwork.sudlife.psf.localbase.StaticDataMgr;
import com.outwork.sudlife.psf.opportunity.OpportunityMgr;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.activities.DashBoardActivity;
import com.outwork.sudlife.psf.ui.activities.NotificationsListActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shravanch on 19-12-2019.
 */

public class PSFUpdateLeadActivity extends BaseActivity {
    private BottomSheetDialog bottomSheetDialog;
    private ListView lvBottom;
    private TextView /*tv_customerSalutation,*/tv_customername, tv_dob, tv_gender, tv_contact_number, toolbar_title;
    private EditText et_source_of_lead, et_education, et_occupation, et_annualincome, et_landlineno, et_email_id, et_alt_contactno, et_psfzone, et_sudbranch, et_existing_sud_ife_customer,
            et_policy_no, et_product_name, et_reference, et_reference_mobileno, et_reference_leadcode, et_lead_stage, et_first_appointment_date, et_expected_actpremium, et_product_one, et_conversion_propensity,
            et_actual_product, et_actpremium_paid, et_proposal_no, et_notes, et_lead_created_date, et_allocate_fls, et_policy_status, et_bank_name, et_sum_assured, et_risk_comm_date, et_lapsed_date, et_maturity_value, et_maturity_date, et_ppt_last_date, et_paid_to_date, et_pt, et_ppt, et_fund_value, et_total_received_premium, et_instalment_premium, et_frequency;
    private TextInputLayout tiLayoutothers, tiLayoutSubStatus;
    private RelativeLayout jointcall_layout;
    private CheckBox jointcall_checkbox;

    /* private EditText etLeadStage, etEmailId, etAlternativeContactNo, etAge, etExistingCustomer, etPremiumPaid, etProductName, etProductone, etProducttwo, etProductNameOther,
             etOccupation, et_campaigndate, etSourceOfLead, etOther, etNoOfFamilyNumbers, etZccSupportRequried, etPreferredLang,
             etPreferredDate, etPreferredTime, etNotes, etStatus, etEducation, etSubStatus, etIncome, etPremiumExpected, etNextFollowupDate, etConversionPropensity,
             etProposalNumber, etLeadCreatedDate, etMarriedStatus, etFirstAppointmentDate, etActualPremium;*/
    private AlertDialog.Builder alertDialog;
    private List<String> myList;
    private String pvisitdate, fappointmentDate, nappointmentDate;
    private ArrayList<String> stringArrayList;
    private List<String> stagesList = new ArrayList<>();
    private PSFLeadModel leadModel = new PSFLeadModel();
    private List<ProposalCodesModel> proposalCodesModelList = new ArrayList<>();
    private PlannerModel plannerModel = new PlannerModel();
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;
    TextView textCartItemCount;
    int mCartItemCount = 0;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
               /* etNextFollowupDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etNextFollowupDate.setText(visitdt);
                etNextFollowupDate.setEnabled(true);
                etNextFollowupDate.setError(null);
                nappointmentDate = TimeUtils.convertDatetoUnix(visitdt);*/
            }
            if (customSelector == 1) {
                et_first_appointment_date.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                et_first_appointment_date.setText(visitdt);
                et_first_appointment_date.setEnabled(true);
                et_first_appointment_date.setError(null);
                fappointmentDate = TimeUtils.convertDatetoUnix(visitdt);
            }


          /*  if (customSelector == 2) {
                et_lapsed_date.setText(TimeUtils.getFormattedYearMonthDate(date));
                String visitdt = TimeUtils.getFormattedYearMonthDate(date);
                et_lapsed_date.setText(visitdt);
                et_lapsed_date.setEnabled(true);
            }*/


            if (customSelector == 3) {
          /*      etPreferredDate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                etPreferredDate.setText(visitdt);
                etPreferredDate.setEnabled(true);
                pvisitdate = TimeUtils.convertDatetoUnix(visitdt);
                etPreferredDate.setError(null);*/
            }
        }

        @Override
        public void onDateTimeCancel() {
            et_first_appointment_date.setEnabled(true);
            // et_lapsed_date.setEnabled(true);
           /* etNextFollowupDate.setEnabled(true);
            etFirstAppointmentDate.setEnabled(true);
            etPreferredDate.setEnabled(true);*/
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_update_psflead);

        mgr = LocalBroadcastManager.getInstance(this);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("leadobj")) {
            leadModel = new Gson().fromJson(getIntent().getStringExtra("leadobj"), PSFLeadModel.class);
        }
        if (SharedPreferenceManager.getInstance().getString(Constants.STAGES_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                OpportunityIntentService.insertOpportunityStages(PSFUpdateLeadActivity.this);
            }
        }
        if (SharedPreferenceManager.getInstance().getString(Constants.PROPOSAL_CODES_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                LeadIntentService.insertProposalCodes(PSFUpdateLeadActivity.this);
            }
        }


        initilizeViews();
        initToolBar();
        populateData(leadModel);
        setListener();

       /* Utils.setTypefaces(IvokoApplication.robotoTypeface,et_source_of_lead,et_education,et_occupation,et_annualincome,et_landlineno,et_email_id,et_alt_contactno,et_psfzone,et_sudbranch,et_existing_sud_ife_customer,
                et_policy_no,et_product_name,et_reference,et_reference_leadcode,et_lead_stage,et_first_appointment_date,et_expected_actpremium,et_product_one,et_conversion_propensity,
                et_actual_product,et_actpremium_paid,et_proposal_no,et_notes,et_lead_created_date,et_allocate_fls); *//*etLeadStage, etEmailId, etAlternativeContactNo, etAge, etExistingCustomer, etPremiumPaid, etProductName, etProductone, etProductNameOther,
                etOccupation, et_campaigndate, etSourceOfLead, etOther, etNoOfFamilyNumbers, etZccSupportRequried, etPreferredLang,
                etPreferredDate, etPreferredTime, etNotes, etStatus, etEducation, etSubStatus, etIncome, etPremiumExpected, etNextFollowupDate, etConversionPropensity,
                etProposalNumber, etLeadCreatedDate, etProducttwo, etActualPremium, etMarriedStatus, etFirstAppointmentDate*//*
        Utils.setTypefaces(IvokoApplication.robotoLightTypeface, (TextView) findViewById(R.id.fname),
                (TextView) findViewById(R.id.lname), (TextView) findViewById(R.id.contact_number),
                (TextView) findViewById(R.id.address), (TextView) findViewById(R.id.city),
                (TextView) findViewById(R.id.pincode), (TextView) findViewById(R.id.bank),
                (TextView) findViewById(R.id.branch_code), (TextView) findViewById(R.id.branch_name));
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoMediumTypeface,
                (TextView) findViewById(R.id.personalinfo_lbl), (TextView) findViewById(R.id.pastinfo_lbl),
                (TextView) findViewById(R.id.otherinfo_lbl), (TextView) findViewById(R.id.bankinfo_lbl),
                (TextView) findViewById(R.id.zccinfo_lbl), (TextView) findViewById(R.id.updateinfo_lbl));*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("pcodes_broadcast"));
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("oppstage_broadcast"));


        if (isNetworkAvailable()) {
            updateNotification();
        } else {
            List<PSFNotificationsModel> finalList = new ArrayList<>();
            List<PSFNotificationsModel> psfNotificationsModelList = LeadMgr.getInstance(PSFUpdateLeadActivity.this).getPSFNotificationsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));

            if (psfNotificationsModelList.size() > 0) {

                for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                    if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                        finalList.add(notificationsModel);
                    }

                }

                mCartItemCount = finalList.size();
                setupBadge();
            } else {
                mCartItemCount = 0;
                setupBadge();
            }

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    public void populateData(PSFLeadModel leadModel) {


        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadSource())) {
            et_source_of_lead.setText(leadModel.getLeadSource());
        } else {
            et_source_of_lead.setText(getResources().getString(R.string.notAvailable));
        }


      /*  if (Utils.isNotNullAndNotEmpty(leadModel.getPolicyHolderSalutation())) {
            tv_customerSalutation.setText(leadModel.getPolicyHolderSalutation());
        } else {
            tv_customerSalutation.setText(getResources().getString(R.string.notAvailable));
        }*/


        if (Utils.isNotNullAndNotEmpty(leadModel.getPolicyHolderFirstName())) {
            tv_customername.setText(leadModel.getPolicyHolderFirstName());
        } else {
            tv_customername.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getPolicyHolderDob())) {
            tv_dob.setText(leadModel.getPolicyHolderDob());
        } else {
            tv_dob.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getGender())) {
            tv_gender.setText(leadModel.getGender());
        } else {
            tv_gender.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getEducation())) {
            et_education.setText(leadModel.getEducation());
        } else {
            et_education.setText("Not Available");
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getOccupation())) {
            et_occupation.setText(leadModel.getOccupation());
        } else {
            et_occupation.setText("Not Available");
        }

        if (Utils.isNotNullAndNotEmpty(String.valueOf(leadModel.getAnnualIncome()))) {
            et_annualincome.setText(String.valueOf(leadModel.getAnnualIncome()));
        } else {
            et_annualincome.setText("");
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getContactNo())) {
            tv_contact_number.setText(leadModel.getContactNo());
        } else {
            tv_contact_number.setText("Not Available");
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getLandLineNo())) {
            et_landlineno.setText(leadModel.getLandLineNo());
        } else {
            et_landlineno.setText("Not Available");
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getEmailId())) {

            if(leadModel.getEmailId().equalsIgnoreCase("Not Available")){
                leadModel.setEmailId("");
            }else{
                et_email_id.setText(leadModel.getEmailId());
            }



            et_email_id.setText(leadModel.getEmailId());
        } /*else {
            et_email_id.setText("Not Available");
        }*/

        if (Utils.isNotNullAndNotEmpty(leadModel.getAlternateMobileNo())) {
            if(leadModel.getAlternateMobileNo().equalsIgnoreCase("Not Available")){
                leadModel.setAlternateMobileNo("");
            }else{
                et_alt_contactno.setText(leadModel.getAlternateMobileNo());
            }



        } /*else {
            et_alt_contactno.setText("Not Available");
        }*/


        if (Utils.isNotNullAndNotEmpty(leadModel.getPsfZone())) {
            et_psfzone.setText(leadModel.getPsfZone());
        } else {
            //et_psfzone.setText("Not Available");
            et_psfzone.setVisibility(View.GONE);

        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getSudBranchOffice())) {
            et_sudbranch.setText(leadModel.getSudBranchOffice());
        } else {
            //et_sudbranch.setText("Not Available");
            et_sudbranch.setVisibility(View.GONE);
        }

        if (leadModel.getIsExistingCustomer() != null) {
            if (leadModel.getIsExistingCustomer()) {
                et_existing_sud_ife_customer.setText("Yes");
                findViewById(R.id.et_premium_paid_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_product_name_lbl).setVisibility(View.VISIBLE);

                if (leadModel.getLeadSource().equalsIgnoreCase("Reference/Self")) {
                    findViewById(R.id.et_policy_status_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_bank_name_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_sum_assured_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_risk_comm_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_frequency_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_instalment_premium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_total_received_premium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_fund_value_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_ppt_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_pt_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_paid_to_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_ppt_last_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_maturity_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_maturity_value_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_lapsed_date_lbl).setVisibility(View.GONE);

                } else {
                    findViewById(R.id.et_policy_status_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_bank_name_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_sum_assured_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_risk_comm_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_frequency_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_instalment_premium_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_total_received_premium_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_fund_value_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_ppt_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_pt_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_paid_to_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_ppt_last_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_maturity_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_maturity_value_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_lapsed_date_lbl).setVisibility(View.VISIBLE);

                }


            } else {
                et_existing_sud_ife_customer.setText("No");
                findViewById(R.id.et_premium_paid_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_name_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_policy_status_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_bank_name_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_sum_assured_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_risk_comm_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_frequency_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_instalment_premium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_total_received_premium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_fund_value_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_ppt_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_pt_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_paid_to_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_ppt_last_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_maturity_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_maturity_value_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_lapsed_date_lbl).setVisibility(View.GONE);
            }
        }


        if (leadModel.getPolicyNo() != null) {
            et_policy_no.setText(String.valueOf(leadModel.getPolicyNo()));
        } else {
            et_policy_no.setText(getResources().getString(R.string.notAvailable));
        }
        if (Utils.isNotNullAndNotEmpty(leadModel.getProductName())) {
            et_product_name.setText(leadModel.getProductName());
        } else {
            et_product_name.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getPolicyStatus())) {
            et_policy_status.setText(leadModel.getPolicyStatus());
        } else {
            et_policy_status.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getBankName())) {
            et_bank_name.setText(leadModel.getBankName());
        } else {
            et_bank_name.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getSumAssured() != null) {
            et_sum_assured.setText(String.valueOf(leadModel.getSumAssured()));
        } else {
            et_sum_assured.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getRiskCommDate())) {
            et_risk_comm_date.setText(leadModel.getRiskCommDate());
        } else {
            et_risk_comm_date.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getFrequency())) {
            et_frequency.setText(leadModel.getFrequency());
        } else {
            et_frequency.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getInstallmentPremium() != null) {
            et_instalment_premium.setText(String.valueOf(leadModel.getInstallmentPremium()));
        } else {
            et_instalment_premium.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getTotalReceivedPremium() != null) {
            et_total_received_premium.setText(String.valueOf(leadModel.getTotalReceivedPremium()));
        } else {
            et_total_received_premium.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getFundValue() != null) {
            et_fund_value.setText(String.valueOf(leadModel.getFundValue()));
        } else {
            et_fund_value.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getPpt() != null) {
            et_ppt.setText(String.valueOf(leadModel.getPpt()));
        } else {
            et_ppt.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getPt() != null) {
            et_pt.setText(String.valueOf(leadModel.getPt()));
        } else {
            et_pt.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getPaidToDate())) {
            et_paid_to_date.setText(leadModel.getPaidToDate());
        } else {
            et_paid_to_date.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getPptLastDate())) {
            et_ppt_last_date.setText(leadModel.getPptLastDate());
        } else {
            et_ppt_last_date.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getMaturityDate())) {
            et_maturity_date.setText(leadModel.getMaturityDate());
        } else {
            et_maturity_date.setText(getResources().getString(R.string.notAvailable));
        }

        if (leadModel.getMaturityValue() != null) {
            et_maturity_value.setText(String.valueOf(leadModel.getMaturityValue()));
        } else {
            et_maturity_value.setText(getResources().getString(R.string.notAvailable));
        }

        if (Utils.isNotNullAndNotEmpty(leadModel.getLapsedDate())) {
            et_lapsed_date.setText(leadModel.getLapsedDate());
        } else {
            et_lapsed_date.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getReferenceType())) {
            et_reference.setText(leadModel.getReferenceType());
        } else {
            et_reference.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getReferenceMobileNo())) {
            et_reference_mobileno.setText(leadModel.getReferenceMobileNo());
        } else {
            et_reference_mobileno.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getReferenceLeadCode())) {
            et_reference_leadcode.setText(leadModel.getReferenceLeadCode());
        } else {
            et_reference_leadcode.setText(getResources().getString(R.string.notAvailable));
        }


        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadStage())) {
            if (leadModel.getLeadStage().equalsIgnoreCase("Open")) {
                jointcall_layout.setVisibility(View.GONE);
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);
            } else if (leadModel.getLeadStage().equalsIgnoreCase("Unallocated")) {
                jointcall_layout.setVisibility(View.GONE);
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);

            } else if (leadModel.getLeadStage().equalsIgnoreCase("Proposition presented")) {
                jointcall_layout.setVisibility(View.VISIBLE);
                if (leadModel.getIsjointVisit() != null) {
                    if (leadModel.getIsjointVisit()) {
                        jointcall_checkbox.setChecked(true);

                    } else {
                        jointcall_checkbox.setChecked(false);

                    }
                }


                if (Utils.isNotNullAndNotEmpty(leadModel.getAppointmentDate())) {
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.VISIBLE);
                    et_first_appointment_date.setText(leadModel.getAppointmentDate());
                }

                if ((leadModel.getExpectedPremiumPaid() != null)) {
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.VISIBLE);
                    et_expected_actpremium.setText(String.valueOf(leadModel.getExpectedPremiumPaid()));
                }

                if (Utils.isNotNullAndNotEmpty(leadModel.getProduct1())) {
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.VISIBLE);
                    et_product_one.setText(leadModel.getProduct1());
                }


                if (Utils.isNotNullAndNotEmpty(leadModel.getConversionPropensity())) {
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.VISIBLE);
                    et_conversion_propensity.setText(leadModel.getConversionPropensity());
                }

                //findViewById(R.id.et_product_one_lbl).setVisibility(View.VISIBLE);

                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);
            } else if (leadModel.getLeadStage().equalsIgnoreCase("Not Interested")) {
                jointcall_layout.setVisibility(View.GONE);
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);
                if (Utils.isNotNullAndNotEmpty(leadModel.getRemarks())) {
                    et_notes.setText(leadModel.getRemarks());
                }
            } else if (leadModel.getLeadStage().equalsIgnoreCase("Converted")) {
                jointcall_layout.setVisibility(View.GONE);
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);


                if (Utils.isNotNullAndNotEmpty(leadModel.getProduct2())) {
                    et_actual_product.setText(leadModel.getProduct2());
                }

            } else if (leadModel.getLeadStage().equalsIgnoreCase("Pre-Fixed Appointment")) {
                jointcall_layout.setVisibility(View.GONE);
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.VISIBLE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);
            } else if (leadModel.getLeadStage().equalsIgnoreCase("Follow-Up")) {
                jointcall_layout.setVisibility(View.GONE);
                findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);
            }
            et_lead_stage.setText(leadModel.getLeadStage());

            if (leadModel.getCreatedDate() != null) {
                if (Utils.isNotNullAndNotEmpty(leadModel.getCreatedDate())) {
                    et_lead_created_date.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getCreatedDate()), "dd/MM/yyyy hh:mm a"));
                }
            }


            if (leadModel.getAppointmentDate() != null) {
                if (Utils.isNotNullAndNotEmpty(leadModel.getAppointmentDate())) {
                    et_first_appointment_date.setText(TimeUtils.getFormattedDatefromUnix(String.valueOf(leadModel.getAppointmentDate()), "dd/MM/yyyy hh:mm a"));
                }
            }

        }
    }

    private void setListener() {

        et_existing_sud_ife_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                et_existing_sud_ife_customer.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Yes");
                stringArrayList.add("No");
                bottomSheetDialogforExistingCust(et_existing_sud_ife_customer, stringArrayList, "Are You Existing SUD Life customer?");
            }
        });

      /*  et_reference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*et_source_of_lead.setFocusableInTouchMode(false);
                List<StaticDataDto> staticList = new StaticDataMgr(PSFUpdateLeadActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "leadsource");
                if (staticList.size() != 0) {
                    ArrayList<String> strings = new ArrayList<>();
                    for (int i = 0; i < staticList.size(); i++) {
                        strings.add(staticList.get(i).getName());
                    }
                    bottomSheetDialog(et_source_of_lead, strings, "Select Source Of Lead");
                }*//*


                et_reference.setFocusableInTouchMode(false);
                stringArrayList = new ArrayList<>();
                stringArrayList.add("Spouse");
                stringArrayList.add("Children");
                stringArrayList.add("Parents");
                stringArrayList.add("Friend");
                stringArrayList.add("Relative");
                stringArrayList.add("Colleage");
                stringArrayList.add("Neighbour");
                bottomSheetDialog(((EditText) findViewById(R.id.et_reference)), stringArrayList, "Source of Lead");
            }
        });*/


        et_psfzone.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                et_psfzone.setFocusableInTouchMode(false);
                String[] listItems = {"PSF - East", "PSF - North", "PSF - South", "PSF - West 1", "PSF - West 2"};

                AlertDialog.Builder builder = new AlertDialog.Builder(PSFUpdateLeadActivity.this);
                builder.setTitle("Select PSFZone");

                builder.setItems(listItems, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(PSFNewCreateLeadActivity.this, "Position: " + which + " Value: " + listItems[which], Toast.LENGTH_LONG).show();

                        et_psfzone.setText(listItems[which]);
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });

        et_first_appointment_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_lead_stage.getText().toString().equalsIgnoreCase("Proposition presented")) {

                    et_first_appointment_date.setText("");
                    et_first_appointment_date.setEnabled(false);
                    et_first_appointment_date.setFocusableInTouchMode(false);
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_YEAR, -60);
                    Date mindate = calendar.getTime();
                    calendar.add(Calendar.YEAR, 1);
                    Date maxDate = calendar.getTime();

                    new SlideDateTimePicker.Builder(getSupportFragmentManager())
                            .setListener(listener)
                            .setInitialDate(new Date())
                            .setMinDate(mindate)
                            .setMaxDate(maxDate)
                            .setCustomSelector(1)
                            .build()
                            .show();


                } else {

                    et_first_appointment_date.setText("");
                    et_first_appointment_date.setEnabled(false);
                    et_first_appointment_date.setFocusableInTouchMode(false);
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_YEAR, 365);
                    Date backDate = calendar.getTime();

                    new SlideDateTimePicker.Builder(getSupportFragmentManager())
                            .setListener(listener)
                            .setInitialDate(new Date())
                            .setMinDate(new Date())
                            .setMaxDate(backDate)
                            .setCustomSelector(1)
                            .build()
                            .show();

                }


            }
        });

        et_lead_stage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_lead_stage.setFocusableInTouchMode(false);
                stagesList = OpportunityMgr.getInstance(PSFUpdateLeadActivity.this).getOpportunitystagesnamesList(userid);


                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                    //position = 7;
               if(leadModel.getBmUserid().equalsIgnoreCase(leadModel.getAssignedToId())){

                   List<String> newList = new ArrayList<>();

                   for (String num : stagesList) {
                       if (num.equalsIgnoreCase("Unallocated")) {

                       } else {

                           newList.add(num);

                       }
                   }

                   stagesList.clear();
                   stagesList = newList;

               }


                } else {
                    List<String> newList = new ArrayList<>();

                    for (String num : stagesList) {
                        if (num.equalsIgnoreCase("Unallocated") || num.equalsIgnoreCase("Open")) {

                        } else {

                            newList.add(num);

                        }
                    }

                    stagesList.clear();
                    stagesList = newList;
                }


                if (stagesList.size() != 0) {
                    bottomSheetDialogForSubStatus(((EditText) findViewById(R.id.et_lead_stage)), stagesList, "Select Lead Stage");
                }
            }
        });
        et_conversion_propensity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringArrayList = new ArrayList<>();
                stringArrayList.add("RED");
                stringArrayList.add("AMBER");
                stringArrayList.add("GREEN");
                bottomSheetDialog(((EditText) findViewById(R.id.et_conversion_propensity)), stringArrayList, "Select Conversion propensity");

            }
        });


        et_product_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_product_name.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(PSFUpdateLeadActivity.this).getProductList(groupId);
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(et_product_name, stringArrayList, "Select Product");
                }
            }
        });


        et_actual_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_actual_product.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(PSFUpdateLeadActivity.this).getPSFProductList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "Product 2");
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(((EditText) findViewById(R.id.et_actual_product)), stringArrayList, "Select Product");
                }
            }
        });


        et_product_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_product_one.setFocusableInTouchMode(false);
                List<ProductMasterDto> productModelArrayList = ProductDataMgr.getInstance(PSFUpdateLeadActivity.this).getPSFProductList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "Product 1");
                if (productModelArrayList.size() != 0) {
                    stringArrayList = new ArrayList<>();
                    for (int i = 0; i < productModelArrayList.size(); i++) {
                        stringArrayList.add(productModelArrayList.get(i).getProductName());
                    }
                    bottomSheetDialog(((EditText) findViewById(R.id.et_product_one)), stringArrayList, "Select Product");
                }
            }
        });

    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Update Lead");
        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        textCartItemCount = (TextView) findViewById(R.id.notification_badge);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showToast("Notification Clicked!!!");
                Intent in = new Intent(PSFUpdateLeadActivity.this, NotificationsListActivity.class);
                startActivity(in);
            }
        });

        setupBadge();
    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void bottomSheetDialogforExistingCust(final EditText editText, ArrayList<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(PSFUpdateLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(PSFUpdateLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                if (editText.getId() == R.id.et_existing_sud_ife_customer) {
                    if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("No")) {
                        et_policy_no.setText("");
                        findViewById(R.id.et_premium_paid_lbl).setVisibility(View.GONE);
                        findViewById(R.id.et_product_name_lbl).setVisibility(View.GONE);
                        et_policy_no.setVisibility(View.GONE);
                        et_product_name.setText("");
                        et_product_name.setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.et_premium_paid_lbl).setVisibility(View.VISIBLE);
                        findViewById(R.id.et_product_name_lbl).setVisibility(View.VISIBLE);
                        et_policy_no.setVisibility(View.VISIBLE);
                        et_product_name.setVisibility(View.VISIBLE);
                    }
                } /*else if (editText.getId() == R.id.et_zcc_support_requried) {
                    if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("No")) {
                       // etPreferredLang.setText("");
                        findViewById(R.id.et_preferred_language_lbl).setVisibility(View.GONE);
                        findViewById(R.id.et_preferred_Date_lbl).setVisibility(View.GONE);
                      //  etPreferredLang.setVisibility(View.GONE);
                       // etPreferredDate.setText("");
                       // etPreferredDate.setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.et_preferred_language_lbl).setVisibility(View.VISIBLE);
                        findViewById(R.id.et_preferred_Date_lbl).setVisibility(View.VISIBLE);
                       // etPreferredLang.setVisibility(View.VISIBLE);
                       // etPreferredDate.setVisibility(View.VISIBLE);
                    }
                }*/
                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    public void bottomSheetDialog(final EditText editText, ArrayList<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(PSFUpdateLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(PSFUpdateLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());

                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    public void initilizeViews() {
        tiLayoutothers = (TextInputLayout) findViewById(R.id.tilayout_other);
        tiLayoutSubStatus = (TextInputLayout) findViewById(R.id.til_sub_status);

        et_source_of_lead = (EditText) findViewById(R.id.et_source_of_lead);
        // tv_customerSalutation = (TextView) findViewById(R.id.tv_customerSalutation);
        tv_customername = (TextView) findViewById(R.id.tv_customername);
        tv_dob = (TextView) findViewById(R.id.tv_dob);
        tv_gender = (TextView) findViewById(R.id.tv_gender);
        et_education = (EditText) findViewById(R.id.et_education);
        et_occupation = (EditText) findViewById(R.id.et_occupation);
        et_annualincome = (EditText) findViewById(R.id.et_annualincome);
        tv_contact_number = (TextView) findViewById(R.id.tv_contact_number);
        et_landlineno = (EditText) findViewById(R.id.et_landlineno);
        et_email_id = (EditText) findViewById(R.id.et_email_id);
        et_alt_contactno = (EditText) findViewById(R.id.et_alt_contactno);
        et_psfzone = (EditText) findViewById(R.id.et_psfzone);
        et_sudbranch = (EditText) findViewById(R.id.et_sudbranch);
        et_existing_sud_ife_customer = (EditText) findViewById(R.id.et_existing_sud_ife_customer);
        et_product_name = (EditText) findViewById(R.id.et_product_name);
        et_policy_no = (EditText) findViewById(R.id.et_policy_no);
        et_policy_status = (EditText) findViewById(R.id.et_policy_status);
        et_bank_name = (EditText) findViewById(R.id.et_bank_name);
        et_sum_assured = (EditText) findViewById(R.id.et_sum_assured);
        et_risk_comm_date = (EditText) findViewById(R.id.et_risk_comm_date);
        et_frequency = (EditText) findViewById(R.id.et_frequency);
        et_instalment_premium = (EditText) findViewById(R.id.et_instalment_premium);
        et_total_received_premium = (EditText) findViewById(R.id.et_total_received_premium);
        et_fund_value = (EditText) findViewById(R.id.et_fund_value);
        et_ppt = (EditText) findViewById(R.id.et_ppt);
        et_pt = (EditText) findViewById(R.id.et_pt);
        et_paid_to_date = (EditText) findViewById(R.id.et_paid_to_date);
        et_ppt_last_date = (EditText) findViewById(R.id.et_ppt_last_date);
        et_maturity_date = (EditText) findViewById(R.id.et_maturity_date);
        et_maturity_value = (EditText) findViewById(R.id.et_maturity_value);
        et_lapsed_date = (EditText) findViewById(R.id.et_lapsed_date);
        et_reference = (EditText) findViewById(R.id.et_reference);
        et_reference_mobileno = (EditText) findViewById(R.id.et_reference_mobileno);
        et_reference_leadcode = (EditText) findViewById(R.id.et_reference_leadcode);
        et_lead_stage = (EditText) findViewById(R.id.et_lead_stage);
        et_first_appointment_date = (EditText) findViewById(R.id.et_first_appointment_date);
        et_expected_actpremium = (EditText) findViewById(R.id.et_expected_actpremium);
        et_product_one = (EditText) findViewById(R.id.et_product_one);
        et_conversion_propensity = (EditText) findViewById(R.id.et_conversion_propensity);
        et_actual_product = (EditText) findViewById(R.id.et_actual_product);
        et_actpremium_paid = (EditText) findViewById(R.id.et_actpremium_paid);
        et_proposal_no = (EditText) findViewById(R.id.et_proposal_no);
        et_notes = (EditText) findViewById(R.id.et_notes);
        et_lead_created_date = (EditText) findViewById(R.id.et_lead_created_date);
        et_allocate_fls = (EditText) findViewById(R.id.et_allocate_fls);
        jointcall_checkbox = (CheckBox) findViewById(R.id.jointcall_checkbox);
        jointcall_layout = (RelativeLayout) findViewById(R.id.jointcall_layout);


        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // stagesList = OpportunityMgr.getInstance(PSFUpdateLeadActivity.this).getOpportunitystagesnamesList(userid);
                // proposalCodesModelList = ProposalCodesDao.getInstance(PSFUpdateLeadActivity.this).getProposalCodesList(groupId);
            }
        };
    }


    public void bottomSheetDialogForSubStatus(final EditText editText, List<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        bottomSheetDialog = new BottomSheetDialog(PSFUpdateLeadActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(PSFUpdateLeadActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());

                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Open")) {
                    jointcall_layout.setVisibility(View.GONE);
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Unallocated")) {
                    jointcall_layout.setVisibility(View.GONE);
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);

                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Proposition presented")) {
                    jointcall_layout.setVisibility(View.VISIBLE);
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Not Interested")) {
                    jointcall_layout.setVisibility(View.GONE);
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Converted")) {
                    jointcall_layout.setVisibility(View.GONE);
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Pre-Fixed Appointment")) {
                    jointcall_layout.setVisibility(View.GONE);
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Follow-Up")) {
                    jointcall_layout.setVisibility(View.GONE);
                    findViewById(R.id.et_first_appointment_date_lbl).setVisibility(View.VISIBLE);
                    findViewById(R.id.et_expected_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_one_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_conversion_propensity_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_product_two_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_actpremium_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_proposal_no_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_notes_lbl).setVisibility(View.GONE);
                    findViewById(R.id.et_allocate_fls_lbl).setVisibility(View.GONE);
                }

                editText.setError(null);
                bottomSheetDialog.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }


    public void updateLead() {
//        If status is Met, Followup Date and Expected Premium is Mandatory
        // If Leadstatus(stage) is converted, Proposal number is mandatory
        //If zcc support is required, Language is mandatory
        if (tv_contact_number.getText().length() != 10) {
            tv_contact_number.setError("Contact number must be 10 digits ");
            showToast("Contact number must be 10 digits");
            tv_contact_number.requestFocus();
            //etContactNumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});

        }  else if (et_alt_contactno.getText().length() > 10 || (et_alt_contactno.getText().length() < 10 && et_alt_contactno.getText().length() > 0)) {


            et_alt_contactno.setError("Alt Contact number must be 10 digits ");
            et_alt_contactno.requestFocus();


            //etContactNumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});

        }else if ( et_email_id.getText().length() >0 && !Utils.isValidEmail(et_email_id.getText().toString().trim())) {
            et_email_id.setError("Pleae Enter Valid Email");
            et_email_id.requestFocus();
        }/*else if ((!(et_alt_contactno.getText().toString().equalsIgnoreCase("Not Available"))) && (et_alt_contactno.getText().length() > 10 || (et_alt_contactno.getText().length() < 10 && et_alt_contactno.getText().length() > 0))) {

            et_alt_contactno.setError("Alt Contact number must be 10 digits ");
            showToast("Contact number must be 10 digits");
            et_alt_contactno.requestFocus();

        }*//* else if ((!(et_email_id.getText().toString().equalsIgnoreCase("Not Available"))) && et_email_id.getText().toString().length() > 0 && !Utils.isValidEmail(et_email_id.getText().toString().trim())) {
            et_email_id.setError("Pleae Enter Valid Email");
            showToast("Pleae Enter Valid Email");
            et_email_id.requestFocus();
        }*/ /*else if (!Utils.isNotNullAndNotEmpty(et_psfzone.getText().toString())) {
            et_psfzone.setFocusableInTouchMode(true);
            et_psfzone.setError("PSF Zone is mandatory");
            et_psfzone.requestFocus();
        }*/ else if (!Utils.isNotNullAndNotEmpty(et_existing_sud_ife_customer.getText().toString())) {
            et_existing_sud_ife_customer.setFocusableInTouchMode(true);
            et_existing_sud_ife_customer.setError("Please Select Existing or Not");
            showToast("Please Select Existing or Not");
            et_existing_sud_ife_customer.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(et_source_of_lead.getText().toString())) {
            et_source_of_lead.setFocusableInTouchMode(true);
            et_source_of_lead.setError("Source of Lead is mandatory");
            showToast("Source of Lead is mandatory");
            et_source_of_lead.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(et_reference.getText().toString())) {
            et_reference.setFocusableInTouchMode(true);
            et_reference.setError("Reference type is mandatory");
            showToast("Reference type is mandatory");
            et_reference.requestFocus();
        } else if (!Utils.isNotNullAndNotEmpty(et_lead_stage.getText().toString())) {
            et_lead_stage.setFocusableInTouchMode(true);
            et_lead_stage.setError("Please Select Lead Stage");
            showToast("Please Select Lead Stage");
            et_lead_stage.requestFocus();
        } else if (et_lead_stage.getText().toString().equalsIgnoreCase("Unallocated") && !Utils.isNotNullAndNotEmpty(et_first_appointment_date.getText().toString())) {
            et_first_appointment_date.setFocusableInTouchMode(true);
            et_first_appointment_date.setError("Please Select Appointment Date");
            showToast("Please Select Appointment Date");
            et_first_appointment_date.requestFocus();
        } /*else if (et_lead_stage.getText().toString().equalsIgnoreCase("Unallocated") && !Utils.isNotNullAndNotEmpty(et_allocate_fls.getText().toString())) {
            et_allocate_fls.setFocusableInTouchMode(true);
            et_allocate_fls.setError("Please Select FLS");
            et_allocate_fls.requestFocus();
        }*/ else if (et_lead_stage.getText().toString().equalsIgnoreCase("Pre-Fixed Appointment") && !Utils.isNotNullAndNotEmpty(et_first_appointment_date.getText().toString())) {
            et_first_appointment_date.setFocusableInTouchMode(true);
            et_first_appointment_date.setError("Please Select Appointment Date");
            showToast("Please Select Appointment Date");
            et_first_appointment_date.requestFocus();
        } else if (et_lead_stage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(et_first_appointment_date.getText().toString())) {
            et_first_appointment_date.setFocusableInTouchMode(true);
            et_first_appointment_date.setError("Please Select Appointment Date");
            showToast("Please Select Appointment Date");
            et_first_appointment_date.requestFocus();
        } else if (et_lead_stage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(et_expected_actpremium.getText().toString())) {
            et_expected_actpremium.setFocusableInTouchMode(true);
            et_expected_actpremium.setError("Please Specify Expected/Actual Premium");
            showToast("Please Specify Expected/Actual Premium");
            et_expected_actpremium.requestFocus();
        } else if (et_lead_stage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(et_product_one.getText().toString())) {
            et_product_one.setFocusableInTouchMode(true);
            et_product_one.setError("Please Select a Product");
            showToast("Please Select a Product");
            et_product_one.requestFocus();
        } else if (et_lead_stage.getText().toString().equalsIgnoreCase("Proposition presented") && !Utils.isNotNullAndNotEmpty(et_conversion_propensity.getText().toString())) {
            et_conversion_propensity.setFocusableInTouchMode(true);
            et_conversion_propensity.setError("Please Select Convertion Propensity");
            showToast("Please Select Convertion Propensity");
            et_conversion_propensity.requestFocus();
        } else if (et_lead_stage.getText().toString().equalsIgnoreCase("Converted") && !Utils.isNotNullAndNotEmpty(et_actual_product.getText().toString())) {
            et_actual_product.setFocusableInTouchMode(true);
            et_actual_product.setError("Please Select a Product");
            showToast("Please Select a Product");
            et_actual_product.requestFocus();
        } else if (et_lead_stage.getText().toString().equalsIgnoreCase("Converted") && !Utils.isNotNullAndNotEmpty(et_actpremium_paid.getText().toString())) {
            et_actpremium_paid.setFocusableInTouchMode(true);
            et_actpremium_paid.setError("Please Enter Actual Premium Paid");
            showToast("Please Enter Actual Premium Paid");
            et_actpremium_paid.requestFocus();
        }/*else if (et_lead_stage.getText().toString().equalsIgnoreCase("Converted") && !Utils.isNotNullAndNotEmpty(et_proposal_no.getText().toString())) {
            et_proposal_no.setError("Please Enter Proposal Number");
            et_proposal_no.requestFocus();
        }*/ else if (et_lead_stage.getText().toString().equalsIgnoreCase("Not interested") && !Utils.isNotNullAndNotEmpty(et_notes.getText().toString())) {
            et_notes.setError("Please Specify Reason");
            showToast("Please Specify Reason");
            et_notes.requestFocus();
        } else if (et_lead_stage.getText().toString().equalsIgnoreCase("Follow-Up") && !Utils.isNotNullAndNotEmpty(et_first_appointment_date.getText().toString())) {
            et_first_appointment_date.setFocusableInTouchMode(true);
            et_first_appointment_date.setError("Please Select Appointment Date");
            showToast("Please Select Appointment Date");
            et_first_appointment_date.requestFocus();
        } else {
           /* if (et_lead_stage.getText().toString().equalsIgnoreCase("Converted") && Utils.isNotNullAndNotEmpty(et_proposal_no.getText().toString())) {
                proposalCodesModelList = ProposalCodesDao.getInstance(PSFUpdateLeadActivity.this).getProposalCodesList(groupId);
                for (int i = 0; i < proposalCodesModelList.size(); i++) {
                    long pno = Long.parseLong(et_proposal_no.getText().toString());
                    if (pno >= proposalCodesModelList.get(i).getMinvalue() && pno <= proposalCodesModelList.get(i).getMaxvalue()) {
                        et_proposal_no.setError(null);
                        updateLeaddata();
                        break;
                    } else {
                        et_proposal_no.setError("Please Enter Valid Proposal Number");
                        et_proposal_no.requestFocus();
                    }
                }
            } else {
                updateLeaddata();
            }*/

            updateLeaddata();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.order_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                if (isNetworkAvailable()){
                    updateLead();
                    return true;
                }else {
                    showToast("Please check your internet connection.");
                }
            } else  if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                updateLead();
                return true;
            }



        }
        return super.onOptionsItemSelected(item);
    }

    private void updateLeaddata() {
        leadModel.setUserId(userid);
        leadModel.setGroupId(groupId);
        if (et_email_id.getText().toString().equalsIgnoreCase("Not Available")){
            leadModel.setEmailId("");
        }else {
            leadModel.setEmailId(et_email_id.getText().toString());
        }

        if (et_alt_contactno.getText().toString().equalsIgnoreCase("Not Available")){
            leadModel.setAlternateMobileNo("");
        }else {
            leadModel.setAlternateMobileNo(et_alt_contactno.getText().toString());
        }


        if (et_education.getText().toString().equalsIgnoreCase("Not Available")){
            leadModel.setEducation("");
        }else {
            leadModel.setEducation(et_education.getText().toString());
        }

        if (et_occupation.getText().toString().equalsIgnoreCase("Not Available")){
            leadModel.setOccupation("");
        }else {
            leadModel.setOccupation(et_occupation.getText().toString());
        }


       /* if (Utils.isNotNullAndNotEmpty(et_annualincome.getText().toString()))
        leadModel.setAnnualIncome(Integer.parseInt(et_annualincome.getText().toString()));*/


        if (!et_annualincome.getText().toString().equalsIgnoreCase("")) {

            try {
                int i = Integer.parseInt(et_annualincome.getText().toString());
                leadModel.setAnnualIncome(i);
            } catch (NumberFormatException ex) { // handle your exception
                ex.printStackTrace();

            }

        }


        if (Utils.isNotNullAndNotEmpty(et_notes.getText().toString())){
            if (et_notes.getText().toString().equalsIgnoreCase("Not Available")){
                leadModel.setRemarks("");
            }else {
                leadModel.setRemarks(et_notes.getText().toString());
            }
        }

        if (Utils.isNotNullAndNotEmpty(et_existing_sud_ife_customer.getText().toString()))
            if (et_existing_sud_ife_customer.getText().toString().equalsIgnoreCase("yes")) {
                leadModel.setIsExistingCustomer(true);
            } else {
                leadModel.setIsExistingCustomer(false);
            }
        if (Utils.isNotNullAndNotEmpty(et_policy_no.getText().toString())){

            if (et_policy_no.getText().toString().equalsIgnoreCase("Not Available")){
                leadModel.setPolicyNo("");
            }else {
                leadModel.setPolicyNo(et_policy_no.getText().toString());
            }
        }

        if (et_product_name.getText().toString().equalsIgnoreCase("Not Available")){
            leadModel.setProductName("");
        }else {
            leadModel.setProductName(et_product_name.getText().toString());
        }


        if (et_source_of_lead.getText().toString().equalsIgnoreCase("Not Available")){
            leadModel.setLeadSource("");
        }else {
            leadModel.setLeadSource(et_source_of_lead.getText().toString());
        }

        if (et_reference.getText().toString().equalsIgnoreCase("Not Available")){
            leadModel.setReferenceType("");
        }else {
            leadModel.setReferenceType(et_reference.getText().toString());
        }
        if (et_reference_mobileno.getText().toString().equalsIgnoreCase("Not Available")){
            leadModel.setReferenceMobileNo("");
        }else {
            leadModel.setReferenceMobileNo(et_reference_mobileno.getText().toString());
        }
        if (et_lead_stage.getText().toString().equalsIgnoreCase("Not Available")){
            leadModel.setLeadStage("");
        }else {
            leadModel.setLeadStage(et_lead_stage.getText().toString());
        }



        //if (et_lead_stage.getText().toString().equalsIgnoreCase(""))
        if (jointcall_checkbox.isChecked()) {
            leadModel.setIsjointVisit(true);
        } else {
            leadModel.setIsjointVisit(false);
        }


        if (Utils.isNotNullAndNotEmpty(et_first_appointment_date.getText().toString())) {
            leadModel.setAppointmentDate(TimeUtils.convertDatetoUnix(et_first_appointment_date.getText().toString()));


            long unixSeconds = Long.parseLong(TimeUtils.convertDatetoUnix(et_first_appointment_date.getText().toString()));
            Date date = new Date(unixSeconds * 1000L); // *1000 is to convert seconds to milliseconds
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // the format of your date
            String formattedDate = sdf.format(date);
            System.out.println(formattedDate);


            int day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", formattedDate));
            int monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", formattedDate));
            int year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", formattedDate));
            leadModel.setAppointmentDay(day);
            leadModel.setAppointmentMonth(monthno);
            leadModel.setAppointmentYear(year);

        }
        //leadModel.setAppointmentDate(et_first_appointment_date.getText().toString());


        if (Utils.isNotNullAndNotEmpty(et_expected_actpremium.getText().toString()))
            leadModel.setExpectedPremiumPaid(Integer.parseInt(et_expected_actpremium.getText().toString()));
        leadModel.setConversionPropensity(et_conversion_propensity.getText().toString());
        if (Utils.isNotNullAndNotEmpty(et_actpremium_paid.getText().toString()))
            leadModel.setActualPremiumPaid(Integer.parseInt(et_actpremium_paid.getText().toString()));
        leadModel.setProposalNumber(et_proposal_no.getText().toString());
        if (Utils.isNotNullAndNotEmpty(et_lead_created_date.getText().toString()))
            leadModel.setCreatedDate(TimeUtils.convertDatetoUnix(et_lead_created_date.getText().toString()));
        if (Utils.isNotNullAndNotEmpty(et_product_one.getText().toString()))
            leadModel.setProduct1(et_product_one.getText().toString());
        if (Utils.isNotNullAndNotEmpty(et_actual_product.getText().toString()))
            leadModel.setProduct2(et_actual_product.getText().toString());



        //leadModel.setModifiedDate(TimeUtils.convertDatetoUnix(et_lead_created_date.getText().toString()));
        String currentDate = TimeUtils.getCurrentDate("dd/MM/yyyy hh:mm a");
        leadModel.setModifiedDate(TimeUtils.convertDatetoUnix(currentDate));

        if (leadModel.getLeadStage().equalsIgnoreCase("Converted")){   //dequick

            leadModel.setWarning_message("Initiated lead update");
        }

        //LeadMgr.getInstance(PSFUpdateLeadActivity.this).updateLead(leadModel, "offline");
        LeadMgr.getInstance(PSFUpdateLeadActivity.this).updatePSFLeadStatus(leadModel, "offline");



        /*if (Utils.isNotNullAndNotEmpty(etLeadStage.getText().toString()))
            if (etLeadStage.getText().toString().equalsIgnoreCase("Contacted & meeting fixed")) {
                postcreateplan(TimeUtils.convertDatetoUnix(etNextFollowupDate.getText().toString()));
            } else if (etLeadStage.getText().toString().equalsIgnoreCase("Proposition presented")) {
                postcreateplan(TimeUtils.convertDatetoUnix(etNextFollowupDate.getText().toString()));
            }*/
        LeadIntentService.syncPSFLeadstoServer(PSFUpdateLeadActivity.this);


       /* showAlert("", "Your Lead update has been initiated.",
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);*/

        ProgressDialog progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);//you can cancel it by pressing back button
        progressBar.setMessage("please wait ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();//displays the progress bar



        Handler mHand  = new Handler();
        mHand.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                progressBar.dismiss();

                showAlert("", "Your Lead update has been initiated.",
                        "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        },
                        "",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        }, false);



            }
        }, 5000);



    }


    private void updateNotification() {
        //showProgressDialog("Refreshing data . . .");
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getProposalCodes = client.getPsfNotifications(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        getProposalCodes.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFNotificationsModel>>() {
                        }.getType();
                        List<PSFNotificationsModel> psfNotificationsModelList = new Gson().fromJson(response.body().getData(), listType);
                        List<PSFNotificationsModel> finalList = new ArrayList<>();
                        if (psfNotificationsModelList.size() > 0) {
                            for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                                if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                                    finalList.add(notificationsModel);
                                }

                            }

                            mCartItemCount = finalList.size();
                            setupBadge();
                            //dismissProgressDialog();

                        } else {
                            mCartItemCount = 0;
                            setupBadge();
                            //dismissProgressDialog();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                //dismissProgressDialog();
            }
        });


    }



 /*   private void postcreateplan(String sdate) {
        plannerModel.setGroupid(groupId);
        plannerModel.setUserid(userid);
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadid())) {
            plannerModel.setLeadid(leadModel.getLeadid());
            plannerModel.setLocalleadid(leadModel.getId());
        } else {
            plannerModel.setLocalleadid(leadModel.getId());
        }
        if (Utils.isNotNullAndNotEmpty(sdate)) {
            plannerModel.setScheduleday(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", TimeUtils.getFormattedDatefromUnix(sdate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setSchedulemonth(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", TimeUtils.getFormattedDatefromUnix(sdate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setScheduleyear(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", TimeUtils.getFormattedDatefromUnix(sdate, "dd/MM/yyyy hh:mm a"))));
            plannerModel.setScheduletime(Integer.parseInt(sdate));
        }
        plannerModel.setFirstname(leadModel.getFirstname());
        plannerModel.setLastname(leadModel.getLastname());
        plannerModel.setPlanstatus(1);
        plannerModel.setCompletestatus(0);
        plannerModel.setType("Appointment");
        plannerModel.setActivitytype("Appointment");
        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadstage()))
            plannerModel.setSubtype(leadModel.getLeadstage());
        plannerModel.setNetwork_status("offline");
        PlannerMgr.getInstance(PSFUpdateLeadActivity.this).insertPlanner(plannerModel, leadModel.getUserid(), leadModel.getGroupid());
    }*/
}
