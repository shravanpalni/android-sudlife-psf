package com.outwork.sudlife.psf.branches.models;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import com.outwork.sudlife.psf.restinterfaces.POJO.GeoAddress;

/**
 * Created by Panch on 2/22/2017.
 */
public class ContactModel {
    private static final String TAG = ContactModel.class.getSimpleName();

    @SerializedName("contactid")
    @Expose
    private String contactid;
    @SerializedName("salutation")
    @Expose
    private String salutation;
    @SerializedName("firstname")
    @Expose
    private String cfirstname;
    @SerializedName("lastname")
    @Expose
    private String clname;
    @SerializedName("displayname")
    @Expose
    private String cdisplayname;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("phonenumber")
    @Expose
    private String phoneno;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("practicing")
    @Expose
    private String practicing;
    @SerializedName("ownerid")
    @Expose
    private String ownerid;
    @SerializedName("assignedto")
    @Expose
    private String assignedto;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("customerid")
    @Expose
    private String customerid;
    @SerializedName("customername")
    @Expose
    private String customername;
    @SerializedName("createddate")
    @Expose
    private String createddate;
    @SerializedName("modifieddate")
    @Expose
    private String modifieddate;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("speciality")
    @Expose
    private String speciality;
    @SerializedName("classification")
    @Expose
    private String classification;
    @SerializedName("customerclassification")
    @Expose
    private String customerclassification;
    @SerializedName("address")
    @Expose
    private GeoAddress address;
    @SerializedName("currentaddress")
    @Expose
    private GeoAddress currentaddress;
    @SerializedName("customertype")
    @Expose
    private String customertype;

    private int id;
    private String network_status;
    private String internaltype;
    private GeoAddress geoAddress;
    private String orgType;

    public ContactModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassification() {
        return classification;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getNetwork_status() {
        return network_status;
    }

    public void setNetwork_status(String network_status) {
        this.network_status = network_status;
    }

    public String getOwnerid() {
        return ownerid;
    }

    public void setOwnerid(String ownerid) {
        this.ownerid = ownerid;
    }

    public String getPracticing() {
        return practicing;
    }

    public void setPracticing(String practicing) {
        this.practicing = practicing;
    }

    public String getCustomerclassification() {
        return customerclassification;
    }

    public void setCustomerclassification(String customerclassification) {
        this.customerclassification = customerclassification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getCustomertype() {
        return customertype;
    }

    public void setCustomertype(String customertype) {
        this.customertype = customertype;
    }

    public String getContactid() {
        return contactid;
    }

    public void setContactid(String contactid) {
        this.contactid = contactid;
    }

    public String getCfirstname() {
        return cfirstname;
    }

    public String getAssignedto() {
        return assignedto;
    }

    public void setAssignedto(String assignedto) {
        this.assignedto = assignedto;
    }

    public void setCfirstname(String cfirstname) {
        this.cfirstname = cfirstname;
    }

    public String getClname() {
        return clname;
    }

    public void setClname(String clname) {
        this.clname = clname;
    }

    public String getCdisplayname() {
        return cdisplayname;
    }

    public void setCdisplayname(String cdisplayname) {
        this.cdisplayname = cdisplayname;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(String modifieddate) {
        this.modifieddate = modifieddate;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


    public String getInternaltype() {
        return internaltype;
    }

    public void setInternaltype(String internaltype) {
        this.internaltype = internaltype;
    }

    public GeoAddress getGeoAddress() {
        return geoAddress;
    }

    public void setGeoAddress(GeoAddress geoAddress) {
        this.geoAddress = geoAddress;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public GeoAddress getCurrentaddress() {
        return currentaddress;
    }

    public void setCurrentaddress(GeoAddress currentaddress) {
        this.currentaddress = currentaddress;
    }

    public GeoAddress getAddress() {
        return address;
    }

    public void setAddress(GeoAddress address) {
        this.address = address;
    }

    public ContactModel(String json) {
        ContactModel temp = new Gson().fromJson(json, ContactModel.class);
        this.id = temp.getId();
        this.contactid = temp.getContactid();
        this.customerid = temp.getCustomerid();
        this.cfirstname = temp.getCfirstname();
        this.cdisplayname = temp.getCdisplayname();
        this.designation = temp.getDesignation();
        this.phoneno = temp.getPhoneno();
        this.email = temp.getEmail();
        this.customername = temp.getCustomername();
        this.groupid = temp.getGroupid();
        this.userid = temp.getUserid();
        this.speciality = temp.getSpeciality();
        this.classification = temp.getClassification();
        this.address = temp.getAddress();
        this.internaltype = temp.getInternaltype();
        this.orgType = temp.getOrgType();
    }

    public JSONObject getAsJsonObject() {
        String jsonRepresentation = new Gson().toJson(this, ContactModel.class);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonRepresentation);
        } catch (JSONException e) {
            Log.e("", "Error converting to JSON in getAsJsonObject()" + e.getMessage());
        }
        return jsonObject;
    }

}
