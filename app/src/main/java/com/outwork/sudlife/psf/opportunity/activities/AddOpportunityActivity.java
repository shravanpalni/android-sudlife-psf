package com.outwork.sudlife.psf.opportunity.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.opportunity.OpportunityMgr;
import com.outwork.sudlife.psf.opportunity.models.OpportunityItems;
import com.outwork.sudlife.psf.opportunity.models.OpportunityModel;
import com.outwork.sudlife.psf.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

public class AddOpportunityActivity extends BaseActivity {
    private TextView oppstage;
    private Button submit;
    private EditText firstname, lastname, age, occupation, income, familymem, remarks, opprtValue, closingDate;
    private AppCompatSpinner fstage;
    private OpportunityModel opportunityModel = new OpportunityModel();
    private String  userName,sOrderDate, closingdate, sclosingDate;
    private List<String> stagelist = new ArrayList<>();
    private RadioGroup gender;
    private RadioButton male, female;
    private ArrayAdapter<String> stageAdapter;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
            } else if (customSelector == 1) {
                closingDate.setText(TimeUtils.getDateFormat(date));
                closingDate.setEnabled(true);
                sclosingDate = TimeUtils.convertDatetoUnix(TimeUtils.getDateFormat(date));
            }
        }

        @Override
        public void onDateTimeCancel() {
            closingDate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opport_form);

        mgr = LocalBroadcastManager.getInstance(this);

        firstname = (EditText) findViewById(R.id.firstName);
        lastname = (EditText) findViewById(R.id.lastName);
        gender = (RadioGroup) findViewById(R.id.gender);
        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);
        age = (EditText) findViewById(R.id.age);
        occupation = (EditText) findViewById(R.id.occupation);
        income = (EditText) findViewById(R.id.income);
        familymem = (EditText) findViewById(R.id.familymem);
        remarks = (EditText) findViewById(R.id.fRemarks);
        closingDate = (EditText) findViewById(R.id.closingDate);
        oppstage = (TextView) findViewById(R.id.oppstage);
        opprtValue = (EditText) findViewById(R.id.fOpprtValue);
        submit = (Button) findViewById(R.id.save);
        fstage = (AppCompatSpinner) findViewById(R.id.fstage);
        male.setChecked(true);
        initToolBar();
        getStages();
        setListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("oppstage_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Create Lead");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private void getStages() {
        stagelist = OpportunityMgr.getInstance(AddOpportunityActivity.this).getOpportunitystagesnamesList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        if (stagelist.size() > 0) {
            stageAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, stagelist);
            stageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            fstage.setAdapter(stageAdapter);
            fstage.setSelection(0);
            fstage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    fstage.setSelection(position);
                    opportunityModel.setStage(fstage.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } else {
            oppstage.setVisibility(View.GONE);
            fstage.setVisibility(View.GONE);
        }
    }

    private void setListeners() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getStages();
            }
        };

        remarks.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.fRemarks) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        closingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closingDate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 0);
                Date backDate = calendar.getTime();
                calendar.add(Calendar.DAY_OF_YEAR, +15);
                Date initDate = calendar.getTime();
                calendar.add(Calendar.DAY_OF_YEAR, 100);
                Date maxDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(backDate)
                        .setInitialDate(initDate)
                        .setMaxDate(maxDate)
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    createOpportunity();
                    OpportunityMgr.getInstance(AddOpportunityActivity.this).insertLocalOpportunity(opportunityModel, "offline");
                    OpportunityIntentService.syncOfflineRecords(AddOpportunityActivity.this);
                    showAlert("", "Your Opportunity is saved successfully",
                            "OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                }
                            },
                            "",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                }
                            }, false);
                }
            }
        });

    }

    private void createOpportunity() {
        opportunityModel.setUserid(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        opportunityModel.setGroupid(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
        if (Utils.isNotNullAndNotEmpty(firstname.getText().toString())) {
            opportunityModel.setFirstname(firstname.getText().toString().replaceAll(",", ""));
        }
        if (Utils.isNotNullAndNotEmpty(lastname.getText().toString())) {
            opportunityModel.setLastname(lastname.getText().toString().replaceAll(",", ""));
        }
        if (Utils.isNotNullAndNotEmpty(occupation.getText().toString())) {
            opportunityModel.setOccupation(occupation.getText().toString());
        }
        if (Utils.isNotNullAndNotEmpty(age.getText().toString())) {
            opportunityModel.setAge(Integer.parseInt(age.getText().toString()));
        }
        if (Utils.isNotNullAndNotEmpty(income.getText().toString())) {
            opportunityModel.setIncome(Integer.parseInt(income.getText().toString()));
        }
        if (Utils.isNotNullAndNotEmpty(familymem.getText().toString())) {
            opportunityModel.setFamilymembers(Integer.parseInt(familymem.getText().toString()));
        }

        if (Utils.isNotNullAndNotEmpty(opprtValue.getText().toString())) {
            opportunityModel.setValue(opprtValue.getText().toString().replaceAll(",", ""));
        }
        if (Utils.isNotNullAndNotEmpty(opprtValue.getText().toString())) {
            opportunityModel.setOrdervalue(opprtValue.getText().toString().replaceAll(",", ""));
        }
        if (Utils.isNotNullAndNotEmpty(closingDate.getText().toString())) {
            opportunityModel.setClosingdate(sclosingDate);
        }
        if (Utils.isNotNullAndNotEmpty(remarks.getText().toString())) {
            opportunityModel.setRemarks(remarks.getText().toString());
        }
        List<OpportunityItems> deliveryList = new ArrayList<>();
//        OpportunityItems opportunityItems = new OpportunityItems();
//        opportunityItems.setProduct("product3");
//        opportunityItems.setProduct("80");
//        opportunityItems.setProduct("25.0");
//        deliveryList.add(opportunityItems);
        opportunityModel.setItems(deliveryList);
        opportunityModel.setGender(((RadioButton) findViewById(gender.getCheckedRadioButtonId())).getText().toString());
    }

    private boolean validate() {
        if (!Utils.isNotNullAndNotEmpty(firstname.getText().toString())) {
            firstname.setError("Firstname is mandatory");
            firstname.requestFocus();
            return false;
        }
        if (!Utils.isNotNullAndNotEmpty(lastname.getText().toString())) {
            lastname.setError("Lastname is mandatory");
            lastname.requestFocus();
            return false;
        }
        if (!Utils.isNotNullAndNotEmpty(occupation.getText().toString())) {
            occupation.setError("Occupation is mandatory");
            occupation.requestFocus();
            return false;
        }
        if (!Utils.isNotNullAndNotEmpty(opprtValue.getText().toString())) {
            opprtValue.setError("Amount is mandatory");
            opprtValue.requestFocus();
            return false;
        }
        if (!Utils.isNotNullAndNotEmpty(closingDate.getText().toString())) {
            closingDate.setError("Closing date is mandatory");
            closingDate.setFocusableInTouchMode(true);
            closingDate.requestFocus();
            return false;
        }
        if (!Utils.isNotNullAndNotEmpty(remarks.getText().toString())) {
            remarks.setError("Description is mandtory");
            remarks.requestFocus();
            return false;
        }
        return true;
    }

    private void saveOpportunity() {
        if (validate()) {
            createOpportunity();
            OpportunityMgr.getInstance(AddOpportunityActivity.this).insertLocalOpportunity(opportunityModel, "offline");
            OpportunityIntentService.syncOfflineRecords(AddOpportunityActivity.this);
            showAlert("", "Your Opportunity is saved successfully",
                    "OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    },
                    "",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    }, false);
        }
    }
}
