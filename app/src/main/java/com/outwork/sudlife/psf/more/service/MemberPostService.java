package com.outwork.sudlife.psf.more.service;

import com.outwork.sudlife.psf.more.models.Comment;
import com.outwork.sudlife.psf.more.models.Msg;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MemberPostService {

    @GET("post/groups/{groupid}")
    Call<RestResponse> getFeed(@Header("utoken") String userToken,
                               @Path("groupid") String groupid,
                               @Query("type") String type,
                               @Query("initialid") String initialid,
                               @Query("count") String count,
                               @Query("direction") String direction,
                               @Query("date") String date);

    @GET("post/groups/{groupid}")
    Call<RestResponse> getMyfeed(@Header("utoken") String userToken, @Path("groupid") String groupid,
                                 @Query("initialid") String initialid,
                                 @Query("count") String count,
                                 @Query("direction") String direction,
                                 @Query("date") String date);

    @GET("Message/message")
    Call<RestResponse> getMemberMessage(@Header("utoken") String userToken,
                                        @Query("type") String type,
                                        @Query("subtype") String subtype,
                                        @Query("messageid") String messageid);

    @POST("Message/msg")
    Call<RestResponse> postMemmberMessage(@Header("utoken") String userToken,
                                          @Query("groupid") String groupid,
                                          @Body Msg sJsonBody);

    @POST("Comment/comments/msg")
    Call<RestResponse> postComment(@Header("utoken") String userToken, @Body Comment sJsonBody);

    @GET("Comment/comments/msg")
    Call<RestResponse> getComments(@Header("utoken") String userToken,
                                   @Query("subjectID") String subjectid,
                                   @Query("initialid") String initialid,
                                   @Query("direction") String direction,
                                   @Query("date") String date,
                                   @Query("count") String count);

    @GET("Post/types")
    Call<RestResponse> getPostTypes(@Header("utoken") String userToken,
                                    @Query("groupid") String groupid);
}
