package com.outwork.sudlife.psf.targets.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.targets.services.TargetsIntentService;
import com.outwork.sudlife.psf.targets.targetsmngr.TargetsMgr;
import com.outwork.sudlife.psf.targets.adapters.TargetsSummaryAdapter;
import com.outwork.sudlife.psf.targets.models.MSBModel;
import com.outwork.sudlife.psf.targets.models.TargetSummaryModel;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class MyPerformanceViewActivity extends BaseActivity {
    String monthsArray[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private TextView tvMonth;
    private RecyclerView rcvForAll;
    private TextView tvNodata;
    private Spinner spItems;
    private LinearLayout llrcv;
    //    String[] spitemsArray = {"My Performance", "My Performance TillDate", "Team performance"};
    String[] spitemsArray = {"My Performance", /*"My Performance TillDate", */"Team performance"};
    String[] spitemsArrayMyPerformance = {"My Performance"};
    private int pos;
    private TextView fetcchtime;
    private BroadcastReceiver mBroadcastReceiver;
    private LocalBroadcastManager mgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_targets_view);
        mgr = LocalBroadcastManager.getInstance(this);
        if (SharedPreferenceManager.getInstance().getString(Constants.TARGETS_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                TargetsIntentService.insertTargetRecords(MyPerformanceViewActivity.this);
            }
        }
        initToolBar();
        tvMonth = (TextView) findViewById(R.id.toolbar_title);
        llrcv = (LinearLayout) findViewById(R.id.ll_in_rcv);
        tvNodata = (TextView) findViewById(R.id.tv_nodata);
        spItems = (Spinner) findViewById(R.id.sp_spinner_items);
        rcvForAll = (RecyclerView) findViewById(R.id.rcv_ftm);
        fetcchtime = (TextView) findViewById(R.id.fetchtime);
        String currentDate = TimeUtils.getCurrentDate("yyyy-MM-dd");
        final int day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", currentDate));
        final int monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", currentDate));
        int year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", currentDate));
        String currentTime = TimeUtils.getCurrentUnixTimeStamp();
        String time = TimeUtils.getFormattedDatefromUnix(currentTime, "yyyy-MM-dd");

        if (SharedPreferenceManager.getInstance().getString(Constants.INTERNALROLE, "").equalsIgnoreCase("member")) {
            ArrayAdapter arrayAdapter = new ArrayAdapter(MyPerformanceViewActivity.this, android.R.layout.simple_dropdown_item_1line, spitemsArrayMyPerformance);
            spItems.setAdapter(arrayAdapter);
        } else {
            ArrayAdapter arrayAdapter = new ArrayAdapter(MyPerformanceViewActivity.this, android.R.layout.simple_dropdown_item_1line, spitemsArray);
            spItems.setAdapter(arrayAdapter);
        }

        spItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getSelectedItem().toString();
                int postion = Arrays.asList(monthsArray).indexOf(tvMonth.getText().toString());
                if (item.equalsIgnoreCase("My Performance")) {
                    pos = postion + 1;
                    getListOfMsbRecordsBasedOnMonth(postion + 1);
                } else if (item.equalsIgnoreCase("My Performance TillDate")) {
                    pos = postion + 1;
                    getListOfMsbRecordsBasedOnMonth(postion + 1);
                    HashMap<String, Integer> tilldateTargetsRecordsHashMap = PlannerMgr.getInstance(MyPerformanceViewActivity.this).getPlannerListByScheduleDates(monthno, day);
                    getListOfMsbRecordsBasedOnTillDate(postion + 1, tilldateTargetsRecordsHashMap);
                } else if (item.equalsIgnoreCase("Team performance")) {
                    startActivity(new Intent(MyPerformanceViewActivity.this, TeamPerformanceViewActivity.class));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                String items = adapterView.getSelectedItem().toString();
            }
        });

       /* spItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getSelectedItem().toString();
                int postion = Arrays.asList(monthsArray).indexOf(tvMonth.getText().toString());
                if (item.equalsIgnoreCase("My Performance")) {
                    getListOfMsbRecordsBasedOnMonth(postion + 1);
                } else if (item.equalsIgnoreCase("My Performance TillDate")) {
                    getListOfMsbRecordsBasedOnMonth(postion + 1);

                 *//*   HashMap<String, Integer> tilldateTargetsRecordsHashMap = PlannerMgr.getInstance(MyPerformanceViewActivity.this).getPlannerListByScheduleDates(monthno, day);

                    getListOfMsbRecordsBasedOnTillDate(postion + 1, tilldateTargetsRecordsHashMap);*//*
                } else {
                    startActivity(new Intent(MyPerformanceViewActivity.this, TeamPerformanceViewActivity.class));
                }
            }
        });*/
        //get activitycount,visitcount,visitscompleted,activitiescompleted, from planner table(tilldate)
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getListOfMsbRecordsBasedOnMonth(pos);
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.calender_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (id == R.id.calender) {
            monthsCalendarDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("targets_broadcast"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    //tool bar intilization
    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        long currentmnth = Long.parseLong(TimeUtils.getCurrentDate("MM"));
        textView.setText(getMonth(currentmnth));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        }
    }

    //months calendar
    public void monthsCalendarDialog() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.list_item_for_month, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        final ListView lvItems = (ListView) deleteDialogView.findViewById(R.id.lv_month);
        ArrayAdapter arrayAdapter = new ArrayAdapter(MyPerformanceViewActivity.this, android.R.layout.simple_list_item_1, monthsArray);
        lvItems.setAdapter(arrayAdapter);

        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvMonth.setText(adapterView.getItemAtPosition(i).toString());
                pos = i + 1;
                getListOfMsbRecordsBasedOnMonth(i + 1);
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    public String getMonth(long month) {
        return new DateFormatSymbols().getMonths()[(int) month - 1];
    }


    //get list of target records based month from monthlysummerytable
    public void getListOfMsbRecordsBasedOnMonth(int monthno) {
        ArrayList<TargetSummaryModel> ftmArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> activitiesPlannesArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> visitsPlannedArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> expectedConncetoinsArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> leadsExpectedArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> expectedBusienssArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> gapToTargetArrayList = new ArrayList<>();
        int ftm = 0, activitiesPlanned = 0, overallactivitiesCompleted = 0,
                overallconnectionsCompleted = 0, achievedConnection = 0, achievedLeads = 0, achievedBusiness = 0, visitsPlanned = 0, overallvisitsachieved = 0, expectedConnections = 0, leadsExpected = 0, expectedBusiness = 0, gapToTarget = 0;

        TargetSummaryModel targetSummaryModel;
        ArrayList<MSBModel> msbModelArrayList = TargetsMgr.getInstance(MyPerformanceViewActivity.this).getMSBRecordsBasedOnMonth(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), monthno);
        if (msbModelArrayList.size() > 0) {
            rcvForAll.setVisibility(View.VISIBLE);
            tvNodata.setVisibility(View.GONE);
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                ftm += Integer.parseInt(msbModelArrayList.get(i).getFtm());
                activitiesPlanned += Integer.parseInt(msbModelArrayList.get(i).getActivitiesplanned());
                visitsPlanned += Integer.parseInt(msbModelArrayList.get(i).getVisitsplanned());
                overallvisitsachieved += Integer.parseInt(msbModelArrayList.get(i).getVistiscompleted());
                overallactivitiesCompleted += Integer.parseInt(msbModelArrayList.get(i).getActivitiescompleted());
                achievedConnection += Integer.parseInt(msbModelArrayList.get(i).getAchievedconnection());
                achievedLeads += Integer.parseInt(msbModelArrayList.get(i).getAchievedleads());
                achievedBusiness += Integer.parseInt(msbModelArrayList.get(i).getAchievedbusiness());
                expectedConnections += Integer.parseInt(msbModelArrayList.get(i).getExpectedconncetions());
                leadsExpected += Integer.parseInt(msbModelArrayList.get(i).getExpectedleads());
                expectedBusiness += Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                gapToTarget += Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
            }
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(ftm));
                targetSummaryModel.setSummaryName("FTM");
                targetSummaryModel.setAchievedBusiness(String.valueOf(achievedBusiness));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getFtm());
                ftmArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(activitiesPlanned));
                targetSummaryModel.setOverallAchieved(String.valueOf(overallactivitiesCompleted));
                targetSummaryModel.setSummaryName("Activities");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getActivitiesplanned());
                targetSummaryModel.setAchievedValue(msbModelArrayList.get(i).getActivitiescompleted());
                activitiesPlannesArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(visitsPlanned));
                targetSummaryModel.setOverallAchieved(String.valueOf(overallvisitsachieved));
                targetSummaryModel.setSummaryName("Visits");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getVisitsplanned());
                targetSummaryModel.setAchievedValue(msbModelArrayList.get(i).getVistiscompleted());
                visitsPlannedArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(expectedConnections));
                targetSummaryModel.setOverallAchieved(String.valueOf(achievedConnection));
                targetSummaryModel.setSummaryName("Connections");
                targetSummaryModel.setAchievedConnection(String.valueOf(achievedConnection));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedconncetions());
                expectedConncetoinsArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(leadsExpected));
                targetSummaryModel.setOverallAchieved(String.valueOf(achievedLeads));
                targetSummaryModel.setSummaryName("Leads");
                targetSummaryModel.setAchievedLeads(String.valueOf(achievedLeads));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedleads());
                leadsExpectedArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(expectedBusiness));
                targetSummaryModel.setOverallAchieved(String.valueOf(achievedBusiness));
                targetSummaryModel.setSummaryName("Business");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedbusiness());
                expectedBusienssArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(gapToTarget));
                targetSummaryModel.setSummaryName("Gap To Target");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                int gaptoTarget = Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                targetSummaryModel.setTotalValue(String.valueOf(gaptoTarget));
                gapToTargetArrayList.add(targetSummaryModel);
            }
            TargetSummaryModel targetSummaryModelFtm = new TargetSummaryModel();
            targetSummaryModelFtm.setSummaryName("FTM");
            targetSummaryModelFtm.setOverallTotal(String.valueOf(ftm));
            targetSummaryModelFtm.setAchievedBusiness(String.valueOf(achievedBusiness));
            targetSummaryModelFtm.setTargetSummaryModelArrayList(ftmArrayList);

            TargetSummaryModel targetSummaryModelap = new TargetSummaryModel();
            targetSummaryModelap.setSummaryName("Activities");
            targetSummaryModelap.setOverallTotal(String.valueOf(activitiesPlanned));
            targetSummaryModelap.setOverallAchieved(String.valueOf(overallactivitiesCompleted));
            targetSummaryModelap.setTargetSummaryModelArrayList(activitiesPlannesArrayList);

            TargetSummaryModel targetSummaryModelvp = new TargetSummaryModel();
            targetSummaryModelvp.setSummaryName("Visits");
            targetSummaryModelvp.setOverallTotal(String.valueOf(visitsPlanned));
            targetSummaryModelvp.setOverallAchieved(String.valueOf(overallvisitsachieved));
            targetSummaryModelvp.setTargetSummaryModelArrayList(visitsPlannedArrayList);

            TargetSummaryModel targetSummaryModelec = new TargetSummaryModel();
            targetSummaryModelec.setSummaryName("Connections");
            targetSummaryModelec.setAchievedConnection(String.valueOf(achievedConnection));
            targetSummaryModelec.setOverallTotal(String.valueOf(expectedConnections));
            targetSummaryModelec.setTargetSummaryModelArrayList(expectedConncetoinsArrayList);

            TargetSummaryModel targetSummaryModelel = new TargetSummaryModel();
            targetSummaryModelel.setSummaryName("Leads");
            targetSummaryModelec.setAchievedLeads(String.valueOf(achievedLeads));
            targetSummaryModelec.setOverallAchieved(String.valueOf(achievedLeads));
            targetSummaryModelel.setOverallTotal(String.valueOf(leadsExpected));
            targetSummaryModelel.setTargetSummaryModelArrayList(leadsExpectedArrayList);

            TargetSummaryModel targetSummaryModeleb = new TargetSummaryModel();
            targetSummaryModeleb.setSummaryName("Business");
            targetSummaryModeleb.setOverallTotal(String.valueOf(expectedBusiness));
            targetSummaryModeleb.setTargetSummaryModelArrayList(expectedBusienssArrayList);

            TargetSummaryModel targetSummaryModelgt = new TargetSummaryModel();
            targetSummaryModelgt.setSummaryName("Gap To Target");
            targetSummaryModelgt.setOverallTotal(String.valueOf(gapToTarget));
            targetSummaryModelgt.setTargetSummaryModelArrayList(gapToTargetArrayList);

            ArrayList<TargetSummaryModel> targetSummaryModels = new ArrayList<>();
            targetSummaryModels.add(targetSummaryModelFtm);
            targetSummaryModels.add(targetSummaryModelap);
            targetSummaryModels.add(targetSummaryModelvp);
            targetSummaryModels.add(targetSummaryModelec);
            targetSummaryModels.add(targetSummaryModelel);
            targetSummaryModels.add(targetSummaryModeleb);
            targetSummaryModels.add(targetSummaryModelgt);

            TargetsSummaryAdapter targetsSummaryAdapter = new TargetsSummaryAdapter(MyPerformanceViewActivity.this, targetSummaryModels, targetSummaryModelFtm.getTargetSummaryModelArrayList().size() * 10);
            rcvForAll.setAdapter(targetsSummaryAdapter);
            rcvForAll.setLayoutManager(new LinearLayoutManager(MyPerformanceViewActivity.this));

        } else {
            rcvForAll.setVisibility(View.GONE);
            tvNodata.setVisibility(View.VISIBLE);
        }
        fetcchtime.setText("Last Refreshed Time : " + TimeUtils.getFormattedDatefromUnix(SharedPreferenceManager.getInstance().getString(Constants.TARGETS_LAST_FETCH_TIME, ""), "dd/MM/yyyy hh:mm a"));
    }

    public void getListOfMsbRecordsBasedOnTillDate(int monthno, HashMap<String, Integer> tilldateTargetsRecordsHashMap) {
        ArrayList<TargetSummaryModel> ftmArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> activitiesPlannesArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> visitsPlannedArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> expectedConncetoinsArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> leadsExpectedArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> expectedBusienssArrayList = new ArrayList<>();
        ArrayList<TargetSummaryModel> gapToTargetArrayList = new ArrayList<>();
        int ftm = 0, activitiesPlanned = 0, achievedConnection = 0, achievedLeads = 0, achievedBusiness = 0, visitsPlanned = 0, expectedConnections = 0, leadsExpected = 0, expectedBusiness = 0, gapToTarget = 0;
        TargetSummaryModel targetSummaryModel;
        ArrayList<MSBModel> msbModelArrayList = TargetsMgr.getInstance(MyPerformanceViewActivity.this).getMSBRecordsBasedOnMonth(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), monthno);
        if (msbModelArrayList.size() > 0) {
            rcvForAll.setVisibility(View.VISIBLE);
            tvNodata.setVisibility(View.GONE);
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                ftm += Integer.parseInt(msbModelArrayList.get(i).getFtm());
                activitiesPlanned += Integer.parseInt(msbModelArrayList.get(i).getActivitiesplanned());
                visitsPlanned += Integer.parseInt(msbModelArrayList.get(i).getVisitsplanned());
                achievedConnection += Integer.parseInt(msbModelArrayList.get(i).getAchievedconnection());
                achievedLeads += Integer.parseInt(msbModelArrayList.get(i).getAchievedleads());
                achievedBusiness += Integer.parseInt(msbModelArrayList.get(i).getAchievedbusiness());
                expectedConnections += Integer.parseInt(msbModelArrayList.get(i).getExpectedconncetions());
                leadsExpected += Integer.parseInt(msbModelArrayList.get(i).getExpectedleads());
                expectedBusiness += Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                gapToTarget += Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
            }
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(ftm));
                targetSummaryModel.setSummaryName("FTM");
                targetSummaryModel.setAchievedBusiness(String.valueOf(achievedBusiness));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getFtm());
                ftmArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(activitiesPlanned));
                targetSummaryModel.setSummaryName("Activity Planned");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getActivitiesplanned());
                activitiesPlannesArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(visitsPlanned));
                targetSummaryModel.setSummaryName("Visits Planned");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getVisitsplanned());
                visitsPlannedArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(expectedConnections));
                targetSummaryModel.setSummaryName("Expected Connections");
                targetSummaryModel.setAchievedConnection(String.valueOf(achievedConnection));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedconncetions());
                expectedConncetoinsArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(leadsExpected));
                targetSummaryModel.setSummaryName("Expected Leads");
                targetSummaryModel.setAchievedLeads(String.valueOf(achievedLeads));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedleads());
                leadsExpectedArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(expectedBusiness));
                targetSummaryModel.setSummaryName("Expected Business");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedbusiness());
                expectedBusienssArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(gapToTarget));
                targetSummaryModel.setSummaryName("Gap To Target");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                int gaptoTarget = Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                targetSummaryModel.setTotalValue(String.valueOf(gaptoTarget));
                gapToTargetArrayList.add(targetSummaryModel);
            }
            TargetSummaryModel targetSummaryModelFtm = new TargetSummaryModel();
            targetSummaryModelFtm.setSummaryName("FTM");
            targetSummaryModelFtm.setOverallTotal(String.valueOf(ftm));
            targetSummaryModelFtm.setAchievedBusiness(String.valueOf(achievedBusiness));
            targetSummaryModelFtm.setTargetSummaryModelArrayList(ftmArrayList);

            TargetSummaryModel targetSummaryModelap = new TargetSummaryModel();
            targetSummaryModelap.setSummaryName("Activity Planned");
            targetSummaryModelap.setOverallTotal(String.valueOf(activitiesPlanned));
            targetSummaryModelap.setTargetSummaryModelArrayList(activitiesPlannesArrayList);


            TargetSummaryModel targetSummaryModelvp = new TargetSummaryModel();
            targetSummaryModelvp.setSummaryName("Visits Planned");
            targetSummaryModelvp.setOverallTotal(String.valueOf(visitsPlanned));
            targetSummaryModelvp.setTargetSummaryModelArrayList(visitsPlannedArrayList);

            TargetSummaryModel targetSummaryModelec = new TargetSummaryModel();
            targetSummaryModelec.setSummaryName("Expected Connections");
            targetSummaryModelec.setAchievedConnection(String.valueOf(achievedConnection));
            targetSummaryModelec.setOverallTotal(String.valueOf(expectedConnections));
            targetSummaryModelec.setTargetSummaryModelArrayList(expectedConncetoinsArrayList);

            TargetSummaryModel targetSummaryModelel = new TargetSummaryModel();
            targetSummaryModelel.setSummaryName("Expected Leads");
            targetSummaryModelec.setAchievedLeads(String.valueOf(achievedLeads));
            targetSummaryModelel.setOverallTotal(String.valueOf(leadsExpected));
            targetSummaryModelel.setTargetSummaryModelArrayList(leadsExpectedArrayList);

            TargetSummaryModel targetSummaryModeleb = new TargetSummaryModel();
            targetSummaryModeleb.setSummaryName("Expected Business");
            targetSummaryModeleb.setOverallTotal(String.valueOf(expectedBusiness));
            targetSummaryModeleb.setTargetSummaryModelArrayList(expectedBusienssArrayList);

            TargetSummaryModel targetSummaryModelgt = new TargetSummaryModel();
            targetSummaryModelgt.setSummaryName("Gap To Target");
            targetSummaryModelgt.setOverallTotal(String.valueOf(gapToTarget));
            targetSummaryModelgt.setTargetSummaryModelArrayList(gapToTargetArrayList);

            ArrayList<TargetSummaryModel> targetSummaryModels = new ArrayList<>();
            targetSummaryModels.add(targetSummaryModelFtm);
            targetSummaryModels.add(targetSummaryModelap);
            targetSummaryModels.add(targetSummaryModelvp);
            targetSummaryModels.add(targetSummaryModelec);
            targetSummaryModels.add(targetSummaryModelel);
            targetSummaryModels.add(targetSummaryModeleb);
            targetSummaryModels.add(targetSummaryModelgt);

            TargetsSummaryAdapter targetsSummaryAdapter = new TargetsSummaryAdapter(MyPerformanceViewActivity.this, targetSummaryModels, targetSummaryModelFtm.getTargetSummaryModelArrayList().size() * 10);
            rcvForAll.setAdapter(targetsSummaryAdapter);
            rcvForAll.setLayoutManager(new LinearLayoutManager(MyPerformanceViewActivity.this));

        } else {
            rcvForAll.setVisibility(View.GONE);
            tvNodata.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (SharedPreferenceManager.getInstance().getString(Constants.LOGINTYPE, "").equalsIgnoreCase("member")) {
            ArrayAdapter arrayAdapter = new ArrayAdapter(MyPerformanceViewActivity.this, android.R.layout.simple_dropdown_item_1line, spitemsArrayMyPerformance);
            spItems.setAdapter(arrayAdapter);
        } else {
            ArrayAdapter arrayAdapter = new ArrayAdapter(MyPerformanceViewActivity.this, android.R.layout.simple_dropdown_item_1line, spitemsArray);
            spItems.setAdapter(arrayAdapter);
        }
    }
}
                                                   /*------end--------*/
//unused code
     /*  rcvActivitiesPlanned = (RecyclerView) findViewById(R.id.rcv_activities_planned);
          rcvVisitsPlanned = (RecyclerView) findViewById(R.id.rcv_visits_planned);
          rcvExpectedConnections = (RecyclerView) findViewById(R.id.rcv_expected_connections);
          rcvLeadsexpected = (RecyclerView) findViewById(R.id.rcv_leads_expected);
          rcvExpectedBusiness = (RecyclerView) findViewById(R.id.rcv_expected_business);
          rcvGaptoTarget = (RecyclerView) findViewById(R.id.rcv_gapto_target);
           //    tvBranchName = (TextView) findViewById(R.id.tv_summary_name);
        //    tvOverallAmount = (TextView) findViewById(R.id.tv_overall_total);
  */

     /*  TargetsSummaryAdapter targetsSummaryAdapter1 = new TargetsSummaryAdapter(MyPerformanceViewActivity.this, activitiesPlannesArrayList);
            rcvActivitiesPlanned.setAdapter(targetsSummaryAdapter1);
            rcvActivitiesPlanned.setLayoutManager(new LinearLayoutManager(MyPerformanceViewActivity.this));
            rcvActivitiesPlanned.setNestedScrollingEnabled(false);


            TargetsSummaryAdapter targetsSummaryAdapter2 = new TargetsSummaryAdapter(MyPerformanceViewActivity.this, visitsPlannedArrayList);
            rcvVisitsPlanned.setAdapter(targetsSummaryAdapter2);
            rcvVisitsPlanned.setLayoutManager(new LinearLayoutManager(MyPerformanceViewActivity.this));
            rcvVisitsPlanned.setNestedScrollingEnabled(false);


            TargetsSummaryAdapter targetsSummaryAdapter3 = new TargetsSummaryAdapter(MyPerformanceViewActivity.this, expectedConncetoinsArrayList);
            rcvExpectedConnections.setAdapter(targetsSummaryAdapter3);
            rcvExpectedConnections.setLayoutManager(new LinearLayoutManager(MyPerformanceViewActivity.this));
            rcvExpectedConnections.setNestedScrollingEnabled(false);


            TargetsSummaryAdapter targetsSummaryAdapter4 = new TargetsSummaryAdapter(MyPerformanceViewActivity.this, leadsExpectedArrayList);
            rcvLeadsexpected.setAdapter(targetsSummaryAdapter4);
            rcvLeadsexpected.setLayoutManager(new LinearLayoutManager(MyPerformanceViewActivity.this));
            rcvLeadsexpected.setNestedScrollingEnabled(false);


            TargetsSummaryAdapter targetsSummaryAdapter5 = new TargetsSummaryAdapter(MyPerformanceViewActivity.this, expectedBusienssArrayList);
            rcvExpectedBusiness.setAdapter(targetsSummaryAdapter5);
            rcvExpectedBusiness.setLayoutManager(new LinearLayoutManager(MyPerformanceViewActivity.this));
            rcvExpectedBusiness.setNestedScrollingEnabled(false);

            TargetsSummaryAdapter targetsSummaryAdapter6 = new TargetsSummaryAdapter(MyPerformanceViewActivity.this, gapToTargetArrayList);
            rcvGaptoTarget.setAdapter(targetsSummaryAdapter6);
            rcvGaptoTarget.setLayoutManager(new LinearLayoutManager(MyPerformanceViewActivity.this));
            rcvGaptoTarget.setNestedScrollingEnabled(false);*/



    /* ArrayList<MSBModel> msbModelArrayList = TargetsMgr.getInstance(MyPerformanceViewActivity.this).getMSBRecoreds(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        if (msbModelArrayList.size() > 0) {
            llrcv.setVisibility(View.VISIBLE);
            tvNodata.setVisibility(View.GONE);
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                ftm += Integer.parseInt(msbModelArrayList.get(i).getFtm());
                activitiesPlanned += Integer.parseInt(msbModelArrayList.get(i).getActivitiesplanned());
                visitsPlanned += Integer.parseInt(msbModelArrayList.get(i).getVisitsplanned());
                expectedConnections += Integer.parseInt(msbModelArrayList.get(i).getExpectedconncetions());
                leadsExpected += Integer.parseInt(msbModelArrayList.get(i).getExpectedleads());
                expectedBusiness += Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                achievedConnection += Integer.parseInt(msbModelArrayList.get(i).getAchievedconnection());
                achievedLeads += Integer.parseInt(msbModelArrayList.get(i).getAchievedleads());
                achievedBusiness += Integer.parseInt(msbModelArrayList.get(i).getAchievedbusiness());
                gapToTarget += Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
            }
            for (int i = 0; i < msbModelArrayList.size(); i++) {
                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(ftm));
                targetSummaryModel.setSummaryName("FTM");
                targetSummaryModel.setAchievedBusiness(String.valueOf(achievedBusiness));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getFtm());
                ftmArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(activitiesPlanned));
                targetSummaryModel.setSummaryName("Activity Planned");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getActivitiesplanned());
                activitiesPlannesArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(visitsPlanned));
                targetSummaryModel.setSummaryName("Visits Planned");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getVisitsplanned());
                visitsPlannedArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(expectedConnections));
                targetSummaryModel.setSummaryName("Expected Connections");
                targetSummaryModel.setAchievedConnection(String.valueOf(achievedConnection));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedconncetions());
                expectedConncetoinsArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(leadsExpected));
                targetSummaryModel.setSummaryName("Expected Leads");
                targetSummaryModel.setAchievedLeads(String.valueOf(achievedLeads));
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedleads());
                leadsExpectedArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(expectedBusiness));
                targetSummaryModel.setSummaryName("Expected Business");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                targetSummaryModel.setTotalValue(msbModelArrayList.get(i).getExpectedbusiness());
                expectedBusienssArrayList.add(targetSummaryModel);

                targetSummaryModel = new TargetSummaryModel();
                targetSummaryModel.setOverallTotal(String.valueOf(gapToTarget));
                targetSummaryModel.setSummaryName("Gap To Target");
                targetSummaryModel.setBranchName(msbModelArrayList.get(i).getCustomername());
                int gaptoTarget = Integer.parseInt(msbModelArrayList.get(i).getFtm()) - Integer.parseInt(msbModelArrayList.get(i).getExpectedbusiness());
                targetSummaryModel.setTotalValue(String.valueOf(gaptoTarget));
                gapToTargetArrayList.add(targetSummaryModel);
            }
            TargetSummaryModel targetSummaryModelftm = new TargetSummaryModel();
            targetSummaryModelftm.setSummaryName("FTM");
            targetSummaryModelftm.setOverallTotal(String.valueOf(ftm));
            targetSummaryModelftm.setTargetSummaryModelArrayList(ftmArrayList);

            TargetSummaryModel targetSummaryModelap = new TargetSummaryModel();
            targetSummaryModelap.setSummaryName("Activity Planned");
            targetSummaryModelap.setOverallTotal(String.valueOf(activitiesPlanned));
            targetSummaryModelap.setTargetSummaryModelArrayList(activitiesPlannesArrayList);


            TargetSummaryModel targetSummaryModelvp = new TargetSummaryModel();
            targetSummaryModelvp.setSummaryName("Visits Planned");
            targetSummaryModelvp.setOverallTotal(String.valueOf(visitsPlanned));
            targetSummaryModelvp.setTargetSummaryModelArrayList(visitsPlannedArrayList);

            TargetSummaryModel targetSummaryModelec = new TargetSummaryModel();
            targetSummaryModelec.setSummaryName("Expected Connections");
            targetSummaryModelec.setOverallTotal(String.valueOf(expectedConnections));
            targetSummaryModelec.setTargetSummaryModelArrayList(expectedConncetoinsArrayList);

            TargetSummaryModel targetSummaryModelel = new TargetSummaryModel();
            targetSummaryModelel.setSummaryName("Expected Leads");
            targetSummaryModelel.setOverallTotal(String.valueOf(leadsExpected));
            targetSummaryModelel.setTargetSummaryModelArrayList(leadsExpectedArrayList);

            TargetSummaryModel targetSummaryModeleb = new TargetSummaryModel();
            targetSummaryModeleb.setSummaryName("Expected Business");
            targetSummaryModeleb.setOverallTotal(String.valueOf(expectedBusiness));
            targetSummaryModeleb.setTargetSummaryModelArrayList(expectedBusienssArrayList);

            TargetSummaryModel targetSummaryModelgt = new TargetSummaryModel();
            targetSummaryModelgt.setSummaryName("Gap To Target");
            targetSummaryModelgt.setOverallTotal(String.valueOf(gapToTarget));
            targetSummaryModelgt.setTargetSummaryModelArrayList(gapToTargetArrayList);

            ArrayList<TargetSummaryModel> targetSummaryModels = new ArrayList<>();
            targetSummaryModels.add(targetSummaryModelftm);
            targetSummaryModels.add(targetSummaryModelap);
            targetSummaryModels.add(targetSummaryModelvp);
            targetSummaryModels.add(targetSummaryModelec);
            targetSummaryModels.add(targetSummaryModelel);
            targetSummaryModels.add(targetSummaryModeleb);
            targetSummaryModels.add(targetSummaryModelgt);
            TargetsSummaryAdapter targetsSummaryAdapter = new TargetsSummaryAdapter(MyPerformanceViewActivity.this, targetSummaryModels, targetSummaryModelftm.getTargetSummaryModelArrayList().size() * 10);
            rcvForAll.setAdapter(targetsSummaryAdapter);
            rcvForAll.setLayoutManager(new LinearLayoutManager(MyPerformanceViewActivity.this));
        } else {
            llrcv.setVisibility(View.GONE);
            tvNodata.setVisibility(View.VISIBLE);
        }*/
