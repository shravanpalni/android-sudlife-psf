package com.outwork.sudlife.psf.more.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Panch on 11/25/2016.
 */
public class FileModel {
    @SerializedName("fileid")
    @Expose
    private String fileid;
    @SerializedName("fileurl")
    @Expose
    private String fileurl;
    @SerializedName("fileName")
    @Expose
    private String fileName;
    @SerializedName("extension")
    @Expose
    private String extension;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("altfileurl")
    @Expose
    private String altfileurl;
    @SerializedName("altfileurl1")
    @Expose
    private String altfileurl1;
    @SerializedName("altfileurl2")
    @Expose
    private String altfileurl2;

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    public String getFileurl() {
        return fileurl;
    }

    public void setFileurl(String fileurl) {
        this.fileurl = fileurl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAltfileurl() {
        return altfileurl;
    }

    public void setAltfileurl(String altfileurl) {
        this.altfileurl = altfileurl;
    }

    public String getAltfileurl1() {
        return altfileurl1;
    }

    public void setAltfileurl1(String altfileurl1) {
        this.altfileurl1 = altfileurl1;
    }

    public String getAltfileurl2() {
        return altfileurl2;
    }

    public void setAltfileurl2(String altfileurl2) {
        this.altfileurl2 = altfileurl2;
    }
}
