package com.outwork.sudlife.psf.localbase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.psf.R;

/**
 * Created by Panch on 2/23/2017.
 */

public class ProductAutocompleteAdapter extends BaseAdapter implements Filterable {
    private Context context;
    private final LayoutInflater mInflater;

    private List<ProductMasterDto> resultList = new ArrayList<>();
    private static final int MAX_RESULTS = 10;
    private String groupid;
    private String userId;

    static class ViewHolder {
        public TextView name;
        public ImageView picture;
    }

    public ProductAutocompleteAdapter(Context context, String groupId, String userId){
        this.context = context;
        this.groupid=groupId;
        this.userId = userId;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.resultList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this.resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @SuppressLint("ViewHolder") @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {


        View view = convertView;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_product_dropdown, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView)view.findViewById(R.id.productname);
            view.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) view.getTag();

        ProductMasterDto dto = (ProductMasterDto) this.resultList.get(position);

        holder.name.setText(dto.getProductName());

        return view;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @Override
            public String convertResultToString(Object resultValue) {
                return ((ProductMasterDto) resultValue).getProductName();
            }
            @Override
            protected FilterResults performFiltering(CharSequence userinput) {
                FilterResults filterResults = new FilterResults();
                if (userinput != null) {

                    List<ProductMasterDto> prodList = new ProductDataMgr(context).getProductList(groupid,userinput.toString());
                    filterResults.values = prodList;
                    filterResults.count = prodList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence userinput, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<ProductMasterDto>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }


}