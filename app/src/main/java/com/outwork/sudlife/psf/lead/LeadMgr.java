package com.outwork.sudlife.psf.lead;

import android.content.Context;

import com.outwork.sudlife.psf.lead.db.LeadDao;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;

import java.util.List;

public class LeadMgr {

    private static Context context;
    private static LeadDao leadDao;

    public LeadMgr(Context context) {
        this.context = context;
        leadDao = leadDao.getInstance(context);
    }

    private static LeadMgr instance;

    public static LeadMgr getInstance(Context context) {
        if (instance == null)
            instance = new LeadMgr(context);
        return instance;
    }

    public void insertLeadsList(List<LeadModel> list, String userId) {
        leadDao.insertLeadsList(list, userId);
    }

    public long insertLocalLead(LeadModel leadModel, String status) {
        return leadDao.insertLocalLead(leadModel, status);
    }

    public void updateLeadOnline(LeadModel leadModel, String userId, String leadId) {
        leadDao.updateLeadOnline(leadModel, userId, leadId);
    }

    public void updateLeaddumyOnline(LeadModel leadModel, String userId) {
        leadDao.updateLeaddumyOnline(leadModel, userId);
    }

    public void updateLead(LeadModel leadModel, String network_status) {
        leadDao.updateLead(leadModel, network_status);
    }

    public List<LeadModel> getLeadsList(String userId) {
        return leadDao.getLeadsList(userId);
    }

    public List<LeadModel> getLeadsListonLeadStage(String userId, String leadstage) {
        return leadDao.getLeadsListonLeadStage(userId, leadstage);
    }

    public LeadModel getLeadbyId(String leadId, String userId) {
        return leadDao.getLeadbyId(leadId, userId);
    }

    public LeadModel getLeadbylocalId(int leadId, String userId) {
        return leadDao.getLeadbylocalId(leadId, userId);
    }

    public List<LeadModel> getOfflineLeadsList(String userId, String newtwork_status) {
        return leadDao.getOfflineLeadsList(userId, newtwork_status);
    }

    public List<String> getLeadsNamesList(String userId) {
        return leadDao.getLeadsNamesList(userId);
    }

    public List<LeadModel> getLeadListforSearch(String userId, String userinput) {
        return leadDao.getLeadListforSearch(userId, userinput);
    }



    public List<LeadModel> getLeadListforSearchBM(String userId, String userinput,List<String> users) {
        return leadDao.getLeadListforSearchBM(userId, userinput,users);
    }

    public List<LeadModel> getLeadListforfilter(String userId, String userinput) {
        return leadDao.getLeadListforfilter(userId, userinput);
    }

    public List<LeadModel> getLeadListSearchbyName(String userId, String userinput, String stage) {
        return leadDao.getLeadListSearchbyName(userId, userinput, stage);
    }





    public List<LeadModel> getLeadListSearchbyBranchname(String userId, String userinput, String stage) {
        return leadDao.getLeadListSearchbyBranchname(userId, userinput, stage);
    }

    public List<LeadModel> getLeadListSearchbyBankname(String userId, String userinput, String stage) {
        return leadDao.getLeadListSearchbyBankname(userId, userinput, stage);
    }


    public List<LeadModel> getLeadListSearchbyBanknameBM(String userId, String userinput, String stage,List<String> users) {
        return leadDao.getLeadListSearchbyBanknameBM(userId, userinput, stage,users);
    }


    public List<LeadModel> getLeadListSearchbyContactno(String userId, String userinput, String stage) {
        return leadDao.getLeadListSearchbyContactno(userId, userinput, stage);
    }

    public List<LeadModel> getLeadListSearchbySUDCode(String userId, String userinput, String stage) {
        return leadDao.getLeadListSearchbySUDCode(userId, userinput, stage);
    }

    public List<LeadModel> getLeadListbetweendates(String userId, String strtdate, String endate, String leadstage) {
        return leadDao.getLeadListbetweendates(userId, strtdate, endate, leadstage);
    }

    public List<LeadModel> getLeadListbetweendates(String userId, String strtdate, String endate) {
        return leadDao.getLeadListbetweendates(userId, strtdate, endate);
    }


    public long insertPSFLocalLead(PSFLeadModel leadModel, String status) {
        return leadDao.insertPSFLocalLead(leadModel, status);
    }

    public List<PSFLeadModel> getOfflinePSFLeadsList(String userId, String newtwork_status) {
        return leadDao.getOfflinePSFLeadsList(userId, newtwork_status);
    }



    public void insertPSFLeadsList(List<PSFLeadModel> list, String userId) {
        leadDao.insertPSFLeadsList(list, userId);
    }



    public void insertPSFBMLeadsList(List<PSFLeadModel> list, String userId) {
        leadDao.insertPSFBMLeadsList(list, userId);
    }



    public List<PSFLeadModel> getPSFLeadsList(String userId) {
        return leadDao.getPSFLeadsList(userId);
    }

    public List<PSFLeadModel> getPSFLeadsListforBM(String userId,List<String> users) {
        return leadDao.getPSFLeadsListforBM(userId,users);
    }



    public List<PSFLeadModel> getPSFLeadsListonLeadStage(String userId, String leadstage) {
        return leadDao.getPSFLeadsListonLeadStage(userId, leadstage);
    }

    public List<PSFLeadModel> getPSFLeadsListonLeadStageBM(String userId, String leadstage,List<String> users) {
        return leadDao.getPSFLeadsListonLeadStageBM(userId, leadstage,users);
    }
    public List<PSFLeadModel> getPSFMyLeadsListBM(String userId,List<String> users) {
        return leadDao.getPSFMyLeadsListBM(userId,users);
    }

    public List<PSFLeadModel> getPSFMyDirectLeadsListBM(String userId,String stage,List<String> users,String leadsource,String maturitymonth) {
        return leadDao.getPSFMyDirectLeadsListBM(userId,stage,users,leadsource,maturitymonth);
    }

    public List<PSFLeadModel> getnonmaturityfilterMyDirectLeadsBM(String userid, String leadstage,List<String> users,String leadsource) {
        return leadDao.getnonmaturityfilterMyDirectLeadsBM(userid, leadstage,users,leadsource);
    }

    public List<PSFLeadModel> getPSFMydirectleadsListSearchnameBM(String userId,String userinput,String stage) {
        return leadDao.getPSFMydirectleadsListSearchnameBM(userId,userinput,stage);
    }

    public List<PSFLeadModel> getPSFcampaignMyDirectLeadsWiseListSearchnameBM(String userId,String userinput,String stage,String leadsource,String maturitymonth) {
        return leadDao.getPSFcampaignMyDirectLeadsWiseListSearchnameBM(userId,userinput,stage,leadsource,maturitymonth);
    }

    public List<PSFLeadModel> getPSFFLMWiseListBM(String userId,String stage,String flsusrid,List<String> users) {
        return leadDao.getPSFFLMWiseListBM(userId,stage,flsusrid,users);
    }

    public List<PSFLeadModel> getPSFFLMWiseListSearchnameBM(String flsusrid,String userinput,String stage) {
        return leadDao.getPSFFLMWiseListSearchnameBM(flsusrid,userinput,stage);
    }

    public List<PSFLeadModel> getPSFMyLeadsListbetweendatesBM(String userId,String strtdate, String endate) {
        return leadDao.getPSFMyLeadsListbetweendatesBM(userId,strtdate,endate);
    }

    public List<PSFLeadModel> getPSFLeadListbetweendates(String userId, String strtdate, String endate, String leadstage) {
        return leadDao.getPSFLeadListbetweendates(userId, strtdate, endate, leadstage);
    }


    public List<PSFLeadModel> getPSFLeadListbetweendatesStageBM(String userId, String strtdate, String endate, String leadstage,List<String> users) {
        return leadDao.getPSFLeadListbetweendatesStageBM(userId, strtdate, endate, leadstage,users);
    }


    public List<PSFLeadModel> getPSFLeadListbetweendates(String userId, String strtdate, String endate) {
        return leadDao.getPSFLeadListbetweendates(userId, strtdate, endate);
    }

    public List<PSFLeadModel> getPSFLeadListbetweendatesBM(String userId, String strtdate, String endate,List<String> users) {
        return leadDao.getPSFLeadListbetweendatesBM(userId, strtdate, endate,users);
    }

    public List<PSFLeadModel> getPSFLeadListforfilter(String userId, String userinput) {
        return leadDao.getPSFLeadListforfilter(userId, userinput);
    }


    public List<PSFLeadModel> getPSFOfflineLeadsList(String userId, String newtwork_status) {
        return leadDao.getPSFOfflineLeadsList(userId, newtwork_status);
    }


    public List<PSFLeadModel> getLeadIdList(String groupId, String userId, String userInput) {
        return leadDao.getLeadIdList(groupId, userId, userInput);
    }




    public void updatePSFLeadOnline(PSFLeadModel leadModel, String userId, String leadId) {
        leadDao.updatePSFLeadOnline(leadModel, userId, leadId);
    }





    public void updatePSFLeadStatus(PSFLeadModel leadModel, String network_status) {
        leadDao.updatePSFLead(leadModel, network_status);
    }

    public void updateFailedPSFLead(PSFLeadModel leadModel, String network_status,String message) {
        leadDao.updateFailedPSFLead(leadModel, network_status,message);
    }

    public Integer getRetryCount( PSFLeadModel leadModel) {
        return leadDao.getRetryCount( leadModel);
    }

    public void updateRetryCount(PSFLeadModel leadModel,String leadId, int count) {
        leadDao.updateRetryCount(leadModel,leadId, count);
    }

    public List<PSFLeadModel> getPSFLeadListSearchbyCampaignWise(String userId, String userinput, String stage) {
        return leadDao.getPSFLeadListSearchbyCampaignWise(userId, userinput, stage);
    }

    public List<PSFLeadModel> getPSFLeadListSearchbyCampaignWiseBM(String userId, String userinput, String stage,List<String> users) {
        return leadDao.getPSFLeadListSearchbyCampaignWiseBM(userId, userinput, stage,users);
    }


    public List<PSFLeadModel> getPSFLeadListforSearch(String userId, String userinput) {
        return leadDao.getPSFLeadListforSearch(userId, userinput);
    }

    public List<PSFLeadModel> getPSFLeadListforSearchBM(String userId, String userinput,List<String> users) {
        return leadDao.getPSFLeadListforSearchBM(userId, userinput,users);
    }



    public List<PSFLeadModel> getPSFLeadListforSearchbyLeadType(String userId) {
        return leadDao.getPSFLeadListforSearchbyLeadType(userId);
    }

    public List<PSFLeadModel> getPSFLeadListforSearchbyLeadTypeBM(String userId,List<String> users) {
        return leadDao.getPSFLeadListforSearchbyLeadTypeBM(userId,users);
    }




    public List<PSFLeadModel> getPSFLeadListforSearchbyLeadType(String userId, String stage) {
        return leadDao.getPSFLeadListforSearchbyLeadType(userId, stage);
    }


    public List<PSFLeadModel> getPSFLeadListforSearchbyLeadTypeBM(String userId, String stage,List<String> users) {
        return leadDao.getPSFLeadListforSearchbyLeadTypeBM(userId, stage,users);
    }


    public List<PSFLeadModel> getPSFLeadListforJointvisit(String userId) {
        return leadDao.getPSFLeadListforJointvisit(userId);
    }


    public List<PSFLeadModel> getPSFLeadListforJointvisitBM(String userId,List<String> users) {
        return leadDao.getPSFLeadListforJointvisitBM(userId,users);
    }

    public List<PSFLeadModel> getPSFLeadListforJointvisit(String userId,String stage) {
        return leadDao.getPSFLeadListforJointvisit(userId,stage);
    }

    public List<PSFLeadModel> getPSFLeadListforJointvisitBM(String userId,String stage,List<String> users) {
        return leadDao.getPSFLeadListforJointvisitBM(userId,stage,users);
    }


    public List<PSFLeadModel> getPSFLeadListByDate(String userId, int day, int month, int year) {
        return leadDao.getPSFLeadListByDate(userId, day, month, year);
    }



    public void insertPSFNotificationsList(List<PSFNotificationsModel> list, String userId) {
        leadDao.insertPSFNotificationsList(list, userId);
    }


    public List<PSFNotificationsModel> getPSFNotificationsList(String userId) {
        return leadDao.getPSFNotificationsList(userId);
    }

    public List<PSFNotificationsModel> getPSFNotificationUserNames(String leadid,String flsuserid) {
        return leadDao.getPSFNotificationUserNames(leadid,flsuserid);
    }
    public List<PSFNotificationsModel> getPSFNotificationUserBasedonLeadid(String leadid) {
        return leadDao.getPSFNotificationUserBasedonLeadid(leadid);
    }

    public void updatePSFNotificationStatus(PSFNotificationsModel notificationModel,String userid, String notifyid) {
        leadDao.updatePSFNotificationStatus(notificationModel,userid, notifyid);
    }


    public List<PSFNotificationsModel> getPSFNotificationCountList(String userId) {
        return leadDao.getPSFNotificationCountList(userId);
    }




    public List<PSFLeadModel> getPSFAppointmentList(String userId,String day, String month, String year) {
        return leadDao.getPSFAppointmentList(userId,day,month,year);
    }


    public List<PSFLeadModel> getPSFAppointmentListBM(String userId,String day, String month, String year,List<String> users) {
        return leadDao.getPSFAppointmentListBM(userId,day,month,year,users);
    }


    public List<PSFLeadModel> getPSFLeadsBasedOnLeadId(String userId , String leadid) {
        return leadDao.getPSFLeadsBasedOnLeadId(userId,leadid);
    }


    public List<PSFLeadModel> getCampaignwiseLeads(String userId, String leadstage) {
        return leadDao.getCampaignwiseLeads(userId, leadstage);
    }

    public List<PSFLeadModel> getCampaignwisewithmorefilterLeads(String userId, String leadstage,String leadsource,String maturitymonth) {
        return leadDao.getCampaignwisewithmorefilterLeads(userId, leadstage,leadsource,maturitymonth);
    }

    public List<PSFLeadModel> getPSFLeadListSearchbymaturitydate(String userId,String userinput, String leadsource,String maturitymonth) {
        return leadDao.getPSFLeadListSearchbymaturitydate(userId,userinput,leadsource,maturitymonth);
    }

    public List<PSFLeadModel> getPSFLeadListSearchbymaturitydate(String userId,String userinput,String leadstage, String leadsource,String maturitymonth) {
        return leadDao.getPSFLeadListSearchbymaturitydate(userId,userinput,leadstage,leadsource,maturitymonth);
    }

    public List<PSFLeadModel> getPSFLeadListSearchbynostagenonmaturity(String userId,String userinput, String leadsource) {
        return leadDao.getPSFLeadListSearchbynostagenonmaturity(userId,userinput,leadsource);
    }


    public List<PSFLeadModel> getPSFLeadListSearchbynonmaturity(String userId,String userinput, String leadstage,String leadsource) {
        return leadDao.getPSFLeadListSearchbynonmaturity(userId,userinput, leadstage,leadsource);
    }

    public List<PSFLeadModel> getnonmaturityfilterLeads(String userId, String leadstage,String leadsource) {
        return leadDao.getnonmaturityfilterLeads(userId, leadstage,leadsource);
    }


    public List<PSFLeadModel> getCampaignwiseLeadsBM(String userId, String leadstage,List<String> users) {
        return leadDao.getCampaignwiseLeadsBM(userId, leadstage,users);
    }

    public List<PSFLeadModel> getCampaignwisewithmorefilterLeadsBM(String userId, String leadstage,List<String> users,String leadsource,String maturitymonth) {
        return leadDao.getCampaignwisewithmorefilterLeadsBM(userId, leadstage,users,leadsource,maturitymonth);
    }

    public List<PSFLeadModel> getCampaignwisewithmorefilterFLMLeadsBM(String userId, String leadstage,List<String> users,String leadsource,String maturitymonth) {
        return leadDao.getCampaignwisewithmorefilterFLMLeadsBM(userId, leadstage,users,leadsource,maturitymonth);
    }


    public List<PSFLeadModel> getPSFLeadListSearchbymaturitydateBM(String userId,String userinput, String leadstage,List<String> users,String leadsource,String maturitymonth) {
        return leadDao.getPSFLeadListSearchbymaturitydateBM(userId,userinput, leadstage,users,leadsource,maturitymonth);
    }

    public List<PSFLeadModel> getPSFcampaignFLMWiseListSearchnameBM(String userId,String userinput,String leadstage,String leadsource,String maturitymonth) {
        return leadDao.getPSFcampaignFLMWiseListSearchnameBM(userId,userinput, leadstage,leadsource,maturitymonth);
    }


    public List<PSFLeadModel> getPSFLeadListSearchbynonmaturityBM(String userId,String userinput, String leadstage,List<String> users,String leadsource) {
        return leadDao.getPSFLeadListSearchbynonmaturityBM(userId,userinput, leadstage,users,leadsource);
    }


    public List<PSFLeadModel> getPSFLeadListSearchbymaturitydateBM(String userId,String userinput,List<String> users,String leadsource,String maturitymonth) {
        return leadDao.getPSFLeadListSearchbymaturitydateBM(userId,userinput,users,leadsource,maturitymonth);
    }


    public List<PSFLeadModel> getPSFLeadListSearchbynostagenonmaturityBM(String userId,String userinput,List<String> users,String leadsource) {
        return leadDao.getPSFLeadListSearchbynostagenonmaturityBM(userId,userinput,users,leadsource);
    }



    public List<PSFLeadModel> getnonmaturityfilterLeadsBM(String userId, String leadstage,List<String> users,String leadsource) {
        return leadDao.getnonmaturityfilterLeadsBM(userId, leadstage,users,leadsource);
    }

    public List<PSFLeadModel> getnonmaturityfilterFLMLeadsBM(String flsId, String leadstage,List<String> users,String leadsource) {
        return leadDao.getnonmaturityfilterFLMLeadsBM(flsId, leadstage,users,leadsource);
    }

    public List<PSFLeadModel> getCampaignwiseLeads(String userId) {
        return leadDao.getCampaignwiseLeads(userId);
    }

    public List<PSFLeadModel> getCampaignwisewithmorefilterLeads(String userId,String leadsource,String maturitymonth) {
        return leadDao.getCampaignwisewithmorefilterLeads(userId,leadsource,maturitymonth);
    }

    public List<PSFLeadModel> getnonmaturityfilterLeads(String userId,String leadsource) {
        return leadDao.getnonmaturityfilterLeads(userId,leadsource);
    }

    public List<PSFLeadModel> getCampaignwiseLeadsBM(String userId,List<String> users) {
        return leadDao.getCampaignwiseLeadsBM(userId,users);
    }

    public List<PSFLeadModel> getCampaignwisewithmorefilterLeadsBM(String userId,List<String> users,String leadsource,String maturitymonth) {
        return leadDao.getCampaignwisewithmorefilterLeadsBM(userId,users,leadsource,maturitymonth);
    }

    public List<PSFLeadModel> getnonmaturityfilterLeadsBM(String userId,List<String> users,String leadsource) {
        return leadDao.getnonmaturityfilterLeadsBM(userId,users,leadsource);
    }

    public List<PSFLeadModel> getDispositionwiseLeads(String userId, String leadstage) {
        return leadDao.getDispositionwiseLeads(userId, leadstage);
    }


    public List<PSFLeadModel> getDispositionwiseLeadsBM(String userId, String leadstage,List<String> users) {
        return leadDao.getDispositionwiseLeadsBM(userId, leadstage,users);
    }

    public List<PSFLeadModel> getDispositionwiseLeads(String userId) {
        return leadDao.getDispositionwiseLeads(userId);
    }

    public List<PSFLeadModel> getDispositionwiseLeadsBM(String userId,List<String> users) {
        return leadDao.getDispositionwiseLeadsBM(userId,users);
    }

    public List<PSFLeadModel> getPSFdistinctNamesList(String userId,String day, String month, String year,List<String> users) {
        return leadDao.getPSFdistinctNamesList(userId,day,month,year,users);
    }

    public List<PSFLeadModel> getPSFLeadListforSearchbySelfLeadType(String userId, String stage) {
        return leadDao.getPSFLeadListforSearchbySelfLeadType(userId, stage);
    }


    public List<PSFLeadModel> getPSFLeadListforSearchbySelfLeadTypeBM(String userId, String stage,List<String> users) {
        return leadDao.getPSFLeadListforSearchbySelfLeadTypeBM(userId, stage,users);
    }

    public List<PSFLeadModel> getPSFLeadListforSearchSelfLeadType(String userId) {
        return leadDao.getPSFLeadListforSearchSelfLeadType(userId);
    }

    public List<PSFLeadModel> getPSFLeadListforSearchSelfLeadTypeBM(String userId,List<String> users) {
        return leadDao.getPSFLeadListforSearchSelfLeadTypeBM(userId,users);
    }
}



