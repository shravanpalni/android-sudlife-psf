package com.outwork.sudlife.psf.branches.adapter;


import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.ui.models.Userprofile;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;

/**
 * Created by shravanch on 14-07-2018.
 */

public class TeamHierarchySOAdapter extends RecyclerView.Adapter<TeamHierarchySOAdapter.TaskViewHolder> {

    private final LayoutInflater mInflater;
    private Context context;
    private ArrayList<Userprofile> userprofilesArrayList;


    public static class TaskViewHolder extends RecyclerView.ViewHolder {

        public TextView team_hie_so_item;



        public TaskViewHolder(View v) {
            super(v);

            team_hie_so_item = (TextView) v.findViewById(R.id.team_hie_so_item);

            Utils.setTypefaces(IvokoApplication.robotoMediumTypeface, team_hie_so_item);

        }
    }

    public TeamHierarchySOAdapter(Context context, ArrayList<Userprofile> userprofilesArrayList) {
        this.context = context;
        this.userprofilesArrayList = userprofilesArrayList;

        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.team_hie_so_list, parent, false);
        return new TaskViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, final int position) {

        //final TeamMemberPlannerModel plannerModel = (TeamMemberPlannerModel) this.plannerModelList.get(position);

        final Userprofile userprofile = (Userprofile) this.userprofilesArrayList.get(position);

        holder.team_hie_so_item.setText(userprofile.getFirstname() + " " + userprofile.getLastname());





    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {


        return userprofilesArrayList.size();
    }
}








































/*extends RecyclerView.Adapter<TeamHierarchySOAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Userprofile> userprofilesArrayList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.team_hie_so_item);

        }
    }


    public TeamHierarchySOAdapter(Context context, ArrayList<Userprofile> userprofilesArrayList) {
        this.context = context;
        this.userprofilesArrayList = userprofilesArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.team_hie_so_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //Userprofile movie = userprofilesArrayList.get(position);



        holder.title.setText(userprofilesArrayList.get(position).getFirstname() + " " + userprofilesArrayList.get(position).getLastname());


    }

    @Override
    public int getItemCount() {
        return userprofilesArrayList.size();
    }
}
*/