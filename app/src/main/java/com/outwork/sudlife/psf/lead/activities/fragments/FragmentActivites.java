package com.outwork.sudlife.psf.lead.activities.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.lead.model.LeadModel;
import com.outwork.sudlife.psf.notifications.GlobalData;
import com.outwork.sudlife.psf.opportunity.adapter.ActivitiesAdapter;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.adapter.PlannerAdapter;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bvlbh on 4/17/2018.
 */

public class FragmentActivites extends Fragment {

    private RecyclerView rcvActivities;
    private TextView tvNodata;
    private List<PlannerModel> plannerModelList = new ArrayList<>();
    private String toolbardate;
    private int day, monthno, year;
    private String userid;
    private LeadModel leadModel = new LeadModel();
    private PlannerAdapter plannerAdapter;
    private PlannerModel plannerModel = new PlannerModel();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lead_activties, null);
        if (getArguments() != null) {
            leadModel = new Gson().fromJson(getArguments().getString("leadobj"), LeadModel.class);
        }
        rcvActivities = (RecyclerView) view.findViewById(R.id.rcv_leads);
        tvNodata = (TextView) view.findViewById(R.id.tv_nodata);
        toolbardate = TimeUtils.getCurrentDate("yyyy-MM-dd");
        day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
        monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
        year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
        userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");


        if (Utils.isNotNullAndNotEmpty(leadModel.getLeadid())) {
            plannerModelList = PlannerMgr.getInstance(getActivity()).getPlannerListByOpportunityID(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), leadModel.getLeadid());
            if (plannerModelList.size() > 0) {
                plannerAdapter = new PlannerAdapter(getActivity(), plannerModelList,"lead");
                rcvActivities.setAdapter(plannerAdapter);
                rcvActivities.setLayoutManager(new LinearLayoutManager(getActivity()));
            } else {
                tvNodata.setVisibility(View.VISIBLE);
            }
        } else if (leadModel.getId() > 0) {
            plannerModelList = PlannerMgr.getInstance(getActivity()).getPlannerListBylocalLeadID(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), leadModel.getId());
            if (plannerModelList.size() > 0) {
                plannerAdapter = new PlannerAdapter(getActivity(), plannerModelList,"lead");
                rcvActivities.setAdapter(plannerAdapter);
                rcvActivities.setLayoutManager(new LinearLayoutManager(getActivity()));
            } else {
                tvNodata.setVisibility(View.VISIBLE);
            }
        }
        plannerModelList = PlannerMgr.getInstance(getActivity()).getPlannerListByLead(GlobalData.getInstance().getLeadModel().getLeadid());
        if (plannerModelList.size() != 0) {
            ArrayList<PlannerModel> plannerModels = new ArrayList<>();
            for (int i = 0; i < plannerModelList.size(); i++) {
                if (!plannerModelList.get(i).getLeadid().equalsIgnoreCase("")) {
                    plannerModels.add(plannerModelList.get(i));
                }
            }
            tvNodata.setVisibility(View.GONE);
            rcvActivities.setVisibility(View.VISIBLE);
            ActivitiesAdapter plannerAdapter = new ActivitiesAdapter(getActivity(), plannerModels);
            rcvActivities.setAdapter(plannerAdapter);
            rcvActivities.setLayoutManager(new LinearLayoutManager(getActivity()));
        } else {
            tvNodata.setVisibility(View.VISIBLE);
            rcvActivities.setVisibility(View.GONE);
        }
        return view;
    }
}
