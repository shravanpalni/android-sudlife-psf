package com.outwork.sudlife.psf.lead.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.services.CustomerService;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.activities.fragments.FilterFragment;
import com.outwork.sudlife.psf.lead.adapters.FlsAdapter;
import com.outwork.sudlife.psf.lead.adapters.PSFLeadsAdapter;
import com.outwork.sudlife.psf.lead.model.PSFFlsModel;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.localbase.StaticDataDto;
import com.outwork.sudlife.psf.localbase.StaticDataMgr;
import com.outwork.sudlife.psf.monthpicker.YearMonthPickerDialog;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.adapter.UserTeamNamesAdapter;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.activities.NotificationsListActivity;
import com.outwork.sudlife.psf.ui.models.UserTeams;
import com.outwork.sudlife.psf.ui.models.Userprofile;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Created by shravanch on 17-12-2019.
 */

public class PSFLeadListActivity extends BaseActivity implements FilterFragment.OnFragmentInteractionListener {

    private RecyclerView rcvLeads;
    private RelativeLayout searchLayout;
    private BroadcastReceiver mBroadcastReceiver;
    private EditText searchText;
    private TabLayout tabLayout;
    private String searchby = "", stage = "";
    private int position;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ImageView filter;
    private EditText startdate, enddate;
    private Button submit;
    private String sdate, edate;
    private LocalBroadcastManager mgr;
    private PSFLeadsAdapter leadsAdapter;
    private TextView fetcchtime, toolbar_title, tv_nodata;
    private Boolean isfiltered = false;
    private boolean isAlreadyLoaded = false;
    private List<PSFLeadModel> leadModels = new ArrayList<>();
    private RadioButton my_leads_wise, fls_wise, asc_date, des_date, filter_campaignwise, more_filter_campaignwise, more_filter_campaignwise_flm, more_filter_campaignwise_myleads, filter_dispositionwise, filter_identification, filter_jointwork, filter_self;
    private FloatingActionButton fb_lead_add;
    List<UserTeams> userTeamsList = new ArrayList<>();
    private ArrayList<Userprofile> teamMembersHeirarchyArrayList = new ArrayList();
    private List<String> teammemberuseridlist;
    private List<String> teammemberuseridforShowinglist;
    private boolean isFirstTime = false;
    TextView textCartItemCount;
    int mCartItemCount;
    UserTeams userteamObj;
    private String selectedTeamId = null;
    private ArrayList<String> stringArrayList;
    private String maturitydatemonth = "";
    String selectedleadsource = "";
    private Dialog teamdialog;
    private ArrayList<Userprofile> flsList = new ArrayList();
    private ArrayList<Userprofile> showflsList = new ArrayList();
    private String flsuserid = null;
    private String branchId = null;
    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                startdate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                sdate = TimeUtils.convertDatetoUnix(visitdt);
                startdate.setText(visitdt);
                startdate.setEnabled(true);
                startdate.setError(null);
            }
            if (customSelector == 1) {
                enddate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                edate = TimeUtils.convertDatetoUnix(visitdt);
                enddate.setText(visitdt);
                enddate.setEnabled(true);
                enddate.setError(null);
            }
        }

        @Override
        public void onDateTimeCancel() {
            startdate.setEnabled(true);
            enddate.setEnabled(true);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_list_of_leads);
        mgr = LocalBroadcastManager.getInstance(this);
       /* if (isNetworkAvailable()) {
            getServerLeads();
        }*/
       //int userlevel = SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0);


        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
            position = 8;
        } else {
            position = 6;
        }

       /* if (SharedPreferenceManager.getInstance().getString(Constants.LEADS_LOADED, "").equals("notloaded")) {
            if (isNetworkAvailable()) {
                LeadIntentService.insertLeadstoServer(PSFLeadListActivity.this);
            }
        }*/
        if (isNetworkAvailable()) {
            if (LeadMgr.getInstance(PSFLeadListActivity.this).getOfflinePSFLeadsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline").size() > 0) {

                LeadIntentService.syncPSFLeadstoServer(PSFLeadListActivity.this);
            }
        }


        rcvLeads = (RecyclerView) findViewById(R.id.rcv_leads);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.lead_swipe_refresh_layout);
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setEnabled(false);
        //mSwipeRefreshLayout.setClickable(false);
        //mSwipeRefreshLayout.setOnTouchListener(null);

        // mSwipeRefreshLayout.setColorSchemeResources(R.color.topbarcolor, R.color.daycolor);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);


        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
            tabLayout.addTab(tabLayout.newTab().setText("My Direct Leads"));
            tabLayout.addTab(tabLayout.newTab().setText("Unallocated"));
            tabLayout.addTab(tabLayout.newTab().setText("Open"));
            tabLayout.addTab(tabLayout.newTab().setText("Pre-Fixed\n Appointment"));
            tabLayout.addTab(tabLayout.newTab().setText("Proposition\n presented"));
            tabLayout.addTab(tabLayout.newTab().setText("Follow-Up"));
            tabLayout.addTab(tabLayout.newTab().setText("Converted"));
            tabLayout.addTab(tabLayout.newTab().setText("Not Interested"));
            tabLayout.addTab(tabLayout.newTab().setText("All"));
        } else {
            tabLayout.addTab(tabLayout.newTab().setText("Open"));
            tabLayout.addTab(tabLayout.newTab().setText("Pre-Fixed\n Appointment"));
            tabLayout.addTab(tabLayout.newTab().setText("Proposition\n presented"));
            tabLayout.addTab(tabLayout.newTab().setText("Follow-Up"));
            tabLayout.addTab(tabLayout.newTab().setText("Converted"));
            tabLayout.addTab(tabLayout.newTab().setText("Not Interested"));
            tabLayout.addTab(tabLayout.newTab().setText("All"));
        }

        //if (isNetworkAvailable()) {
            tabLayout.getTabAt(position).select();
            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                //getNewLeadsData(); // testing
                if (isNetworkAvailable()) {
                    new GetNewLeadsData().execute();
                }else {
                    AlertDialog.Builder alertDialog=new AlertDialog.Builder(PSFLeadListActivity.this);
                    alertDialog.setTitle("Alert!");
                    alertDialog.setMessage("No Internet!");
                    alertDialog.setPositiveButton("OK",new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                                finish();

                        }

                    });



                    alertDialog.show();
                }




            } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                if (isNetworkAvailable()) {
                    refreshData();
                }else {
                    showToast("No internet!");
                }

            }

       // }
        LeadIntentService.insertPSFNotifications(PSFLeadListActivity.this);

        //tabLayout.addTab(tabLayout.newTab().setText("Unallocated"));

        fb_lead_add = (FloatingActionButton) findViewById(R.id.fb_lead_add);

        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
            fb_lead_add.setVisibility(View.VISIBLE);
        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
            fb_lead_add.setVisibility(View.VISIBLE);
        }

        fb_lead_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(PSFLeadListActivity.this, ListCustomerActivity.class);
                Intent intent = new Intent(PSFLeadListActivity.this, PSFNewCreateLeadActivity.class);
                //intent.putExtra("branchid",branchId);
                startActivity(intent);
            }
        });
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        searchText = (EditText) findViewById(R.id.searchText);
        filter = (ImageView) findViewById(R.id.filter);
        fetcchtime = (TextView) findViewById(R.id.fetchtime);
        leadModels.clear();
        tabLayout.getTabAt(position).select();

        tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        tv_nodata.setVisibility(View.GONE);
        int userlevel = SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0);
    /*    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

            if (isNetworkAvailable()) {

                userTeamsList = PlannerMgr.getInstance(PSFLeadListActivity.this).getUserTeamList(userid);
                Log.i("shravan", "userTeamsList calendar size = = = " + userTeamsList.size());

                if (userTeamsList.size() > 1) {

                    showDialog();

                } else {

                    for (UserTeams us : userTeamsList) {


                        if (isNetworkAvailable()) {

                            selectedTeamId = us.getTeamid().toString();
                            getTeamHierarchyMembers(us.getTeamid().toString());

                        } else {

                            showSimpleAlert("", "Oops...No Internet connection.");
                        }


                    }


                }


            } else {

                showSimpleAlert("", "Oops...No Internet connection.");
            }


        } else*/
        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
            final List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsList(userid);
            initToolBar();

            if (leadModels.size() > 0) {
                isFirstTime = false;

            } else {
                isFirstTime = true;
            }
            showProgressDialog("Loading . . .");
            showList(leadModels, searchby, true);
            Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, (TextView) findViewById(R.id.tv_nodata), toolbar_title);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText));

        }

        setListener();
        mBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                leadModels.clear();
                tabLayout.getTabAt(position).select();
                if (stage.length() > 0) {
                    // isFirstTime = true;
                    List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, stage);
                    if (leadModels.size() > 0) {
                        isFirstTime = true;
                    }
                    showList(leadModels, searchby, true);
                } else {
                    // isFirstTime = true;
                    List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsList(userid);
                    if (leadModels.size() > 0) {
                        isFirstTime = true;
                    }
                    showList(leadModels, searchby, true);
                }
            }
        };


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i("shravan", "onResume");
        if (isNetworkAvailable()) {
            //getServerPSFLeads();
            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                if (isAlreadyLoaded){
                    Log.i("shravan", "onResume called------");
                    new GetRefreshLeadsforBM().execute();
                }

            }else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                refreshData();

            }

        }else {
        tabLayout.getTabAt(position).select();
        if (stage.length() > 0) {
            if (isfiltered) {
                if (Utils.isNotNullAndNotEmpty(stage)) {

                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                        if (stage.equalsIgnoreCase("My Direct Leads")) {

                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                        } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesStageBM(userid,
                                    sdate, edate, stage, teammemberuseridlist);
                        } else {
                            // showToast("Please choose a User.");
                        }


                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                sdate, edate, stage);
                    }


                } else {

                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                        if (stage.equalsIgnoreCase("My Direct Leads")) {

                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                        } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesBM(userid,
                                    sdate, edate, teammemberuseridlist);
                        } else {
                            // showToast("Please choose a User.");
                        }


                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                sdate, edate);
                    }


                }
            } else {
                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                    if (stage.equalsIgnoreCase("My Direct Leads")) {

                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                    } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStageBM(userid, stage, teammemberuseridlist);
                    } else {
                        // showToast("Please choose a User.");
                    }


                } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, stage);
                }


            }
        } else {
            if (isfiltered) {
                if (Utils.isNotNullAndNotEmpty(stage)) {


                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {


                        if (stage.equalsIgnoreCase("My Direct Leads")) {

                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                        } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesStageBM(userid,
                                    sdate, edate, stage, teammemberuseridlist);
                        } else {
                            // showToast("Please choose a User.");
                        }


                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                sdate, edate, stage);
                    }


                } else {


                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                        if (stage.equalsIgnoreCase("My Direct Leads")) {

                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                        } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesBM(userid,
                                    sdate, edate, teammemberuseridlist);
                        } else {
                            //  showToast("Please choose a User.");
                        }


                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                sdate, edate);
                    }

                }
            } else {


                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                    if (stage.equalsIgnoreCase("My Direct Leads")) {

                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                    } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListforBM(userid, teammemberuseridlist);
                    } else {
                        // showToast("Please choose a User.");
                    }


                } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsList(userid);
                }

            }
        }
        showList(leadModels, searchby, false);
        initToolBar();
        }


    }


    @Override
    public void onPause() {
        super.onPause();
        Log.i("shravan", "onPause");

    }


    @Override
    protected void onRestart() {
        super.onRestart();

        Log.i("shravan", "onRestart");

        if (isNetworkAvailable()) {
            // getServerPSFLeads(); // testing
        }
       /* leadModels.clear();
        tabLayout.getTabAt(position).select();
        if (stage.length() > 0) {
            if (isfiltered) {
                if (Utils.isNotNullAndNotEmpty(stage)) {

                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                        if (teammemberuseridlist != null && teammemberuseridlist.size()>0){
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesStageBM(userid,
                                    sdate, edate, stage, teammemberuseridlist);
                        }else {
                            showToast("Please choose a User.");
                        }



                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                sdate, edate, stage);
                    }


                } else {

                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                        if (teammemberuseridlist != null && teammemberuseridlist.size()>0){
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesBM(userid,
                                    sdate, edate, teammemberuseridlist);
                        }else {
                            showToast("Please choose a User.");
                        }




                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                sdate, edate);
                    }


                }
            } else {
                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                    if (teammemberuseridlist != null && teammemberuseridlist.size()>0){
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStageBM(userid, stage, teammemberuseridlist);
                    }else {
                        showToast("Please choose a User.");
                    }


                } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, stage);
                }


            }
        } else {
            if (isfiltered) {
                if (Utils.isNotNullAndNotEmpty(stage)) {


                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                        if (teammemberuseridlist != null && teammemberuseridlist.size()>0){
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesStageBM(userid,
                                    sdate, edate, stage, teammemberuseridlist);
                        }else {
                            showToast("Please choose a User.");
                        }



                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                sdate, edate, stage);
                    }


                } else {


                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                        if (teammemberuseridlist != null && teammemberuseridlist.size()>0){
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesBM(userid,
                                    sdate, edate, teammemberuseridlist);
                        }else {
                            showToast("Please choose a User.");
                        }



                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                sdate, edate);
                    }

                }
            } else {


                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                    if (teammemberuseridlist != null && teammemberuseridlist.size()>0){

                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListforBM(userid, teammemberuseridlist);
                    }else {
                        showToast("Please choose a User.");
                    }


                } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsList(userid);
                }

            }
        }
        showList(leadModels, searchby, true);*/
       /* if (stage.length() > 0) {
            if (isfiltered) {
                if (Utils.isNotNullAndNotEmpty(stage)) {
                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                            sdate, edate, stage);
                } else {
                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                            sdate, edate);
                }
            } else {
                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, stage);
            }
        } else {
            if (isfiltered) {
                if (Utils.isNotNullAndNotEmpty(stage)) {
                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                            sdate, edate, stage);
                } else {
                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                            sdate, edate);
                }
            } else {
                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsList(userid);
            }
        }*/


    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("shravan", "onStart");
        mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("lead_broadcast"));
        // mgr.registerReceiver(mBroadcastReceiver, new IntentFilter("psfnotificationsupdate_broadcast"));
        if (isNetworkAvailable()) {
            updateNotification();
        } else {
            List<PSFNotificationsModel> finalList = new ArrayList<>();
            List<PSFNotificationsModel> psfNotificationsModelList = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFNotificationsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));

            if (psfNotificationsModelList.size() > 0) {

                for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                    if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                        finalList.add(notificationsModel);
                    }

                }

                mCartItemCount = finalList.size();
                setupBadge();
            } else {
                mCartItemCount = 0;
                setupBadge();
            }

        }


    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("shravan", "onStop");
        mgr.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter, menu);
//        if (isfiltered) {
//            MenuItem menuItem = menu.findItem(R.id.filter);
//            MenuItemCompat.getActionView(menuItem);
//            menuItem.setIcon(R.drawable.ic_action_filter_filled);
//        } else {
//            MenuItem menuItem = menu.findItem(R.id.filter);
//            MenuItemCompat.getActionView(menuItem);
//            menuItem.setIcon(R.drawable.ic_action_filter);
//        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.filter) {
//            FilterFragment filterFragment = new FilterFragment();
//            FragmentManager fm = getSupportFragmentManager();
//            FragmentTransaction ft = fm.beginTransaction();
//            ft.add(android.R.id.content, filterFragment);
//            ft.addToBackStack(null);
//            ft.commit();
            dateFilter();

            //showToast("To be enabled");
//            if (isfiltered) {
//                item.setIcon(R.drawable.ic_action_filter_filled);
//            } else {
//                item.setIcon(R.drawable.ic_action_filter);
//            }
        }
        if (item.getItemId() == R.id.refresh) {
            if (isNetworkAvailable()) {
                refreshData();

            } else {
                showToast("Please Check Internet Connection....");
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void refreshData() {

        showProgressDialog("Refreshing Data..Please Wait....");
        LeadIntentService.syncPSFLeadstoServer(PSFLeadListActivity.this);
        LeadIntentService.insertPSFNotifications(PSFLeadListActivity.this);
        final List<PSFLeadModel> offlineLeadsList = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFOfflineLeadsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");
        if (offlineLeadsList.size() == 0) {
            leadModels.clear();
            getServerPSFLeads();
        } else {
            dismissProgressDialog();
        }
    }

    private void setListener() {
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showpopup();
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mSwipeRefreshLayout.setRefreshing(false);
                /*leadModels.clear();
                tabLayout.getTabAt(position).select();
                if (stage.length() > 0) {
                    if (isfiltered) {
                        if (Utils.isNotNullAndNotEmpty(stage)) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                    sdate, edate, stage);
                        } else {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                    sdate, edate);
                        }
                    } else {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, stage);
                    }
                } else {
                    if (isfiltered) {
                        if (Utils.isNotNullAndNotEmpty(stage)) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                    sdate, edate, stage);
                        } else {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                    sdate, edate);
                        }
                    } else {
                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsList(userid);
                    }
                }
                showList(leadModels, searchby, true);
                mSwipeRefreshLayout.setRefreshing(false);*/
            }
        });


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {     ///////////sh1
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                    if (isNetworkAvailable()) {

                        if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {

                            if (tab.getText().toString().equalsIgnoreCase("My Direct Leads")) {

                                leadModels.clear();
                                stage = "My Direct Leads";
                                position = 0;
                                isfiltered = false;
                                List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else if (tab.getText().toString().equalsIgnoreCase("Unallocated")) {

                                leadModels.clear();
                                stage = "Unallocated";
                                position = 1;
                                isfiltered = false;
                                List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStageBM(userid, "Unallocated", teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else if (tab.getText().toString().equalsIgnoreCase("Open")) {

                                leadModels.clear();
                                stage = "Open";
                                position = 2;
                                isfiltered = false;
                                List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStageBM(userid, "Open", teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else if (tab.getText().toString().equalsIgnoreCase("Pre-Fixed\n Appointment")) {

                                leadModels.clear();
                                stage = "Pre-Fixed Appointment";
                                position = 3;
                                isfiltered = false;
                                List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStageBM(userid, "Pre-Fixed Appointment", teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else if (tab.getText().toString().equalsIgnoreCase("Proposition\n presented")) {

                                leadModels.clear();
                                stage = "Proposition presented";
                                position = 4;
                                isfiltered = false;
                                List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStageBM(userid, "Proposition presented", teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else if (tab.getText().toString().equalsIgnoreCase("Follow-Up")) {
                                leadModels.clear();
                                stage = "Follow-Up";
                                position = 5;
                                isfiltered = false;
                                List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStageBM(userid, "Follow-Up", teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else if (tab.getText().toString().equalsIgnoreCase("Converted")) {
                                leadModels.clear();
                                stage = "Converted";
                                position = 6;
                                isfiltered = false;
                                List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStageBM(userid, "Converted", teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else if (tab.getText().toString().equalsIgnoreCase("Not Interested")) {
                                leadModels.clear();
                                stage = "Not Interested";
                                position = 7;
                                isfiltered = false;
                                List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStageBM(userid, "Not Interested", teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else {

                                leadModels.clear();
                                stage = "";
                                position = 8;
                                isfiltered = false;
                                List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListforBM(userid, teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            }


                        } else {

                            //showToast("Please choose a User.");
                        }


                    } else {
                        showToast("Oops.... No Internet!");
                    }


                } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {


                    if (tab.getText().toString().equalsIgnoreCase("Open")) {

                        leadModels.clear();
                        stage = "Open";
                        position = 0;
                        isfiltered = false;
                        List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, "Open");
                        showList(leadModels, searchby, true);
                    } else if (tab.getText().toString().equalsIgnoreCase("Pre-Fixed\n Appointment")) {

                        leadModels.clear();
                        stage = "Pre-Fixed Appointment";
                        position = 1;
                        isfiltered = false;
                        List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, "Pre-Fixed Appointment");
                        showList(leadModels, searchby, true);
                    } else if (tab.getText().toString().equalsIgnoreCase("Proposition\n presented")) {

                        leadModels.clear();
                        stage = "Proposition presented";
                        position = 2;
                        isfiltered = false;
                        List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, "Proposition presented");
                        showList(leadModels, searchby, true);
                    } else if (tab.getText().toString().equalsIgnoreCase("Follow-Up")) {
                        leadModels.clear();
                        stage = "Follow-Up";
                        position = 3;
                        isfiltered = false;
                        List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, "Follow-Up");
                        showList(leadModels, searchby, true);
                    } else if (tab.getText().toString().equalsIgnoreCase("Converted")) {
                        leadModels.clear();
                        stage = "Converted";
                        position = 4;
                        isfiltered = false;
                        List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, "Converted");
                        showList(leadModels, searchby, true);
                    } else if (tab.getText().toString().equalsIgnoreCase("Not Interested")) {
                        leadModels.clear();
                        stage = "Not Interested";
                        position = 5;
                        isfiltered = false;
                        List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, "Not Interested");
                        showList(leadModels, searchby, true);
                    } else {

                        leadModels.clear();
                        stage = "";
                        position = 6;
                        isfiltered = false;
                        List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsList(userid);
                        showList(leadModels, searchby, true);
                    }


                }


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void initToolBar() {

        List<PSFLeadModel> leadscount = new ArrayList<>();

        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                leadscount = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListforBM(userid, teammemberuseridlist);
            } else {
                // showToast("Please choose a User.");
            }


        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
            leadscount = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsList(userid);

        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Leads (" + leadscount.size() + ")");
        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        textCartItemCount = (TextView) findViewById(R.id.notification_badge);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(PSFLeadListActivity.this, NotificationsListActivity.class);
                startActivity(in);

                //showToast("Notification Clicked!!!");
            }
        });

        setupBadge();
    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void setUpSearchLayout() {
        searchLayout.setVisibility(View.VISIBLE);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (leadsAdapter != null && s != null) {
                    leadsAdapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void showList(List<PSFLeadModel> leadModels, String type, boolean reverse) {

        //leadModels.clear();
//        tv_nodata.setVisibility(View.GONE);
        if (leadModels.size() > 0) {

            if (reverse) {
                Collections.reverse(leadModels);
            }
            //
            tv_nodata.setVisibility(View.GONE);
            rcvLeads.setVisibility(View.VISIBLE);
            leadsAdapter = new PSFLeadsAdapter(leadModels, PSFLeadListActivity.this, type, stage, teammemberuseridlist, selectedTeamId, selectedleadsource, maturitydatemonth, flsuserid);
            rcvLeads.setAdapter(leadsAdapter);
            rcvLeads.setLayoutManager(new LinearLayoutManager(PSFLeadListActivity.this));
            if (leadModels.size() > 10) {
                setUpSearchLayout();
            }
        } else {

            //refreshData();

            tv_nodata.setVisibility(View.VISIBLE);
            tv_nodata.setText("No Data");
            /*if (isFirstTime) {
                tv_nodata.setText("Loading ...");

            } else {
                tv_nodata.setText("No Data");
            }*/


            rcvLeads.setVisibility(View.GONE);
        }

        dismissProgressDialog();


        fetcchtime.setText("Last Refreshed Time : " + TimeUtils.getFormattedDatefromUnix(SharedPreferenceManager.getInstance().getString(Constants.LEAD_LAST_FETCH_TIME, ""), "dd/MM/yyyy hh:mm a"));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    public void showpopup() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PSFLeadListActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.leads_filter_layout, null);

        alertDialog.setView(convertView);
        RadioGroup type = (RadioGroup) convertView.findViewById(R.id.type);
        type.clearCheck();
        searchby = "";
        fls_wise = (RadioButton) convertView.findViewById(R.id.fls_wise);
        my_leads_wise = (RadioButton) convertView.findViewById(R.id.my_leads_wise);
        asc_date = (RadioButton) convertView.findViewById(R.id.asc_date);
        des_date = (RadioButton) convertView.findViewById(R.id.des_date);
        filter_campaignwise = (RadioButton) convertView.findViewById(R.id.filter_campaignwise);
        more_filter_campaignwise = (RadioButton) convertView.findViewById(R.id.more_filter_campaignwise);
        more_filter_campaignwise_flm = (RadioButton) convertView.findViewById(R.id.more_filter_campaignwise_flm);
        more_filter_campaignwise_myleads = (RadioButton) convertView.findViewById(R.id.more_filter_campaignwise_myleads);
        filter_dispositionwise = (RadioButton) convertView.findViewById(R.id.filter_dispositionwise);
        filter_self = (RadioButton) convertView.findViewById(R.id.filter_self);
        filter_identification = (RadioButton) convertView.findViewById(R.id.filter_identification);
        filter_jointwork = (RadioButton) convertView.findViewById(R.id.filter_jointwork);

        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
            fls_wise.setVisibility(View.VISIBLE);
            my_leads_wise.setVisibility(View.VISIBLE);
            more_filter_campaignwise_flm.setVisibility(View.VISIBLE);
            more_filter_campaignwise_myleads.setVisibility(View.VISIBLE);
            more_filter_campaignwise.setVisibility(View.GONE);

        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
            more_filter_campaignwise.setVisibility(View.VISIBLE);
            fls_wise.setVisibility(View.GONE);
            my_leads_wise.setVisibility(View.GONE);
            more_filter_campaignwise_myleads.setVisibility(View.GONE);
            more_filter_campaignwise_myleads.setVisibility(View.GONE);

        }

        if (Utils.isNotNullAndNotEmpty(searchby)) {
            if (searchby.equalsIgnoreCase("flswise")) {
                fls_wise.setChecked(true);
            } else if (searchby.equalsIgnoreCase("myleadswise")) {
                my_leads_wise.setChecked(true);
            } else if (searchby.equalsIgnoreCase("ascdate")) {
                asc_date.setChecked(true);
            } else if (searchby.equalsIgnoreCase("desdate")) {
                des_date.setChecked(true);
            } else if (searchby.equalsIgnoreCase("campaign")) {
                filter_campaignwise.setChecked(true);
            } else if (searchby.equalsIgnoreCase("morefiltercampaign")) {
                more_filter_campaignwise.setChecked(true);
            } else if (searchby.equalsIgnoreCase("morefiltercampaignflm")) {
                more_filter_campaignwise.setChecked(true);
            } else if (searchby.equalsIgnoreCase("morefiltercampaignmydirectleads")) {
                more_filter_campaignwise.setChecked(true);
            } else if (searchby.equalsIgnoreCase("dispositionwise")) {
                filter_dispositionwise.setChecked(true);
            } else if (searchby.equalsIgnoreCase("self")) {
                filter_self.setChecked(true);
            } else if (searchby.equalsIgnoreCase("leadidentification")) {
                filter_identification.setChecked(true);
            } else if (searchby.equalsIgnoreCase("jointvisit")) {
                filter_jointwork.setChecked(true);
            }
        }
        Utils.setTypefaces(IvokoApplication.robotoTypeface, asc_date, des_date, filter_campaignwise, more_filter_campaignwise, filter_dispositionwise, filter_self, filter_identification, filter_jointwork);

        final AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.setTitle("Select Filter Type for search.....");
        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                isfiltered = false;
                searchText.setText("");
                if (checkedId == R.id.fls_wise) {
                    searchby = "flswise";
                    searchText.setHint("FLS Wise");
                    //searchText.setFocusable(true);
                    //searchText.setFocusableInTouchMode(true);
                    alert.cancel();
                } else if (checkedId == R.id.my_leads_wise) {
                    searchby = "myleadswise";
                    searchText.setHint("My Direct Leads");
                    //searchText.setFocusable(true);
                    //searchText.setFocusableInTouchMode(true);
                    alert.cancel();
                } else if (checkedId == R.id.asc_date) {
                    searchby = "ascdate";
                    searchText.setHint("Search with Date");
                    searchText.setFocusable(false);
                    searchText.setFocusableInTouchMode(false);
                    alert.cancel();
                } else if (checkedId == R.id.des_date) {
                    searchby = "desdate";
                    searchText.setHint("Search with Date");
                    searchText.setFocusable(false);
                    searchText.setFocusableInTouchMode(false);
                    alert.cancel();
                } else if (checkedId == R.id.filter_campaignwise) {
                    searchby = "campaign";
                    searchText.setHint("Search with Campaign Wise");
                    searchText.setFocusable(true);
                    searchText.setFocusableInTouchMode(true);
                    alert.cancel();
                } else if (checkedId == R.id.more_filter_campaignwise) {
                    searchby = "morefiltercampaign";
                    searchText.setHint("Search with Campaign Wise with more filter options");
                    searchText.setFocusable(true);
                    searchText.setFocusableInTouchMode(true);
                    alert.dismiss();

                } else if (checkedId == R.id.more_filter_campaignwise_flm) {
                    searchby = "morefiltercampaignflm";
                    searchText.setHint("Search with Campaign Wise with more filter options FLM");
                    searchText.setFocusable(true);
                    searchText.setFocusableInTouchMode(true);
                    alert.dismiss();

                } else if (checkedId == R.id.more_filter_campaignwise_myleads) {
                    searchby = "morefiltercampaignmydirectleads";
                    searchText.setHint("Search with Campaign Wise with more filter options MY Direct Leads");
                    searchText.setFocusable(true);
                    searchText.setFocusableInTouchMode(true);
                    alert.dismiss();

                } else if (checkedId == R.id.filter_dispositionwise) {
                    searchby = "dispositionwise";
                    searchText.setHint("Search with Disposition Wise");
                    searchText.setFocusable(true);
                    searchText.setFocusableInTouchMode(true);
                    alert.cancel();
                } else if (checkedId == R.id.filter_self) {
                    searchby = "self";
                    searchText.setHint("Search with Reference/Self");
                    searchText.setFocusable(false);
                    searchText.setFocusableInTouchMode(false);
                    alert.cancel();
                } else if (checkedId == R.id.filter_identification) {
                    searchby = "leadidentification";
                    searchText.setHint("Search with Identification of leads created by JARVIS and by FLS");
                    searchText.setFocusable(false);
                    searchText.setFocusableInTouchMode(false);
                    alert.cancel();
                } else if (checkedId == R.id.filter_jointwork) {
                    searchby = "jointvisit";
                    searchText.setHint("Search with Joint Field Work with Supervisor");
                    searchText.setFocusable(false);
                    searchText.setFocusableInTouchMode(false);
                    alert.cancel();
                }
                leadModels.clear();
                List<PSFLeadModel> leadModels;


                if (Utils.isNotNullAndNotEmpty(stage)) {


                    if (searchby.equalsIgnoreCase("flswise")) {


                        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
                        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
                        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, selectedTeamId);
                        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
                            @Override
                            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                                if (response.isSuccessful()) {
                                    // dismissProgressDialog();
                                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                                        Log.i("shravan", "response = = =" + response.body().toString());

                                        flsList = new ArrayList<>();
                                        showflsList = new ArrayList<>();
                                        ArrayList<Userprofile> rawflsList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                                        }.getType());
                                        if (Utils.isNotNullAndNotEmpty(rawflsList)) {

                                            for (Userprofile up : rawflsList) {

                                                flsList.add(up);

                                                if (!(up.getUserid().equalsIgnoreCase(SharedPreferenceManager.getInstance().getString(Constants.USERID, "")))) {

                                                    showflsList.add(up);
                                                }

                                            }


                                            final Dialog dialog = new Dialog(PSFLeadListActivity.this);

                                            //dialog.setTitle("Select FLS");

                                            View view = (PSFLeadListActivity.this).getLayoutInflater().inflate(R.layout.userteam_dialog, null);

                                            ListView lv = (ListView) view.findViewById(R.id.memberList);
                                            FlsAdapter clad = new FlsAdapter(PSFLeadListActivity.this, showflsList);
                                            lv.setAdapter(clad);

                                            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    // TODO Auto-generated method stub

                                                    Userprofile dto = (Userprofile) parent.getItemAtPosition(position);
                                                    flsuserid = dto.getUserid();
                                                    Log.i("shravan", "PSFLeadsAdapter  -- - flsuserid===" + flsuserid);

                                                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                                        if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                                            List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFFLMWiseListBM(userid, stage, flsuserid, teammemberuseridlist);
                                                            showList(leadModels, searchby, false);
                                                        } else {
                                                            // showToast("Please choose a User.");
                                                        }


                                                    }
                                                    dialog.cancel();


                                                }
                                            });

                                            dialog.setContentView(view);

                                            dialog.show();


                                        } else {
                                            Toast.makeText(PSFLeadListActivity.this, "FLS list empty!", Toast.LENGTH_SHORT).show();

                                        }
                                    } else {
                                        Toast.makeText(PSFLeadListActivity.this, "Data missing!", Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(PSFLeadListActivity.this, "Server error!", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<RestResponse> call, Throwable t) {

                            }
                        });


                    } else if (searchby.equalsIgnoreCase("myleadswise")) {


                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                //leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListforSearchbyLeadTypeBM(userid, stage, teammemberuseridlist);
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        }


                    } else if (searchby.equalsIgnoreCase("leadidentification")) {


                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListforSearchbyLeadTypeBM(userid, stage, teammemberuseridlist);
                                showList(leadModels, searchby, true);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListforSearchbyLeadType(userid, stage);
                            showList(leadModels, searchby, true);
                        }


                    } else if (searchby.equalsIgnoreCase("self")) {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListforSearchbySelfLeadTypeBM(userid, stage, teammemberuseridlist);
                                showList(leadModels, searchby, true);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListforSearchbySelfLeadType(userid, stage);
                            showList(leadModels, searchby, true);
                        }


                    } else if (searchby.equalsIgnoreCase("jointvisit")) {
                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListforJointvisitBM(userid, stage, teammemberuseridlist);
                                showList(leadModels, searchby, true);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListforJointvisit(userid, stage);
                            showList(leadModels, searchby, true);
                        }


                    } else if (searchby.equalsIgnoreCase("campaign")) {


                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwiseLeadsBM(userid, stage, teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwiseLeads(userid, stage);
                            showList(leadModels, searchby, false);
                        }


                    } else if (searchby.equalsIgnoreCase("morefiltercampaign")) {

                        List<StaticDataDto> staticList = new StaticDataMgr(PSFLeadListActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "updateleadsource");

                        if (staticList.size() > 0) {

                            stringArrayList = new ArrayList<>();

                            for (StaticDataDto value : staticList) {

                                stringArrayList.add(value.getName());

                            }

                            bottomSheetDateWiseDialog(stringArrayList, "Select Campaign Wise");

                        }


                    } else if (searchby.equalsIgnoreCase("morefiltercampaignflm")) {

                        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
                        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
                        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, selectedTeamId);
                        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
                            @Override
                            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                                if (response.isSuccessful()) {
                                    // dismissProgressDialog();
                                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                                        Log.i("shravan", "response = = =" + response.body().toString());

                                        flsList = new ArrayList<>();
                                        showflsList = new ArrayList<>();
                                        ArrayList<Userprofile> rawflsList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                                        }.getType());
                                        if (Utils.isNotNullAndNotEmpty(rawflsList)) {

                                            for (Userprofile up : rawflsList) {

                                                flsList.add(up);

                                                if (!(up.getUserid().equalsIgnoreCase(SharedPreferenceManager.getInstance().getString(Constants.USERID, "")))) {

                                                    showflsList.add(up);
                                                }

                                            }


                                            final Dialog dialog = new Dialog(PSFLeadListActivity.this);

                                            //dialog.setTitle("Select FLS");

                                            View view = (PSFLeadListActivity.this).getLayoutInflater().inflate(R.layout.userteam_dialog, null);

                                            ListView lv = (ListView) view.findViewById(R.id.memberList);
                                            FlsAdapter clad = new FlsAdapter(PSFLeadListActivity.this, showflsList);
                                            lv.setAdapter(clad);

                                            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    // TODO Auto-generated method stub

                                                    Userprofile dto = (Userprofile) parent.getItemAtPosition(position);
                                                    flsuserid = dto.getUserid();
                                                    Log.i("shravan", "PSFLeadsAdapter  -- - flsuserid===" + flsuserid);

                                                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                                       /* if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                                            List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFFLMWiseListBM(userid, stage,flsuserid, teammemberuseridlist);
                                                            showList(leadModels, searchby, false);
                                                        } else {
                                                            // showToast("Please choose a User.");
                                                        }*/

                                                        List<StaticDataDto> staticList = new StaticDataMgr(PSFLeadListActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "updateleadsource");

                                                        if (staticList.size() > 0) {

                                                            stringArrayList = new ArrayList<>();

                                                            for (StaticDataDto value : staticList) {

                                                                stringArrayList.add(value.getName());

                                                            }

                                                            bottomSheetFLMWiseDialog(stringArrayList, "Select Campaign FLM  Wise", flsuserid);

                                                        }


                                                    }

                                                    dialog.cancel();


                                                }
                                            });

                                            dialog.setContentView(view);

                                            dialog.show();


                                        } else {
                                            Toast.makeText(PSFLeadListActivity.this, "FLS list empty!", Toast.LENGTH_SHORT).show();

                                        }
                                    } else {
                                        Toast.makeText(PSFLeadListActivity.this, "Data missing!", Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(PSFLeadListActivity.this, "Server error!", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<RestResponse> call, Throwable t) {

                            }
                        });


                    }else if (searchby.equalsIgnoreCase("morefiltercampaignmydirectleads")) {

                        List<StaticDataDto> staticList = new StaticDataMgr(PSFLeadListActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "updateleadsource");

                        if (staticList.size() > 0) {

                            stringArrayList = new ArrayList<>();

                            for (StaticDataDto value : staticList) {

                                stringArrayList.add(value.getName());

                            }

                            bottomSheetMyDirectLeadsWiseDialog(stringArrayList, "Select Campaign My Direct Leads Wise");

                        }


                    } else if (searchby.equalsIgnoreCase("dispositionwise")) {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getDispositionwiseLeadsBM(userid, stage, teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getDispositionwiseLeads(userid, stage);
                            showList(leadModels, searchby, false);

                        }


                    } else {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStageBM(userid, stage, teammemberuseridlist);
                                showList(leadModels, searchby, true);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, stage);
                            showList(leadModels, searchby, true);

                        }


                    }


                    //showList(leadModels, searchby, true);
                } else {

                    if (searchby.equalsIgnoreCase("flswise")) {


                        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
                        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
                        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, selectedTeamId);
                        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
                            @Override
                            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                                if (response.isSuccessful()) {
                                    // dismissProgressDialog();
                                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                                        Log.i("shravan", "response = = =" + response.body().toString());

                                        flsList = new ArrayList<>();
                                        showflsList = new ArrayList<>();
                                        ArrayList<Userprofile> rawflsList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                                        }.getType());
                                        if (Utils.isNotNullAndNotEmpty(rawflsList)) {

                                            for (Userprofile up : rawflsList) {

                                                flsList.add(up);

                                                if (!(up.getUserid().equalsIgnoreCase(SharedPreferenceManager.getInstance().getString(Constants.USERID, "")))) {

                                                    showflsList.add(up);
                                                }

                                            }


                                            final Dialog dialog = new Dialog(PSFLeadListActivity.this);

                                            //dialog.setTitle("Select FLS");

                                            View view = (PSFLeadListActivity.this).getLayoutInflater().inflate(R.layout.userteam_dialog, null);

                                            ListView lv = (ListView) view.findViewById(R.id.memberList);
                                            FlsAdapter clad = new FlsAdapter(PSFLeadListActivity.this, showflsList);
                                            lv.setAdapter(clad);

                                            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    // TODO Auto-generated method stub

                                                    Userprofile dto = (Userprofile) parent.getItemAtPosition(position);
                                                    flsuserid = dto.getUserid();
                                                    Log.i("shravan", "PSFLeadsAdapter  -- - flsuserid===" + flsuserid);

                                                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                                        if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                                            List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFFLMWiseListBM(userid, stage, flsuserid, teammemberuseridlist);
                                                            showList(leadModels, searchby, false);
                                                        } else {
                                                            // showToast("Please choose a User.");
                                                        }


                                                    }
                                                    dialog.cancel();


                                                }
                                            });

                                            dialog.setContentView(view);

                                            dialog.show();


                                        } else {
                                            Toast.makeText(PSFLeadListActivity.this, "FLS list empty!", Toast.LENGTH_SHORT).show();

                                        }
                                    } else {
                                        Toast.makeText(PSFLeadListActivity.this, "Data missing!", Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(PSFLeadListActivity.this, "Server error!", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<RestResponse> call, Throwable t) {

                            }
                        });


                    } else if (searchby.equalsIgnoreCase("myleadswise")) {


                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                //leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListforSearchbyLeadTypeBM(userid, stage, teammemberuseridlist);
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        }


                    } else if (searchby.equalsIgnoreCase("leadidentification")) {


                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).
                                        getPSFLeadListforSearchbyLeadTypeBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), teammemberuseridlist);
                                showList(leadModels, searchby, true);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).
                                    getPSFLeadListforSearchbyLeadType(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                            showList(leadModels, searchby, true);

                        }


                    } else if (searchby.equalsIgnoreCase("self")) {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {


                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListforSearchSelfLeadTypeBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), teammemberuseridlist);
                                showList(leadModels, searchby, true);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListforSearchSelfLeadType(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                            showList(leadModels, searchby, true);

                        }


                    } else if (searchby.equalsIgnoreCase("jointvisit")) {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).
                                        getPSFLeadListforJointvisitBM(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), teammemberuseridlist);
                                showList(leadModels, searchby, true);
                            } else {
                                //showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).
                                    getPSFLeadListforJointvisit(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                            showList(leadModels, searchby, true);
                        }


                    } else if (searchby.equalsIgnoreCase("campaign")) {


                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwiseLeadsBM(userid, teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwiseLeads(userid);
                            showList(leadModels, searchby, false);

                        }


                    } else if (searchby.equalsIgnoreCase("morefiltercampaign")) {

                        List<StaticDataDto> staticList = new StaticDataMgr(PSFLeadListActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "updateleadsource");

                        if (staticList.size() > 0) {

                            stringArrayList = new ArrayList<>();

                            for (StaticDataDto value : staticList) {

                                stringArrayList.add(value.getName());

                            }

                            bottomSheetDateWiseDialog(stringArrayList, "Select Campaign Wise");

                        }


                    } else if (searchby.equalsIgnoreCase("morefiltercampaignflm")) {

                        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
                        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
                        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, selectedTeamId);
                        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
                            @Override
                            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                                if (response.isSuccessful()) {
                                    // dismissProgressDialog();
                                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                                        Log.i("shravan", "response = = =" + response.body().toString());

                                        flsList = new ArrayList<>();
                                        showflsList = new ArrayList<>();
                                        ArrayList<Userprofile> rawflsList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                                        }.getType());
                                        if (Utils.isNotNullAndNotEmpty(rawflsList)) {

                                            for (Userprofile up : rawflsList) {

                                                flsList.add(up);

                                                if (!(up.getUserid().equalsIgnoreCase(SharedPreferenceManager.getInstance().getString(Constants.USERID, "")))) {

                                                    showflsList.add(up);
                                                }

                                            }


                                            final Dialog dialog = new Dialog(PSFLeadListActivity.this);

                                            //dialog.setTitle("Select FLS");

                                            View view = (PSFLeadListActivity.this).getLayoutInflater().inflate(R.layout.userteam_dialog, null);

                                            ListView lv = (ListView) view.findViewById(R.id.memberList);
                                            FlsAdapter clad = new FlsAdapter(PSFLeadListActivity.this, showflsList);
                                            lv.setAdapter(clad);

                                            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    // TODO Auto-generated method stub

                                                    Userprofile dto = (Userprofile) parent.getItemAtPosition(position);
                                                    flsuserid = dto.getUserid();
                                                    Log.i("shravan", "PSFLeadsAdapter  -- - flsuserid===" + flsuserid);

                                                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                                       /* if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                                            List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFFLMWiseListBM(userid, stage,flsuserid, teammemberuseridlist);
                                                            showList(leadModels, searchby, false);
                                                        } else {
                                                            // showToast("Please choose a User.");
                                                        }*/

                                                        List<StaticDataDto> staticList = new StaticDataMgr(PSFLeadListActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "updateleadsource");

                                                        if (staticList.size() > 0) {

                                                            stringArrayList = new ArrayList<>();

                                                            for (StaticDataDto value : staticList) {

                                                                stringArrayList.add(value.getName());

                                                            }

                                                            bottomSheetFLMWiseDialog(stringArrayList, "Select Campaign FLM Wise", flsuserid);

                                                        }


                                                    }

                                                    dialog.cancel();


                                                }
                                            });

                                            dialog.setContentView(view);

                                            dialog.show();


                                        } else {
                                            Toast.makeText(PSFLeadListActivity.this, "FLS list empty!", Toast.LENGTH_SHORT).show();

                                        }
                                    } else {
                                        Toast.makeText(PSFLeadListActivity.this, "Data missing!", Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(PSFLeadListActivity.this, "Server error!", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<RestResponse> call, Throwable t) {

                            }
                        });


                    }else if (searchby.equalsIgnoreCase("morefiltercampaignmydirectleads")) {

                        List<StaticDataDto> staticList = new StaticDataMgr(PSFLeadListActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "updateleadsource");

                        if (staticList.size() > 0) {

                            stringArrayList = new ArrayList<>();

                            for (StaticDataDto value : staticList) {

                                stringArrayList.add(value.getName());

                            }

                            bottomSheetMyDirectLeadsWiseDialog(stringArrayList, "Select Campaign My Direct Leads Wise");

                        }


                    } else if (searchby.equalsIgnoreCase("dispositionwise")) {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getDispositionwiseLeadsBM(userid, teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else {
                                //showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getDispositionwiseLeads(userid);
                            showList(leadModels, searchby, false);
                        }


                    } else if (searchby.equalsIgnoreCase("ascdate")) {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListforBM(userid, teammemberuseridlist);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsList(userid);
                            showList(leadModels, searchby, false);

                        }


                    } else {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListforBM(userid, teammemberuseridlist);
                                showList(leadModels, searchby, true);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsList(userid);
                            showList(leadModels, searchby, true);
                        }


                    }


                }
            }
        });


        alert.show();
    }

    public void dateFilter() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PSFLeadListActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.layout_dates_filter, null);

        alertDialog.setView(convertView);
        startdate = (EditText) convertView.findViewById(R.id.strtdate);
        enddate = (EditText) convertView.findViewById(R.id.enddate);
        submit = (Button) convertView.findViewById(R.id.submit);
        final AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.setTitle("Date Filter");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -60);
        startdate.setText(TimeUtils.getFormattedDate(calendar.getTime()));
        sdate = TimeUtils.convertDatetoUnix(TimeUtils.getFormattedDate(calendar.getTime()));
        enddate.setText(TimeUtils.getCurrentDate());
        edate = TimeUtils.convertDatetoUnix(TimeUtils.getCurrentDate());

        startdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startdate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -60);
                Date mindate = calendar.getTime();
                calendar.add(Calendar.DAY_OF_YEAR, -7);
                Date initdate = calendar.getTime();
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initdate)
                        .setMinDate(mindate)
                        .setMaxDate(new Date())
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enddate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -60);
                Date mindate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(mindate)
                        .setMaxDate(new Date())
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNotNullAndNotEmpty(startdate.getText().toString())) {
                    startdate.setError("Date required");
                    startdate.requestFocus();
                } else if (!Utils.isNotNullAndNotEmpty(enddate.getText().toString())) {
                    enddate.setError("Date required");
                    enddate.requestFocus();
                } else {
                    if (Integer.parseInt(sdate) > Integer.parseInt(edate)) {
                        showToast("Startdate should not be greater than Endnddate");
                    } else {
                        leadModels.clear();
                        // isfiltered = true;
                        if (Utils.isNotNullAndNotEmpty(stage)) {
                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                                if (stage.equalsIgnoreCase("My Direct Leads")) {

                                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListbetweendatesBM(userid, sdate, edate);

                                } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesStageBM(userid, sdate, edate, stage, teammemberuseridlist);
                                } else {
                                    // showToast("Please choose a User.");
                                }


                            } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid, sdate, edate, stage);
                            }


                        } else {

                            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                if (stage.equalsIgnoreCase("My Direct Leads")) {

                                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListbetweendatesBM(userid, sdate, edate);

                                } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesBM(userid, sdate, edate, teammemberuseridlist);
                                } else {
                                    // showToast("Please choose a User.");
                                }


                            } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {

                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid, sdate, edate);
                            }


                        }
                        showList(leadModels, searchby, false);
                        alert.cancel();
                    }
                }
            }
        });
        alert.show();
    }

    @Override
    public void onClickItem(String result) {
        leadModels.clear();
        if (result.matches("[a-zA-Z ]+")) {
            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListforfilter(userid,
                    result);
        } else {
            String[] dates = result.split(" ");
            if (Utils.isNotNullAndNotEmpty(stage)) {
                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                        dates[0], dates[1], stage);
            } else {
                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                        dates[0], dates[1]);
            }
        }
        ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.GONE);
//        if (leadModels.size() > 0) {
//            rcvLeads.setVisibility(View.VISIBLE);
//            leadsAdapter = new LeadsAdapter(leadModels, PSFLeadListActivity.this);
//            rcvLeads.setAdapter(leadsAdapter);
//            rcvLeads.setLayoutManager(new LinearLayoutManager(PSFLeadListActivity.this));
//        } else {
//            ((TextView) findViewById(R.id.tv_nodata)).setVisibility(View.VISIBLE);
//            rcvLeads.setVisibility(View.GONE);
//        }
    }

    public void getServerPSFLeads() {
        Log.i("shravan", "serverleads entry........");
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getLeads = client.getServerPSFLeads(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), SharedPreferenceManager.getInstance().getString(Constants.USERID, "")
                , SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "");
        final String newFetchTime = TimeUtils.getCurrentUnixTimeStamp();
        getLeads.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                dismissProgressDialog();
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFLeadModel>>() {
                        }.getType();
                        List<PSFLeadModel> leadModelList = new Gson().fromJson(response.body().getData(), listType);
                        if (leadModelList.size() > 0) {
                            LeadMgr.getInstance(PSFLeadListActivity.this).insertPSFLeadsList(leadModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        // SharedPreferenceManager.getInstance().putString(Constants.LEAD_LAST_FETCH_TIME, newFetchTime);
                        //SharedPreferenceManager.getInstance().putString(Constants.LEADS_LOADED, "loaded");
                        tabLayout.getTabAt(position).select();
                        if (stage.length() > 0) {
                            if (isfiltered) {
                                if (Utils.isNotNullAndNotEmpty(stage)) {

                                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                        if (stage.equalsIgnoreCase("My Direct Leads")) {

                                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                                        } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesStageBM(userid,
                                                    sdate, edate, stage, teammemberuseridlist);
                                        } else {
                                            // showToast("Please choose a User.");
                                        }


                                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                                sdate, edate, stage);
                                    }


                                } else {

                                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                        if (stage.equalsIgnoreCase("My Direct Leads")) {

                                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                                        } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesBM(userid,
                                                    sdate, edate, teammemberuseridlist);
                                        } else {
                                            //  showToast("Please choose a User.");
                                        }


                                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                                sdate, edate);
                                    }


                                }
                            } else {
                                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                                    if (stage.equalsIgnoreCase("My Direct Leads")) {

                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                                    } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStageBM(userid, stage, teammemberuseridlist);
                                    } else {
                                        // showToast("Please choose a User.");
                                    }


                                } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListonLeadStage(userid, stage);
                                }


                            }
                        } else {
                            if (isfiltered) {
                                if (Utils.isNotNullAndNotEmpty(stage)) {


                                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                        if (stage.equalsIgnoreCase("My Direct Leads")) {

                                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                                        } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesStageBM(userid,
                                                    sdate, edate, stage, teammemberuseridlist);
                                        } else {
                                            //showToast("Please choose a User.");
                                        }


                                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                                sdate, edate, stage);
                                    }


                                } else {


                                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                        if (stage.equalsIgnoreCase("My Direct Leads")) {

                                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                                        } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendatesBM(userid,
                                                    sdate, edate, teammemberuseridlist);
                                        } else {
                                            //showToast("Please choose a User.");
                                        }


                                    } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadListbetweendates(userid,
                                                sdate, edate);
                                    }

                                }
                            } else {


                                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                    if (stage.equalsIgnoreCase("My Direct Leads")) {

                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyLeadsListBM(userid, teammemberuseridlist);

                                    } else if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListforBM(userid, teammemberuseridlist);
                                    } else {
                                        // showToast("Please choose a User.");
                                    }


                                } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsList(userid);
                                }

                            }
                        }
                        showList(leadModels, searchby, false);
                        initToolBar();
                        Log.i("shravan", "serverleads exit........");
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
                SharedPreferenceManager.getInstance().putString(Constants.LEADS_LOADED, "notloaded");
            }
        });
    }


/*    public void getNewLeadsData() {
        Log.i("shravan", "getNewLeadsData entry........");
        showProgressDialog("Please wait");
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getLeads = client.getServerPSFLeads(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), SharedPreferenceManager.getInstance().getString(Constants.USERID, "")
                , SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "");
        final String newFetchTime = TimeUtils.getCurrentUnixTimeStamp();
        getLeads.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {

                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFLeadModel>>() {
                        }.getType();
                        List<PSFLeadModel> leadModelList = new Gson().fromJson(response.body().getData(), listType);
                        if (leadModelList.size() > 0) {
                            LeadMgr.getInstance(PSFLeadListActivity.this).insertPSFLeadsList(leadModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        dismissProgressDialog();
                        Log.i("shravan", "getNewLeadsData exit........");
                        //SharedPreferenceManager.getInstance().putString(Constants.LEAD_LAST_FETCH_TIME, newFetchTime);
                        //SharedPreferenceManager.getInstance().putString(Constants.LEADS_LOADED, "loaded");

                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
                //SharedPreferenceManager.getInstance().putString(Constants.LEADS_LOADED, "notloaded");
            }
        });
    }*/


    private void updateNotification() {
        //showProgressDialog("Refreshing data . . .");
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getProposalCodes = client.getPsfNotifications(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        getProposalCodes.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFNotificationsModel>>() {
                        }.getType();
                        List<PSFNotificationsModel> psfNotificationsModelList = new Gson().fromJson(response.body().getData(), listType);
                        List<PSFNotificationsModel> finalList = new ArrayList<>();
                        if (psfNotificationsModelList.size() > 0) {
                            for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                                if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                                    finalList.add(notificationsModel);
                                }

                            }

                            mCartItemCount = finalList.size();
                            setupBadge();
                            //dismissProgressDialog();

                        } else {
                            mCartItemCount = 0;
                            setupBadge();
                            //dismissProgressDialog();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                //dismissProgressDialog();
            }
        });


    }

    private class GetRefreshLeadsforBM extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(PSFLeadListActivity.this);
            progressDialog.setMessage("Loading Leads....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            //showProgressDialog("Loading . . .");


        }


        @Override
        protected String doInBackground(String... params) {


            FormsService client = RestService.createServicev1(FormsService.class);
            Call<RestResponse> getLeads = client.getServerPSFLeads(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), SharedPreferenceManager.getInstance().getString(Constants.USERID, "")
                    , SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "");


            try {
                Response<RestResponse> restResponse = getLeads.execute();
                int statusCode = restResponse.code();
                if (statusCode == 200) {
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFLeadModel>>() {
                        }.getType();
                        List<PSFLeadModel> leadModelList = new Gson().fromJson(restResponse.body().getData(), listType);
                        if (leadModelList.size() > 0) {
                            LeadMgr.getInstance(PSFLeadListActivity.this).insertPSFLeadsList(leadModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        //dismissProgressDialog();
                        Log.i("shravan", "getNewLeadsData exit........"); /*else {
                            showToast("No Data available");
                        }*/
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }


            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListforBM(userid, teammemberuseridlist);

            showList(leadModels, searchby, false);
            initToolBar();
            Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, (TextView) findViewById(R.id.tv_nodata), toolbar_title);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText));
            dismissProgressDialog();
            position = 8;
            tabLayout.getTabAt(position).select();

        }


        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private class GetNewLeadsData extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(PSFLeadListActivity.this);
            progressDialog.setMessage("Loading Leads....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            //showProgressDialog("Loading . . .");


        }


        @Override
        protected String doInBackground(String... params) {


            FormsService client = RestService.createServicev1(FormsService.class);
            Call<RestResponse> getLeads = client.getServerPSFLeads(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), SharedPreferenceManager.getInstance().getString(Constants.USERID, "")
                    , SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "");


            try {
                Response<RestResponse> restResponse = getLeads.execute();
                int statusCode = restResponse.code();
                if (statusCode == 200) {
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFLeadModel>>() {
                        }.getType();
                        List<PSFLeadModel> leadModelList = new Gson().fromJson(restResponse.body().getData(), listType);
                        if (leadModelList.size() > 0) {
                            LeadMgr.getInstance(PSFLeadListActivity.this).insertPSFLeadsList(leadModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        //dismissProgressDialog();
                        Log.i("shravan", "getNewLeadsData exit........"); /*else {
                            showToast("No Data available");
                        }*/
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }


            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                if (isNetworkAvailable()) {

                    userTeamsList = PlannerMgr.getInstance(PSFLeadListActivity.this).getUserTeamList(userid);
                    Log.i("shravan", "userTeamsList calendar size = = = " + userTeamsList.size());

                    if (userTeamsList.size() > 1) {

                        showDialog();

                    } else {

                        for (UserTeams us : userTeamsList) {


                            if (isNetworkAvailable()) {

                                selectedTeamId = us.getTeamid().toString();
                                getTeamHierarchyMembers(us.getTeamid().toString());

                            } else {

                                showSimpleAlert("", "Oops...No Internet connection.");
                            }


                        }


                    }


                } else {

                    showSimpleAlert("", "Oops...No Internet connection.");
                }


            }
        }


        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    private class GetTeamHierarchyMembers extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {


            CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
            Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, selectedTeamId);

            try {
                Response<RestResponse> restResponse = responseCallGetTargets.execute();
                int statusCode = restResponse.code();
                if (statusCode == 200) {
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                        teamMembersHeirarchyArrayList = new Gson().fromJson(restResponse.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                        }.getType());
                        if (Utils.isNotNullAndNotEmpty(teamMembersHeirarchyArrayList)) {

                            teammemberuseridlist = new ArrayList<>();
                            teammemberuseridforShowinglist = new ArrayList<>();

                            //teammemberuseridlist.clear();

                            for (Userprofile up : teamMembersHeirarchyArrayList) {

                                teammemberuseridlist.add(up.getUserid());


                                if (!(up.getUserid().equalsIgnoreCase(SharedPreferenceManager.getInstance().getString(Constants.USERID, "")))) {

                                    teammemberuseridforShowinglist.add(up.getUserid());
                                }


                            }


                            /*List<PSFLeadModel>*/


                        } else {
                            showToast("No Data available");
                        }
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }


            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

            isAlreadyLoaded = true;

            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListforBM(userid, teammemberuseridlist);

            showList(leadModels, searchby, false);
            initToolBar();
            Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, (TextView) findViewById(R.id.tv_nodata), toolbar_title);
            Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText));
            dismissProgressDialog();
        }

        @Override
        protected void onPreExecute() {

            showProgressDialog("Loading . . .");


        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    private void showDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(PSFLeadListActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        LayoutInflater factory = LayoutInflater.from(this);
        final View dialogView = factory.inflate(R.layout.team_selection_dialog, null);
        //View dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.team_selection_dialog, viewGroup, false);
        ListView lv = (ListView) dialogView.findViewById(R.id.memberList);
        lv.setClickable(true);
        UserTeamNamesAdapter clad = new UserTeamNamesAdapter(PSFLeadListActivity.this, userTeamsList, groupId, userid);
        lv.setAdapter(clad);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                Log.i("shravan", "clicked dialog1");
                userteamObj = (UserTeams) parent.getItemAtPosition(position);
                if (isNetworkAvailable()) {
                    showProgressDialog("Loading . . .");
                    selectedTeamId = userteamObj.getTeamid().toString();
                    //getTeamHierarchyMembers(userteamObj.getTeamid().toString());
                    new GetTeamHierarchyMembers().execute();
                    alertDialog.dismiss();
                    //teamdialog = null;
                    Log.i("shravan", "clicked dialog2");
                    //new GetTeamHierarchyMembers().execute();


                } else {

                    showSimpleAlert("", "Oops...No Internet connection.");
                }


            }
        });

    }


    private void getTeamHierarchyMembers(String teamid) {
        showProgressDialog("Loading, please wait....");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, teamid);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        teamMembersHeirarchyArrayList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                        }.getType());
                        if (Utils.isNotNullAndNotEmpty(teamMembersHeirarchyArrayList)) {

                            teammemberuseridlist = new ArrayList<>();
                            teammemberuseridforShowinglist = new ArrayList<>();

                            //teammemberuseridlist.clear();

                            for (Userprofile up : teamMembersHeirarchyArrayList) {

                                teammemberuseridlist.add(up.getUserid());


                                if (!(up.getUserid().equalsIgnoreCase(SharedPreferenceManager.getInstance().getString(Constants.USERID, "")))) {

                                    teammemberuseridforShowinglist.add(up.getUserid());
                                }


                            }

                            isAlreadyLoaded = true;
                            List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFLeadsListforBM(userid, teammemberuseridlist);


                            if (leadModels.size() > 0) {
                                isFirstTime = false;

                            } else {
                                isFirstTime = true;
                            }
                            // showProgressDialog("Loading . . .");////////
                            showList(leadModels, searchby, false);
                            initToolBar();
                            Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, (TextView) findViewById(R.id.tv_nodata), toolbar_title);
                            Utils.setTypefaces(IvokoApplication.robotoTypeface, (EditText) findViewById(R.id.searchText));


                        } else {
                            showToast("No Data available");

                        }
                    }
                } else {
                    dismissProgressDialog();
                    showToast("No Data available");

                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
                showToast("Server Error!");

            }
        });
    }


    public void bottomSheetDateWiseDialog(ArrayList<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        ListView lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(PSFLeadListActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();


        ArrayAdapter adapter = new ArrayAdapter(PSFLeadListActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                selectedleadsource = parent.getItemAtPosition(position).toString();

                if (selectedleadsource.equalsIgnoreCase("Maturity")) {
                    // rackMonthPicker.show();
                    //createDialogWithoutDateField().show();
                    Calendar calendar = Calendar.getInstance();
                    //calendar.set(2010, 01, 01);

                    YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(PSFLeadListActivity.this, calendar, new YearMonthPickerDialog.OnDateSetListener() {
                        @Override
                        public void onYearMonthSet(int year, int month) {
                            Calendar oldcalendar = Calendar.getInstance();
                            oldcalendar.set(Calendar.YEAR, year);
                            oldcalendar.set(Calendar.MONTH, month);

                            SimpleDateFormat olddateFormat = new SimpleDateFormat("MMMM yyyy");

                            //yearMonth.setText(dateFormat.format(calendar.getTime()));
                            Log.i("shravan", olddateFormat.format(oldcalendar.getTime()));

                            SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM yyyy");
                            Date date = null;
                            try {
                                date = (dateFormat).parse(olddateFormat.format(oldcalendar.getTime()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            int noOfDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                            maturitydatemonth = simpleDateFormat.format(date);
                            Log.i("shravan", maturitydatemonth);
                            //System.out.println(properFormattedDate+"-"+String.valueOf(noOfDays));
                            if (Utils.isNotNullAndNotEmpty(stage)) {

                               /* if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                    if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwisewithmorefilterLeadsBM(userid, stage, teammemberuseridlist, "Maturity", maturitydatemonth);
                                        showList(leadModels, searchby, false);
                                    } else {
                                        // showToast("Please choose a User.");
                                    }


                                } else*/
                                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwisewithmorefilterLeads(userid, stage, "Maturity", maturitydatemonth);
                                    showList(leadModels, searchby, false);
                                }


                            } else {

                              /*  if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                    if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwisewithmorefilterLeadsBM(userid, teammemberuseridlist, "Maturity", maturitydatemonth);
                                        showList(leadModels, searchby, false);
                                    } else {
                                        // showToast("Please choose a User.");
                                    }


                                } else*/
                                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                                    leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwisewithmorefilterLeads(userid, "Maturity", maturitydatemonth);
                                    showList(leadModels, searchby, false);

                                }


                            }
                        }
                    });

                    yearMonthPickerDialog.show();
                    Log.i("shravan", "Maturity selected");


                } else {

                    Log.i("shravan", "Maturity not selected");


                    if (Utils.isNotNullAndNotEmpty(stage)) {

                     /*   if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getnonmaturityfilterLeadsBM(userid, stage, teammemberuseridlist, selectedleadsource);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else*/
                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getnonmaturityfilterLeads(userid, stage, selectedleadsource);
                            showList(leadModels, searchby, false);
                        }


                    } else {

                       /* if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getnonmaturityfilterLeadsBM(userid, teammemberuseridlist, selectedleadsource);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        } else*/
                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                            leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getnonmaturityfilterLeads(userid, selectedleadsource);
                            showList(leadModels, searchby, false);

                        }


                    }


                }


                if (isNetworkAvailable()) {
                    //getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), selectedtab.getText().toString(), selected, "", "","","","");
                } else {
                    showToast("No Internet");
                }


                bottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    public void bottomSheetFLMWiseDialog(ArrayList<String> strings, String title, String flsid) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        ListView lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(PSFLeadListActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();


        ArrayAdapter adapter = new ArrayAdapter(PSFLeadListActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                selectedleadsource = parent.getItemAtPosition(position).toString();

                if (selectedleadsource.equalsIgnoreCase("Maturity")) {
                    // rackMonthPicker.show();
                    //createDialogWithoutDateField().show();
                    Calendar calendar = Calendar.getInstance();
                    //calendar.set(2010, 01, 01);

                    YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(PSFLeadListActivity.this, calendar, new YearMonthPickerDialog.OnDateSetListener() {
                        @Override
                        public void onYearMonthSet(int year, int month) {
                            Calendar oldcalendar = Calendar.getInstance();
                            oldcalendar.set(Calendar.YEAR, year);
                            oldcalendar.set(Calendar.MONTH, month);

                            SimpleDateFormat olddateFormat = new SimpleDateFormat("MMMM yyyy");

                            //yearMonth.setText(dateFormat.format(calendar.getTime()));
                            Log.i("shravan", olddateFormat.format(oldcalendar.getTime()));

                            SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM yyyy");
                            Date date = null;
                            try {
                                date = (dateFormat).parse(olddateFormat.format(oldcalendar.getTime()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            int noOfDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                            maturitydatemonth = simpleDateFormat.format(date);
                            Log.i("shravan", maturitydatemonth);
                            //System.out.println(properFormattedDate+"-"+String.valueOf(noOfDays));
                            if (Utils.isNotNullAndNotEmpty(stage)) {

                                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                    if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwisewithmorefilterFLMLeadsBM(flsid, stage, teammemberuseridlist, "Maturity", maturitydatemonth);


                                        showList(leadModels, searchby, false);
                                    } else {
                                        // showToast("Please choose a User.");
                                    }


                                }


                            } else {

                                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                    if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                        //leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwisewithmorefilterLeadsBM(userid, teammemberuseridlist, "Maturity", maturitydatemonth);
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwisewithmorefilterFLMLeadsBM(flsid, "", teammemberuseridlist, "Maturity", maturitydatemonth);
                                        showList(leadModels, searchby, false);
                                    } else {
                                        // showToast("Please choose a User.");
                                    }


                                }


                            }
                        }
                    });

                    yearMonthPickerDialog.show();
                    Log.i("shravan", "Maturity selected");


                } else {

                    Log.i("shravan", "Maturity not selected");


                    if (Utils.isNotNullAndNotEmpty(stage)) {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getnonmaturityfilterFLMLeadsBM(flsid, stage, teammemberuseridlist, selectedleadsource);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        }


                    } else {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getnonmaturityfilterFLMLeadsBM(flsid, "", teammemberuseridlist, selectedleadsource);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        }


                    }


                }


                if (isNetworkAvailable()) {
                    //getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), selectedtab.getText().toString(), selected, "", "","","","");
                } else {
                    showToast("No Internet");
                }


                bottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }


    public void bottomSheetMyDirectLeadsWiseDialog(ArrayList<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        ListView lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(PSFLeadListActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();


        ArrayAdapter adapter = new ArrayAdapter(PSFLeadListActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                selectedleadsource = parent.getItemAtPosition(position).toString();

                if (selectedleadsource.equalsIgnoreCase("Maturity")) {
                    // rackMonthPicker.show();
                    //createDialogWithoutDateField().show();
                    Calendar calendar = Calendar.getInstance();
                    //calendar.set(2010, 01, 01);

                    YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(PSFLeadListActivity.this, calendar, new YearMonthPickerDialog.OnDateSetListener() {
                        @Override
                        public void onYearMonthSet(int year, int month) {
                            Calendar oldcalendar = Calendar.getInstance();
                            oldcalendar.set(Calendar.YEAR, year);
                            oldcalendar.set(Calendar.MONTH, month);

                            SimpleDateFormat olddateFormat = new SimpleDateFormat("MMMM yyyy");

                            //yearMonth.setText(dateFormat.format(calendar.getTime()));
                            Log.i("shravan", olddateFormat.format(oldcalendar.getTime()));

                            SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM yyyy");
                            Date date = null;
                            try {
                                date = (dateFormat).parse(olddateFormat.format(oldcalendar.getTime()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            int noOfDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                            maturitydatemonth = simpleDateFormat.format(date);
                            Log.i("shravan", maturitydatemonth);
                            //System.out.println(properFormattedDate+"-"+String.valueOf(noOfDays));
                            if (Utils.isNotNullAndNotEmpty(stage)) {

                                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                    if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                        //leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwisewithmorefilterLeadsBM(userid, stage, teammemberuseridlist, "Maturity", maturitydatemonth);
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyDirectLeadsListBM(userid,stage, teammemberuseridlist,"Maturity", maturitydatemonth);

                                        showList(leadModels, searchby, false);
                                    } else {
                                        // showToast("Please choose a User.");
                                    }


                                }


                            } else {

                                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                    if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                        //leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getCampaignwisewithmorefilterLeadsBM(userid, teammemberuseridlist, "Maturity", maturitydatemonth);
                                        leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFMyDirectLeadsListBM(userid,"", teammemberuseridlist,"Maturity", maturitydatemonth);

                                        showList(leadModels, searchby, false);
                                    } else {
                                        // showToast("Please choose a User.");
                                    }


                                }


                            }
                        }
                    });

                    yearMonthPickerDialog.show();
                    Log.i("shravan", "Maturity selected");


                } else {

                    Log.i("shravan", "Maturity not selected");


                    if (Utils.isNotNullAndNotEmpty(stage)) {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getnonmaturityfilterMyDirectLeadsBM(userid, stage, teammemberuseridlist, selectedleadsource);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        }


                    } else {

                        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                            if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getnonmaturityfilterMyDirectLeadsBM(userid,"", teammemberuseridlist, selectedleadsource);
                                showList(leadModels, searchby, false);
                            } else {
                                // showToast("Please choose a User.");
                            }


                        }


                    }


                }


                if (isNetworkAvailable()) {
                    //getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), selectedtab.getText().toString(), selected, "", "","","","");
                } else {
                    showToast("No Internet");
                }


                bottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }

}
