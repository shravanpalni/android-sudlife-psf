package com.outwork.sudlife.psf.lead.model;

import com.intrusoft.sectionedrecyclerview.Section;

import java.util.List;

/**
 * Created by shravanch on 08-01-2020.
 */

public class CalendarSectionHeader implements Section<PSFLeadModel> {

    List<PSFLeadModel> childList;
    String sectionText;

    public CalendarSectionHeader(List<PSFLeadModel> childList, String sectionText) {
        this.childList = childList;
        this.sectionText = sectionText;
    }

    @Override
    public List<PSFLeadModel> getChildItems() {
        return childList;
    }

    public String getSectionText() {
        return sectionText;
    }
}