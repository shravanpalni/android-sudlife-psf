package com.outwork.sudlife.psf.notifications.firebase;

import android.content.Context;


import com.outwork.sudlife.psf.restinterfaces.POJO.DeviceSubscription;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.restinterfaces.UserRestInterface;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Panch on 4/13/2016.
 */
public class PlayServicesHelper {
    private Context context;
    private String regId;

    public PlayServicesHelper(Context activity) {
        this.context = activity;
        updateSubscription();
    }

    private void updateSubscription() {
        String userId = null;
        if (SharedPreferenceManager.getInstance().getString(Constants.USERID,"") != null) {
            userId = SharedPreferenceManager.getInstance().getString(Constants.USERID,"");
        }
        String FCMToken = IvokoApplication.getInstance().getFCMToken(context);
        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN,"");

        DeviceSubscription deviceSubscription = new DeviceSubscription();
        deviceSubscription.setExternalid(userId);
        deviceSubscription.setDevicetype("Android");
        deviceSubscription.setRegistrationid(FCMToken);

        UserRestInterface client = RestService.createService(UserRestInterface.class);
        Call<RestResponse> subscriptionupdate = client.subscribeNotification(userToken, deviceSubscription);
        subscriptionupdate.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    //do nothing now
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
            }
        });
    }
}