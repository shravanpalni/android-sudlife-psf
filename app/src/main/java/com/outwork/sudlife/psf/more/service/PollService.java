package com.outwork.sudlife.psf.more.service;

import com.google.gson.JsonObject;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PollService {

    @GET("PollService/polls")
    Call<RestResponse> getPolls(@Header("utoken") String userToken,
                                @Query("groupid") String groupid);

    @POST("PollService/poll/{pollid}/answer/{answerid}")
    Call<RestResponse> poll(@Header("utoken") String userToken,
                            @Path("pollid") String pollid,
                            @Path("answerid") String answerid);

    @GET("PollService/poll/{pollid}")
    Call<RestResponse> checkPolled(@Header("utoken") String userToken,
                                   @Path("pollid") String pollid);

    @GET("PollService/polls/{pollid}")
    Call<RestResponse> getPoll(@Header("utoken") String userToken,
                               @Path("pollid") String pollid,
                               @Query("groupid") String groupid);

    @POST("admin/polls")
    Call<RestResponse> postPoll (@Header("utoken") String userToken,
                                 @Query("groupid") String groupid,
                                 @Query("type") String type,
                                 @Body JsonObject sJsonBody);
}
