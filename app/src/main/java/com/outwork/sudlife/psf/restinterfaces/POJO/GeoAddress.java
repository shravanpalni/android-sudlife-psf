package com.outwork.sudlife.psf.restinterfaces.POJO;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Panch on 12/22/2016.
 */

public class GeoAddress implements Parcelable, Serializable {

    @SerializedName("line1")
    @Expose
    private String addressline1;

    public String getAddressline1() {
        return addressline1;
    }

    @SerializedName("line2")
    @Expose
    private String addressline2;

    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;


    @SerializedName("locality")
    @Expose
    private String locality;


    @SerializedName("landmark")
    @Expose
    private String landmark;

    public void setAddressline1(String addressline1) {
        this.addressline1 = addressline1;
    }

    public String getAddressline2() {
        return addressline2;
    }

    public void setAddressline2(String addressline2) {
        this.addressline2 = addressline2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }


    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public GeoAddress() {

    }


    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(addressline1);
        dest.writeString(addressline2);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(locality);
        dest.writeString(landmark);
    }

    public GeoAddress(String json) {

        GeoAddress temp = new Gson().fromJson(json, GeoAddress.class);
        this.addressline1 = temp.getAddressline1();
        this.addressline2 = temp.getAddressline2();
        this.city = temp.getCity();
        this.state = temp.getState();
        this.locality = temp.getLocality();
        this.landmark = temp.getLandmark();
    }

    public GeoAddress(Parcel in) {
        this.addressline1 = in.readString();
        this.addressline2 = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.locality = in.readString();
        this.landmark = in.readString();
    }

    public static final Parcelable.Creator<GeoAddress> CREATOR
            = new Parcelable.Creator<GeoAddress>() {
        public GeoAddress createFromParcel(Parcel in) {
            return new GeoAddress(in);
        }

        public GeoAddress[] newArray(int size) {
            return new GeoAddress[size];
        }
    };
}
