package com.outwork.sudlife.psf.ui.models;

/**
 * Created by Habi on 01-08-2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Groupinfo implements Serializable{

    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("groupname")
    @Expose
    private String groupname;
    @SerializedName("groupcode")
    @Expose
    private String groupcode;
    @SerializedName("objecttype")
    @Expose
    private String objecttype;
    @SerializedName("Deleted")
    @Expose
    private Object deleted;
    @SerializedName("ownerid")
    @Expose
    private Object ownerid;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("groupType")
    @Expose
    private String groupType;
    @SerializedName("groupicon")
    @Expose
    private String groupicon;
    @SerializedName("groupimage")
    @Expose
    private String groupimage;
    @SerializedName("displayname")
    @Expose
    private String displayname;
    @SerializedName("internalname")
    @Expose
    private String internalname;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("dialogid")
    @Expose
    private Object dialogid;
    @SerializedName("xmpp_room_jid")
    @Expose
    private Object xmppRoomJid;
    @SerializedName("type")
    @Expose
    private Object type;
    @SerializedName("createddate")
    @Expose
    private Object createddate;
    @SerializedName("modifieddate")
    @Expose
    private Object modifieddate;
    @SerializedName("unreadCount")
    @Expose
    private Object unreadCount;
    @SerializedName("featured")
    @Expose
    private String featured;
    @SerializedName("isproductavailable")
    @Expose
    private String isproductavailable;
    @SerializedName("ishealthcare")
    @Expose
    private String ishealthcare;
    @SerializedName("istaskavailable")
    @Expose
    private String istaskavailable;
    @SerializedName("isorderproductavailable")
    @Expose
    private String isorderproductavailable;

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getGroupcode() {
        return groupcode;
    }

    public void setGroupcode(String groupcode) {
        this.groupcode = groupcode;
    }

    public String getObjecttype() {
        return objecttype;
    }

    public void setObjecttype(String objecttype) {
        this.objecttype = objecttype;
    }

    public Object getDeleted() {
        return deleted;
    }

    public void setDeleted(Object deleted) {
        this.deleted = deleted;
    }

    public Object getOwnerid() {
        return ownerid;
    }

    public void setOwnerid(Object ownerid) {
        this.ownerid = ownerid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getGroupicon() {
        return groupicon;
    }

    public void setGroupicon(String groupicon) {
        this.groupicon = groupicon;
    }

    public String getGroupimage() {
        return groupimage;
    }

    public void setGroupimage(String groupimage) {
        this.groupimage = groupimage;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getInternalname() {
        return internalname;
    }

    public void setInternalname(String internalname) {
        this.internalname = internalname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getDialogid() {
        return dialogid;
    }

    public void setDialogid(Object dialogid) {
        this.dialogid = dialogid;
    }

    public Object getXmppRoomJid() {
        return xmppRoomJid;
    }

    public void setXmppRoomJid(Object xmppRoomJid) {
        this.xmppRoomJid = xmppRoomJid;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Object getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Object createddate) {
        this.createddate = createddate;
    }

    public Object getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(Object modifieddate) {
        this.modifieddate = modifieddate;
    }

    public Object getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(Object unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public String getIsproductavailable() {
        return isproductavailable;
    }

    public void setIsproductavailable(String isproductavailable) {
        this.isproductavailable = isproductavailable;
    }

    public String getIshealthcare() {
        return ishealthcare;
    }

    public void setIshealthcare(String ishealthcare) {
        this.ishealthcare = ishealthcare;
    }

    public String getIstaskavailable() {
        return istaskavailable;
    }

    public void setIstaskavailable(String istaskavailable) {
        this.istaskavailable = istaskavailable;
    }

    public String getIsorderproductavailable() {
        return isorderproductavailable;
    }

    public void setIsorderproductavailable(String isorderproductavailable) {
        this.isorderproductavailable = isorderproductavailable;
    }

}