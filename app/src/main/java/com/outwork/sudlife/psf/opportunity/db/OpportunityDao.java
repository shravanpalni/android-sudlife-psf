package com.outwork.sudlife.psf.opportunity.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import com.outwork.sudlife.psf.dao.DBHelper;
import com.outwork.sudlife.psf.dao.IvokoProvider;
import com.outwork.sudlife.psf.opportunity.models.OpportunityItems;
import com.outwork.sudlife.psf.opportunity.models.OpportunityModel;
import com.outwork.sudlife.psf.opportunity.models.StagesModel;
import com.outwork.sudlife.psf.utilities.Utils;

public class OpportunityDao {

    public static final String TAG = OpportunityDao.class.getSimpleName();

    private static OpportunityDao instance;
    private Context applicationContext;

    public static OpportunityDao getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new OpportunityDao(applicationContext);
        }
        return instance;
    }

    private OpportunityDao(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public long insertLocalOpportunity(OpportunityModel dto, String network_status) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(IvokoProvider.Opportunity.userid, dto.getUserid());
        values.put(IvokoProvider.Opportunity.groupid, dto.getGroupid());
        values.put(IvokoProvider.Opportunity.firstname, dto.getFirstname());
        values.put(IvokoProvider.Opportunity.lastname, dto.getLastname());
        values.put(IvokoProvider.Opportunity.gender, dto.getGender());
        values.put(IvokoProvider.Opportunity.closingdate, dto.getClosingdate());
        values.put(IvokoProvider.Opportunity.stage, dto.getStage());
        if (dto.getAge() != null)
            values.put(IvokoProvider.Opportunity.age, dto.getAge());
        values.put(IvokoProvider.Opportunity.network_status, network_status);
        values.put(IvokoProvider.Opportunity.value, dto.getValue());
        if (Utils.isNotNullAndNotEmpty(dto.getOccupation()))
            values.put(IvokoProvider.Opportunity.occupation, dto.getOccupation());
        if (dto.getIncome() != null)
            values.put(IvokoProvider.Opportunity.income, dto.getIncome());
        if (dto.getFamilymembers() != null)
            values.put(IvokoProvider.Opportunity.familymembers, dto.getFamilymembers());
        if (Utils.isNotNullAndNotEmpty(dto.getRemarks()))
            values.put(IvokoProvider.Opportunity.notes, dto.getRemarks());
        if (Utils.isNotNullAndNotEmpty(dto.getItems())) {
            values.put(IvokoProvider.Opportunity.items, new Gson().toJson(Utils.isNotNullAndNotEmpty(dto.getItems())));
        }
        long oppID = db.insert(IvokoProvider.Opportunity.TABLE, null, values);
        return oppID;
    }

    public long insertOpportunity(SQLiteDatabase db, OpportunityModel dto) {
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(dto.getOpportunityid())) {
            values.put(IvokoProvider.Opportunity.opportunityid, dto.getOpportunityid());
        }
        values.put(IvokoProvider.Opportunity.userid, dto.getUserid());
        values.put(IvokoProvider.Opportunity.groupid, dto.getGroupid());
        values.put(IvokoProvider.Opportunity.firstname, dto.getFirstname());
        values.put(IvokoProvider.Opportunity.lastname, dto.getLastname());
        values.put(IvokoProvider.Opportunity.gender, dto.getGender());
        values.put(IvokoProvider.Opportunity.closingdate, dto.getClosingdate());
        values.put(IvokoProvider.Opportunity.stage, dto.getStage());
        if (dto.getAge() != null)
            values.put(IvokoProvider.Opportunity.age, dto.getAge());
        values.put(IvokoProvider.Opportunity.value, dto.getValue());
        if (Utils.isNotNullAndNotEmpty(dto.getOccupation()))
            values.put(IvokoProvider.Opportunity.occupation, dto.getOccupation());
        if (dto.getIncome() != null)
            values.put(IvokoProvider.Opportunity.income, dto.getIncome());
        if (dto.getFamilymembers() != null)
            values.put(IvokoProvider.Opportunity.familymembers, dto.getFamilymembers());
        if (Utils.isNotNullAndNotEmpty(dto.getRemarks()))
            values.put(IvokoProvider.Opportunity.notes, dto.getRemarks());
        if (Utils.isNotNullAndNotEmpty(dto.getItems())) {
            values.put(IvokoProvider.Opportunity.items, new Gson().toJson(Utils.isNotNullAndNotEmpty(dto.getItems())));
        }
        long oppID = db.insert(IvokoProvider.Opportunity.TABLE, null, values);
        return oppID;
    }

    public void insertOpportunityList(List<OpportunityModel> OpportunityModelList, String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        for (int j = 0; j < OpportunityModelList.size(); j++) {
            OpportunityModel opportunityModel = OpportunityModelList.get(j);
            if (!isRecordExist(db, opportunityModel.getOpportunityid())) {
                insertOpportunity(db, opportunityModel);
            } else {
                if (Utils.isNotNullAndNotEmpty(opportunityModel.getOpportunityid())) {
                    deleteOpportunityRecordID(opportunityModel.getOpportunityid());
                    if (!opportunityModel.getIsdeleted())
                        insertOpportunity(db, opportunityModel);
                }
            }
        }
    }

    private void updateOpportunity(SQLiteDatabase db, OpportunityModel opportunityModel, String userId) {
        ContentValues values = new ContentValues();
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getOpportunityid())) {
            values.put(IvokoProvider.Opportunity.opportunityid, opportunityModel.getOpportunityid());
        }
        values.put(IvokoProvider.Opportunity.userid, opportunityModel.getUserid());
        values.put(IvokoProvider.Opportunity.groupid, opportunityModel.getGroupid());
        values.put(IvokoProvider.Opportunity.firstname, opportunityModel.getFirstname());
        values.put(IvokoProvider.Opportunity.lastname, opportunityModel.getLastname());
        values.put(IvokoProvider.Opportunity.gender, opportunityModel.getGender());
        values.put(IvokoProvider.Opportunity.closingdate, opportunityModel.getClosingdate());
        values.put(IvokoProvider.Opportunity.stage, opportunityModel.getStage());
        if (opportunityModel.getAge() != null)
            values.put(IvokoProvider.Opportunity.age, opportunityModel.getAge());
        values.put(IvokoProvider.Opportunity.network_status, opportunityModel.getStatus());
        values.put(IvokoProvider.Opportunity.value, opportunityModel.getValue());
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getOccupation()))
            values.put(IvokoProvider.Opportunity.occupation, opportunityModel.getOccupation());
        if (opportunityModel.getIncome() != null)
            values.put(IvokoProvider.Opportunity.income, opportunityModel.getIncome());
        if (opportunityModel.getFamilymembers() != null)
            values.put(IvokoProvider.Opportunity.familymembers, opportunityModel.getFamilymembers());
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getRemarks()))
            values.put(IvokoProvider.Opportunity.notes, opportunityModel.getRemarks());

        db.update(IvokoProvider.Opportunity.TABLE, values, IvokoProvider.Opportunity.opportunityid + " = " + opportunityModel.getOpportunityid() + " AND " +
                IvokoProvider.Opportunity.userid + " = ?", new String[]{userId});
    }

    public void updateOpportunityOnline(OpportunityModel dto, String userId, String opportunityid) {

        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IvokoProvider.Opportunity.opportunityid, opportunityid);
        values.put(IvokoProvider.Opportunity.network_status, "");

        db.update(IvokoProvider.Opportunity.TABLE, values, IvokoProvider.Opportunity._ID + " = " + dto.getId() + " AND " +
                IvokoProvider.Opportunity.userid + " = ?", new String[]{userId});
    }

    public void updateLocalOpportunity(OpportunityModel dto, String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(IvokoProvider.Opportunity.notes, dto.getRemarks());
        values.put(IvokoProvider.Opportunity.closingdate, dto.getClosingdate());
        values.put(IvokoProvider.Opportunity.value, dto.getValue());
        values.put(IvokoProvider.Opportunity.stage, dto.getStage());
        values.put(IvokoProvider.Opportunity.network_status, "offline");

        db.update(IvokoProvider.Opportunity.TABLE, values, IvokoProvider.Opportunity._ID + " = " + dto.getId() + " AND " +
                IvokoProvider.Opportunity.userid + " = ?", new String[]{userId});
    }

    public OpportunityModel getOpportunity(String OpportunityId, String userid) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();
        OpportunityModel opportunityModel = new OpportunityModel();
        if (isTableExists(db, IvokoProvider.Opportunity.TABLE)) {
            Cursor cursor = null;
            // security

           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Opportunity.TABLE;
            sql += " WHERE " + IvokoProvider.Opportunity.opportunityid + " = '" + OpportunityId + "'";
            sql += " AND " + IvokoProvider.Opportunity.userid + " = '" + userid + "'";
            sql += " LIMIT 1 ";*/




            try {
                //cursor = db.rawQuery(sql, null);
               /* cursor=db.rawQuery("select * from "+IvokoProvider.Opportunity.TABLE+" where "+ "opportunityid=? and userid=? limit 1",
                        new String [] {OpportunityId, userid});*/

                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Opportunity.TABLE + " WHERE " + IvokoProvider.Opportunity.opportunityid + " = ?" + " AND " + IvokoProvider.Opportunity.userid + " = ?" + " LIMIT 1" , new String[]{OpportunityId,userid});


                if (cursor.moveToFirst()) {
                    do {
                        opportunityModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity._ID)));
                        opportunityModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.userid)));
                        opportunityModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.groupid)));
                        opportunityModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.network_status)));
                        opportunityModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.firstname)));
                        opportunityModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.notes)));
                        opportunityModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.opportunityid)));
                        opportunityModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.lastname)));
                        opportunityModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.age)));
                        opportunityModel.setOrdervalue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));
                        opportunityModel.setClosingdate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.closingdate)));
                        opportunityModel.setGender(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.gender)));
                        opportunityModel.setStage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.stage)));
                        opportunityModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.occupation)));
                        opportunityModel.setIncome(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.income)));
                        opportunityModel.setValue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));

                        opportunityModel.setFamilymembers(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.familymembers)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return opportunityModel;
    }

    public OpportunityModel getOfflineOpportunityByID(int id, String userid) {

        OpportunityModel opportunityModel = new OpportunityModel();
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getReadableDatabase();

        if (isTableExists(db, IvokoProvider.Opportunity.TABLE)) {
            Cursor cursor = null;
            // security

           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Opportunity.TABLE;
            sql += " WHERE " + IvokoProvider.Opportunity._ID + " = '" + id + "'";
            sql += " AND " + IvokoProvider.Opportunity.userid + " = '" + userid + "'";
            sql += " LIMIT 1 ";*/
            try {
                //cursor = db.rawQuery(sql, null);
                /*cursor=db.rawQuery("select * from "+IvokoProvider.Opportunity.TABLE+" where "+ "_id=? and userid=? limit 1",
                        new String [] {String.valueOf(id), userid});*/


                cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Opportunity.TABLE + " WHERE " + IvokoProvider.Opportunity._ID + " = ?" + " AND " + IvokoProvider.Opportunity.userid + " = ?" + " LIMIT 1" , new String[]{String.valueOf(id),userid});

                if (cursor.moveToFirst()) {
                    do {
                        opportunityModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity._ID)));
                        opportunityModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.userid)));
                        opportunityModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.groupid)));
                        opportunityModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.network_status)));
                        opportunityModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.firstname)));
                        opportunityModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.notes)));
                        opportunityModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.opportunityid)));
                        opportunityModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.lastname)));
                        opportunityModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.age)));
                        opportunityModel.setOrdervalue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));
                        opportunityModel.setClosingdate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.closingdate)));
                        opportunityModel.setGender(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.gender)));
                        opportunityModel.setStage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.stage)));
                        opportunityModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.occupation)));
                        opportunityModel.setIncome(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.income)));
                        opportunityModel.setValue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));
                        opportunityModel.setFamilymembers(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.familymembers)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return opportunityModel;
    }

    public List<OpportunityModel> getOpportunitylocalidsList(String userId, String oppid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<OpportunityModel> opportunityList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Opportunity.TABLE)) {
            Cursor cursor = null;
            // security

           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Opportunity.TABLE;
            sql += " WHERE " + IvokoProvider.Opportunity.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Opportunity.opportunityid + " = '" + oppid + "'";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor=db.rawQuery("select * from "+IvokoProvider.Opportunity.TABLE+" where "+ "userid=? and opportunityid=? ",
                        new String [] {userId, oppid});

                if (cursor.moveToFirst()) {
                    do {
                        OpportunityModel opportunityModel = new OpportunityModel();
                        opportunityModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity._ID)));
                        opportunityModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.network_status)));
                        opportunityModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.opportunityid)));
                        opportunityModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.firstname)));
                        opportunityModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.lastname)));
                        opportunityModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.age)));
                        opportunityModel.setGender(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.gender)));
                        opportunityModel.setIncome(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.income)));
                        opportunityList.add(opportunityModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return opportunityList;
    }

    public List<OpportunityModel> getOpportunityList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<OpportunityModel> opportunityList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Opportunity.TABLE)) {
            Cursor cursor = null;
            //security

           /* String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Opportunity.TABLE;
            sql += " WHERE " + IvokoProvider.Opportunity.userid + " = '" + userId + "'";
            sql += " ORDER BY " + IvokoProvider.Opportunity.closingdate + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor=db.rawQuery("select * from "+IvokoProvider.Opportunity.TABLE+" where "+ "userid=? order by closingdate=? ",
                        new String [] {userId, " ASC "});



                if (cursor.moveToFirst()) {
                    do {
                        OpportunityModel opportunityModel = new OpportunityModel();
                        opportunityModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity._ID)));
                        opportunityModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.userid)));
                        opportunityModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.groupid)));
                        opportunityModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.network_status)));
                        opportunityModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.firstname)));
                        opportunityModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.notes)));
                        opportunityModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.opportunityid)));
                        opportunityModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.lastname)));
                        opportunityModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.age)));
                        opportunityModel.setOrdervalue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));
                        opportunityModel.setClosingdate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.closingdate)));
                        opportunityModel.setGender(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.gender)));
                        opportunityModel.setStage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.stage)));
                        opportunityModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.occupation)));
                        opportunityModel.setIncome(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.income)));
                        opportunityModel.setValue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));
                        opportunityModel.setFamilymembers(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.familymembers)));
                        opportunityList.add(opportunityModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return opportunityList;
    }

    public List<OpportunityModel> getOpportunityListforSearch(String userId, String userInput) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<OpportunityModel> opportunityList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Opportunity.TABLE)) {
            Cursor cursor = null;
            // security
            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Opportunity.TABLE;
            sql += " WHERE " + IvokoProvider.Opportunity.userid + " = '" + userId + "'";
            sql += " AND (" + IvokoProvider.Opportunity.firstname + " LIKE '%" + userInput + "%'";
            sql += " OR " + IvokoProvider.Opportunity.stage + " LIKE '%" + userInput + "%'";
            sql += " OR " + IvokoProvider.Opportunity.lastname + " LIKE '%" + userInput + "%')";
            sql += " ORDER BY " + IvokoProvider.Opportunity.closingdate + " ASC ";*/
            try {
                // cursor = db.rawQuery(sql, null);


                cursor=db.rawQuery("select * from "+IvokoProvider.Opportunity.TABLE+" where "+ "userid=? and (firstname like ? or stage like ? or lastname like ?) order by closingdate=? ",
                        new String [] {userId,'%' + userInput + '%', " ASC "});




                if (cursor.moveToFirst()) {
                    do {
                        OpportunityModel opportunityModel = new OpportunityModel();
                        opportunityModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity._ID)));
                        opportunityModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.userid)));
                        opportunityModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.groupid)));
                        opportunityModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.network_status)));
                        opportunityModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.firstname)));
                        opportunityModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.notes)));
                        opportunityModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.opportunityid)));
                        opportunityModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.lastname)));
                        opportunityModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.age)));
                        opportunityModel.setOrdervalue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));
                        opportunityModel.setClosingdate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.closingdate)));
                        opportunityModel.setGender(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.gender)));
                        opportunityModel.setStage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.stage)));
                        opportunityModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.occupation)));
                        opportunityModel.setIncome(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.income)));
                        opportunityModel.setValue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));
                        opportunityModel.setFamilymembers(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.familymembers)));
                        opportunityList.add(opportunityModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return opportunityList;
    }

    public List<OpportunityModel> getOpportunityListonStage(String userId, String userInput) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<OpportunityModel> opportunityList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Opportunity.TABLE)) {
            Cursor cursor = null;
            // security
            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Opportunity.TABLE;
            sql += " WHERE " + IvokoProvider.Opportunity.stage + " = '" + userInput + "'";
            sql += " AND " + IvokoProvider.Opportunity.userid + " = '" + userId + "'";
            sql += " ORDER BY " + IvokoProvider.Opportunity.closingdate + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);

                cursor=db.rawQuery("select * from "+IvokoProvider.Opportunity.TABLE+" where "+ "stage=? and userid=? order by closingdate=? ",
                        new String [] {userInput,userId," ASC "});



                if (cursor.moveToFirst()) {
                    do {
                        OpportunityModel opportunityModel = new OpportunityModel();
                        opportunityModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity._ID)));
                        opportunityModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.userid)));
                        opportunityModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.groupid)));
                        opportunityModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.network_status)));
                        opportunityModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.firstname)));
                        opportunityModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.notes)));
                        opportunityModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.opportunityid)));
                        opportunityModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.lastname)));
                        opportunityModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.age)));
                        opportunityModel.setOrdervalue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));
                        opportunityModel.setClosingdate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.closingdate)));
                        opportunityModel.setGender(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.gender)));
                        opportunityModel.setStage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.stage)));
                        opportunityModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.occupation)));
                        opportunityModel.setIncome(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.income)));
                        opportunityModel.setValue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));
                        opportunityModel.setFamilymembers(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.familymembers)));
                        opportunityList.add(opportunityModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return opportunityList;
    }

    public List<OpportunityModel> getOpportunityOfflineList(String userId, String network_status) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<OpportunityModel> opportunityList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.Opportunity.TABLE)) {
            Cursor cursor = null;
            // security
            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.Opportunity.TABLE;
            sql += " WHERE " + IvokoProvider.Opportunity.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.Opportunity.network_status + " = '" + network_status + "'";*/

            try {
                //cursor = db.rawQuery(sql, null);

                cursor=db.rawQuery("select * from "+IvokoProvider.Opportunity.TABLE+" where "+ "userid=? and network_status=?",
                        new String [] {userId,network_status});


                if (cursor.moveToFirst()) {
                    do {
                        OpportunityModel opportunityModel = new OpportunityModel();
                        ArrayList<OpportunityItems> productModelList = new ArrayList<>();
                        opportunityModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity._ID)));
                        opportunityModel.setUserid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.userid)));
                        opportunityModel.setGroupid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.groupid)));
                        opportunityModel.setNetwork_status(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.network_status)));
                        opportunityModel.setFirstname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.firstname)));
                        opportunityModel.setRemarks(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.notes)));
                        opportunityModel.setOpportunityid(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.opportunityid)));
                        opportunityModel.setLastname(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.lastname)));
                        opportunityModel.setAge(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.age)));
                        opportunityModel.setOrdervalue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));
                        opportunityModel.setClosingdate(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.closingdate)));
                        opportunityModel.setGender(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.gender)));
                        opportunityModel.setStage(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.stage)));
                        opportunityModel.setOccupation(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.occupation)));
                        opportunityModel.setIncome(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.income)));
                        opportunityModel.setValue(cursor.getString(cursor.getColumnIndex(IvokoProvider.Opportunity.value)));
                        opportunityModel.setFamilymembers(cursor.getInt(cursor.getColumnIndex(IvokoProvider.Opportunity.familymembers)));
                        JSONArray jsonarray2 = null;
                        opportunityModel.setItems(productModelList);
                        opportunityList.add(opportunityModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return opportunityList;
    }

    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            else{
                cursor.close();
            }
        }
        return false;
    }

    private boolean isRecordExist(SQLiteDatabase db, String OpportunityId) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.Opportunity.TABLE + " WHERE " + IvokoProvider.Opportunity.opportunityid + " = ?", new String[]{OpportunityId});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void deleteOpportunityRecordID(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.Opportunity.TABLE)) {
            db.execSQL("delete from " + IvokoProvider.Opportunity.TABLE + " WHERE " + IvokoProvider.Opportunity.opportunityid + " = '" + id + "'");
        }
    }

    /****************************************************stages**************************/

    public long insertOpportunitystages(SQLiteDatabase db, StagesModel dto, String userid) {
        ContentValues values = new ContentValues();

        values.put(IvokoProvider.OpportunityStages.userid, userid);
        if (Utils.isNotNullAndNotEmpty(dto.getStageid())) {
            values.put(IvokoProvider.OpportunityStages.stageid, dto.getStageid());
        }
        values.put(IvokoProvider.OpportunityStages.name, dto.getName());
        values.put(IvokoProvider.OpportunityStages.description, dto.getDescription());
        values.put(IvokoProvider.OpportunityStages.defaultorder, dto.getOrder());
        values.put(IvokoProvider.OpportunityStages.isdefault, dto.getIsdefault());
        values.put(IvokoProvider.OpportunityStages.colorcode, dto.getColorcode());
        values.put(IvokoProvider.OpportunityStages.modifiedon, dto.getModifiedon());

        long oppID = db.insert(IvokoProvider.OpportunityStages.TABLE, null, values);
        return oppID;
    }

    public void insertOpportunitystagesList(List<StagesModel> stagesModelList, String userid) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getWritableDatabase();
        for (int j = 0; j < stagesModelList.size(); j++) {
            StagesModel stagesModel = stagesModelList.get(j);

            if (!isStageRecordExist(db, stagesModel.getStageid())) {
                insertOpportunitystages(db, stagesModel, userid);
            } else {
                if (Utils.isNotNullAndNotEmpty(stagesModel.getStageid())) {
                    deleteStageRecordID(stagesModel.getStageid());
                    insertOpportunitystages(db, stagesModel, userid);
                }
            }
        }
    }

    public void updateOpportunitystages(SQLiteDatabase db, StagesModel dto, String userid) {
        ContentValues values = new ContentValues();

        values.put(IvokoProvider.OpportunityStages.userid, userid);
        if (Utils.isNotNullAndNotEmpty(dto.getStageid())) {
            values.put(IvokoProvider.OpportunityStages.stageid, dto.getStageid());
        }
        values.put(IvokoProvider.OpportunityStages.name, dto.getName());
        values.put(IvokoProvider.OpportunityStages.description, dto.getDescription());
        values.put(IvokoProvider.OpportunityStages.defaultorder, dto.getOrder());
        values.put(IvokoProvider.OpportunityStages.isdefault, dto.getIsdefault());
        values.put(IvokoProvider.OpportunityStages.colorcode, dto.getColorcode());
        values.put(IvokoProvider.OpportunityStages.modifiedon, dto.getModifiedon());

        db.update(IvokoProvider.OpportunityStages.TABLE, values, IvokoProvider.OpportunityStages._ID + " = " + dto.getStageid() + " AND " +
                IvokoProvider.OpportunityStages.userid + " = ?", new String[]{userid});
    }

    public List<String> getOpportunitystagesnamesList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();

        List<String> stagesModelNamesList = new ArrayList<>();

        if (isTableExists(db, IvokoProvider.OpportunityStages.TABLE)) {
            Cursor cursor = null;
            // security
          /*  String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.OpportunityStages.TABLE;
            sql += " WHERE " + IvokoProvider.OpportunityStages.userid + " = '" + userId + "'";
            sql += " ORDER BY " + IvokoProvider.OpportunityStages.defaultorder + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);
                //security

                cursor=db.rawQuery("select * from "+IvokoProvider.OpportunityStages.TABLE+" where "+ "userid=? order by defaultorder=?",
                        new String [] {userId," ASC "});



                if (cursor.moveToFirst()) {
                    do {
                        stagesModelNamesList.add(cursor.getString(cursor.getColumnIndex(IvokoProvider.OpportunityStages.name)));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return stagesModelNamesList;
    }

    public String getOpportunitystagescolor(String userId, String stagename) {
        String colorcode = "";
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();

        if (isTableExists(db, IvokoProvider.OpportunityStages.TABLE)) {
            Cursor cursor = null;
            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.OpportunityStages.TABLE;
            sql += " WHERE " + IvokoProvider.OpportunityStages.userid + " = '" + userId + "'";
            sql += " AND " + IvokoProvider.OpportunityStages.name + " = '" + stagename + "'";
            sql += " LIMIT 1 ";*/
            try {
                //cursor = db.rawQuery(sql, null);
                // security

                cursor=db.rawQuery("select * from "+IvokoProvider.OpportunityStages.TABLE+" where "+ "userid=? and name=? limit 1",
                        new String [] {userId,stagename});

                if (cursor.moveToFirst()) {
                    do {
                        colorcode = cursor.getString(cursor.getColumnIndex(IvokoProvider.OpportunityStages.colorcode));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return colorcode;
    }

    public List<StagesModel> getOpportunitystagesList(String userId) {
        SQLiteDatabase db = DBHelper.getInstance(this.applicationContext).getReadableDatabase();
        List<StagesModel> stagesModelList = new ArrayList<>();
        if (isTableExists(db, IvokoProvider.OpportunityStages.TABLE)) {
            Cursor cursor = null;
            /*String sql = "";
            sql += "SELECT * FROM " + IvokoProvider.OpportunityStages.TABLE;
            sql += " WHERE " + IvokoProvider.OpportunityStages.userid + " = '" + userId + "'";
            sql += " ORDER BY " + IvokoProvider.OpportunityStages.defaultorder + " ASC ";*/
            try {
                //cursor = db.rawQuery(sql, null);
                // security

                cursor=db.rawQuery("select * from "+IvokoProvider.OpportunityStages.TABLE+" where "+ "userid=? order by defaultorder=?",
                        new String [] {userId," ASC "});
                if (cursor.moveToFirst()) {
                    do {
                        StagesModel stagesModel = new StagesModel();

                        stagesModel.setId(cursor.getInt(cursor.getColumnIndex(IvokoProvider.OpportunityStages._ID)));
                        stagesModel.setUserId(cursor.getString(cursor.getColumnIndex(IvokoProvider.OpportunityStages.userid)));
                        stagesModel.setStageid(cursor.getString(cursor.getColumnIndex(IvokoProvider.OpportunityStages.stageid)));
                        stagesModel.setName(cursor.getString(cursor.getColumnIndex(IvokoProvider.OpportunityStages.name)));
                        stagesModel.setDescription(cursor.getString(cursor.getColumnIndex(IvokoProvider.OpportunityStages.description)));
                        stagesModel.setOrder(cursor.getInt(cursor.getColumnIndex(IvokoProvider.OpportunityStages.defaultorder)));
                        stagesModel.setIsdefault(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(IvokoProvider.OpportunityStages.isdefault))));
                        stagesModel.setColorcode(cursor.getString(cursor.getColumnIndex(IvokoProvider.OpportunityStages.colorcode)));
                        stagesModel.setModifiedon(cursor.getString(cursor.getColumnIndex(IvokoProvider.OpportunityStages.modifiedon)));
                        stagesModelList.add(stagesModel);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return stagesModelList;
    }

    public void deleteStageRecordID(String id) {
        SQLiteDatabase db = DBHelper.getInstance(applicationContext).getWritableDatabase();
        if (isTableExists(db, IvokoProvider.OpportunityStages.TABLE)) {
            db.execSQL("delete from " + IvokoProvider.OpportunityStages.TABLE + " WHERE " + IvokoProvider.OpportunityStages.stageid + " = '" + id + "'");
        }
    }

    private boolean isStageRecordExist(SQLiteDatabase db, String stageId) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + IvokoProvider.OpportunityStages.TABLE + " WHERE " + IvokoProvider.OpportunityStages.stageid + " = ?", new String[]{stageId});
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
}