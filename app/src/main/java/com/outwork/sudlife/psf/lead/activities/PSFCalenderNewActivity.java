package com.outwork.sudlife.psf.lead.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.services.CustomerService;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.adapters.AdapterSectionRecycler;
import com.outwork.sudlife.psf.lead.adapters.PSFLeadsAdapter;
import com.outwork.sudlife.psf.lead.model.CalendarSectionHeader;
import com.outwork.sudlife.psf.lead.model.PSFLeadModel;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.adapter.UserTeamNamesAdapter;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.activities.DashBoardActivity;
import com.outwork.sudlife.psf.ui.activities.NotificationsListActivity;
import com.outwork.sudlife.psf.ui.base.AppBaseActivity;
import com.outwork.sudlife.psf.ui.models.UserTeams;
import com.outwork.sudlife.psf.ui.models.Userprofile;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shravanch on 26-12-2019.
 */

@RequiresApi(api = Build.VERSION_CODES.O)
public class PSFCalenderNewActivity extends BaseActivity implements OnDateSelectedListener, OnMonthChangedListener {

    private MaterialCalendarView compactCalendarView;
    private TextView nomessageview, toolbar_title;
    //private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("EEE, d MMM yyyy");
    private RecyclerView leadsRecyclerView;
    private FloatingActionButton addplan;
    private PSFLeadsAdapter flscalenderAdapter;

    AdapterSectionRecycler bmcalenderAdapter;
    private int day, monthno, year;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressBar msgListProgressBar;
    private String toolbardate, displaydate;
    private TextView textCartItemCount;
    int mCartItemCount;
    List<PSFLeadModel> leadModels = new ArrayList<>();
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private SimpleDateFormat dateFormatDisplaying = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.getDefault());
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private List<CalendarSectionHeader> sections = new ArrayList<>();
    private  List<UserTeams> userTeamsList = new ArrayList<>();
    private UserTeams userteamObj;
    private ArrayList<Userprofile> teamMembersHeirarchyArrayList = new ArrayList();
    private List<String> teammemberuseridlist;
    private List<String> showteammemberuseridlist;
    private String selectedTeamId = null;


    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_psfcalender);


        //leadModels = LeadMgr.getInstance(PSFCalenderNewActivity.this).getPSFAppointmentList(userid);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        leadsRecyclerView = (RecyclerView) findViewById(R.id.fflogRecycler);
        nomessageview = (TextView) findViewById(R.id.ffnomessages);
        compactCalendarView = (MaterialCalendarView) findViewById(R.id.calendarView);
        msgListProgressBar = (ProgressBar) findViewById(R.id.ffmessageListProgressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.task_swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.topbarcolor, R.color.daycolor);
        addplan = (FloatingActionButton) findViewById(R.id.addplan);
        addplan.setVisibility(View.GONE);
        toolbardate = TimeUtils.getCurrentDate("yyyy-MM-dd");
        displaydate = TimeUtils.getCurrentDate("dd/MM/yyyy hh:mm a");
        initToolBar();
        day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
        monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
        year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
        compactCalendarView.setCurrentDate(Calendar.getInstance().getTime());
        compactCalendarView.setDateSelected(Calendar.getInstance(), true);
        compactCalendarView.setTileSizeDp(32);
        toolbar_title.setText(dateFormatForMonth.format(compactCalendarView.getCurrentDate().getDate()));

        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, toolbar_title);
        Utils.setTypefaces(IvokoApplication.robotoTypeface, nomessageview);


        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

            if (isNetworkAvailable()) {

                userTeamsList = PlannerMgr.getInstance(PSFCalenderNewActivity.this).getUserTeamList(userid);
                Log.i("shravan", "userTeamsList calendar size = = = " + userTeamsList.size());

                if (userTeamsList.size() > 1) {

                    showTeamDialog();

                } else {

                    for (UserTeams us : userTeamsList) {


                        if (isNetworkAvailable()) {
                            selectedTeamId = us.getTeamid().toString();
                            getTeamHierarchyMembers(us.getTeamid().toString());

                        } else {

                            showSimpleAlert("", "Oops...No Internet connection.");
                        }


                    }


                }


            } else {

                showSimpleAlert("", "Oops...No Internet connection.");
            }


        }else  if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
            showlist(day, monthno, year);
        }


        compactCalendarView.setOnDateChangedListener(this);
        compactCalendarView.setOnMonthChangedListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            /*    day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", toolbardate));
                monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", toolbardate));
                year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", toolbardate));
                compactCalendarView.setCurrentDate(Calendar.getInstance().getTime());
                compactCalendarView.setDateSelected(Calendar.getInstance(), true);*/
                showlist(day, monthno, year);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });


    }


    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {


        toolbar_title.setText(dateFormatForMonth.format(date.getDate()));
        String dateString = (dateFormat.format(date.getDate()));
        displaydate = dateFormatDisplaying.format(date.getDate());
        day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", dateString));
        monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", dateString));
        year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", dateString));
        showlist(day, monthno, year);


    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {


        String t = Calendar.getInstance().getTime().toString();
        SimpleDateFormat sdf3 = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
        Date d1 = null;
        try {
            d1 = sdf3.parse(t);

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("check..." + d1);
        Log.i("shravan", "d1 - - -" + d1);

        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

       /* if (year != date.getYear() && month != date.getMonth() && day != date.getDay()) {


        }*/
        if (year == date.getYear() && month == date.getMonth() && day == date.getDay()) {
            compactCalendarView.setDateSelected(Calendar.getInstance(), true);
            compactCalendarView.setDateSelected(date.getCalendar(), false);
        } else {
            compactCalendarView.setDateSelected(date.getCalendar(), true);
        }


        toolbar_title.setText(dateFormatForMonth.format(date.getDate()));
        displaydate = dateFormatDisplaying.format(date.getDate());
//                String date = (dateFormat.format(firstDayOfNewMonth));
        SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String[] date2 = dateFormatForMonth.format(compactCalendarView.getFirstDayOfWeek()).split("-");
        showlist(Integer.parseInt(date2[0]), Integer.parseInt(date2[1]), Integer.parseInt(date2[2]));


    }


    public void showlist(int day, int monthno, int year) {
        leadModels.clear();
        sections.clear();
        leadsRecyclerView.setAdapter(null);
        //leadModels = LeadMgr.getInstance(PSFCalenderNewActivity.this).getPSFLeadListByDate(userid, day, monthno, year);


        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

            if (teammemberuseridlist != null && teammemberuseridlist.size()>0){
                leadModels = LeadMgr.getInstance(PSFCalenderNewActivity.this).getPSFAppointmentListBM(userid, String.valueOf(day), String.valueOf(monthno), String.valueOf(year),teammemberuseridlist);
            }else {
                showToast("Please choose a User.");
            }

            if (teammemberuseridlist != null && teammemberuseridlist.size()>0){
                List<PSFLeadModel> childList = new ArrayList<>();
                List<PSFLeadModel> newDistList = new ArrayList<>();




                newDistList = LeadMgr.getInstance(PSFCalenderNewActivity.this).getPSFdistinctNamesList(userid, String.valueOf(day), String.valueOf(monthno), String.valueOf(year),teammemberuseridlist);

                for (PSFLeadModel abc : newDistList) {

                    childList = new ArrayList<>();
                    for (PSFLeadModel longlist : leadModels) {

                        if (abc.getEmpName().equalsIgnoreCase(longlist.getEmpName())) {

                            childList.add(longlist);

                        }

                    }

                    Log.i("shravan", "childList = = " + childList.size());

                    //sections = new ArrayList<>();
                    if (childList.size() > 0) {

                        sections.add(new CalendarSectionHeader(childList, abc.getEmpType() + " - " + abc.getEmpName()));
                    }

                }

                Log.i("shravan", "sections====" + sections.size());
            }else {
                showToast("Please choose a User.");
            }






            if (sections.size() > 0) {
                leadsRecyclerView.setVisibility(View.VISIBLE);
                nomessageview.setVisibility(View.INVISIBLE);
                msgListProgressBar.setVisibility(View.INVISIBLE);
                bmcalenderAdapter = new AdapterSectionRecycler(this, sections,selectedTeamId);
                leadsRecyclerView.setAdapter(bmcalenderAdapter);
                leadsRecyclerView.setLayoutManager(new LinearLayoutManager(PSFCalenderNewActivity.this));
            } else {
                nomessageview.setText("No Leads");
                nomessageview.setVisibility(View.VISIBLE);
                leadsRecyclerView.setVisibility(View.GONE);
            }

        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
            leadModels = LeadMgr.getInstance(PSFCalenderNewActivity.this).getPSFAppointmentList(userid, String.valueOf(day), String.valueOf(monthno), String.valueOf(year));
            if (leadModels.size() > 0) {
                leadsRecyclerView.setVisibility(View.VISIBLE);
                nomessageview.setVisibility(View.INVISIBLE);
                msgListProgressBar.setVisibility(View.INVISIBLE);
                List<String> dummylist = new ArrayList<>();
                flscalenderAdapter = new PSFLeadsAdapter(leadModels, PSFCalenderNewActivity.this, "", "",dummylist,"","","","");
                leadsRecyclerView.setAdapter(flscalenderAdapter);
                leadsRecyclerView.setLayoutManager(new LinearLayoutManager(PSFCalenderNewActivity.this));
            } else {
                nomessageview.setText("No Leads");
                nomessageview.setVisibility(View.VISIBLE);
                leadsRecyclerView.setVisibility(View.GONE);
            }

        }

    }

    @Override
    public void onBackPressed() {
    finish();
    }


    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        textCartItemCount = (TextView) findViewById(R.id.notification_badge);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }




        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showToast("Notification Clicked!!!");
                Intent in = new Intent(PSFCalenderNewActivity.this, NotificationsListActivity.class);
                startActivity(in);
            }
        });

        setupBadge();
    }


    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }



    @Override
    protected void onStart() {
        super.onStart();

        if (isNetworkAvailable()) {
            updateNotification();
        } else {
            List<PSFNotificationsModel> finalList = new ArrayList<>();
            List<PSFNotificationsModel> psfNotificationsModelList = LeadMgr.getInstance(PSFCalenderNewActivity.this).getPSFNotificationsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));

            if (psfNotificationsModelList.size() > 0) {

                for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                    if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                        finalList.add(notificationsModel);
                    }

                }

                mCartItemCount = finalList.size();
                setupBadge();
            } else {
                mCartItemCount = 0;
                setupBadge();
            }

        }

    }


    private void updateNotification() {
        //showProgressDialog("Refreshing data . . .");
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getProposalCodes = client.getPsfNotifications(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        getProposalCodes.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFNotificationsModel>>() {
                        }.getType();
                        List<PSFNotificationsModel> psfNotificationsModelList = new Gson().fromJson(response.body().getData(), listType);
                        List<PSFNotificationsModel> finalList = new ArrayList<>();
                        if (psfNotificationsModelList.size() > 0) {
                            for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                                if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                                    finalList.add(notificationsModel);
                                }

                            }

                            mCartItemCount = finalList.size();
                            setupBadge();
                            //dismissProgressDialog();

                        } else {
                            mCartItemCount = 0;
                            setupBadge();
                            //dismissProgressDialog();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                //dismissProgressDialog();
            }
        });


    }



    private void showTeamDialog() {


        final Dialog dialog = new Dialog(this);

        dialog.setTitle("Select Team");

        View view = getLayoutInflater().inflate(R.layout.team_selection_dialog, null);

        ListView lv = (ListView) view.findViewById(R.id.memberList);
        UserTeamNamesAdapter clad = new UserTeamNamesAdapter(PSFCalenderNewActivity.this, userTeamsList, groupId, userid);
        lv.setAdapter(clad);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                userteamObj = (UserTeams) parent.getItemAtPosition(position);
                if (isNetworkAvailable()) {
                    selectedTeamId = userteamObj.getTeamid().toString();
                    getTeamHierarchyMembers(userteamObj.getTeamid().toString());
                    dialog.dismiss();

                } else {

                    showSimpleAlert("", "Oops...No Internet connection.");
                }


            }
        });

        dialog.setContentView(view);

        dialog.show();

    }


    private void getTeamHierarchyMembers(String teamid) {
        showProgressDialog("Loading, please wait....");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, teamid);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {
                        teamMembersHeirarchyArrayList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                        }.getType());
                        if (Utils.isNotNullAndNotEmpty(teamMembersHeirarchyArrayList)) {

                            teammemberuseridlist = new ArrayList<>();
                            showteammemberuseridlist  = new ArrayList<>();

                            //teammemberuseridlist.clear();

                            for (Userprofile up : teamMembersHeirarchyArrayList) {
                                teammemberuseridlist.add(up.getUserid());

                                if (!(up.getUserid().equalsIgnoreCase(SharedPreferenceManager.getInstance().getString(Constants.USERID, "")))){

                                    showteammemberuseridlist.add(up.getUserid());
                                }

                            }

                            showlist(day, monthno, year);
                            //List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFCalenderNewActivity.this).getPSFLeadsListforBM(userid, teammemberuseridlist);





                        } else {
                            showToast("No SO available");

                        }
                    }
                } else {
                    dismissProgressDialog();
                    showToast("No SO available");

                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                dismissProgressDialog();
                showToast("No SO available");

            }
        });
    }

}

