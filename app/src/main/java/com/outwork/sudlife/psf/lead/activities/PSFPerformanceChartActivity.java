package com.outwork.sudlife.psf.lead.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.services.CustomerService;
import com.outwork.sudlife.psf.lead.LeadMgr;
import com.outwork.sudlife.psf.lead.adapters.FlsAdapter;
import com.outwork.sudlife.psf.lead.model.MyValueFormatter;
import com.outwork.sudlife.psf.lead.model.PSFFlsModel;
import com.outwork.sudlife.psf.lead.model.PSFNotificationsModel;
import com.outwork.sudlife.psf.lead.model.PSFPerformanceModel;
import com.outwork.sudlife.psf.lead.services.LeadIntentService;
import com.outwork.sudlife.psf.localbase.StaticDataDto;
import com.outwork.sudlife.psf.localbase.StaticDataMgr;
import com.outwork.sudlife.psf.opportunity.services.FormsService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.adapter.UserTeamNamesAdapter;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.activities.DashBoardActivity;
import com.outwork.sudlife.psf.ui.activities.NotificationsListActivity;
import com.outwork.sudlife.psf.ui.models.UserTeams;
import com.outwork.sudlife.psf.ui.models.Userprofile;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shravanch on 26-12-2019.
 */

public class PSFPerformanceChartActivity extends BaseActivity {

    TextView textCartItemCount;
    int mCartItemCount;
    ArrayList<BarEntry> datavalues = new ArrayList<>();
    ArrayList<String> xAxisData = new ArrayList<>();
    ArrayList<BarDataSet> finaldataSets = null;
    int day, monthno, year;
    private AppCompatButton ftd, mtd, ytd;
    private TextView selectedtab, tv_nodata;
    private LinearLayout nodata_layout, chart_layout;
    private HorizontalBarChart chart;
    private ImageView filter;
    private ArrayList<String> stringArrayList;
    private EditText startdate, enddate;
    private Button submit;
    private String sdate, edate;
    //private ArrayList<Userprofile> flsList;
    private List<UserTeams> userTeamsList = new ArrayList<>();
    private UserTeams userteamObj;
    private String finalTeamId = null;
    private ArrayList<Userprofile> flsList = new ArrayList();
    private ArrayList<Userprofile> showflsList = new ArrayList();
    private String flmuserid = null;
    private String flsid = "";


    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                startdate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                sdate = TimeUtils.convertDatetoUnix(visitdt);
                startdate.setText(visitdt);
                startdate.setEnabled(true);
                startdate.setError(null);
            }
            if (customSelector == 1) {
                enddate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                edate = TimeUtils.convertDatetoUnix(visitdt);
                enddate.setText(visitdt);
                enddate.setEnabled(true);
                enddate.setError(null);
            }
        }

        @Override
        public void onDateTimeCancel() {
            startdate.setEnabled(true);
            enddate.setEnabled(true);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psf_performace_charts);

        String currentdate = TimeUtils.getCurrentDate("yyyy-MM-dd");
        day = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "dd", currentdate));
        monthno = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "MM", currentdate));
        year = Integer.parseInt(Utils.formatDateFromString("yyyy-MM-dd", "yyyy", currentdate));

        initToolBar();
        initializeViews();
        setListners();
        if (isNetworkAvailable()) {

            if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                userTeamsList = PlannerMgr.getInstance(PSFPerformanceChartActivity.this).getUserTeamList(userid);
                Log.i("shravan", "userTeamsList calendar size = = = " + userTeamsList.size());

                if (userTeamsList.size() > 1) {

                    showUserTeamsDialog();

                } else {

                    for (UserTeams us : userTeamsList) {


                        if (isNetworkAvailable()) {
                            finalTeamId = us.getTeamid().toString();
                            //getTeamHierarchyMembers(us.getTeamid().toString(),"");
                            getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), selectedtab.getText().toString(), "FTD", "", "","",finalTeamId,"");

                        } else {

                            showSimpleAlert("", "Oops...No Internet connection.");
                        }


                    }


                }



            }else   if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
                getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), "FTD", "", "", "","","","");
            }






        } else {
            showToast("No Internet");
        }
     /*   HorizontalBarChart chart = (HorizontalBarChart) findViewById(R.id.chart);
        BarData data = new BarData(getXAxisValues(), getDataSet());
        chart.setData(data);
        chart.setDescription(null);
        //chart.getDescription().setEnabled(false);


        chart.animateXY(500, 500);
        chart.getXAxis().setDrawGridLines(false);

        //chart.getXAxis().setEnabled(false);
        chart.getAxisLeft().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        //chart.getLeftAxis().setDrawGridLines(false);

        chart.getAxisLeft().setDrawLabels(false);
        chart.getAxisRight().setDrawLabels(false);
        // chart.getXAxis().setDrawLabels(false);

        chart.getLegend().setEnabled(false);
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.invalidate();*/
    }

  /*  private ArrayList<BarDataSet> getDataSet() {
        ArrayList<BarDataSet> dataSets = null;
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(110.000f, 0);
        valueSet1.add(v1e1);
        BarEntry v1e2 = new BarEntry(40.000f, 1);
        valueSet1.add(v1e2);
        BarEntry v1e3 = new BarEntry(60.000f, 2);
        valueSet1.add(v1e3);
        BarEntry v1e4 = new BarEntry(30.000f, 3);
        valueSet1.add(v1e4);
        BarEntry v1e5 = new BarEntry(90.000f, 4);
        valueSet1.add(v1e5);
        BarEntry v1e6 = new BarEntry(100.000f, 5);
        valueSet1.add(v1e6);

        BarEntry v1e7 = new BarEntry(100.000f, 6);
        valueSet1.add(v1e7);

        BarEntry v1e8 = new BarEntry(100.000f, 7);
        valueSet1.add(v1e8);
        *//*ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        BarEntry v2e1 = new BarEntry(150.000f, 0); // Jan
        valueSet2.add(v2e1);
        BarEntry v2e2 = new BarEntry(90.000f, 1); // Feb
        valueSet2.add(v2e2);
        BarEntry v2e3 = new BarEntry(120.000f, 2); // Mar
        valueSet2.add(v2e3);
        BarEntry v2e4 = new BarEntry(60.000f, 3); // Apr
        valueSet2.add(v2e4);
        BarEntry v2e5 = new BarEntry(20.000f, 4); // May
        valueSet2.add(v2e5);
        BarEntry v2e6 = new BarEntry(80.000f, 5); // Jun
        valueSet2.add(v2e6);*//*
        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "");
        //barDataSet1.setColor(Color.rgb(0, 155, 0));

        barDataSet1.setColor(getResources().getColor(R.color.toolbarColor));
       *//* BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Brand 2");
        barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);*//*
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        //dataSets.add(barDataSet2);
        return dataSets;
    }*/

/*    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("TOTAL");
        xAxis.add("ISSUED");
        xAxis.add("CONVERTED");
        xAxis.add("FOLLOW-UP");
        xAxis.add("PROP PRESENTED");
        xAxis.add("PRE FIX APPMT");
        xAxis.add("OPEN");
        xAxis.add("NOT INTERESTED");
        return xAxis;
    }*/


    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("PERFORMANCE REPORT");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }


        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        textCartItemCount = (TextView) findViewById(R.id.notification_badge);


        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showToast("Notification Clicked!!!");
                Intent in = new Intent(PSFPerformanceChartActivity.this, NotificationsListActivity.class);
                startActivity(in);
            }
        });

        setupBadge();
    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    private void initializeViews() {
        chart = (HorizontalBarChart) findViewById(R.id.chart);
        ftd = (AppCompatButton) findViewById(R.id.ftd);
        mtd = (AppCompatButton) findViewById(R.id.mtd);
        ytd = (AppCompatButton) findViewById(R.id.ytd);
        selectedtab = (TextView) findViewById(R.id.selectedtab);
        tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        selectedtab.setText("FTD");
        tv_nodata.setText("No Data Available.");
        chart_layout = (LinearLayout) findViewById(R.id.chart_layout);
        nodata_layout = (LinearLayout) findViewById(R.id.nodata_layout);
        chart_layout.setVisibility(View.VISIBLE);
        nodata_layout.setVisibility(View.GONE);
        filter = (ImageView) findViewById(R.id.filter);


    }


    private void setListners() {

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ArrayList<String> filterlist = new ArrayList<>();
                filterlist.add("Date Wise");
                filterlist.add("Campaign Wise");
                filterlist.add("Source Wise- Self or Allocated");
                if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
                    filterlist.add("Campaign Wise + FLM Wise");
                    filterlist.add("FLM Wise");
                }

                bottomSheetFilterDialog(filterlist, "Select Filter");


            }
        });

        ftd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkAvailable()) {
                    selectedtab.setText("FTD");
                    getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), "FTD", "", "", "","","","");
                } else {
                    showToast("No Internet");
                }

            }
        });

        mtd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkAvailable()) {
                    selectedtab.setText("MTD");
                    getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), "MTD", "", "", "","","","");
                } else {
                    showToast("No Internet");
                }

            }
        });

        ytd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkAvailable()) {
                    selectedtab.setText("YTD");
                    getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), "YTD", "", "", "","","","");
                } else {
                    showToast("No Internet");
                }

            }
        });


    }


    private void getPSFPerformanceReport(String day, String month, String year, String type, String leadsource, String startdate, String enddate,String flsuserid,String teamId,String filtertype) {
        chart_layout.setVisibility(View.VISIBLE);
        chart.setVisibility(View.VISIBLE);
        nodata_layout.setVisibility(View.GONE);

        String assigntoid = "";
        String bmuserid = "";

        if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {
            bmuserid = userid;
            if (Utils.isNotNullAndNotEmpty(flsuserid)){
                assigntoid = flsuserid;
            }else{
                assigntoid = "";
            }


        } else if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 4) {
            assigntoid = userid;
            bmuserid = "";
        }

        showProgressDialog("Loading, please wait....");

        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getperfreport = client.getPerformanceReport(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), assigntoid, bmuserid, day, month, year, type, (SharedPreferenceManager.getInstance().getString(Constants.EMPLOYEETYPE, "")), leadsource, startdate, enddate,finalTeamId,filtertype);
        getperfreport.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        datavalues.clear();
                        xAxisData.clear();
//                        finaldataSets.clear();
                        dismissProgressDialog();


                        Type listType = new TypeToken<ArrayList<PSFPerformanceModel>>() {
                        }.getType();
                        List<PSFPerformanceModel> psfPerformanceModels = new Gson().fromJson(response.body().getData(), listType);
                        if (psfPerformanceModels.size() > 0) {


                            int index = 0;

                            for (PSFPerformanceModel p : psfPerformanceModels) {
                                //String count = p.getLeadcount();
                                BarEntry v1e1 = new BarEntry(Float.parseFloat(p.getLeadcount()), index);
                                datavalues.add(v1e1);

                                xAxisData.add(p.getLeadstage());
                                index++;

                            }


                            BarDataSet barDataSet1 = new BarDataSet(datavalues, "");
                            barDataSet1.setValueFormatter(new MyValueFormatter());
                            //barDataSet1.setColor(Color.rgb(0, 155, 0));

                            barDataSet1.setColor(getResources().getColor(R.color.toolbarColor));
       /* BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Brand 2");
        barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);*/
                            finaldataSets = new ArrayList<>();
                            finaldataSets.add(barDataSet1);


                            BarData data = new BarData(xAxisData, finaldataSets);
                            data.setValueFormatter(new MyValueFormatter());
                            chart.setData(data);
                            chart.setClickable(false);
                            chart.setTouchEnabled(false);
                            chart.setPinchZoom(false);
                            chart.setDoubleTapToZoomEnabled(false);
                            chart.setScaleEnabled(false);
                            chart.setDescription(null);
                            //chart.getDescription().setEnabled(false);

                            chart.animateXY(300, 300);
                            chart.getXAxis().setDrawGridLines(false);
                            data.setValueTextSize(15f);
                            //chart.getXAxis().setEnabled(false);
                            chart.getAxisLeft().setEnabled(false);
                            chart.getAxisRight().setEnabled(false);
                            //chart.getLeftAxis().setDrawGridLines(false);
                            chart.getAxisLeft().setDrawLabels(false);
                            chart.getAxisRight().setDrawLabels(false);
                            //chart.setBackground(getResources().getColor(R.color.white));
                            // chart.getXAxis().setDrawLabels(false);

                            chart.getLegend().setEnabled(false);
                            XAxis xAxis = chart.getXAxis();
                            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                            xAxis.setTextSize(11);
                            xAxis.setAvoidFirstLastClipping(false);
                            chart.invalidate();


                        }


                   /*     Type listType = new TypeToken<ArrayList<PSFNotificationsModel>>() {
                        }.getType();
                        List<PSFNotificationsModel>psfNotificationsModelList = new Gson().fromJson(response.body().getData(), listType);
                        if (psfNotificationsModelList.size() > 0) {

                            LeadMgr.getInstance(LeadIntentService.this).insertPSFNotificationsList(psfNotificationsModelList, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
                        }
                        SharedPreferenceManager.getInstance().putString(Constants.PSF_NOTIFICATIONS_LOADED, "loaded");
                        Intent intent = new Intent("psfnotifications_broadcast");
                        mgr = LocalBroadcastManager.getInstance(LeadIntentService.this);
                        mgr.sendBroadcast(intent);*/
                    } else {

                        dismissProgressDialog();
                        chart_layout.setVisibility(View.GONE);
                        chart.setVisibility(View.GONE);
                        nodata_layout.setVisibility(View.VISIBLE);
                        tv_nodata.setText("Data not available.");
                        //showToast("Data not available.");


                    }
                } else {

                    dismissProgressDialog();
                    chart_layout.setVisibility(View.GONE);
                    chart.setVisibility(View.GONE);
                    nodata_layout.setVisibility(View.VISIBLE);
                    tv_nodata.setText("Something went wrong. Please try later.");
                    //showToast("Something went wrong. Please try later.");
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                dismissProgressDialog();
                chart_layout.setVisibility(View.GONE);
                chart.setVisibility(View.GONE);
                nodata_layout.setVisibility(View.VISIBLE);
                tv_nodata.setText("Server Error.");
                //showToast("Server Error.");
                //SharedPreferenceManager.getInstance().putString(Constants.PSF_NOTIFICATIONS_LOADED, "notloaded");
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();

        if (isNetworkAvailable()) {
            updateNotification();
        } else {
            List<PSFNotificationsModel> finalList = new ArrayList<>();
            List<PSFNotificationsModel> psfNotificationsModelList = LeadMgr.getInstance(PSFPerformanceChartActivity.this).getPSFNotificationsList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));

            if (psfNotificationsModelList.size() > 0) {

                for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                    if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                        finalList.add(notificationsModel);
                    }

                }

                mCartItemCount = finalList.size();
                setupBadge();
            } else {
                mCartItemCount = 0;
                setupBadge();
            }

        }

    }

    private void updateNotification() {
        //showProgressDialog("Refreshing data . . .");
        FormsService client = RestService.createServicev1(FormsService.class);
        Call<RestResponse> getProposalCodes = client.getPsfNotifications(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""));
        getProposalCodes.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PSFNotificationsModel>>() {
                        }.getType();
                        List<PSFNotificationsModel> psfNotificationsModelList = new Gson().fromJson(response.body().getData(), listType);
                        List<PSFNotificationsModel> finalList = new ArrayList<>();
                        if (psfNotificationsModelList.size() > 0) {
                            for (final PSFNotificationsModel notificationsModel : psfNotificationsModelList) {

                                if (Utils.isNotNullAndNotEmpty(notificationsModel.getNotifyid()) && !notificationsModel.getIsread()) {
                                    finalList.add(notificationsModel);
                                }

                            }

                            mCartItemCount = finalList.size();
                            setupBadge();
                            //dismissProgressDialog();

                        } else {
                            mCartItemCount = 0;
                            setupBadge();
                            //dismissProgressDialog();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                t.printStackTrace();
                //dismissProgressDialog();
            }
        });


    }


    public void bottomSheetFilterDialog(ArrayList<String> strings, String title) {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        ListView lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(PSFPerformanceChartActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(PSFPerformanceChartActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String selected = parent.getItemAtPosition(position).toString();

                if (selected.equalsIgnoreCase("Date Wise")) {

                    dateFilter();


                } else if (selected.equalsIgnoreCase("Campaign Wise")) {

                    List<StaticDataDto> staticList = new StaticDataMgr(PSFPerformanceChartActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "updateleadsource");

                    if (staticList.size() > 0) {

                        stringArrayList = new ArrayList<>();

                        for (StaticDataDto value : staticList) {

                            stringArrayList.add(value.getName());

                        }

                        bottomSheetDateWiseDialog(stringArrayList, "Select Campaign Wise");


                    }

                } else if (selected.equalsIgnoreCase("Campaign Wise + FLM Wise")) {


                    String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
                    CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
                    Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, finalTeamId);
                    responseCallGetTargets.enqueue(new Callback<RestResponse>() {
                        @Override
                        public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                            if (response.isSuccessful()) {
                    // dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                        Log.i("shravan", "response = = =" + response.body().toString());

                        flsList = new ArrayList<>();
                        showflsList = new ArrayList<>();
                        ArrayList<Userprofile> rawflsList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                        }.getType());
                        if (Utils.isNotNullAndNotEmpty(rawflsList)) {

                            for (Userprofile up : rawflsList) {

                                flsList.add(up);

                                if (!(up.getUserid().equalsIgnoreCase(SharedPreferenceManager.getInstance().getString(Constants.USERID, "")))) {

                                    showflsList.add(up);
                                }

                            }


                            final Dialog dialog = new Dialog(PSFPerformanceChartActivity.this);

                            //dialog.setTitle("Select FLS");

                            View view = (PSFPerformanceChartActivity.this).getLayoutInflater().inflate(R.layout.userteam_dialog, null);

                            ListView lv = (ListView) view.findViewById(R.id.memberList);
                            FlsAdapter clad = new FlsAdapter(PSFPerformanceChartActivity.this, showflsList);
                            lv.setAdapter(clad);

                            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    // TODO Auto-generated method stub

                                    Userprofile dto = (Userprofile) parent.getItemAtPosition(position);
                                    flmuserid = dto.getUserid();
                                    Log.i("shravan", "PSFLeadsAdapter  -- - flsuserid===" + flmuserid);

                                    if (SharedPreferenceManager.getInstance().getInt(Constants.USERLEVEL, 0) == 3) {

                                                       /* if (teammemberuseridlist != null && teammemberuseridlist.size() > 0) {
                                                            List<PSFLeadModel> leadModels = LeadMgr.getInstance(PSFLeadListActivity.this).getPSFFLMWiseListBM(userid, stage,flsuserid, teammemberuseridlist);
                                                            showList(leadModels, searchby, false);
                                                        } else {
                                                            // showToast("Please choose a User.");
                                                        }*/

                                        List<StaticDataDto> staticList = new StaticDataMgr(PSFPerformanceChartActivity.this).getStaticList(SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""), "updateleadsource");

                                        if (staticList.size() > 0) {

                                            stringArrayList = new ArrayList<>();

                                            for (StaticDataDto value : staticList) {

                                                stringArrayList.add(value.getName());

                                            }

                                            bottomSheetDateWiseDialog(stringArrayList, "Select Campaign Wise + FLM Wise");


                                        }


                                    }

                                    dialog.cancel();


                                }
                            });

                            dialog.setContentView(view);

                            dialog.show();


                        } else {
                            Toast.makeText(PSFPerformanceChartActivity.this, "FLS list empty!", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(PSFPerformanceChartActivity.this, "Data missing!", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(PSFPerformanceChartActivity.this, "Server error!", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

            }
        });










                }else if (selected.equalsIgnoreCase("Source Wise- Self or Allocated")) {

                    if (isNetworkAvailable()) {
                        getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), selectedtab.getText().toString(), "Reference/Self", "", "","","","");
                    } else {
                        showToast("No Internet");
                    }


                } else if (selected.equalsIgnoreCase("FLM Wise")) {
                    if (isNetworkAvailable()) {

                        //getTeamHierarchyMembers(SharedPreferenceManager.getInstance().getString(Constants.TEAMID, ""), "");
                        if (finalTeamId != null){

                            getTeamHierarchyMembers(finalTeamId, "");
                        }else {
                            showToast("Choose atleast one Team");
                        }

                        //showToast("To be enabled");
                    } else {
                        showToast("No Internet");
                    }


                }


                bottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }


    public void bottomSheetDateWiseDialog(ArrayList<String> strings, String title) {


        if (title.equalsIgnoreCase("Select Campaign Wise + FLM Wise")){
            flsid = flmuserid;

        }else {
            flsid = "";
        }
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottom_dialog_list_item, null);
        ListView lvBottom = (ListView) modalbottomsheet.findViewById(R.id.lv_bottom);
        TextView tvHeading = (TextView) modalbottomsheet.findViewById(R.id.tv_title_for_bottom_view);
        ImageView imgClose = (ImageView) modalbottomsheet.findViewById(R.id.img_close);
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(PSFPerformanceChartActivity.this);
        tvHeading.setText(title);
        bottomSheetDialog.setContentView(modalbottomsheet);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        ArrayAdapter adapter = new ArrayAdapter(PSFPerformanceChartActivity.this, android.R.layout.simple_list_item_1, strings);
        lvBottom.setAdapter(adapter);

        lvBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String selected = parent.getItemAtPosition(position).toString();

                if (isNetworkAvailable()) {
                    getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), selectedtab.getText().toString(), selected, "", "",flsid,"","");
                } else {
                    showToast("No Internet");
                }


                bottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
    }


    public void dateFilter() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PSFPerformanceChartActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.layout_dates_filter, null);

        alertDialog.setView(convertView);
        startdate = (EditText) convertView.findViewById(R.id.strtdate);
        enddate = (EditText) convertView.findViewById(R.id.enddate);
        submit = (Button) convertView.findViewById(R.id.submit);
        final AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.setTitle("Date Filter");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -7);
        startdate.setText(TimeUtils.getFormattedDate(calendar.getTime()));
        sdate = TimeUtils.convertDatetoUnix(TimeUtils.getFormattedDate(calendar.getTime()));
        enddate.setText(TimeUtils.getCurrentDate());
        edate = TimeUtils.convertDatetoUnix(TimeUtils.getCurrentDate());

        startdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startdate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -60);
                Date mindate = calendar.getTime();
                calendar.add(Calendar.DAY_OF_YEAR, -7);
                Date initdate = calendar.getTime();
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(initdate)
                        .setMinDate(mindate)
                        .setMaxDate(new Date())
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enddate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, -60);
                Date mindate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(mindate)
                        .setMaxDate(new Date())
                        .setCustomSelector(1)
                        .build()
                        .show();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNotNullAndNotEmpty(startdate.getText().toString())) {
                    startdate.setError("Date required");
                    startdate.requestFocus();
                } else if (!Utils.isNotNullAndNotEmpty(enddate.getText().toString())) {
                    enddate.setError("Date required");
                    enddate.requestFocus();
                } else {
                    if (Integer.parseInt(sdate) > Integer.parseInt(edate)) {
                        showToast("Startdate should not be greater than Endnddate");
                    } else {


                        String selectedstrtdate = TimeUtils.getFormattedDatefromUnix(sdate, "yyyy-MM-dd");
                        String selectedenddate = TimeUtils.getFormattedDatefromUnix(edate, "yyyy-MM-dd");

                        if (isNetworkAvailable()) {

                            selectedtab.setText("Custom Date");

                            getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), "customdate", "", selectedstrtdate, selectedenddate,"","","");
                        } else {
                            showToast("No Internet");
                        }


                        alert.cancel();
                    }
                }
            }
        });
        alert.show();
    }


    private void getTeamHierarchyMembers(String teamid, String leadid) {
        //showProgressDialog("Loading, please wait....");

        String userToken = SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "");
        CustomerService servcieForTragets = RestService.createServicev1(CustomerService.class);
        Call<RestResponse> responseCallGetTargets = servcieForTragets.getTeamHierarchyMembers(userToken, teamid);
        responseCallGetTargets.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    // dismissProgressDialog();
                    if (Utils.isNotNullAndNotEmpty(response.body().toString())) {

                        Log.i("shravan", "response = = =" + response.body().toString());

                        flsList = new ArrayList();
                       ArrayList<Userprofile> rawflsList = new Gson().fromJson(response.body().getData(), new TypeToken<ArrayList<Userprofile>>() {
                        }.getType());
                        if (Utils.isNotNullAndNotEmpty(rawflsList)) {

                            for (Userprofile up : rawflsList){

                                if (!(up.getUserid().equalsIgnoreCase(SharedPreferenceManager.getInstance().getString(Constants.USERID, "")))){


                                    flsList.add(up);


                                }


                            }




                            showDialog();


                        } else {
                            showToast("FLS list empty!");
                            //Toast.makeText(context, "FLS list empty!", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        showToast("Data missing!");
                        //Toast.makeText(context, "Data missing!", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    showToast("Server error!");
                    //Toast.makeText(context, "Server error!", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {

            }
        });
    }


    private void showDialog() {


        final Dialog dialog = new Dialog(PSFPerformanceChartActivity.this);

        //dialog.setTitle("Select FLS");

        View view = (PSFPerformanceChartActivity.this).getLayoutInflater().inflate(R.layout.userteam_dialog, null);

        ListView lv = (ListView) view.findViewById(R.id.memberList);
        FlsAdapter clad = new FlsAdapter(PSFPerformanceChartActivity.this, flsList);
        lv.setAdapter(clad);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                Userprofile dto = (Userprofile) parent.getItemAtPosition(position);
                String flsuserid = dto.getUserid();
               // getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), selectedtab.getText().toString(), selected, "", "");
                getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), selectedtab.getText().toString(), "", "", "",flsuserid,"","FLM Wise");

                dialog.cancel();



            }
        });

        dialog.setContentView(view);

        dialog.show();

    }




    private void showUserTeamsDialog() {


        final Dialog dialog = new Dialog(this);

        dialog.setTitle("Select Team");

        View view = getLayoutInflater().inflate(R.layout.team_selection_dialog, null);



        ListView lv = (ListView) view.findViewById(R.id.memberList);
        UserTeamNamesAdapter clad = new UserTeamNamesAdapter(PSFPerformanceChartActivity.this, userTeamsList, groupId, userid);
        lv.setAdapter(clad);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                userteamObj = (UserTeams) parent.getItemAtPosition(position);
                if (isNetworkAvailable()) {

                    finalTeamId = userteamObj.getTeamid().toString();

                    //getTeamHierarchyMembers(userteamObj.getTeamid().toString(),"");
                    getPSFPerformanceReport(String.valueOf(day), String.valueOf(monthno), String.valueOf(year), selectedtab.getText().toString(), "FTD", "", "","",finalTeamId,"");
                    dialog.dismiss();

                } else {

                    showSimpleAlert("", "Oops...No Internet connection.");
                }


            }
        });

        dialog.setContentView(view);

        dialog.show();

    }


}
