package com.outwork.sudlife.psf.lead.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.ui.activities.NotificationsListActivity;
import com.outwork.sudlife.psf.ui.activities.WebviewLibActivity;

/**
 * Created by shravanch on 03-01-2020.
 */

public class PSFFactFinderActivity extends BaseActivity {

    private TextView toolbar_title;
    private LinearLayout humanlife,childedu,childmarrige,retirement,salesportal,salesillustration;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psf_fact_finder);

        initToolBar();
        initializeViews();
        setListners();


    }

    private void initializeViews(){

        humanlife = (LinearLayout) findViewById(R.id.humanlife);
        childedu = (LinearLayout) findViewById(R.id.childedu);
        childmarrige = (LinearLayout) findViewById(R.id.childmarrige);
        retirement = (LinearLayout) findViewById(R.id.retirement);
        salesportal = (LinearLayout) findViewById(R.id.salesportal);
        salesillustration = (LinearLayout) findViewById(R.id.salesillustration);





    }

    private void setListners(){

        humanlife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()){
                    Intent intent = new Intent(getApplicationContext(), WebviewLibActivity.class);
                    //intent.putExtra("url", "https://www.sudlife.in/life-insurance-knowledge-center/life-insurance-calculators/human-life-value");  // old url
                    intent.putExtra("url", "https://www.sudlife.in/human-life-value-calculator");  // new url

                    startActivity(intent);

                }else {
                    showToast("No Internet! Please connect to internet.");
                }

            }
        });

        childedu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()){
                    Intent child_education_intent = new Intent(getApplicationContext(), WebviewLibActivity.class);
                    //child_education_intent.putExtra("url", "https://www.sudlife.in/life-insurance-knowledge-center/life-insurance-calculators/childs-education");// old url
                    child_education_intent.putExtra("url", "https://www.sudlife.in/child-education-expense-calculator"); // new url
                    startActivity(child_education_intent);

                }else {
                    showToast("No Internet! Please connect to internet.");
                }


            }
        });

        childmarrige.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()){
                    Intent child_marriage_intent = new Intent(getApplicationContext(), WebviewLibActivity.class);
                    //child_marriage_intent.putExtra("url", "https://www.sudlife.in/life-insurance-knowledge-center/life-insurance-calculators/childs-marriage"); // old url
                    child_marriage_intent.putExtra("url", "https://cp.sudlife.in/life-insurance-knowledge-center/life-insurance-calculators/childs-marriage"); // new url
                    startActivity(child_marriage_intent);

                }else {
                    showToast("No Internet! Please connect to internet.");
                }

            }
        });

        retirement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()){
                    Intent retirement_cal_intent = new Intent(getApplicationContext(), WebviewLibActivity.class);
                    //retirement_cal_intent.putExtra("url", "https://www.sudlife.in/life-insurance-knowledge-center/life-insurance-calculators/retirement"); // old url
                    retirement_cal_intent.putExtra("url", "https://www.sudlife.in/retirement-calculator"); // new url
                    startActivity(retirement_cal_intent);

                }else {
                    showToast("No Internet! Please connect to internet.");
                }

            }
        });

        salesportal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkAvailable()){
                    Intent sales_illustration_intent = new Intent(getApplicationContext(), WebviewLibActivity.class);
                    sales_illustration_intent.putExtra("url", "https://portal.sudlife.in/");
                    startActivity(sales_illustration_intent);

                }else {
                    showToast("No Internet! Please connect to internet.");
                }

            }
        });

        salesillustration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()){
                    Intent sales_illustration_intent = new Intent(getApplicationContext(), WebviewLibActivity.class);
                    sales_illustration_intent.putExtra("url", "http://si.sudlife.in/SalesIllustration/ProductPage.aspx");
                    startActivity(sales_illustration_intent);

                }else {
                    showToast("No Internet! Please connect to internet.");
                }


            }
        });

    }





    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Fact Finder");
        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        TextView textCartItemCount = (TextView) findViewById(R.id.notification_badge);
        notification.setVisibility(View.GONE);
        textCartItemCount.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }


    }


}
