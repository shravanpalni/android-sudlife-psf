package com.outwork.sudlife.psf.more.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.more.models.ChannelDto;

import java.util.ArrayList;
import java.util.List;

public class ChannelTypesAdapter extends BaseAdapter {
    private Context context;
    private final LayoutInflater mInflater;
    private List<ChannelDto> posttypeList = new ArrayList<ChannelDto>();

    static class ViewHolder {
        public TextView name;
        public ImageView picture;
    }

    public ChannelTypesAdapter(Context context, List<ChannelDto> postTypeList){

        this.context = context;
        this.posttypeList = postTypeList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.posttypeList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this.posttypeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @SuppressLint("ViewHolder") @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {


        View view = convertView;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item_categories, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView)view.findViewById(R.id.chTitle);
            viewHolder.picture = (ImageView)view.findViewById(R.id.channelImage);
            view.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) view.getTag();

        ChannelDto dto = (ChannelDto) this.posttypeList.get(position);

        if(dto.getChannelName().length()>0) {

//                Bitmap letterTile = tileProvider.getLetterTile(dto.getTitle(), dto.getTitle().substring(0,1), tileSize, tileSize);
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate color based on a key (same key returns the same color), useful for list/grid views
            int color2 = generator.getColor(dto.getChannelName().substring(0,1));
            TextDrawable drawable = TextDrawable.builder().buildRound(dto.getChannelName().substring(0,1), color2);

            if (drawable != null) {
                holder.picture.setImageDrawable(drawable);
            }
        }
        holder.name.setText(dto.getChannelName());
        return view;
    }
}
