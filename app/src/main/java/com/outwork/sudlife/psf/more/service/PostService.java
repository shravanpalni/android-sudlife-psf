package com.outwork.sudlife.psf.more.service;

import com.outwork.sudlife.psf.more.models.Msg;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PostService {

    @GET("groups/{groupid}/posts/admin")
    Call<RestResponse> getAdminPosts(@Header("utoken") String userToken,
                                     @Path("groupid") String groupid,
                                     @Query("type") String type,
                                     @Query("initialid") String initialid,
                                     @Query("count") String count,
                                     @Query("direction") String direction,
                                     @Query("date") String date);

    @GET("groups/{groupid}/posts/all")
    Call<RestResponse> getAllPosts  (@Header("utoken") String userToken,
                                     @Path("groupid") String groupid,
                                     @Query("type") String type,
                                     @Query("initialid") String initialid,
                                     @Query("count") String count,
                                     @Query("direction") String direction,
                                     @Query("date") String date);



    @GET("groups/{groupid}/posts/member")
    Call<RestResponse> getMemberPosts (@Header("utoken") String userToken,
                                       @Path("groupid") String groupid,
                                       @Query("type") String type,
                                       @Query("initialid") String initialid,
                                       @Query("count") String count,
                                       @Query("direction") String direction,
                                       @Query("date") String date);

    @GET("groups/{groupid}/posts/me")
    Call<RestResponse> getMyPosts    (@Header("utoken") String userToken,
                                      @Path("groupid") String groupid,
                                      @Query("type") String type,
                                      @Query("initialid") String initialid,
                                      @Query("count") String count,
                                      @Query("direction") String direction,
                                      @Query("date") String date);


    @POST("ChannelService/channels/posts")
    Call<RestResponse> postMessage(@Header("utoken") String userToken,
                                   @Query("groupid") String groupid,
                                   @Query("channelids") String channelids,
                                   @Body Msg sJsonBody);

    @GET("groups/{groupid}/channel/posts")
    Call<RestResponse> getChannelPosts (@Header("utoken") String userToken,
                                        @Path("groupid") String groupid,
                                        @Query("count") String count,
                                        @Query("initialid") String initialid,
                                        @Query("direction") String direction,
                                        @Query("date") String date);

    @GET("ChannelService/channels")
    Call<RestResponse> getChannels      (@Header("utoken") String userToken,
                                         @Query("groupid") String groupid);


}
