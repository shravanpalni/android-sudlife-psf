package com.outwork.sudlife.psf.branches.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.branches.activities.ViewContactActivity;
import com.outwork.sudlife.psf.branches.adapter.ContactListAdapter;
import com.outwork.sudlife.psf.branches.models.BranchesModel;
import com.outwork.sudlife.psf.branches.models.ContactModel;
import com.outwork.sudlife.psf.restinterfaces.POJO.GeoAddress;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Habi on 10-05-2018.
 */

public class BranchViewFragment extends Fragment {
    private String customerId, userId, groupId;
    private ContactListAdapter contactListAdapter;
    private ListView contactsListView;
    private List<ContactModel> contactList = new ArrayList<>();
    private TextView customerName, customerType, branchcode, addressline1, locality, city, phone, email, branchfoundationday;
    private BranchesModel branchesModel;

    public BranchViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_view_customer, container, false);

        groupId = SharedPreferenceManager.getInstance().getString(Constants.GROUPID, "");
        userId = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
        Bundle b = getActivity().getIntent().getExtras();
        if (b != null) {
            customerId = b.getString("customerid");
        }
        if (getActivity().getIntent().getExtras() != null && getActivity().getIntent().getExtras().containsKey("customer")) {
            branchesModel = new Gson().fromJson(getActivity().getIntent().getStringExtra("customer"), BranchesModel.class);
        }
        customerName = (TextView) view.findViewById(R.id.uName);
        customerType = (TextView) view.findViewById(R.id.uBusinessType);
        branchcode = (TextView) view.findViewById(R.id.branchcode);
        branchfoundationday = (TextView) view.findViewById(R.id.branchfoundationday);

        addressline1 = (TextView) view.findViewById(R.id.uaddressline1);
        locality = (TextView) view.findViewById(R.id.locality);
        city = (TextView) view.findViewById(R.id.city);
        phone = (TextView) view.findViewById(R.id.phone);
        email = (TextView) view.findViewById(R.id.email);
        contactsListView = (ListView) view.findViewById(R.id.memberList);

        initValues();
//        Utils.setTypefaces(IvokoApplication.robotoTypeface, customerName, customerType, locality, addressline1, branchcode, phone, email, city);
        Utils.setTypefaces(IvokoApplication.robotoLightTypeface, (TextView) view.findViewById(R.id.branchcodelbl),
                (TextView) view.findViewById(R.id.branchfoundationdaylbl), (TextView) view.findViewById(R.id.emaillbl),
                (TextView) view.findViewById(R.id.localitylbl), (TextView) view.findViewById(R.id.citylbl),
                (TextView) view.findViewById(R.id.phonelbl), (TextView) view.findViewById(R.id.uaddresslabel),
                (TextView) view.findViewById(R.id.uNamelbl), (TextView) view.findViewById(R.id.uBusinessTypelbl),
                customerName, customerType, locality, addressline1, branchcode, phone, email, city,branchfoundationday);
        return view;
    }

    private void initValues() {
//        BranchesModel branchesModel = null;
//        if (!Utils.isNotNullAndNotEmpty(customerId))
//            branchesModel = new ContactMgr(getActivity()).getContact(customerId);
        if (branchesModel != null) {

     /*       GeoAddress custAddress = branchesModel.getAddress();
            setAddressValues(custAddress);*/

            if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomername())) {
                customerName.setText(branchesModel.getCustomername());
            } else {
                customerName.setText(getResources().getString(R.string.notAvailable));
            }
            if (Utils.isNotNullAndNotEmpty(branchesModel.getCustomertype())) {
                customerType.setText(branchesModel.getCustomertype());
            } else {
                customerType.setText(getResources().getString(R.string.notAvailable));
            }
            if (Utils.isNotNullAndNotEmpty(branchesModel.getBranchcode())) {
                branchcode.setText(branchesModel.getBranchcode());
            } else {
                branchcode.setText(getResources().getString(R.string.notAvailable));
            }
            if (Utils.isNotNullAndNotEmpty(branchesModel.getAddressline1())) {
                addressline1.setText(branchesModel.getAddressline1());
            } else {
                addressline1.setText(getResources().getString(R.string.notAvailable));
            }

            if (Utils.isNotNullAndNotEmpty(branchesModel.getLocality())) {
                locality.setText(branchesModel.getLocality());
            } else {
                locality.setText(getResources().getString(R.string.notAvailable));
            }
            if (Utils.isNotNullAndNotEmpty(branchesModel.getCity())) {
                city.setText(branchesModel.getCity());
            } else {
                city.setText(getResources().getString(R.string.notAvailable));
            }
            if (Utils.isNotNullAndNotEmpty(branchesModel.getBranchfoundationday())) {
                branchfoundationday.setText(branchesModel.getBranchfoundationday());
            } else {
                branchfoundationday.setText(getResources().getString(R.string.notAvailable));
            }
            if (Utils.isNotNullAndNotEmpty(branchesModel.getPhoneno())) {
                phone.setText(branchesModel.getPhoneno());
            } else {
                phone.setText(getResources().getString(R.string.notAvailable));
            }
            if (Utils.isNotNullAndNotEmpty(branchesModel.getEmail())) {
                email.setText(branchesModel.getEmail());
            } else {
                email.setText(getResources().getString(R.string.notAvailable));
            }

        }
        contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                ContactModel branchesModel1 = (ContactModel) parent.getItemAtPosition(position);
                Intent in = new Intent(getActivity(), ViewContactActivity.class);
                in.putExtra("contact", branchesModel1.getAsJsonObject().toString());
                in.putExtra("contactid", branchesModel1.getContactid());
                startActivity(in);
            }
        });
    }

    private void setAddressValues(GeoAddress address) {
        if (address != null) {
            String fulladdress = "";
            if (Utils.isNotNullAndNotEmpty(address.getAddressline1())) {
                fulladdress = address.getAddressline1() + " ";
            }
            if (Utils.isNotNullAndNotEmpty(address.getAddressline2())) {
                fulladdress = fulladdress + address.getAddressline2() + " ";
            } else if (Utils.isNotNullAndNotEmpty(address.getLocality())) {
                fulladdress = fulladdress + address.getLocality() + " ";
            }
            if (Utils.isNotNullAndNotEmpty(address.getLandmark())) {
                fulladdress = fulladdress + address.getLandmark() + " ";
            }
            if (Utils.isNotNullAndNotEmpty(address.getCity())) {
                fulladdress = fulladdress + address.getCity();
            }
            if (Utils.isNotNullAndNotEmpty(fulladdress)) {
                addressline1.setText(fulladdress);
            } else {
                addressline1.setText(getResources().getString(R.string.notAvailable));
            }
        } else {
            addressline1.setText(getResources().getString(R.string.notAvailable));
        }
    }

//    private void initializeToolBar() {
//        Toolbar toolbar = (Toolbar) findViewById(R.id.vCustomerToolBar);
//        setSupportActionBar(toolbar);
//
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayShowTitleEnabled(true);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setTitle("");
//            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
//        }
//    }
}
