package com.outwork.sudlife.psf.planner.activities;

import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.core.LocationProvider;
import com.outwork.sudlife.psf.dto.Geocode;
import com.outwork.sudlife.psf.dto.geocode.STATUS;
import com.outwork.sudlife.psf.localbase.StaticDataMgr;
import com.outwork.sudlife.psf.opportunity.models.OpportunityModel;
import com.outwork.sudlife.psf.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.restinterfaces.ReverseGeoInterface;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateOpportuntiyPlanActivity extends BaseActivity implements LocationProvider.LocationCallback {

    private EditText vplandate, title;
    private TextView typelabel;
    private AppCompatSpinner activitytype;
    private String pvisitdate;
    private Button submit;
    private RadioGroup type;
    private RadioButton visit, activity;
    private ArrayAdapter<String> activityTypeAdapter;
    private PlannerModel plannerModel = new PlannerModel();
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private LocationProvider mLocationProvider;
    private double latitude, longitude;
    private String addressline1, addressline2, locality, city, state, zip, addressline = "";
    private OpportunityModel opportunityModel;

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
        }

        @Override
        public void onDateTimeSet(Date date, int customSelector) {
            if (customSelector == 0) {
                vplandate.setText(TimeUtils.getFormattedDate(date));
                String visitdt = TimeUtils.getFormattedDate(date);
                vplandate.setText(visitdt);
                vplandate.setEnabled(true);
                pvisitdate = TimeUtils.convertDatetoUnix(visitdt);
            }
        }

        @Override
        public void onDateTimeCancel() {
            vplandate.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunity_plan);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("opportunity")) {
            opportunityModel = new Gson().fromJson(getIntent().getStringExtra("opportunity"), OpportunityModel.class);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("date")) {
            pvisitdate = getIntent().getStringExtra("date");
        }
        initToolBar();
        initViews();
//        getActivitytypes();
        setListeners();
        mLocationProvider = new LocationProvider(CreateOpportuntiyPlanActivity.this, this);
        mLocationProvider.connect();
//        dispatchlocationIntent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mLocationProvider != null)
            mLocationProvider.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mLocationProvider != null)
            mLocationProvider.disconnect();
    }

    @Override
    public void handleNewLocation(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            if (Utils.isNotNullAndNotEmpty(String.valueOf(latitude)) && Utils.isNotNullAndNotEmpty(String.valueOf(longitude)))
                getLocation(latitude, longitude);
        }
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Create Opportunity Plan");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }

    private void initViews() {
        typelabel = (TextView) findViewById(R.id.atype);
        title = (EditText) findViewById(R.id.title);
        vplandate = (EditText) findViewById(R.id.vplandate);
        submit = (Button) findViewById(R.id.submit);
        type = (RadioGroup) findViewById(R.id.type);
        visit = (RadioButton) findViewById(R.id.visit);
        activity = (RadioButton) findViewById(R.id.activity);
//        activitytype = (AppCompatSpinner) findViewById(R.id.activitytype);
        visit.setChecked(true);
        plannerModel.setType("Visit");
        plannerModel.setActivitytype("Visit");
        vplandate.setText(TimeUtils.getFormattedDatefromUnix(pvisitdate, "dd/MM/yyyy hh:mm a"));
        if (visit.isChecked()) {
//            typelabel.setVisibility(View.GONE);
//            activitytype.setVisibility(View.GONE);
        }
    }

    private void getActivitytypes() {
        List<String> activitytypeList = StaticDataMgr.getInstance(CreateOpportuntiyPlanActivity.this).getListNames(groupId, "subtype");
        if (activitytypeList.size() > 0) {
            activityTypeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, activitytypeList);
            activityTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            activitytype.setAdapter(activityTypeAdapter);
            activitytype.setSelection(0);
            activitytype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    activitytype.setSelection(position);
                    plannerModel.setSubtype(activitytype.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } else {
            typelabel.setVisibility(View.GONE);
            activitytype.setVisibility(View.GONE);
        }
    }

    private void setListeners() {
        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.visit) {
                    plannerModel.setType("Visit");
                    plannerModel.setActivitytype("Visit");
//                    typelabel.setVisibility(View.GONE);
//                    activitytype.setVisibility(View.GONE);
                } else {
                    plannerModel.setType("Activity");
                    plannerModel.setActivitytype("Activity");
//                    typelabel.setVisibility(View.VISIBLE);
//                    activitytype.setVisibility(View.VISIBLE);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNotNullAndNotEmpty(title.getText().toString())) {
                    title.setError("Title is Mandatory");
                    title.requestFocus();
                } else if (!Utils.isNotNullAndNotEmpty(vplandate.getText().toString())) {
                    vplandate.setError("Date required");
                    vplandate.requestFocus();
                } else {
                    postcreateplan(0, "Your Plan is submitted successfully");
                }
            }
        });
        vplandate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vplandate.setEnabled(false);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, 10);
                Date backDate = calendar.getTime();

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .setMinDate(new Date())
                        .setMaxDate(backDate)
                        .setCustomSelector(0)
                        .build()
                        .show();
            }
        });
    }

    private void postcreateplan(int compltestatus, String msg) {
        plannerModel.setGroupid(groupId);
        plannerModel.setUserid(userid);
        plannerModel.setFirstname(opportunityModel.getFirstname());
        plannerModel.setLastname(opportunityModel.getLastname());
        plannerModel.setTitle(title.getText().toString());
        if (Utils.isNotNullAndNotEmpty(opportunityModel.getOpportunityid())) {
            plannerModel.setOpportunityid(opportunityModel.getOpportunityid());
            plannerModel.setLocalopportunityid(opportunityModel.getId());
        } else {
            plannerModel.setLocalopportunityid(opportunityModel.getId());
        }
        plannerModel.setScheduleday(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "dd", TimeUtils.getFormattedDatefromUnix(pvisitdate, "dd/MM/yyyy hh:mm a"))));
        plannerModel.setSchedulemonth(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "MM", TimeUtils.getFormattedDatefromUnix(pvisitdate, "dd/MM/yyyy hh:mm a"))));
        plannerModel.setScheduleyear(Integer.parseInt(Utils.formatDateFromString("dd/MM/yyyy hh:mm a", "yyyy", TimeUtils.getFormattedDatefromUnix(pvisitdate, "dd/MM/yyyy hh:mm a"))));
        plannerModel.setScheduletime(Integer.parseInt(pvisitdate));
        plannerModel.setPlanstatus(0);
        plannerModel.setCompletestatus(0);
        plannerModel.setNetwork_status("offline");
        PlannerMgr.getInstance(CreateOpportuntiyPlanActivity.this).insertPlanner(plannerModel, userid, groupId);
        List<PlannerModel> plannerModelArrayList = new ArrayList<>();
        plannerModelArrayList.add(plannerModel);
        PlannerMgr.getInstance(CreateOpportuntiyPlanActivity.this).insertPlannerNotificationList(plannerModelArrayList, userid, groupId);
        OpportunityIntentService.syncOfflineRecords(CreateOpportuntiyPlanActivity.this);
        showAlert("", "Your Plan is saved successfully",
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                },
                "",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }, false);
    }

    private void getLocation(double lat, double lng) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ReverseGeoInterface.MAP_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String latlong = lat + "," + lng;
        ReverseGeoInterface service = retrofit.create(ReverseGeoInterface.class);
        Call<Geocode> locationdetails = service.getLocation("GEOMETRIC_CENTER", latlong, "AIzaSyB6QdommPXh8xlvGJL9IkauaajCpKfNWpc");

        locationdetails.enqueue(new Callback<Geocode>() {
            @Override
            public void onResponse(Call<Geocode> call, Response<Geocode> response) {
                Geocode geocode = response.body();
                if (geocode != null)
                    if (geocode.getResults() != null)
                        if (geocode.getStatus() == STATUS.OK) {
                            if (geocode.getResults().size() > 0) {
                                if (geocode.getResults().get(0).getAddressComponents() != null) {
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(0).getLongName()))
                                        addressline1 = geocode.getResults().get(0).getAddressComponents().get(0).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(1).getLongName()))
                                        addressline2 = geocode.getResults().get(0).getAddressComponents().get(1).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(2).getLongName()))
                                        locality = geocode.getResults().get(0).getAddressComponents().get(2).getLongName();
                                    if (Utils.isNotNullAndNotEmpty(geocode.getResults().get(0).getAddressComponents().get(3).getLongName()))
                                        city = geocode.getResults().get(0).getAddressComponents().get(3).getLongName();
                                    addressline = addressline1 + " " + addressline2 + " " + locality + " " + city;
                                }
                            }
                        }
            }

            @Override
            public void onFailure(Call<Geocode> call, Throwable t) {
            }
        });
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        dismissProgressDialog();
//        switch (requestCode) {
//            case ACCESS_FINE_LOCATION_INTENT_ID: {
//                if (grantResults != null) {
//                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                        dispatchlocationIntent();
//                    } else {
//                        showToast(getResources().getString(R.string.locationtoast));
//                    }
//                } else {
//                    showToast(getResources().getString(R.string.locationtoast));
//                }
//                return;
//            }
//        }
//    }

}
