package com.outwork.sudlife.psf.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.outwork.sudlife.psf.IvokoApplication;
import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.planner.activities.CalenderActivity;
import com.outwork.sudlife.psf.planner.service.PlannerIntentService;
import com.outwork.sudlife.psf.restinterfaces.RestResponse;
import com.outwork.sudlife.psf.restinterfaces.RestService;
import com.outwork.sudlife.psf.restinterfaces.UserRestInterface;
import com.outwork.sudlife.psf.ui.BaseActivity;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.TimeUtils;
import com.outwork.sudlife.psf.utilities.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Panch on 2/2/2016.
 */
public class ResetPasswordActivity extends BaseActivity {
    public static final String TAG = ResetPasswordActivity.class.getSimpleName();

    private EditText current_pwd, new_pwd, confirm_pwd;
    private Button reset;
    private CheckBox currentpwdVisibility, newpwdvisibility, confirmpwdVisibility;
    private TextView toolbar_title;
    private String screenfrom;
    private String currentpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("screenfrom")) {
            screenfrom = getIntent().getStringExtra("screenfrom");
        }

        current_pwd = (EditText) findViewById(R.id.current_pwd_et);
        new_pwd = (EditText) findViewById(R.id.new_pwd_et);
        confirm_pwd = (EditText) findViewById(R.id.confirm_pwd_et);
        reset = (Button) findViewById(R.id.reset);
        currentpwdVisibility = (CheckBox) findViewById(R.id.currentpwdVisibility);
        newpwdvisibility = (CheckBox) findViewById(R.id.newpwdVisibility);
        confirmpwdVisibility = (CheckBox) findViewById(R.id.confirmpwdVisibility);

        initToolBar();
        Utils.setTypefaces(IvokoApplication.robotoTypeface, current_pwd, new_pwd, (TextView) findViewById(R.id.desc));
        Utils.setTypefaces(IvokoApplication.robotoBoldTypeface, reset, toolbar_title, (TextView) findViewById(R.id.desc));
        if (Utils.isNotNullAndNotEmpty(screenfrom)) {
            if (screenfrom.equals("Signin")) {
                findViewById(R.id.currentpasswordview).setVisibility(View.GONE);
            } else {
                findViewById(R.id.currentpasswordview).setVisibility(View.VISIBLE);
            }
        } else {
            findViewById(R.id.currentpasswordview).setVisibility(View.VISIBLE);
        }
        currentpwdVisibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    current_pwd.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    current_pwd.setInputType(129);
                }
            }
        });
        newpwdvisibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    new_pwd.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    new_pwd.setInputType(129);
                }
            }
        });
        confirmpwdVisibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    confirm_pwd.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    confirm_pwd.setInputType(129);
                }
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_pwd.getText().toString().isEmpty() && !Utils.isNotNullAndNotEmpty(screenfrom)) {
                    current_pwd.setError("Required");
                    current_pwd.requestFocus();
                } else if (new_pwd.getText().toString().isEmpty()) {
                    new_pwd.setError("Required new password");
                    new_pwd.requestFocus();
                } else if (confirm_pwd.getText().toString().isEmpty()) {
                    confirm_pwd.setError("Confirm your new password");
                    confirm_pwd.requestFocus();
                } else if (!confirm_pwd.getText().toString().equals(new_pwd.getText().toString())) {
                    confirm_pwd.setError("Your password and confirmation password do not match");
                    confirm_pwd.requestFocus();
                } else {
                    if (isNetworkAvailable()) {
                        showProgressDialog("please wait..");
                        UserRestInterface client = RestService.createServicev1(UserRestInterface.class);
                        if (Utils.isNotNullAndNotEmpty(screenfrom)) {
                            if (screenfrom.equals("Signin")) {
                                currentpassword = SharedPreferenceManager.getInstance().getString(Constants.password, "");
                            } else {
                                currentpassword = current_pwd.getText().toString();
                            }
                        } else {
                            currentpassword = current_pwd.getText().toString();
                        }
                        Call<RestResponse> resetPassword = client.resetPassword(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""),
                                currentpassword,
                                confirm_pwd.getText().toString());
                        resetPassword.enqueue(new Callback<RestResponse>() {
                            @Override
                            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                                dismissProgressDialog();
                                if (response.isSuccessful()) {
                                    if (response.body().getStatus().equalsIgnoreCase(Constants.success)) {
                                        if (Utils.isNotNullAndNotEmpty(response.body().getData())) {
                                            showAlert("", "Your Password has been changed successfully", "Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (Utils.isNotNullAndNotEmpty(screenfrom)) {
                                                        if (screenfrom.equalsIgnoreCase("Signin")) {
                                                            List<String> stringList = new ArrayList<>();
                                                            stringList.add(Utils.getPreviousMonthDate(new Date()));
                                                            stringList.add(TimeUtils.getCurrentDate("MM-yyyy"));
                                                            stringList.add(Utils.getNextMonthDate(new Date()));
                                                            for (int i = 0; i < stringList.size(); i++) {
                                                                String[] date = stringList.get(i).split("-");
                                                                PlannerIntentService.insertPlanList(ResetPasswordActivity.this, date[0], date[1]);
                                                            }
                                                            SharedPreferenceManager.getInstance().putString(Constants.LOGININTIME, TimeUtils.getCurrentUnixTimeStamp());
                                                            Intent in = new Intent(ResetPasswordActivity.this, DashBoardActivity.class);
                                                            startActivity(in);
                                                            finish();
                                                        }
                                                    } else {
                                                        finish();
                                                    }
                                                }
                                            }, "", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            }, false);
                                        }
                                    } else {
                                        showAlert("", "Your Password reset was not successfull. Please try again", "Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                            }
                                        }, "", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                            }
                                        }, false);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<RestResponse> call, Throwable t) {
                                dismissProgressDialog();
                                showAlert("", "Your Password reset was . Please try again", "Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }, "", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }, false);
                            }
                        });
                    } else {
                        dismissProgressDialog();
                        showToast("Please Check Internet Connection...");
                    }
                }
            }
        });
    }

    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.ftoolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Reset Password");
        ImageView notification = (ImageView) findViewById(R.id.notification_iv);
        notification.setVisibility(View.GONE);
        TextView  textCartItemCount = (TextView) findViewById(R.id.notification_badge);
        textCartItemCount.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }
    }
}