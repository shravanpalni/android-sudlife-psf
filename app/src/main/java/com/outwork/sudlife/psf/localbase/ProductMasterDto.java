package com.outwork.sudlife.psf.localbase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.outwork.sudlife.psf.lead.model.CategoryModel;

/**
 * Created by Panch on 10/5/2015.
 */
public class ProductMasterDto {

    private int id;

    @SerializedName("productid")
    @Expose
    private String productId;
    @SerializedName("productname")
    @Expose
    private String productName;
    private String groupId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }

    @SerializedName("category")
    @Expose
    private CategoryModel category;

}