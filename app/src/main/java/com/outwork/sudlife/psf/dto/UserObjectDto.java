package com.outwork.sudlife.psf.dto;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import com.outwork.sudlife.psf.utilities.Utils;


public class UserObjectDto implements Serializable {

    public static final String JSON_USERTOKEN = "usertoken";
    public static final String JSON_LOGINTYPE = "logintype";
    public static final String JSON_LOGINTIME = "logintime";
    public static final String JSON_LOGINID = "loginID";
    public static final String JSON_USERID = "userid";
    public static final String JSON_EMAIL = "email";
    public static final String JSON_USERTYPE = "objecttype";
    public static final String JSON_GROUPINFO = "groupinfo";
    public static final String JSON_GROUPCOUNT = "groupcount";
    public static final String JSON_USERPROFILE = "userprofile";
    public static final String JSON_LOCDATA = "locdata";
    public static final String JSON_DEVICEINFO = "deviceinfo";
    public static final String JSON_DESC = "description";
    public static final String JSON_HELPURL = "helpURL";
    public static final String JSON_MESSAGE = "message";
    public static final String JSON_STATUS = "status";
    public static final String JSON_GCMSUBSCRIPTION = "gcmsubscription";

    private UserDetailsDto userDto;
    private String email;
    private String userToken;
    private String userId;
    private String title;
    private String firstName;
    private String lastName;
    private String groupId;
    private String groupCode;
    private String isHealthcare;
    private String groupImage;
    private String groupTitle;
    private String userType;
    private String groupIcon;
    private String loginId;
    private String loginType;
    private String lastlogintime;
    private LocationDto defaultLocation;
    private GoogleApiClient mGoogleApiClient;
    private String deviceId;
    private String description;
    private String deviceos;
    private String osversion;
    private List<UserGroupsDto> userGroups = new ArrayList<UserGroupsDto>();
    private boolean isFeatureEnabled;
    private boolean isorderproductavailable;


    private static final int REQUEST = 112;
    protected static String[] PERMISSIONS_READ_STATE = {
            Manifest.permission.READ_PHONE_STATE
    };

    public boolean isorderproductavailable() {
        return isorderproductavailable;
    }

    public void setIsorderproductavailable(boolean isorderproductavailable) {
        this.isorderproductavailable = isorderproductavailable;
    }

    public boolean isProductEnabled() {
        return isProductEnabled;
    }

    public void setProductEnabled(boolean productEnabled) {
        isProductEnabled = productEnabled;
    }

    private boolean isProductEnabled;


    public boolean isGcmsubscription() {
        return gcmsubscription;
    }

    public void setGcmsubscription(boolean gcmsubscription) {
        this.gcmsubscription = gcmsubscription;
    }

    private boolean gcmsubscription;


    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getUserId() {
        return userId;
    }

    public String getDescription() {
        return description;
    }


    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceos() {
        return deviceos;
    }

    public void setDeviceos(String deviceos) {
        this.deviceos = deviceos;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    public UserObjectDto(Context context) {

        //  this.deviceId = Utils.getDeviceId(context);

        //   this.deviceId = getDeviceId(context);
        this.deviceos = Utils.getdeviceType();
        this.osversion = Utils.getDeviceOS();
//		defaultLocation = new LocationDto();

        //      buildGoogleApiClient(context);
    }


    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }


    public List<UserGroupsDto> getUserGroups() {
        return userGroups;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public void parse(JSONObject object, Context context) throws JSONException {


        this.userToken = object.getString(JSON_USERTOKEN);
        this.userId = object.getString(JSON_USERID);
        this.email = object.getString(JSON_EMAIL);
        this.userType = object.getString(JSON_USERTYPE);
        this.loginId = object.getString(JSON_LOGINID);

        if (!TextUtils.isEmpty(object.getString(JSON_GCMSUBSCRIPTION)) &&
                object.has(JSON_GCMSUBSCRIPTION)) {
            if (object.getString(JSON_GCMSUBSCRIPTION).equalsIgnoreCase("1")) {
                this.setGcmsubscription(true);
            } else {
                this.setGcmsubscription(false);
            }

        } else {
            this.setGcmsubscription(false);
        }

        if (!TextUtils.isEmpty(object.getString("groupinfo")) &&
                object.has("groupinfo")) {

            JSONArray userGroupArray = object.getJSONArray("groupinfo");
            for (int i = 0; i < userGroupArray.length(); i++) {
                UserGroupsDto dto = UserGroupsDto.parse(userGroupArray.getJSONObject(i));
                this.userGroups.add(dto);
                dto.setUserToken(this.userToken);
                dto.setUserId(this.userId);
                dto.setLoginId(this.loginId);
                //boolean insert = new MyGroupsMgr(context).insertGroup(dto);

                if (i == 0) {
                    this.setGroupImage(dto.getGroupImage());
                    this.setGroupId(dto.getGroupId());
                    this.setGroupIcon(dto.getGroupIcon());
                    this.setGroupTitle(dto.getTitle());
                    this.setLoginType(dto.getMemberType());
                    this.setGroupCode(dto.getGroupCode());
                    this.setFeatureEnabled(dto.isFeatureEnabled());
                    this.setProductEnabled(dto.isProductEnabled());
                    this.setIsHealthcare(dto.getIsHealthCare());
                    this.setIsorderproductavailable(dto.isorderproductavailable());
                }
            }

        }


    }

    public String getLoginType() {
        return loginType;
    }

    public LocationDto getDefaultLocation() {
        return defaultLocation;
    }

    public void setDefaultLocation(LocationDto defaultLocation) {
        this.defaultLocation = defaultLocation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserDetailsDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDetailsDto userDto) {
        this.userDto = userDto;
    }

    public String getGroupIcon() {
        return groupIcon;
    }

    public void setGroupIcon(String groupIcon) {
        this.groupIcon = groupIcon;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupImage() {
        return groupImage;
    }

    public void setGroupImage(String groupImage) {
        this.groupImage = groupImage;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }

    public String getLastlogintime() {
        return lastlogintime;
    }

    public void setLastlogintime(String lastlogintime) {
        this.lastlogintime = lastlogintime;
    }

    public boolean isFeatureEnabled() {
        return isFeatureEnabled;
    }

    public void setFeatureEnabled(boolean featureEnabled) {
        isFeatureEnabled = featureEnabled;
    }


    public String getIsHealthcare() {
        return isHealthcare;
    }

    public void setIsHealthcare(String isHealthcare) {
        this.isHealthcare = isHealthcare;
    }

    protected synchronized void buildGoogleApiClient(final Context context) {
        final GoogleApiClient.Builder builder = new GoogleApiClient.Builder(context);
        builder.addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {

            @Override
            public void onConnected(Bundle bundle) {


                try {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        Geocoder gcd = new Geocoder(context, Locale.getDefault());
                        if (mLastLocation == null) {
                        } else {
                            defaultLocation.setLatitude(String.valueOf(mLastLocation.getLatitude()));
                            defaultLocation.setLongitude(String.valueOf(mLastLocation.getLongitude()));
                            List<Address> addresses = gcd.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
                            if (addresses.size() > 0) {
                                defaultLocation.setCity(addresses.get(0).getLocality());
                                defaultLocation.setCountry(addresses.get(0).getCountryName());
                                defaultLocation.setZipCode(addresses.get(0).getPostalCode());

                            }
                        }

                    }


                } catch (Exception e) {
                }
            }

            @Override
            public void onConnectionSuspended(int i) {
            }
        });
        builder.addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {

            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {

            }


        });
        builder.addApi(LocationServices.API);
        builder.build();
        mGoogleApiClient.connect();
    }


//   private String getDeviceId(Context context) {
//        String code = null;
//        int permission = ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE);
//
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (permission != PackageManager.PERMISSION_GRANTED) {
//                Log.e("", "Permission not granted, granting now...");
//                // We don't have permission so prompt the user
//                ActivityCompat.requestPermissions(context, PERMISSIONS_READ_STATE, REQUEST);
//            } else {
//                if (TextUtils.isEmpty(code)) {
//                    TelephonyManager telephonyManager = (TelephonyManager) context
//                            .getSystemService(Context.TELEPHONY_SERVICE);
//                    code = telephonyManager.getDeviceId();
//
//                    if (TextUtils.isEmpty(code)) {
//                        code = UUID.randomUUID().toString();
//                    }
//                }
//            }
//        } else {
//            if (TextUtils.isEmpty(code)) {
//                TelephonyManager telephonyManager = (TelephonyManager) context
//                        .getSystemService(Context.TELEPHONY_SERVICE);
//                code = telephonyManager.getDeviceId();
//
//                if (TextUtils.isEmpty(code)) {
//                    code = UUID.randomUUID().toString();
//                }
//            }
//        }
//
//        return code;
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//            case REQUEST: {
//                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Log.d("Utils", "permission granted");
//                    if (TextUtils.isEmpty(code)) {
//                        TelephonyManager telephonyManager = (TelephonyManager) Utils.this
//                                .getSystemService(Context.TELEPHONY_SERVICE);
//                        code = telephonyManager.getDeviceId();
//
//                        if (TextUtils.isEmpty(code)) {
//                            code = UUID.randomUUID().toString();
//                        }
//                    }
//                } else {
//                    Log.d("Utils", "permission failed");
//                }
//                return;
//            }
//        }
//    }
}
