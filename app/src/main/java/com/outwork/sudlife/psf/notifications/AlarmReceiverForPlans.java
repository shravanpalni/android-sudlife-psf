package com.outwork.sudlife.psf.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.outwork.sudlife.psf.R;
import com.outwork.sudlife.psf.planner.PlannerMgr;
import com.outwork.sudlife.psf.planner.activities.CalenderActivity;
import com.outwork.sudlife.psf.planner.models.PlannerModel;
import com.outwork.sudlife.psf.utilities.Constants;
import com.outwork.sudlife.psf.utilities.SharedPreferenceManager;
import com.outwork.sudlife.psf.utilities.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by Habi on 22-09-2017.
 */
public class AlarmReceiverForPlans extends WakefulBroadcastReceiver {
    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
    private String scheduletime, after15, before15;
    //    private String contentText = "You have a task due";
    private String contentText = "";
    private Date date, date1, date2;
    private boolean isRepeating = false;
    private Uri notifySound;

    public void onReceive(Context context, Intent pintent) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        contentText = pintent.getStringExtra("contentText");
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, new Intent(context, CalenderActivity.class), 0);
        try {
            notifySound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notifySound);
        } catch (Exception e) {
            e.printStackTrace();
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentTitle("OutWork");
        builder.setContentText(contentText);
        builder.setSmallIcon(R.mipmap.ic_app_logo);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(contentText));
        builder.setSound(notifySound);
        if (Build.VERSION.SDK_INT >= 17) {
            builder.setShowWhen(true);
        }
        builder.setContentIntent(pIntent);

        Notification noti = builder.getNotification();

        noti.flags = Notification.FLAG_AUTO_CANCEL;

        final int _id = new Random().nextInt(443254);

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Utils.isNotNullAndNotEmpty(contentText)) {
            nm.notify(_id, noti);
        }
        if (pintent.getStringExtra("typeOfNotification").equalsIgnoreCase("single")) {
            PlannerMgr.getInstance(context).updatePlanNotificationStatus(pintent.getStringExtra("id"),
                    pintent.getStringExtra("notificationStatus"),
                    SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
        } else {
            List<PlannerModel> plannerModelList = GlobalData.getInstance().getPlannerModelArrayList();
            for (int i = 0; i < plannerModelList.size(); i++) {
                PlannerMgr.getInstance(context).updatePlanNotificationStatus(String.valueOf(plannerModelList.get(i).getLid()),
                        pintent.getStringExtra("notificationStatus"),
                        SharedPreferenceManager.getInstance().getString(Constants.USERID, ""));
            }
        }
    }

    public static String getCalculatedDate(String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        int j = days;
        cal.add(Calendar.DAY_OF_YEAR, j);
        return s.format(new Date(cal.getTimeInMillis()));
    }
}
