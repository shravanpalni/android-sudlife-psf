package com.outwork.sudlife.psf.targets.targetsmngr;

import android.content.Context;

import com.outwork.sudlife.psf.targets.db.MSBDao;
import com.outwork.sudlife.psf.targets.models.MSBModel;

import java.util.ArrayList;

public class TargetsMgr {

    private static Context context;
    private static MSBDao msbDao;

    public TargetsMgr(Context context) {
        this.context = context;
        msbDao = msbDao.getInstance(context);
    }

    private static TargetsMgr instance;


    public static TargetsMgr getInstance(Context context) {
        if (instance == null)
            instance = new TargetsMgr(context);
        return instance;
    }

    public void insertTargetsList(ArrayList<MSBModel> msbModelList) {
        msbDao.insertTargetsList(msbModelList);
    }

    public ArrayList<MSBModel> getMSBRecordsBasedOnMonth(String userid, int month) {
        return msbDao.getMSBRecordsBasedOnMonth(userid, month);
    }

    public ArrayList<MSBModel> getMSBRecoreds(String userid) {
        return msbDao.getMSBRecoreds(userid);
    }

    public ArrayList<MSBModel> getMSBOfflineRecoreds(String userid, String status) {
        return msbDao.getMSBOfflineRecoreds(userid, status);
    }

    public void updateTargetRecord(MSBModel msbModel) {
        msbDao.updateRecord(msbModel);
    }

    public void updateTargetRecordByStatus(MSBModel msbModel) {
        msbDao.updateTargetRecordByStatus(msbModel);
    }


}