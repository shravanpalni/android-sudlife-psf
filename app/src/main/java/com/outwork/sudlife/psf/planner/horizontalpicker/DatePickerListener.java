package com.outwork.sudlife.psf.planner.horizontalpicker;

import org.joda.time.DateTime;

public interface DatePickerListener {
    void onDateSelected(DateTime dateSelected);
}